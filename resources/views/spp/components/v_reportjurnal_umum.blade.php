@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Jurnal Umum</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Tahun</label>
                                <select class="form-control" id="tahun" name="tahun" required="">
                                    @if (empty(session('tahun')))
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun
                                        </option>
                                    @endif

                                    @foreach ($tahunlist as $tahunx)
                                        @if ($tahun == $tahunx)
                                            <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                            </option>
                                        @else
                                            <option value="{{ $tahunx }}">{{ $tahunx }}
                                            </option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Bulan</label>
                                <select class="form-control" id="bulan" name="bulan" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Bulan
                                    </option>
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                            </div>
                            <div class="col-md-4" style="margin-top: 10px;">
                                <button type="button" class="btn btn-sm btn-outline-danger mt-4 filter"><i
                                        class="fa fa-filter"></i></button>
                                <button type="button" class="btn btn-sm btn-outline-info mt-4 printNota"><i
                                        class="fa fa-print"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <hr>
                        <div class="widget-header text-center">
                            @if (!empty(session('sekolah')))
                                <b>{{ session('sekolah') }}</b>
                                <br>
                            @endif
                            <b>JURNAL UMUM</b>
                            <br>
                            <b>Periode Bulan {{ $bulan }} Tahun {{ $tahun }} </b>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table id="table_doc" width="100%;" border="1">
                                <thead class="table-info">
                                    <tr>
                                        <td class="text-center">Tanggal</td>
                                        <td class="text-center">Akun</td>
                                        <td class="text-center"> Debit</td>
                                        <td class="text-center">Kredit</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_debit = 0;
                                        $total_kredit = 0;
                                        $lastRowDateTime = null;
                                        $row_count = 0;
                                    @endphp
                                    @if (count($report) > 0)
                                        @foreach ($report as $key => $item)
                                            @php
                                                $row_count++;
                                            @endphp
                                            <tr>
                                                <td colspan="1" class="text-center">
                                                    {{ \Carbon\Carbon::parse($item['tgl_bayar'])->isoFormat('dddd, D MMMM Y ') }}
                                                </td>
                                                <td class="text-left">
                                                    <span class="ml-2">{{ $item['kode_akun'] }} -
                                                        {{ $item['akun'] }}</span>
                                                </td>
                                                @if ($item['debet'] != 0)
                                                    <td class="text-left">
                                                        <span
                                                            class="ml-3">{{ number_format($item['debet']) }}</span>
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif

                                                @if ($item['kredit'] != 0)
                                                    <td class="text-right">
                                                        <span
                                                            class="mr-3">{{ number_format($item['kredit']) }}</span>
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif

                                                @php
                                                    $total_debit += intval($item['debet']);
                                                    $total_kredit += intval($item['kredit']);
                                                @endphp
                                            </tr>
                                            @if ($row_count % 2 == 0)
                                                <tr>
                                                    <td colspan="4">
                                                        <div class="table-info" style="height:10px;"></div>
                                                    </td>
                                                <tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center text-red">Belum Ada Catatan </td>
                                        <tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" class="text-right"><span class="mr-3">Jumlah</span>
                                        </td>
                                        <td class="text-left"><span
                                                class="ml-2">Rp.{{ number_format($total_debit, 0) }}</span>
                                        </td>
                                        <td class="text-right"><span
                                                class="mr-2">Rp.{{ number_format($total_kredit, 0) }}</span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {

                @if (!empty($index_bulan))
                    $("select[name='bulan'] > option[value=" + {{ $index_bulan }} + "]").prop("selected",
                    true);
                @endif

                $('#table_doc').each(function() {
                    var Column_number_to_Merge = 1;

                    // Previous_TD holds the first instance of same td. Initially first TD=null.
                    var Previous_TD = null;
                    var i = 1;
                    $("tbody", this).find('tr').each(function() {
                        // find the correct td of the correct column
                        // we are considering the table column 1, You can apply on any table column
                        var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge +
                            ')');

                        if (Previous_TD == null) {
                            // for first row
                            Previous_TD = Current_td;
                            i = 1;
                        } else if (Current_td.text() == Previous_TD.text()) {
                            // the current td is identical to the previous row td
                            // remove the current td
                            Current_td.remove();
                            // increment the rowspan attribute of the first row td instance
                            Previous_TD.attr('rowspan', i + 1);
                            i = i + 1;
                        } else {
                            // means new value found in current td. So initialize counter variable i
                            Previous_TD = Current_td;
                            i = 1;
                        }
                    });
                });

                $('body').on('click', 'button.filter', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if (typeof bulan == 'underfined' || bulan == '' || bulan == null) {
                        window.notif('error', 'Silahkan pilih bulan terlebih dahulu');
                    } else {

                        var url =
                            '{{ route('filter_report_jurnal', ['bulan' => ':bulan', 'tahun' => ':tahun']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.target = "_parent";
                        a.href = url;
                        a.click();
                    }
                });

                $('body').on('click', 'button.printNota', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if (typeof bulan == 'underfined' || bulan == '' || bulan == null) {
                        window.notif('error', 'Silahkan pilih bulan terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('print_jurnal_umum', ['bulan' => ':bulan', 'tahun' => ':tahun']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }
                });

                @if (!empty($index_bulan))
                    $("select[name='bulan'] > option[value=" + {{ $index_bulan }} + "]").prop("selected",
                    true);
                @endif

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
