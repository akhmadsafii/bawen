@extends('spp.apps')
@section('spp.components')
    <style>
        table#table_doc {
            width: auto !important;
        }

    </style>
    <!-- Page Title Area -->
    <link rel="stylesheet"
        href="http://cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css" />
    <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css" />
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pemberitahuan</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">WhatApps Blast</li>
            </ol>
        </div>
    </div>
    <!-- /.page-title-left -->

    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">

                        <div class="tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item"><a class="nav-link active" href="#home-tab2" data-toggle="tab"
                                        aria-expanded="true">Single Contact</a>
                                </li>
                                <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                        href="#profile-tab2" data-toggle="tab" aria-expanded="false">Multiple Contact</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home-tab2" aria-expanded="true">
                                    <form id="DocForm" name="DocForm" method="POST" action="{{ route('send_wa_blast') }}"
                                        class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nomor WhatApps </label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="number" name="number"
                                                    placeholder="08xxxxx" type="text" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Pesan <span
                                                    class="text-red">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="message" id="pesan" rows="3" required placeholder="pesan Anda "></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="display:none;">
                                            <label class="col-md-3 col-form-label" for="l0">File Attach <span
                                                    class="text-red">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="file" name="file" id="file" accept="image/*,application/pdf">
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 btn-list">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="material-icons list-icon">send</i>
                                                            Kirim
                                                        </button>
                                                    </div>
                                                    <!-- /.col-sm-12 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="profile-tab2" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="box-title">Pilih Kontak WhatApps</div>
                                            <div class="table-responsive">
                                                <table id="table_doc" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>NIS</th>
                                                            <th>NISN</th>
                                                            <th>Nama Siswa</th>
                                                            <th>Kelas</th>
                                                            <th>Jurusan</th>
                                                            <th>Kontak</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- Append Create Datatables-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <form id="DocForm" name="DocForm" method="POST"
                                                action="{{ route('send_wa_blast_multi') }}" class="form-horizontal"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="col-md-3 col-form-label" for="l0">Nomor WhatApps </label>
                                                    <div class="col-md-9">
                                                        <input class="form-control" id="number" name="numberx"
                                                            placeholder="08xxxxx" type="text" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 col-form-label" for="l0">Pesan <span
                                                            class="text-red">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="message" id="pesan" rows="3" required placeholder="pesan Anda "></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 btn-list">
                                                            <button type="submit" class="btn btn-primary">
                                                                <i class="material-icons list-icon">send</i>
                                                                Kirim
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <!-- /.form-group -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript"
        src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
    <script type="text/javascript" language="javascript"
        src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        },
                    }, ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_siswa_kontak') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'jurusan',
                            name: 'jurusan'
                        },
                        {
                            data: 'telepon',
                            name: 'telepon'
                        },
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);
                var telepone = [];
                $('#table_doc tbody').on('click', 'tr', function() {
                    var data = table_setting.api().row(this).data();
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                        const index = telepone.indexOf(data['telepon']);
                        if (index == 0) {
                            telepone.shift();
                        } else if (index > -1) {
                            telepone.splice(index, 1); // 2nd parameter means remove one item only
                        }
                    } else {
                        $(this).addClass('selected');
                        telepone.push(data['telepon']);
                    }
                    $('input[name="numberx"]').val(telepone.toString());
                });

            });
        })(jQuery, window);
    </script>
@endsection
