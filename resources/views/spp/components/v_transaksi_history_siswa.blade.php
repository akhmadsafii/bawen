@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Histori</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Transaksi Pembayaran SPP</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading text-left">
                        <div class="headersx"></div>
                        <div class="row " style="display:none;">
                            <div class="col-md-4">
                                <label>Tanggal Mulai <span class="text-red">*</span></label>
                                <div class="input-group input-has-value">
                                    <input type="text" class="form-control datepicker" name="tgl_mulai" id="tgl_mulai"
                                        data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"> <span
                                        class="input-group-addon"><i class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Tanggal Akhir <span class="text-red">*</span></label>
                                <div class="input-group input-has-value">
                                    <input type="text" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir"
                                        data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"> <span
                                        class="input-group-addon"><i class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_doc" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Jurusan</th>
                                        <th>Nominal</th>
                                        <th>Tanggal Bayar </th>
                                        <th>Tanggal filter </th>
                                        <th>Tahun Ajaran </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Append Create Datatables-->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Jurusan</th>
                                        <th>Nominal</th>
                                        <th>Tanggal Bayar </th>
                                        <th>Tanggal filter </th>
                                        <th>Tahun Ajaran </th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            var minDate, maxDate;


            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-history-siswa') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'kelas',
                            name: 'kelas'
                        },
                        {
                            data: 'jurusan',
                            name: 'jurusan'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar'
                        },
                        {
                            data: 'tgl_filter',
                            name: 'tgl_filter',
                            visible:false
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);

                //show data
                $('body').on('click', '.show.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result[
                                        'data']));

                                    $('#printStruckModal').modal('show');
                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info">Informasi Siswa </h5>
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info">Informasi Transaksi </h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata + `<div class="col-md-12">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info">Informasi Pembayaran </h5>
                                           <table class="table">
                                            <thead>
                                                 <tr>
                                                    <td>#</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Bulan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                        `;

                                    list_bulan = ['-',
                                        'Juli', 'Agustus', 'September',
                                        'Oktober', 'November', 'Desember',
                                        'Januari', 'Februari', 'Maret',
                                        'April', 'Mei', 'Juni'
                                    ];

                                    rows.tagihan.forEach(element => {
                                        printdata = printdata + `
                                                <tr>
                                                    <td>#</td>
                                                    <td>` + element['nama_tagihan'] + `</td>
                                                    <td>` + list_bulan[element['bulan']] + `</td>
                                                    <td>` + element['nominal'].format() + `</td>
                                                </tr>
                                            `;
                                    });
                                    printdata = printdata + `</table>`;

                                    $('.print-data').html(printdata);

                                    $('.printNota').attr('data-id', rows.id);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                }
                            }

                        }
                    });
                });


                //print history
                $('body').on('click', '.print.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('print_nota', ':id') }}';
                    url = url.replace(':id', id);
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = url;
                    a.click();
                });

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

            });
        })(jQuery, window);
    </script>
@endsection
