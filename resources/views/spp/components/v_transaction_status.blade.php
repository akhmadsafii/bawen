<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('spp.includes.head')
</head>

<body class="body-bg-full profile-page">

    <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-12 col-md-6 ml-md-auto login-center login-center-mini mx-auto">
            <div class="navbar-header">
                @if (count($transaction) > 0)
                    <table width="100%" class="table">
                        <tbody>
                            <tr>
                                <td> Kode Transaksi </td>
                                <td>:</td>
                                <td>{{ $transaction['order_id'] }}</td>
                            </tr>
                            <tr>
                                <td> Total Transaksi </td>
                                <td>:</td>
                                <td>{{ number_format($transaction['gross_amount'], 0) }}</td>
                            </tr>
                            @if (!empty($transaction['transaction_status']))

                                <tr>
                                    <td> Status Pembayaran </td>
                                    <td>:</td>
                                    <td class="text-success mt-2">
                                        @if ($transaction['transaction_status'] == 'settlement')
                                            Success
                                        @elseif ($transaction['transaction_status'] == 'pending')
                                            Pending
                                        @elseif ($transaction['transaction_status'] == 'deny')
                                            Failed
                                        @elseif ($transaction['transaction_status'] == 'expire')
                                            Expired
                                        @elseif ($transaction['transaction_status'] == 'cancel')
                                            Cancel
                                        @elseif ($transaction['transaction_status'] == 'capture')
                                            Capture
                                        @endif
                                    </td>
                                </tr>

                            @endif

                            @if (!empty($transaction['settlement_time']))
                                <tr>
                                    <td> Waktu </td>
                                    <td>:</td>
                                    <td>
                                        {{ $transaction['settlement_time'] }}
                                    </td>
                                </tr>
                            @endif
                            @if (!empty($transaction['payment_type']))
                                <tr>
                                    <td> Virtual Account Bank </td>
                                    <td>:</td>
                                    <td>
                                        @if ($transaction['payment_type'] == 'bank_transfer')
                                            @if (!empty($transaction['permata_va_number']))
                                                Permata - {{ $transaction['permata_va_number'] }}
                                            @endif
                                            @if (!empty($transaction['bank']))
                                                {{ $transaction['bank'] }} - {{ $transaction['va_number'] }}
                                            @endif
                                            @if (!empty($transaction['va_numbers']) > 0)
                                                @foreach ($transaction['va_numbers'] as $list)
                                                    {{ $list['bank'] }} - {{ $list['va_number'] }}
                                                @endforeach
                                            @endif
                                        @endif
                                        @if ($transaction['payment_type'] == 'echannel')
                                            {{ $transaction['bill_key'] }} - {{ $transaction['biller_code'] }}
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                @else
                    <p class="text-red">Error Data </p>
                @endif

            </div>
            <p class="text-center text-muted"></p><a href="{{ route('sumbangan-spp') }}"
                class="btn btn-block btn-primary ripple mr-tb-30">Back to Home</a>
        </div>
        <!-- /.login-center -->
    </div>

    @include('spp.includes.foot')
</body>

</html>
