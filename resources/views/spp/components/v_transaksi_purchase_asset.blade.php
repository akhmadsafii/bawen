@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi Pembelian Aset </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pembelian Aset </li>
                <li class="breadcrumb-item"> <a href="{{ route('asset_aktiva') }}">Tambah Aset</a> </li>
                <li class="breadcrumb-item"> <a href="{{ route('history_jurnal') }}">Histori Jurnal Umum</a> </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="modal fade bs-modal-lg" tabindex="-1" id="detailasset" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaction Purchase Asset Fixed </h5>
                </div>
                <div class="modal-body">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>Kode Transaksi </td>
                                <td>:</td>
                                <td><span class="text-kode_transaksi ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Tanggal Transaksi </td>
                                <td>:</td>
                                <td><span class="text-tanggal_transaksi ml-3"></span></td>
                                <td>Tanggal Diterima </td>
                                <td>:</td>
                                <td><span class="text-tanggal_diterima ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Kode Akun </td>
                                <td>:</td>
                                <td><span class="text-kode_akun ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Total Transaksi </td>
                                <td>:</td>
                                <td><span class="text-total_transaksi ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Status Transaksi </td>
                                <td>:</td>
                                <td><span class="text-status_transaksi ml-3"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="table-responsive">
                        <table id="table_doc_item" class="table table-striped table-responsive">
                            <thead>
                                <tr class="table-info">
                                    <th>No</th>
                                    <th>Nama Item</th>
                                    <th>Harga Beli</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><span class="text-nama_item ml-3"></td>
                                    <td><span class="text-harga_beli ml-3"></td>
                                    <td><span class="text-qty_jumlah"></span>
                                    <td><span class="text-subtotal ml-3"></span></td>
                                    <td><span class="text-note ml-3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>

                    <form id="DocFormxEdit" name="DocFormxEdit" class="form-horizontal" enctype="multipart/form-data">
                       @csrf
                        <input type="hidden" name="id_edit_item" class="form-control">
                        <div class="box-title">Form Penyusutan Aset </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="l36">Usia Tahun </label>
                                    <input class="form-control"  id="usia_tahun" name="usia_tahun"  type="number" required="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="l37">Usia Bulan </label>
                                    <input class="form-control"  id="usia_bulan" name="usia_bulan"  type="number" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions btn-list">
                            <button class="btn btn-primary update-penyusutan" type="submit" >Update</button>
                            <button class="btn btn-outline-default" type="reset">Reset</button>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data"
                            method="POST" action="{{ route('store_transaksi_purchase') }}">
                            @csrf
                            <div class="row mr-b-50">
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer01">Tahun Ajaran</label>
                                    <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                        </option>
                                        @foreach ($tahun_ajaran as $key => $value)
                                            @php
                                                $explode_tahun = explode('/', $value['tahun_ajaran']);
                                            @endphp
                                            @if ($explode_tahun[0] == session('tahun'))
                                                <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                    {{ $value['tahun_ajaran'] }}
                                                </option>
                                            @else
                                                <option value="{{ $value['tahun_ajaran'] }}">
                                                    {{ $value['tahun_ajaran'] }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer02">Tanggal Transaksi</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl_trans"
                                            data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                            value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer02">Tanggal Diterima</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl_diterima"
                                            data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                            value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer02">Jenis Transaksi</label>
                                    <select class="form-control" id="sumber" name="sumber" required="">
                                        <option disabled="disabled" selected="true" value="">Pilih Jenis Transaksi
                                        </option>
                                        @foreach ($sumberx as $sumber)
                                            <option value="{{ $sumber['id'] }}"> {{ $sumber['kode'] }} -
                                                {{ $sumber['nama'] }}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer03">Supplier </label>
                                    <input type="text" class="form-control" id="validationServer03" placeholder="Supplier"
                                        required="" name="supplier">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer04">Kontak Supplier </label>
                                    <input type="text" class="form-control" id="validationServer04"
                                        placeholder="No.Kontak" required="" name="telepon">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationServer05">Alamat Supplier </label>
                                    <input type="text" class="form-control" id="validationServer05" placeholder="Alamat"
                                        required="" name="alamat">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="table_pengeluaran" width="100%" class="table table-striped table-responsive">
                                    <thead>
                                        <tr class="table-info">
                                            <th>Akun <span class="text-red">*</span></th>
                                            <th>Nama Aset <span class="text-red">*</span></th>
                                            <th>Harga Beli <span class="text-red">*</span></th>
                                            <th>Quantity<span class="text-red">*</span></th>
                                            <th>Subtotal</th>
                                            <th>Catatan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="clonable form-clone" parentId="1">
                                            <td>
                                                <select class="form-control akun_aktiva" id="akun_aktiva" name="akun_aktiva[]" required="">
                                                    <option disabled="disabled" selected="true" value="">Pilih Akun
                                                    </option>
                                                    @foreach ($akun_asset as $akun)
                                                        <option value="{{ $akun['id'] }}">
                                                            {{ $akun['nama_akun'] }} - {{ $akun['kode_akun'] }} - {{ $akun['nama'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input name="nama_asset[]" type="text" id="nama_asset" class="form-control"
                                                    placeholder="Nama Asset " readonly="readonly">
                                                <input name="id_akun_asset[]" type="hidden" id="id_akun_asset" class="form-control">
                                                <input name="id_asset[]" type="hidden" id="id_asset" class="form-control">
                                            </td>
                                            <td><input name="nominal[]" type="text" id="nominal" class="form-control"
                                                    onkeyup="currencyFormat(this)" placeholder="Nominal"
                                                    required="required">
                                            </td>
                                            <td><input name="qty[]" type="number" id="qty" class="form-control qty"
                                                    placeholder="Quantity" required="required">
                                            </td>
                                            <td><input name="subtotal[]" type="text" id="subtotal" class="form-control"
                                                    placeholder="Subtotal" readonly>
                                            <td>
                                                <textarea class="form-control" name="catatan[]" id="catatan" rows="1"
                                                    required="required"></textarea>
                                            </td>

                                            <td width="10%" class="btn-group text-center" role="group" width="100%">
                                                <button type="button" name="remove" id="removex"
                                                    class="btn btn-danger remove-clone remove"><i
                                                        class="fa fa-minus"></i></button> &nbsp;
                                                <button type="button" name="add" id="addx"
                                                    class="btn btn-info add-clone add"><i
                                                        class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <td colspan="2">
                                            <div class="form-group">
                                                <label for="l38">Keterangan / Memo</label>
                                                <textarea class="form-control" name="keterangan" id="keterangan"
                                                    rows="3"></textarea>
                                            </div>
                                        </td>
                                        <td colspan="2">
                                            <div class="form-group row ml-3">
                                                <label for="l38"><i class="fa fa-file"></i> Berkas</label><br>
                                                <span class="text-info ml-3" id="tambah_berkas"><input name="image"
                                                        type="file" id="image" class="form-control"
                                                        accept="image/*"></span>
                                            </div>
                                        </td>
                                    </tfoot>
                                </table>
                            </div>
                            <center>
                                <button type="submit" class="btn btn-info update pos_transaction">
                                    <i class="material-icons list-icon">save</i>
                                    Simpan
                                </button>
                                <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i> Batal
                                </button>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-title">List Transaction Purchase Fixed Asset </div>
                            <div class="widget-body clearfix">
                                <div class="table-responsive">
                                    <table id="table_list_barang" class="table table-striped table-responsive"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode</th>
                                                <th>Tanggal Transaksi </th>
                                                <th>Tanggal Diterima </th>
                                                <th>Kode Akun</th>
                                                <th>Aset</th>
                                                <th>Qty</th>
                                                <th>Harga Beli </th>
                                                <th>Subtotal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-title">List Depreciation Asset </div>
                            <div class="widget-body clearfix">
                                <div class="table-responsive">
                                    <table id="table_list_barang_penyusutan" class="table table-striped table-responsive"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Akun</th>
                                                <th>Nama Aset</th>
                                                <th>Nominal</th>
                                                <th>Usia Tahun</th>
                                                <th>Usia Bulan</th>
                                                <th>Penyusutan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            var config_table, config_table2, table_setting, table_detailitem, table_setting2, table_setting1;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_asset_purchase') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_transaksi',
                            name: 'kode_transaksi'
                        },
                        {
                            data: 'tgl_transaksi',
                            name: 'tgl_transaksi'
                        },
                        {
                            data: 'tgl_diterima',
                            name: 'tgl_diterima'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_asset',
                            name: 'nama_asset'
                        },
                        {
                            data: 'jumlah',
                            name: 'jumlah'
                        },
                        {
                            data: 'satuan',
                            name: 'satuan'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_list_barang').dataTable(config_table);
                config_table2 = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_asset_purchase') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_asset',
                            name: 'nama_asset'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'usia_tahun',
                            name: 'usia_tahun'
                        },
                        {
                            data: 'usia_bulan',
                            name: 'usia_bulan'
                        },
                        {
                            data: 'penyusutan',
                            name: 'penyusutan'
                        },
                    ],
                };

                table_setting2 = $('#table_list_barang_penyusutan').dataTable(config_table2);
                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('input[type="number"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });


                $('body').on('click', '.show.asset.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_asset_aktiva_transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    $('#detailasset').modal('show');
                                    $('span.text-kode_transaksi').html(rows.kode_transaksi);
                                    $('span.text-tanggal_transaksi').html(dateFormatID(rows
                                        .tgl_transaksi));
                                    $('span.text-tanggal_diterima').html(dateFormatID(rows
                                        .tgl_diterima));
                                    $('span.text-kode_akun').html(rows.kode_akun);
                                    $('span.text-total_transaksi').html(rows.nominal
                                        .format());

                                    $('span.text-harga_beli').html(rows.satuan
                                        .format());

                                    $('span.text-subtotal').html(rows.nominal
                                        .format());

                                    $('span.text-note').html(rows.keterangan);
                                    $('span.text-nama_item').html(rows.nama_asset);
                                    $('input[name="usia_tahun"]').val(rows.usia_tahun);
                                    $('input[name="usia_bulan"]').val(rows.usia_bulan);
                                    $('input[name="id_edit_item"]').val(rows.id);
                                    $('span.text-qty_jumlah').html(rows.jumlah);
                                    if (rows.status == '1') {
                                        $('span.text-status_transaksi').html('Beli');
                                    } else if (rows.status == '2') {
                                        $('span.text-status_transaksi').html('Jual');
                                    }
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });

                });

                $('body').on('change','select.akun_aktiva',function(){
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    var id       = $(this).val();
                    var url      = "{{ route('show_asset_aktiva',':id') }}";
                        url      = url.replace(':id',id);
                    if(id){
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                            },
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON.stringify(result['data']));
                                        $('tr[parentId="' + parentId + '"] input[name="id_akun_asset[]"]').val(rows.id_akun);
                                        $('tr[parentId="' + parentId + '"] input[name="nama_asset[]"]').val(rows.nama);
                                        $('tr[parentId="' + parentId + '"] input[name="id_asset[]"]').val(rows.id);
                                    }else if(result['info'] == 'error'){
                                        window.notif('error', result['message']);
                                    }
                                }
                            }
                        });

                    }

                });

                $('body').on('keyup', 'input.qty', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    var qty = $(this).val();
                    var subtotal = 0;
                    var harga_beli = $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').val()
                        .split(',').join('');;
                    if (harga_beli == null || harga_beli == '') {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').attr('class',
                            'form-control is-invalid');
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').removeAttr(
                            'form-control is-invalid');
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').attr(
                            'form-control');
                    }
                    subtotal = parseInt(qty) * parseInt(harga_beli);
                    if (isNaN(subtotal)) {
                        $('tr[parentId="' + parentId + '"] input[name="subtotal[]"]').val('0');
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="subtotal[]"]').val(subtotal.format());
                    }
                });

                $('body').on('keydown', 'input.qty', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    var qty = $(this).val();
                    var subtotal = 0;
                    var harga_beli = $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').val()
                        .split(',').join('');;
                    if (harga_beli == null || harga_beli == '') {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').attr('class',
                            'form-control is-invalid');
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').removeAttr(
                            'form-control is-invalid');
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').attr(
                            'form-control');
                    }
                    subtotal = parseInt(qty) * parseInt(harga_beli);
                    if (isNaN(subtotal)) {
                        $('tr[parentId="' + parentId + '"] input[name="subtotal[]"]').val('0');
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="subtotal[]"]').val(subtotal.format());
                    }

                });


                //submit update
                $('body').on('submit', '#DocFormxEdit', function(e) {
                    e.preventDefault();
                    var id_piutang  =  $('input[name="id_edit_item"]').val();
                    var urlx = '{{ route('store_update_penyusutan_asset') }}';
                    var formData = new FormData(this);
                    const loader = $('update-penyusutan');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading'
                            );
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#detailasset').modal('hide');
                                    table_setting2.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result[
                                        'message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result[
                                        'message']);
                                    $('#detailasset').modal('hide');
                                    table_setting2.fnDraw(false);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html(
                                    '<i class="fa fa-save"></i> Update');
                                $('#transaksi_cicilan').modal('hide');
                                table_setting.fnDraw(false);
                            }
                        }
                    });
                });


                window.appendDetailItem = function detailitem(kode) {
                    var urlx = "{{ route('show_by_kode_ajax_detail_asset', ['id' => ':id']) }}";
                    urlx = urlx.replace(':id', kode);
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'tgl_transaksi',
                                name: 'tgl_transaksi'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting1 = $('#table_doc_item').dataTable(config_table);
                };

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //format date indonesia
                window.dateFormatID = function parseDate(xt) {
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
                        'September', 'Oktober', 'November', 'Desember'
                    ];
                    var tanggal = new Date(xt).getDate();
                    var xhari = new Date(xt).getDay();
                    var xbulan = new Date(xt).getMonth();
                    var xtahun = new Date(xt).getYear();
                    var harix = hari[xhari];
                    var bulanx = bulan[xbulan];
                    var tahunx = (xtahun < 1000) ? xtahun + 1900 : xtahun;
                    var fulldate = harix + ', ' + tanggal + ' ' + bulanx + ' ' + tahunx;
                    return fulldate;
                }

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }
            });
        })(jQuery, window);
    </script>
@endsection
