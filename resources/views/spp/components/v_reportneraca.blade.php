@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Neraca</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Tahun</label>
                                <select class="form-control" id="tahun" name="tahun" required="">
                                    @if (empty(session('tahun')))
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun
                                        </option>
                                    @endif

                                    @foreach ($tahunlist as $tahunx)
                                        @if ($tahun == $tahunx)
                                            <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                            </option>
                                        @else
                                            <option value="{{ $tahunx }}">{{ $tahunx }}
                                            </option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-4" style="display:none;">
                                <label>Bulan</label>
                                <select class="form-control" id="bulan" name="bulan" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Bulan
                                    </option>
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                            </div>
                            <div class="col-md-4" style="margin-top: 10px;">
                                <button type="button" class="btn btn-sm btn-outline-danger filter mt-4"><i
                                        class="fa fa-filter"></i></button>
                                <button type="button" class="btn btn-sm btn-outline-info mt-4 printNota"><i
                                        class="fa fa-print"></i></button>
                                <a href="{{ route('export_neraca', ['tahun' => $tahun]) }}"
                                    class="btn btn-sm btn-outline-info mt-4">Export</a>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        @php
                            $saldo_awal_modal = 0;
                            $saldo_akun_aktiva_lancar = 0;
                            $jumlah_saldo_akun_aktiva_lancar = 0;
                            $saldo_akun_aktiva_tetap = 0;
                            $jumlah_saldo_akun_aktiva_tetap = 0;

                            $saldo_akun_investasi = 0;
                            $jumlah_akun_investasi = 0;
                            $total_investasi = 0;

                            $saldo_akun_aktiva_non = 0;
                            $jumlah_akun_aktiva_non = 0;
                            $total_aktiva_non = 0;

                            $saldo_akun_aktiva_lain = 0;
                            $jumlah_akun_aktiva_lain = 0;
                            $total_aktiva_lain = 0;

                            $total_aktiva = 0;
                            $modal = 0;
                            $jumlah_modal = 0;
                            $hutang = 0;
                            $jumlah_hutang = 0;
                            $total_passiva = 0;
                            $total_passivax = 0;
                            $laba_ditahan = 0;
                        @endphp
                        <div class="table-responsive">
                            <hr>
                            <div class="widget-header text-center">
                                @if (!empty(session('sekolah')))
                                    <b>{{ session('sekolah') }}</b>
                                    <br>
                                @endif
                                <b>NERACA</b>
                                <br>
                                <b>Periode Tahun {{ $tahun }} </b>
                            </div>
                            <hr>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr class="table-info">
                                        <td><b>Aktiva</b></td>
                                        <td><b>Passiva</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="table table-hover">
                                                <tbody>
                                                    @foreach ($report_neraca as $key => $laporan)
                                                        @if ($key == 'aktiva')
                                                            @foreach ($laporan as $list)
                                                                <tr>
                                                                    <td>{{ $list['nama'] }}</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                @foreach ($list['akun'] as $akunx => $aktiva)
                                                                    @if ($list['kode'] == '1.001')
                                                                        @php
                                                                            $saldo_akun_aktiva_lancar = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                            $jumlah_saldo_akun_aktiva_lancar += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                        @endphp
                                                                        <tr>
                                                                            <td> <span class="ml-2">
                                                                                    {{ $aktiva['nama'] }}</span></td>
                                                                            <td>{{ number_format($saldo_akun_aktiva_lancar, 0) }}
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    @endif
                                                                    @if ($list['kode'] == '1.002')
                                                                        @php
                                                                            $saldo_akun_aktiva_tetap = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                            $jumlah_saldo_akun_aktiva_tetap += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                        @endphp
                                                                        <tr>
                                                                            <td> <span class="ml-2">
                                                                                    {{ $aktiva['nama'] }}</span></td>
                                                                            <td>{{ number_format($saldo_akun_aktiva_tetap, 0) }}
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    @endif

                                                                    @if ($list['kode'] == '1.004')
                                                                        @php
                                                                            $saldo_akun_aktiva_non = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                            $jumlah_akun_aktiva_non += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                        @endphp
                                                                        <tr>
                                                                            <td> <span class="ml-2">
                                                                                    {{ $aktiva['nama'] }}</span></td>
                                                                            <td>{{ number_format($saldo_akun_aktiva_non, 0) }}
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    @endif
                                                                    @if ($list['kode'] == '1.005')
                                                                        @php
                                                                            $saldo_akun_aktiva_lain = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                            $jumlah_akun_aktiva_lain += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                                        @endphp
                                                                        <tr>
                                                                            <td> <span class="ml-2">
                                                                                    {{ $aktiva['nama'] }}</span></td>
                                                                            <td>{{ number_format($saldo_akun_aktiva_lain, 0) }}
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    @endif
                                                                    @php
                                                                        $saldo_awal_modal = intval($aktiva['saldo_awal']);
                                                                    @endphp
                                                                @endforeach

                                                                <tr>
                                                                    <td><span class="ml-2"><b>Jumlah
                                                                                {{ $list['nama'] }}</b></span>
                                                                    </td>
                                                                    <td></td>
                                                                    @if ($list['kode'] == '1.001')
                                                                        <td>{{ number_format($jumlah_saldo_akun_aktiva_lancar, 0) }}
                                                                        </td>
                                                                    @elseif ($list['kode'] == '1.002')
                                                                        <td>{{ number_format($jumlah_saldo_akun_aktiva_tetap, 0) }}
                                                                        </td>
                                                                    @elseif ($list['kode'] == '1.003')
                                                                        <td>{{ number_format($jumlah_akun_investasi, 0) }}
                                                                        </td>
                                                                    @elseif ($list['kode'] == '1.004')
                                                                        <td>{{ number_format($jumlah_akun_aktiva_non, 0) }}
                                                                        </td>
                                                                    @elseif ($list['kode'] == '1.005')
                                                                        <td>{{ number_format($jumlah_akun_aktiva_lain, 0) }}
                                                                        </td>
                                                                    @endif
                                                                </tr>
                                                                @php
                                                                    $total_aktiva = intval($jumlah_saldo_akun_aktiva_lancar) + intval($jumlah_saldo_akun_aktiva_tetap) + intval($jumlah_akun_investasi) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_lain);
                                                                @endphp
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="table table-hover">
                                                <tbody>
                                                    @foreach ($report_neraca as $key => $laporan)
                                                        @if ($key == 'modal')
                                                            @foreach ($laporan as $list)
                                                                <tr>
                                                                    <td>{{ $list['nama'] }}</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                @foreach ($list['akun'] as $akunx => $aktiva)
                                                                    @php
                                                                        if ($aktiva['saldo_awal'] == 0) {
                                                                            $modal = intval($saldo_awal_modal) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                            $jumlah_modal += intval($saldo_awal_modal) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                        } else {
                                                                            $modal = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                            $jumlah_modal += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                        }

                                                                    @endphp
                                                                    <tr>
                                                                        <td> <span class="ml-2">
                                                                                {{ $aktiva['nama'] }}</span></td>
                                                                        <td>{{ number_format($modal, 0) }}
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endforeach
                                                            <tr>
                                                                <td><span class="ml-2"><b>Jumlah
                                                                            Modal </b></span>
                                                                </td>
                                                                <td></td>
                                                                <td>{{ number_format($jumlah_modal, 0) }}
                                                                </td>
                                                            </tr>
                                                        @elseif($key == 'hutang')
                                                            @foreach ($laporan as $list)
                                                                <tr>
                                                                    <td>{{ $list['nama'] }}</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                @foreach ($list['akun'] as $akunx => $aktiva)
                                                                    @php
                                                                        $hutang = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                        $jumlah_hutang += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                                    @endphp
                                                                    <tr>
                                                                        <td> <span class="ml-2">
                                                                                {{ $aktiva['nama'] }}</span></td>
                                                                        <td>{{ number_format($hutang, 0) }}
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endforeach
                                                            <tr>
                                                                <td><span class="ml-2"><b>Jumlah Hutang /
                                                                            Kewajiban </b></span>
                                                                </td>
                                                                <td></td>
                                                                <td>{{ number_format($jumlah_hutang, 0) }}</td>
                                                            </tr>
                                                        @endif
                                                        @php
                                                            $total_passiva = intval($jumlah_modal) + intval($jumlah_hutang);
                                                            $laba_ditahan = intval($report_laba_rugi['laba_rugi']);
                                                            $total_passivax = intval($total_passiva) + intval($laba_ditahan);
                                                        @endphp
                                                    @endforeach
                                                    <tr>
                                                        <td><span class="ml-2"><b> Laba / rugi </b></span>
                                                        </td>
                                                        <td></td>
                                                        <td>{{ number_format($laba_ditahan, 0) }}</td>
                                                        </tr-->
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Total Aktiva &nbsp;<span class="ml-5">
                                                <b>{{ number_format($total_aktiva, 0) }}</b> </span>
                                        </td>
                                        <td align="center ">Total Passiva &nbsp; <span
                                                class="ml-5"><b>{{ number_format($total_passivax, 0) }}</b></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $('body').on('click', 'button.filter', function() {
                    var tahun = $('select[name="tahun"] option:selected').val();
                    var split_tahun = tahun.split('/');
                    if ((typeof tahun == 'underfined' || tahun == '' || tahun == null)) {
                        window.notif('error', 'Silahkan pilih Tahun filter terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('report_neraca_filter', ['tahun' => ':tahun']) }}';
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.target = "_parent";
                        a.href = url;
                        a.click();
                    }
                });

                $('body').on('click', 'button.printNota', function() {
                    var tahun = $('select[name="tahun"] option:selected').val();
                    var split_tahun = tahun.split('/');
                    if ((typeof tahun == 'underfined' || tahun == '' || tahun == null)) {
                        window.notif('error', 'Silahkan pilih Tahun filter terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('report_neraca_print', ['tahun' => ':tahun']) }}';
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }
                });
            });
        })(jQuery, window);
    </script>
@endsection
