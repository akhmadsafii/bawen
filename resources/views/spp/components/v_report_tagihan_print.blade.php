<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        .text-center {
            text-align: center !important;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <h1>{{ session('sekolah') }}</h1>
    <p>Tagihan Siswa Belum dibayar </p>

    <table>
        <tbody>
            <tr>
                <td>Kelas</td>
                <td>:</td>
                <td>{{ $nama_kelas }}</td>
            </tr>
            <tr>
                <td>Tahun Ajaran</td>
                <td>:</td>
                <td>{{ $nama_tahun_ajaran }}</td>
            </tr>
        </tbody>
    </table>

    <table class="table" width="100%;">
        <thead class="table-info">
            <tr>
                <td rowspan='2' class="text-center" style="text-align: center; vertical-align: middle;">No</td>
                <td rowspan='2' class="text-center" style="text-align: center; vertical-align: middle;">NIS</td>
                <td rowspan='2' class="text-center" style="text-align: center; vertical-align: middle;">Nama</td>
                <td colspan='{{ count($pos) }}' class="text-center"
                    style="text-align: center; vertical-align: middle;">Tagihan</td>
                <td rowspan='2' class="text-center" style="text-align: center; vertical-align: middle;">Total</td>
            </tr>
            <tr>
                @php

                    $listColumn = [];
                    foreach ($report_tagihan as $t) {
                        foreach ($t['tagihan'] as $m) {
                            array_push($listColumn, $m['nama']);
                        }
                    }
                    $filteredList = array_unique($listColumn);
                @endphp
                @foreach ($filteredList as $v)
                    <td class="text-center" style="text-align: center; vertical-align: middle;">
                        {{ $v }}</td>
                @endforeach
            <tr>
        </thead>
        <tbody class="text-center">
            @if (count($report_tagihan) > 0)
                @foreach ($report_tagihan as $key => $valx)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $valx['nis'] }}</td>
                        <td>{{ ucfirst($valx['nama']) }}</td>
                        @php
                            $total_tagihan = 0;
                        @endphp
                        @foreach ($valx['tagihan'] as $x => $d)
                            @if (intval($d['ditagih']) == 0)
                                <td class="text-info">{{ number_format($d['ditagih'], 0) }}
                                </td>
                            @else
                                <td class="text-red">{{ number_format($d['ditagih'], 0) }}
                                </td>
                            @endif
                            @php
                                $total_tagihan += intval($d['ditagih']);
                            @endphp
                        @endforeach
                        <td colspan='{{ count($pos) }}' >{{ number_format($total_tagihan,0) }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-red" colspan="4">Tagihan tidak ditemukan</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>

</html>
