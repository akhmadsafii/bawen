@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Akuntansi</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Akun</li>
                <li class="breadcrumb-item"><a href="{{ route('akun_kategori_spp') }}">Akun Kategori</a></li>
                <li class="breadcrumb-item"><a href="{{ route('akun_subkategori_spp') }}">Sub Akun Kategori</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- show detail -->
    <div class="modal modal-danger fade bs-modal-lg" id="showModalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Detail Akun </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Kode</label>
                        <div class="col-md-9">
                            <input class="form-control" id="kode_show" placeholder="Readonly" readonly="" type="text"
                                name="kode_show">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Nama</label>
                        <div class="col-md-9">
                            <input class="form-control" id="nama_show" placeholder="Readonly" readonly="" type="text"
                                name="name_show">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Kategori</label>
                        <div class="col-md-9">
                            <input class="form-control" id="kategori_show" placeholder="Readonly" readonly="" type="text"
                                name="kategori_show">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Sub Kategori</label>
                        <div class="col-md-9">
                            <input class="form-control" id="subkategori_show" placeholder="Readonly" readonly=""
                                type="text" name="subkategori_show">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Jenis Transaksi</label>
                        <div class="col-md-9">
                            <input class="form-control" id="jenis_transaksi_show" placeholder="Readonly" readonly=""
                                type="text" name="jenis_transaksi_show">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->

    <!-- show edit form -->
    <div class="modal modal-danger fade bs-modal-lg" id="showModalDetailEdit" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Edit Akun </h5>
                </div>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Kategori</label>
                            <div class="col-md-9">
                                <select name="id_kategori" id="id_kategori" class="form-control">
                                    <option disabled="disabled"> Pilih Akun Kategori </option>
                                    @foreach ($kategori_akun as $key => $val)
                                        <option value="{{ $val['id'] }}"> {{ $val['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Sub Kategori</label>
                            <div class="col-md-9">
                                <select name="id_subkategori" id="id_subkategori" class="form-control">
                                    <option disabled="disabled"> Pilih Akun Sub Kategori </option>
                                    @foreach ($subkategori_akun as $key => $val)
                                        <option value="{{ $val['id'] }}"> {{ $val['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input class="form-control" id="id_akun" placeholder="id" type="hidden" name="id_akun"
                                required="">
                            <label class="col-md-3 col-form-label" for="l11">Kode</label>
                            <div class="col-md-9">
                                <input class="form-control" id="kode" placeholder="Kode" type="text" name="kode"
                                    required="" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Nama</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama" placeholder="Nama" type="text" name="nama"
                                    required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Jenis Transaksi</label>
                            <div class="col-md-9">
                                <select name="tipe_transaksi" id="tipe_transaksi" class="form-control">
                                    <option disabled="disabled"> Pilih Jenis Transaksi </option>
                                    <option value="harta">Harta</option>
                                    <option value="piutang">Piutang </option>
                                    <option value="aktiva">Aktiva </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->

    <!-- show modal new -->
    <div class="modal modal-danger fade bs-modal-lg" id="showModalDetailNew" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Tambah Akun </h5>
                </div>
                <form id="DocFormxx" name="DocFormxx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Kategori</label>
                            <div class="col-md-9">
                                <select name="id_kategori_post" id="id_kategori_post" class="form-control">
                                    <option disabled="disabled" selected="selected"> Pilih Akun Kategori </option>
                                    @foreach ($kategori_akun as $key => $val)
                                        <option value="{{ $val['id'] }}"> {{ $val['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Sub Kategori</label>
                            <div class="col-md-9">
                                <select name="id_subkategori_post" id="id_subkategori_post" class="form-control">
                                    <option disabled="disabled" selected="selected"> Pilih Akun Sub Kategori </option>
                                    @foreach ($subkategori_akun as $key => $val)
                                        <option value="{{ $val['id'] }}"> {{ $val['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Kode</label>
                            <div class="col-md-9">
                                <input class="form-control" id="kode_post" placeholder="Kode" type="text" name="kode_post"
                                    required="" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Nama</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama_post" placeholder="Nama" type="text" name="nama_post"
                                    required="">
                            </div>
                        </div>
                        <div class="form-group row" style="display:none;" id="jenistr">
                            <label class="col-md-3 col-form-label" for="l11">Jenis</label>
                            <div class="col-md-9">
                                <select name="tipe_transaksi_post" id="tipe_transaksi_post" class="form-control">
                                    <option disabled="disabled" selected="selected"> Pilih Jenis Transaksi </option>
                                    <option value="harta">Harta</option>
                                    <option value="piutang">Piutang </option>
                                    <option value="aktiva">Aktiva </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info tambah">
                            <i class="material-icons list-icon">save</i>
                            Tambah
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_docx" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Jenis Transaksi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Jenis Transaksi</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNew'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_akun_spp') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'akun_kategori',
                            name: 'akun_kategori'
                        },
                        {
                            data: 'sub_akun_kategori',
                            name: 'sub_akun_kategori'
                        },
                        {
                            data: 'jenis_transaksi',
                            name: 'jenis_transaksi',
                            visible: false
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_docx').dataTable(config_table);

                //reset modal
                $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                $('body').on('change', 'select[name="id_kategori_post"]', function() {
                    var id_kategori = $(this).val();
                    var urlx = '{{ route('subkategori_byAK', ':id') }}';
                    urlx = urlx.replace(':id', id_kategori);
                    var newOptionsSelect = '';
                    $.ajax({
                        type: 'GET',
                        url: urlx,
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var JSONObject = JSON.parse(JSON.stringify(result[
                                        'data']));

                                    $("select#id_subkategori_post").empty();

                                    newOptionsSelect = newOptionsSelect +
                                        `<option disabled="disabled" selected="selected"> Pilih Akun Sub Kategori </option>`;

                                    for (var key in JSONObject) {
                                        if (JSONObject.hasOwnProperty(key)) {
                                            newOptionsSelect = newOptionsSelect +
                                                '<option value="' + JSONObject[key]["id"] +
                                                '">' + JSONObject[key]["nama"] +
                                                '</option>';

                                            if (JSONObject[key]["akun_kategori"] ==
                                                'Aktiva') {
                                                $('#jenistr').show();
                                            } else {
                                                $('#jenistr').hide();
                                            }

                                        }
                                    }

                                    $("select#id_subkategori_post").append(
                                        newOptionsSelect);
                                }
                            }

                        }
                    });
                });

                //select sub kategori
                $('body').on('change', 'select[name="id_subkategori_post"]', function() {
                    var id_subkategori = $(this).val();
                    var urlx = '{{ route('show_akuntansi_kodeakun', ':id') }}';
                    urlx = urlx.replace(':id', id_subkategori);
                    $.ajax({
                        type: 'GET',
                        url: urlx,
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('input[name="kode_post"]').val(result['data']);

                                }
                            }
                        }
                    });

                });

                //select sub kategori
                $('body').on('change', 'select[name="id_subkategori"]', function() {
                    var id_subkategori = $(this).val();
                    var urlx = '{{ route('show_akuntansi_kodeakun', ':id') }}';
                    urlx = urlx.replace(':id', id_subkategori);
                    $.ajax({
                        type: 'GET',
                        url: urlx,
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('input[name="kode"]').val(result['data']);
                                }
                            }
                        }
                    });

                });

                //new akun kategori
                $('body').on('click', '#createNew', function() {
                    $('#showModalDetailNew').modal('show');
                });

                $('body').on('submit', '#DocFormxx', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_akun_akuntansispp') }}';

                    var formData = new FormData(this);

                    const loader = $('button.tambah');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showModalDetailNew').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i>');

                                    window.notif(result['info'], result['message']);

                                    $('input[name="nama_post"]').val('');
                                    $('input[name="kode_post"]').val('');

                                    $('select').each(function() {
                                        $(this).val($(this).find("option[selected]")
                                            .val());
                                    });

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i>');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //show detail
                $('body').on('click', '.show.akuntansi.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_akuntansi_akunspp', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#showModalDetail').modal('show');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="kode_show"]').val(rows.kode);
                                    $('input[name="name_show"]').val(rows.nama);
                                    $('input[name="kategori_show"]').val(rows
                                        .akun_kategori);
                                    $('input[name="subkategori_show"]').val(rows
                                        .sub_akun_kategori);
                                    $('input[name="jenis_transaksi_show"]').val(rows
                                        .jenis_transaksi);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    window.notif('error', result['message']);
                                }
                            }

                        }
                    });

                });

                //edit detail
                $('body').on('click', '.edit.akuntansi.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_akuntansi_akunspp', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    $('#showModalDetailEdit').modal('show');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_akun"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="tipe_transaksi"]').val(rows.jenis_transaksi).trigger('change');

                                    $("select[name='id_kategori'] > option[value=" + rows
                                        .id_akun_kategori + "]").prop("selected", true);

                                    $("select[name='id_subkategori'] > option[value=" + rows
                                        .id_sub_akun_kategori + "]").prop("selected",
                                        true);

                                    /*$('select#id_subkategori option:not(:selected)').attr(
                                        'disabled', true);

                                    $('select#id_kategori option:not(:selected)').attr(
                                        'disabled', true);*/

                                    // $("select[name='tipe_transaksi'] > option[value=" + rows
                                    //     .jenis_transaksi + "]").prop("selected",
                                    //     true);

                                    /*$('select#tipe_transaksi option:not(:selected)').attr(
                                        'disabled', true);*/


                                    $('input[name="kode"]').val(rows.kode);

                                } else {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    window.notif('error', result['message']);
                                }
                            }

                        }
                    });

                });

                //update
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_akun"]').val();
                    var urlx = '{{ route('update_akuntansi_akunspp', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showModalDetailEdit').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.edit.akuntansi.akun').html(
                                        '<i class="fa fa-edit"></i>');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.edit.akuntansi.akun').html(
                                        '<i class="fa fa-edit"></i>');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
