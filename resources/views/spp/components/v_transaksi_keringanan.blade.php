@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Potongan Pemasukan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Keringanan SPP</li>
                <li class="breadcrumb-item "><a href="{{ route('konfirmasi-transaksi') }}">Konfirmasi</a></li>
                <li class="breadcrumb-item "><a href="{{ route('pemasukan-tagihan') }}">Transaksi</a></li>
                <li class="breadcrumb-item "><a href="{{ route('history-transaksi-pemasukan') }}">Histori</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- form pencarian  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="opendatasiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Data Siswa </h5>
                </div>
                <div class="modal-body">
                    <table id="table_docx" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Photo</th>
                                <th>NISN</th>
                                <th>Nama Siswa</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Append Create Datatables-->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Photo</th>
                                <th>NISN</th>
                                <th>Nama Siswa</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="Formsiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Form Keringanan SPP </h5>
                </div>
                <p class="text-red float-right mt-2">Tanda (*) Form harus diisi!</p>
                <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" class="form-control is-valid" name="id_siswa" id="id_siswa">
                        <div class="row mr-b-50">
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>NIS</label>
                                <input type="text" class="form-control is-valid" id="nisn" name="nisn" placeholder="Nisn"
                                    readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Nama</label>
                                <input type="text" class="form-control is-valid" id="nama" name="nama" placeholder="Nama"
                                    readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Jurusan</label>
                                <input type="text" class="form-control is-valid" id="jurusan" name="jurusan"
                                    placeholder="Jurusan" readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Rombongan Belajar </label>
                                <input type="text" class="form-control is-valid" id="rombel" name="rombel"
                                    placeholder="Rombel" readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Tahun Ajaran <span class="text-red">*</span></label>
                                <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required>
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                    @foreach ($tahun_ajaran as $key => $value)
                                        <option value="{{ $value['tahun_ajaran'] }}">
                                            {{ $value['tahun_ajaran'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Tagihan <span class="text-red">*</span></label>
                                <select class="form-control" id="id_tagihan" name="id_tagihan" required>
                                    <option disabled="disabled" selected="true" value="">Pilih Tagihan</option>
                                    @foreach ($tagihan as $key => $value)
                                        <option value="{{ $value['id'] }}">
                                            {{ $value['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Nominal <span class="text-red">*</span></label>
                                <input type="text" class="form-control is-valid" id="nominal" name="nominal"
                                    placeholder="Nominal" required="" onkeyup="currencyFormat(this)">
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Keterangan <span class="text-red">*</span> </label>
                                <textarea class="form-control" id="keterangan" name="keterangan" rows="3"
                                    required></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left data-siswa"
                            data-dismiss="modal">Kembali ke Pencarian Data Siswa </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="FormsiswaEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Form Keringanan SPP </h5>
                </div>
                <p class="text-red float-right mt-2">Tanda (*) Form harus diisi!</p>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" class="form-control is-valid" name="idx" id="idx">
                        <input type="hidden" class="form-control is-valid" name="id_siswa" id="id_siswa">
                        <div class="row mr-b-50">
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>NIS</label>
                                <input type="text" class="form-control is-valid" id="nisn" name="nisn" placeholder="Nisn"
                                    readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Nama</label>
                                <input type="text" class="form-control is-valid" id="nama" name="nama" placeholder="Nama"
                                    readonly="">
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Rombongan Belajar </label>
                                <input type="text" class="form-control is-valid" id="rombel" name="rombel"
                                    placeholder="Rombel" readonly="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Tahun Ajaran <span class="text-red">*</span></label>
                                <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required>
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                    @foreach ($tahun_ajaran as $key => $value)
                                        <option value="{{ $value['tahun_ajaran'] }}">
                                            {{ $value['tahun_ajaran'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Tagihan <span class="text-red">*</span></label>
                                <select class="form-control" id="id_tagihan" name="id_tagihan" required>
                                    <option disabled="disabled" selected="true" value="">Pilih Tagihan</option>
                                    @foreach ($tagihan as $key => $value)
                                        <option value="{{ $value['id'] }}">
                                            {{ $value['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Nominal <span class="text-red">*</span></label>
                                <input type="text" class="form-control is-valid" id="nominal" name="nominal"
                                    placeholder="Nominal" required="" onkeyup="currencyFormat(this)">
                            </div>

                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Keterangan <span class="text-red">*</span> </label>
                                <textarea class="form-control" id="keterangan" name="keterangan" rows="3"
                                    required></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">close </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading text-right">
                        <div class="headersx"></div>
                    </div>
                    <div class="widget-body clearfix">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NISN</th>
                                    <th>Kelas</th>
                                    <th>Rombel</th>
                                    <th>Nominal</th>
                                    <th>Keterangan </th>
                                    <th>Tahun Ajaran </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NISN</th>
                                    <th>Kelas</th>
                                    <th>Rombel</th>
                                    <th>Nominal</th>
                                    <th>Keterangan </th>
                                    <th>Tahun Ajaran </th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table, table_sett;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNew'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('dt-keringanan-spp') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'kelas',
                            name: 'kelas'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan'
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ]
                };

                table_setting = $('#table_doc').dataTable(config_table);

                $('body').on('click', '#createNew', function() {
                    $('#opendatasiswa').modal('show');
                    table_sett = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('ajax_data_siswa') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'nisn',
                                name: 'nisn'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'rombel',
                                name: 'rombel'
                            },
                            {
                                data: 'jurusan',
                                name: 'jurusan'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    $('#table_docx').dataTable(table_sett);
                });

                $('body').on('click', '.data-siswa', function() {
                    $('#createNew').click();
                });

                $('body').on('click', '.choose', function() {
                    var id = $(this).data('id');
                    var nama = $(this).data('nama_siswa');
                    var nisn = $(this).data('nisn');
                    var jurusan = $(this).data('jurusan');
                    var rombel = $(this).data('rombel');
                    var kelas = $(this).data('nama_kelas');
                    $('#opendatasiswa').modal('hide');
                    $('#Formsiswa').modal('show');
                    $('input[name="id_siswa"]').val(id);
                    $('input[name="nama"]').val(nama);
                    $('input[name="nisn"]').val(nisn);
                    $('input[name="jurusan"]').val(jurusan);
                    $('input[name="rombel"]').val(rombel);
                });

                //submit
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();

                    var urlx = '{{ route('store_keringanan') }}';

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#Formsiswa').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //show data
                $('body').on('click', '.show.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_diskon_spp', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#printStruckModal').modal('show');
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    printdata = printdata + `
                                       <div class="col-md-12">
                                            <table class="table-stripe table-hover" width="100%">
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Nominal</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Keterangan</td>
                                                        <td>:</td>
                                                        <td>` + rows.keterangan + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                         </div>`;

                                    $('.print-data').html(printdata);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                //show data
                $('body').on('click', '.edit.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit_diskon_spp', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#FormsiswaEdit').modal('show');
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="idx"]').val(rows.id);
                                    $('input[name="id_siswa"]').val(rows.id_kelas_siswa);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="nisn"]').val(rows.nisn);
                                    $('input[name="rombel"]').val(rows.rombel);
                                    $('input[name="nominal"]').val(rows.nominal.format());
                                    $('textarea[name="keterangan"]').val(rows.keterangan);
                                    $('select[name="tahun_ajaran"] option[value="'+rows.tahun_ajaran+'"]').prop('selected', true);
                                    $('select[name="id_tagihan"] option[value="'+rows.id_tagihan+'"]').prop('selected', true);

                                } else {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                }
                            }

                        }
                    });
                });


                 //keyup nominal manual calc
                 $("body").on('keyup', 'input[name="nominal"]', function() {
                    var xv = parseInt($(this).val().split(',').join(''));
                    // calc total
                    if(isNaN(xv) || xv == 0){
                        window.notif('error', 'Nominal Harus di isi ');
                        $('button.update').attr('disabled','disabled');
                    }else{
                        $('button.update').removeAttr('disabled');
                    }
                });

                 //reset modal
                 $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //submit
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="idx"]').val();
                    var urlx = '{{ route('update_keringanan',':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#FormsiswaEdit').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });
                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

            });
        })(jQuery, window);
    </script>
@endsection
