<div class="widget-body clearfix">
    <div class="table-responsive">
        @php
            $total_pendapatan = 0;
            $total_pengeluaran = 0;
            $total_laba_bersih = 0;
        @endphp
        <div class="ml-5 mr-5">
            <table class="table table-hover table-bordered" id="tblexportData">
                <thead>
                    @if (!empty(session('sekolah')))
                        <tr>
                            <td colspan="3">
                                <b>{{ session('sekolah') }}</b>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="3"><b>LAPORAN LABA / RUGI </b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Periode Bulan {{ $bulan }} Tahun {{ $tahun }} </b></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($report as $key => $laporan)
                        @if ($key == 'pendapatan')
                            <tr>
                                <td><b>{{ $laporan['nama'] }}</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach ($laporan['sub_akun_kategori'] as $subakun)
                                <tr>
                                    <td><span class="ml-3">{{ $subakun['nama'] }}</span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @foreach ($subakun['akun'] as $akun)
                                    @php
                                        if ($laporan['id'] == 4) {
                                            $total_pendapatan += intval($akun['nominal']);
                                        }
                                    @endphp
                                    @if (intval($akun['nominal']) != 0)
                                        <tr>
                                            <td><span class="ml-5">{{ $akun['nama'] }}</span>
                                            </td>
                                            <td><span
                                                    class="ml-3">{{ number_format($akun['nominal'], 0) }}</span>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                                @if ($laporan['id'] == 4)
                                    @if ($subakun['kode'] == '4.001')
                                        @php
                                            $total_pendapatan_1 = 0;
                                            $total_pendapatan_1 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left">Sub Total
                                                        {{ $subakun['nama'] }} </span></b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-success">{{ number_format($total_pendapatan_1, 0) }}</span></b>
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($subakun['kode'] == '4.002')
                                        @php
                                            $total_pendapatan_2 = 0;
                                            $total_pendapatan_2 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left">Sub Total
                                                        {{ $subakun['nama'] }} </span></b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-success">{{ number_format($total_pendapatan_2, 0) }}</span></b>
                                            </td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach

                        @elseif ($key == 'pengeluaran')

                            <tr>
                                <td><b>{{ $laporan['nama'] }}</b></td>
                                <td></td>
                                <td></td>
                            </tr>

                            @foreach ($laporan['sub_akun_kategori'] as $subakun)
                                <tr>
                                    <td><span class="ml-3">{{ $subakun['nama'] }}</span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @foreach ($subakun['akun'] as $akun)
                                    @php
                                        if ($laporan['id'] == 5) {
                                            $total_pengeluaran += intval($akun['nominal']);
                                        }
                                    @endphp
                                    @if (intval($akun['nominal']) != 0)
                                        <tr>
                                            <td><span class="ml-5">{{ $akun['nama'] }}</span>
                                            </td>
                                            <td><span
                                                    class="ml-3">{{ number_format($akun['nominal'], 0) }}</span>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endforeach

                                @if ($laporan['id'] == 5)
                                    @if ($subakun['kode'] == '5.001')
                                        @php
                                            $total_pengeluaran_1 = 0;
                                            $total_pengeluaran_1 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                        Total
                                                        {{ $subakun['nama'] }} </span>
                                                </b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_1, 0) }}</span></b>
                                            </td>
                                        </tr>

                                    @elseif ($subakun['kode'] == '5.002')
                                        @php
                                            $total_pengeluaran_2 = 0;
                                            $total_pengeluaran_2 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                        Total
                                                        {{ $subakun['nama'] }} </span>
                                                </b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_2, 0) }}</span></b>
                                            </td>
                                        </tr>

                                    @elseif ($subakun['kode'] == '5.003')

                                        @php
                                            $total_pengeluaran_3 = 0;
                                            $total_pengeluaran_3 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                        Total
                                                        {{ $subakun['nama'] }} </span>
                                                </b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_3, 0) }}</span></b>
                                            </td>
                                        </tr>

                                    @elseif ($subakun['kode'] == '5.004')

                                        @if (count($subakun['akun']) > 0)
                                            @php
                                                $total_pengeluaran_4 = 0;
                                                $total_pengeluaran_4 += intval($akun['nominal']);
                                            @endphp

                                        @else
                                            @php
                                                $total_pengeluaran_4 = 0;
                                            @endphp
                                        @endif

                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                        Total
                                                        {{ $subakun['nama'] }} </span>
                                                </b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_4, 0) }}</span></b>
                                            </td>
                                        </tr>

                                    @elseif ($subakun['kode'] == '5.005')
                                        @php
                                            $total_pengeluaran_5 = 0;
                                            $total_pengeluaran_5 += intval($akun['nominal']);
                                        @endphp
                                        <tr>
                                            <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                        Total
                                                        {{ $subakun['nama'] }} </span>
                                                </b></td>
                                            <td></td>
                                            <td><b><span
                                                        class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_5, 0) }}</span></b>
                                            </td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <tr>
                        <td><b>Laba Bersih </b></td>
                        <td></td>
                        <td>
                            @php
                                $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
                            @endphp
                            @if ($total_laba_bersih > 0)
                                <b class="text-info ml-4">{{ number_format($total_laba_bersih, 0) }}</b>
                            @else
                                <b class="text-red ml-4">{{ number_format($total_laba_bersih, 0) }}</b>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
