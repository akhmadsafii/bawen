@extends('spp.apps')
@section('spp.components')
  <!-- Page Title Area -->
  <div class="row page-title clearfix">
    <div class="page-title-left">
        <h5 class="mr-0 mr-r-5">Pengaturan</h5>
    </div>
    <!-- /.page-title-left -->
    <div class="page-title-right d-inline-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Template</li>
        </ol>
    </div>
    <!-- /.page-title-right -->
</div>
 <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
