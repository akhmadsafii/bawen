@extends('spp.apps')
@section('spp.components')
    <style type="text/css">
        #message p {
            padding: 10px 35px;
            font-size: 18px;
        }

        /* Add a green text color and a checkmark when the requirements are right */
        .valid {
            color: green;
        }

        .valid:before {
            position: relative;
            left: -35px;
            content: "✔";
        }

        /* Add a red text color and an "x" icon when the requirements are wrong */
        .invalid {
            color: red;
        }

        .invalid:before {
            position: relative;
            left: -35px;
            content: "✖";
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Profil > Ubah Password</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item "><a href="{{ route('ubah_profil_admin') }}">Ubah Profil</a>
                </li>
                <li class="breadcrumb-item active">Ubah Password
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus diisi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info text-center alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon md-48">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <form id="PasswordForm" name="PasswordForm" method="POST" class="form-horizontal" enctype="multipart/form-data"
            action="{{ route('update_passwordx') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">

                            <div class="form-group row">
                                <p class="mr-b-0">Username</p>
                                <div class="input-group">
                                    <input id="username" type="text" name="username" readonly="readonly"
                                        class="form-control" value="{{ session('username') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <p class="mr-b-0">Password Lama <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="password_lama" type="password" name="password_lama" class="form-control password"
                                        placeholder="xxxx">
                                    <div class="input-group-addon toggle-password">
                                        <a href="javascript:void(0)"><i
                                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Password Baru <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="password_baru" type="password" name="password_baru" class="form-control password2"
                                        placeholder="xxxx"
                                        title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                        <div class="input-group-addon toggle-password">
                                            <a href="javascript:void(0)"><i
                                                class="material-icons list-icon eye2 text-dark">remove_red_eye</i></a>
                                        </div>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)" id="generate_pass">Generate Password</a>
                                        </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <p class="mr-b-0">Konfirm Password Baru <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="password_confirm_baru" type="password" name="password_confirm_baru"
                                        class="form-control password3" placeholder="xxxx"
                                        title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                </div>
                            </div>

                            <div id="message" style="display: none;">
                                <h4>Password must contain the following:</h4>
                                <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                                <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                                <p id="number" class="invalid">A <b>number</b></p>
                                <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                            </div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $(document).ready(function() {

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                var myInput = document.getElementById("password_baru");
                var letter = document.getElementById("letter");
                var capital = document.getElementById("capital");
                var number = document.getElementById("number");
                var length = document.getElementById("length");

                // When the user clicks on the password field, show the message box
                myInput.onfocus = function() {
                    document.getElementById("message").style.display = "block";
                }

                // When the user clicks outside of the password field, hide the message box
                myInput.onblur = function() {
                    document.getElementById("message").style.display = "none";
                }

                // When the user starts to type something inside the password field
                myInput.onkeyup = function() {
                    // Validate lowercase letters
                    var lowerCaseLetters = /[a-z]/g;
                    if (myInput.value.match(lowerCaseLetters)) {
                        letter.classList.remove("invalid");
                        letter.classList.add("valid");
                    } else {
                        letter.classList.remove("valid");
                        letter.classList.add("invalid");
                    }

                    // Validate capital letters
                    var upperCaseLetters = /[A-Z]/g;
                    if (myInput.value.match(upperCaseLetters)) {
                        capital.classList.remove("invalid");
                        capital.classList.add("valid");
                    } else {
                        capital.classList.remove("valid");
                        capital.classList.add("invalid");
                    }

                    // Validate numbers
                    var numbers = /[0-9]/g;
                    if (myInput.value.match(numbers)) {
                        number.classList.remove("invalid");
                        number.classList.add("valid");
                    } else {
                        number.classList.remove("valid");
                        number.classList.add("invalid");
                    }

                    // Validate length
                    if (myInput.value.length >= 8) {
                        length.classList.remove("invalid");
                        length.classList.add("valid");
                    } else {
                        length.classList.remove("valid");
                        length.classList.add("invalid");
                    }
                }

                const mata = document.querySelector(".eye")
                const mata2 = document.querySelector(".eye2")
                const inputPass = document.querySelector(".password");
                const inputPassBaru = document.querySelector(".password2");
                const inputPassConfirm = document.querySelector(".password3");

                mata.addEventListener("click", () => {
                    mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPass.type === "password") {
                        inputPass.setAttribute("type", "text")

                    } else if (inputPass.type === "text") {
                        inputPass.setAttribute("type", "password")
                        mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                })

                mata2.addEventListener("click", () => {
                    mata2.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPassBaru.type === "password" && inputPassConfirm.type ==="password") {
                        inputPassBaru.setAttribute("type", "text"),
                        inputPassConfirm.setAttribute("type", "text")
                    } else if (inputPassBaru.type === "text" && inputPassConfirm.type ==="text") {
                        inputPassBaru.setAttribute("type", "password"),
                        inputPassConfirm.setAttribute("type", "password")
                        mata2.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                })

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPassBaru.value = retVal;
                    inputPassConfirm.value = retVal;
                }

                $('body').on('click','a#generate_pass',function(){
                    generatePassword();
                });

            });

        })(jQuery, window);
    </script>
@endsection
