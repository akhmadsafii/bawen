@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        @if (session('role') == 'siswa' || session('role') == 'ortu')
                            @if (count($tagihan['tagihan']) > 0)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-outline-info">
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3">Data Siswa </h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody align="left">
                                                            <tr width="100%;">
                                                                <td>Nama</td>
                                                                <td>:</td>
                                                                <td>{{ ucwords($tagihan['nama']) }}</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Kelas</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['kelas'] }}</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Rombel</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['rombel'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jurusan</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['jurusan'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sekolah</td>
                                                                <td>:</td>
                                                                <td>{{ session('sekolah') }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tahun Ajaran </td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['tahun_ajaran'] }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    {{-- <div class="col-md-12 mt-2">
                                                        <a href="{{ route('history-pembayaran') }}"
                                                            class="form-control"><i class="fa fa-eye"></i> Histori
                                                            Transaksi Pembayaran </a>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card card-outline-info">
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3"> Detail Tagihan </h5>
                                            </div>
                                            <div class="card-body">
                                                @php
                                                    $total_tagihan = 0;
                                                @endphp
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr class="table-warning">
                                                                <td>No</td>
                                                                <td>Nama</td>
                                                                <td>Periode</td>
                                                                <td>Bulan</td>
                                                                <td>Nominal</td>
                                                                <td>Status</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($tagihan['tagihan'] as $key => $val)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{ $val['nama'] }}</td>
                                                                    <td>{{ $val['priode'] }}</td>
                                                                    <td>{{ $val['bulan'] }}</td>
                                                                    <td>{{ number_format($val['ditagih'], 0) }}</td>
                                                                    <td>
                                                                        @if (intval($val['ditagih']) == 0)
                                                                            <span class="text-success"> Lunas </span>
                                                                        @else
                                                                            <span class="text-danger">Pending</span>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $total_tagihan += intval($val['ditagih']);
                                                                @endphp
                                                            @endforeach
                                                        <tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td>Total</td>
                                                                <td>:</td>
                                                                <td>{{ number_format($total_tagihan, 0) }}</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert aler-info"> Bulan {{ $bulan }} tidak ada tagihan!.</div>
                            @endif
                        @elseif (session('role') == 'tata-usaha')
                            @php
                                $tahun_data = [];
                                $year_now = \Carbon\Carbon::now()->format('Y');
                                for ($i = 2010; $i <= $year_now; $i++) {
                                    $tahun_data[] = $i;
                                }
                            @endphp

                            <div class="col-md-4 ml-2 form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Filter Tahun </label>
                                <div class="col-md-6">
                                    <select class="form-control" id="tahun" name="tahun" required="">
                                        @if (empty(session('tahun')))
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun
                                            </option>
                                        @endif

                                        @foreach ($tahun_data as $tahunx)
                                            @if ($tahun == $tahunx)
                                                <option value="{{ $tahunx }}" selected="selected">
                                                    {{ $tahunx }}
                                                </option>
                                            @else
                                                <option value="{{ $tahunx }}">{{ $tahunx }}
                                                </option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            @if ($saldo_akun > 0)
                                @foreach ($saldo_akun as $key => $akun)
                                    @if ($key == 0)
                                        @if (($akun['id'] == '10' && intval($akun['saldo_awal']) == 0) || ($akun['id'] == '1' && intval($akun['saldo_awal']) == 0))
                                            <div class="alert alert-error border-error" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <div class="widget-list">
                                                    <div class="col-md-12 widget-holder">
                                                        <div class="widget-body clearfix text-center ">
                                                            <h4><i class="fa fa-warning"></i>
                                                                <strong class="text-red"> Harap Input dulu saldo awal
                                                                    untuk
                                                                    Modal dan kas
                                                                </strong>
                                                            </h4>
                                                            <a href="{{ route('TransaksiSaldoAkunMulti') }}">Input Saldo
                                                                sekarang !</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg">
                                    <div class="widget-body clearfix">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 widget-holder" style="display: none;">
                                                <div class="widget-bg bg-info text-inverse">
                                                    <div class="widget-body clearfix">
                                                        <div class="widget-counter">
                                                            <h6>Total Tagihan <small class="text-inverse">This
                                                                    Month</small></h6>
                                                            <h4 class="h4">Rp.<span
                                                                    class="counter">{{ number_format('525000', 0) }}</span>
                                                            </h4><i class="material-icons list-icon">add_shopping_cart</i>
                                                        </div>
                                                        <!-- /.widget-counter -->
                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                            <div class="col-md-3 col-sm-6 widget-holder">
                                                <div class="widget-bg bg-info text-inverse">
                                                    <div class="widget-body clearfix">
                                                        <div class="widget-counter">
                                                            <h6>Pemberitahuan <small class="text-inverse">Belum dibaca
                                                                </small></h6>
                                                            <h4 class="h4"><span
                                                                    class="counter">{{ $notif_count }}</span>
                                                            </h4><i class="list-icon material-icons">mail_outline</i>
                                                        </div>
                                                        <!-- /.widget-counter -->
                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                            <div class="col-md-3 col-sm-6 widget-holder">
                                                <div class="widget-bg bg-success text-inverse">
                                                    <div class="widget-body clearfix">
                                                        <div class="widget-counter">
                                                            <h6>Total Pemasukan <small class="text-inverse">This
                                                                    Month</small></h6>
                                                            <h4 class="h4">Rp.<span
                                                                    class="counter">{{ number_format($report_x['pemasukan'], 0) }}</span>
                                                            </h4><i class="material-icons list-icon">add_shopping_cart</i>
                                                        </div>
                                                        <!-- /.widget-counter -->
                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                            <div class="col-md-3 col-sm-6 widget-holder">
                                                <div class="widget-bg bg-primary text-inverse">
                                                    <div class="widget-body clearfix">
                                                        <div class="widget-counter">
                                                            <h6>Total Pengeluaran <small class="text-inverse">This
                                                                    Month</small></h6>
                                                            <h4 class="h4">Rp.<span
                                                                    class="counter">{{ number_format($report_x['pengeluaran'], 0) }}</span>
                                                            </h4><i class="material-icons list-icon">add_shopping_cart</i>
                                                        </div>
                                                        <!-- /.widget-counter -->
                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                            <div class="col-md-3 col-sm-6 widget-holder">
                                                <div class="widget-bg bg-warning text-inverse">
                                                    <div class="widget-body clearfix">
                                                        <div class="widget-counter">
                                                            <h6> Laba Rugi <small class="text-inverse">This Month</small>
                                                            </h6>
                                                            <h4 class="h4">Rp.<span
                                                                    class="counter">{{ number_format($report_x['laba_rugi'], 0) }}</span>
                                                            </h4><i class="material-icons list-icon">add_shopping_cart</i>
                                                        </div>
                                                        <!-- /.widget-counter -->
                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <div class="row">
                                <div class="col-md-12 widget-holder">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <h5 class="box-title text-center"> GRAFIK REALISASI PENERIMAAN & PENGGUNAAN
                                                ANGGARAN <br>
                                                Periode
                                                Tahun {{ $tahun }}</h5>
                                            <canvas id="chartJsBar" height="150"></canvas>
                                        </div>
                                        <!-- /.widget-body -->
                                    </div>
                                    <!-- /.widget-bg -->
                                </div>
                                <div class="col-md-6 widget-holder" style="display:none;">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <h5 class="box-title text-center"> GRAFIK REALISASI PENGGUNAAN
                                                ANGGARAN <br>
                                                Periode
                                                Tahun {{ $tahun }}</h5>
                                            <canvas id="chartJsBarp" height="150"></canvas>
                                        </div>
                                        <!-- /.widget-body -->
                                    </div>
                                    <!-- /.widget-bg -->
                                </div>
                            </div>
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg" id="divData">
                                    <div class="widget-body clearfix">
                                        <div class="text-center">
                                            <b>TABEL REALISASI PENERIMAAN ANGGARAN </b>
                                            <br>
                                            @if (!empty(session('sekolah')))
                                                <b>{{ session('sekolah') }}</b>
                                                <br>
                                            @endif
                                            <b>PERIODE TAHUN {{ $tahun }} </b>
                                        </div>
                                        <hr>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="tablex">
                                                <thead class="table-info">
                                                    <tr>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">No</td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">ITEM
                                                        </td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">TARGET</td>
                                                        <td colspan='{{ count($list_bulan) }}' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">BULAN
                                                            PENERIMAAN </td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">TOTAL
                                                            REALISASI </td>
                                                    </tr>
                                                    <tr>
                                                        @foreach ($list_bulan as $bulan)
                                                            <td class="text-center"
                                                                style="text-align: center; vertical-align: middle;">
                                                                {{ $bulan }}
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $total_target = 0;
                                                        $totalx = 0;
                                                        $sumArray_bulan = [];
                                                        $sumArry_bulan_v = [];
                                                        $total_realisasi = 0;
                                                    @endphp
                                                    @if (count($realisasi) > 0)
                                                        @foreach ($realisasi as $key => $val)
                                                            @php
                                                                $total_target += intval($val['target']);
                                                                $total_realisasi += intval($val['realisasi']);
                                                            @endphp
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $val['nama'] }}</td>
                                                                <td>{{ number_format($val['target'], 0) }}</td>
                                                                @foreach ($val['bulan'] as $x => $k)
                                                                    @php
                                                                        if (!isset($sumArray_bulan[$x])) {
                                                                            $sumArray_bulan[$x] = 0;
                                                                        }
                                                                        $sumArray_bulan[$x] += $k['nominal'];
                                                                    @endphp
                                                                    <td>{{ number_format($k['nominal'], 0) }}</td>
                                                                @endforeach

                                                                <td>{{ number_format($val['realisasi'], 0) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            @foreach ($list_bulan as $x => $bulan)
                                                                @php
                                                                    $sumArray_bulan[$x] = 0;
                                                                @endphp
                                                                <td>-</td>
                                                            @endforeach
                                                            <td>-</td>
                                                        </tr>
                                                    @endif

                                                </tbody>
                                                <tfoot>
                                                    <tr class="table-info">
                                                        <td colspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">Total</td>
                                                        <td>{{ number_format($total_target, 0) }}</td>
                                                        @foreach ($sumArray_bulan as $p)
                                                            <td>{{ number_format($p, 0) }}</td>
                                                        @endforeach
                                                        <td>{{ number_format($total_realisasi, 0) }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg" id="divData">
                                    <div class="widget-body clearfix">
                                        <div class="text-center">
                                            <b>TABEL REALISASI PENGGUNAAN ANGGARAN </b>
                                            <br>
                                            @if (!empty(session('sekolah')))
                                                <b>{{ session('sekolah') }}</b>
                                                <br>
                                            @endif
                                            <b>PERIODE TAHUN {{ $tahun }} </b>
                                        </div>
                                        <hr>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="tablex">
                                                <thead class="table-info">
                                                    <tr>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">No</td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">ITEM
                                                        </td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">TARGET</td>
                                                        <td colspan='{{ count($list_bulan) }}' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">BULAN
                                                            PENGGUNAAN </td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">TOTAL
                                                            REALISASI </td>
                                                    </tr>
                                                    <tr>
                                                        @foreach ($list_bulan as $bulan)
                                                            <td class="text-center"
                                                                style="text-align: center; vertical-align: middle;">
                                                                {{ $bulan }}
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @php
                                                        $total_target_pengeluaran = 0;
                                                        $totalx_pengeluaran = 0;
                                                        $sumArray_bulan_pengeluaran = [];
                                                        $sumArry_bulan_v_pengeluaran = [];
                                                        $total_realisasi_pengeluaran = 0;
                                                    @endphp
                                                    @if (count($realisasi_pengeluaran) > 0)
                                                        @foreach ($realisasi_pengeluaran as $key => $val)
                                                            @php
                                                                $total_target_pengeluaran += intval($val['target']);
                                                                $total_realisasi_pengeluaran += intval($val['realisasi']);
                                                            @endphp
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $val['nama'] }}</td>
                                                                <td>{{ number_format($val['target'], 0) }}</td>
                                                                @foreach ($val['bulan'] as $x => $k)
                                                                    @php
                                                                        if (!isset($sumArray_bulan_pengeluaran[$x])) {
                                                                            $sumArray_bulan_pengeluaran[$x] = 0;
                                                                        }
                                                                        $sumArray_bulan_pengeluaran[$x] += $k['nominal'];
                                                                    @endphp
                                                                    <td>{{ number_format($k['nominal'], 0) }}</td>
                                                                @endforeach

                                                                <td>{{ number_format($val['realisasi'], 0) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            @foreach ($list_bulan as $x => $bulan)
                                                                @php
                                                                    $sumArray_bulan_pengeluaran[$x] = 0;
                                                                @endphp
                                                                <td>-</td>
                                                            @endforeach
                                                            <td>-</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr class="table-info">
                                                        <td colspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">Total</td>
                                                        <td>{{ number_format($total_target_pengeluaran, 0) }}</td>
                                                        @foreach ($sumArray_bulan_pengeluaran as $p)
                                                            <td>{{ number_format($p, 0) }}</td>
                                                        @endforeach
                                                        <td>{{ number_format($total_realisasi_pengeluaran, 0) }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 widget-holder">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <h5 class="box-title text-center"> Arus Kas
                                                <br>
                                                Periode
                                                Tahun {{ $tahun }}
                                            </h5>
                                            <div class="loader_arus_kas text-center"></div>
                                            <canvas id="chartJsBar2" height="150"></canvas>
                                        </div>
                                        <!-- /.widget-body -->
                                    </div>
                                    <!-- /.widget-bg -->
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    @if (session('role') == 'tata-usaha')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <script>
            (function($, global) {
                "use-strict"
                $(document).ready(function() {

                    $('body').on('change', 'select[name="tahun"]', function() {
                        var tahun = $(this).val();
                        if (tahun != '') {
                            var url = '{{ route('sumbangan-spp-tahun', ':id') }}';
                            url = url.replace(':id', tahun);
                            window.location.href = url;
                        }
                    });

                    var ctx = document.getElementById("chartJsBar");
                    if (ctx === null) return;

                    var ctx2 = document.getElementById("chartJsBar").getContext("2d");
                    var data2 = {
                        labels: @json($list_bulan),
                            datasets: [{
                                label: "Penerimaan ",
                                backgroundColor: "#5867c3",
                                strokeColor: "#5867c3",
                                data: @json($data_chart)
                            },
                            {
                                label: "Penggunaan  ",
                                backgroundColor: "#fc0b03",
                                strokeColor: "#fc0b03",
                                data: @json($data_chart_pengeluaran)
                            },
                        ]
                    };

                    //realisasi grafik
                    var chartJsBar = new Chart(ctx2, {
                        type: "bar",
                        data: data2,
                        options: {
                            legend: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                titleFontColor: "#000",
                                titleMarginBottom: 10,
                                backgroundColor: "rgba(255,255,255,.9)",
                                bodyFontColor: "#000",
                                borderColor: "#e9e9e9",
                                bodySpacing: 10,
                                borderWidth: 3,
                                xPadding: 10,
                                yPadding: 10,
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        return "Rp. " + Number(tooltipItem.yLabel).toFixed(0).replace(
                                            /./g,
                                            function(c, i, a) {
                                                return i > 0 && c !== "." && (a.length - i) % 3 ===
                                                    0 ? "," + c : c;
                                            });
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        callback: function(value, index, values) {
                                            if (parseInt(value) > 999) {
                                                return 'Rp.' + value.toString().replace(
                                                    /\B(?=(\d{3})+(?!\d))/g, ",");
                                            }
                                        }
                                    }
                                }]
                            }
                        },
                        responsive: true
                    });

                    /*var ctxp = document.getElementById("chartJsBarp");
                    if (ctxp === null) return;

                    var ctx2p = document.getElementById("chartJsBarp").getContext("2d");
                    var data2p = {
                        labels: @json($list_bulan),
                        datasets: [{
                            label: "Nominal",
                            backgroundColor: "#fc0b03",
                            strokeColor: "#fc0b03",
                            data: @json($data_chart_pengeluaran)
                        }, ]
                    };



                    //realisasi grafik
                    var chartJsBarp = new Chart(ctx2p, {
                        type: "bar",
                        data: data2p,
                        options: {
                            legend: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                titleFontColor: "#000",
                                titleMarginBottom: 10,
                                backgroundColor: "rgba(255,255,255,.9)",
                                bodyFontColor: "#000",
                                borderColor: "#e9e9e9",
                                bodySpacing: 10,
                                borderWidth: 3,
                                xPadding: 10,
                                yPadding: 10,
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        return "Rp. " + Number(tooltipItem.yLabel).toFixed(0).replace(
                                            /./g,
                                            function(c, i, a) {
                                                return i > 0 && c !== "." && (a.length - i) % 3 ===
                                                    0 ? "," + c : c;
                                            });
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        callback: function(value, index, values) {
                                            if (parseInt(value) > 999) {
                                                return 'Rp.' + value.toString().replace(
                                                    /\B(?=(\d{3})+(?!\d))/g, ",");
                                            }
                                        }
                                    }
                                }]
                            }
                        },
                        responsive: true
                    });
                    */


                    //
                    window.chartAruskas = function getadta() {
                        var url = '{{ route('arus_kas_chart') }}';
                        const loader = $('.loader_arus_kas');
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                                $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                            },
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {

                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    window.appendChartAruskas(rows, loader);

                                }
                            }
                        });
                    }

                    window.appendChartAruskas = function xdata(rows, loader) {
                        //grafik arus kas
                        var ctx3 = document.getElementById("chartJsBar2");
                        if (ctx3 === null) return;
                        var ctx4 = document.getElementById("chartJsBar2").getContext("2d");
                        var datax = {
                            labels: rows.list_bulan,
                            datasets: [{
                                    label: "Pemasukan",
                                    backgroundColor: "#38d57a",
                                    strokeColor: "#38d57a",
                                    data: rows.pemasukan
                                },
                                {
                                    label: "Pengeluaran",
                                    backgroundColor: "#fc0b03",
                                    strokeColor: "#fc0b03",
                                    data: rows.pengeluaran
                                }
                            ]
                        };

                        //arus kas grafik
                        var chartJsBarx = new Chart(ctx4, {
                            type: "bar",
                            data: datax,
                            options: {
                                legend: {
                                    display: false
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                    titleFontColor: "#000",
                                    titleMarginBottom: 10,
                                    backgroundColor: "rgba(255,255,255,.9)",
                                    bodyFontColor: "#000",
                                    borderColor: "#e9e9e9",
                                    bodySpacing: 10,
                                    borderWidth: 3,
                                    xPadding: 10,
                                    yPadding: 10,
                                },
                                scales: {
                                    xAxes: [{
                                        gridLines: {
                                            display: false
                                        }
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            display: false
                                        },
                                        ticks: {
                                            callback: function(value, index, values) {
                                                if (parseInt(value) > 999) {
                                                    return 'Rp.' + value.toString().replace(
                                                        /\B(?=(\d{3})+(?!\d))/g, ",");
                                                }
                                            }
                                        }
                                    }]
                                },
                                datalabels: {
                                    formatter: function(value, context) {
                                        return context.chart.data.labels[
                                            context.dataIndex
                                        ];
                                    },
                                },
                            },
                            responsive: true
                        });

                        $(loader).html('');
                    }

                    window.chartAruskas();

                });
            })(jQuery, window);
        </script>
    @endif
@endsection
