@php
header('X-Frame-Options: *');
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
    <script type="text/javascript">
        //disabled pace loading
        window.paceOptions = {
            ajax: false,
            restartOnRequestAfter: false,
        };
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" sizes="20x20" href="{{ session('logo') ?? asset('asset/img/sma.png') }}">
    <title>{{ !empty(Session::get('title')) ? session('title') : 'Demo Smartschool' }}</title>
    <!-- CSS -->
    <link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
        rel="stylesheet" type="text/css">

    @if ($MIDTRANS_PRODUCTION == 0)
        <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
                data-client-key="{{ $MIDTRANS_CLIENT_KEY ?? (env('MIDTRANS_CLIENT_KEY') ?? '') }}"></script>
    @elseif($MIDTRANS_PRODUCTION == 1)
        <script type="text/javascript" src="https://app.midtrans.com/snap/snap.js"
                data-client-key="{{ $MIDTRANS_CLIENT_KEY ?? (env('MIDTRANS_CLIENT_KEY') ?? '') }}"></script>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
</head>

<body class="sidebar-horizontal header-centered">
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            @if (count($tagihan) > 0)
                <main class="main-wrapper clearfix">
                    <div class="row page-title clearfix">
                        <div class="page-title-left">
                            <h5 class=" text-center">Tagihan Pembayaran </h5>
                            <p class="mr-0 text-muted d-none d-md-inline-block"></p>
                        </div>
                        <div class="page-title-right d-inline-flex">
                            <ol class="breadcrumb">

                            </ol>
                        </div>
                    </div>
                    <div class="widget-list ml-3 mr-3">
                        <!-- /.modal -->
                        <div class="modal modal-info fade bs-modal-md-primary" id="metodeBayar" tabindex="-1"
                            role="dialog" aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header text-inverse">
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">×</button>
                                        <h5 class="modal-title" id="myMediumModalLabel">Pilih Metode Pembayaran </h5>
                                    </div>
                                    <form id="formMetode" name="formMetode" class="form-horizontal"
                                        enctype="multipart/form-data" novalidate>
                                        @csrf
                                        <div class="modal-body">
                                            <div class="col-sm-12 mb-3">
                                                <div class="form-group">
                                                    <input type="hidden" name="kode_trans" id="kode_trans"
                                                        value="{{ $tagihan['kode'] }}">
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="gopay">
                                                            <span class="label-text">Gopay</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="shopee_pay">
                                                            <span class="label-text">Shopee Pay</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="dana">
                                                            <span class="label-text">Dana</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="linkaja">
                                                            <span class="label-text">LinkAja</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="ovo">
                                                            <span class="label-text">Ovo</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="virtual_account">
                                                            <span class="label-text">Virtual Account</span>
                                                        </label>
                                                    </div>
                                                    <div class="radiobox radio-info">
                                                        <label>
                                                            <input type="radio" name="metode" value="transfer_bank">
                                                            <span class="label-text">Transfer Bank / Upload Bukti
                                                                Transfer </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-info change_metode">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                                                data-dismiss="modal">Close this</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <div class="row">
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg">
                                    <div class="ecommerce-invoice">
                                        <div class="d-flex">
                                            <div class="col-md-6">

                                                <p>
                                                    {{ $sekolah['nama'] ?? '' }}
                                                    <br>
                                                    {{ $sekolah['alamat'] ?? '' }}
                                                </p>
                                            </div>
                                            @if (!empty($sekolah))
                                                <div class="col-md-6 text-right">
                                                    @if (!empty($sekolah['file']))
                                                        <img src="{{ $sekolah['file'] }}" width="100px"
                                                            height="100px">
                                                    @endif
                                                </div>
                                            @endif
                                        </div>
                                        <!-- /.row -->
                                        <hr>
                                        <div class="d-flex">
                                            <div class="col-md-6 text-muted">
                                                <h6 class="mr-t-0"><strong>Kepada Yth:</strong></h6>
                                                <br>
                                                {{ Str::ucfirst($profil_ortu['nama']) ?? '' }}
                                                <br> {{ $profil_ortu['email'] ?? '' }}
                                                <br> {{ $profil_ortu['telepon'] ?? '' }}
                                                <br>
                                                <h6 class="mr-t-2"><strong> Orang Tua / Wali dari Siswa
                                                        :</strong></h6>
                                                {{ Str::ucfirst($tagihan['nama']) }} - {{ $tagihan['nis'] }} -
                                                {{ $tagihan['rombel'] }} - {{ $tagihan['jurusan'] }}
                                            </div>

                                            <div class="col-md-6 text-right">
                                                <h3>Invoice</h3>
                                                <h6 class="mr-t-0"><strong>Detail:</strong></h6>
                                                <strong>Tanggal:</strong> <span
                                                    class="text-muted">{{ \Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y') }}</span>
                                                <br><strong>No.Invoice:</strong> <span
                                                    class="text-muted">{{ $tagihan['kode'] }}</span>
                                                <br><strong>Total Tagihan:</strong> <span class="text-muted">Rp.
                                                    {{ number_format($tagihan['nominal'], 0) }}</span>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                        <hr class="border-0">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr class="bg-color-scheme-dark text-white">
                                                    <th class="text-center">&#35;</th>
                                                    <th>Item</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Nominal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($tagihan['tagihan'] as $key => $value)
                                                    <tr>
                                                        <td class="text-center">{{ $loop->iteration }}</td>
                                                        <td>{{ $value['nama_tagihan'] }} </td>
                                                        <th class="text-center">1</th>
                                                        <td class="text-center">
                                                            Rp.{{ number_format($value['nominal'], 0) }}</td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-8">
                                                @if (!empty($rekening))
                                                    <p class="text-info">Catatan : Pembayaran via Offline </p>
                                                    <ul class="text-muted small">
                                                        <li>Bank : {{ $rekening['nama_bank'] }} </li>
                                                        <li>Atas Nama : {{ $rekening['atas_nama'] }} </li>
                                                        <li>No.rekening : {{ $rekening['rekening_bank'] }} </li>
                                                    </ul>
                                                @endif
                                            </div>
                                            <div class="col-md-4 invoice-sum text-right">
                                                <ul class="list-unstyled">
                                                    <li><strong>Total: Rp.
                                                            {{ number_format($tagihan['nominal'], 0) }}</strong>
                                                    </li>
                                                </ul>
                                                <button class="btn btn-success" id="metode" data-toggle="modal"
                                                    data-target="#metodeBayar">Lanjut</button>
                                                <a class="btn btn-info cek_pembayaran" style="display: none;">
                                                    Cek Status Pembayaran </a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.ecommerce-invoice -->

                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                        </div>
                        <!-- /.row -->
                    </div>
                </main>
            @else
                <div id="wrapper" class="row wrapper mt-2">
                    <div class="col-10 ml-sm-auto col-sm-6 col-md-4  ml-md-auto login-center mx-auto">
                        <div class="page-title">
                            <h1>Data Tidak ditemukan</h1>
                        </div>
                        <p class="mr-t-10 mr-b-20">Kode Transaksi tidak valid!</p><a href="javascript: history.back();"
                            class="btn btn-info btn-lg btn-rounded mr-b-20 ripple">Go Back</a>
                    </div>
                    <!-- /.login-center -->
                </div>
            @endif
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('body').on('submit', '#formMetode', function(e) {
                e.preventDefault();
                var urlx = '{{ route('change_metode_bayar') }}';
                var formData = new FormData(this);
                const loader = $('.change_metode');
                $.ajax({
                    type: "POST",
                    url: urlx,
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result) {
                        if (typeof result['data'] !== 'underfined' && typeof result[
                                'info'] !== 'underfined') {
                            if (result['info'] == 'success') {
                                $('#metodeBayar').modal('hide');
                                $(loader).html(
                                    '<i class="material-icons list-icon">save</i>');
                                if (result['url'] != '') {
                                    if (result['metode'] == 'transfer_bank') {
                                        window.location.href = result['url'];
                                    }
                                } else {
                                    var rows = JSON.parse(JSON.stringify(result[
                                        'data']));

                                     var link_pembayaran = rows.link_bayar;
                                     if(link_pembayaran != null){
                                        notif('success', result
                                                    .status_message);
                                                window.location.href = link_pembayaran;
                                     }else{
                                        notif('info', result.status_message);
                                     }

                                    /*var tokenwebMt = rows.token_vtweb;
                                    var kode_midtrans = rows.kode_midtrans;
                                    if (tokenwebMt != '') {
                                        $('#metode').hide();
                                        var url =
                                            '{{ route('cek-order', ['id' => ':id', 'role' => ':role']) }}';
                                        url = url.replace(':id', kode_midtrans);
                                        url = url.replace(':role', 'ortu');

                                        snap.pay(tokenwebMt, {
                                            onSuccess: function(result) {
                                                notif('success', result
                                                    .status_message);
                                                //console.log(result);
                                                window.location.href = url;
                                            },
                                            onPending: function(result) {
                                                notif('info', result
                                                    .status_message);
                                                window.location.href = url;
                                            },
                                            onError: function(result) {
                                                notif('error', result
                                                    .status_message);
                                            },
                                            onClose: function() {
                                                notif('info',
                                                    'customer closed the popup without finishing the payment'
                                                );
                                            }
                                        });

                                    }*/


                                }

                            } else if (result['info'] == 'error') {
                                notif(result['info'], result['message']);
                            }
                        }
                    },
                    error: function(data) {
                        let log = "";
                        if (typeof data !== 'underfined') {
                            let item = data['responseJSON']['errors'];
                            for (var key in item) {
                                //console.log(item[key])
                                log = log + item[key];
                            }
                            notif('error', log);
                        }
                    }
                });
            });

            function notif(tipe, value) {
                $.toast({
                    icon: tipe,
                    text: value,
                    hideAfter: 5000,
                    showConfirmButton: true,
                    position: 'top-right',
                });
                return true;
            }
        });
    </script>
</body>

</html>
