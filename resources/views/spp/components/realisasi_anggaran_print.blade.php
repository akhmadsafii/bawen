<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
       PrintWindow();
    </script>
</head>
<body>
    <center>
        <b>LAPORAN REALISASI PENERIMAAN  PEMBAYARAN ANGGARAN</b>
        <br>
        @if (!empty(session('sekolah')))
            <b>{{ session('sekolah') }}</b>
            <br>
        @endif
        <b>Periode Tahun {{ $request_tahun }} </b>
        <hr>
    </center>
    <table class="table  table-striped table-hover">
        <table class="table  table-striped table-hover">
            <thead class="table-info">
                <tr>
                    <td rowspan='2' class="text-center"
                        style="text-align: center; vertical-align: middle;">No</td>
                    <td rowspan='2' class="text-center"
                        style="text-align: center; vertical-align: middle;">ITEM</td>
                    <td rowspan='2' class="text-center"
                        style="text-align: center; vertical-align: middle;">TARGET</td>
                    <td colspan='{{ count($list_bulan) }}' class="text-center"
                        style="text-align: center; vertical-align: middle;">BULAN PENERIMAAN</td>
                    <td rowspan='2' class="text-center"
                        style="text-align: center; vertical-align: middle;">TOTAL REALISASI</td>
                </tr>
                <tr>
                    @foreach ($list_bulan as $bulan)
                        <td class="text-center" style="text-align: center; vertical-align: middle;">
                            {{ $bulan }}
                        </td>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                    $total_target = 0;
                    $sumArray_bulan = [];
                    $total_realisasi = 0;
                @endphp
                @if (count($realisasi) > 0)

                    @foreach ($realisasi as $key => $val)
                        @php
                            $total_target += intval($val['target']);
                            $total_realisasi += intval($val['realisasi']);
                        @endphp
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $val['nama'] }}</td>
                            <td>{{ number_format($val['target'], 0) }}</td>
                            @foreach ($val['bulan'] as $x => $k)
                                @php
                                    if (!isset($sumArray_bulan[$x])) {
                                        $sumArray_bulan[$x] = 0;
                                    }
                                    $sumArray_bulan[$x] += $k['nominal'];
                                @endphp
                                <td>{{ number_format($k['nominal'], 0) }}</td>
                            @endforeach
                            <td>{{ number_format($val['realisasi']) }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        @foreach ($list_bulan as $x => $bulan)
                            @php
                                  $sumArray_bulan[$x] = 0;
                            @endphp
                          <td>-</td>
                        @endforeach
                        <td>-</td>
                    </tr>
                @endif
            </tbody>
            <tfoot>
                <tr class="table-info">
                    <td colspan='2' class="text-center"
                        style="text-align: center; vertical-align: middle;">Total</td>
                    <td>{{ number_format($total_target, 0) }}</td>
                    @foreach ($sumArray_bulan as $p)
                        <td>{{ number_format($p, 0) }}</td>
                    @endforeach
                    <td>{{ number_format($total_realisasi, 0) }}</td>
                </tr>
            </tfoot>
    </table>
</body>
</html>
