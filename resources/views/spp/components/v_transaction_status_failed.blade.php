<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('spp.includes.head')
</head>
<body class="body-bg-full profile-page">
    <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center login-center-mini mx-auto">
            <p class="text-center text-muted"> <i class="material-icons list-icon md-48">sms_failed</i> <br> Gagal Pembayaran , silahkan coba <a href="{{ route('transfer_by_code',['id'=>$kode_transaksi]) }}">Metode Lain </a> </p><a href="{{ route('sumbangan-spp') }}" class="btn btn-block btn-primary ripple mr-tb-30">Back to Home</a>
        </div>
        <!-- /.login-center -->
    </div>
    @include('spp.includes.foot')
</body>
</html>
