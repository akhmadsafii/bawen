<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        .text-center {
            text-align: center !important;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center><strong><u><b>Pembelian Aset Aktiva Tetap</b></u></strong></center>
    <br>
    <br>
    <table width="100%">
        <tbody>
            <tr>
                <td>Kode Transaksi </td>
                <td>:</td>
                <td>{{ $detail['kode_transaksi'] }}</td>
            </tr>
            <tr>
                <td>Tanggal Transaksi </td>
                <td>:</td>
                <td>{{ \Carbon\Carbon::parse($detail['tgl_transaksi'])->isoFormat('dddd, D MMMM Y ') }}</td>
                <td>Tanggal Diterima </td>
                <td>:</td>
                <td>{{ \Carbon\Carbon::parse($detail['tgl_diterima'])->isoFormat('dddd, D MMMM Y ') }}</td>
            </tr>
            <tr>
                <td>Kode Akun </td>
                <td>:</td>
                <td>{{ $detail['kode_akun'] }}</td>
            </tr>
            <tr>
                <td>Total Transaksi </td>
                <td>:</td>
                <td>{{ number_format($detail['nominal']) }}</td>
            </tr>
            <tr>
                <td>Status </td>
                <td>:</td>
                <td>
                    @switch($detail['status'])
                        @case('1')
                            Beli
                        @break

                        @case('2')
                            Jual
                        @break

                        @default
                    @endswitch
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <strong>Detail Item </strong>
    <table width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Item</th>
                <th>Harga Beli</th>
                <th>Qty</th>
                <th>Subtotal</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody style="text-align:center">
            <tr>
                <td>1</td>
                <td>{{ $detail['nama_asset'] }}</td>
                <td>{{ number_format($detail['satuan'],0) }}</td>
                <td>{{ $detail['jumlah'] }}</td>
                <td>{{ number_format($detail['nominal'], 0) }}</td>
                <td>{{ $detail['keterangan'] ?? '-' }}</td>
            </tr>
        </tbody>
    </table>

    <br>
    <br>
    <table width="100%;">
        <tbody>
            <tr>
                <td align="right">
                    <strong>Penerima,</strong>
                    <br>
                    <br>
                    <strong><b>Bendahara / Tata Usaha </b></strong>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
