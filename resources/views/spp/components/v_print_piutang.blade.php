<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin: 0;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto;
            width: 100%;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center>
        <h1><u><b>Nota Piutang </b></u></h1>
    </center>
    <table width="100%" border="1">
        <tbody>
            <tr>
                <td>Debitur</td>
                <td>:</td>
                <td>{{ Str::ucfirst($data_piutang['debitur']) }}</td>
                <td>Kode Transaksi </td>
                <td>:</td>
                <td>{{ $data_piutang['kode_transaksi'] }}</td>
            </tr>
            <tr>
                <td>No.Kontak</td>
                <td>:</td>
                <td>{{ $data_piutang['telepon'] }}</td>
                <td>Jatuh Tempo</td>
                <td>:</td>
                <td>{{ \Carbon\Carbon::parse($data_piutang['tgl_jatuh_tempo'])->isoFormat('dddd, D MMMM Y') }}</td>
            </tr>
            <tr>
                <td>Total Piutang </td>
                <td>:</td>
                <td>{{ number_format($data_piutang['nominal'], 0) }}</td>
                <td>Status </td>
                <td>:</td>
                <td>
                    @php
                        $sisa = 0;
                        $nominal = intval($data_piutang['nominal']);
                        $angsuran = intval($data_piutang['total_angsuran']);
                        $sisa = intval($nominal) - intval($angsuran);
                    @endphp
                    @if ($sisa > 0)
                        Belum Lunas
                    @else
                        Lunas
                    @endif
                </td>
            </tr>
            <tr>
                <td>Jenis Angsuran </td>
                <td>:</td>
                <td>{{ Str::ucfirst($data_piutang['jenis_angsuran']) }}</td>
                @if ($sisa > 0)
                    <td>Sisa Angsuran</td>
                    <td>:</td>
                    <td>{{ number_format($sisa, 0) }}</td>
                @endif
            </tr>

            @if (intval($data_piutang['total_angsuran']) > 0)
                <tr>
                    <td>Total Angsuran </td>
                    <td>:</td>
                    <td>{{ number_format($data_piutang['total_angsuran'], 0) }}</td>
                </tr>
            @endif
            <tr>
                <td>Mata Uang </td>
                <td>:</td>
                <td>Rupiah (IDR) </td>
            </tr>
        </tbody>
    </table>
    <br>
    <strong>Rincian Item </strong>
    <br>
    <br>
    <table width="100%" border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Item</th>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody style="text-align: center;">
            @foreach ($data_detail_item as $key => $val)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['nama'] }}</td>
                    <td>{{ \Carbon\Carbon::parse($val['tgl_bayar'])->isoFormat('dddd, D MMMM Y') }}</td>
                    <td>{{ number_format($val['nominal'], 0) }} </td>
                    <td>{{ $val['keterangan'] ?? '-' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <br>
    <strong>Rincian Pembayaran Angsuran Cicilan </strong>
    <br>
    <br>
    <table width="100%" border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody style="text-align: center;">
            @if (count($cicilan_pembayaran) > 0)
                @foreach ($cicilan_pembayaran as $cicilan)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ \Carbon\Carbon::parse($cicilan['tgl_transaksi'])->isoFormat('dddd, D MMMM Y') }}
                        </td>
                        <td>{{ number_format($cicilan['nominal'], 0) }}</td>
                        <td>{{ $cicilan['keterangan'] ?? '-' }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="3"> Belum ada catatan cicilan pembayaran</td>
                </tr>
            @endif
        </tbody>
    </table>
    <br>
    <br>
    <table width="100%;">
        <tbody>
            <tr>
                <td align="left"></td>
                <td align="center">
                    <strong>Penyetor </strong>
                    <br>
                    <br>
                    <br>
                    <strong><b>{{ ucwords($data_piutang['debitur']) }} </b></strong>
                </td>
                <td align="right">
                    <strong>Penerima,</strong>
                    <br>
                    <br>
                    @if (!empty($data_piutang['operator']))
                        <strong><b>{{ ucwords($data_piutang['operator']) }}</b></strong>
                        <br>
                    @else
                        <br>
                    @endif
                    <strong><b>Bendahara / Tata Usaha </b></strong>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
