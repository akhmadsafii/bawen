@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Saldo Akun</li>
                <li class="breadcrumb-item"><a href="{{ route('index_saldoAkun') }}">Histori Saldo Akun </a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data" method="POST"
                        action="{{ route('store_saldo_akunmulti') }}">
                        @csrf
                        <div class="widget-body clearfix">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Tahun</label>
                                    <select class="form-control" id="tahun" name="tahun" required="">
                                        @if (empty(session('tahun')))
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun
                                            </option>
                                        @endif

                                        @foreach ($tahunlist as $tahunx)
                                            @if ($tahun == $tahunx)
                                                <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                                </option>
                                            @else
                                                <option value="{{ $tahunx }}">{{ $tahunx }}
                                                </option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <div class="table-responsive">
                                        <table id="table_akun" class="table table-striped table-responsive">
                                            <thead>
                                                <tr class="table-info">
                                                    <th>Akun</th>
                                                    <th>Kode</th>
                                                    <th>Saldo Awal</th>
                                                    <th>Saldo Akhir</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($rekening_akun as $key => $akun)
                                                    <input name="id_akun[]" type="hidden" id="id_akun"
                                                        class="form-control" value="{{ $akun['id'] }}">
                                                    <tr parentId="1">
                                                        <td>
                                                            <input name="akun[]" type="text" id="akun"
                                                                class="form-control" readonly="readonly"
                                                                value="{{ $akun['akun'] }}">
                                                        </td>
                                                        <td><input name="kode[]" type="text" id="kode"
                                                                class="form-control" readonly="readonly"
                                                                value="{{ $akun['kode_akun'] }}">
                                                        </td>
                                                        <td><input name="saldo_awal[]" type="text" id="saldo_awal"
                                                                class="form-control" onkeyup="currencyFormat(this)"
                                                                value="{{ number_format($akun['saldo_awal'],0) ?? '0' }}">
                                                        </td>
                                                        <td><input name="saldo_akhir[]" type="text" id="saldo_akhir"
                                                                class="form-control" onkeyup="currencyFormat(this)"
                                                                value="{{ number_format($akun['saldo_akhir'],0) ?? '0' }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i>
                                Batal
                            </button>
                            <button type="submit" class="btn btn-info update pos_transaction">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //on focus
                $('body').on('focus', 'input[name="saldo_awal[]"]', function() {
                    if ($(this).val() == 0) {
                        $(this).val(''); //reset blank
                    }
                });

                //on focus
                $('body').on('keydown', 'input[name="saldo_awal[]"]', function(e) {
                    if (e.keyCode === 13) {
                        if ($(this).val() == '') {
                            window.notif('warning',
                                'Periksa Kembali saldo Awal  !!!'
                            );
                        } else {
                            var clonned = $(this).parents('.clonable:last-child').clone();
                            var parentId = clonned.attr('parentId');
                            $('tr[parentId="' + parentId + '"] input[name="saldo_akhir[]"]').focus();
                        }
                    }
                });

                $('body').on('keydown', 'input[name="saldo_akhir[]"]', function(e) {
                    if (e.keyCode === 13) {
                        if ($(this).val() == '') {
                            window.notif('warning',
                                'Periksa Kembali saldo Awal  !!!'
                            );
                        }
                    }
                });

                //on focus
                $('body').on('focus', 'input[name="saldo_akhir[]"]', function() {
                    if ($(this).val() == 0) {
                        $(this).val(''); //reset blank
                    }
                });

                $("body").bind("keydown", '#DocForm', function(e) {
                    if (e.keyCode === 13) return false;
                });

                $('body').on('click', 'button.update.pos_transaction', function(e) {
                    if (e.keyCode === 13) return false;
                    e.preventDefault();
                    $('#DocForm').submit();
                });

                $('body').on('change','select[name="tahun"]',function(){
                    var tahun = $(this).val();
                    if(tahun != ''){
                        var url = '{{ route('TransaksiSaldoAkunMultiTahun',':id') }}';
                        url = url.replace(':id',tahun);
                        window.location.href = url;
                    }
                });

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }
            });
        })(jQuery, window);
    </script>
@endsection
