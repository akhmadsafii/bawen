@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Kirim Massal</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Notifikasi Tagihan </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right d-inline-flex">
            <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading text-right">
                        <div class="headersx"></div>
                    </div>
                    <div class="widget-body clearfix">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('post_tagihan_massal') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Bulan <span
                                        class="text-red">(*)</span> </label>
                                <div class="col-md-6">
                                    <select class="form-control" id="bulan" name="bulan" required>
                                        <option value="" disabled="disabled" selected="selected"> Pilih Bulan </option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                        <option value="Januari">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l13">Pesan <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-6">
                                    <select class="form-control" id="pesan" name="pesan" required="">
                                        <option disabled selected value=""> Pilih Pesan </option>
                                        @foreach ($pesan as $key => $val)
                                            <option value="{{ $val['id'] }}">{{ Str::ucfirst($val['kategori']) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l13">Tahun Ajaran</label>
                                <div class="col-md-6">
                                    <select class="form-control tahunAjaran_filter" id="tahunAjaran_filter"
                                        name="tahunAjaran_filter">
                                        @if (empty(session('tahun')))
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun
                                            </option>
                                        @endif

                                        <option value="all">Semua Tahun Ajaran
                                        </option>

                                        @foreach ($tahunlist as $tahunx)
                                            @if ($tahun == $tahunx)
                                                <option value="{{ $tahunx }}" selected="selected">
                                                    {{ $tahunx }}
                                                </option>
                                            @else
                                                <option value="{{ $tahunx }}">{{ $tahunx }}
                                                </option>
                                            @endif
                                        @endforeach



                                    </select>
                                </div>
                            </div>
                            <fieldset>
                                 <span class="text-info"> Pilih Siswa </span><span class="text-red">(*)</span>
                                <div class="tables-responsive">
                                    <table id="table_doc" class="table table-striped table-responsive">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Photo</th>
                                                <th>NIS</th>
                                                <th>Nama</th>
                                                <th>Rombel</th>
                                                <th>Jurusan</th>
                                                <th>Tahun Ajaran</th>
                                                <th>
                                                    <div class="th-inner checkbox-checked"><input name="btSelectAll"
                                                            type="checkbox"> Pilih</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Photo</th>
                                                <th>NIS</th>
                                                <th>Nama</th>
                                                <th>Rombel</th>
                                                <th>Jurusan</th>
                                                <th>Tahun Ajaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </fieldset>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info update pos_konfirm">
                                    <i class="material-icons list-icon">send</i>
                                    Kirim
                                </button>
                                <button type="reset" class="btn btn-danger text-left">
                                    Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var urlx = '{{ route('ajax_data_siswatahunMassal', ['tahun' => $tahun]) }}';
                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: urlx,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'photo',
                            name: 'photo'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'jurusan',
                            name: 'jurusan'
                        },
                        {
                            data: 'tahun',
                            name: 'tahun'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);
                $('body').on('change', 'input[name="btSelectAll"]', function() {
                    if (this.checked) {
                        $(".checkSingle").each(function() {
                            this.checked = true;
                        });
                    } else {
                        $(".checkSingle").each(function() {
                            this.checked = false;
                        });
                    }
                });

                //filter by tahun ajaran
                $('body').on('change', 'select.tahunAjaran_filter', function() {
                    var valx = $(this).val();
                    var urlx = '{{ route('ajax_data_siswatahunMassal', ':tahun') }}';
                    urlx = urlx.replace(':tahun', valx);

                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'nis',
                                name: 'nis'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'rombel',
                                name: 'rombel'
                            },
                            {
                                data: 'jurusan',
                                name: 'jurusan'
                            },
                            {
                                data: 'tahun',
                                name: 'tahun'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_setting = $('#table_doc').dataTable(config_table);
                });
            });
        })(jQuery, window);
    </script>
@endsection
