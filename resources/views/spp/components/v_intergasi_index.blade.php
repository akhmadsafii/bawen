@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Akuntansi</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Intergasi Pembayaran </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <div class="row">
            <!-- Tabs Vertical Left -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">

                        <div class="tabs tabs-bordered">
                            <ul class="nav nav-tabs">
                                <li class="nav-item" aria-expanded="false"><a class="nav-link active"
                                        href="#home-tab-v1" data-toggle="tab" aria-expanded="true">Pembayaran</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#profile-tab-v1"
                                        data-toggle="tab" aria-expanded="false">Notifikasi</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs -->
                            <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home-tab-v1" aria-expanded="true">
                                        <div class="box-title"> Setting Midtrans </div>
                                        <hr>
                                        @if (count($payment_gateway) > 0)
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                        <div class="box-title">Panduan integrasi Midtrans.</div>
                                                        <ol>
                                                            <li>Daftarkan Akun dengan Email yang aktif jika belum punya akun
                                                                di
                                                                midtrans, kunjungi alamat website : <a
                                                                    href="https://midtrans.com/id/passport">
                                                                    https://midtrans.com/id/passport</a></li>
                                                            <li>login dengan akun yng sudah didaftarkan di halaman midtrans
                                                            </li>
                                                            <li>Pilih Environtment Production / Sandbox untuk melakukan
                                                                pengujian
                                                            </li>
                                                            <li>Ambil Server key dan Secret key di menu access key lalu
                                                                isikan
                                                                form
                                                                yang sudah di tersedia </li>
                                                            <li>Klik tombol update untuk mengetahui perubahan</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                        @foreach ($payment_gateway as $key => $val)
                                                            <input type="hidden" id="jenis" class="form-control"
                                                                value="payment">
                                                            @if ($val['kode'] === 'MIDTRANS_SERVER_KEY')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">Server
                                                                        Key </label>
                                                                    <div class="col-md-8">
                                                                        <input class="form-control"
                                                                            id="server_key_midtrans"
                                                                            placeholder="Server Key Midtrans" type="text"
                                                                            name="server_key_midtrans" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if ($val['kode'] === 'MIDTRANS_CLIENT_KEY')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">Client Key
                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <input class="form-control"
                                                                            id="client_key_midtrans"
                                                                            placeholder="Client Key Midtrans" type="text"
                                                                            name="client_key_midtrans" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if ($val['kode'] === 'PRODUCTION')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">Mode
                                                                        Release
                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <select name="production" id="production"
                                                                            class="form-control">
                                                                            <option disabled="disabled" selected="selected">
                                                                                Pilih Mode </option>
                                                                            <option value="0"
                                                                                @if ($val['token'] == '0') selected="selected" @endif>
                                                                                Testing
                                                                                / Sandbox </option>
                                                                            <option value="1"
                                                                                @if ($val['token'] == '1') selected="selected" @endif>
                                                                                Production </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <p class="text-red"> Check Table Environment in your database serve </p>
                                        @endif
                                    </div>
                                    <div class="tab-pane" id="profile-tab-v1" aria-expanded="false">
                                        <div class="box-title"> Setting Pusher </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <div class="box-title">Panduan integrasi Pusher.</div>
                                                    <ol>
                                                        <li>Daftarkan Akun dengan Email yang aktif jika belum punya akun di
                                                            pusher, kunjungi alamat website : <a href="https://pusher.com/">
                                                                https://pusher.com/</a></li>
                                                        <li>login dengan akun yng sudah didaftarkan di halaman pusher</li>
                                                        <li>Buat Chanel Plan dan Pilih Chanel Plan yang telah dibuat tadi.
                                                        </li>
                                                        <li>Ambil APP_ID, APP_SECRET,APP_KEY, APP_CLUSTER di menu App keys
                                                            lalu
                                                            isikan
                                                            form
                                                            yang sudah di tersedia </li>
                                                        <li>Klik tombol update untuk mengetahui perubahan</li>
                                                    </ol>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    @if (count($notifikasi) > 0)
                                                        <input type="hidden" id="jenis" class="form-control"
                                                            value="notification">
                                                        @foreach ($notifikasi as $key => $val)
                                                            @if ($val['kode'] === 'PUSHER_APP_ID')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">App
                                                                        ID
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control" id="app_id_pusher"
                                                                            placeholder="APP ID PUSHER " type="text"
                                                                            name="app_id_pusher" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if ($val['kode'] === 'PUSHER_APP_KEY')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">App
                                                                        Key
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control" id="app_key_pusher"
                                                                            placeholder="APP KEY PUSHER" type="text"
                                                                            name="app_key_pusher" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if ($val['kode'] === 'PUSHER_APP_SECRET')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">App
                                                                        Secret
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control" id="app_key_pusher"
                                                                            placeholder="APP SECRET PUSHER" type="text"
                                                                            name="app_secret_pusher" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if ($val['kode'] === 'PUSHER_APP_CLUSTER')
                                                                <div class="form-group row">
                                                                    <label class="col-md-4 col-form-label text-right"
                                                                        for="l11">App
                                                                        Cluster
                                                                    </label>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control" id="app_key_pusher"
                                                                            placeholder="APP CLUSTER PUSHER" type="text"
                                                                            name="app_cluster_pusher" required=""
                                                                            value="{{ $val['token'] ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <p class="text-red"> Check Table Environment in your database
                                                            serve </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions modal-footer">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 btn-list">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="material-icons list-icon">save</i>
                                                    Update
                                                </button>
                                            </div>
                                            <!-- /.col-sm-12 -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {

                //update
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();

                    var urlx = "{{ route('update_storex') }}";

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result['message']);

                                    window.location.href = "{{ route('auth.logout') }}";
                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
