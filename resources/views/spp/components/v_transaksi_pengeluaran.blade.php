@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pengeluaran / Pengunaan </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data"
                            method="POST" action="{{ route('store_pengeluaran') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Tahun Ajaran</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="tahun_ajaran" name="tahun_ajaran"
                                                required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                                </option>
                                                @foreach ($tahun_ajaran as $key => $value)
                                                    @php
                                                        $explode_tahun = explode('/', $value['tahun_ajaran']);
                                                    @endphp
                                                    @if ($explode_tahun[0] == session('tahun'))
                                                        <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                            {{ $value['tahun_ajaran'] }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $value['tahun_ajaran'] }}">
                                                            {{ $value['tahun_ajaran'] }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Tanggal</label>
                                        <div class="input-group input-has-value col-md-8">
                                            <input type="text" class="form-control datepicker" name="tgl_bayar"
                                                data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                                value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label">Jenis Transaksi <span class="text-red">*</span></label>
                                        <div class="col-md-8">
                                        <select class="form-control" id="sumber" name="sumber" required="">
                                            <option disabled="disabled" selected="true" value="">Pilih Jenis Transaksi </option>
                                            @foreach ($sumber as $rekening)
                                                <option value="{{ $rekening['id'] }}">
                                                    {{ $rekening['kode'] }} -
                                                    {{ $rekening['nama'] }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <center>
                                <div class="table-responsive">
                                    <table id="table_pengeluaran" width="100%" class="table table-striped table-responsive">
                                        <thead>
                                            <tr class="table-info">
                                                <th>Akun <span class="text-red">*</span></th>
                                                <th>Nominal <span class="text-red">*</span></th>
                                                <th>Catatan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="clonable form-clone" parentId="1">
                                                <td>
                                                    <select class="form-control" id="akun_pengeluaran"
                                                        name="akun_pengeluaran[]" required="">
                                                        <option disabled="disabled" selected="true" value="">Pilih Akun
                                                            Pengeluaran
                                                        </option>
                                                        @foreach ($akun as $akun )
                                                        <option   value="{{ $akun['id'] }}"> {{ $akun['kode'] }} - {{ $akun['nama'] }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input name="nominal[]" type="text" id="nominal" class="form-control"
                                                        onkeyup="currencyFormat(this)" placeholder="Nominal"></td>
                                                <td>
                                                    <textarea class="form-control" name="catatan[]" id="catatan"
                                                        rows="1"></textarea>
                                                </td>
                                                <td width="10%" class="btn-group text-center" role="group" width="100%">
                                                    <button type="button" name="remove" id="removex"
                                                        class="btn btn-danger remove-clone remove"><i
                                                            class="fa fa-minus"></i></button> &nbsp;
                                                    <button type="button" name="add" id="addx"
                                                        class="btn btn-info add-clone add"><i
                                                            class="fa fa-plus"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <td>
                                                <label>No Referensi</label>
                                                <input class="form-control" id="no_referensi" placeholder="No Referensi"
                                                        type="text" name="no_referensi">
                                            </td>
                                            <td>
                                                <label>Keterangan <span class="text-red">*</span></label>
                                                <textarea class="form-control" name="keterangan" id="keterangan" rows="1"></textarea>
                                            </td>
                                            <td>
                                                <div class="form-group row ml-3">
                                                    <label for="l38"><i class="fa fa-file"></i>
                                                        Berkas</label><br>
                                                    <span class="text-info ml-3" id="tambah_berkas"><input name="image"
                                                            type="file" id="image" class="form-control"
                                                            accept="image/*"></span>
                                                </div>
                                            </td>
                                        </tfoot>
                                    </table>
                                </div>
                                <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i> Batal
                                </button>
                                <button type="submit" class="btn btn-info update pos_transaction">
                                    <i class="material-icons list-icon">save</i>
                                    Simpan
                                </button>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });

                //on enter
                $('body').on('keyup', 'input[name="nominal[]"]', function(event) {
                    if (event.keyCode === 13) {
                        return false;
                    }
                });

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

            });
        })(jQuery, window);
    </script>
@endsection
