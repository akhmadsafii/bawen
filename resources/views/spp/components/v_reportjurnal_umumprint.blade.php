<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            margin: 0;
            padding: 0
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                /* DIN A4 standard, Europe */
                margin: 0;
            }

            html,
            body {
                width: 210mm;
                /* height: 297mm; */
                height: 282mm;
                font-size: 11px;
                background: #FFF;
                overflow: visible;
            }

            body {
                padding-top: 15mm;
            }
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }

        $(document).ready(function() {
            $('#table_doc').each(function() {
                var Column_number_to_Merge = 1;

                // Previous_TD holds the first instance of same td. Initially first TD=null.
                var Previous_TD = null;
                var i = 1;
                $("tbody", this).find('tr').each(function() {
                    // find the correct td of the correct column
                    // we are considering the table column 1, You can apply on any table column
                    var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge +
                        ')');

                    if (Previous_TD == null) {
                        // for first row
                        Previous_TD = Current_td;
                        i = 1;
                    } else if (Current_td.text() == Previous_TD.text()) {
                        // the current td is identical to the previous row td
                        // remove the current td
                        Current_td.remove();
                        // increment the rowspan attribute of the first row td instance
                        Previous_TD.attr('rowspan', i + 1);
                        i = i + 1;
                    } else {
                        // means new value found in current td. So initialize counter variable i
                        Previous_TD = Current_td;
                        i = 1;
                    }
                });
            });

            PrintWindow();
        });


    </script>
</head>

<body>
    <center>
        @if (!empty(session('sekolah')))
            <b>{{ session('sekolah') }}</b>
            <br>
        @endif
        <b>JURNAL UMUM</b>
        <br>
        <b>Periode Bulan {{ $bulan }} Tahun {{ $tahun }} </b>
    </center>
    <br>
    <hr>
    <br>
    <table id="table_doc" width="100%;" border="1">
        <thead class="table-info">
            <tr>
                <td>Tanggal</td>
                <td>Akun</td>
                <td align="left"> Debit</td>
                <td align="right">Kredit</td>
            </tr>
        </thead>
        <tbody>
            @php
                $total_debit = 0;
                $total_kredit = 0;
                $row_count = 0;
            @endphp
            @if (count($report) > 0)
                @foreach ($report as $key => $item)
                    @php
                        $row_count++;
                    @endphp
                    <tr>
                        <td>
                            {{ \Carbon\Carbon::parse($item['tgl_bayar'])->isoFormat('dddd, D MMMM Y ') }}
                        </td>
                        <td>
                            {{ $item['kode_akun'] }} - {{ $item['akun'] }}
                        </td>
                        @if ($item['debet'] != 0)
                            <td align="left">
                                <span>{{ number_format($item['debet']) }}</span>
                            </td>
                        @else
                            <td></td>
                        @endif

                        @if ($item['kredit'] != 0)
                            <td align="right">
                                <span>{{ number_format($item['kredit']) }}</span>
                            </td>
                        @else
                            <td></td>
                        @endif

                        @php
                            $total_debit += intval($item['debet']);
                            $total_kredit += intval($item['kredit']);
                        @endphp

                        @if ($row_count % 2 == 0)
                    <tr>
                        <td colspan="4">
                            <div class="table-info" style="height:10px;"></div>
                        </td>
                    <tr>
                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td rowspan="4" colspan="4" class="text-center text-red">Belum Ada Catatan </td>
            <tr>
                @endif
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" align="right">Jumlah </td>
                <td align="left">Rp.{{ number_format($total_debit, 0) }}</td>
                <td align="right">Rp.{{ number_format($total_kredit, 0) }}</td>
            </tr>
        </tfoot>
    </table>
</body>

</html>
