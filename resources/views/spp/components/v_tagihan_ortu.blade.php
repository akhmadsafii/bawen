@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Tagihan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Tagihan Bulan {{ $bulan }}</li>
                @if (count($tagihan['tagihan']) > 0)
                    <li class="breadcrumb-item "><a href="{{ route('print_tagihan_ortu') }}" target="_blank"> <i
                                class="fa fa-print"> Print </i> </a></li>
                    <li class="breadcrumb-item "><i class="fa fa-money" data-toggle="modal" data-target="#konfirmasi">
                            Konfirmasi </i></li>
                @endif
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.modal -->
    <div class="modal modal-info fade bs-modal-md-primary" id="metodeBayar" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Pilih Metode Pembayaran </h5>
                </div>
                <form id="formMetode" name="formMetode" class="form-horizontal" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="modal-body">
                        <div class="col-sm-12 mb-3">
                            @php
                                $date_now = \Carbon\Carbon::now()->timestamp;
                                $kode_trans = 'TRX-' . $tagihan['nis'] . $date_now;
                            @endphp
                            <div class="form-group">
                                <input type="hidden" name="kode_trans" id="kode_trans"
                                    value="{{ $tagihan['kode'] ?? $kode_trans }}">
                                <div class="radiobox radio-info">
                                    <label>
                                        <input type="radio" name="metode" value="gopay">
                                        <span class="label-text">Gopay</span>
                                    </label>
                                </div>
                                <div class="radiobox radio-info">
                                    <label>
                                        <input type="radio" name="metode" value="shopee_pay">
                                        <span class="label-text">Shopee Pay</span>
                                    </label>
                                </div>
                                <div class="radiobox radio-info">
                                    <label>
                                        <input type="radio" name="metode" value="virtual_account">
                                        <span class="label-text">Virtual Account</span>
                                    </label>
                                </div>
                                <div class="radiobox radio-info">
                                    <label>
                                        <input type="radio" name="metode" value="transfer_bank">
                                        <span class="label-text">Transfer Bank / Upload Bukti
                                            Transfer </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info change_metode">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- konfirmasi -->
    <div class="modal modal-info fade bs-modal-lg-primary" id="konfirmasi" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Konfirmasi Pembayaran</h5>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 mb-3">
                        <table class="table">
                            <thead class="table-info">
                                <tr>
                                    <td>#</td>
                                    <td>Nama Bank </td>
                                    <td>No.Rekening</td>
                                    <td> Atas Nama</td>
                                <tr>
                            </thead>
                            <tbody align="left">
                                @foreach ($rekening as $d)
                                    <tr width="100%;">
                                        <td>#</td>
                                        <td>{{ ucwords($d['nama_bank']) }}</td>
                                        <td>{{ $d['rekening_bank'] }}</td>
                                        <td>{{ ucwords($d['atas_nama']) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <span class="text-danger ml-2 "> Catatan</span>
                    <p class="text-info text-center">
                        Pembayaran dapat di lakukan secara offline setelah mendapatkan notifikasi tagihan dari Administrator
                        sekolah.
                    </p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading text-right">
                        <div class="headersx"></div>
                    </div>
                    <div class="widget-body clearfix">
                        @if (count($tagihan['tagihan']) > 0)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card card-outline-info">
                                        <div class="card-header">
                                            <h5 class="card-title mt-0 mb-3">Data Siswa </h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody align="left">
                                                        <tr width="100%;">
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>{{ ucwords($tagihan['nama']) }}</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>Kelas</td>
                                                            <td>:</td>
                                                            <td>{{ $tagihan['kelas'] }}</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>Rombel</td>
                                                            <td>:</td>
                                                            <td>{{ $tagihan['rombel'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jurusan</td>
                                                            <td>:</td>
                                                            <td>{{ $tagihan['jurusan'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Sekolah</td>
                                                            <td>:</td>
                                                            <td>{{ session('sekolah') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tahun Ajaran </td>
                                                            <td>:</td>
                                                            <td>{{ $tagihan['tahun_ajaran'] }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <button id="metode" class="btn btn-info" data-toggle="modal"
                                                    data-target="#metodeBayar" style="display:none;">Konfirmasi
                                                    Pembayaran</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card card-outline-info">
                                        <div class="card-header">
                                            <h5 class="card-title mt-0 mb-3">Data Ortu </h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody align="left">
                                                        <tr width="100%;">
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>{{ ucwords($ortu['nama']) }}</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td>{{ $ortu['email'] }}</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>Telepon</td>
                                                            <td>:</td>
                                                            <td>{{ $ortu['telepon'] }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <button id="metode" class="btn btn-info" data-toggle="modal"
                                                    data-target="#metodeBayar" style="display:none;">Konfirmasi
                                                    Pembayaran</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2 ">
                                    <div class="card card-outline-info">
                                        <div class="card-header">
                                            <h5 class="card-title mt-0 mb-3"> Detail Tagihan </h5>
                                        </div>
                                        <div class="card-body">
                                            @php
                                                $total_tagihan = 0;
                                            @endphp
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr class="table-warning">
                                                            <td>No</td>
                                                            <td>Nama</td>
                                                            <td>Periode</td>
                                                            <td>Nominal</td>
                                                            <td>Status</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($tagihan['tagihan'] as $key => $val)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $val['nama'] }}</td>
                                                                <td>{{ $val['priode'] }}</td>
                                                                <td>{{ number_format($val['ditagih'], 0) }}</td>
                                                                <td>
                                                                    @if (intval($val['ditagih']) == 0)
                                                                        <span class="text-success"> Lunas </span>
                                                                    @else
                                                                        <span class="text-danger">Pending</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $total_tagihan += intval($val['ditagih']);
                                                            @endphp

                                                        @endforeach
                                                    <tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>Total</td>
                                                            <td>:</td>
                                                            <td>{{ number_format($total_tagihan, 0) }}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="alert aler-info"> Bulan {{ $bulan }} tidak ada tagihan!.</div>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('body').on('submit', '#formMetode', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('change_metode_bayar') }}';
                    var formData = new FormData(this);
                    const loader = $('.change_metode');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#metodeBayar').modal('hide');
                                    $(loader).html(
                                        '<i class="material-icons list-icon">save</i>');
                                    if (result['url'] != '') {
                                        if (result['metode'] == 'transfer_bank') {
                                            window.location.href = result['url'];
                                        }
                                    } else {
                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));
                                        var tokenwebMt = rows.token_vtweb;
                                        var kode_midtrans = rows.kode_midtrans;
                                        if (tokenwebMt != '') {
                                            $('#metode').hide();
                                            var url =
                                                '{{ route('cek-order', ['id' => ':id', 'role' => ':role']) }}';
                                            url = url.replace(':id', kode_midtrans);
                                            url = url.replace(':role', 'ortu');

                                            snap.pay(tokenwebMt, {
                                                onSuccess: function(result) {
                                                    notif('success', result
                                                        .status_message);
                                                    //console.log(result);
                                                    window.location.href = url;
                                                },
                                                onPending: function(result) {
                                                    notif('info', result
                                                        .status_message);
                                                    window.location.href = url;
                                                },
                                                onError: function(result) {
                                                    notif('error', result
                                                        .status_message);
                                                },
                                                onClose: function() {
                                                    notif('info',
                                                        'customer closed the popup without finishing the payment'
                                                    );
                                                }
                                            });

                                        }
                                    }

                                } else if (result['info'] == 'error') {
                                    notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                notif('error', log);
                            }
                        }
                    });
                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
