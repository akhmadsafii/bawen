@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Jurnal Umum</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data"  method="POST" action="{{ route('store_transaksi_jurnal') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="l0">Tahun Ajaran</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                            @foreach ($tahun_ajaran as $key => $value)
                                                @php
                                                    $explode_tahun = explode('/', $value['tahun_ajaran']);
                                                @endphp
                                                @if ($explode_tahun[0] == session('tahun'))
                                                    <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @else
                                                    <option value="{{ $value['tahun_ajaran'] }}">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="l0">Tanggal</label>
                                    <div class="input-group input-has-value col-md-8">
                                        <input type="text" class="form-control datepicker" name="tgl_bayar"
                                            data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                            value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="l0">No Referensi</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="no_referensi" placeholder="No Referensi"
                                            type="text" name="no_referensi">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-12 ml-4">
                            <div class="table-responsive">
                                <table id="table_akun" width="100%" class="table table-striped table-responsive">
                                    <thead>
                                        <tr class="table-info">
                                            <th width="30%">Akun <span class="text-red">*</span></th>
                                            <th width="20%">Deskripsi <span class="text-red">*</span></th>
                                            <th width="20%">Debit</th>
                                            <th width="20%">Kredit</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="clonable form-clone" parentId="1">
                                            <td width="30%">
                                                <select class="form-control akun" name="akun[]" id="akun"
                                                    required="required">
                                                    <option disabled="disabled" selected="selected"> Pilih Akun </option>
                                                    @foreach ($rekening_akun as $rekening)
                                                        <option value="{{ $rekening['id'] }}">{{ $rekening['kode'] }} -
                                                            {{ $rekening['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td width="20%">
                                                <textarea class="form-control" name="deskripsi[]" id="deskripsi" rows="1"
                                                    required="required"></textarea>
                                            </td>
                                            <td width="20%"><input name="debit[]" type="text" id="debit"
                                                    class="form-control" onkeyup="currencyFormat(this)"></td>
                                            <td width="20%"><input name="kredit[]" type="text" id="kredit"
                                                    class="form-control" onkeyup="currencyFormat(this)"></td>
                                            <td width="10%" class="btn-group text-center" role="group" width="100%">
                                                <button type="button" name="remove" id="removex"
                                                    class="btn btn-danger remove-clone remove"><i
                                                        class="fa fa-minus"></i></button> &nbsp;
                                                <button type="button" name="add" id="addx"
                                                    class="btn btn-info add-clone add"><i
                                                        class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="l38">Catatan / Memo</label>
                                                    <textarea class="form-control" name="catatan" id="catatan"
                                                        rows="3"></textarea>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group row">
                                                    <label for="l38"><i class="fa fa-file"></i> Lampiran
                                                        Berkas</label><br>
                                                    <span class="text-info" id="tambah_berkas"><input name="image"
                                                            type="file" id="image" class="form-control" accept="image/*"></span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="l38">Total Debit</label><br>
                                                    <span class="text-info total_debit" data-total_debit="0">Rp.0</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="l38">Total Kredit</label><br>
                                                    <span class="text-info total_kredit" data-total_kredit="0">Rp.0</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="alert alert-icon alert-warning border-warning alert-dismissible fade show"
                                    role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close"><span aria-hidden="true">×</span>
                                    </button> <i class="material-icons list-icon">warning</i>
                                    <strong class="text-red">
                                        Total Debit dan total kredit harus sama (Balance) !
                                    </strong>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i> Batal
                            </button>
                            <button type="submit" class="btn btn-info update pos_transaction">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');

                    //cek validasi form debit kredit di isi semua
                    var debit = $('tr[parentId="' + parentId + '"] input[name="debit[]"]').val().split(
                        ',').join('');
                    var kredit = $('tr[parentId="' + parentId + '"] input[name="kredit[]"]').val()
                        .split(',').join('');
                    var akun = $('tr[parentId="' + parentId +
                        '"] select[name="akun[]"] option:selected');

                    var deskripsi = $('tr[parentId="' + parentId +
                        '"] textarea[name="deskripsi[]"]');

                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('0');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('input[name="kredit[]').each(function() {
                        return $(this).removeAttr('readonly');
                    });

                    clonned.find('input[name="debit[]').each(function() {
                        return $(this).removeAttr('readonly');
                    });

                    if (akun.val() == null && deskripsi == null && debit == 0 && kredit == 0) {
                        window.notif('error', 'Form item tidak boleh kosong !');
                    } else if (akun.val() == null || akun.val() == '') {
                        window.notif('error', 'Pilih Salah Satu Akun terlebih dahulu!');
                    } else if (deskripsi == null || deskripsi == '') {
                        window.notif('error', 'Deskripsi transaksi akun di isi  terlebih dahulu!');
                    } else if ((deskripsi == null || deskripsi == '') && (akun.val() == null || akun
                            .val() == '')) {
                        window.notif('error', 'Deskripsi dan  akun di isi  terlebih dahulu!');
                    } else {
                        if (debit == 0 && kredit == 0) {
                            window.notif('error', 'Pilih Salah Satu Posisi Debit atau Kredit' + akun
                                .text());
                            $('tr[parentId="' + parentId + '"] input[name="debit[]"]').val('0');
                            $('tr[parentId="' + parentId + '"] input[name="kredit[]"]').val('0');
                        } else if (debit > 0 && kredit > 0) {
                            window.notif('error', 'Pilih Salah Satu Posisi Debit atau Kredit' + akun
                                .text() + 'tidak boleh diisi semua!');
                            $('tr[parentId="' + parentId + '"] input[name="debit[]"]').val('0');
                            $('tr[parentId="' + parentId + '"] input[name="kredit[]"]').val('0');
                        } else if (debit > 0) {
                            $(this).hide();
                            $('.clonable').parents('tbody').append(clonned);
                            $('tr[parentId="' + parentId + '"] input[name="kredit[]"]').attr('readonly',
                                'readonly');
                            $('tr[parentId="' + parentId + '"] input[name="kredit[]"]').val('0');
                        } else if (kredit > 0) {
                            $(this).hide();
                            $('.clonable').parents('tbody').append(clonned);
                            $('tr[parentId="' + parentId + '"] input[name="debit[]"]').attr('readonly',
                                'readonly');
                            $('tr[parentId="' + parentId + '"] input[name="debit[]"]').val('0');
                        }
                    }

                    window.calculate(); // calculate
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                    window.calculate(); // calculate
                });

                //on focus
                $('body').on('focus', 'input[name="debit[]"]', function() {
                    if ($(this).val() == 0) {
                        $(this).val(''); //reset blank
                    }
                });

                //on enter
                $('body').on('keyup', 'input[name="debit[]"]', function(event) {
                    if (event.keyCode === 13) {
                        window.calculate(); // calculate
                        var clonned = $('.clonable:last-child');
                        var parentId = clonned.attr('parentId');

                        //readonly kredit
                        var kredit = $('tr[parentId="' + parentId + '"] input[name="kredit[]"]');
                        if (parseInt($(this).val().split(',').join('')) > 0) {
                            kredit.attr('readonly', 'readonly');
                            kredit.val('0');
                        }

                    }
                });

                $('body').on('keyup', 'input[name="kredit[]"]', function(event) {
                    if (event.keyCode === 13) {
                        window.calculate(); // calculate
                        var clonned = $('.clonable:last-child');
                        var parentId = clonned.attr('parentId');

                        //readonly kredit
                        var debit = $('tr[parentId="' + parentId + '"] input[name="debit[]"]');
                        if (parseInt($(this).val().split(',').join('')) > 0) {
                            debit.attr('readonly', 'readonly');
                            debit.val('0');
                        }

                    }
                });

                //on focus
                $('body').on('focus', 'input[name="kredit[]"]', function() {
                    if ($(this).val() == 0) {
                        $(this).val(''); //reset blank
                    }
                });

                $("body").bind("keydown", '#DocForm', function(e) {
                    if (e.keyCode === 13) return false;
                });

                /*$("body").on("submit", '#DocForm', function(e) {
                    if (e.keyCode === 13) return false;
                    e.preventDefault();
                    let check = false;
                    const loader = $('button.update');
                    var formData = new FormData(this);
                    formData.append('image', $('input[type=file]')[0].files[0]);
                    var akun = $('select[name*="akun[]"]').each(function() {
                        var vax = $(this).val();
                        if (vax == null) {
                            check = true;
                        }
                    });
                    var textarea = $('textarea[name*="deskripsi[]"]').each(function() {
                        var vax = $(this).val();
                        if (vax == null) {
                            check = true;
                        }
                    });
                    if (check == true) {
                        window.notif('error', 'Periksa Kembali Form Akunnya !');
                    } else {
                        var total_kredit = $('span.total_kredit').attr('data-total_kredit');
                        var total_debit = $('span.total_debit').attr('data-total_debit');
                        if (typeof total_debit != 'undefined' && typeof total_kredit !=
                            'underfined') {
                            if (parseInt(total_debit) >= 0 && parseInt(total_kredit) >= 0) {
                                if (parseInt(total_debit) == parseInt(total_kredit)) {
                                    window.PostSubmit(formData, loader);
                                } else {
                                    window.notif('warning',
                                        'Periksa Kembali Total Debit dan  total kredit harus sama (Balance) !!!'
                                    );
                                }
                            }
                        }
                    }
                });*/

                /**submit form */
                $('body').on('click','button.update.pos_transaction',function(e){
                    if (e.keyCode === 13) return false;
                    e.preventDefault();
                    let check = false;
                    var akun = $('select[name*="akun[]"]').each(function() {
                        var vax = $(this).val();
                        if (vax == null) {
                            check = true;
                        }
                    });
                    var textarea = $('textarea[name*="deskripsi[]"]').each(function() {
                        var vax = $(this).val();
                        if (vax == null) {
                            check = true;
                        }
                    });
                    if (check == true) {
                        window.notif('error', 'Periksa Kembali Form Akunnya !');
                    } else {
                        var total_kredit = $('span.total_kredit').attr('data-total_kredit');
                        var total_debit = $('span.total_debit').attr('data-total_debit');
                        if (typeof total_debit != 'undefined' && typeof total_kredit !=
                            'underfined') {
                            if (parseInt(total_debit) >= 0 && parseInt(total_kredit) >= 0) {
                                if (parseInt(total_debit) == parseInt(total_kredit)) {
                                    $('form#DocForm').submit();
                                } else {
                                    window.notif('warning',
                                        'Periksa Kembali Total Debit dan  total kredit harus sama (Balance) !!!'
                                    );
                                }
                            }
                        }
                    }
                });


                window.calculate = function count() {
                    var nominal_debit_parse = 0;
                    var nominal_kredit_parse = 0;

                    $('input[name="debit[]"]').each(function() {
                        var valx = $(this).val();
                        if (valx != '') {
                            nominal_debit_parse += parseInt(valx.split(',').join(''));
                        }
                    });

                    $('input[name="kredit[]"]').each(function() {
                        var valx = $(this).val();
                        if (valx != '') {
                            nominal_kredit_parse += parseInt(valx.split(',').join(''));
                        }
                    });

                    $('span.total_kredit').removeAttr('data-total_kredit');
                    $('span.total_kredit').attr('data-total_kredit', nominal_kredit_parse);
                    $('span.total_kredit').html('Rp.' + nominal_kredit_parse.format());

                    $('span.total_debit').removeAttr('data-total_debit');
                    $('span.total_debit').attr('data-total_debit', nominal_debit_parse);
                    $('span.total_debit').html('Rp.' + nominal_debit_parse.format());
                }

                //submit form
                window.PostSubmit = function postx(dataform, loader) {
                    var url = '{{ route('store_transaksi_jurnal') }}';
                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: dataform,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {

                                $(loader).html(
                                    '<i class="fa fa-save"></i> Simpan');

                                window.notif(result['info'], result['message']);

                                if (result['info'] == 'success') {
                                    window.location.reload(true);
                                }

                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });
                }

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

            });
        })(jQuery, window);
    </script>
@endsection
