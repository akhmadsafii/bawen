@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Histori</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Saldo Akun</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Saldo Akun</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data" id="print-data">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModalEdit" tabindex="-1"
        role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Saldo Akun</h5>
                </div>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" class="form-control is-valid" id="id_saldo_akun" name="id_saldo_akun">
                        <table class="table" width="100%">
                            <tbody>
                                <tr width="100%;">
                                    <td>ID Akun</td>
                                    <td>:</td>
                                    <td>
                                        <select class="form-control akun" name="akun" id="akun" required="required">
                                            <option disabled="disabled" selected="selected"> Pilih Akun
                                            </option>
                                            @foreach ($rekening_akun as $rekening)
                                                <option value="{{ $rekening['id'] }}">
                                                    {{ $rekening['kode'] }} -
                                                    {{ $rekening['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr width="100%;">
                                    <td>Saldo Awal</td>
                                    <td>:</td>
                                    <td><input name="saldo_awal" type="text" id="saldo_awal" class="form-control"
                                            onkeyup="currencyFormat(this)"></td>
                                </tr>
                                <tr width="100%;">
                                    <td>Saldo Akhir</td>
                                    <td>:</td>
                                    <td><input name="saldo_akhir" type="text" id="saldo_akhir" class="form-control"
                                            onkeyup="currencyFormat(this)"></td>
                                </tr>
                                <tr width="100%;">
                                    <td>Tahun</td>
                                    <td>:</td>
                                    <td>
                                        <select class="form-control" id="tahun" name="tahun" required="">
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun</option>
                                            @foreach ($tahunlist as $tahunx)
                                                @if ($tahun == $tahunx)
                                                    <option value="{{ $tahunx }}" selected="selected">
                                                        {{ $tahunx }}
                                                    </option>
                                                @endif
                                                <option value="{{ $tahunx }}">{{ $tahunx }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning btn-rounded ripple text-left">Update</button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_doc" class="table table-striped table-responsive">
                                <thead>
                                    <tr class="table-info">
                                        <th>No</th>
                                        <th>Kode Akun</th>
                                        <th>Nama Akun</th>
                                        <th>Saldo Awal</th>
                                        <th>Saldo Akhir</th>
                                        <th>Tahun</th>
                                        <th>Update </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Append Create Datatables-->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Akun</th>
                                        <th>Nama Akun</th>
                                        <th>Saldo Awal</th>
                                        <th>Saldo Akhir</th>
                                        <th>Tahun Ajaran </th>
                                        <th>Update </th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            (function($, global) {
                "use-strict"
                var table_setting;
                let config_table;
                $(document).ready(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4]
                                },
                                customize: function(doc) {
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                            '');
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('ajax_data_saldoakun') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'kode_akun',
                                name: 'kode_akun'
                            },
                            {
                                data: 'akun',
                                name: 'akun'
                            },
                            {
                                data: 'saldo_awal',
                                name: 'saldo_awal'
                            },
                            {
                                data: 'saldo_akhir',
                                name: 'saldo_akhir'
                            },
                            {
                                data: 'tahun',
                                name: 'tahun'
                            },
                            {
                                data: 'updated_at',
                                name: 'updated_at'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_setting = $('#table_doc').dataTable(config_table);
                    //show data
                    $('body').on('click', '.show.akuntansi.saldoakun', function() {
                        var id = $(this).data('id');
                        var url = '{{ route('show_saldo_akun', ':id') }}';
                        url = url.replace(':id', id);
                        const loader = $(this);
                        let printdata = '';
                        let list_bulan = '';
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                        var rows = JSON.parse(JSON.stringify(result['data']));

                                        $('#printStruckModal').modal('show');

                                        $('.print-data').html(''); //reset append

                                        printdata = printdata + `
                                       <div class="col-md-12">
                                            <table class="table" width="100%">
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Kode Akun</td>
                                                        <td>:</td>
                                                        <td>` + rows.kode_akun + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Akun</td>
                                                        <td>:</td>
                                                        <td>` + rows.akun + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Saldo Awal</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.saldo_awal).format() + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Saldo Akhir</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.saldo_akhir).format() + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Tahun</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                        $('.print-data').html(printdata);

                                    } else {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                    }
                                }

                            }
                        });
                    });

                    //reset modal
                    $('body').on('hidden.bs.modal', '.modal', function() {
                        $(this).removeData('bs.modal');
                    });

                    //show data
                    $('body').on('click', '.edit.akuntansi.saldoakun', function() {
                        var id = $(this).data('id');
                        var url = '{{ route('show_saldo_akun', ':id') }}';
                        url = url.replace(':id', id);
                        const loader = $(this);
                        let printdata = '';
                        let list_bulan = '';
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                        var rows = JSON.parse(JSON.stringify(result['data']));

                                        $('#printStruckModalEdit').modal('show');
                                        $('input[name="id_saldo_akun"]').val(rows.id);
                                        $('input[name="saldo_awal"]').val(parseInt(rows
                                            .saldo_awal).format());
                                        $('input[name="saldo_akhir"]').val(parseInt(rows
                                            .saldo_akhir).format());

                                        $('select[name="akun"] option:contains(' +
                                            rows.akun + ')').prop("selected", true);

                                        var split_tahun = rows.tahun.split('/');

                                        $('select[name="tahun"] option:contains(' +
                                            split_tahun[0] + ')').prop("selected", true);
                                    } else {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                    }
                                }

                            }
                        });
                    });

                    //update
                    $('body').on('submit', '#DocFormx', function(e) {
                        e.preventDefault();
                        var id = $('input[name="id_saldo_akun"]').val();
                        var urlx = '{{ route('update_saldo_akun', ':id') }}';
                        urlx = urlx.replace(':id', id);

                        var formData = new FormData(this);

                        const loader = $('button.update');

                        $.ajax({
                            type: "PUT",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $('#printStruckModalEdit').modal('hide');
                                        table_setting.fnDraw(false);
                                        $(loader).html('<i class="fa fa-save"></i> Update');

                                        window.notif(result['info'], result['message']);

                                    } else if (result['info'] == 'error') {
                                        $(loader).html('<i class="fa fa-save"></i> Update');

                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                }

                            }
                        });

                    });

                    //currensy
                    Number.prototype.format = function(n, x) {
                        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                    };

                    //window notif
                    window.notif = function notif(tipe, value) {
                        $.toast({
                            icon: tipe,
                            text: value,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        return true;
                    }

                    //str upword
                    window.ucwordx = function ucwords(str) {
                        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                            return $1.toUpperCase();
                        });
                    }

                    String.prototype.reverse = function() {
                        return this.split("").reverse().join("");
                    }

                    window.currencyFormat = function reformatText(input) {
                        var x = input.value;
                        x = x.replace(/,/g, ""); // Strip out all commas
                        x = x.reverse();
                        x = x.replace(/.../g, function(e) {
                            return e + ",";
                        }); // Insert new commas
                        x = x.reverse();
                        x = x.replace(/^,/, ""); // Remove leading comma
                        input.value = x;
                    }

                });
            })(jQuery, window);
        </script>
    @endsection
