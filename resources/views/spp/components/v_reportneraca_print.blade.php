<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            margin: 0;
            padding: 0
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                /* DIN A4 standard, Europe */
                margin: 0;
            }

            html,
            body {
                width: 210mm;
                /* height: 297mm; */
                height: 282mm;
                font-size: 11px;
                background: #FFF;
                overflow: visible;
            }

            body {
                padding-top: 15mm;
            }
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }

        $(document).ready(function() {
            $('#table_doc').each(function() {
                var Column_number_to_Merge = 1;

                // Previous_TD holds the first instance of same td. Initially first TD=null.
                var Previous_TD = null;
                var i = 1;
                $("tbody", this).find('tr').each(function() {
                    // find the correct td of the correct column
                    // we are considering the table column 1, You can apply on any table column
                    var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge +
                        ')');

                    if (Previous_TD == null) {
                        // for first row
                        Previous_TD = Current_td;
                        i = 1;
                    } else if (Current_td.text() == Previous_TD.text()) {
                        // the current td is identical to the previous row td
                        // remove the current td
                        Current_td.remove();
                        // increment the rowspan attribute of the first row td instance
                        Previous_TD.attr('rowspan', i + 1);
                        i = i + 1;
                    } else {
                        // means new value found in current td. So initialize counter variable i
                        Previous_TD = Current_td;
                        i = 1;
                    }
                });
            });

            PrintWindow();
        });
    </script>
</head>

<body>
    <center>
        @if (!empty(session('sekolah')))
            <b>{{ session('sekolah') }}</b>
            <br>
        @endif
        <b>NERACA</b>
        <br>
        <b>Periode Tahun {{ $tahun }} </b>
    </center>
    <br>
    <hr>
    <br>

    @php
        $saldo_awal_modal = 0;
        $saldo_akun_aktiva_lancar = 0;
        $jumlah_saldo_akun_aktiva_lancar = 0;
        $saldo_akun_aktiva_tetap = 0;
        $jumlah_saldo_akun_aktiva_tetap = 0;

        $saldo_akun_investasi = 0;
        $jumlah_akun_investasi = 0;
        $total_investasi = 0;

        $saldo_akun_aktiva_non = 0;
        $jumlah_akun_aktiva_non = 0;
        $total_aktiva_non = 0;

        $saldo_akun_aktiva_lain = 0;
        $jumlah_akun_aktiva_lain = 0;
        $total_aktiva_lain = 0;

        $total_aktiva = 0;
        $modal = 0;
        $jumlah_modal = 0;
        $hutang = 0;
        $jumlah_hutang = 0;
        $total_passiva = 0;
        $total_passivax = 0;
        $laba_ditahan = 0;
    @endphp
    <table class="table table-bordered">
        <tbody>
            <tr class="table-info">
                <td><b>Aktiva</b></td>
                <td><b>Passiva</b></td>
            </tr>
            <tr>
                <td>
                    <table class="table table-hover" border="1" width="100%">
                        <tbody>
                            @foreach ($report_neraca as $key => $laporan)
                                @if ($key == 'aktiva')
                                    @foreach ($laporan as $list)
                                        <tr>
                                            <td>{{ $list['nama'] }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @foreach ($list['akun'] as $akunx => $aktiva)
                                            @if ($list['kode'] == '1.001')
                                                @php
                                                    $saldo_akun_aktiva_lancar = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                    $jumlah_saldo_akun_aktiva_lancar += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                @endphp
                                                <tr>
                                                    <td> <span class="ml-2">
                                                            {{ $aktiva['nama'] }}</span></td>
                                                    <td>{{ number_format($saldo_akun_aktiva_lancar, 0) }}
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @if ($list['kode'] == '1.002')
                                                @php
                                                    $saldo_akun_aktiva_tetap = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                    $jumlah_saldo_akun_aktiva_tetap += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                @endphp
                                                <tr>
                                                    <td> <span class="ml-2">
                                                            {{ $aktiva['nama'] }}</span></td>
                                                    <td>{{ number_format($saldo_akun_aktiva_tetap, 0) }}
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endif

                                            @if ($list['kode'] == '1.004')
                                                @php
                                                    $saldo_akun_aktiva_non = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                    $jumlah_akun_aktiva_non += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                @endphp
                                                <tr>
                                                    <td> <span class="ml-2">
                                                            {{ $aktiva['nama'] }}</span></td>
                                                    <td>{{ number_format($saldo_akun_aktiva_non, 0) }}
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @if ($list['kode'] == '1.005')
                                                @php
                                                    $saldo_akun_aktiva_lain = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                    $jumlah_akun_aktiva_lain += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                @endphp
                                                <tr>
                                                    <td> <span class="ml-2">
                                                            {{ $aktiva['nama'] }}</span></td>
                                                    <td>{{ number_format($saldo_akun_aktiva_lain, 0) }}
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @php
                                                $saldo_awal_modal = intval($aktiva['saldo_awal']);
                                            @endphp
                                        @endforeach

                                        <tr>
                                            <td><span class="ml-2"><b>Jumlah
                                                        {{ $list['nama'] }}</b></span>
                                            </td>
                                            <td></td>
                                            @if ($list['kode'] == '1.001')
                                                <td>{{ number_format($jumlah_saldo_akun_aktiva_lancar, 0) }}
                                                </td>
                                            @elseif ($list['kode'] == '1.002')
                                                <td>{{ number_format($jumlah_saldo_akun_aktiva_tetap, 0) }}
                                                </td>
                                            @elseif ($list['kode'] == '1.003')
                                                <td>{{ number_format($jumlah_akun_investasi, 0) }}
                                                </td>
                                            @elseif ($list['kode'] == '1.004')
                                                <td>{{ number_format($jumlah_akun_aktiva_non, 0) }}
                                                </td>
                                            @elseif ($list['kode'] == '1.005')
                                                <td>{{ number_format($jumlah_akun_aktiva_lain, 0) }}
                                                </td>
                                            @endif
                                        </tr>
                                        @php
                                            $total_aktiva = intval($jumlah_saldo_akun_aktiva_lancar) + intval($jumlah_saldo_akun_aktiva_tetap) + intval($jumlah_akun_investasi) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_lain);
                                        @endphp
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </td>
                <td>
                    <table class="table table-hover">
                        <tbody>
                            @foreach ($report_neraca as $key => $laporan)
                                @if ($key == 'modal')
                                    @foreach ($laporan as $list)
                                        <tr>
                                            <td>{{ $list['nama'] }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @foreach ($list['akun'] as $akunx => $aktiva)
                                            @php
                                                $modal = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                $jumlah_modal += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);

                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($modal, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    <tr>
                                        <td><span class="ml-2"><b>Jumlah
                                                    Modal </b></span>
                                        </td>
                                        <td></td>
                                        <td>{{ number_format($jumlah_modal, 0) }}
                                        </td>
                                    </tr>
                                @elseif($key == 'hutang')
                                    @foreach ($laporan as $list)
                                        <tr>
                                            <td>{{ $list['nama'] }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @foreach ($list['akun'] as $akunx => $aktiva)
                                            @php
                                                $hutang = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                                $jumlah_hutang += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($hutang, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    <tr>
                                        <td><span class="ml-2"><b>Jumlah Hutang / Kewajiban </b></span>
                                        </td>
                                        <td></td>
                                        <td>{{ number_format($jumlah_hutang) }}</td>
                                    </tr>
                                @endif
                                @php
                                    $total_passiva = intval($jumlah_modal) + intval($jumlah_hutang);
                                    $laba_ditahan = intval($report_laba_rugi['laba_rugi']);
                                    $total_passivax = intval($total_passiva) + intval($laba_ditahan);
                                @endphp
                            @endforeach
                            <tr>
                                <td><span class="ml-2"><b>Laba / Rugi </b></span>
                                </td>
                                <td></td>
                                <td>{{ number_format($laba_ditahan, 0) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">Total Aktiva <span
                        class="ml-5"><b>{{ number_format($total_aktiva, 0) }}</b></span></td>
                <td align="center ">Total Passiva <span
                        class="ml-5"><b>{{ number_format($total_passivax, 0) }}</b></span></td>
            </tr>
        </tbody>
    </table>

</body>

</html>
