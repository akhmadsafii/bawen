<!DOCTYPE html>
<html>
<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto;
            width: 100%;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }
            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>
<body>
     <center><h1><u><b>Nota Pembayaran</b></u></h1></center>
    <div class="grid-container">
        <div class="grid-item">
            <table class="table">
                <tbody align="left">
                    <tr width="100%">
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ ucwords($detail['nama']) }}</td>
                    </tr>
                    <tr width="100%">
                        <td>NIS</td>
                        <td>:</td>
                        <td>{{ ucwords($detail['nis']) }}</td>
                    </tr>
                    <tr width="100%;">
                        <td>Kelas</td>
                        <td>:</td>
                        <td>{{ $detail['kelas'] }}</td>
                    </tr>
                    <tr width="100%;">
                        <td>Rombel</td>
                        <td>:</td>
                        <td>{{ $detail['rombel'] }}</td>
                    </tr>
                    <tr>
                        <td>Jurusan</td>
                        <td>:</td>
                        <td>{{ $detail['jurusan'] }}</td>
                    </tr>
                    <tr>
                        <td>Sekolah</td>
                        <td>:</td>
                        <td>{{ $detail['sekolah'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="grid-item">
            <table width="100%" class="table">
                <tbody align="left">
                    <tr>
                        <td>Kode</td>
                        <td>:</td>
                        <td><small><b>{{ $detail['kode'] }}</b></small></td>
                    </tr>
                    <tr>
                        <td>Tahun Ajaran</td>
                        <td>:</td>
                        <td>{{ $detail['tahun_ajaran'] }}</td>
                    </tr>
                    <!--tr>
                        <td>Jumlah Tagihan Dibayar</td>
                        <td>:</td>
                        <td>{{ $detail['jml_tagihan_dibayar'] }}</td>
                    </tr-->
                    <tr>
                        <td>Total</td>
                        <td>:</td>
                        <td>Rp. {{ number_format($detail['nominal'], 0) }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Bayar</td>
                        <td>:</td>
                        <td>{{ \Carbon\Carbon::parse($detail['tgl_bayar'])->isoFormat('dddd, D MMMM Y') }}</td>
                    </tr>
                    @if (!empty($detail['keterangan']))
                    <tr>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>
                            {{ $detail['keterangan'] }}
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <table class="table">
        <legend><b>Detail  Pembayaran</b></legend>
        <thead>
            <tr>
                <td>No</td>
                <td>Nama Tagihan</td>
                <td>Nominal</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($detail['tagihan'] as $key => $val)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['nama_tagihan'] }} </td>
                    <td>Rp. {{ number_format($val['nominal'], 0) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <table width="100%;">
        <tbody>
            <tr>
                <td align="left"></td>
                <td align="center">
                    <strong>Penyetor </strong>
                    <br>
                    <br>
                    <br>
                    <strong><b>{{ ucwords($detail['nama']) }} </b></strong>
                </td>
                <td align="right">
                    <strong>Penerima,</strong>
                    <br>
                    <br>
                    @if (!empty($detail['operator']))
                    <strong><b>{{ ucwords($detail['operator']) }}</b></strong>
                    <br>
                    @else
                    <br>
                    @endif
                    <strong><b>Bendahara / Tata Usaha </b></strong>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
