@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">POS</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pos Pengeluaran</li>
                <li class="breadcrumb-item "><a href="{{ route('setting-pos') }}">Pos Pemasukan</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.popup show  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="showModalPos" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Detail </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Kode </label>
                        <div class="col-md-9">
                            <input class="form-control" id="kode" name="kodex" placeholder="Kode" disabled="disabled"
                                type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nama </label>
                        <div class="col-md-9">
                            <input class="form-control" id="nama" name="namax" placeholder="Nama" disabled="disabled"
                                type="text">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
     <!-- /.popup edit  -->
     <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="showModalPosEdit" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header text-inverse">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h5 class="modal-title" id="myLargeModalLabel"> Edit Detail </h5>
             </div>
             <div class="row page-title clearfix">
                <div class="page-title-left">
                </div>
                <div class="page-title-right d-inline-flex">
                    <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus
                        disi!.</p>
                </div>
            </div>
             <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
             <div class="modal-body">
                <input type="hidden" id="id_pos" name="id_pos">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="l11">Akuntansi Akun<span
                            class="text-red">(*)</span></label>
                    <div class="col-md-4">
                        <select class="form-control" id="id_akun" name="id_akun" required="required">
                            <option disabled="disabled" selected="true" value="">Pilih Akuntansi Akun</option>
                            @foreach ($akuntansi_akun as $key => $value)
                                <option value="{{ $value['id'] }}"> {{ $value['kode'] }} - {{ $value['nama'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l0">Kode  <span
                        class="text-red">(*)</span> </label>
                     <div class="col-md-9">
                         <input class="form-control" id="kode" name="kode" placeholder="Kode"
                             type="text">
                     </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l0">Nama  <span
                        class="text-red">(*)</span></label>
                     <div class="col-md-9">
                         <input class="form-control" id="nama" name="nama" placeholder="Nama"
                             type="text">
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                <button type="submit" class="btn btn-info update">
                    <i class="material-icons list-icon">save</i>
                    Update
                </button>
                 <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                     this</button>
             </div>
             </form>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNew'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_pos_pengeluaran') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'jenis',
                            name: 'jenis'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);

                //create new

                $('body').on('click','#createNew',function(){
                    window.location.href = '{{ route('create_pos_pengeluaran') }}';
                });

                 //reset modal
                 $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //show data
                $('body').on('click', '.show.setting.pos', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show-pos-pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="namax"]').val(rows.nama);
                                    $('input[name="kodex"]').val(rows.kode);
                                    $('#showModalPos').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                //show data
                $('body').on('click', '.edit.setting.pos', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-pos-pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_pos"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="kode"]').val(rows.kode);
                                    $('#showModalPosEdit').modal('show');
                                    $("select#id_akun > option[value=" + rows
                                        .id_akun + "]").prop("selected", true);
                                } else {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                }
                            }

                        }
                    });
                });

                //update
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_pos"]').val();
                    var urlx = '{{ route('store_pos_update_pengeluaran', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showModalPosEdit').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.edit.setting.pos').html('<i class="fa fa-edit"></i>');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                  //remove  data
                  $('body').on('click', '.remove.setting.pos', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy-pos-pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });


                //remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

            });

        })(jQuery, window);
    </script>
@endsection
