@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi Penyusutan Aset </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penyusutan Aset </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="detailasset" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaction Purchase Asset Fixed </h5>
                </div>
                <div class="modal-body">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>Kode Transaksi </td>
                                <td>:</td>
                                <td><span class="text-kode_transaksi ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Tanggal Transaksi </td>
                                <td>:</td>
                                <td><span class="text-tanggal_transaksi ml-3"></span></td>
                                <td>Tanggal Diterima </td>
                                <td>:</td>
                                <td><span class="text-tanggal_diterima ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Kode Akun </td>
                                <td>:</td>
                                <td><span class="text-kode_akun ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Total Transaksi </td>
                                <td>:</td>
                                <td><span class="text-total_transaksi ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Status Transaksi </td>
                                <td>:</td>
                                <td><span class="text-status_transaksi ml-3"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="table-responsive">
                        <table id="table_doc_item" class="table table-striped table-responsive">
                            <thead>
                                <tr class="table-info">
                                    <th>No</th>
                                    <th>Nama Item</th>
                                    <th>Harga Beli</th>
                                    <th>Subtotal</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><span class="text-nama_item ml-3"></td>
                                    <td><span class="text-harga_beli ml-3"></td>
                                    <td><span class="text-subtotal ml-3"></span></td>
                                    <td><span class="text-note ml-3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>

                    <form id="DocFormxEdit" name="DocFormxEdit" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_edit_item" class="form-control">
                        <div class="box-title">Form Penyusutan Aset </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="l36">Usia Tahun </label>
                                    <input class="form-control" id="usia_tahun" name="usia_tahun" type="number"
                                        required="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="l37">Usia Bulan </label>
                                    <input class="form-control" id="usia_bulan" name="usia_bulan" type="number"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions btn-list">
                            <button class="btn btn-primary update-penyusutan" type="submit">Update</button>
                            <button class="btn btn-outline-default" type="reset">Reset</button>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="alert alert-icon alert-info border-info alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button> <i class="material-icons list-icon">info</i>  <strong>Informasi!</strong><br>

                         <ul class="mr-t-10">
                            <li>Harap sesuaikan dulu akun penyusutan dan beban penyusutan aset di transaksi jurnal umum</li>
                            <li> Nilai Akumulasi Penyusutan Aset  mengacu pada tabel LIST DEPRECIATION ASSET  di lakukan secara manual melalui transaksi jurnal umum</li>
                        </ul>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-title">List Transaction Purchase Fixed Asset </div>
                            <div class="widget-body clearfix">
                                <div class="table-responsive">
                                    <table id="table_list_barang" class="table table-striped table-responsive" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode</th>
                                                <th>Tanggal Transaksi </th>
                                                <th>Tanggal Diterima </th>
                                                <th>Kode Akun</th>
                                                <th>Aset</th>
                                                <th>Qty</th>
                                                <th>Harga Beli </th>
                                                <th>Subtotal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-title">List Depreciation Asset </div>
                            <div class="widget-body clearfix">
                                <div class="table-responsive">
                                    <table id="table_list_barang_penyusutan" class="table table-striped table-responsive"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Akun</th>
                                                <th>Aset</th>
                                                <th>Nominal</th>
                                                <th>Usia Tahun</th>
                                                <th>Usia Bulan</th>
                                                <th>Penyusutan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            var config_table, config_table2, table_setting, table_detailitem, table_setting2, table_setting1;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('penyusutan-asset') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_transaksi',
                            name: 'kode_transaksi'
                        },
                        {
                            data: 'tgl_transaksi',
                            name: 'tgl_transaksi'
                        },
                        {
                            data: 'tgl_diterima',
                            name: 'tgl_diterima'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_asset',
                            name: 'nama_asset'
                        },
                        {
                            data: 'jumlah',
                            name: 'jumlah'
                        },
                        {
                            data: 'satuan',
                            name: 'satuan'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_list_barang').dataTable(config_table);

                config_table2 = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_asset_purchase') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_asset',
                            name: 'nama_asset'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'usia_tahun',
                            name: 'usia_tahun'
                        },
                        {
                            data: 'usia_bulan',
                            name: 'usia_bulan'
                        },
                        {
                            data: 'penyusutan',
                            name: 'penyusutan'
                        },
                    ],
                };

                table_setting2 = $('#table_list_barang_penyusutan').dataTable(config_table2);


                $('body').on('click', '.show.asset.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_asset_aktiva_transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    $('#detailasset').modal('show');
                                    $('span.text-kode_transaksi').html(rows.kode_transaksi);
                                    $('span.text-tanggal_transaksi').html(dateFormatID(rows
                                        .tgl_transaksi));
                                    $('span.text-tanggal_diterima').html(dateFormatID(rows
                                        .tgl_diterima));
                                    $('span.text-kode_akun').html(rows.kode_akun);
                                    $('span.text-total_transaksi').html(rows.nominal
                                        .format());

                                    $('span.text-harga_beli').html(rows.satuan
                                        .format());

                                    $('span.text-subtotal').html(rows.nominal
                                        .format());

                                    $('span.text-note').html(rows.keterangan);
                                    $('span.text-nama_item').html(rows.nama_asset);
                                    $('input[name="usia_tahun"]').val(rows.usia_tahun);
                                    $('input[name="usia_bulan"]').val(rows.usia_bulan);
                                    $('input[name="id_edit_item"]').val(rows.id);

                                    if (rows.status == '1') {
                                        $('span.text-status_transaksi').html('Beli');
                                    } else if (rows.status == '2') {
                                        $('span.text-status_transaksi').html('Jual');
                                    }
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });

                });

                  //submit update
                  $('body').on('submit', '#DocFormxEdit', function(e) {
                    e.preventDefault();
                    var id_piutang  =  $('input[name="id_edit_item"]').val();
                    var urlx = '{{ route('store_update_penyusutan_asset') }}';
                    var formData = new FormData(this);
                    const loader = $('update-penyusutan');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading'
                            );
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#detailasset').modal('hide');
                                    table_setting2.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result[
                                        'message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result[
                                        'message']);
                                    $('#detailasset').modal('hide');
                                    table_setting2.fnDraw(false);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html(
                                    '<i class="fa fa-save"></i> Update');
                                $('#transaksi_cicilan').modal('hide');
                                table_setting.fnDraw(false);
                            }
                        }
                    });
                });

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //format date indonesia
                window.dateFormatID = function parseDate(xt) {
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
                        'September', 'Oktober', 'November', 'Desember'
                    ];
                    var tanggal = new Date(xt).getDate();
                    var xhari = new Date(xt).getDay();
                    var xbulan = new Date(xt).getMonth();
                    var xtahun = new Date(xt).getYear();
                    var harix = hari[xhari];
                    var bulanx = bulan[xbulan];
                    var tahunx = (xtahun < 1000) ? xtahun + 1900 : xtahun;
                    var fulldate = harix + ', ' + tanggal + ' ' + bulanx + ' ' + tahunx;
                    return fulldate;
                }

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }


            });
        })(jQuery, window);
    </script>
@endsection
