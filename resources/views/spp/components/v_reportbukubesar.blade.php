@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Buku Besar</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Akun</label>
                                <select class="form-control" id="akun" name="akun" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Akun
                                    </option>
                                    @foreach ($rekening_akun as $key => $value)

                                        <option value="{{ $value['id'] }}" @if ($value['id'] == $akun)
                                            selected="selected"
                                    @endif>
                                    {{ $value['nama'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Tahun</label>
                                <select class="form-control" id="tahun" name="tahun" required="">
                                    @if (empty(session('tahun')))
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun
                                        </option>
                                    @endif

                                    @foreach ($tahunlist as $tahunx)
                                        @if ($tahun == $tahunx)
                                            <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                            </option>
                                        @endif
                                        <option value="{{ $tahunx }}">{{ $tahunx }}
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Bulan</label>
                                <select class="form-control" id="bulan" name="bulan" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Bulan
                                    </option>
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                            </div>
                            <div class="col-md-3 text-left" style="margin-top: 10px;">
                                <button type="button" class="btn btn-sm btn-outline-primary mt-4 filter"><i
                                        class="fa fa-filter"></i></button>
                                <button type="button" class="btn btn-sm btn-outline-info mt-4 printNota"><i
                                        class="fa fa-print"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <hr>
                        <div class="widget-header text-center">
                            @if (!empty(session('sekolah')))
                                <b>{{ session('sekolah') }}</b>
                                <br>
                            @endif
                            <b>Buku Besar</b>
                            <br>
                        </div>
                        <hr>

                        <table width="100%">
                            <tbody>
                                @if (count($report) > 0)
                                    @php
                                        $current = current($report['akuns']);
                                    @endphp
                                    <tr>
                                        <td class="text-right">Kode</td>
                                        <td>:</td>
                                        <td>{{ $current['kode_akun'] }}</td>
                                        <td class="text-right">Bulan</td>
                                        <td>:</td>
                                        <td>{{ $bulan }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Nama Akun</td>
                                        <td>:</td>
                                        <td>{{ $current['nama_akun'] }}</td>
                                        <td class="text-right">Tahun</td>
                                        <td>:</td>
                                        <td>{{ $tahun }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <br>
                        @if (count($report) > 0)
                            @if (intval($report['saldo_awal']) == 0)
                                <div class="alert alert-error border-error" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <div class="widget-list">
                                        <div class="col-md-12 widget-holder">
                                            <div class="widget-body clearfix">
                                                <strong class="text-red"> Harap Input dulu saldo awal  </strong>
                                                <a href="{{ route('TransaksiSaldoAkunMulti') }}">Input Saldo sekarang !</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                        <div class="table-responsive">
                            @php
                                $total_debit = 0;
                                $total_kredit = 0;
                                $row_count = 0;
                                $saldo = 0;
                            @endphp
                            <table id="table_doc" class="table table-striped" data-toggle="table" width="100%;" border="1">
                                <thead class="table-info">
                                    <tr>
                                        <td class="text-center" style="text-align: center; vertical-align: middle;">
                                            Tanggal</td>
                                        <td class="text-center" style="text-align: center; vertical-align: middle;">
                                            Deskripsi</td>
                                        <td class="text-center" style="text-align: center; vertical-align: middle;">Debit
                                        </td>
                                        <td class="text-center" style="text-align: center; vertical-align: middle;">
                                            Kredit</td>
                                        <td class="text-center" style="text-align: center; vertical-align: middle;">Saldo
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $saldo_awal = 0;
                                        $total_saldo = 0;
                                    @endphp

                                    @if (count($report) > 0)
                                        @php
                                            $saldo_awal = intval($report['saldo_awal']);
                                        @endphp
                                        <tr>
                                            <td></td>
                                            <td>Saldo Awal </td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td class="text-left">
                                                <span class="ml-3">{{ number_format($saldo_awal, 0) }}</span>
                                            </td>
                                        </tr>
                                        @foreach ($report['akuns'] as $reportx)
                                            @php
                                                $total_debit += intval($reportx['debet']);
                                                $total_kredit += intval($reportx['kredit']);
                                                if ($reportx['akun_kategori'] == '2' || $reportx['akun_kategori'] =='3' ) {
                                                    $saldo = $saldo + (intval($reportx['kredit']) - intval($reportx['debet']));
                                                    $total_saldo = intval($saldo_awal) + $saldo;
                                                } else {
                                                    $saldo = $saldo + (intval($reportx['debet']) - intval($reportx['kredit']));
                                                    $total_saldo = intval($saldo_awal) + $saldo;
                                                }
                                                $row_count++;
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ \Carbon\Carbon::parse($reportx['tgl_bayar'])->isoFormat('dddd, D MMMM Y ') }}
                                                </td>
                                                @if (!empty($reportx['keterangan']))
                                                    <td>{{ $reportx['keterangan'] }}</td>
                                                @else
                                                    <td>-</td>
                                                @endif

                                                @if ($reportx['debet'] != 0)
                                                    <td class="text-left">
                                                        <span
                                                            class="ml-3">{{ number_format($reportx['debet'], 0) }}</span>
                                                    </td>
                                                @else
                                                    <td>0</td>
                                                @endif

                                                @if ($reportx['kredit'] != 0)
                                                    <td class="text-left">
                                                        <span
                                                            class="ml-3">{{ number_format($reportx['kredit'], 0) }}</span>
                                                    </td>
                                                @else
                                                    <td>0</td>
                                                @endif

                                                <td class="text-left">
                                                    <span
                                                        class="ml-3">{{ number_format($total_saldo, 0) }}</span>
                                                </td>
                                            </tr>
                                            @if ($row_count % 2 == 0)
                                                <tr>
                                                    <td colspan="5">
                                                        <div class="table-info" style="height:10px;"></div>
                                                    </td>
                                                <tr>
                                            @endif
                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="5" class="text-center text-red">Belum Ada Catatan </td>
                                        <tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" class="text-right">Jumlah</td>
                                        <td>Rp.{{ number_format($total_debit, 0) }}</td>
                                        <td>Rp.{{ number_format($total_kredit, 0) }}</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {

                @if (!empty($index_bulan))
                    $("select[name='bulan'] > option[value=" + {{ $index_bulan }} + "]").prop("selected",
                    true);
                @endif

                $('#table_doc').each(function() {
                    var Column_number_to_Merge = 1;

                    // Previous_TD holds the first instance of same td. Initially first TD=null.
                    var Previous_TD = null;
                    var i = 1;
                    $("tbody", this).find('tr').each(function() {
                        // find the correct td of the correct column
                        // we are considering the table column 1, You can apply on any table column
                        var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge +
                            ')');

                        if (Previous_TD == null) {
                            // for first row
                            Previous_TD = Current_td;
                            i = 1;
                        } else if (Current_td.text() == Previous_TD.text()) {
                            // the current td is identical to the previous row td
                            // remove the current td
                            Current_td.remove();
                            // increment the rowspan attribute of the first row td instance
                            Previous_TD.attr('rowspan', i + 1);
                            i = i + 1;
                        } else {
                            // means new value found in current td. So initialize counter variable i
                            Previous_TD = Current_td;
                            i = 1;
                        }
                    });
                });

                $('body').on('click', 'button.filter', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var akun = $('select[name="akun"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if (typeof bulan == 'underfined' || bulan == '' || bulan == null) {
                        window.notif('error', 'Silahkan pilih bulan terlebih dahulu');
                    } else {

                        var url =
                            '{{ route('report_bukubesar_filter', ['bulan' => ':bulan', 'tahun' => ':tahun', 'akun' => ':akun']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        url = url.replace(':akun', akun);
                        var a = document.createElement('a');
                        a.target = "_parent";
                        a.href = url;
                        a.click();
                    }
                });

                $('body').on('click', 'button.printNota', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var akun = $('select[name="akun"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if (typeof bulan == 'underfined' || bulan == '' || bulan == null) {
                        window.notif('error', 'Silahkan pilih bulan terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('report_bukubesar_print', ['bulan' => ':bulan', 'tahun' => ':tahun', 'akun' => ':akun']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        url = url.replace(':akun', akun);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }
                });

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
