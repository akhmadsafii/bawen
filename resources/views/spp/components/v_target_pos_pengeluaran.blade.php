@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Target</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pos Pengeluaran</li>
                <li class="breadcrumb-item "><a href="{{ route('target_pos') }}">Pos Pemasukan</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.popup show  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="showModalPosTarget" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Detail </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Kode </label>
                        <div class="col-md-9">
                            <input class="form-control" id="kode" name="kodex" placeholder="Kode" disabled="disabled"
                                type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nama </label>
                        <div class="col-md-9">
                            <input class="form-control" id="nama" name="namax" placeholder="Nama" disabled="disabled"
                                type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Target </label>
                        <div class="col-md-9">
                            <input class="form-control" id="targetx" name="targetx" placeholder="Target"
                                disabled="disabled" type="text" data-masked-input="Rp. 999,999,999" maxlength="19">
                        </div>
                    </div>
                    <div class="form-group row" style="display:none;">
                        <label class="col-md-3 col-form-label" for="l0">Realisasi </label>
                        <div class="col-md-9">
                            <input class="form-control" id="realisasix" name="realisasix" placeholder="realisasi"
                                disabled="disabled" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran </label>
                        <div class="col-md-9">
                            <input class="form-control" id="tahun_ajaranx" name="tahun_ajaranx"
                                placeholder="Tahun ajaran " disabled="disabled" type="text">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- /.popup edit  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="showModalPosTargetEdit" tabindex="-1"
        role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Edit </h5>
                </div>
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                    </div>
                    <div class="page-title-right d-inline-flex">
                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus
                            disi!.</p>
                    </div>
                </div>
                <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id_target" name="id_target">

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Kode </label>
                            <div class="col-md-9">
                                <input class="form-control" id="kode" name="kode" placeholder="Kode" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">ID </label>
                            <div class="col-md-9">
                                <input class="form-control" id="id_pos" name="id_pos" placeholder="ID POS" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Nama </label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama" name="nama" placeholder="Nama" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Target <span class="text-red">*</span>
                            </label>
                            <div class="col-md-9">
                                <input class="form-control" id="target" name="target" placeholder="Target" type="text"
                                    onkeyup="currencyFormat(this)">
                            </div>
                        </div>
                        <div class="form-group row" style="display:none;">
                            <label class="col-md-3 col-form-label" for="l0">Realisasi </label>
                            <div class="col-md-9">
                                <input class="form-control" id="realisasi" name="realisasi" placeholder="realisasi"
                                    type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran <span
                                    class="text-red">*</span> </label>
                            <div class="col-md-9">
                                <select class="form-control" id="tahun_ajaran" name="tahun_ajaran">
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                    @foreach ($tahun_ajaran as $key => $value)
                                        <option value="{{ $value['tahun_ajaran'] }}">{{ $value['tahun_ajaran'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- /.popup edit  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="showModalPosTargetEditReal" tabindex="-1"
        role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Edit Realisasi Anggaran </h5>
                </div>
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                    </div>
                    <div class="page-title-right d-inline-flex">
                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus
                            disi!.</p>
                    </div>
                </div>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id_target" name="id_target">

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Kode </label>
                            <div class="col-md-9">
                                <input class="form-control" id="kode" name="kode" placeholder="Kode" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">ID </label>
                            <div class="col-md-9">
                                <input class="form-control" id="id_pos" name="id_pos" placeholder="ID POS" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Nama </label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama" name="nama" placeholder="Nama" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran </label>
                            <div class="col-md-9">
                                <input class="form-control" id="tahun_ajaran" name="tahun_ajaran" placeholder="Tahun Ajaran" type="text"
                                    readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Realisasi </label>
                            <div class="col-md-9">
                                <input class="form-control" id="realisasi" name="realisasi" placeholder="realisasi"
                                    type="text" onkeyup="currencyFormat(this)">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update">
                            <i class="material-icons list-icon">save</i>
                            simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="box-title">Pos Pengeluaran </div>
                    <div class="widget-body clearfix">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Target</th>
                                    <th>Realisasi</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Target</th>
                                    <th>Realisasi</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNew'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_target_pengeluaran') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nama_pos',
                            name: 'nama_pos'
                        },
                        {
                            data: 'target',
                            name: 'target'
                        },
                        {
                            data: 'realisasi',
                            name: 'realisasi',
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);

                //create new

                $('body').on('click', '#createNew', function() {
                    window.location.href = '{{ route('create_target_pengeluaran') }}';
                });

                //reset modal
                $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //show data
                $('body').on('click', '.show.setting.pos.target', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_target_pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="namax"]').val(rows.nama_pos);
                                    $('input[name="kodex"]').val(rows.kode);
                                    $('input[name="targetx"]').val(rows.target);
                                    $('input[name="realisasix"]').val(rows.realisasi);
                                    $('input[name="tahun_ajaranx"]').val(rows.tahun_ajaran);
                                    $('#showModalPosTarget').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                //show data
                $('body').on('click', '.edit.setting.pos.target', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_target_pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_target"]').val(rows.id);
                                    $('input[name="id_pos"]').val(rows.id_pos);
                                    $('input[name="nama"]').val(rows.nama_pos);
                                    $('input[name="kode"]').val(rows.kode);
                                    $('input[name="target"]').val(rows.target.format());
                                    $('input[name="realisasi"]').val(rows.realisasi);
                                    $('input[name="tahun_ajaran"]').val(rows.tahun_ajaran);
                                    var tempx = new Array();
                                    tempx = rows.tahun_ajaran.split(",");

                                    var fldx = document.getElementById('tahun_ajaran');
                                    for (var i = 0; i < fldx.options.length; i++) {
                                        for (var j = 0; j < tempx.length; j++) {
                                            if (fldx.options[i].text == tempx[j]) fldx
                                                .options[i].selected = true;
                                        }
                                    }
                                    $('#showModalPosTargetEdit').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                }
                            }

                        }
                    });
                });

                //realisasi  data
                $('body').on('click', '.real.setting.pos.target', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_target_pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_target"]').val(rows.id);
                                    $('input[name="id_pos"]').val(rows.id_pos);
                                    $('input[name="nama"]').val(rows.nama_pos);
                                    $('input[name="kode"]').val(rows.kode);
                                    $('input[name="tahun_ajaran"]').val(rows.tahun_ajaran);
                                    if(rows.realisasi == null){
                                        $('input[name="realisasi"]').val('0');
                                    }else{
                                        $('input[name="realisasi"]').val(parseInt(rows.realisasi).format());
                                    }
                                    $('#showModalPosTargetEditReal').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                }
                            }

                        }
                    });
                });

                //update
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_target"]').val();
                    var urlx = '{{ route('store_target_realisasi_pengeluaran', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showModalPosTargetEditReal').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.real.setting.pos').html(
                                        '<i class="fa fa-check"></i>');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //update
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_target"]').val();
                    var urlx = '{{ route('store_target_update_pengeluaran', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showModalPosTargetEdit').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.edit.setting.pos').html(
                                        '<i class="fa fa-edit"></i>');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove  data
                $('body').on('click', '.remove.setting.pos.target', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy_target_pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });


                //remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

            });

        })(jQuery, window);
    </script>
@endsection
