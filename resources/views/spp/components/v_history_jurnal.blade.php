@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Histori</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Jurnal Umum</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <div class="table-responsive">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                 <td>Kode Transaksi</td>
                                                 <td>:</td>
                                                 <td><span class="kode_trans"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Lampiran Berkas</td>
                                                <td>:</td>
                                                <td><span class="lampiran-berkas"></span></td>
                                           </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                                <div class="tabs">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="nav-item" style="display:none;"><a class="nav-link " href="#home-tab2"
                                                data-toggle="tab" aria-expanded="true">Detail Item </a>
                                        </li>
                                        <li class="nav-item" aria-expanded="false"><a class="nav-link active"
                                                href="#jurnal-tab2" data-toggle="tab" aria-expanded="false">Detail
                                                Jurnal Item </a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-tabs -->
                                    <div class="tab-content">
                                        <div class="tab-pane " id="home-tab2" aria-expanded="true" style="display:none;">
                                            <input type="hidden" name="kode_trans" id="kode_trans" class="form-control">
                                            <div class="table-responsive">
                                                <table id="table_doc_item" class="table table-striped table-responsive"
                                                    width="100%">
                                                    <thead>
                                                        <tr class="table-info">
                                                            <th>No</th>
                                                            <th>Item</th>
                                                            <th>Kode Transaksi</th>
                                                            <th>Nominal</th>
                                                            <th>Tanggal</th>
                                                            <th>Keterangan </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- Append Create Datatables-->
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Item</th>
                                                            <th>Kode Transaksi</th>
                                                            <th>Nominal</th>
                                                            <th>Tanggal</th>
                                                            <th>Keterangan </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>

                                        </div>
                                        <div class="tab-pane active" id="jurnal-tab2" aria-expanded="false">
                                            <div class="table-responsive">
                                                <table id="table_doc_item_jurnal"
                                                    class="table table-striped table-responsive">
                                                    <thead>
                                                        <tr class="table-info">
                                                            <th>No</th>
                                                            <th>Akun</th>
                                                            <th>Tanggal</th>
                                                            <th>Debit</th>
                                                            <th>Kredit</th>
                                                            <th>Keterangan </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- Append Create Datatables-->
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Akun</th>
                                                            <th>Tanggal</th>
                                                            <th>Debit</th>
                                                            <th>Kredit</th>
                                                            <th>Keterangan </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.tabs -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="headersx">
                            <div class="form-inline">
                                <div class="form-group mr-2">
                                    <div class="input-group input-has-value">
                                        <input type="text" class="form-control datepicker" name="tgl_filter"
                                            data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                            value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                                <a class="btn btn-primary" href="javascript: void(0);" id="filter">Filter</a>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_doc" class="table table-striped table-responsive">
                                <thead>
                                    <tr class="table-info">
                                        <th>No</th>
                                        <th>Referensi</th>
                                        <th>Kode Transaksi</th>
                                        <th>Nominal</th>
                                        <th>Tanggal</th>
                                        <th>Tahun Ajaran </th>
                                        <th>Keterangan </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Append Create Datatables-->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Referensi</th>
                                        <th>Kode Transaksi</th>
                                        <th>Nominal</th>
                                        <th>Tanggal</th>
                                        <th>Tahun Ajaran </th>
                                        <th>Keterangan </th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_history_jurnal') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'no_referensi',
                            name: 'no_referensi'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar'
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);
                //show data
                $('body').on('click', '.show.history.akuntansi.jurnal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-transaksi-kode', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#printStruckModal').modal('show');
                                    $('input[name="kode_trans"]').val(id);
                                    $('span.kode_trans').html(id);
                                    $('.tabs a[href="#jurnal-tab2"]').trigger(
                                    'click'); // default active tabs
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                //action tabs active
                $('body').on('click', '.tabs a[href="#home-tab2"]', function() {
                    var kode = $('input[name="kode_trans"]').val();
                    var url = '{{ route('show_by_kode_ajax_detail', ':id') }}';
                    url = url.replace(':id', kode);
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                },
                                customize: function(doc) {
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join(
                                            '*').split(
                                            '');
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'kode_transaksi',
                                name: 'kode_transaksi',
                                visible: false,
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting = $('#table_doc_item').dataTable(config_table);
                });

                $('body').on('click', '.tabs a[href="#jurnal-tab2"]', function() {
                    var kode = $('input[name="kode_trans"]').val();
                    var url = '{{ route('detail-jurnal-by-kode', ':id') }}';
                    url = url.replace(':id', kode);

                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                },
                                customize: function(doc) {
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join(
                                            '*').split(
                                            '');
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'akun',
                                name: 'akun'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'debet',
                                name: 'debet'
                            },
                            {
                                data: 'kredit',
                                name: 'kredit'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting = $('#table_doc_item_jurnal').dataTable(config_table);
                });

                $('body').on('click', '#filter', function() {
                    var tanggal = $('input[name="tgl_filter"]').val();
                    var url = '{{ route('report_transaksijurnal_bydate', ':id') }}';
                    url = url.replace(':id', tanggal);

                    config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'no_referensi',
                            name: 'no_referensi'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar'
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);
                });

            });
        })(jQuery, window);
    </script>

@endsection
