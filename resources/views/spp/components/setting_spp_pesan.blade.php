@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pesan </li>
                <li class="breadcrumb-item "><a href="{{ route('setting-pos') }}">POS</a></li>
                <li class="breadcrumb-item "><a href="{{ route('setting-tagihan') }}">Tagihan</a></li>
                <li class="breadcrumb-item "><a href="{{ route('setting-template-tagihan') }}">Rekening</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- popup show detail  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" tabindex="-1" role="dialog"
        id="showModalPesanSetting" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Setting Pesan </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Kategori</label>
                        <div class="col-md-9">
                            <input class="form-control" id="kategorix" placeholder="Readonly" readonly="" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Perihal</label>
                        <div class="col-md-9">
                            <input class="form-control" id="perihalx" placeholder="Readonly" readonly="" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Salam Pembuka</label>
                        <div class="col-md-9">
                            <input class="form-control" id="salampembukax" placeholder="Readonly" readonly="" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Isi</label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="isix" rows="3" readonly=""></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Salam Penutup</label>
                        <div class="col-md-9">
                            <input class="form-control" id="salampenutupx" placeholder="Readonly" readonly="" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l11">Status</label>
                        <div class="col-md-9">
                            <input class="form-control" id="statusx" placeholder="Readonly" readonly="" type="text">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
     <!-- popup input  -->
     <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" tabindex="-1" role="dialog"
     id="addModalPesanSetting" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header text-inverse">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h5 class="modal-title" id="myLargeModalLabel">Tambah Setting Pesan </h5>
             </div>
             <div class="row page-title clearfix">
                <div class="page-title-left">
                </div>
                <div class="page-title-right d-inline-flex">
                    <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus
                        disi!.</p>
                </div>
            </div>
             <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
             <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="l11">Kode Pesan <span class="text-red">*</span></label>
                    <div class="col-md-9">
                        <input class="form-control" id="kode_pesan" name="kode_pesan" placeholder="Kode Pesan" type="text">
                    </div>
                </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l11">Kategori <span class="text-red">*</span></label>
                     <div class="col-md-9">
                        <select class="form-control" id="kategori" name="kategori">
                            <option value="" disabled="disabled" selected="selected">Pilih Kategori</option>
                            <option value="tagihan">Tagihan</option>
                            <option value="pemberitahuan">Pemberitahuan</option>
                        </select>
                     </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l11">Perihal <span class="text-red">*</span></label>
                     <div class="col-md-9">
                         <input class="form-control" id="perihal" name="perihal" placeholder="Perihal"  type="text">
                     </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l11">Salam Pembuka <span class="text-red">*</span></label>
                     <div class="col-md-9">
                         <input class="form-control" id="salampembuka" name="salampembuka" placeholder="Salam Pembuka"  type="text">
                     </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l11">Isi <span class="text-red">*</span></label>
                     <div class="col-md-9">
                         <textarea class="form-control" id="isi" rows="3" name="isi" ></textarea>
                     </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-md-3 col-form-label" for="l11">Salam Penutup <span class="text-red">*</span></label>
                     <div class="col-md-9">
                         <input class="form-control" id="salampenutup" name="salampenutup" placeholder="Salam Penutup" type="text">
                     </div>
                 </div>
                 <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="l11">Atas Nama <span class="text-red">*</span></label>
                    <div class="col-md-9">
                        <input class="form-control" id="atas_nama" name="atas_nama" placeholder="Atas Nama" type="text">
                    </div>
                </div>
             </div>
             <div class="modal-footer">
                <button type="submit" class="btn btn-info update">
                    <i class="material-icons list-icon">save</i>
                    Simpan
                </button>
                 <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                     this</button>
             </div>
             </form>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Perihal</th>
                                    <th>Salam Pembuka</th>
                                    <th>Isi</th>
                                    <th>Salam Penutup</th>
                                    <th>Atas Nama</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Perihal</th>
                                    <th>Salam Pembuka</th>
                                    <th>Isi</th>
                                    <th>Salam Penutup</th>
                                    <th>Atas Nama</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNew'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('datax-setting-pesan') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kategori',
                            name: 'kategori'
                        },
                        {
                            data: 'perihal',
                            name: 'perihal'
                        },
                        {
                            data: 'salam_pembuka',
                            name: 'salam_pembuka'
                        },
                        {
                            data: 'isi',
                            name: 'isi'
                        },
                        {
                            data: 'salam_penutup',
                            name: 'salam_penutup'
                        },
                        {
                            data: 'atas_nama',
                            name: 'atas_nama'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);
                $('body').on('click', '#createNew', function() {
                    $('#addModalPesanSetting').modal('show');
                });

                //show data
                $('body').on('click', '.show.setting.pesan', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_settingpesan', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#kategorix').val(rows.kategori);
                                    $('#perihalx').val(rows.perihal);
                                    $('#salampembukax').val(rows.salam_pembuka);
                                    $('textarea#isix').val(rows.isi);
                                    $('#salampenutupx').val(rows.salam_penutup);
                                    $('#showModalPesanSetting').modal('show');
                                    if (rows.status == 1) {
                                        $('#statusx').val('aktif');
                                    } else if (rows.status == 0) {
                                        $('#statusx').val('tidak Aktif');
                                    }
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                }
                            }

                        }
                    });
                });

                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_setting_pesan') }}';

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#addModalPesanSetting').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key] +' ';
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                 //reset modal
                 $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //remove  data
                $('body').on('click', '.remove.setting.pesan', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy_settingpesan', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                 //remove
                 window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
