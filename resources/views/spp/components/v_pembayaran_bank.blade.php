<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('spp.includes.head')
</head>

<body class="sidebar-horizontal header-centered">
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            @if (count($tagihan) > 0)
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-heading clearfix text-center">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger border-info mt-1" role="alert">
                                        <button aria-label="Close" class="close" data-dismiss="alert"
                                            type="button"><span aria-hidden="true">×</span>
                                        </button>
                                        <div class="widget-list">
                                            <div class="col-md-12 widget-holder">
                                                <div class="widget-body clearfix">
                                                    <div class="row">
                                                        <i class="material-icons list-icon md-48">warning</i>
                                                        <ul class="mr-t-10">
                                                            @foreach ($errors->all() as $error)
                                                                <li class="text-red">{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-error border-error" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div class="widget-list">
                                            <div class="col-md-12 widget-holder">
                                                <div class="widget-body clearfix">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <!-- /.widget-heading -->
                            <div class="widget-body clearfix">
                                @if ($message = Session::get('success'))
                                    <div class="body-bg-full profile-page">
                                        <div id="wrapper" class="row wrapper">
                                            <div
                                                class="col-10 ml-sm-auto col-sm-6 col-md-6 ml-md-auto login-center login-center-mini mx-auto">
                                                <div class="navbar-header text-center mb-2">
                                                    <i class="material-icons list-icon md-48">done</i>
                                                </div>
                                                <br>
                                                <p class="text-center text-muted text-success mt-3">
                                                    {{ $message }} </p>
                                                <br><a href="{{ route('sumbangan-spp') }}"
                                                    class="btn btn-block btn-primary ripple mr-tb-30">Kembali ke Halaman
                                                    Utama </a>
                                            </div>
                                            <!-- /.login-center -->
                                        </div>
                                    </div>
                                    <script>
                                        (function($, global) {
                                            "use-strict"

                                            function my_onkeydown_handler(event) {
                                                switch (event.keyCode) {
                                                    case 116: // 'F5'
                                                        event.preventDefault();
                                                        event.keyCode = 0;
                                                        window.status = "F5 disabled";
                                                        break;
                                                }
                                            }
                                            document.addEventListener("keydown", my_onkeydown_handler);

                                        })(jQuery, window);
                                    </script>
                                @else
                                    <p class="text-info text-center">Pembayaran Tagihan Metode Transfer Bank / Upload
                                        Bukti
                                        Transfer
                                    </p>
                                    <hr>
                                    <div class="row">
                                        <div
                                            class="col-md-6  ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center mx-auto">
                                            @if (!empty(session('id')) || !empty(session('id_kelas_siswa')))
                                                <form id="DocForm" name="DocForm" method="POST"
                                                    action="{{ route('store_upload') }}" class="form-horizontal"
                                                    enctype="multipart/form-data">
                                                @else
                                                    <form id="DocForm" name="DocForm" method="POST"
                                                        action="{{ route('store_upload_public') }}"
                                                        class="form-horizontal" enctype="multipart/form-data">
                                            @endif
                                            @csrf
                                            <input class="form-control" id="kode" name="kode"
                                                placeholder="Kode Transaksi" type="hidden"
                                                value="{{ old('kode') ?? ($tagihan['kode'] ?? '') }}">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Bank"
                                                type="hidden"
                                                value="{{ old('nama') ?? ($rekening['nama_bank'] ?? '') }}">
                                            <input class="form-control" id="no_rekening" name="no_rekening"
                                                placeholder="Rekening tujuan " type="hidden"
                                                value="{{ old('no_rekening') ?? ($rekening['rekening_bank'] ?? '') }}">

                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3" class="text-info">Informasi Data Siswa</td>
                                                    </tr>
                                                    <tr>
                                                        <td> Nama Siswa</td>
                                                        <td>:</td>
                                                        <td>{{ Str::ucfirst($tagihan['nama']) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>NIS</td>
                                                        <td>:</td>
                                                        <td>{{ $tagihan['nis'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>{{ $tagihan['rombel'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>{{ $tagihan['jurusan'] }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Tagihan yang harus dibayarkan</td>
                                                        <td>:</td>
                                                        <td>{{ number_format($tagihan['nominal'], 0) }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="3" class="text-info">Informasi Data Bank
                                                            Tujuan</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Nama Bank</td>
                                                        <td>:</td>
                                                        <td>{{ $rekening['nama_bank'] ?? '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No.Rekening</td>
                                                        <td>:</td>
                                                        <td>{{ old('no_rekening') ?? ($rekening['rekening_bank'] ?? '') }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Atas Nama (Pemilik Rekening)</td>
                                                        <td>:</td>
                                                        <td>{{ old('atas_nama') ?? ($rekening['atas_nama'] ?? '') }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Tanggal Transfer</td>
                                                        <td>:</td>
                                                        <td>
                                                            <div class="input-group input-has-value">
                                                                <input type="text" id="tanggal_bayar"
                                                                    name="tanggal_bayar"
                                                                    value="{{ old('tanggal_bayar') ?? \Carbon\Carbon::now()->format('Y-m-d') }}"
                                                                    readonly="readonly" class="form-control datepicker"
                                                                    data-date-format="yyyy-mm-dd"
                                                                    data-plugin-options='{"autoclose": true}'>
                                                                <span class="input-group-addon"><i
                                                                        class="list-icon material-icons">date_range</i></span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bukti Transfer <span class="text-red">(*)</td>
                                                        <td>:</td>
                                                        <td><input id="image" type="file" name="image"
                                                                accept="image/*,doc,docx,pdf"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="text-right">
                                                            @if ($message = Session::get('success'))
                                                                <a href="{{ route('sumbangan-spp') }}"
                                                                    class="btn btn-info">Kembali</a>
                                                            @else
                                                                <a href="{{ route('pembayaran-konfirmasi', ['id' => $tagihan['kode']]) }}"
                                                                    class="btn btn-info">Metode Lain</a>
                                                            @endif
                                                        </td>
                                                        <td> <button type="submit" class="btn btn-primary">
                                                                <i class="material-icons list-icon">send</i>
                                                                Kirim
                                                            </button></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                            </form>

                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            @else
                <div class="row">
                    <div class="col-md-12 widget-holder text-center">
                        <div class="widget-bg">
                            <div class="page-title">
                                <h1>Data Tidak ditemukan</h1>
                            </div>
                            <p class="mr-t-10 mr-b-20">Kode Transaksi tidak valid!</p><a
                                href="javascript: history.back();"
                                class="btn btn-info btn-lg btn-rounded mr-b-20 ripple">Go Back</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @include('spp.includes.foot')
</body>

</html>
