@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Tambah Target POS Pengeluaran </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('store_target_post_pengeluaran') }}"
                            class="form-horizontal" enctype="multipart/form-data">

                            @csrf

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0"> POS <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <select class="form-control" id="id_pos" name="id_pos">
                                        <option disabled="disabled" selected="true" value="">Pilih POS </option>
                                        @foreach ($pos as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['nama'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Kode </label>
                                <div class="col-md-9">
                                    <input class="form-control" id="kode" name="kode" placeholder="Kode" type="text"
                                        readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Target <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <input class="form-control" id="target" name="target" placeholder="Target" type="text"
                                        onkeyup="currencyFormat(this)">
                                </div>
                            </div>
                            <div class="form-group row" style="display:none;">
                                <label class="col-md-3 col-form-label" for="l0">Realisasi </label>
                                <div class="col-md-9">
                                    <input class="form-control" id="realisasi" name="realisasi" placeholder="realisasi"
                                        type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran <span
                                        class="text-red">*</span> </label>
                                <div class="col-md-9">
                                    <select class="form-control" id="tahun_ajaran" name="tahun_ajaran">
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun_ajaran as $key => $value)
                                            @php
                                                $explode_tahun = explode('/', $value['tahun_ajaran']);
                                            @endphp
                                            @if ($explode_tahun[0] == session('tahun'))
                                                <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                    {{ $value['tahun_ajaran'] }}
                                                </option>
                                            @else
                                                <option value="{{ $value['tahun_ajaran'] }}">
                                                    {{ $value['tahun_ajaran'] }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('target_pos_pengeluaran') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('body').on('change', '#id_pos', function() {
                    var id = $(this).val();
                    var url = '{{ route('ajax_pengeluaran', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {

                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('.pemasukan').show();
                                    $('input[name="kode"]').val(rows.kode);
                                }
                            }

                        }
                    });
                });

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

            });

        })(jQuery, window);
    </script>
@endsection
