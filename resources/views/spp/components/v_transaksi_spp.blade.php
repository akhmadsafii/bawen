@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable.dx:first .add-clone-form {
            display: none !important;
        }

        .clonable.dx:last-child .add-clone-form {
            display: inline-block !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

        .modal:nth-of-type(even) {
            z-index: 1062 !important;
        }

        .modal-backdrop.show:nth-of-type(even) {
            z-index: 1061 !important;
        }

        td.details-control {
            cursor: pointer;
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_open.png') no-repeat center center;
        }

        tr.shown td.details-control {
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_close.png') no-repeat center center;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"><small> Pembayaran SPP</small></h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Tagihan</li>
                <li class="breadcrumb-item " style="display:none;"><a href="{{ route('potongan-spp') }}">Keringanan</a>
                </li>
                <li class="breadcrumb-item "><a href="{{ route('konfirmasi-transaksi') }}">Konfirmasi</a></li>
                <li class="breadcrumb-item "><a href="{{ route('history-transaksi-pemasukan') }}">Histori</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.modal -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="transaksiModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myTransaksi">Transaksi</h5>
                </div>
                <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="inline-block user--online thumb-lg photo">
                                    <img id="photox" class="rounded-circle img-thumbnail" alt=""
                                        style="margin-left: 20px;margin-top: -14px;">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <input type="hidden" class="form-control is-valid" id="id_siswa" name="id_siswa">
                                <input type="hidden" class="form-control is-valid" id="id_kelas" name="id_kelas">
                                <input type="hidden" class="form-control is-valid" id="nis" name="nis">
                                <table class="table" width="100%">
                                    <tbody>
                                        <tr width="100%;">
                                            <td>NIS</td>
                                            <td>:</td>
                                            <td><span class="nisn_modal"></span></td>
                                        </tr>
                                        <tr width="100%;">
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><span class="nama_modal"></span></td>
                                        </tr>
                                        <tr width="100%;">
                                            <td>Rombongan Belajar</td>
                                            <td>:</td>
                                            <td><span class="rombel_modal"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Jurusan</td>
                                            <td>:</td>
                                            <td><span class="jurusan_modal"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-top: 1px;">
                            <div class="col-md-4">
                                <label>Tahun Ajaran <span class="text-red">*</span></label>
                                <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                    @foreach ($tahun_ajaran as $key => $value)
                                        @php
                                            $explode_tahun = explode('/', $value['tahun_ajaran']);
                                        @endphp
                                        @if ($explode_tahun[0] == session('tahun'))
                                            <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                        @else
                                            <option value="{{ $value['tahun_ajaran'] }}">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Tanggal Bayar <span class="text-red">*</span></label>
                                <div class="input-group input-has-value">
                                    <input type="text" class="form-control datepicker" name="tgl_bayar"
                                        data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"> <span
                                        class="input-group-addon"><i class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="note text-red"></div>
                        <br>
                        <div class="form-append" style="margin-top: -49px;"></div>
                        <hr>
                        <div class="row mr-b-50 mt-2">
                            <div class="col-md-6 mb-10 input-has-value">
                                <label>Metode Bayar <span class="text-red">*</span></label>
                                <select class="form-control" id="metode_bayar" name="metode_bayar" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Metode Bayar </option>
                                    @foreach ($rekening_akun as $rekening)
                                        @if ($rekening['id'] != '15' && $rekening['id'] != '16')
                                            <option value="{{ $rekening['id'] }}">
                                                {{ $rekening['kode'] }} -
                                                {{ $rekening['nama'] }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 mb-10 input-has-value">
                                <label>Keterangan <span class="text-red">*</span></label>
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="1"></textarea>
                            </div>
                            <div class="col-md-6 mb-10 input-has-value" style="display:none;">
                                <label>Jenis Transaksi</label>
                                <select class="form-control" id="jenis_transaksi" name="jenis_transaksi" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Jenis Transaksi </option>
                                    <option value="pemasukan"> Pemasukan </option>
                                    <option value="pengeluaran" disabled="disabled">Pengeluaran</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="row mr-b-2">
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Bayar <span class="text-red">*</span></label>
                                <input type="text" class="form-control is-valid" id="bayar" name="bayar"
                                    onkeyup="currencyFormat(this)" placeholder="Bayar" required="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Kembalian</label>
                                <input type="text" class="form-control is-valid" id="kembalian" name="kembalian"
                                    placeholder="Kembalian" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update pos_transaction">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- list tagihan -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="listTagihanModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headerlistTagihan">List Tagihan </h5>
                </div>
                <div class="modal-body">

                    <table class="table table-bordered table-striped table-responsive" id="head_list">
                        <thead>
                            <tr class="table-info">
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>NISN</th>
                                <th>NIS</th>
                                <th> Rombongan Belajar </th>
                            </tr>
                        </thead>
                        <tbody>
                            <td><span class="text-nama"></span></td>
                            <td><span class="text-kelas"></span></td>
                            <td><span class="text-jurusan"></span></td>
                            <td><span class="text-nisn"></span></td>
                            <td><span class="text-nisn"></span></td>
                            <td><span class="text-rombel"></span></td>
                        <tbody>
                    </table>

                    <div class="card card-outline-info mt-3">
                        <div class="card-header">
                            <h6 class="card-subtitle">Rincian Tagihan </h6>
                        </div>
                        <div class="card-body">
                            <div class="custom-scroll-content scrollbar-enabled">
                                <div class="table-list-tagihan"></div>
                                <div class="table-pembayaran-terakhir"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-info ripple text-left printNota"><i
                            class="fa fa-print list-icon"></i></a>
                    <a href="javascript:void(0)" id="edit-transaction" class="btn btn-warning  ripple text-left"
                        data-dismiss="modal" style="display:none;"><i class="fa fa-edit list-icon"></i></a>
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- print struk Edit  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModalEdit" tabindex="-1"
        role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Form Edit Transaksi</h5>
                </div>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" class="form-control is-valid" id="id_trans" name="id_trans">
                        <div class="row print-dataedit">
                        </div>
                        <div class="row mr-b-2">
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Bayar</label>
                                <input type="text" class="form-control is-valid" id="bayar" name="bayar"
                                    onkeyup="currencyFormat(this)" placeholder="Bayar" required="">
                            </div>
                            <div class="col-md-6 mb-3 input-has-value">
                                <label>Kembalian</label>
                                <input type="text" class="form-control is-valid" id="kembalian" name="kembalian"
                                    placeholder="Kembalian" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update pos_transaction">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- filter search custom   -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="filterCustom" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Filter Custom Search </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l8">NIS (Siswa)</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input class="form-control" name="nis_search" id="l8" placeholder="Search NIS ..."
                                    type="text"> <span class="input-group-btn"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tahun<span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <!--select class="form-control" id="tahun_ajaranx" name="tahun_ajaranx" required="">
                                                        <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                                        @foreach ($tahun_ajaran as $key => $value)
    @php
    $explode_tahun = explode('/', $value['tahun_ajaran']);
    @endphp
                                                            @if ($explode_tahun[0] == session('tahun'))
    <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                                    {{ $value['tahun_ajaran'] }}
                                                                </option>
@else
    <option value="{{ $value['tahun_ajaran'] }}">
                                                                    {{ $value['tahun_ajaran'] }}
                                                                </option>
    @endif
    @endforeach
                                                    </select-->
                            <select class="form-control" id="tahun_ajaranx" name="tahun_ajaranx" required="">
                                @if (empty(session('tahun')))
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun
                                    </option>
                                @endif

                                @foreach ($tahunlist as $tahunx)
                                    @if ($tahun == $tahunx)
                                        <option value="{{ $tahunx }}" selected="selected">
                                            {{ $tahunx }}
                                        </option>
                                    @else
                                        <option value="{{ $tahunx }}">{{ $tahunx }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <center><a class="btn btn-success text-center" href="javascript: void(0);" id="search_nis"> <i
                                class="fa fa-search"></i>
                        </a></center>
                    <br>
                    <hr>
                    <div class="result_filter"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- rirwayat transaksi  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="riwayatTransaksi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Riwayat Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data">
                    </div>
                    <div class="table-responsive">
                        <table id="table_riwayat" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Jenis</th>
                                    <th>Total</th>
                                    <th>Tanggal</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode </th>
                                    <th>Jenis</th>
                                    <th>Total</th>
                                    <th>Tanggal </th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-rounded ripple text-left transaksix"
                        data-dismiss="modal">
                        Transaksi Lainnya </button>
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- rirwayat transaksi  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="riwayatTransaksi2" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Riwayat Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data2">
                    </div>
                    <div class="table-responsive">
                        <table id="table_riwayat2" class="table table-striped table-responsive" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Jenis</th>
                                    <th>Total</th>
                                    <th>Tanggal</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode </th>
                                    <th>Jenis</th>
                                    <th>Total</th>
                                    <th>Tanggal </th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-rounded ripple text-left transaksix" data-dismiss="modal"
                        style="display:none;">
                        Transaksi Lainnya </button>
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- riwayat detail transaksi -->
    <div class="modal" id="modaldetailRiwayat" style="z-index: 99999;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Riwayat Transaksi</h4>
                    <button type="button" class="close detailriwayat" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="container"></div>
                <div class="modal-body">
                    <div class="row print-datax"></div>
                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn detailriwayat">Close</a>
                </div>
            </div>
        </div>
    </div>

    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <center>
                            <div class="row">
                                <!--div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Tahun Ajaran</label>
                                                                        <select class="form-control tahunAjaran_filter" id="tahun_ajaran" name="tahun_ajaran">
                                                                            <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                                                            @foreach ($tahun_ajaran as $key => $value)
    <option value="{{ $value['tahun_ajaran'] }}">
                                                                                    {{ $value['tahun_ajaran'] }}
                                                                                </option>
    @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div-->
                                <div class="col-md-3">
                                    <label>Tahun</label>
                                    <select class="form-control tahunAjaran_filter" id="tahun_ajaran" name="tahun_ajaran"
                                        required="">
                                        @if (empty(session('tahun')))
                                            <option disabled="disabled" selected="true" value="">Pilih Tahun
                                            </option>
                                        @endif

                                        @foreach ($tahunlist as $tahunx)
                                            @if ($tahun == $tahunx)
                                                <option value="{{ $tahunx }}" selected="selected">
                                                    {{ $tahunx }}
                                                </option>
                                            @else
                                                <option value="{{ $tahunx }}">{{ $tahunx }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kelas</label>
                                        <select class="form-control kelas_filter" id="kelas_filter" name="kelas_filter">
                                            <option disabled="disabled" selected="true" value="">Pilih Kelas </option>
                                            @foreach ($kelas as $key => $value)
                                                <option value="{{ $value['id'] }}">
                                                    {{ $value['nama'] }} - {{ $value['jurusan'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </center>
                        <hr>
                    </div>
                    <div class="widget-body clearfix">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Photo</th>
                                    <th>NIS</th>
                                    <th>NISN</th>
                                    <th>Nama Siswa</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Tahun</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Photo</th>
                                    <th>NIS</th>
                                    <th>NISN</th>
                                    <th>Nama Siswa</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            let counter = $('input.nominal').length + 1;
            const monthNames = ["Januari", "Februari", "Maret",
                "April", "Mei", "Juni",
                "Juli", "Agustus", "September", "Oktober",
                "November", "Desember"
            ];

            const d = new Date();

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                window.detailTable = function detail(d) {
                    var data = `
                       <div class="box-title text-center">Detail Biodata Siswa </div>
                       <div class="row">
                         <div class="col-mo-6"><span class="ml-3">` + d.photo + `</span></div>
                         <div class="col-mo-6">
                           <div class="table-responsive ml-3">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                               <tr>
                                  <td>Nama</td>
                                  <td>` + d.nama + `</td>
                                  <td>NISN</td>
                                  <td>` + d.nisn + `</td>
                                  <td>Tahun</td>
                                  <td>` + d.tahun + `</td>
                                  <td>Jenis Kelamin </td>
                                  <td>` + d.jenkel + `</td>
                               </tr>
                               <tr>
                                  <td>Tanggal Lahir</td>
                                  <td>` + d.tgl_lahir + `</td>
                                  <td>Tempat Lahir</td>
                                  <td>` + d.tempat_lahir + `</td>
                                  <td>Alamat Tinggal </td>
                                  <td>` + d.alamat + `</td>
                               </tr>
                               <tr>
                                  <td>Kelas</td>
                                  <td>` + d.kelas + `</td>
                                  <td>NIS</td>
                                  <td>` + d.nis + `</td>
                                  <td>Tahun Angkatan</td>
                                  <td>` + d.tahun_angkatan + `</td>
                               </tr>
                               <tr>
                                  <td>Jurusan</td>
                                  <td>` + d.jurusan + `</td>
                                  <td>Agama</td>
                                  <td>` + d.agama + `</td>
                                  <td>Telepone</td>
                                  <td>` + d.telepon + `</td>
                               </tr>
                               <tr>
                                  <td>Rombel</td>
                                  <td>` + d.rombel + `</td>
                                  <td>Email</td>
                                  <td>` + d.email + `</td>
                                  <td>Wali Kelas</td>
                                  <td>` + parseBlankValue(d.wali_kelas) + `</td>
                               </tr>
                            </table>
                           </div>
                         </div>
                       </div>
                    `;
                    return data;
                }

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            },
                        },
                        {
                            text: '<i class="fa fa-filter"></i>',
                            attr: {
                                title: 'Filter',
                                id: 'filter'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-siswa') }}",
                    columns: [{
                            "class": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'photo',
                            name: 'photo'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'jurusan',
                            name: 'jurusan'
                        },
                        {
                            data: 'tahun',
                            name: 'tahun'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);

                $('#table_doc tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).parents('tr');
                    var row = table_setting.api().row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(detailTable(row.data())).show();
                        tr.addClass('shown');
                    }
                });

                //create tagihan
                $('body').on('click', '.create.tagihan', function() {
                    var id = $(this).data('id');
                    var photo = $(this).data('photo');
                    var url = '{{ route('show_tagihan_bulan', ':id') }}';
                    url = url.replace(':id', id);
                    let formappend = "";
                    let list_tagihan = "";
                    let printdata = "";
                    let list_bulan = '';

                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                var rows = JSON.parse(JSON
                                    .stringify(
                                        result['data']));

                                list_bulan = ['-',
                                    'Juli', 'Agustus', 'September',
                                    'Oktober', 'November', 'Desember',
                                    'Januari', 'Februari', 'Maret',
                                    'April', 'Mei', 'Juni'
                                ];

                                //filter bulan ini tagihan
                                var $pembayaran_check = [];
                                rows.tagihan.forEach((element, index) => {
                                    var x_bulan = element['id_bulan'];
                                    if (list_bulan[element['id_bulan']] ==
                                        monthNames[d.getMonth()]) {
                                        $pembayaran_check.push(element);
                                    }
                                });

                                /*$('input[name="id_siswa"]').val(rows.id);
                                $('input[name="kelas"]').val(rows.kelas);
                                $('input[name="jurusan"]').val(rows.jurusan);
                                $('input[name="rombel"]').val(rows.rombel);
                                $('input[name="nis"]').val(rows.nis);
                                $('input[name="nisn"]').val(rows.nis);
                                $('input[name="nama"]').val(rows.nama);*/

                                $('span.nisn_modal').html(rows.nis);
                                $('input[name="nis"]').val(rows.nis);
                                $('span.nama_modal').html(rows.nama.ucwordsFormat());
                                $('span.rombel_modal').html(rows.rombel);
                                $('span.jurusan_modal').html(rows.jurusan);
                                $('input[name="kelas"]').val(rows.kelas);
                                $('input[name="id_siswa"]').val(rows.id);

                                $('.form-append').html('');

                                if ($pembayaran_check.length == 0) {
                                    $(loader).html('<i class="fa fa-plus"></i>');
                                    window.notif('info', 'Tidak ada tagihan ' + monthNames[d
                                            .getMonth()] + ' untuk  ' +
                                        rows
                                        .nama);
                                    //redirect show riwayat transaksi
                                    swal({
                                        title: "Informasi Bulan " + monthNames[d
                                                .getMonth()] +
                                            " Tidak ada tagihan ",
                                        text: "ingin Melihat Riwayat Transaksi " +
                                            rows.nama + " !",
                                        type: "info",
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Lihat Riwayat !',
                                        cancelButtonText: 'Transaksi Lainnya !',
                                        confirmButtonClass: 'btn btn-success',
                                        cancelButtonClass: 'btn btn-info',
                                        buttonsStyling: false
                                    }).then(function() {
                                        window.riwayat_transaksi(id, rows);
                                    }, function(dismiss) {
                                        if (dismiss === 'cancel') {

                                            $('input[name="bayar"]').val('');
                                            $('input[name="kembalian"]').val('');
                                            $('textarea#keterangan').val('');

                                            window.getTransaksiX(id, photo);
                                        }
                                    });

                                } else {
                                    $('#myTransaksi').html('Transaksi Tagihan Bulan ' +
                                        monthNames[d.getMonth()]);
                                    $('#transaksiModal').modal('show');
                                    $('input[name="bayar"]').val('');
                                    $('input[name="kembalian"]').val('');
                                    $('textarea#keterangan').val('');

                                    //photo
                                    if (photo == null || typeof photo == 'underfined') {
                                        //$('figure.thumb-lg.photo').hide();
                                        $('#photox').removeAttr('src');
                                        $('#photox').attr('src',
                                            'https://via.placeholder.com/150');
                                    } else {
                                        //$('figure.thumb-lg.photo').show();
                                        $('#photox').removeAttr('src');
                                        $('#photox').attr('src', photo);
                                    }

                                    var periodx = '';

                                    var total_parent = parseInt(rows.tagihan.length) +
                                        1;
                                    var nominal_parsex = 0;
                                    rows.tagihan.forEach((element, index) => {
                                        var x_tagihan = element['id_tagihan'];
                                        var x_bulan = element['id_bulan'];
                                        var x_nama = element['nama'];

                                        list_tagihan = list_tagihan +
                                            `<option value="` +
                                            x_tagihan + `">` + x_nama +
                                            `</option>`;

                                        if (list_bulan[element['id_bulan']] ==
                                            monthNames[d.getMonth()]) {

                                            if (element['priode'] != 'bulanan') {
                                                periodx = `<input type="hidden" class="form-control " id="bulan" name="bulan[]"
                                                            placeholder="bulan" value="` + element['id_bulan'] +
                                                    `"> `;
                                            } else {
                                                periodx = `<input type="hidden" class="form-control " id="bulan" name="bulan[]"
                                                            placeholder="bulan" value="` + element['id_bulan'] +
                                                    `"> ` + list_bulan[element[
                                                        'id_bulan']];
                                            }

                                            printdata = printdata +
                                                `<tr class="clonable dx " parentId="` +
                                                index + `">
                                                <td>
                                                <input type="hidden" class="form-control " id="id_tagihan" name="id_tagihan[]"
                                                            placeholder="id_tagihan" value="` + element['id_tagihan'] +
                                                `">  ` + element['nama'] + `
                                                </td>
                                                <td>` + element['priode'] + `</td>
                                                <td>
                                                    ` + periodx + `
                                                </td>
                                                <td><input name="nominal[]" type="text" id="nominal" class="form-control" required="required" onkeyup="currencyFormat(this)" value="` +
                                                parseInt(element['ditagih'])
                                                .format() + `"></td>
                                                <td class="btn-group text-center" role="group" width="100%">
                                                    <button type="button" name="remove" id="removex" class="btn btn-danger remove-clone remove"><i class="fa fa-remove"></i></button> &nbsp;
                                                    <button type="button" name="add" id="addx_form" class="btn btn-info add-clone-form add " style="display:none;"><i class="fa fa-plus"></i></button>
                                                </td>
                                                `;

                                            nominal_parsex += parseInt(element[
                                                'nominal']);

                                        }
                                    });

                                    setTimeout(() => {
                                        var clonexD = $('.dx');
                                        clonexD.each(function(el, index) {
                                            const lastItem = clonexD[
                                                clonexD.length - 1];
                                            var lastKEy = $(lastItem)
                                                .attr('parentId');

                                            $('tr[parentId="' +
                                                lastKEy +
                                                '"] button.add-clone-form'
                                            ).show();
                                        });
                                    }, 100);

                                    formappend = `<br><table class="table table-hover" style="display:block;">
                                    <legend> <span class="ml-3">List Tagihan</span> <span class="text-red">*</span> </legend>
                                    <thead>
                                        <tr>
                                            <td>Tagihan</td>
                                            <td>Periode</td>
                                            <td>Bulan</td>
                                            <td>Nominal</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ` + printdata + `
                                        <tr class="clonable form-clone " parentId="` + total_parent + `" style="display:none;">
                                        <td>
                                            <select class="form-control id_tagihan" name="id_tagihan[]" id="id_tagihan" required="required">
                                                <option disabled="disabled" selected="true" value="">Pilih Tagihan</option>
                                                ` + list_tagihan + `
                                            </select>
                                        </td>
                                        <td width="20%"><input name="periode[]" type="text" id="periode" class="form-control" readonly=""></td>
                                        <td id="modifBulan">
                                            <select class="form-control bulan" name="bulan[]" id="bulan" required="required">
                                                <option disabled="disabled" selected="true" value="">Pilih Bulan</option>
                                                <option value="1">Juli</option>
                                                <option value="2">Agustus</option>
                                                <option value="3">September</option>
                                                <option value="4">Oktober</option>
                                                <option value="5">November</option>
                                                <option value="6">Desember</option>
                                                <option value="7">Januari</option>
                                                <option value="8">Februari</option>
                                                <option value="9">Maret</option>
                                                <option value="10">April</option>
                                                <option value="11">Mei</option>
                                                <option value="12">Juni</option>
                                            </select>
                                        </td>
                                        <td><input name="nominal[]" type="text" id="nominal" class="form-control" required="" onkeyup="currencyFormat(this)"></td>
                                        <td class="btn-group text-center" role="group" width="100%">
                                            <button type="button" name="remove" id="removex" class="btn btn-danger remove-clone remove"><i class="fa fa-remove"></i></button> &nbsp;
                                            <button type="button" name="add" id="addx" class="btn btn-info add-clone add"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                    </table>`;

                                    formappend = formappend +
                                        '<div style="margin-top:0px;"> <span class="ml-3">Total Tagihan :</span> <span class="total_tagihan" data-nominal="0">0</span></div>'

                                    $('.form-append').html(formappend);

                                    //set bulan ini
                                    /*$("select[name='bulan[]'] option").filter(
                                        function() {
                                            return $(this).text() == monthNames[d
                                                .getMonth()];
                                        }).prop("selected", true);*/

                                    //calc
                                    $('span.total_tagihan').html("Rp." + nominal_parsex
                                        .format());
                                    $('span.total_tagihan').attr('data-nominal',
                                        nominal_parsex);
                                    $(loader).html('<i class="fa fa-plus"></i>');

                                }
                            }
                        }
                    });

                });

                //show riwayat transaksi
                $('body').on('click', '.more.transaction', function() {
                    var id = $(this).data('id');
                    var photo = $(this).data('photo');

                    $('input[name="bayar"]').val('');
                    $('input[name="kembalian"]').val('');
                    $('select#metode_bayar').prop('selectedIndex', 0);
                    $('select#jenis_transaksi').prop('selectedIndex', 0);
                    $('textarea#keterangan').val('');

                    $(this).html('<i class="fa fa-spin fa-spinner"><i>');
                    setTimeout(() => {
                        window.getTransaksiX(id, photo);
                        $(this).html('<i class="fa fa-fire"><i> Transaksi');
                    }, 1000);
                });

                //show riwayat transaksi
                $('body').on('click', '.show.riwayatx', function() {
                    var id = $(this).data('id');
                    var cdata = $(this);
                    let formappend = "";
                    let list_tagihan = "";
                    let printdata = "";
                    let list_bulan = '';
                    let rows = '';
                    const loader = $(this);

                    var url = '{{ route('ajax_siswa_id', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            loader.html('<i class="fa fa-spin fa-spinner"><i>');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    loader.html('<i class="fa fa-eye"></i> Histori');

                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    window.riwayat_transaksix(rows, cdata);

                                } else if (result['info'] == 'error') {
                                    loader.html('<i class="fa fa-eye"></i> Histori');
                                    window.notif('error', result['message']);
                                }
                            }
                        }
                    });

                });

                //show clone form
                $('body').on('click', 'button.add-clone-form', function() {
                    $(this).hide();
                    $('.clonable.form-clone').show();
                    var clonned = $(this).parents('.clonable.form-clone:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    $(this).hide();

                    $('.clonable').parents('tbody').append(clonned);
                    /*$("select[name='bulan[]'] option").filter(
                        function() {
                            return $(this).text() == monthNames[d.getMonth()];
                        }).prop("selected", true);*/

                });

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    $(this).hide();

                    $('.clonable').parents('tbody').append(clonned);

                    /*$("select[name='bulan[]'] option").filter(
                        function() {
                            return $(this).text() == monthNames[d.getMonth()];
                        }).prop("selected", true);*/
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                    var nominal_parse = 0;

                    $('input[name="nominal[]"]').each(function() {
                        var valx = $(this).val();
                        if (valx != '') {
                            nominal_parse += parseInt(valx.split(',').join(''));
                        }
                    });

                    if (!isNaN(nominal_parse)) {
                        $('span.total_tagihan').html("Rp." + nominal_parse.format());
                        $('span.total_tagihan').attr('data-nominal', nominal_parse);
                        $('button.update.pos_transaction').prop("disabled", true);
                    }

                    var indexClone = $('input[name="nominal[]"]').length;
                    if (indexClone == 1) {
                        $('.clonable.form-clone').show();
                    }
                });

                //calc bayar atau kembalian
                $('body').on('keyup', 'input[name="bayar"]', function() {
                    var xv = $(this).val().split(',').join('');
                    var nominal = $('span.total_tagihan').attr('data-nominal');
                    var kembalian = parseInt(xv) - parseInt(nominal);
                    $('input[name="kembalian"]').val(kembalian.format());
                    var $check_list_tagihan = $('input[name="nominal[]"]').val();
                    if ($check_list_tagihan == '') {
                        $('.note').html('List Tagihan pilih dahulu !');
                    } else {
                        if (!isNaN(parseInt(xv))) {
                            if (kembalian > 0) {
                                $('.note').html('');
                                $('button.update.pos_transaction').prop("disabled", false);
                            } else if (kembalian == 0) {
                                $('.note').html('');
                                $('button.update.pos_transaction').prop("disabled", false);
                            } else {
                                $('.note').html('Nominal Bayar Kurang !');
                                $('button.update.pos_transaction').prop("disabled", true);
                            }
                        } else {
                            $('input[name="kembalian"]').val('');
                        }
                    }
                });


                //keyup nominal manual calc
                $("body").on('keyup', 'input[name="nominal[]"]', function() {
                    var parentId = $(this).parents('.clonable').attr('parentId');
                    var xv = parseInt($(this).val().split(',').join(''));
                    // calc total
                    if (isNaN(xv) || xv == 0) {
                        $('span.total_tagihan').html("Rp. 0");
                        $('span.total_tagihan').attr('data-nominal', '0');
                        window.notif('error', 'Nominal Harus di isi ');
                    } else {
                        $('span.total_tagihan').html("Rp." + xv
                            .format());
                        $('span.total_tagihan').attr('data-nominal',
                            xv);
                    }
                });

                //select tagihan
                $('body').on('change', 'select.id_tagihan', function() {
                    var x = $(this).val();
                    var parentId = $(this).parents('.clonable').attr('parentId');
                    var url = '{{ route('get-tagihan-id', ':id') }}';
                    url = url.replace(':id', x);
                    if (x) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON
                                            .stringify(
                                                result['data']));
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="nominal[]"]').val(rows
                                            .nominal.format());

                                        $('tr[parentId="' + parentId +
                                            '"] input[name="periode[]"]').val(rows
                                            .periode);
                                        //set bulan ini

                                        if (rows.periode != 'bulanan') {

                                            $('tr[parentId="' + parentId +
                                                '"] select[name="bulan[]"]').hide();

                                        } else {
                                            $('tr[parentId="' + parentId +
                                                '"] select[name="bulan[]"]').show();
                                        }

                                        // calc total
                                        var nominal_parse = 0;
                                        $('input[name="nominal[]"]').each(function() {
                                            var valx = $(this).val();
                                            nominal_parse += parseInt(valx.split(
                                                ',').join(''));
                                        });
                                        $('span.total_tagihan').html("Rp." + nominal_parse
                                            .format());
                                        $('span.total_tagihan').attr('data-nominal',
                                            nominal_parse);
                                    }
                                }
                            }
                        });
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').val('');
                        $('tr[parentId="' + parentId + '"] input[name="periode[]"]').val('');
                    }
                });

                //submit update
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var bayar = $('input[name="bayar"]').val();
                    var note = $('textarea[name="keterangan"]').val();
                    if (bayar == '' || bayar == null) {
                        window.notif('warning', 'Nominal Bayar tidak boleh kosong!');
                    } else {
                        if (parseInt(bayar) == 0) {
                            window.notif('warning', 'Nominal Bayar tidak boleh nol !');
                        } else {
                            if (note == '' || note == null) {
                                window.notif('warning',
                                    'Keterangan Harus Diisi untuk keperluan catatan jurnal !');
                            } else {
                                var urlx = '{{ route('store-transaksi') }}';
                                var formData = new FormData(this);
                                const loader = $('button.update');
                                $.ajax({
                                    type: "POST",
                                    url: urlx,
                                    beforeSend: function() {
                                        $(loader).html(
                                            '<i class="fa fa-spin fa-spinner"></i> Loading'
                                        );
                                    },
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function(result) {
                                        if (typeof result['data'] !== 'underfined' &&
                                            typeof result[
                                                'info'] !== 'underfined') {
                                            if (result['info'] == 'success') {
                                                $('#transaksiModal').modal('hide');
                                                table_setting.fnDraw(false);
                                                $(loader).html(
                                                    '<i class="fa fa-save"></i> Simpan');
                                                window.notif(result['info'], result[
                                                    'message']);

                                                var rows = JSON.parse(JSON.stringify(result[
                                                    'data']));
                                                var bayar = result['bayar'];
                                                var kembalian = result['kembalian'];

                                                setTimeout(() => {
                                                    window.previewx(rows.id, bayar,
                                                        kembalian);
                                                }, 500);

                                            } else if (result['info'] == 'error') {
                                                $(loader).html(
                                                    '<i class="fa fa-save"></i> Simpan');
                                                window.notif(result['info'], result[
                                                    'message']);
                                                $('#transaksiModal').modal('hide');
                                                table_setting.fnDraw(false);
                                            }
                                        }
                                    },
                                    error: function(data) {
                                        let log = "";
                                        if (typeof data !== 'underfined') {
                                            let item = data['responseJSON']['errors'];
                                            for (var key in item) {
                                                //console.log(item[key])
                                                log = log + item[key];
                                            }
                                            window.notif('error', log);
                                            $(loader).html(
                                                '<i class="fa fa-save"></i> Update');
                                            $('#transaksiModal').modal('hide');
                                            table_setting.fnDraw(false);
                                        }
                                    }
                                });
                            }

                        }
                    }
                });

                //submit
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_trans"]').val();

                    var url = '{{ route('update-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $('#printStruckModalEdit').modal('hide');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    var bayar = result['bayar'];
                                    var kembalian = result['kembalian'];

                                    setTimeout(() => {
                                        window.previewx(rows.id, bayar, kembalian);
                                    }, 500);

                                } else if (result['info'] == 'error') {

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //transaksi other
                window.getTransaksiX = function transX(id, photo) {
                    var url = '{{ route('ajax_tagihan_kelas', ':id') }}';
                    url = url.replace(':id', id);
                    let formappend = "";
                    let list_tagihan = "";

                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_siswa"]').val(rows.id);

                                    //photo
                                    if (photo == null || typeof photo == 'underfined') {
                                        //$('figure.thumb-lg.photo').hide();
                                        $('#photox').removeAttr('src');
                                        $('#photox').attr('src',
                                            'https://via.placeholder.com/150');
                                    } else {
                                        //$('figure.thumb-lg.photo').show();
                                        $('#photox').removeAttr('src');
                                        $('#photox').attr('src', photo);
                                    }

                                    /*$('input[name="kelas"]').val(rows.kelas);
                                    $('input[name="jurusan"]').val(rows.jurusan);
                                    $('input[name="rombel"]').val(rows.rombel);
                                    $('input[name="nis"]').val(rows.nis);
                                    $('input[name="nisn"]').val(rows.nis);
                                    $('input[name="nama"]').val(rows.nama);*/

                                    $('span.nisn_modal').html(rows.nis);
                                    $('input[name="nis"]').val(rows.nis);
                                    $('span.nama_modal').html(rows.nama.ucwordsFormat());
                                    $('span.rombel_modal').html(rows.rombel);
                                    $('span.jurusan_modal').html(rows.jurusan);
                                    $('input[name="kelas"]').val(rows.kelas);


                                    $('.form-append').html('');

                                    $('#myTransaksi').html('Transaksi Pembayaran Tagihan');

                                    $('#transaksiModal').modal('show');

                                    rows.tagihan.forEach(element => {
                                        var id_tagihan = element['id'];
                                        var name_tagihan = element['nama'];
                                        list_tagihan = list_tagihan +
                                            `<option value="` +
                                            id_tagihan + `">` + name_tagihan +
                                            `</option>`;
                                    });

                                    formappend = `<br><table class="table table-hover" width="100%">
                                        <legend><span class="ml-3">List Tagihan</span> <span class="text-red">*</span> </legend>
                                        <thead>
                                            <tr>
                                                <td>Tagihan</td>
                                                <td>Periode</td>
                                                <td>Bulan</td>
                                                <td>Nominal</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="clonable" parentId="1">
                                                <td>
                                                    <select class="form-control id_tagihan" name="id_tagihan[]" id="id_tagihan" required>
                                                        <option disabled="disabled" selected="true" value="">Pilih Tagihan</option>
                                                        ` + list_tagihan + `
                                                    </select>
                                                </td>
                                                <td width="20%"><input name="periode[]" type="text" id="periode" class="form-control" readonly=""></td>
                                                <td id="modifBulan">
                                                    <select class="form-control bulan" name="bulan[]" id="bulan" required="required">
                                                        <option disabled="disabled" selected="true" value="">Pilih Bulan</option>
                                                        <option value="1">Juli</option>
                                                        <option value="2">Agustus</option>
                                                        <option value="3">September</option>
                                                        <option value="4">Oktober</option>
                                                        <option value="5">November</option>
                                                        <option value="6">Desember</option>
                                                        <option value="7">Januari</option>
                                                        <option value="8">Februari</option>
                                                        <option value="9">Maret</option>
                                                        <option value="10">April</option>
                                                        <option value="11">Mei</option>
                                                        <option value="12">Juni</option>
                                                    </select>
                                                </td>
                                                <td><input name="nominal[]" type="text" id="nominal" class="form-control" required=""></td>
                                                <td class="btn-group text-center" role="group" width="100%">
                                                    <button type="button" name="remove" id="removex" class="btn btn-danger remove-clone remove"><i class="fa fa-remove"></i></button> &nbsp;
                                                    <button type="button" name="add" id="addx" class="btn btn-info add-clone add"><i class="fa fa-plus"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        </table>`;

                                    formappend = formappend +
                                        '<div style="margin-top:0px;"> <span class="ml-3">Total Tagihan :</span> <span class="total_tagihan" data-nominal="0">0</span></div>'

                                    $('.form-append').html(formappend);

                                    //set bulan ini
                                    /*$("select[name='bulan[]'] option").filter(
                                        function() {
                                            return $(this).text() == monthNames[d
                                            .getMonth()];
                                        }).prop("selected", true);*/


                                    $(loader).html('<i class="fa fa-plus"></i>');
                                } else {
                                    $(loader).html('<i class="fa fa-plus"></i>');
                                }
                            }

                        }
                    });

                }

                //print view
                window.previewx = function getid(id, bayar, kembalian) {
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    var catatan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    if (rows.keterangan == null) {
                                        catatan = '-'
                                    } else {
                                        catatan = rows.keterangan;
                                    }


                                    $('#printStruckModal').modal('show');
                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"> <span class="ml-2">Informasi Siswa </span> </h5>
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Transaksi </span> </h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keterangan</td>
                                                        <td>:</td>
                                                        <td>` + catatan + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata + `<div class="col-md-12">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info"><span class="ml-2"> Informasi Pembayaran </span></h5>
                                           <table class="table">
                                            <thead>
                                                 <tr class="alert-success">
                                                    <td>No</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                        `;

                                    list_bulan = ['-',
                                        'Juli', 'Agustus', 'September',
                                        'Oktober', 'November', 'Desember',
                                        'Januari', 'Februari', 'Maret',
                                        'April', 'Mei', 'Juni'
                                    ];

                                    var index_number = 0;
                                    rows.tagihan.forEach(element => {
                                        index_number++;
                                        printdata = printdata + `
                                                <tr>
                                                    <td>` + index_number + `</td>
                                                    <td>` + element['nama_tagihan'] + `</td>
                                                    <td>` + element['nominal'].format() + `</td>
                                                </tr>
                                            `;
                                    });


                                    printdata = printdata + `</table>`;
                                    printdata = printdata + `<div class="table-info table-responsive">
                                                <table>
                                                <tbody>
                                                    <tr>
                                                        <td><span class="ml-3">Bayar</span></td>
                                                        <td>:</td>
                                                        <td colspan="2"><small><b>` + bayar + `</b></small></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="ml-3">Kembalian</span></td>
                                                        <td>:</td>
                                                        <td colspan="2"><small><b>` + kembalian + `</b></small></td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                               </div>`;

                                    $('.print-data').html(printdata);

                                    $('#edit-transaction').attr('data-id', id);
                                    $('#edit-transaction').attr('data-bayar', bayar);
                                    $('#edit-transaction').attr('data-kembalian', kembalian);
                                    $('.printNota').attr('data-id', id);

                                    $('#edit-transaction').html('<i class="fa fa-edit"></i>');

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                }
                            }

                        }
                    });
                }

                //edit transaction
                $('body').on('click', '#edit-transaction', function() {
                    var id = $(this).data('id');
                    var bayar = $('#edit-transaction').data('bayar');
                    var kembalian = $('#edit-transaction').data('kembalian');
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    let list_tagihan = '';
                    let id_tagihanx = '';
                    let name_bulanx = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('#printStruckModalEdit').modal('show');
                                    $('input[name="id_trans"]').val(id);
                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Siswa </span> </h5>
                                            <input type="hidden" class="form-control is-valid" id="nisn" name="nisn"
                                            placeholder="Nisn" value="` + rows.nis + `">
                                            <input type="hidden" class="form-control is-valid" id="id_siswa" name="id_siswa"
                                            placeholder="id diswa " value="` + rows.id_kelas_siswa + `">
                                            <input type="hidden" class="form-control is-valid" id="tgl_bayar" name="tgl_bayar"
                                            placeholder="tgl_bayar" value="` + rows.tgl_bayar + `">
                                            <input type="hidden" class="form-control is-valid" id="tahun_ajaran" name="tahun_ajaran"
                                            placeholder="tahun_ajaran" value="` + rows.tahun_ajaran + `">
                                            <input type="hidden" class="form-control is-valid" id="bayar" name="bayar"
                                            placeholder="bayar" value="` + bayar + `">
                                            <input type="hidden" class="form-control is-valid" id="kembalian" name="kembalian"
                                            placeholder="bayar" value="` + kembalian + `">
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Transaksi </span></h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata + `<div class="col-md-12">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info"><span class="ml-2">Informasi Pembayaran </span></h5>
                                           <table class="table">
                                            <thead>
                                                 <tr>
                                                    <td>#</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Bulan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        `;

                                    list_bulan = ['-',
                                        'Juli', 'Agustus', 'September',
                                        'Oktober', 'November', 'Desember',
                                        'Januari', 'Februari', 'Maret',
                                        'April', 'Mei', 'Juni'
                                    ];

                                    rows.tagihan.forEach(element => {

                                        printdata = printdata + `
                                                <tr>
                                                    <td>#<input type="hidden" class="form-control is-valid" id="id_detail" name="id_detail[]"
                                                        placeholder="id" value="` + element['id'] + `"></td>
                                                    <td>
                                                        <input type="hidden" class="form-control is-valid" id="id_tagihan" name="id_tagihan[]"
                                                        placeholder="id_tagihan" value="` + element['id_tagihan'] +
                                            `">  ` + element['nama_tagihan'] + `
                                                    </td>
                                                    <td>
                                                        <input type="hidden" class="form-control is-valid" id="bulan" name="bulan[]"
                                                        placeholder="bulan" value="` + element['bulan'] + `"> ` +
                                            list_bulan[element['bulan']] +
                                            `
                                                    </td>
                                                    <td> <input name="nominal[]" type="text" id="nominal" class="form-control" onkeyup="currencyFormat(this)" value="` +
                                            element['nominal'].format() + `"></td>
                                                </tr>
                                            `;


                                    });

                                    printdata = printdata + `</tbody></table></div>`;

                                    $('.print-dataedit').html(printdata);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                //print nota
                $('body').on('click', '.printNota', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('print_nota', ':id') }}';
                    url = url.replace(':id', id);
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = url;
                    a.click();
                });

                //filter data
                $('body').on('click', '#filter', function() {
                    $('#filterCustom').modal('show');
                });

                //filter by nis siswa
                $('body').on('click', 'a#search_nis', function() {
                    let printdata = '';
                    let list_bulan = '';
                    var search_nis = $('input[name="nis_search"]').val();
                    //var tahun_ajaran = $('select[name="tahun_ajaranx"] option:selected').val().split('/');
                    var tahun_ajaran = $('select[name="tahun_ajaranx"] option:selected').val();
                    const loader = $('.result_filter');
                    if (search_nis == null || search_nis == '' && tahun_ajaran != '') {
                        window.notif('error', 'Form Search NIS  Harus di isi!');
                    } else if (tahun_ajaran == '') {
                        window.notif('error', 'Pilih Tahun Ajaran Terlebih dahulu!');
                    } else {
                        var url =
                            "{{ route('ajax_siswa_search', ['id' => ':id', 'tahun' => ':tahun']) }}";
                        url = url.replace(':id', search_nis);
                        url = url.replace(':tahun', tahun_ajaran);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                                loader.html('<i class="fa fa-spin fa-spinner"><i>');
                            },
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        if (result['data'].length == 0) {
                                            loader.html('');
                                            window.notif('error', ' Data ' + search_nis +
                                                ' tidak  ditemukan ');
                                        } else {
                                            var rows = JSON.parse(JSON
                                                .stringify(
                                                    result['data']));
                                            var rows_profil = JSON.parse(JSON
                                                .stringify(
                                                    result['profil'][0]));

                                            printdata = printdata +
                                                `<div class="col-md-5 float-right"><a  class="btn btn-primary btn-sm create tagihan text-center " data-nis="` +
                                                rows.nis + `"  data-id="` + rows_profil.id +
                                                `" data-id_kelas="` + rows_profil.id_kelas +
                                                `" data-nama_kelas="` + rows.kelas +
                                                `"  data-jurusan="` + rows.jurusan +
                                                `" style="color: #fff" data-dismiss="modal" ><i class="fa fa-plus"></i> Buat Transaksi </a></div>`;

                                            printdata = printdata + `
                                            <div class="col-md-5">
                                                    <h5 class="box-title table-info"> <span class="ml-3"> Informasi Siswa </span> </h5>
                                                    <table class="table" width="100%">
                                                        <tbody>
                                                            <tr width="100%">
                                                                <td>Nama</td>
                                                                <td>:</td>
                                                                <td>` + ucwordx(rows.nama) + `</td>
                                                            </tr>
                                                            <tr width="100%">
                                                                <td>Kelas</td>
                                                                <td>:</td>
                                                                <td>` + rows.kelas + `</td>
                                                            </tr>
                                                            <tr width="100%">
                                                                <td>Rombel</td>
                                                                <td>:</td>
                                                                <td>` + rows.rombel + `</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jurusan</td>
                                                                <td>:</td>
                                                                <td>` + rows.jurusan + `</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>`;

                                            printdata = printdata +
                                                `<div class="col-md-12">`;
                                            printdata = printdata + `
                                                    <h5 class="box-title table-info"> <span class="ml-3">Informasi Pembayaran </span></h5>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <td>#</td>
                                                                <td>Nama Tagihan</td>
                                                                <td>Periode</td>
                                                                <td>Tahun Ajaran</td>
                                                                <td>Terakhir Pembayaran</td>
                                                                <td>Nominal</td>
                                                                <td>Total</td>
                                                            </tr>
                                                        </thead>
                                                    `;

                                            rows.tagihan.forEach(element => {
                                                var xnominal = '';
                                                var xtotal = '';
                                                if (isNaN(parseInt(parseBlankValue(
                                                        element['nominal'])))) {
                                                    xnominal = 0;
                                                } else {
                                                    xnominal = element['nominal']
                                                        .format();
                                                }

                                                if (isNaN(parseInt(parseBlankValue(
                                                        element[
                                                            'total_dibayar']
                                                    )))) {
                                                    xtotal = 0;
                                                } else {
                                                    xtotal = element[
                                                            'total_dibayar']
                                                        .format();
                                                }

                                                printdata = printdata + `
                                                            <tr>
                                                                <td>#</td>
                                                                <td>` + parseBlankValue(element['nama']) + `</td>
                                                                <td>` + parseBlankValue(element['priode']) + `</td>
                                                                <td>` + parseBlankValue(element['tahun_ajaran']) + `</td>
                                                                <td>` + parseBlankValue(element['terakhir_bayar']) + `</td>
                                                                <td>` + xnominal + `</td>
                                                                <td>` + xtotal + `</td>
                                                            </tr>
                                                        `;
                                            });

                                            printdata = printdata + `</table>`;

                                            $('.result_filter').html(printdata);

                                        }
                                    } else if (result['info'] == 'error') {
                                        loader.html('');
                                        window.notif('error', ' Data ' + search_nis +
                                            ' tidak  ditemukan ');
                                    }
                                }
                            }
                        });
                    }
                });

                //filter by tahun ajaran
                $('body').on('change', 'select.tahunAjaran_filter', function() {
                    var valx = $(this).val();
                    var urlx = '{{ route('ajax_data_siswatahunx', ':tahun') }}';
                    urlx = urlx.replace(':tahun', valx);

                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                },
                            },
                            {
                                text: '<i class="fa fa-filter"></i>',
                                attr: {
                                    title: 'Filter',
                                    id: 'filter'
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                "class": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": ''
                            },
                            {
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'nis',
                                name: 'nis'
                            },
                            {
                                data: 'nisn',
                                name: 'nisn'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'rombel',
                                name: 'rombel'
                            },
                            {
                                data: 'jurusan',
                                name: 'jurusan'
                            },
                            {
                                data: 'tahun',
                                name: 'tahun'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_setting = $('#table_doc').dataTable(config_table);

                });


                //filter by kelas
                $('body').on('change', 'select.kelas_filter', function() {
                    //var tahunajaran = $('select.tahunAjaran_filter option:selected').val().split('/');
                    var tahunajaranx = $('select.tahunAjaran_filter option:selected').val();
                    var xv = $(this).val();
                    if (tahunajaranx == '') {
                        window.notif('error', 'Pilih Tahun Ajaran terlebih dahulu');
                    } else {
                        var urlxt =
                            "{{ route('ajax_siswa_searchx', ['id' => ':id', 'tahun' => ':tahun']) }}";
                        urlxt = urlxt.replace(':id', xv);
                        urlxt = urlxt.replace(':tahun', tahunajaranx);

                        var config_table = {
                            destroy: true,
                            dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                            buttons: [{
                                    text: '<i class="fa fa-refresh"></i>',
                                    action: function(e, dt, node, config) {
                                        dt.ajax.reload(null, false);
                                    },
                                },
                                {
                                    text: '<i class="fa fa-filter"></i>',
                                    attr: {
                                        title: 'Filter',
                                        id: 'filter'
                                    }
                                },
                                'colvis',
                            ],
                            processing: true,
                            serverSide: true,
                            responsive: true,
                            ajax: urlxt,
                            columns: [{
                                    "class": 'details-control',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                {
                                    data: 'DT_RowIndex',
                                    name: 'DT_RowIndex'
                                },
                                {
                                    data: 'photo',
                                    name: 'photo'
                                },
                                {
                                    data: 'nis',
                                    name: 'nis'
                                },
                                {
                                    data: 'nisn',
                                    name: 'nisn'
                                },
                                {
                                    data: 'nama',
                                    name: 'nama'
                                },
                                {
                                    data: 'rombel',
                                    name: 'rombel'
                                },
                                {
                                    data: 'jurusan',
                                    name: 'jurusan'
                                },
                                {
                                    data: 'tahun',
                                    name: 'tahun'
                                },
                                {
                                    data: 'action',
                                    name: 'action',
                                    orderable: false,
                                    searchable: false
                                }
                            ],
                        };

                        table_setting = $('#table_doc').dataTable(config_table);

                    }

                });

                //riwayat transaksi
                window.riwayat_transaksi = function getriwayat(id, rowsx) {

                    $('.print-data').html('');

                    var printdata = "";
                    $('#riwayatTransaksi').modal('show');
                    printdata = printdata + `
                                       <div class="col-md-12">
                                            <h5 class="box-title table-info"> <span class="ml-2">Informasi Siswa </span> </h5>
                                            <div class="row">
                                                <div class="col-md-4">
                                                        <figure class="inline-block user--online thumb-lg photo">
                                                            <img id="photox2" class="rounded-circle img-thumbnail" alt=""
                                                                style="margin-left: 20px;margin-top: -5px;">
                                                        </figure>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr width="100%;">
                                                                <td>Nama</td>
                                                                <td>:</td>
                                                                <td>` + ucwordx(rowsx.nama) + `</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Kelas</td>
                                                                <td>:</td>
                                                                <td>` + rowsx.kelas + `</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Rombel</td>
                                                                <td>:</td>
                                                                <td>` + rowsx.rombel + `</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jurusan</td>
                                                                <td>:</td>
                                                                <td>` + rowsx.jurusan + `</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Tahun Ajaran</td>
                                                                <td>:</td>
                                                                <td>` + parseBlankValue(rowsx.tahun_ajaran) + `</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>`;

                    $('.print-data').html(printdata);

                    //photo
                    if (rowsx.photo == null || typeof rowsx.photo == 'underfined') {
                        //$('figure.thumb-lg.photo').hide();
                        $('#photox2').removeAttr('src');
                        $('#photox2').attr('src',
                            'https://via.placeholder.com/150');
                    } else {
                        //$('figure.thumb-lg.photo').show();
                        $('#photox2').removeAttr('src');
                        $('#photox2').attr('src', photo);
                    }

                    $('button.transaksix').attr('data-id', id);

                    var urlsx = "{{ route('get_riwayat_transaksi', ':id') }}";
                    urlsx = urlsx.replace(':id', id);

                    var config_tablex = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlsx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'kode',
                                name: 'kode'
                            },
                            {
                                data: 'bentuk',
                                name: 'bentuk'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'tahun_ajaran',
                                name: 'tahun_ajaran'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    var table_settingx = $('#table_riwayat').dataTable(config_tablex);
                }

                //lihat riwayat transaksi
                window.riwayat_transaksix = function riwayat(rows, cdata) {

                    var idx = cdata.data('id');
                    var printdata = '';

                    $('.print-data2').html('');
                    $('#riwayatTransaksi2').modal('show');

                    printdata = printdata + `
                                       <div class="col-md-12">
                                            <h5 class="box-title table-info"> <span class="ml-2">Informasi Siswa </span> </h5>
                                            <div class="row">
                                                <div class="col-md-4">
                                                        <figure class="inline-block user--online thumb-lg photo">
                                                            <img id="photox2" class="rounded-circle img-thumbnail" alt=""
                                                                style="margin-left: 20px;margin-top: -5px;">
                                                        </figure>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table">
                                                    <tbody>
                                                        <tr width="100%;">
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>` + ucwordx(cdata.data('nama')) + `</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>NIS</td>
                                                            <td>:</td>
                                                            <td>` + cdata.data('nis') + `</td>
                                                        </tr>
                                                        <tr width="100%;">
                                                            <td>Kelas</td>
                                                            <td>:</td>
                                                            <td>` + cdata.data('nama_kelas') + `</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jurusan</td>
                                                            <td>:</td>
                                                            <td>` + cdata.data('jurusan') + `</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>`;

                    $('.print-data2').html(printdata);

                    $('button.transaksix').attr('data-id', idx);

                    //console.log(cdata.data('photo'));

                    //photo
                    if (cdata.data('photo') == null || typeof cdata.data('photo') == 'underfined') {
                        //$('figure.thumb-lg.photo').hide();
                        $('#photox2').removeAttr('src');
                        $('#photox2').attr('src',
                            'https://via.placeholder.com/150');
                    } else {
                        //$('figure.thumb-lg.photo').show();
                        $('#photox2').removeAttr('src');
                        $('#photox2').attr('src', cdata.data('photo'));
                    }


                    var urlsx = "{{ route('get_riwayat_transaksi', ':id') }}";
                    urlsx = urlsx.replace(':id', idx);

                    var config_tablex = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlsx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'kode',
                                name: 'kode'
                            },
                            {
                                data: 'bentuk',
                                name: 'bentuk',
                                visible: false
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'tahun_ajaran',
                                name: 'tahun_ajaran'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    var table_settingx = $('#table_riwayat2').dataTable(config_tablex)
                }

                //transaksi lainnya
                $('body').on('click', 'button.transaksix', function() {
                    var id = $(this).data('id');
                    var photo = $(this).data('photo');
                    window.getTransaksiX(id, photo);
                });

                //print riwayat
                $('body').on('click', '.print.riwayat', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('print_nota', ':id') }}';
                    url = url.replace(':id', id);
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = url;
                    a.click();
                });

                //reset modal
                $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //lihat transaksi lainnya
                $('body').on('click', '.detailriwayat', function() {
                    var id = $(this).data('id');
                    var cdata = $(this);
                    let formappend = "";
                    let list_tagihan = "";
                    let printdata = "";
                    let list_bulan = '';
                    let rows = '';
                    const loader = $(this);

                    var url = '{{ route('ajax_siswa_id', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            loader.html('<i class="fa fa-spin fa-spinner"><i>');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    loader.html('<i class="fa fa-eye"></i> Histori');

                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    window.riwayat_transaksix(rows, cdata);

                                } else if (result['info'] == 'error') {
                                    loader.html('<i class="fa fa-eye"></i> Histori');
                                    window.notif('error', result['message']);
                                }
                            }
                        }
                    });
                });

                //show data
                $('body').on('click', '.show.riwayat', function() {
                    var id = $(this).data('id');
                    var cdata = $(this);
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    var catatan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    if (rows.keterangan == null) {
                                        catatan = '-'
                                    } else {
                                        catatan = rows.keterangan;
                                    }

                                    $('#riwayatTransaksi2').modal('hide');

                                    $('#modaldetailRiwayat').modal('show');
                                    $('.detailriwayat').attr('data-id', cdata.data(
                                        'id_kelas_siswa'));
                                    $('.detailriwayat').attr('data-nama', cdata.data(
                                        'nama'));
                                    $('.detailriwayat').attr('data-nis', cdata.data('nis'));
                                    $('.detailriwayat').attr('data-jurusan', cdata.data(
                                        'jurusan'));
                                    $('.detailriwayat').attr('data-nama_kelas', cdata.data(
                                        'nama_kelas'));

                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"> <span class="ml-2">Informasi Siswa</span> </h5>
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"> <span class="ml-2"> Informasi Transaksi</span> </h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <!--tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr-->
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keterangan</td>
                                                        <td>:</td>
                                                        <td>` + catatan + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata + `<div class="col-md-12">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info"> <span class="ml-2">Informasi Pembayaran</span></h5>
                                           <table class="table">
                                            <thead>
                                                 <tr class="table-warning">
                                                    <td>No</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                        `;
                                    var indexnumber = 0;
                                    rows.tagihan.forEach(element => {
                                        indexnumber++;
                                        printdata = printdata + `
                                                <tr>
                                                    <td>` + indexnumber + `</td>
                                                    <td>` + element['nama_tagihan'] + `</td>
                                                    <td>` + element['nominal'].format() + `</td>
                                                </tr>
                                            `;
                                    });
                                    printdata = printdata + `</table>`;

                                    $('.print-datax').html(printdata);

                                    $('.printNota').attr('data-id', rows.id);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });


                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

                String.prototype.ucwordsFormat = function() {
                    return this.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                }

            });

        })(jQuery, window);

        //currensy
        /*Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };*/
        Number.prototype.format = function(d, w, s, c) {
            var re = '\\d(?=(\\d{' + (w || 3) + '})+' + (d > 0 ? '\\b' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~d));
            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };

    </script>
@endsection
