<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            margin: 0;
            padding: 0
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                /* DIN A4 standard, Europe */
                margin: 0;
            }

            html,
            body {
                width: 210mm;
                /* height: 297mm; */
                height: 282mm;
                font-size: 11px;
                background: #FFF;
                overflow: visible;
            }

            body {
                padding-top: 15mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center>
        @if (!empty(session('sekolah')))
            <b>{{ session('sekolah') }}</b>
            <br>
            <br>
        @endif
        <b>Buku Besar</b>
    </center>
    <br>
    <hr>
    <br>
    <table width="100%">
        <tbody>
            @if (count($report) > 0)
                @php
                    $current = current($report['akuns']);
                @endphp
                <tr>
                    <td class="text-right">Kode</td>
                    <td>:</td>
                    <td>{{ $current['kode_akun'] }}</td>
                    <td class="text-right">Bulan</td>
                    <td>:</td>
                    <td>{{ $bulan }}</td>
                </tr>
                <tr>
                    <td class="text-right">Nama Akun</td>
                    <td>:</td>
                    <td>{{ $current['nama_akun'] }}</td>
                    <td class="text-right">Tahun</td>
                    <td>:</td>
                    <td>{{ $tahun }}</td>
                </tr>
            @endif
        </tbody>
    </table>
    <br>
    <div class="table-responsive">
        @php
            $total_debit = 0;
            $total_kredit = 0;
            $row_count = 0;
            $saldo = 0;
        @endphp
        <table id="table_doc" class="table table-striped" data-toggle="table" width="100%;" border="1">
            <thead class="table-info">
                <tr>
                    <td class="text-center" style="text-align: center; vertical-align: middle;">
                        Tanggal</td>
                    <td class="text-center" style="text-align: center; vertical-align: middle;">
                        Deskripsi</td>
                    <td class="text-center" style="text-align: center; vertical-align: middle;">Debit
                    </td>
                    <td class="text-center" style="text-align: center; vertical-align: middle;">
                        Kredit</td>
                    <td class="text-center" style="text-align: center; vertical-align: middle;">Saldo
                    </td>
                </tr>
            </thead>
            <tbody>
                @php
                    $saldo_awal = 0;
                    $total_saldo = 0;
                @endphp

                @if (count($report) > 0)
                    @php
                        $saldo_awal = intval($report['saldo_awal']);
                    @endphp
                    <tr>
                        <td></td>
                        <td>Saldo Awal </td>
                        <td>-</td>
                        <td>-</td>
                        <td class="text-left">
                            <span class="ml-3">{{ number_format($saldo_awal, 0) }}</span>
                        </td>
                    </tr>
                    @foreach ($report['akuns'] as $reportx)
                        @php
                            $total_debit += intval($reportx['debet']);
                            $total_kredit += intval($reportx['kredit']);
                            if ($reportx['akun_kategori'] == '2' || $reportx['akun_kategori'] =='3') {
                                $saldo = $saldo + (intval($reportx['kredit']) - intval($reportx['debet']));
                                $total_saldo = intval($saldo_awal) + $saldo;
                            } else {
                                $saldo = $saldo + (intval($reportx['debet']) - intval($reportx['kredit']));
                                $total_saldo = intval($saldo_awal) + $saldo;
                            }
                            $row_count++;
                        @endphp
                        <tr>
                            <td>
                                {{ \Carbon\Carbon::parse($reportx['tgl_bayar'])->isoFormat('dddd, D MMMM Y ') }}
                            </td>
                            @if (!empty($reportx['keterangan']))
                                <td>{{ $reportx['keterangan'] }}</td>
                            @else
                                <td>-</td>
                            @endif

                            @if ($reportx['debet'] != 0)
                                <td class="text-left">
                                    <span class="ml-3">{{ number_format($reportx['debet'], 0) }}</span>
                                </td>
                            @else
                                <td>0</td>
                            @endif

                            @if ($reportx['kredit'] != 0)
                                <td class="text-left">
                                    <span class="ml-3">{{ number_format($reportx['kredit'], 0) }}</span>
                                </td>
                            @else
                                <td>0</td>
                            @endif

                            <td class="text-left">
                                <span class="ml-3">{{ number_format($total_saldo, 0) }}</span>
                            </td>
                        </tr>
                        @if ($row_count % 2 == 0)
                            <tr>
                                <td colspan="5">
                                    <div class="table-info" style="height:10px;"></div>
                                </td>
                            <tr>
                        @endif
                    @endforeach

                @else
                    <tr>
                        <td colspan="5" class="text-center text-red">Belum Ada Catatan </td>
                    <tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2" class="text-right">Jumlah</td>
                    <td>Rp.{{ number_format($total_debit, 0) }}</td>
                    <td>Rp.{{ number_format($total_kredit, 0) }}</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

</body>

</html>
