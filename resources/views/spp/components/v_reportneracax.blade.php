@php
$saldo_awal_modal = 0;
$saldo_akun_aktiva_lancar = 0;
$jumlah_saldo_akun_aktiva_lancar = 0;
$saldo_akun_aktiva_tetap = 0;
$jumlah_saldo_akun_aktiva_tetap = 0;

$saldo_akun_investasi = 0;
$jumlah_akun_investasi = 0;
$total_investasi = 0;

$saldo_akun_aktiva_non = 0;
$jumlah_akun_aktiva_non = 0;
$total_aktiva_non = 0;

$saldo_akun_aktiva_lain = 0;
$jumlah_akun_aktiva_lain = 0;
$total_aktiva_lain = 0;

$total_aktiva = 0;
$modal = 0;
$jumlah_modal = 0;
$hutang = 0;
$jumlah_hutang = 0;
$total_passiva = 0;
$total_passivax = 0;
$laba_ditahan = 0;
@endphp
<table class="table table-bordered">
    <tbody>
        <tr>
            <td>
                <table class="table table-hover" border="1" width="100%">
                    <tbody>
                        <tr>
                            <td colspan="3"><center><b>LAPORAN NERACA TAHUN {{ $xtahun }}</b></center>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Aktiva</b></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @foreach ($report_neraca as $key => $laporan)
                            @if ($key == 'aktiva')
                                @foreach ($laporan as $list)
                                    <tr>
                                        <td>{{ $list['nama'] }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach ($list['akun'] as $akunx => $aktiva)
                                        @if ($list['kode'] == '1.001')
                                            @php
                                                $saldo_akun_aktiva_lancar = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                $jumlah_saldo_akun_aktiva_lancar += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($saldo_akun_aktiva_lancar, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endif
                                        @if ($list['kode'] == '1.002')
                                            @php
                                                $saldo_akun_aktiva_tetap = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                $jumlah_saldo_akun_aktiva_tetap += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($saldo_akun_aktiva_tetap, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endif

                                        @if ($list['kode'] == '1.004')
                                            @php
                                                $saldo_akun_aktiva_non = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                $jumlah_akun_aktiva_non += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($saldo_akun_aktiva_non, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endif
                                        @if ($list['kode'] == '1.005')
                                            @php
                                                $saldo_akun_aktiva_lain = intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                                $jumlah_akun_aktiva_lain += intval($aktiva['saldo_awal']) + intval($aktiva['debet']) - intval($aktiva['kredit']);
                                            @endphp
                                            <tr>
                                                <td> <span class="ml-2">
                                                        {{ $aktiva['nama'] }}</span></td>
                                                <td>{{ number_format($saldo_akun_aktiva_lain, 0) }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endif
                                        @php
                                            $saldo_awal_modal = intval($aktiva['saldo_awal']);
                                        @endphp
                                    @endforeach

                                    <tr>
                                        <td><span class="ml-2"><b>Jumlah
                                                    {{ $list['nama'] }}</b></span>
                                        </td>
                                        <td></td>
                                        @if ($list['kode'] == '1.001')
                                            <td>{{ number_format($jumlah_saldo_akun_aktiva_lancar, 0) }}
                                            </td>
                                        @elseif ($list['kode'] == '1.002')
                                            <td>{{ number_format($jumlah_saldo_akun_aktiva_tetap, 0) }}
                                            </td>
                                        @elseif ($list['kode'] == '1.003')
                                            <td>{{ number_format($jumlah_akun_investasi, 0) }}
                                            </td>
                                        @elseif ($list['kode'] == '1.004')
                                            <td>{{ number_format($jumlah_akun_aktiva_non, 0) }}
                                            </td>
                                        @elseif ($list['kode'] == '1.005')
                                            <td>{{ number_format($jumlah_akun_aktiva_lain, 0) }}
                                            </td>
                                        @endif
                                    </tr>
                                    @php
                                        $total_aktiva = intval($jumlah_saldo_akun_aktiva_lancar) + intval($jumlah_saldo_akun_aktiva_tetap) + intval($jumlah_akun_investasi) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_non) + intval($jumlah_akun_aktiva_lain);
                                    @endphp
                                @endforeach
                            @endif
                        @endforeach
                        <tr>
                            <td><span class="ml-2"><b>Total Aktiva </b></span>
                            </td>
                            <td></td>
                            <td>{{ number_format($total_aktiva, 0) }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table table-hover" border="1" width="100%">
                    <tbody>
                        <tr>
                            <td><b>Passiva</b></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @foreach ($report_neraca as $key => $laporan)
                            @if ($key == 'modal')
                                @foreach ($laporan as $list)
                                    <tr>
                                        <td>{{ $list['nama'] }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach ($list['akun'] as $akunx => $aktiva)
                                        @php
                                            $modal = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                            $jumlah_modal += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);

                                        @endphp
                                        <tr>
                                            <td> <span class="ml-2">
                                                    {{ $aktiva['nama'] }}</span></td>
                                            <td>{{ number_format($modal, 0) }}
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td><span class="ml-2"><b>Jumlah
                                                Modal </b></span>
                                    </td>
                                    <td></td>
                                    <td>{{ number_format($jumlah_modal, 0) }}
                                    </td>
                                </tr>
                            @elseif($key == 'hutang')
                                @foreach ($laporan as $list)
                                    <tr>
                                        <td>{{ $list['nama'] }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach ($list['akun'] as $akunx => $aktiva)
                                        @php
                                            $hutang = intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                            $jumlah_hutang += intval($aktiva['saldo_awal']) + intval($aktiva['kredit']) - intval($aktiva['debet']);
                                        @endphp
                                        <tr>
                                            <td> <span class="ml-2">
                                                    {{ $aktiva['nama'] }}</span></td>
                                            <td>{{ number_format($hutang, 0) }}
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td><span class="ml-2"><b>Jumlah Hutang / Kewajiban </b></span>
                                    </td>
                                    <td></td>
                                    <td>{{ number_format($jumlah_hutang) }}</td>
                                </tr>
                            @endif
                            @php
                                $total_passiva = intval($jumlah_modal) + intval($jumlah_hutang);
                                $laba_ditahan = intval($report_laba_rugi['laba_rugi']);
                                $total_passivax = intval($total_passiva) + intval($laba_ditahan);
                            @endphp
                        @endforeach
                        <tr>
                            <td><span class="ml-2"><b>Laba / Rugi </b></span>
                            </td>
                            <td></td>
                            <td>{{ number_format($laba_ditahan, 0) }}</td>
                        </tr>
                        <tr>
                            <td><span class="ml-2"><b>Total Passiva </b></span>
                            </td>
                            <td></td>
                            <td>{{ number_format($total_passivax, 0) }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
