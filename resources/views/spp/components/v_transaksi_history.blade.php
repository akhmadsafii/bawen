@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Histori</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Histori</li>
                <!--li class="breadcrumb-item "><a href="{{ route('potongan-spp') }}">Keringanan</a></li-->
                <li class="breadcrumb-item "><a href="{{ route('pemasukan-tagihan') }}">Tagihan</a></li>
                <li class="breadcrumb-item "><a href="{{ route('konfirmasi-transaksi') }}">Konfirmasi</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- print struk  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data" id="print-data">
                    </div>
                    <div class="table-responsive" id="jurnal-item" style="display:none;">
                        <hr>
                        <h5 class="box-title table-info"> <span class="ml-3">Informasi Jurnal Item</span> </h5>
                        <table id="table_doc_item_jurnal" class="table table-striped table-responsive">
                            <thead>
                                <tr class="table-info">
                                    <th>No</th>
                                    <th>Akun</th>
                                    <th>Tanggal</th>
                                    <th>Debit</th>
                                    <th>Kredit</th>
                                    <th>Keterangan </th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" data-id="" class="btn btn-info btn-rounded ripple text-left printNota"><i
                            class="fa fa-print list-icon"></i></a>
                    <a href="javascript:void(0)" data-id="" data-kode=""
                        class="btn btn-info btn-rounded ripple text-left printNotaEcel show-jurnal"><i
                            class="fa fa-book list-icon"></i> Jurnal Item </a>
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- print struk Edit  -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="printStruckModalEdit" tabindex="-1"
        role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel"> Form Edit Transaksi</h5>
                </div>
                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" class="form-control is-valid" id="id_trans" name="id_trans">
                        <div class="row print-dataedit">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update pos_transaction">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="headersx">
                            <div class="form-inline">
                                <div class="form-group mr-2">
                                    <div class="input-group input-has-value">
                                        <input type="text" class="form-control datepicker" name="tgl_filter"
                                            data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                            value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                                <a class="btn btn-primary" href="javascript: void(0);" id="filter">Filter</a>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_doc" class="table table-striped table-responsive">
                                <thead>
                                    <tr class="table-info">
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Jurusan</th>
                                        <th>Metode</th>
                                        <th>Nominal</th>
                                        <th>Tanggal Bayar </th>
                                        <th>Tahun Ajaran </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Append Create Datatables-->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Jurusan</th>
                                        <th>Metode</th>
                                        <th>Nominal</th>
                                        <th>Tanggal Bayar </th>
                                        <th>Tahun Ajaran </th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting;
            let config_table;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-history-pemasukan') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'kelas',
                            name: 'kelas'
                        },
                        {
                            data: 'jurusan',
                            name: 'jurusan'
                        },
                        {
                            data: 'bentuk',
                            name: 'bentuk'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar'
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_doc').dataTable(config_table);

                //show data
                $('body').on('click', '.show.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('#printStruckModal').modal('show');

                                    $('.print-data').html(''); //reset append

                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"> <span class="ml-3">Informasi Siswa</span> </h5>
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"> <span class="ml-3">Informasi Transaksi</span> </h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tipe</td>
                                                        <td>:</td>
                                                        <td><small><b>` + ucwordx(rows.bentuk).replace('_', ' ') + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <!--tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr-->
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata +
                                        `<div class="col-md-12" id="print-detail">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info"> <span class="ml-3">Informasi Pembayaran</span> </h5>
                                           <table class="table">
                                            <thead>
                                                 <tr class="table-info">
                                                    <td>No</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                        `;
                                    var indexNumber = 0;
                                    rows.tagihan.forEach(element => {
                                        indexNumber++;
                                        printdata = printdata + `
                                                <tr>
                                                    <td> ` + indexNumber + ` </td>
                                                    <td>` + element['nama_tagihan'] + `</td>
                                                    <td>` + element['nominal'].format() + `</td>
                                                </tr>
                                            `;
                                    });
                                    printdata = printdata + `</table>`;

                                    $('.print-data').html(printdata);

                                    $('.printNota').removeAttr('data-id');
                                    $('.printNotaEcel').removeAttr('data-id');
                                    $('.printNota').attr('data-id', rows.id);
                                    $('.printNotaEcel').attr('data-id', rows.id);
                                    $('.show-jurnal').removeAttr('data-kode');
                                    $('.show-jurnal').attr('data-kode', rows.kode);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                //reset modal
                $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //edit transaction
                $('body').on('click', '.edit.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    let list_tagihan = '';
                    let id_tagihanx = '';
                    let name_bulanx = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('#printStruckModalEdit').modal('show');
                                    $('input[name="id_trans"]').val(id);
                                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Siswa </span> </h5>
                                            <input type="hidden" class="form-control is-valid" id="nisn" name="nisn"
                                            placeholder="Nisn" value="` + rows.nis + `">
                                            <input type="hidden" class="form-control is-valid" id="id_siswa" name="id_siswa"
                                            placeholder="Nisn" value="` + rows.id_kelas_siswa + `">
                                            <input type="hidden" class="form-control is-valid" id="tgl_bayar" name="tgl_bayar"
                                            placeholder="Nisn" value="` + rows.tgl_bayar + `">
                                            <input type="hidden" class="form-control is-valid" id="tahun_ajaran" name="tahun_ajaran"
                                            placeholder="Nisn" value="` + rows.tahun_ajaran + `">
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;

                                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Transaksi </span></h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.tgl_bayar + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                                    printdata = printdata + `<div class="col-md-12">`;
                                    printdata = printdata + `
                                        <h5 class="box-title table-info"><span class="ml-2">Informasi Pembayaran </span></h5>
                                           <table class="table">
                                            <thead>
                                                 <tr>
                                                    <td>#</td>
                                                    <td>Nama Tagihan</td>
                                                    <td>Bulan</td>
                                                    <td>Nominal</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        `;

                                    list_bulan = ['-',
                                        'Juli', 'Agustus', 'September',
                                        'Oktober', 'November', 'Desember',
                                        'Januari', 'Februari', 'Maret',
                                        'April', 'Mei', 'Juni'
                                    ];


                                    rows.tagihan.forEach(element => {

                                        printdata = printdata + `
                                                 <tr>
                                                    <td># <input type="hidden" class="form-control is-valid" id="id_detail" name="id_detail[]"
                                                        placeholder="id" value="` + element['id'] + `"></td>
                                                    <td>
                                                        <input type="hidden" class="form-control is-valid" id="id_tagihan" name="id_tagihan[]"
                                                        placeholder="id_tagihan" value="` + element['id_tagihan'] +
                                            `">  ` + element['nama_tagihan'] + `
                                                    </td>
                                                    <td>
                                                        <input type="hidden" class="form-control is-valid" id="bulan" name="bulan[]"
                                                        placeholder="bulan" value="` + element['bulan'] + `"> ` +
                                            list_bulan[element['bulan']] +
                                            `
                                                    </td>
                                                    <td> <input name="nominal[]" type="text" id="nominal" class="form-control" onkeyup="currencyFormat(this)"  value="` +
                                            element['nominal'].format() + `" required=""></td>
                                                </tr>
                                            `;
                                    });
                                    printdata = printdata + `</tbody></table></div>`;
                                    $('.print-dataedit').html(printdata);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                }
                            }

                        }
                    });

                });

                //submit
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_trans"]').val();

                    var url = '{{ route('update-transaksi', ':id') }}';
                    url = url.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#printStruckModalEdit').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-pensil"></i> ');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-pensil"></i> ');
                                    window.notif(result['info'], result['message']);
                                }
                            }

                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //printNota

                //keyup nominal manual calc
                $("body").on('keyup', 'input[name="nominal[]"]', function() {
                    var xv = parseInt($(this).val().split(',').join(''));
                    // calc total
                    if (isNaN(xv) || xv == 0) {
                        window.notif('error', 'Nominal Harus di isi ');
                    }
                });

                //print nota
                $('body').on('click', '.printNota', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('print_nota', ':id') }}';
                    url = url.replace(':id', id);
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = url;
                    a.click();
                });

                //print history
                $('body').on('click', '.print.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('print_nota', ':id') }}';
                    url = url.replace(':id', id);
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = url;
                    a.click();
                });

                //export div to excel
                $('body').on('click', '.show-jurnal', function(e) {
                    e.preventDefault();
                    var kode = $(this).data('kode');
                    $('#jurnal-item').toggle();

                    var url = '{{ route('detail-jurnal-by-kode', ':id') }}';
                    url = url.replace(':id', kode);

                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5]
                                },
                                customize: function(doc) {
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join(
                                            '*').split(
                                            '');
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'akun',
                                name: 'akun'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'debet',
                                name: 'debet'
                            },
                            {
                                data: 'kredit',
                                name: 'kredit'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting = $('#table_doc_item_jurnal').dataTable(config_table);

                });

                //remove transaction
                $('body').on('click', 'button.remove.history.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove-transaksi', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                $('body').on('click', '#filter', function() {
                    var tanggal = $('input[name="tgl_filter"]').val();
                    var url = '{{ route('report_transaksi_bydate', ':id') }}';
                    url = url.replace(':id', tanggal);

                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                                text: '<i class="fa fa-refresh"></i>',
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload(null, false);
                                }
                            },
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                },
                                customize: function(doc) {
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join(
                                            '*').split(
                                            '');
                                }
                            },
                            'colvis',
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nis',
                                name: 'nis'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'kelas',
                                name: 'kelas'
                            },
                            {
                                data: 'jurusan',
                                name: 'jurusan'
                            },
                            {
                                data: 'bentuk',
                                name: 'bentuk'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'tgl_bayar',
                                name: 'tgl_bayar'
                            },
                            {
                                data: 'tahun_ajaran',
                                name: 'tahun_ajaran'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_setting = $('#table_doc').dataTable(config_table);
                });

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_setting.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
