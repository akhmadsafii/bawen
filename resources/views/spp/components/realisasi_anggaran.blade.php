@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Realisasi Penerimaan Pembayaran Anggaran </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="row mb-2">
                        <div class="col-md-4">
                            <label>Tahun Ajaran</label>
                            <!--select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                    @if (empty($request_tahun))
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                        </option>
                                    @endif

                                    @foreach ($tahun_ajaran as $key => $value)
                                        @php
                                            $tahun = explode('/', $value['tahun_ajaran']);
                                        @endphp
                                        @if ($request_tahun == $tahun[0])
                                            <option value="{{ $value['tahun_ajaran'] }}" selected="true">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                    @else
                                            <option value="{{ $value['tahun_ajaran'] }}">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select-->
                            <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                @if (empty(session('tahun')))
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun
                                    </option>
                                @endif

                                @foreach ($tahunlist as $tahunx)
                                    @if ($request_tahun == $tahunx)
                                        <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                        </option>
                                    @else
                                        <option value="{{ $tahunx }}">{{ $tahunx }}
                                        </option>
                                    @endif
                                @endforeach

                            </select>
                        </div>

                        <!--div class="col-md-4">
                                        <label>Bulan</label>
                                        <select class="form-control" id="bulan" name="bulan" required="">
                                            <option value="*" selected="true"> All Bulan </option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div-->

                        <div class="col-md-4" style="margin-top: 10px;">
                            <button type="button" class="btn btn-sm btn-outline-default mt-4 filter"><i
                                    class="fa fa-filter"></i></button>
                            <button type="button" class="btn btn-sm btn-outline-default mt-4 printNota"><i
                                    class="fa fa-print"></i></button>
                        </div>
                    </div>
                    <div class="widget-header text-center">
                        <b>LAPORAN REALISASI PENERIMAAN PEMBAYARAN ANGGARAN</b>
                        <br>
                        @if (!empty(session('sekolah')))
                            <b>{{ session('sekolah') }}</b>
                            <br>
                        @endif
                        <b>Periode Tahun {{ $request_tahun }} </b>
                    </div>
                    <hr>
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table class="table  table-striped table-hover">
                                <thead class="table-info">
                                    <tr>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">No</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">ITEM</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">TARGET</td>
                                        <td colspan='{{ count($list_bulan) }}' class="text-center"
                                            style="text-align: center; vertical-align: middle;">BULAN PENERIMAAN</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">TOTAL REALISASI</td>
                                    </tr>
                                    <tr>
                                        @foreach ($list_bulan as $bulan)
                                            <td class="text-center" style="text-align: center; vertical-align: middle;">
                                                {{ $bulan }}
                                            </td>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_target = 0;
                                        $sumArray_bulan = [];
                                        $total_realisasi = 0;
                                    @endphp
                                    @if (count($realisasi) > 0)

                                        @foreach ($realisasi as $key => $val)
                                            @php
                                                $total_target += intval($val['target']);
                                                $total_realisasi += intval($val['realisasi']);
                                            @endphp
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $val['nama'] }}</td>
                                                <td>{{ number_format($val['target'], 0) }}</td>
                                                @foreach ($val['bulan'] as $x => $k)
                                                    @php
                                                        if (!isset($sumArray_bulan[$x])) {
                                                            $sumArray_bulan[$x] = 0;
                                                        }
                                                        $sumArray_bulan[$x] += $k['nominal'];
                                                    @endphp
                                                    <td>{{ number_format($k['nominal'], 0) }}</td>
                                                @endforeach
                                                <td>{{ number_format($val['realisasi']) }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            @foreach ($list_bulan as $x => $bulan)
                                                @php
                                                    $sumArray_bulan[$x] = 0;
                                                @endphp
                                                <td>-</td>
                                            @endforeach
                                            <td>-</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr class="table-info">
                                        <td colspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">Total</td>
                                        <td>{{ number_format($total_target, 0) }}</td>
                                        @foreach ($sumArray_bulan as $p)
                                            <td>{{ number_format($p, 0) }}</td>
                                        @endforeach
                                        <td>{{ number_format($total_realisasi, 0) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //filter
                $('body').on('click', '.filter', function(e) {
                    e.preventDefault();
                    var tahun_ajaran = $('select[name="tahun_ajaran"] option:selected').val();
                    //var bulan = $('select[name="bulan"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    var url =
                        '{{ route('report_realiasiTahun', ['tahun' => ':tahun']) }}';
                    url = url.replace(':tahun', tahun_ajaran);
                    var a = document.createElement('a');
                    a.href = url;
                    a.click();

                    /*if (bulan == '*') {
                        var url =
                            '{{ route('report_realiasiTahun', ['tahun' => ':tahun']) }}';
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.href = url;
                        a.click();
                    } else {
                        var url =
                            '{{ route('report_realiasi_filter', ['id' => ':id', 'bulan' => ':bulan']) }}';
                        url = url.replace(':id', split_tahun[0]);
                        url = url.replace(':bulan', bulan);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }*/
                });

                //print
                $('body').on('click', '.printNota', function(e) {
                    e.preventDefault();
                    var tahun_ajaran = $('select[name="tahun_ajaran"] option:selected').val();
                    //var bulan = $('select[name="bulan"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');

                    var url =
                        '{{ route('report_realiasiTahunprint', ['tahun' => ':tahun']) }}';
                    url = url.replace(':tahun', tahun_ajaran);
                    var a = document.createElement('a');
                    a.href = url;
                    a.target = "_blank";
                    a.click();


                    /*if (bulan == '*') {
                        var url =
                            '{{ route('report_realiasiTahunprint', ['tahun' => ':tahun']) }}';
                        url = url.replace(':tahun', split_tahun[0]);
                        var a = document.createElement('a');
                        a.href = url;
                        a.target = "_blank";
                        a.click();
                    } else {
                        var url =
                            '{{ route('report_realiasi_filterprint', ['id' => ':id', 'bulan' => ':bulan']) }}';
                        url = url.replace(':id', split_tahun[0]);
                        url = url.replace(':bulan', bulan);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }*/

                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
