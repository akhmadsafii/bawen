<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        .text-center {
            text-align: center !important;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center><strong><u><b>Penjualan Aset Aktiva Tetap</b></u></strong></center>
    <br>
    <br>
    <table width="100%">
        <tbody>
            <tr>
                <td>Kode Transaksi </td>
                <td>:</td>
                <td>{{ $detail['kode_transaksi'] }}</td>
            </tr>
            <tr>
                <td>Tanggal Transaksi </td>
                <td>:</td>
                <td>{{ \Carbon\Carbon::parse($detail['tgl_transaksi'])->isoFormat('dddd, D MMMM Y ') }}</td>
            </tr>
            <tr>
                <td>Kode Akun </td>
                <td>:</td>
                <td>{{ $detail['kode_akun'] }}</td>
            </tr>
            <tr>
                <td>Total Transaksi </td>
                <td>:</td>
                <td>{{ number_format($detail['nominal']) }}</td>
            </tr>
            <tr>
                <td>Status </td>
                <td>:</td>
                <td>
                    @switch($detail['status'])
                        @case('1')
                           Jual
                        @break
                        @default
                    @endswitch
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <strong>Detail Item </strong>
    <table width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Item</th>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody style="text-align:center">
            @if (count($detail_item) > 0)
                @foreach ($detail_item as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item['nama'] }}</td>
                        <td>{{ \Carbon\Carbon::parse($item['tgl_bayar'])->isoFormat('dddd, D MMMM Y ') }}</td>
                        <td>{{ number_format($item['nominal'], 0) }}</td>
                        <td>{{ $item['keterangan'] ?? '-' }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    <br>
    <br>
    <table width="100%;">
        <tbody>
            <tr>
                <td align="right">
                    <strong>Penerima,</strong>
                    <br>
                    <br>
                    @if (!empty($data_piutang['operator']))
                        <strong><b>{{ ucwords($data_piutang['operator']) }}</b></strong>
                        <br>
                    @else
                        <br>
                    @endif
                    <strong><b>Bendahara / Tata Usaha </b></strong>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
