<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center>
        <h1><u><b>Tagihan Bulan {{ $bulan }}</b></u></h1>
    </center>
    @if (count($tagihan['tagihan']) > 0)
        <h5 class="card-title mt-0 mb-3">Data Siswa </h5>
        <table class="table">
            <tbody align="left">
                <tr width="100%;">
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ ucwords($tagihan['nama']) }}</td>
                </tr>
                <tr width="100%;">
                    <td>Kelas</td>
                    <td>:</td>
                    <td>{{ $tagihan['kelas'] }}</td>
                </tr>
                <tr width="100%;">
                    <td>Rombel</td>
                    <td>:</td>
                    <td>{{ $tagihan['rombel'] }}</td>
                </tr>
                <tr>
                    <td>Jurusan</td>
                    <td>:</td>
                    <td>{{ $tagihan['jurusan'] }}</td>
                </tr>
                <tr>
                    <td>Sekolah</td>
                    <td>:</td>
                    <td>{{ session('sekolah') }}</td>
                </tr>
                <tr>
                    <td>Tahun Ajaran </td>
                    <td>:</td>
                    <td>{{ $tagihan['tahun_ajaran'] }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <h5 class="card-title mt-0 mb-3">Data Ortu </h5>
        <table class="table">
            <tbody align="left">
                <tr width="100%;">
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ ucwords($ortu['nama']) }}</td>
                </tr>
                <tr width="100%;">
                    <td>Email</td>
                    <td>:</td>
                    <td>{{ $ortu['email'] }}</td>
                </tr>
                <tr width="100%;">
                    <td>Telepon</td>
                    <td>:</td>
                    <td>{{ $ortu['telepon'] }}</td>
                </tr>
            </tbody>
        </table>

        <br>

        <h5 class="card-title mt-0 mb-3"> Detail Tagihan </h5>
        @php
            $total_tagihan = 0;
        @endphp
        <table class="table table-hover">
            <thead>
                <tr class="table-warning">
                    <td>No</td>
                    <td>Nama</td>
                    <td>Periode</td>
                    <td>Nominal</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($tagihan['tagihan'] as $key => $val)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $val['nama'] }}</td>
                            <td>{{ $val['priode'] }}</td>
                            <td>{{ number_format($val['ditagih'], 0) }}</td>
                            <td>
                                @if (intval($val['ditagih']) == 0)
                                    <span class="alert alert-success"> Lunas </span>
                                @else
                                    <span class="alert alert-danger">Pending</span>
                                @endif
                            </td>
                        </tr>
                        @php
                            $total_tagihan += intval($val['ditagih']);
                        @endphp

                @endforeach
            <tbody>
            <tfoot>
                <tr>
                    <td>Total</td>
                    <td>:</td>
                    <td>{{ number_format($total_tagihan, 0) }}</td>
                </tr>
            </tfoot>
        </table>
    @else
        <div class="alert aler-info"> Bulan ini tidak ada tagihan!.</div>
    @endif
</body>

</html>
