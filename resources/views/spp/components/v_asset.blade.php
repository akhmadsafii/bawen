@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"> Aset Aktiva Tetap </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Aset Aktiva Tetap </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="detailasset" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Asset </h5>
                </div>
                <div class="modal-body">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>Kode Akun </td>
                                <td>:</td>
                                <td><span class="text-kode_akun ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Nama Akun</td>
                                <td>:</td>
                                <td><span class="text-nama_akun ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Nama Barang </td>
                                <td>:</td>
                                <td><span class="text-nama_barang ml-3"></span></td>
                                <td>Catatan </td>
                                <td>:</td>
                                <td><span class="text-catatan ml-3"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade bs-modal-lg" tabindex="-1" id="editdetailasset" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Asset </h5>
                </div>
                <form id="DocFormxedit" name="DocFormxedit" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id_asset" class="form-control">
                        <input type="hidden" name="id_akun_asset" class="form-control">
                        <input type="hidden" name="id_akun_kategori_asset" class="form-control">
                        <input type="hidden" name="id_akun_subkategori_asset" class="form-control">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>Kode Akun </td>
                                    <td>:</td>
                                    <td><span class="text-kode_akun ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Nama Akun</td>
                                    <td>:</td>
                                    <td><span class="text-nama_akun ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Nama Barang </td>
                                    <td>:</td>
                                    <td><input type="text" name="nama_barang" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Catatan </td>
                                    <td>:</td>
                                    <td><textarea name="catatan_barang" rows="3" class="form-control mt-3"></textarea></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info updatex">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">

                        <div class="row">
                            <div class="col-md-12">
                                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data"
                                    method="POST" action="{{ route('store_aktiva_tetap') }}">
                                    @csrf
                                    <div class="table-responsive">
                                        <table id="table_pengeluaran" width="100%"
                                            class="table table-striped table-responsive">
                                            <thead>
                                                <tr class="table-info">
                                                    <th>Akun Aset <span class="text-red">*</span></th>
                                                    <th>Nama Barang <span class="text-red">*</span></th>
                                                    <th>Kode Barang <span class="text-red">*</span></th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="clonable form-clone" parentId="1">
                                                    <td>
                                                        <select class="form-control akun_aktiva" id="akun_aktiva"
                                                            name="akun_aktiva[]" required="">
                                                            <option disabled="disabled" selected="true" value="">Pilih Akun
                                                            </option>
                                                            @foreach ($akun as $akun)
                                                                <option value="{{ $akun['id'] }}"> {{ $akun['kode'] }}
                                                                    -
                                                                    {{ $akun['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input name="nama_item[]" type="text" id="nama_item"
                                                            class="form-control" placeholder="Nama" required="required">
                                                    </td>
                                                    <td><input name="kode_item[]" type="text" id="nama_item"
                                                            class="form-control" placeholder="Nama" required="required">
                                                        <input name="id_akun_kategori[]" type="hidden" id="id_akun_kategori"
                                                            class="form-control" placeholder="Nama">
                                                        <input name="id_akun_sub_kategori[]" type="hidden"
                                                            id="id_akun_kategori" class="form-control" placeholder="Nama">
                                                    </td>

                                                    <td width="10%" class="btn-group text-center" role="group" width="100%">
                                                        <button type="button" name="remove" id="removex"
                                                            class="btn btn-danger remove-clone remove"><i
                                                                class="fa fa-minus"></i></button> &nbsp;
                                                        <button type="button" name="add" id="addx"
                                                            class="btn btn-info add-clone add"><i
                                                                class="fa fa-plus"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <td colspan="2">
                                                    <div class="form-group">
                                                        <label for="l38">Keterangan / Memo</label>
                                                        <textarea class="form-control" name="keterangan" id="keterangan"
                                                            rows="3"></textarea>
                                                    </div>
                                                </td>
                                                <td colspan="2">
                                                    <div class="form-group ml-auto btn-list mt-5">
                                                    <button type="submit" class="btn btn-info update pos_transaction">
                                                        <i class="material-icons list-icon">save</i>
                                                        Simpan
                                                    </button>
                                                    <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i>
                                                        Batal
                                                    </button>
                                                    </div>
                                                </td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="box-title">List Asset Fixed </div>
                                <div class="widget-body clearfix">
                                    <div class="table-responsive">
                                        <table id="table_list_barang" class="table table-striped table-responsive"
                                            width="100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode Akun</th>
                                                    <th>Nama Akun </th>
                                                    <th>Nama Barang </th>
                                                    <th>Catatan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- Append Create Datatables-->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="box-title">List Stock Asset Fixed </div>
                                <div class="widget-body clearfix">
                                    <div class="table-responsive">
                                        <table id="table_list_barang_stock" class="table table-striped table-responsive"
                                            width="100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode Akun</th>
                                                    <th>Nama Akun </th>
                                                    <th>Nama Barang </th>
                                                    <th>Stok</th>
                                                    <th>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- Append Create Datatables-->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {

            var config_table,config_asset, table_setting, table_detailitem, table_setting2, table_setting1,table_asset;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, ]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_asset') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_akun',
                            name: 'nama_akun'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_list_barang').dataTable(config_table);

                config_asset = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4,5 ]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4,5]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4,5]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_asset_stock') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_akun',
                            name: 'kode_akun'
                        },
                        {
                            data: 'nama_akun',
                            name: 'nama_akun'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'stok',
                            name: 'stok'
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan'
                        }
                    ],
                };

                table_asset = $('#table_list_barang_stock').dataTable(config_asset);

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });

                //select akun aktiva
                $('body').on('change', 'select.akun_aktiva', function() {
                    var x = $(this).val();
                    var parentId = $(this).parents('.clonable').attr('parentId');
                    var url = '{{ route('show_akuntansi_akunspp', ':id') }}';
                    url = url.replace(':id', x);
                    if (x) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON
                                            .stringify(
                                                result['data']));

                                        //console.log(rows);

                                        $('tr[parentId="' + parentId +
                                            '"] input[name="id_akun_kategori[]"]').val(
                                            rows.id_akun_kategori);
                                        $('tr[parentId="' + parentId +
                                                '"] input[name="id_akun_sub_kategori[]"]')
                                            .val(rows.id_sub_akun_kategori);


                                    }
                                }
                            }
                        });
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="id_akun_kategori[]"]').val('');
                        $('tr[parentId="' + parentId + '"] input[name="id_akun_sub_kategori[]"]').val(
                            '');
                    }
                });

                $('body').on('click', '.show.asset.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_asset_aktiva', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));


                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    $('#detailasset').modal('show');
                                    $('span.text-kode_akun').html(rows.kode_akun);
                                    $('span.text-nama_akun').html(rows.nama_akun);
                                    $('span.text-nama_barang').html(rows.nama);
                                    $('span.text-catatan').html(rows.keterangan);

                                }
                            }
                        }
                    });

                });

                $('body').on('click', '.edit.asset.akun', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_asset_aktiva', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON
                                        .stringify(
                                            result['data']));

                                    $(loader).html('<i class="fa fa-pensil"></i>');

                                    $('#editdetailasset').modal('show');
                                    $('input[name="id_asset"]').val(rows.id);
                                    $('input[name="id_akun_asset"]').val(rows.id_akun);
                                    $('span.text-kode_akun').html(rows.kode_akun);
                                    $('span.text-nama_akun').html(rows.nama_akun);
                                    $('input[name="nama_barang"]').val(rows.nama);
                                    $('textarea[name="catatan_barang"]').val(rows
                                        .keterangan);
                                    $('input[name="id_akun_kategori_asset"]').val(rows
                                        .id_akun_kategori);
                                    $('input[name="id_akun_subkategori_asset"]').val(rows
                                        .id_akun_sub_kategori);
                                }
                            }
                        }
                    });

                });

                $('body').on('submit', 'form#DocFormxedit', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_update_asset_aktiva') }}';
                    var formData = new FormData(this);
                    const loader = $('button.updatex');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading'
                            );
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#editdetailasset').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result[
                                        'message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result[
                                        'message']);
                                    $('#editdetailasset').modal('hide');
                                    table_setting.fnDraw(false);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html(
                                    '<i class="fa fa-save"></i> Update');
                                $('#editdetailasset').modal('hide');
                                table_setting.fnDraw(false);
                            }
                        }
                    });
                });

            });

        })(jQuery, window);
    </script>
@endsection
