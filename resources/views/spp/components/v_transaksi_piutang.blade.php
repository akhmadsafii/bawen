@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Transaksi Piutang </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Piutang</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-success">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="detailPiutang" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Piutang </h5>
                </div>
                <div class="modal-body">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>Debitur</td>
                                <td>:</td>
                                <td><span class="text-debitur ml-3"></span></td>
                                <td>Kode Transaksi </td>
                                <td>:</td>
                                <td><span class="text-kode ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>No.Kontak</td>
                                <td>:</td>
                                <td><span class="text-kontak ml-3"></span></td>
                                <td>Jatuh Tempo</td>
                                <td>:</td>
                                <td><span class="text-jatuhtempo ml-3"></span></td>
                            </tr>
                            <tr>
                                <td>Total Piutang </td>
                                <td>:</td>
                                <td><span class="text-hutang ml-3"></span></td>
                                <td>Status </td>
                                <td>:</td>
                                <td><span class="text-status ml-3"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <div class="tabs">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="nav-item"><a class="nav-link" href="#home-tab2"
                                                data-toggle="tab" aria-expanded="false">Detail Item </a>
                                        </li>
                                        <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                                href="#profile-tab2" data-toggle="tab" aria-expanded="false">Histori
                                                Pembayaran Cicilan </a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-tabs -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home-tab2" aria-expanded="false">
                                            <div class="table-responsive">
                                                <table id="table_doc_item" class="table table-striped table-responsive">
                                                    <thead>
                                                        <tr class="table-info">
                                                            <th>No</th>
                                                            <th>Nama Item</th>
                                                            <th>Tanggal</th>
                                                            <th>Nominal</th>
                                                            <th>Keterangan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- Append Create Datatables-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile-tab2" aria-expanded="false">
                                            <div class="table-responsive">
                                                <table id="table_doc_cicilan" class="table table-striped table-responsive">
                                                    <thead>
                                                        <tr class="table-info">
                                                            <th>No</th>
                                                            <th>Tanggal</th>
                                                            <th>Nominal</th>
                                                            <th>Keterangan</th>
                                                            <th>Bukti Pembayaran </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- Append Create Datatables-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.tabs -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-modal-lg" tabindex="-1" id="transaksi_cicilan" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Pembayaran Cicilan Piutang </h5>
                </div>
                <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>Kreditor</td>
                                    <td>:</td>
                                    <td><span class="text-debiturx ml-3"></span></td>
                                    <td>Kode Transaksi </td>
                                    <td>:</td>
                                    <td><span class="text-kodex ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>No.Kontak</td>
                                    <td>:</td>
                                    <td><span class="text-kontakx ml-3"></span></td>
                                    <td>Jatuh Tempo</td>
                                    <td>:</td>
                                    <td><span class="text-jatuhtempox ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Total Hutang </td>
                                    <td>:</td>
                                    <td><span class="text-hutangx ml-3"></span></td>
                                    <td>Status </td>
                                    <td>:</td>
                                    <td><span class="text-statusx ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Kode Akun </td>
                                    <td>:</td>
                                    <td><span class="text-kode-akun ml-3"></span></td>
                                    <td>Nama Akun </td>
                                    <td>:</td>
                                    <td><span class="text-nama-akun ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Jenis Angsuran </td>
                                    <td>:</td>
                                    <td><span class="text-jenis-angsuran ml-3"></span></td>
                                    <td>Sisa Angsuran </td>
                                    <td>:</td>
                                    <td><span class="text-sisa-angsuran ml-3"></span></td>
                                </tr>
                                <tr>
                                    <td>Total Angsuran </td>
                                    <td>:</td>
                                    <td><span class="text-total-angsuran ml-3"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <div class="table-responsive">
                            <table id="table_doc_itemx" class="table table-striped table-responsive">
                                <thead>
                                    <tr class="table-info">
                                        <th>No</th>
                                        <th>Nama Item</th>
                                        <th>Tanggal Piutang</th>
                                        <th>Nominal</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Append Create Datatables-->
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <hr>
                        <input type="hidden" name="id_piutang" id="id_piutang" class="form-control">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Tahun Ajaran</label>
                            <div class="col-md-6">
                                <select class="form-control" id="tahun_ajaranx" name="tahun_ajaranx" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                    </option>
                                    @foreach ($tahun_ajaran as $key => $value)
                                        @php
                                            $explode_tahun = explode('/', $value['tahun_ajaran']);
                                        @endphp
                                        @if ($explode_tahun[0] == session('tahun'))
                                            <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                        @else
                                            <option value="{{ $value['tahun_ajaran'] }}">
                                                {{ $value['tahun_ajaran'] }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Tanggal</label>
                            <div class="input-group input-has-value col-md-6">
                                <input type="text" class="form-control datepicker" name="tgl_trans_pembayaran"
                                    data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                    value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                <span class="input-group-addon"><i class="list-icon material-icons">date_range</i></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Akun <span
                                    class="text-red">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="akun_sumber" name="akun_sumber" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Akun
                                    </option>
                                    @foreach ($sumberx as $sumberx)
                                        <option value="{{ $sumberx['id'] }}"> {{ $sumberx['kode'] }} -
                                            {{ $sumberx['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Nominal <span
                                    class="text-red">*</span></label>
                            <div class="input-group input-has-value col-md-6">
                                <input name="nominal_pembayaran" type="text" id="nominal_pembayaran" class="form-control"
                                    onkeyup="currencyFormat(this)" placeholder="Nominal" required="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l38">Bukti Nota Pembayaran Debitor <span
                                    class="text-red">*</span> </label>
                            <div class="input-group input-has-value col-md-6">
                                <input name="bukti_pembayaran" type="file" id="bukti_pembayaran" class="form-control"
                                    accept="image/*" required="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l38">Keterangan / Memo <span
                                    class="text-red">*</span> </label>
                            <div class="input-group input-has-value col-md-6">
                                <textarea name="catatan_cicilan" id="catatan_cicilan" class="form-control" rows="2"
                                    required="required"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update pos_bayar ">
                            <i class="fa fa-money"></i>
                            Bayar
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data"
                            method="POST" action="{{ route('store_transaksi_piutang') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Tahun Ajaran</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="tahun_ajaran" name="tahun_ajaran"
                                                required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                                </option>
                                                @foreach ($tahun_ajaran as $key => $value)
                                                    @php
                                                        $explode_tahun = explode('/', $value['tahun_ajaran']);
                                                    @endphp
                                                    @if ($explode_tahun[0] == session('tahun'))
                                                        <option value="{{ $value['tahun_ajaran'] }}" selected="selected">
                                                            {{ $value['tahun_ajaran'] }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $value['tahun_ajaran'] }}">
                                                            {{ $value['tahun_ajaran'] }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Tanggal</label>
                                        <div class="input-group input-has-value col-md-8">
                                            <input type="text" class="form-control datepicker" name="tgl_trans"
                                                data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                                value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Jenis Transaksi <span
                                                class="text-red">*</span></label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="sumber" name="sumber" required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Jenis Transaksi
                                                </option>
                                                @foreach ($sumber as $sumber)
                                                    <option value="{{ $sumber['id'] }}"> {{ $sumber['kode'] }} -
                                                        {{ $sumber['nama'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0"> Jatuh Tempo <span
                                                class="text-red">*</span></label>
                                        <div class="input-group input-has-value col-md-8">
                                            <input type="text" class="form-control datepicker" name="jatuh_tempo"
                                                data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                                value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Jenis Angsuran <span
                                                class="text-red">*</span></label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="jenis_angsuran" name="jenis_angsuran"
                                                required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Jenis Angsuran
                                                </option>
                                                <option value="bulanan">Bulanan</option>
                                                <option value="tahunan">Tahunan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">Debitur <span
                                                class="text-red">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="text" name="debitor" id="debitor" class="form-control"
                                                    required="" placeholder="Nama Debitor ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="l0">No.Kontak <span
                                                class="text-red">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="text" name="no_kontak" id="no_kontak" class="form-control"
                                                    required="" placeholder="08xxxx">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="table_pengeluaran" width="100%"
                                            class="table table-striped table-responsive">
                                            <thead>
                                                <tr class="table-info">
                                                    <th>Nama Akun <span class="text-red">*</span></th>
                                                    <th>Nominal <span class="text-red">*</span></th>
                                                    <th>Catatan <span class="text-red">*</span></th>
                                                    <th style="display:none;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="clonable form-clone" parentId="1">
                                                    <td>
                                                        <select class="form-control" id="akun_piutang"
                                                            name="akun_piutang[]" required="">
                                                            <option disabled="disabled" selected="true" value="">Pilih Akun
                                                            </option>
                                                            @foreach ($akun as $akun)
                                                                <option value="{{ $akun['id'] }}"> {{ $akun['kode'] }}
                                                                    -
                                                                    {{ $akun['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input name="nominal[]" type="text" id="nominal"
                                                            class="form-control" onkeyup="currencyFormat(this)"
                                                            placeholder="Nominal" required="required"></td>
                                                    <td>
                                                        <textarea class="form-control" name="catatan" id="catatan"
                                                            rows="1" required="required"></textarea>
                                                    </td>
                                                    <td width="10%" class="btn-group text-center" role="group" width="100%"
                                                        style="display:none;">
                                                        <button type="button" name="remove" id="removex"
                                                            class="btn btn-danger remove-clone remove"><i
                                                                class="fa fa-minus"></i></button> &nbsp;
                                                        <button type="button" name="add" id="addx"
                                                            class="btn btn-info add-clone add"><i
                                                                class="fa fa-plus"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <td colspan="2">
                                                    <div class="form-group">
                                                        <label for="l38">Keterangan / Memo</label>
                                                        <textarea class="form-control" name="keterangan" id="keterangan"
                                                            rows="3"></textarea>
                                                    </div>
                                                </td>
                                                <td colspan="2">
                                                    <div class="form-group row ml-3">
                                                        <label for="l38"><i class="fa fa-file"></i> Berkas</label><br>
                                                        <span class="text-info ml-3" id="tambah_berkas"><input name="image"
                                                                type="file" id="image" class="form-control"
                                                                accept="image/*"></span>
                                                    </div>
                                                </td>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <center>
                                <button type="submit" class="btn btn-info update pos_transaction">
                                    <i class="material-icons list-icon">save</i>
                                    Simpan
                                </button>
                                <button type="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i> Batal
                                </button>
                            </center>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="box-title">List Piutang </div>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_piutang" class="table table-striped table-responsive" width="100%">
                                <thead>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Debitur</th>
                                    <th>Kontak </th>
                                    <th>Tanggal</th>
                                    <th>Jatuh Tempo</th>
                                    <th>Jenis</th>
                                    <th>Jumlah Piutang</th>
                                    <th>Nominal Angsuran </th>
                                    <th>Sisa Angsuran</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            var config_table, table_setting, table_detailitem, table_setting1, table_setting2;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_list_piutang') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode_transaksi',
                            name: 'kode_transaksi'
                        },
                        {
                            data: 'debitur',
                            name: 'debitur'
                        },
                        {
                            data: 'telepon',
                            name: 'telepon'
                        },
                        {
                            data: 'tgl_transaksi',
                            name: 'tgl_transaksi'
                        },
                        {
                            data: 'tgl_jatuh_tempo',
                            name: 'tgl_jatuh_tempo'
                        },
                        {
                            data: 'jenis_angsuran',
                            name: 'jenis_angsuran'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'total_angsuran',
                            name: 'total_angsuran'
                        },
                        {
                            data: 'sisa_angsuran',
                            name: 'sisa_angsuran'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_setting = $('#table_piutang').dataTable(config_table);
                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });

                $('body').on('click', '.show.hutang.transaksi', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_piutang_by_id', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    var sisa = 0;
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#detailPiutang').modal('show');
                                    $('a[href="#profile-tab2"]').attr('data-id', rows.id);
                                    $('span.text-debitur').html(rows.debitur);
                                    $('span.text-kontak').html(rows.telepon);
                                    $('span.text-kode').html(rows.kode_transaksi);
                                    $('span.text-jatuhtempo').html(dateFormatID(rows
                                        .tgl_jatuh_tempo));
                                    $('span.text-hutang').html(rows.nominal.format());

                                    sisa = parseInt(rows.nominal) - parseInt(rows
                                        .total_angsuran);
                                    if (sisa > 0) {
                                        $('span.text-status').html(
                                            '<span class="text-red">Belum Lunas</span>');
                                    } else {
                                        $('span.text-status').html(
                                            '<span class="text-success">Lunas</span>');
                                    }

                                    window.appendDetailItem(rows.kode_transaksi);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });
                });

                $('body').on('click', 'a#pembayaran', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_piutang_by_id', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    var sisa = 0;
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-money"></i> Bayar ');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#transaksi_cicilan').modal('show');
                                    $('input[name="id_piutang"]').val(rows.id);
                                    $('span.text-debiturx').html(rows.debitur);
                                    $('span.text-kode-akun').html(rows.kode_akun);
                                    $('span.text-nama-akun').html(rows.nama_akun);
                                    $('span.text-kontakx').html(rows.telepon);
                                    $('span.text-kodex').html(rows.kode_transaksi);
                                    $('span.text-jatuhtempox').html(dateFormatID(rows
                                        .tgl_jatuh_tempo));
                                    $('span.text-hutangx').html(rows.nominal.format());
                                    $('span.text-jenis-angsuran').html(ucwordx(rows
                                        .jenis_angsuran));

                                    sisa = parseInt(rows.nominal) - parseInt(rows
                                        .total_angsuran);
                                    if (sisa > 0) {
                                        $('span.text-statusx').html(
                                            '<span class="text-red">Belum Lunas</span>');
                                    } else {
                                        $('span.text-statusx').html(
                                            '<span class="text-success">Lunas</span>');
                                    }

                                    $('span.text-sisa-angsuran').html(sisa.format());
                                    $('span.text-total-angsuran').html(parseInt(rows
                                        .total_angsuran).format());

                                    window.appendDetailItemx(rows.kode_transaksi);

                                } else {
                                    $(loader).html('<i class="fa fa-money"></i> Bayar ');
                                }
                            }

                        }
                    });
                });

                window.appendDetailItem = function detailitem(kode) {
                    var urlx = "{{ route('show_by_kode_piutang_detail', ['id' => ':id']) }}";
                    urlx = urlx.replace(':id', kode);
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'tgl_transaksi',
                                name: 'tgl_transaksi'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting1 = $('#table_doc_item').dataTable(config_table);
                };

                window.appendDetailItemx = function detailitemx(kode) {
                    var urlx = "{{ route('show_by_kode_piutang_detail', ['id' => ':id']) }}";
                    urlx = urlx.replace(':id', kode);
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'tgl_transaksi',
                                name: 'tgl_transaksi'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                        ],
                    };

                    table_setting1 = $('#table_doc_itemx').dataTable(config_table);

                };

                //click tabs cicilan detail
                $('body').on('click', 'a[href="#profile-tab2"]', function() {
                    var id = $(this).data('id');
                    var urlx = "{{ route('show_cicilan_piutang_by_id', ['id' => ':id']) }}";
                    urlx = urlx.replace(':id', id);
                    config_table = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'tgl_transaksi',
                                name: 'tgl_transaksi'
                            },
                            {
                                data: 'keterangan',
                                name: 'keterangan'
                            },
                            {
                                data: 'nominal',
                                name: 'nominal'
                            },
                            {
                                data: 'file',
                                name: 'file'
                            },
                        ],
                    };

                    table_setting2 = $('#table_doc_cicilan').dataTable(config_table);

                });

                //submit update
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var id_piutang  =  $('input[name="id_piutang"]').val();
                    var urlx = '{{ route('store_cicilan_piutang') }}';
                    var formData = new FormData(this);
                    const loader = $('button.update');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading'
                            );
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#transaksi_cicilan').modal('hide');
                                    table_setting.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result[
                                        'message']);

                                    var url = "{{ route('print_piutang',':id') }}";
                                    url = url.replace(':id',id_piutang);

                                    const link = document.createElement('a');
                                    link.href = url;
                                    link.target = '_blank';
                                    document.body.appendChild(link);
                                    link.click();
                                    link.remove();


                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-save"></i> Simpan');
                                    window.notif(result['info'], result[
                                        'message']);
                                    $('#transaksi_cicilan').modal('hide');
                                    table_setting.fnDraw(false);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html(
                                    '<i class="fa fa-save"></i> Update');
                                $('#transaksi_cicilan').modal('hide');
                                table_setting.fnDraw(false);
                            }
                        }
                    });
                });

                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

                //format date indonesia
                window.dateFormatID = function parseDate(xt) {
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
                        'September', 'Oktober', 'November', 'Desember'
                    ];
                    var tanggal = new Date(xt).getDate();
                    var xhari = new Date(xt).getDay();
                    var xbulan = new Date(xt).getMonth();
                    var xtahun = new Date(xt).getYear();
                    var harix = hari[xhari];
                    var bulanx = bulan[xbulan];
                    var tahunx = (xtahun < 1000) ? xtahun + 1900 : xtahun;
                    var fulldate = harix + ', ' + tanggal + ' ' + bulanx + ' ' + tahunx;
                    return fulldate;
                }

            });
        })(jQuery, window);
    </script>
@endsection
