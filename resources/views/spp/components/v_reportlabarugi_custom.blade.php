@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Laba Rugi</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tahun</label>
                                <select class="form-control" id="tahun" name="tahun" required="">
                                    @if (empty(session('tahun')))
                                        <option disabled="disabled" selected="true" value="">Pilih Tahun
                                        </option>
                                    @endif

                                    @foreach ($tahunlist as $tahunx)
                                        @if ($tahun == $tahunx)
                                            <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}
                                            </option>
                                        @else
                                            <option value="{{ $tahunx }}">{{ $tahunx }}
                                            </option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Bulan</label>
                                <select class="form-control" id="bulan" name="bulan" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Bulan
                                    </option>
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Mode</label>
                                <select class="form-control" id="mode" name="mode" required="">
                                    <option disabled="disabled" selected="true" value="">Pilih Mode
                                    </option>
                                    <option value="1">All</option>
                                    <option value="2">Kategori</option>
                                    <option value="3">Sub Kategori</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-group" role="group" aria-label="Horizontal Button Group" style="margin-top: 34px;">
                                    <button type="button" class="btn btn-sm btn-outline-danger filter"><i
                                            class="fa fa-filter"></i></button>  &nbsp; &nbsp;
                                    <button type="button" class="btn btn-sm btn-outline-info printNota"><i
                                            class="fa fa-print"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body clearfix">
                        <hr>
                        <div class="widget-header text-center" id="widget-header">
                            @if (!empty(session('sekolah')))
                                <b>{{ session('sekolah') }}</b>
                                <br>
                            @endif
                            <b>LAPORAN LABA / RUGI </b>
                            <br>
                            <b>Periode Bulan {{ $bulan }} Tahun {{ $tahun }} </b>
                        </div>
                        <hr>
                        <a href="{{ route('export_laba_rugi', ['bulan' => $bulan, 'tahun' => $tahun, 'mode' => $mode]) }}"
                            class="btn btn-sm btn-outline-info mt-4 float-right">Export</a>
                        <br>
                        <br>
                        <div class="table-responsive">
                            @php
                                $total_pendapatan = 0;
                                $total_pengeluaran = 0;
                                $total_laba_bersih = 0;
                            @endphp
                            <div class="ml-5 mr-5">
                                <table class="table table-hover table-bordered" id="tblexportData">
                                    @if ($mode == 2)
                                        @foreach ($report as $key => $laporan)
                                            @php
                                                if ($laporan['id'] == 4) {
                                                    $total_pendapatan += intval($laporan['nominal']);
                                                } elseif ($laporan['id'] == 5) {
                                                    $total_pengeluaran += intval($laporan['nominal']);
                                                }
                                            @endphp
                                            <tr>
                                                <td><b>{{ $laporan['nama'] }}</b></td>
                                                @if ($laporan['id'] == 4)
                                                    <td><b>{{ number_format($laporan['nominal'], 0) }}</b>
                                                    </td>
                                                @elseif ($laporan['id'] == 5)
                                                    <td><b>{{ number_format($laporan['nominal'], 0) }}</b>
                                                    </td>
                                                @endif
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td><b>Laba Bersih </b></td>
                                            <td></td>
                                            <td>
                                                @php
                                                    $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
                                                @endphp
                                                @if ($total_laba_bersih > 0)
                                                    <b
                                                        class="text-info">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @else
                                                    <b
                                                        class="text-red ">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @endif
                                            </td>
                                        </tr>

                                    @elseif($mode == 3)
                                        @foreach ($report as $key => $laporan)

                                            @php
                                                if ($laporan['id'] == 4) {
                                                    $total_pendapatan += intval($laporan['nominal']);
                                                } elseif ($laporan['id'] == 5) {
                                                    $total_pengeluaran += intval($laporan['nominal']);
                                                }
                                            @endphp

                                            <tr>
                                                <td><b>{{ $laporan['nama'] }}</b></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            @if ($laporan['id'] == 4)
                                                @foreach ($laporan['sub_akun'] as $akun)
                                                    @if (intval($akun['nominal']) != 0)
                                                        <tr>
                                                            <td><span class="ml-5">{{ $akun['nama'] }}</span>
                                                            </td>
                                                            <td>{{ number_format($akun['nominal'], 0) }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif

                                            @if ($laporan['id'] == 5)
                                                @foreach ($laporan['sub_akun'] as $akun)
                                                    @if (intval($akun['nominal']) != 0)
                                                        <tr>
                                                            <td><span class="ml-5">{{ $akun['nama'] }}</span>
                                                            </td>
                                                            <td>{{ number_format($akun['nominal'], 0) }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif

                                        @endforeach

                                        <tr>
                                            <td><b>Laba Bersih </b></td>
                                            <td></td>
                                            <td>
                                                @php
                                                    $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
                                                @endphp
                                                @if ($total_laba_bersih > 0)
                                                    <b
                                                        class="text-info">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @else
                                                    <b
                                                        class="text-red ">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @endif
                                            </td>
                                        </tr>

                                    @elseif ($mode == 1)
                                        @foreach ($report as $key => $laporan)
                                            @if ($key == 'pendapatan')

                                                <tr>
                                                    <td><b>{{ $laporan['nama'] }}</b></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @foreach ($laporan['sub_akun_kategori'] as $subakun)
                                                    <tr>
                                                        <td><span class="ml-3">{{ $subakun['nama'] }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    @foreach ($subakun['akun'] as $akun)
                                                        @php
                                                            if ($laporan['id'] == 4) {
                                                                $total_pendapatan += intval($akun['nominal']);
                                                            }
                                                        @endphp
                                                        @if (intval($akun['nominal']) != 0)
                                                            <tr>
                                                                <td><span
                                                                        class="ml-5">{{ $akun['nama'] }}</span>
                                                                </td>
                                                                <td><span
                                                                        class="ml-3">{{ number_format($akun['nominal'], 0) }}</span>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    @if ($laporan['id'] == 4)
                                                        @if ($subakun['kode'] == '4.001')
                                                            @php
                                                                $total_pendapatan_1 = 0;
                                                                $total_pendapatan_1 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left">Sub Total
                                                                            {{ $subakun['nama'] }} </span></b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-success">{{ number_format($total_pendapatan_1, 0) }}</span></b>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if ($subakun['kode'] == '4.002')
                                                            @php
                                                                $total_pendapatan_2 = 0;
                                                                $total_pendapatan_2 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left">Sub Total
                                                                            {{ $subakun['nama'] }} </span></b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-success">{{ number_format($total_pendapatan_2, 0) }}</span></b>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endif

                                                @endforeach

                                            @elseif ($key == 'pengeluaran')

                                                <tr>
                                                    <td><b>{{ $laporan['nama'] }}</b></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                @foreach ($laporan['sub_akun_kategori'] as $subakun)
                                                    <tr>
                                                        <td><span class="ml-3">{{ $subakun['nama'] }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    @foreach ($subakun['akun'] as $akun)
                                                        @php
                                                            if ($laporan['id'] == 5) {
                                                                $total_pengeluaran += intval($akun['nominal']);
                                                            }
                                                        @endphp
                                                        @if (intval($akun['nominal']) != 0)
                                                            <tr>
                                                                <td><span
                                                                        class="ml-5">{{ $akun['nama'] }}</span>
                                                                </td>
                                                                <td><span
                                                                        class="ml-3">{{ number_format($akun['nominal'], 0) }}</span>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    @if ($laporan['id'] == 5)

                                                        @if ($subakun['kode'] == '5.001')
                                                            @php
                                                                $total_pengeluaran_1 = 0;
                                                                $total_pengeluaran_1 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                                            Total
                                                                            {{ $subakun['nama'] }} </span>
                                                                    </b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_1, 0) }}</span></b>
                                                                </td>
                                                            </tr>

                                                        @elseif ($subakun['kode'] == '5.002')
                                                            @php
                                                                $total_pengeluaran_2 = 0;
                                                                $total_pengeluaran_2 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                                            Total
                                                                            {{ $subakun['nama'] }} </span>
                                                                    </b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_2, 0) }}</span></b>
                                                                </td>
                                                            </tr>

                                                        @elseif ($subakun['kode'] == '5.003')

                                                            @php
                                                                $total_pengeluaran_3 = 0;
                                                                $total_pengeluaran_3 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                                            Total
                                                                            {{ $subakun['nama'] }} </span>
                                                                    </b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_3, 0) }}</span></b>
                                                                </td>
                                                            </tr>

                                                        @elseif ($subakun['kode'] == '5.004')

                                                            @if (count($subakun['akun']) > 0)
                                                                @php
                                                                    $total_pengeluaran_4 = 0;
                                                                    $total_pengeluaran_4 += intval($akun['nominal']);
                                                                @endphp

                                                            @else
                                                                @php
                                                                    $total_pengeluaran_4 = 0;
                                                                @endphp
                                                            @endif

                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                                            Total
                                                                            {{ $subakun['nama'] }} </span>
                                                                    </b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_4, 0) }}</span></b>
                                                                </td>
                                                            </tr>

                                                        @elseif ($subakun['kode'] == '5.005')
                                                            @php
                                                                $total_pengeluaran_5 = 0;
                                                                $total_pengeluaran_5 += intval($akun['nominal']);
                                                            @endphp
                                                            <tr>
                                                                <td align="left"><b> <span class="ml-3 text-left ">Sub
                                                                            Total
                                                                            {{ $subakun['nama'] }} </span>
                                                                    </b></td>
                                                                <td></td>
                                                                <td><b><span
                                                                            class="text-right ml-4 text-danger">{{ number_format($total_pengeluaran_5, 0) }}</span></b>
                                                                </td>
                                                            </tr>

                                                        @endif

                                                    @endif

                                                @endforeach

                                            @endif
                                        @endforeach
                                        <tr>
                                            <td><b>Laba Bersih </b></td>
                                            <td></td>
                                            <td>
                                                @php
                                                    $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
                                                @endphp
                                                @if ($total_laba_bersih > 0)
                                                    <b
                                                        class="text-info ml-4">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @else
                                                    <b
                                                        class="text-red ml-4">{{ number_format($total_laba_bersih, 0) }}</b>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function() {

                @if (!empty($index_bulan))
                    $("select[name='bulan'] > option[value=" + {{ $index_bulan }} + "]").prop("selected",
                    true);
                @endif

                @if (!empty($mode))
                    $("select[name='mode'] > option[value=" + {{ $mode }} + "]").prop("selected",
                    true);
                @endif

                $('body').on('click', 'button.filter', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var mode = $('select[name="mode"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if ((typeof bulan == 'underfined' || bulan == '' || bulan == null) && (mode == '' ||
                            mode == null || typeof mode == 'underfined')) {
                        window.notif('error', 'Silahkan pilih bulan dan mode filter terlebih dahulu');
                    } else if ((typeof bulan != 'underfined' || bulan != '' || bulan != null) && (
                            mode == '' || mode == null || typeof mode == 'underfined')) {
                        window.notif('error', 'Silahkan pilih mode filter terlebih dahulu');
                    } else if ((typeof bulan == 'underfined' || bulan == '' || bulan == null) && (
                            mode != '' || mode != null || typeof mode != 'underfined')) {
                        window.notif('error', 'Silahkan pilih bulan filter terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('report_labarugi_filter', ['bulan' => ':bulan', 'tahun' => ':tahun', 'mode' => ':mode']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        url = url.replace(':mode', mode);
                        var a = document.createElement('a');
                        a.target = "_parent";
                        a.href = url;
                        a.click();
                    }
                });

                $('body').on('click', 'button.printNota', function() {
                    var tahun_ajaran = $('select[name="tahun"] option:selected').val();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var mode = $('select[name="mode"] option:selected').val();
                    var split_tahun = tahun_ajaran.split('/');
                    if ((typeof bulan == 'underfined' || bulan == '' || bulan == null) && (mode == '' ||
                            mode == null || typeof mode == 'underfined')) {
                        window.notif('error', 'Silahkan pilih bulan dan mode filter terlebih dahulu');
                    } else if ((typeof bulan != 'underfined' || bulan != '' || bulan != null) && (
                            mode == '' || mode == null || typeof mode == 'underfined')) {
                        window.notif('error', 'Silahkan pilih mode filter terlebih dahulu');
                    } else if ((typeof bulan == 'underfined' || bulan == '' || bulan == null) && (
                            mode != '' || mode != null || typeof mode != 'underfined')) {
                        window.notif('error', 'Silahkan pilih bulan filter terlebih dahulu');
                    } else {
                        var url =
                            '{{ route('report_labarugi_filterprint', ['bulan' => ':bulan', 'tahun' => ':tahun', 'mode' => ':mode']) }}';
                        url = url.replace(':bulan', bulan);
                        url = url.replace(':tahun', split_tahun[0]);
                        url = url.replace(':mode', mode);
                        var a = document.createElement('a');
                        a.target = "_blank";
                        a.href = url;
                        a.click();
                    }
                });

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
