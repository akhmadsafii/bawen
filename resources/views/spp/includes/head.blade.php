<link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
<script type="text/javascript">
    //disabled pace loading
    window.paceOptions = {
        ajax: false,
        restartOnRequestAfter: false,
    };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" sizes="20x20" href="{{ session('logo') ?? asset('asset/img/sma.png') }}">
<title>{{ !empty(Session::get('title')) ? session('title') : 'Demo Smartschool' }}</title>
<!-- CSS -->
<link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
    rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
<!-- Head Libs -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
    rel="stylesheet" type="text/css">

<!-- @if ($MIDTRANS_PRODUCTION == 0)
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ $MIDTRANS_CLIENT_KEY ?? (env('MIDTRANS_CLIENT_KEY') ?? '') }}"></script>
@elseif($MIDTRANS_PRODUCTION == 1)
    <script type="text/javascript" src="https://app.midtrans.com/snap/snap.js"
        data-client-key="{{ $MIDTRANS_CLIENT_KEY ?? (env('MIDTRANS_CLIENT_KEY') ?? '') }}"></script>
@endif -->

@if (!empty($PUSHER_APP_KEY))
    <script src="//js.pusher.com/3.1/pusher.min.js" defer></script>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                var notificationsWrapper = $('.dropdown-notifications');
                var notificationsToggle = notificationsWrapper.find('a[data-toggle]');
                var notificationsCountElem = notificationsToggle.find('span[data-count]');
                var notificationsCount = parseInt(notificationsCountElem.data('count'));
                var notifications = notificationsWrapper.find('ul.list-message');
                if (notificationsCount <= 0) {
                    notificationsWrapper.hide();
                }
                // Enable pusher logging - don't include this in production
                //Pusher.logToConsole = true;
                var pusher = new Pusher('{{ $PUSHER_APP_KEY ?? (env('PUSHER_APP_KEY') ?? '') }}', {
                    encrypted: true,
                    cluster: '{{ $PUSHER_APP_CLUSTER ?? (env('PUSHER_APP_CLUSTER') ?? '') }}'
                });

                @switch(session('role'))
                @case('siswa')
                var channel_user = pusher.subscribe('spp-channel');
                channel_user.bind('spp_event', function(data) {
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    notif('info', data);
                    var existingNotifications = notifications.html();
                    var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
                    var newNotificationHtml = `<li><a href="#" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });
                @break
                @case('ortu')
                var channel_user = pusher.subscribe('spp-channel-ortu');
                channel_user.bind('spp_event_ortu', function(data) {
                    if ('{{ session('id_kelas_siswa') }}' == data['id_siswa']) {
                        soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                        notif('info', data['message']);
                        var existingNotifications = notifications.html();
                        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
                        if (data['tolak'] == false) {
                            var newNotificationHtml = `<li><a href="` + data['link'] + `" target="_blank" class="media">
                        <span class="d-flex user--online thumb-xs">
                        <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                        <span class="media-body">
                        <span class="media-content">` + data['message'] + `</span>
                        </span></a>
                        </li>`;
                        } else if (data['tolak'] == true) {
                            var newNotificationHtml = `<li><a href="#" target="_blank" class="media">
                        <span class="d-flex user--online thumb-xs">
                        <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                        <span class="media-body">
                        <span class="media-content">` + data['message'] + `</span>
                        </span></a>
                        </li>`;
                        }

                        notifications.html(newNotificationHtml + existingNotifications);
                        notificationsCount += 1;
                        notificationsCountElem.attr('data-count', notificationsCount);
                        notificationsWrapper.find('.notif-count').text(notificationsCount);
                        notificationsWrapper.show();

                        window.pesan(); //detach pesan
                    }
                });
                @break
                @case('tata-usaha')
                var channel_admin = pusher.subscribe('konfirmasi_pembayaran_spp');
                channel_admin.bind('event_konfirm_spp', function(data) {
                    notif('info', data);
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="{{ route('konfirmasi-transaksi') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                    window.pesan(); //detach pesan
                });
                @break
                @endswitch

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                window.soundnotif = function playSound(url) {
                    var audio = document.createElement('audio');
                    audio.style.display = "none";
                    audio.src = url;
                    audio.autoplay = true;
                    audio.onended = function() {
                        audio.remove() //Remove when played.
                    };
                    document.body.appendChild(audio);
                }

                window.pesan = function getpesan() {
                    //count pesan
                    var  urlb;
                    @switch(session('role'))
                    @case('ortu')
                    urlb = "{{ route('notifikasi_trans', ['id' => session('id_kelas_siswa')]) }}";
                    @break
                    @case('tata-usaha')
                    urlb = "{{ route('notifikasiadmin_trans', ['id' => session('id')]) }}";
                    @break
                    @endswitch

                    if (urlb != null) {

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'GET',
                            url: urlb,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $('span.notif-count-pesan').html(result['data']);
                                    }
                                }
                            }
                        });
                    }
                }

            });
        })(jQuery, window);
    </script>
@endif
