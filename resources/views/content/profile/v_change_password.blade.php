@extends('content/profile/layout/profile')
@section('content_profile')
    <style>
        .separator {
            border-right: 1px solid #dfdfe0;
        }

        .icon-btn-save {
            padding-top: 0;
            padding-bottom: 0;
        }

        .input-group {
            margin-bottom: 10px;
        }

        .btn-save-label {
            position: relative;
            left: -12px;
            display: inline-block;
            padding: 6px 12px;
            background: rgba(0, 0, 0, 0.15);
            border-radius: 3px 0 0 3px;
        }

    </style>
    <div class="card">
        <div class="card-header alert-info">
            <h3 class="box-title">Reset Password</h3>
        </div>
        <div class="card-body">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12 mx-auto">
                    <center>
                        <img src="{{ asset('asset/img/reset_pass.png') }}" alt="" width="200">
                    </center>
                </div>
                <div class="col-md-9">
                    <form class="mr-t-30" id="formChangePassword">
                        <div class="form-group row">
                            <label for="sample3UserName" class="text-sm-right col-sm-5 col-form-label">Password Lama</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="material-icons">lock</i>
                                    </div>
                                    <input class="form-control" type="password" name="current_password"
                                        placeholder="Password lama">
                                </div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group row">
                            <label for="sample3Password" class="text-sm-right col-sm-5 col-form-label">Password Baru</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="material-icons">lock</i>
                                    </div>
                                    <input class="form-control" type="password" name="new_password"
                                        placeholder="Password Baru">
                                </div>
                                <!-- /.input-group -->
                            </div>
                            <!-- /.col-sm-9 -->
                        </div>
                        <div class="form-group row">
                            <label for="sample3Password" class="text-sm-right col-sm-5 col-form-label">Ulangi Password
                                Baru</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="material-icons">lock</i>
                                    </div>
                                    <input class="form-control" type="password" name="confirm_password"
                                        placeholder="Ulangi Password Baru">
                                </div>
                                <!-- /.input-group -->
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-sm-7 ml-auto btn-list">
                                    <button type="submit" class="btn btn-info" id="saveBtn">Update</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#formChangePassword").on('submit', function(event) {
            event.preventDefault();
            $("#saveBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveBtn").attr("disabled", true);
            $.ajax({
                url: "{{ route('change_password') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formChangePassword').trigger("reset");
                    }
                    swa(data.status + "!", data.success, data.icon);
                    $('#saveBtn').html('Update');
                    $("#saveBtn").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Update');
                    $("#saveBtn").attr("disabled", false);
                }
            });
        });
    </script>
@endsection
