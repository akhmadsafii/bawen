@extends('content/profile/layout/profile')
@section('content_profile')
    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row justify-content-start flex-column ml-3 my-2 ">
        <h4 class="font-weight-bold">
            Tambahkan paraf anda
        </h4>
        <span>
            <p>
                Paraf Anda akan otomatis tercetak diraport siswa yang diajar
            </p>
        </span>
    </div>
    <div class="row justify-content-start ml-1">
        <div class="col-lg-4 col-md-10">
            <form method="post" enctype="multipart/form-data"
                action="{{ url('program/' . session('config') . '/profile/update_paraf') }}" onsubmit="return loader()">
                @csrf
                <img id="thumb"
                    src="{{ $profile['paraf'] != null ? $profile['paraf'] : 'https://via.placeholder.com/150' }}"
                    width="100%" />
                <input type="file" class="form-control" onchange="preview()" name="image"
                    style="height: 50px; margin-top: 20px;" />
                <button type="submit" class="btn btn-success mt-3" id="btnUpdate">Simpan</button>
            </form>
        </div>
    </div>

    <script>
        function loader() {
            $("#btnUpdate").attr("disabled", true);
            $("#btnUpdate").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
        }

        function preview() {
            thumb.src = URL.createObjectURL(event.target.files[0]);
        }
    </script>
@endsection
