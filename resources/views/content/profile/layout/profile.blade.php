@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == 'e_commerce')
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == 'real_state')
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == 'university')
        @php
            $ext = '_university';
        @endphp
    @elseif($template == 'default')
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif

    @if (session('role') == 'learning-admin' || session('role') == 'admin-kepegawaian' || session('role') == 'pegawai')
        @php
            $ext = '_default';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp

@endif
@extends($temp)
@section('content')
    {{-- {{ dd($template) }} --}}
    @include('content.profile.style')
    @if (session('role') != 'admin')
        <style>
            @media (min-width: 961px) {
                .sidebar-horizontal .side-menu ul {
                    position: absolute;
                    top: 0;
                    left: unset;
                    background: #ffffff;
                    z-index: 7;
                    margin-left: 0;
                    width: 22.92308em;
                    padding: 0.76923em 0;
                    display: none;
                    line-height: 2.76923em;
                    -webkit-box-shadow: 0;
                    box-shadow: unset;
                }

                .sidebar-horizontal .side-menu ul li {
                    background: #fefefe;
                }
            }

        </style>
        @if (session('media_template') == 'mobile')
            <style>
                .view-account .content-panel {
                    padding: 31px !important;
                }

                .img-thumbnail {
                    max-width: 30% !important;
                }

                .left {
                    display: none !important;
                }

            </style>
        @endif
    @endif
    <div class="row">
        <div class="col-md-12">
            @if (session('config') == 'kesiswaan')
                <a href="{{ url('/program/kesiswaan') }}" class="btn btn-info"><i class="fas fa-arrow-left"></i>
                    Kembali</a>
            @endif
            <div class="view-account p-3">
                <section class="module">
                    <div class="module-inner">
                        @include('content.profile.layout.sidebar')
                        <div class="content-panel">
                            @yield('content_profile')
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        $('#passwordForm').on('submit', function(event) {
            console.log("tets");
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            $.ajax({
                url: "{{ route('change_password') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $(".btn-save").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".btn-save").html(
                        '<i class="material-icons list-icon fs-24">https</i> Update Password');
                    noti(data.icon, data.success);

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
@endsection
