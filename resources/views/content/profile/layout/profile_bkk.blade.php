@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    @include('content.profile.style')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h2 class="box-title m-3">EDIT PROFILE</h2>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Edit Profile</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="view-account">
                <section class="module">
                    <div class="module-inner">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="side-bar">
                                    <div class="user-info pb-0">
                                        <img class="img-profile img-circle img-responsive center-block"
                                            src="{{ $data['file'] }}" alt="" style="width: 100%">
                                        <ul class="meta list list-unstyled">
                                            <li class="name">{{ ucwords($data['nama']) }}
                                                <br><label class="label label-info"
                                                    style="color: #6a6a70;">{{ ucwords(session('role')) }}</label>
                                            </li>
                                            <li class="email"><a href="#">{{ $data['email'] }}</a></li>
                                            <li class="activity">Last logged in:
                                                {{ $data['before_last_login'] != null ? (new \App\Helpers\Help())->getTanggalLengkap($data['before_last_login']) : 'Ini adalah pertama kalinya anda login' }}
                                            </li>
                                        </ul>
                                    </div>
                                    <nav class="side-menu">
                                        <ul class="nav">

                                            @if (session('role') != 'admin')
                                                <li class="{{ Request::segment(4) == 'edit' ? 'active' : '' }}">
                                                    <a href="{{ url('program/' . session('config') . '/profile/edit') }}"><span
                                                            class="fa fa-user"></span>
                                                        Profile</a>
                                                </li>
                                                <li
                                                    class="{{ Request::segment(4) == 'v_change-password' ? 'active' : '' }}">
                                                    <a
                                                        href="{{ url('program/' . session('config') . '/profile/v_change-password') }}"><span
                                                            class="fa fa-key"></span> Change
                                                        Password</a>
                                                </li>
                                                <li
                                                    class="{{ Request::segment(4) == 'reset_password' ? 'active' : '' }}">
                                                    <a
                                                        href="{{ url('program/' . session('config') . '/profile/reset_password') }}"><span
                                                            class="fa fa-unlock"></span> Reset
                                                        Password</a>
                                                </li>

                                            @else
                                                <li class="{{ Request::segment(1) == 'edit' ? 'active' : '' }}">
                                                    <a href="{{ route('edit-profile_admin') }}"><span
                                                            class="fa fa-user"></span> Profile</a>
                                                </li>
                                                <li
                                                    class="{{ Request::segment(1) == 'v_change-password' ? 'active' : '' }}">
                                                    <a href="{{ route('view_change_password-admin') }}"><span
                                                            class="fa fa-key"></span> Change
                                                        Password</a>
                                                </li>
                                                <li
                                                    class="{{ Request::segment(1) == 'reset-password' ? 'active' : '' }}">
                                                    <a href="{{ route('reset_password-profile_admin') }}"><span
                                                            class="fa fa-unlock"></span> Reset
                                                        Password</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-md-8">
                                @yield('content_edit_profile')
                            </div>
                        </div>
                        <style>
                            @media (max-width: 767px) {
                                .view-account .side-bar .user-info .img-profile {
                                    width: 60px !important;
                                    height: 60px !important;
                                }


                            }

                            @media (min-width: 961px) {
                                .sidebar-horizontal .side-menu ul {
                                    position: absolute;
                                    top: 0;
                                    left: unset;
                                    background: #ffffff;
                                    z-index: 7;
                                    margin-left: 0;
                                    width: 22.92308em;
                                    padding: 0.76923em 0;
                                    display: none;
                                    line-height: 2.76923em;
                                    -webkit-box-shadow: 0;
                                    box-shadow: unset;
                                }

                                .sidebar-horizontal .side-menu ul li {
                                    background: #fefefe;
                                }
                            }

                        </style>


                    </div>
                </section>
            </div>
        </div>

    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        $('#passwordForm').on('submit', function(event) {
            console.log("tets");
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            $.ajax({
                url: "{{ route('change_password') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $(".btn-save").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".btn-save").html(
                        '<i class="material-icons list-icon fs-24">https</i> Update Password');
                    noti(data.icon, data.success);

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
@endsection
