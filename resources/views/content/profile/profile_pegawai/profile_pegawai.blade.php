@extends('content/profile/layout/profile')
@section('content_profile')
    <div>
        <form class="form-horizontal" id="formUpdate" action="javascript:void(0)">
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Personal Info</h3>
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 col-form-label">NIP</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nip" value="{{ $data['nip'] }}"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 col-form-label">NIK</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nik" value="{{ $data['nik'] }}"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 col-form-label">NUPTK</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nuptk" value="{{ $data['nuptk'] }}"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 col-form-label">Nama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Contact Info</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="l30">Jenkel</label>
                            <select name="jenkel" class="form-control">
                                <option value="">Pilih Jenis Kelamin..</option>
                                <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Agama</label>
                            <select name="agama" class="form-control">
                                <option value="">Pilih Agama..</option>
                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Tempat Lahir</label>
                            <input class="form-control" name="tempat_lahir" value="{{ $data['tempat_lahir'] }}"
                                type="text">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="l30">Telepon</label>
                            <input class="form-control" name="telepon" value="{{ $data['telepon'] }}" type="text"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                        <div class="form-group">
                            <label for="l30">Email</label>
                            <input class="form-control" name="email" value="{{ $data['email'] }}" type="text">
                        </div>
                        <div class="form-group">
                            <label for="l30">Tanggal Lahir</label>
                            <input class="form-control datepicker" name="tgl_lahir"
                                value="{{ date('d-m-Y', strtotime($data['tgl_lahir'])) }}" readonly type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 col-form-label">Alamat</label>
                        <div class="col-md-12">
                            <textarea name="alamat" rows="3" class="form-control">{{ $data['alamat'] }}</textarea>
                        </div>
                        <label class="col-md-1 mt-2 col-form-label">RT</label>
                        <div class="col-md-2 mt-2">
                            <input type="text" name="rt" value="{{ $data['rt'] }}" class="form-control"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                        <label class="col-md-1 mt-2 col-form-label">RW</label>
                        <div class="col-md-2 mt-2">
                            <input type="text" name="rw" value="{{ $data['rw'] }}" class="form-control"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                        <label class="col-md-2 mt-2 col-form-label">Dusun</label>
                        <div class="col-md-4 mt-2">
                            <input type="text" name="dusun" value="{{ $data['dusun'] }}" class="form-control">
                        </div>
                        <label class="col-md-2 mt-2 col-form-label">Kelurahan</label>
                        <div class="col-md-4 mt-2">
                            <input type="text" name="kelurahan" value="{{ $data['kelurahan'] }}" class="form-control">
                        </div>
                        <label class="col-md-2 mt-2 col-form-label">Kecamatan</label>
                        <div class="col-md-4 mt-2">
                            <input type="text" name="kecamatan" value="{{ $data['kecamatan'] }}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="l30">Kode Pos</label>
                            <input type="text" name="kod_pos" value="{{ $data['kode_pos'] }}" class="form-control"
                                onkeypress="return hanyaAngka(event)">
                        </div>
                        <div class="form-group">
                            <label for="l30">Jabatan</label>
                            <select name="id_jabatan_pegawai" class="form-control">
                                <option value="">-- Pilih Jabatan --</option>
                                <option value="" {{ $data['id_jabatan_pegawai'] == null ? 'selected' : '' }}>Kosong
                                </option>
                                @foreach ($jabatan as $jb)
                                    <option value="{{ $jb['id'] }}"
                                        {{ $data['id_jabatan_pegawai'] == $jb['id'] ? 'selected' : '' }}>
                                        {{ $jb['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="l30">TMT Golongan</label>
                            <input type="text" name="tmt_golongan" class="form-control datepicker" readonly
                                value="{{ date('d-m-Y', strtotime($data['tmt_golongan'])) }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">TMT Capeg</label>
                            <input type="text" name="tmt_capeg" class="form-control datepicker" readonly
                                value="{{ date('d-m-Y', strtotime($data['tmt_capeg'])) }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">Gaji Pokok</label>
                            <input type="text" name="gaji_pokok" class="form-control ribuan"
                                value="{{ str_replace(',', '.', number_format($data['gaji_pokok'])) }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">Masa Kerja Golongan</label>
                            <input type="text" name="masa_kerja_golongan" class="form-control"
                                value="{{ $data['masa_kerja_golongan'] }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">NPWP</label>
                            <input type="text" name="npwp" class="form-control" value="{{ $data['npwp'] }}"
                                onkeypress="return hanyaAngka(event)">
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="l30">SK Terakhir</label>
                            <select name="id_sk" class="form-control">
                                <option value="">-- Pilih SK terakhir --</option>
                                <option value="" {{ $data['id_sk'] == null ? 'selected' : '' }}>Kosong</option>
                                @foreach ($sk as $s)
                                    <option value="{{ $s['id'] }}"
                                        {{ $data['id_sk'] == $s['id'] ? 'selected' : '' }}>{{ $s['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Pangkat</label>
                            <select name="id_pangkat_pegawai" class="form-control">
                                <option value="">-- Pilih Pangkat --</option>
                                <option value="" {{ $data['id_pangkat_pegawai'] == null ? 'selected' : '' }}>Kosong
                                </option>
                                @foreach ($pangkat as $pk)
                                    <option value="{{ $pk['id'] }}"
                                        {{ $data['id_pangkat_pegawai'] == $pk['id'] ? 'selected' : '' }}>
                                        {{ $pk['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Jenis Pegawai</label>
                            <select name="id_jenis_pegawai" class="form-control">
                                <option value="">-- Pilih Jenis --</option>
                                <option value="" {{ $data['id_jenis_pegawai'] == null ? 'selected' : '' }}>Kosong
                                </option>
                                @foreach ($jenis as $jn)
                                    <option value="{{ $jn['id'] }}"
                                        {{ $data['id_jenis_pegawai'] == $jn['id'] ? 'selected' : '' }}>
                                        {{ $jn['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Status Pegawai</label>
                            <select name="id_status_pegawai" class="form-control">
                                <option value="">-- Pilih Status --</option>
                                <option value="" {{ $data['id_status_pegawai'] == null ? 'selected' : '' }}>Kosong
                                </option>
                                @foreach ($status as $st)
                                    <option value="{{ $st['id'] }}"
                                        {{ $data['id_status_pegawai'] == $st['id'] ? 'selected' : '' }}>
                                        {{ $st['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Digaji menurut</label>
                            <input type="text" name="digaji_menurut" class="form-control"
                                value="{{ $data['digaji_menurut'] }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">Masa Kerja Keseluruhan</label>
                            <input type="text" name="masa_kerja_keseluruhan" class="form-control"
                                value="{{ $data['masa_kerja_keseluruhan'] }}">
                        </div>

                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>
        <script>
            $('body').on('submit', '#formUpdate', function(e) {
                e.preventDefault();
                $(".btn-update").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $(".btn-update").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('update-profile_tanpa_slug') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            window.location.reload(true);
                        }
                        noti(data.icon, data.message);
                        $('.btn-update').html('Update Profile');
                        $(".btn-update").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('.btn-update').html('Update Profile');
                    }
                });
            });
        </script>
    @endsection
