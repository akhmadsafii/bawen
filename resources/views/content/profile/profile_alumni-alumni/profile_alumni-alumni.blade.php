@extends('content/profile/layout/profile')
@section('content_profile')
    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    <div>
        <form method="POST" action="{{ route('update-profile_tanpa_slug') }}" enctype="multipart/form-data"
            onsubmit="return loader()">
            @csrf
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Personal Info</h3>
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nomor Induk</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="no_induk" value="{{ $data['no_induk'] }}"
                                readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Contact Info</h3>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" name="email" id="email" value="{{ $data['email'] }}"
                                class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Jurusan</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="id_jurusan" id="id_jurusan" class="form-control">
                                <option value="">-- Pilih Jurusan -- </option>
                                @foreach ($jurusan as $jr)
                                    <option value="{{ $jr['id'] }}"
                                        {{ $data['id_jurusan'] == $jr['id'] ? 'selected' : '' }}>{{ $jr['nama'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Telepon</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" name="telepon" id="telepon" value="{{ $data['telepon'] }}"
                                class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Agama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="agama" id="agama" class="form-control">
                                <option value="">-- Pilih Agama --</option>
                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Jenis Kelamin</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tempat lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tanggal lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker"
                                value="{{ date('d-m-Y', strtotime($data['tgl_lahir'])) }}"
                                data-plugin-options="{'autoclose': true}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tahun Lulus</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="tahun_lulus" id="tahun_lulus" class="form-control">
                                @php
                                    $firstYear = (int) date('Y');
                                    $lastYear = $firstYear - 20;
                                @endphp
                                @for ($year = $firstYear; $year >= $lastYear; $year--)
                                    <option value="{{ $year }}"
                                        {{ $data['tahun_lulus'] == $year ? 'selected' : '' }}>{{ $year }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Nomor Ijazah</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" name="no_ijazah" id="no_ijazah" value="{{ $data['no_ijazah'] }}"
                                class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Alamat</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <textarea name="alamat" id="alamat" class="form-control"
                                rows="3">{{ $data['alamat'] }}</textarea>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function loader() {
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $(".btn-update").attr("disabled", true);
        }
    </script>
@endsection
