@extends('content/profile/layout/profile_bkk')
@section('content_edit_profile')
    <style>
        .separator {
            border-right: 1px solid #dfdfe0;
        }

        .icon-btn-save {
            padding-top: 0;
            padding-bottom: 0;
        }

        .input-group {
            margin-bottom: 10px;
        }

        .btn-save-label {
            position: relative;
            left: -12px;
            display: inline-block;
            padding: 6px 12px;
            background: rgba(0, 0, 0, 0.15);
            border-radius: 3px 0 0 3px;
        }

    </style>


    <div class="content-panel mx-0">
        <div class="col-lg-12 col-md-12">

            <form class="form-horizontal w-100" id="formChangePassword">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="fa fa-th-list"></span>
                            Ubah Password
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-4 separator social-login-box"> <br>
                                <img alt="" class="img-thumbnail" src="{{ $data['file'] }}">
                            </div>
                            <div style="margin-top:43px;" class="col-xs-6 col-sm-6 col-md-4 login-box">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-lock"></span></div>
                                        <input class="form-control" type="password" name="current_password"
                                            placeholder="Password lama">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-lock"></span></div>
                                        <input class="form-control" type="password" name="new_password"
                                            placeholder="Password Baru">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-sign-in"></span></div>
                                        <input class="form-control" type="password" name="confirm_password"
                                            placeholder="Ulangi Password Baru">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-4"></div>
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <button class="btn icon-btn-save btn-success" type="submit" style="height: 36px;">
                                    <span class="btn-save-label"><i class="fa fa-floppy-o"></i></span>save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <script>
        $("#formChangePassword").on('submit', function(event) {
            event.preventDefault();
            $(".icon-btn-save ").html(
                '<i class="fa fa-spin fa-spinner" style="36px"></i> Loading');
            $(".icon-btn-save ").attr("disabled", true);
            $.ajax({
                url: "{{ route('change_password') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formChangePassword').trigger("reset");
                    }
                    noti(data.icon, data.success);
                    $('.icon-btn-save').html(
                        '<span class="btn-save-label"><i class="fa fa-floppy-o"></i></span></span> save'
                        );
                    $(".icon-btn-save").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>
@endsection
