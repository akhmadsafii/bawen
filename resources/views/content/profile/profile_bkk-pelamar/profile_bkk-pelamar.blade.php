@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="row widget-holder">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-8">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                </div>
                <div class="col-md-6 col-4">
                    <button id="btnPelamar" class="btn btn-info float-right"><i class="far fa-save"></i> <span
                            class="d-none d-sm-inline-block ml-1">Simpan</span>
                    </button>
                </div>
            </div>

            <hr>
            <form id="formUpdate">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ $data['nama'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                value="{{ $data['email'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Agama</label>
                            <select name="agama" id="agama" class="form-control">
                                <option value="">Pilih Agama..</option>
                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Kelamin</label>
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="">Pilih Jenkel..</option>
                                <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Pendidikan Akhir</label>
                            <select name="pendidikan_akhir" id="pendidikan_akhir" class="form-control">
                                <option value="">Pilih Pendidikan..</option>
                                <option value="sd" {{ $data['pendidikan_akhir'] == 'sd' ? 'selected' : '' }}>SD</option>
                                <option value="smp" {{ $data['pendidikan_akhir'] == 'smp' ? 'selected' : '' }}>SMP
                                </option>
                                <option value="sma" {{ $data['pendidikan_akhir'] == 'sma' ? 'selected' : '' }}>SMA
                                </option>
                                <option value="d3" {{ $data['pendidikan_akhir'] == 'd3' ? 'selected' : '' }}>D3</option>
                                <option value="s1" {{ $data['pendidikan_akhir'] == 's1' ? 'selected' : '' }}>S1</option>
                                <option value="s2" {{ $data['pendidikan_akhir'] == 's2' ? 'selected' : '' }}>S2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Telepon</label>
                            <input type="text" class="form-control" name="telepon" id="telepon"
                                value="{{ $data['telepon'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Lahir</label>
                            <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" readonly
                                value="{{ date('d-m-Y', strtotime($data['tgl_lahir'])) }}">
                        </div>
                        <div class="form-group">
                            <label for="">Alumni dari sekolah ini?</label>
                            <select name="alumni" id="alumni" class="form-control">
                                <option value="1" {{ $data['alumni'] == 1 ? 'selected' : '' }}>Ya</option>
                                <option value="0" {{ $data['alumni'] == 0 ? 'selected' : '' }}>Tidak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Asal Sekolah <small class="text-info">(Kosongi bila anda
                                    alumni)</small></label>
                            <input type="text" class="form-control" name="asal_sekolah" id="asal_sekolah"
                                value="{{ $data['asal_sekolah'] }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" rows="3">{{ $data['alamat'] }}</textarea>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formUpdate', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnPelamar").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate..');
                $("#btnPelamar").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "profile/pelamar",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        $('#btnPelamar').html(
                            '<i class="far fa-save"></i><span class="d-none d-sm-inline-block ml-1">Simpan</span>'
                            );
                        $("#btnPelamar").attr("disabled", false);
                        noti(data.icon, data.message);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPelamar').html(
                            '<i class="far fa-save"></i><span class="d-none d-sm-inline-block ml-1">Simpan</span>'
                            );
                        $("#btnPelamar").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '#btnPelamar', function() {
                $('#formUpdate').submit();
            })
        })
    </script>
@endsection
