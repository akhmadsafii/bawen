@extends('template.template_default.app')
@section('content')


	<div class="container p-5 bg-white">
        <form class="form-horizontal" id="formUpdate" method="POST" enctype="multipart/form-data" action="{{route('update-profile-sarana')}}">
        	@csrf
            <fieldset class="fieldset">
                <h3 class="title-content mb-4">Personal Info</h3>
                <div class="form-group avatar">
                    <div class="row align-items-center">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="" width="100px" height="100px">
                        </figure>
                        <div class="form-inline col-md-8 col-sm-6 col-xs-6">
                            <input type="file" class="file-uploader pull-left" id="images" value="{{ $data['file'] }}" name="image" accept="*"
                                onchange="" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        
                        <div class="col-md-10 col-sm-9 col-xs-12">

                            <input type="hidden" class="form-control" name="id" value="{{$data['id']}}" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{$data['nama']}}">
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Username</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="username" value="{{$data['username']}}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h5 class="fieldset-title my-5">Informasi Pribadi</h5>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="email" class="form-control" name="email" value="{{$data['email']}}">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Jenkel</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="l">Laki - laki</option>
                                <option value="p">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Agama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="katolik">Katolik</option>
                                <option value="hindu">Hindu</option>
                                <option value="budha">Budha</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tempat lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{$data['tempat_lahir']}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tanggal lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="date" class="form-control" name="tgl_lahir"
                                value="{{$data['tgl_lahir']}}" placeholder="{{$data['tgl_lahir']}}">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Telepon</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="number" class="form-control" value="{{$data['telepon']}}" name="telepon">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Alamat</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input name="alamat" id="alamat" class="form-control"
                                rows="3" value="{{$data['alamat']}}" placeholder="{{$data['alamat']}}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <script>
    	$.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}
        });

        function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'bottom-right',
        });
        return true;
        }

    	$('body').on('submit', '#formPassword', function(e) {
                e.preventDefault();
                $("#btn-password").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btn-password").attr("disabled", true);
            
                let data = {
                	current_password :$("#current_password").val(),
                	new_password : $("#new_password").val() ,
                	confirm_password : $("#confirm_password").val()
                };

                $.ajax({
                    type: "POST",
                    url: "{{ route('change_password') }}",
                    data: data,
                    dataType: 'json',
                    success: (data) => {
                        swa(data.status + "!", data.success, data.icon);

                        $('#modal-password').modal("hide");
                        $('#btn-password').html('Password Profile');
                        $("#btn-password").attr("disabled", false);

                    },
                    error: function(data) {
                        swa(data.status + "!", data.success, data.icon);
                        $('#btn-password').html('Update Profile');
                    }
                });
        });

        $('#modal-password').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });

        </script>

@endsection