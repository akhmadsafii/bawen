@extends('content/profile/layout/profile')
@section('content_profile')
    <div class="card">
        <div class="card-header alert-info">
            <div class="row">
                <div class="col-md-8 my-auto">
                    <h3 class="box-title">Update Profile</h3>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-success float-right btn-update" onclick="updateProfile()"><i
                            class="fas fa-save"></i>
                        Simpan</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" id="formUpdate" action="javascript:void(0)">
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-9 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sample2UserName">Nama</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Email</label>
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" value="{{ $data['email'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">NIP</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nip" value="{{ $data['nip'] }}"
                                    onkeypress="return hanyaAngka(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">NIK</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nik" value="{{ $data['nik'] }}"
                                    onkeypress="return hanyaAngka(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">NUPTK</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nuptk" value="{{ $data['nuptk'] }}"
                                    onkeypress="return hanyaAngka(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Jenkel</label>
                            <div class="input-group">
                                <select name="jenkel" class="form-control">
                                    <option value="">Pilih Jenis Kelamin..</option>
                                    <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                    <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sample2UserName">Agama</label>
                            <div class="input-group">
                                <select name="agama" class="form-control">
                                    <option value="">Pilih Agama..</option>
                                    <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                    <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                    </option>
                                    <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu
                                    </option>
                                    <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Telepon</label>
                            <div class="input-group">
                                <input type="text" name="telepon" class="form-control" value="{{ $data['telepon'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Tempat Lahir</label>
                            <div class="input-group">
                                <input type="text" name="telepon" class="form-control" value="{{ $data['telepon'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Tempat Lahir</label>
                            <div class="input-group">
                                <input type="text" name="tempat_lahir" class="form-control"
                                    value="{{ $data['telepon'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Tanggal Lahir</label>
                            <div class="input-group">
                                <div class="input-group">
                                    <input type="text" name="tanggal_lahir"
                                        value="{{ date('d-m-Y', strtotime($data['tgl_lahir'])) }}"
                                        class="form-control datepicker">
                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sample2UserName">Alamat</label>
                            <div class="input-group">
                                <textarea name="alamat" rows="3" class="form-control">{{ $data['alamat'] }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formUpdate', function(e) {
                e.preventDefault();
                $(".btn-update").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $(".btn-update").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('update-profile_tanpa_slug') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            window.location.reload(true);
                        } else {
                            $('.btn-update').html('<i class="fas fa-save"></i> Simpan');
                            $(".btn-update").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('.btn-update').html('<i class="fas fa-save"></i> Simpan');
                        $(".btn-update").attr("disabled", false);
                    }
                });
            });
        })

        function updateProfile() {
            $('#formUpdate').submit();
        }
    </script>
@endsection
