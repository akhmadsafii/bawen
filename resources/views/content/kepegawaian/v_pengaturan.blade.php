@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    <div class="card">
                                        <div class="card-header bg-info">
                                            <h5 class="box-title text-white">
                                                <i class="fa-solid fa-screwdriver-wrench"></i> Pengaturan Program
                                            </h5>
                                        </div>
                                        <div class="card-body">
                                            <form id="formSetting">
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Nama Profil Web</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="nama" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['nama'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">jenjang</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="jenjang" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['jenjang'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Provinsi</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="provinsi" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['provinsi'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Kota/Kabupaten</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="kabupaten" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['kabupaten'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Kecamatan</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="kecamatan" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['kecamatan'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Kelurahan</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="kelurahan" class="form-control"
                                                            value="{{ !empty($setting) ? $setting['kelurahan'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 mb-0 text-left text-sm-right"
                                                        for="samplePhone">Alamat</label>
                                                    <div class="col-sm-10">
                                                        <textarea name="alamat" rows="3"
                                                            class="form-control">{{ !empty($setting) ? $setting['alamat'] : '' }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-2 my-auto text-left text-sm-right"
                                                        for="samplePhone">Logo Objek</label>
                                                    <div class="col-md-10">
                                                        <div class="row mb-2">
                                                            <div class="col-md-3">
                                                                <img id="modal-preview"
                                                                    src="
                                                                        {{ !empty($setting) ? $setting['file'] : 'https://via.placeholder.com/150' }}"
                                                                    alt="Preview" class="form-group mb-1" width="100%"
                                                                    style="margin-top: 10px">
                                                            </div>
                                                        </div>
                                                        <input id="l16" type="file" name="image" accept="image/*"
                                                            onchange="readURL(this);">
                                                        <br><small class="text-muted">Ukuran Logo tidak boleh melebihi
                                                            1MB</small>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="form-group row">
                                                        <div class="col-md-10 ml-md-auto btn-list">
                                                            <button class="btn btn-info btn-rounded" type="submit"
                                                                id="saveBtn">Update</button>
                                                            <button class="btn btn-outline-default btn-rounded"
                                                                type="button">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('body').on('submit', '#formSetting', function(e) {
                    $("#saveBtn").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#saveBtn").attr("disabled", true);
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('kepegawaian-save_setting') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            if (data.status == 'berhasil') {
                                $('#CustomerForm').trigger("reset");
                                $('#ajaxModel').modal('hide');
                                var oTable = $('#data-tabel').dataTable();
                                oTable.fnDraw(false);
                            }
                            noti(data.icon, data.message);
                            $('#saveBtn').html('Update');
                            $("#saveBtn").attr("disabled", false);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Update');
                            $("#saveBtn").attr("disabled", false);
                        }
                    });
                });
            })
        </script>
    @endsection
