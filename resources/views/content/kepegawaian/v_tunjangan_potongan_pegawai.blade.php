@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template' . $ext . '/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">

                    <div class="row mb-2">
                        <div class="col-md-6">
                            <ul class="list-unstyled widget-user-list mb-0">
                                <li class="media">
                                    <div class="d-flex mr-3">
                                        <a href="#" class="user--online thumb-xs">
                                            <img src="{{ $pegawai['file'] }}" class="rounded-circle" alt="">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading"><a href="#">{{ $pegawai['nama'] }}</a>
                                            <small>NIP. {{ $pegawai['nip'] }}</small>
                                        </h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 my-auto">
                            <a href="{{ route('kepegawaian-pegawai') }}" class="btn btn-danger float-right"><i
                                    class="fas fa-angle-double-left"></i> Kembali</a>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header bg-info">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <h3 class="box-title mr-b-0 text-white">
                                                Potongan tunjangan setting</h3>
                                        </div>
                                        <div class="col-md-5">
                                            <form class="float-right">
                                                <div class="col">
                                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control">
                                                        <option value="">Pilih tahun ajaran..</option>
                                                        @foreach ($tahun as $th)
                                                            <option value="{{ substr($th['tahun_ajaran'], 0, 4) }}"
                                                                {{ substr($th['tahun_ajaran'], 0, 4) == $_GET['tahun'] ? 'selected' : '' }}>
                                                                {{ $th['tahun_ajaran'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form id="formSetting">
                                    <div class="modal-body">
                                        <div class="tabs tabs-vertical">
                                            <ul class="nav nav-tabs flex-column">
                                                <li class="nav-item" aria-expanded="false"><a class="nav-link active"
                                                        href="#tunjangan" data-toggle="tab"
                                                        aria-expanded="true">Tunjangan</a>
                                                </li>
                                                <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                                        href="#potongan" data-toggle="tab" aria-expanded="true">Potongan</a>
                                                </li>
                                                <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                                        href="#lembur" data-toggle="tab" aria-expanded="true">Lembur</a>
                                                </li>
                                            </ul>
                                            <!-- /.nav-tabs -->
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tunjangan" aria-expanded="true">
                                                    <div class="col-sm-12 mb-3">
                                                        <div class="card card-outline-color-scheme">
                                                            <div class="card-header">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <h3 class="box-title mr-b-0">
                                                                            Tunjangan</h3>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <button type="button"
                                                                            class="btn btn-primary float-right btnSubmit"
                                                                            id="btnTunjangan">
                                                                            <i class="fas fa-save"></i>
                                                                            Simpan
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="card-body" id="check0">
                                                                <div class="checkbox">
                                                                    <label class="checkbox-checked">
                                                                        <input type="checkbox" name="select-all"
                                                                            id="check_all_tunjangan" class="select-all">
                                                                        <span class="label-text">Select/Desellect
                                                                            All</span>
                                                                    </label>
                                                                </div>
                                                                @php
                                                                    $no = 1;
                                                                    $jml_tunjangan = 0;
                                                                @endphp
                                                                @foreach ($tunjangan['tunjangan_potongan'] as $tnj)
                                                                    @if ($tnj['jenis'] == 'tunjangan')
                                                                        <div class="checkbox ml-4">
                                                                            <label>
                                                                                <input type="hidden"
                                                                                    name="id_tunjangan_{{ $no }}"
                                                                                    value="{{ $tnj['id'] }}">
                                                                                <input type="checkbox" id="option_check"
                                                                                    class="check_tunjangan"
                                                                                    name="tunjangan_check_{{ $no }}"
                                                                                    value="1"
                                                                                    {{ $tnj['status_terima'] == 1 ? 'checked' : '' }}>
                                                                                <span
                                                                                    class="label-text">{{ $tnj['nama'] }}</span>
                                                                            </label>
                                                                        </div>
                                                                        @php
                                                                            $no++;
                                                                            $jml_tunjangan += 1;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                            <input type="hidden" name="jumlah_tunjangan"
                                                                value="{{ $jml_tunjangan }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="potongan" aria-expanded="false">
                                                    <div class="col-sm-12 mb-3">
                                                        <div class="card card-outline-color-scheme">
                                                            <div class="card-header">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <h3 class="box-title mr-b-0">
                                                                            Potongan</h3>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <button type="button"
                                                                            class="btn btn-primary float-right btnSubmit"
                                                                            id="btnPotongan">
                                                                            <i class="fas fa-save"></i>
                                                                            Simpan
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-body" id="check1">
                                                                <div class="checkbox">
                                                                    <label class="checkbox-checked">
                                                                        <input type="checkbox" name="select-all"
                                                                            id="check_all_potongan" class="select-all">
                                                                        <span class="label-text">Select/Desellect
                                                                            All</span>
                                                                    </label>
                                                                </div>
                                                                @php
                                                                    $nomor = 1;
                                                                    $jumlah_potongan = 0;
                                                                @endphp
                                                                @foreach ($tunjangan['tunjangan_potongan'] as $potongan)
                                                                    @if ($potongan['jenis'] == 'potongan')
                                                                        <div class="checkbox ml-4">
                                                                            <label>
                                                                                <input type="hidden"
                                                                                    name="id_potongan_{{ $nomor }}"
                                                                                    value="{{ $potongan['id'] }}">
                                                                                <input type="checkbox" id="option_check"
                                                                                    class="check_potongan"
                                                                                    name="potongan_check_{{ $nomor }}"
                                                                                    value="1"
                                                                                    {{ $potongan['status_terima'] == 1 ? 'checked' : '' }}>
                                                                                <span
                                                                                    class="label-text">{{ $potongan['nama'] }}</span>
                                                                            </label>
                                                                        </div>
                                                                        @php
                                                                            $nomor++;
                                                                            $jumlah_potongan += 1;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                <input type="hidden" name="jumlah_potongan"
                                                                    value="{{ $jumlah_potongan }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="lembur" aria-expanded="false">

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id_pegawai = "{{ $_GET['k'] }}";
            var tahun = "{{ $_GET['tahun'] }}";

            $('#check_all_tunjangan').change(function() {
                $('.check_tunjangan').prop('checked', this.checked);
            });

            $('#check_all_potongan').change(function() {
                $('.check_potongan').prop('checked', this.checked);
            });

            $('.check_tunjangan').change(function() {
                if ($('.check_tunjangan:checked').length == $('.check_tunjangan').length) {
                    $('#check_all_tunjangan').prop('checked', true);
                } else {
                    $('#check_all_tunjangan').prop('checked', false);
                }
            });

            $('.check_potongan').change(function() {
                if ($('.check_potongan:checked').length == $('.check_potongan').length) {
                    $('#check_all_potongan').prop('checked', true);
                } else {
                    $('#check_all_potongan').prop('checked', false);
                }
            });

            $('#formSetting').on('submit', function(event) {
                event.preventDefault();
                $(".btnSubmit").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                $(".btnSubmit").attr("disabled", true);
                let tunjangan = $('#jenis').val();
                $.ajax({
                    url: "{{ route('kepegawaian-tunjangan_potongan_pegawai_simpan') }}",
                    method: "POST",
                    data: $(this).serialize() + '&pegawai=' + id_pegawai + '&tahun=' + tahun,
                    dataType: "json",
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('.btnSubmit').html('<i class="fas fa-save"></i> Simpan');
                        $(".btnSubmit").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('.btnSubmit').html('<i class="fas fa-save"></i> Simpan');
                        $(".btnSubmit").attr("disabled", false);
                    }
                });
            });

            $('#btnPotongan, #btnTunjangan').click(function() {
                $('#formSetting').submit();
            });

            $('#tahun_ajaran').change(function() {
                loadSettingPegawai($(this).val())
            });
        });

        function loadSettingPegawai(tahun) {
            var empty = tahun === null;
            //console.log(jadwal, kelas)
            if (!empty) {
                // $('#loading').removeClass('d-none');
                window.location.href = 'detail?method=setting&nip={{ $_GET['nip'] }}&k={{ $_GET['k'] }}&tahun=' +
                    tahun;
            } else {
                console.log('empty')
            }
        }
    </script>
@endsection
