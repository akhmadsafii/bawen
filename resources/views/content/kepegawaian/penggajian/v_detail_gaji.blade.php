@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template' . $ext . '/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .table td,
        .table th {
            border: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <a href="{{ url()->previous() }}" class="btn btn-outline-info mt-1"><i
                                    class="fas fa-arrow-alt-circle-left"></i>
                                Kembali</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-info my-auto">
                            <div class="row">
                                <div class="col-md-6 my-auto">
                                    <h3 class="box-title m-0 text-white"> <i class="fas fa-file-invoice-dollar"></i>
                                        Penggajian</h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-right">
                                        <center>
                                            <b>Pegawai : {{ $detail['nama'] }}</b> <br>
                                            <a href="{{ route('kepegawaian-tunjangan_penggajian_detail', ['method' => 'edit', 'nip' => $detail['nip'], 'k' => (new \App\Helpers\Help())->encode($detail['id']), 'bulan' => $_GET['bulan'], 'tahun' => $_GET['tahun']]) }}"
                                                class="mx-2 btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-info btn-sm btnPrint"><i
                                                    class="fas fa-print"></i></a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="page_print">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Data Detail Gaji Bulan || {{ $detail['bulan'] }} /
                                        {{ $detail['tahun_ajaran'] }}
                                    </label><br>
                                    <label>NIP: {{ $detail['nip'] }} / {{ $detail['nama'] }}</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-right">

                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="table-responsive col-md-12">
                                    <table class="table">
                                        <tr>
                                            <th class="text-center alert-warning p-2">Tunjangan</th>
                                            <th class="text-center alert-success p-2">Potongan</th>
                                        </tr>
                                        <tr>
                                            <td class="align-top">
                                                <table class="w-100">
                                                    <tr>
                                                        <td>Gaji Pokok</td>
                                                        <td>:</td>
                                                        <td class="text-right">
                                                            {{ str_replace(',', '.', number_format($detail['gaji_pokok'])) }}
                                                        </td>
                                                        <td width="50"></td>
                                                    </tr>
                                                    @php
                                                        $total_kotor = $detail['gaji_pokok'];
                                                    @endphp
                                                    @foreach ($detail['tunjangan_potongan'] as $tunjangan)
                                                        @if ($tunjangan['jenis'] == 'tunjangan')
                                                            <tr>
                                                                <td>{{ $tunjangan['nama'] }}</td>
                                                                <td>:</td>
                                                                <td class="text-right">
                                                                    {{ str_replace(',', '.', number_format($tunjangan['nominal'])) }}
                                                                </td>
                                                                <td width="50"></td>
                                                            </tr>
                                                            @php
                                                                $total_kotor += $tunjangan['nominal'];
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    <tr>
                                                        <th>JUMLAH KOTOR</th>
                                                        <th>:</th>
                                                        <th class="text-right">
                                                            {{ str_replace(',', '.', number_format($total_kotor)) }}
                                                        </th>
                                                        <th width="50"></th>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="align-top">
                                                <table class="w-100">
                                                    @php
                                                        $total_potongan = 0;
                                                    @endphp
                                                    @foreach ($detail['tunjangan_potongan'] as $potongan)
                                                        @if ($potongan['jenis'] == 'potongan')
                                                            <tr>
                                                                <td>{{ $potongan['nama'] }}</td>
                                                                <td>:</td>
                                                                <td class="text-right">
                                                                    {{ str_replace(',', '.', number_format($potongan['nominal'])) }}
                                                                </td>
                                                                <td width="50"></td>
                                                            </tr>
                                                            @php
                                                                $total_potongan += $potongan['nominal'];
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    <tr>
                                                        <th>POTONGAN</th>
                                                        <th>:</th>
                                                        <th class="text-right">
                                                            {{ str_replace(',', '.', number_format($total_potongan)) }}
                                                        </th>
                                                        <th width="50"></th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <hr>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label">JUMLAH BERSIH</label>
                                        <label
                                            class="col-md-6 col-form-label float-right">{{ str_replace(',', '.', number_format($total_kotor - $total_potongan)) }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btnPrint").click(function() {
                $('#page_print').printArea();
            });
        })
    </script>
@endsection
