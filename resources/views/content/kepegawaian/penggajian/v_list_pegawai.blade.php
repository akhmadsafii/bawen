@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="box-title my-0">Filter Gaji Pegawai</h5>
                            <p>Silahkan pilih bulan dan tahun untuk memfilter data gaji dari pegawai:
                            </p>
                            <div class="form-row">
                                <div class="col-6">
                                    <select name="bulan" id="bulan" class="form-control select3">
                                        <option value="">Pilih Bulan..</option>
                                        <option value="januari" {{ $_GET['bulan'] == 'januari' ? 'selected' : '' }}>
                                            Januari</option>
                                        <option value="februari" {{ $_GET['bulan'] == 'februari' ? 'selected' : '' }}>
                                            Februari</option>
                                        <option value="maret" {{ $_GET['bulan'] == 'maret' ? 'selected' : '' }}>Maret
                                        </option>
                                        <option value="april" {{ $_GET['bulan'] == 'april' ? 'selected' : '' }}>April
                                        </option>
                                        <option value="mei" {{ $_GET['bulan'] == 'mei' ? 'selected' : '' }}>Mei</option>
                                        <option value="juni" {{ $_GET['bulan'] == 'juni' ? 'selected' : '' }}>Juni
                                        </option>
                                        <option value="juli" {{ $_GET['bulan'] == 'juli' ? 'selected' : '' }}>Juli
                                        </option>
                                        <option value="agustus" {{ $_GET['bulan'] == 'agustus' ? 'selected' : '' }}>
                                            Agustus</option>
                                        <option value="september" {{ $_GET['bulan'] == 'september' ? 'selected' : '' }}>
                                            September</option>
                                        <option value="oktober" {{ $_GET['bulan'] == 'oktober' ? 'selected' : '' }}>
                                            Oktober</option>
                                        <option value="november" {{ $_GET['bulan'] == 'november' ? 'selected' : '' }}>
                                            November</option>
                                        <option value="desember" {{ $_GET['bulan'] == 'desember' ? 'selected' : '' }}>
                                            Desember</option>
                                    </select>
                                </div>
                                <div class="col">
                                    @php
                                        $firstYear = (int) date('Y');
                                    @endphp
                                    <select name="tahun" class="form-control select3" id="tahun">
                                        <option value="">Pilih Tahun..</option>
                                        @for ($i = $firstYear - 10; $i < $firstYear + 10; $i++)
                                            <option value="{{ $i }}"
                                                {{ $_GET['tahun'] == $i ? 'selected' : '' }}>{{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <h5 class="box-title text-center mt-3">
                                {{-- @php
                                    $tahun = (int) $_GET['tahun'] + 1;
                                @endphp --}}
                                Data Penggajian Pegawai {{ $_GET['bulan'] . ' ' . $_GET['tahun'] }}</h5>
                        </div>
                    </div>
                    <div class="my-2">
                        <div class="table-responsive">
                            <table class="table table-striped widget-status-table table-hover table-bordered"
                                id="data-tabel">
                                <thead>
                                    <tr class="bg-facebook">
                                        <th class="text-white">#</th>
                                        <th class="text-white">NIP</th>
                                        <th class="text-white">Nama Pegawai</th>
                                        <th class="text-white">Bulan</th>
                                        <th class="text-white">Total Gaji</th>
                                        <th class="text-center text-white">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'bulan',
                        name: 'bulan'
                    },
                    {
                        data: 'gaji',
                        name: 'gaji',
                        className: 'text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: "text-center",
                    },
                ]
            });

            $('#tahun').change(function() {
                loadPegawai($('#bulan').val(), $(this).val())
            });
            $('#bulan').change(function() {
                loadPegawai($(this).val(), $('#tahun').val())
            });
        })

        function loadPegawai(bulan, tahun) {
            var tahun_empty = tahun === '';
            var bulan_empty = bulan === '';
            if (!bulan_empty && !tahun_empty) {
                window.location.href = 'penggajian?bulan=' + bulan + '&tahun=' + tahun;
            } else {
                console.log('empty')
            }
        }
    </script>
@endsection
