@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <a href="{{ url()->previous() }}" class="btn btn-outline-info mt-1"><i
                                    class="fas fa-arrow-alt-circle-left"></i>
                                Kembali</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-info">
                            <i class="fas fa-file-invoice-dollar"></i> Penggajian
                            <div class="float-right">
                                <b>Pegawai : {{ $pegawai['nama'] }}</b>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Data Detail Gaji Bulan || {{ ucwords($_GET['bulan']) }} /
                                        {{ $_GET['tahun'] }}
                                    </label><br>
                                    <label>NIP: {{ $pegawai['nip'] }} / {{ $pegawai['nama'] }}</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-inline float-right">
                                        <div class="form-group">
                                            <label for="inputPassword6" class="mx-sm-3">Tanggal Penggajian</label>
                                            <div class="input-group input-has-value">
                                                <input type="text" class="form-control datepicker" id="tanggal"
                                                    value="25-{{ (new \App\Helpers\Help())->getMonthToIndo($_GET['bulan']) . '-' . $_GET['tahun'] }}">
                                                <span class="input-group-addon"><i
                                                        class="list-icon material-icons">date_range</i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="alert alert-info border-info" role="alert">
                                        <button aria-label="Close" class="close" data-dismiss="alert"
                                            type="button"><span aria-hidden="true">×</span>
                                        </button>
                                        <p class="my-0"><strong>Pastikan gaji pokok telah di set terlebih dahulu
                                                di detail pegawai:</strong>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <form id="formGaji">
                                <input type="hidden" name="id_pegawai" value="{{ $_GET['k'] }}">
                                <input type="hidden" name="bulan"
                                    value="{{ (new \App\Helpers\Help())->getMonthToIndo($_GET['bulan']) }}">
                                @php
                                    $next_th = $_GET['tahun'] + 1;
                                @endphp
                                <input type="hidden" name="tahun_ajaran" value="{{ $_GET['tahun'] . '/' . $next_th }}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label font-weight-normal" for="l0">Gaji
                                                Pokok</label>
                                            <div class="col-md-7">
                                                <div class="input-group"><span class="input-group-addon">Rp </span>
                                                    <input class="form-control ribuan input_tunjangan" name="gaji_pokok"
                                                        placeholder="Gaji Pokok" type="text"
                                                        value="{{ str_replace(',', '.', number_format($pegawai['gaji_pokok'])) }}">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="jumlah_tunjangan"
                                            value="{{ count($tunjangan['tunjangan']) }}">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($tunjangan['tunjangan'] as $tnj)
                                            <div class="form-group row">
                                                <label class="col-md-5 col-form-label font-weight-normal"
                                                    for="l0">{{ $tnj['nama'] }}</label>
                                                <input type="hidden" name="id_tunjangan_{{ $no }}"
                                                    value="{{ $tnj['id'] }}">
                                                <div class="col-md-7">
                                                    <div class="input-group"><span class="input-group-addon">Rp </span>
                                                        <input class="form-control ribuan input_tunjangan"
                                                            value="{{ str_replace(',', '.', number_format($tnj['nominal'])) }}"
                                                            name="nominal_tunjangan_{{ $no }}"
                                                            placeholder="{{ $tnj['nama'] }}" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label" for="l0">JUMLAH KOTOR</label>
                                            <div class="col-md-7">
                                                <div class="input-group"><span class="input-group-addon">Rp </span>
                                                    <input class="form-control ribuan total_kotor" name="gaji_kotor"
                                                        readonly placeholder="Jumlah Kotor" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        @php
                                            $nomer = 1;
                                        @endphp
                                        <input type="hidden" name="jumlah_potongan"
                                            value="{{ count($tunjangan['potongan']) }}">
                                        @foreach ($tunjangan['potongan'] as $potongan)
                                            <div class="form-group row">
                                                <input type="hidden" name="id_potongan_{{ $nomer }}"
                                                    value="{{ $potongan['id'] }}">
                                                <label class="col-md-5 col-form-label font-weight-normal"
                                                    for="l0">{{ $potongan['nama'] }}</label>
                                                <div class="col-md-7">
                                                    <div class="input-group"><span class="input-group-addon">Rp </span>
                                                        <input class="form-control ribuan input_potongan"
                                                            name="nominal_potongan_{{ $nomer }}"
                                                            placeholder="{{ $potongan['nama'] }}"
                                                            value="{{ str_replace(',', '.', number_format($potongan['nominal'])) }}"
                                                            type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            @php
                                                $nomer++;
                                            @endphp
                                        @endforeach
                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label" for="l0">POTONGAN</label>
                                            <div class="col-md-7">
                                                <div class="input-group"><span class="input-group-addon">Rp </span>
                                                    <input class="form-control ribuan total_potongan" name="total_potongan"
                                                        readonly placeholder="Jumlah Kotor" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label" for="l0">JUMLAH BERSIH</label>
                                            <div class="col-md-7">
                                                <div class="input-group"><span class="input-group-addon">Rp </span>
                                                    <input class="form-control ribuan gaji_bersih" name="gaji_bersih"
                                                        placeholder="Jumlah Bersih" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{ route('kepegawaian-pegawai') }}" class="btn btn-danger"><i
                                                class="fas fa-times-circle"></i> Batal</a>
                                        <button type="submit" class="btn btn-success float-right" id="saveBtn"><i
                                                class="fas fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formGaji').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-tunjangan_penggajian_simpan_banyak') }}",
                    method: "POST",
                    data: $(this).serialize() + '&tanggal=' + $('#tanggal').val() +
                        '&method=insert',
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.href = data.previous_back;
                        } else {
                            $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            calculateJumlah();

            $(".input_tunjangan, .input_potongan").on("keydown keyup", function() {
                calculateJumlah();
            });
        })

        function calculateJumlah() {
            var tunjangan = 0;
            var potongan = 0;
            $(".input_tunjangan").each(function() {
                let nominal = this.value;
                let remove_thousand = nominal.split('.').join("");
                tunjangan += parseFloat(remove_thousand);
                $(this).css("background-color", "#FEFFB0");
            });
            $(".input_potongan").each(function() {
                let nominal = this.value;
                let remove_thousand = nominal.split('.').join("");
                potongan += parseFloat(remove_thousand);
                $(this).css("background-color", "#FEFFB0");
            });
            $(".total_kotor").val(tunjangan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $(".total_kotor").css("background-color", "#a9f0fb");
            $(".total_potongan").val(potongan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $(".total_potongan").css("background-color", "#a9f0fb");
            var gaji_bersih = tunjangan - potongan;
            $(".gaji_bersih").val(gaji_bersih.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $(".gaji_bersih").css("background-color", "#93ff66");
            // $(".gaji_bersih").css("color", "#fff");
        }
    </script>
@endsection
