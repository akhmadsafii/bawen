@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

        <!-- /.page-title-right -->
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-8">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="btn btn-outline-info m-1 active" onclick="changeTitle('tunjangan')"
                                        href="#tunjangan" data-toggle="tab">Tunjangan</a>
                                </li>
                                <li class="nav-item"><a class="btn btn-outline-info m-1" href="#potongan"
                                        onclick="changeTitle('potongan')" data-toggle="tab">Potongan</a></li>
                                <li class="nav-item"><a class="btn btn-outline-info m-1" href="#lembur"
                                        onclick="changeTitle('lembur')" data-toggle="tab">Lembur</a></li>
                            </ul>
                        </div>
                        <input type="hidden" name="tahun" id="tahun" value="{{ $_GET['tahun'] }}">
                        <input type="hidden" name="jenis" id="jenis" value="tunjangan">
                        <div class="col-md-4">
                            <form class="form-inline float-right">
                                <div class="form-group mb-2">
                                    <div class="input-group">
                                        <select name="tahun_ajaran" class="form-control" id="tahun_ajaran">
                                            <option value="">Filter Berdasarkan Tahun Ajaran..</option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ substr($th['tahun_ajaran'], 0, 4) }}"
                                                    {{ substr($th['tahun_ajaran'], 0, 4) == $_GET['tahun'] ? 'selected' : '' }}>
                                                    {{ substr($th['tahun_ajaran'], 0, 4) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    <form id="formTunjangan">
                                        <div class="card">
                                            <div class="card-header bg-info">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h5 class="box-title my-1 text-white">Settingan Tunjangan Tahun
                                                            {{ $_GET['tahun'] }}</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="float-right">
                                                            <button type="submit" class="btn btn-success"
                                                                id="btn_tunjangan"><i class="fas fa-cog"></i>
                                                                Simpan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-stripped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama</th>
                                                                <th>Nominal Default</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <input type="hidden" name="jumlah"
                                                                value="{{ count($tunjangan['tunjangan']) }}">
                                                            @if (!empty($tunjangan['tunjangan']))
                                                                @php
                                                                    $no = 1;
                                                                @endphp
                                                                @foreach ($tunjangan['tunjangan'] as $tnj)
                                                                    <tr>
                                                                        <input type="hidden" name="id_{{ $no }}"
                                                                            value="{{ $tnj['id'] }}">
                                                                        <td class="vertical-middle">{{ $no }}</td>
                                                                        <td class="vertical-middle">{{ $tnj['nama'] }}
                                                                        </td>
                                                                        <td width="200"><input type="text"
                                                                                name="nominal_{{ $no }}"
                                                                                value="{{ str_replace(',', '.', number_format($tnj['nominal'])) }}"
                                                                                class="form-control ribuan"></td>
                                                                    </tr>
                                                                    @php
                                                                        $no++;
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="3" class="text-center">Data saat ini
                                                                        belum tersedia</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="potongan">
                                    <form id="formPotongan">
                                        <div class="card">
                                            <div class="card-header bg-info">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h5 class="box-title my-1 text-white">Settingan Potongan tahun
                                                            {{ $_GET['tahun'] }}</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="float-right">
                                                            <button type="submit" class="btn btn-success"
                                                                id="btn_potongan"><i class="fas fa-cog"></i>
                                                                Simpan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-stripped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama</th>
                                                                <th>Nominal Default</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <input type="hidden" name="jumlah"
                                                                value="{{ count($tunjangan['potongan']) }}">
                                                            @if (!empty($tunjangan['potongan']))
                                                                @php
                                                                    $no = 1;
                                                                @endphp
                                                                @foreach ($tunjangan['potongan'] as $ptg)
                                                                    <tr>
                                                                        <input type="hidden" name="id_{{ $no }}"
                                                                            value="{{ $ptg['id'] }}">
                                                                        <td class="vertical-middle">{{ $no }}
                                                                        </td>
                                                                        <td class="vertical-middle">{{ $ptg['nama'] }}
                                                                        </td>
                                                                        <td width="200"><input type="text"
                                                                                name="nominal_{{ $no }}"
                                                                                value="{{ str_replace(',', '.', number_format($ptg['nominal'])) }}"
                                                                                class="form-control ribuan"></td>
                                                                    </tr>
                                                                    @php
                                                                        $no++;
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="3" class="text-center">Data saat ini
                                                                        belum tersedia</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $("#tahun_ajaran").change(function() {
                    loadDataTahun($(this).val())
                });

                $('#formTunjangan, #formPotongan, #formLembur').on('submit', function(event) {
                    event.preventDefault();
                    let jenis = $('#jenis').val();
                    let tahun = $('#tahun').val();
                    $("#btn_" + jenis).html(
                        '<i class="fa fa-spin fa-cog"></i> Menyimpan..');
                    $("#btn_" + jenis).attr("disabled", true);
                    updateForm($(this).serialize() + '&tahun=' + tahun + '&jenis=' + jenis, jenis);
                });

            });

            function updateForm(dataForm, jenis) {
                // console.log(data);
                $.ajax({
                    url: "{{ route('kepegawaian-tunjangan_potongan_tahun_simpan_many') }}",
                    method: "POST",
                    data: dataForm,
                    dataType: "json",
                    success: function(data) {
                        noti(data.icon, data.message);
                        $("#btn_" + jenis).html(
                            '<i class="fas fa-cog"></i> Simpan');
                        $("#btn_" + jenis).attr("disabled", false);
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $("#btn_" + jenis).html(
                            '<i class="fas fa-cog"></i> Simpan');
                        $("#btn_" + jenis).attr("disabled", false);
                    }
                });
            }

            function loadDataTahun(tahun_ajaran) {
                var empty = tahun_ajaran === null;
                if (!empty) {
                    window.location.href = 'tunjangan-potongan-tahun?tahun=' + tahun_ajaran;
                } else {
                    console.log('empty')
                }
            }

            function changeTitle(params) {
                $('#jenis').val(params);
            }
        </script>
    @endsection
