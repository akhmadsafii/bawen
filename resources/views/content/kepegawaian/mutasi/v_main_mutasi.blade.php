@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    @yield('content_mutasi')
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
