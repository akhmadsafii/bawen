@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .no_table td,
        .no_table th {
            border: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-md-5 widget-holder">
            <div class="card">
                <div class="card-header bg-info">
                    <h5 class="card-title text-white my-0">Detail Pegawai</h5>
                </div>
                <div class="card-body">
                    <div class="box-info user-profile-2">
                        <div class="user-profile-inner">
                            <div class="profile-img m-auto" style="background-image: url({{ $pegawai['file'] }});">
                            </div>
                            <h4 class="my-2 text-center">{{ ucwords($pegawai['nama']) }}</h4>
                            <div class="user-button">
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">NIP</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['nip'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">NIK</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['nik'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">NUPTK</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['nuptk'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">Email</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['email'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">Telepon</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['telepon'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">Agama</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['agama'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">Gender</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">{{ $pegawai['jenkel'] }}</p>
                                    </div>
                                </div>
                                <div class="form-group my-0 row">
                                    <label class="col-md-4 col-form-label">Tempat, Tgl lahir</label>
                                    <div class="col-md-8">
                                        <p class="form-control-plaintext">
                                            {{ $pegawai['tempat_lahir'] . ' ' . (new \App\Helpers\Help())->getTanggal($pegawai['tgl_lahir']) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 widget-holder">
            <div class="card">
                <div class="card-header bg-info">
                    <div class="row">
                        <div class="col-md-5">
                            <h5 class="card-title text-white my-0">Riwayat Mutasi</h5>
                        </div>
                        <div class="col-md-7">

                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Jenis Mutasi</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPangkat" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table no_table">
                            <tr class="bg-gray">
                                <th colspan="4" class="text-center text-white">Pangkat / Golongan : <span
                                        id="pangkat_golongan"></span></th>
                            </tr>
                            <tr class="bg-gray">
                                <th colspan="2" class="text-center text-white">Nama Pegawai : <span
                                        id="nama_pegawai">Paijo</span>
                                </th>
                                <th colspan="2" class="text-center text-white">NIP : <span id="nip">4243243535</span></th>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="my-2"></div>
                                </td>
                            </tr>
                            <tr>
                                <th class="w-25">Jabatan Pegawai</th>
                                <td class="w-25" id="jabatan_pegawai">Kenaikan Pangkat</td>
                                <th class="w-25">TMT</th>
                                <td class="w-25" id="tmt_jabatan"></td>
                            </tr>
                            <tr>
                                <th>Pangkat Pegawai</th>
                                <td id="pangkat_pegawai">Kenaikan Pangkat</td>
                                <th>TMT</th>
                                <td id="tmt_pangkat"></td>
                            </tr>
                            <tr>
                                <th>Golongan Pegawai</th>
                                <td id="golongan_pegawai">Kenaikan Pangkat</td>
                                <th>TMT</th>
                                <td id="tmt_golongan"></td>
                            </tr>
                            <tr>
                                <th>Gaji Pegawai</th>
                                <td id="gaji_pegawai">Kenaikan Pangkat</td>
                                <th>TMT</th>
                                <td id="tmt_gaji"></td>
                            </tr>
                            <tr>
                                <th>Pensiun</th>
                                <td id="pensiun_pegawai">Kenaikan Pangkat</td>
                                <th>TMT</th>
                                <td id="tmt_pensiun"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'tahun_ajaran',
                        name: 'tahun_ajaran',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan',
                        className: 'vertical-middle text-center'
                    },

                ]
            });

            $(document).on('click', '.detail', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-mutasi_guru_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<small class="text-info"><i class="fa fa-spin fa-spinner"></i> Memproses..</small>'
                        );
                    },
                    success: function(data) {
                        $(loader).html(
                            '<small class="text-info"><i class="fas fa-info-circle"></i> Lihat detail </small>'
                        );
                        $('#modelHeading').html("Detail Mutasi Pangkat");
                        $('#pangkat_golongan').html(data.pangkat_pegawai);
                        $('#nama_pegawai').html(data.nama);
                        $('#nip').html(data.nip);
                        $('#jabatan_pegawai').html(data.jabatan_pegawai);
                        $('#tmt_jabatan').html(convertDate(data.tmt_jabatan_pegawai));
                        $('#pangkat_pegawai').html(data.pangkat_pegawai);
                        $('#tmt_pangkat').html(convertDate(data.tmt_pangkat_pegawai));
                        $('#golongan_pegawai').html(data.golongan_pegawai);
                        $('#tmt_golongan').html(convertDate(data.tmt_golongan));
                        $('#gaji_pegawai').html(rubahRibuan(data.gaji_pokok));
                        $('#tmt_gaji').html(convertDate(data.tmt_gaji));
                        $('#pensiun_pegawai').html(data.pensiun);
                        $('#tmt_pensiun').html(convertDate(data.tmt_pensiun));
                        $('#modalPangkat').modal('show');
                    }
                });
            });

        });
    </script>
@endsection
