@extends('content.kepegawaian.mutasi.v_main_mutasi')
@section('content_mutasi')
    <style>
        .btn-outline-light:active,
        .btn-outline-light.active,
        .show>.btn-outline-light.dropdown-toggle {
            color: #03a9f3 !important;
        }

    </style>
    <div class="card">
        <div class="card-header bg-info">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="box-title my-2 text-white">
                        {{ $_GET['tahun'] != 'all' ? 'Data Mutasi ' . $_GET['tahun'] : 'Semua Riwayat Mutasi' }}
                    </h5>
                </div>
                <div class="col-md-8">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a class="btn btn-outline-light m-1 {{ $_GET['jenis'] == 'semua' ? 'active' : '' }}"
                                onclick="changeJenis('semua')">Semua</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-outline-light m-1 {{ $_GET['jenis'] == 'masuk' ? 'active' : '' }}"
                                onclick="changeJenis('masuk')">Masuk</a>
                        </li>
                        <li class="nav-item"><a
                                class="btn btn-outline-light m-1 {{ $_GET['jenis'] == 'keluar' ? 'active' : '' }}"
                                onclick="changeJenis('keluar')">Keluar</a></li>
                        <li class="nav-item"><a
                                class="btn btn-outline-light m-1 {{ $_GET['jenis'] == 'pangkat' ? 'active' : '' }}"
                                onclick="changeJenis('pangkat')">Pangkat</a></li>
                        <li class="nav-item">
                            <select name="tahun_ajaran" class="form-control my-1" id="tahun_ajaran">
                                <option value="">Filter Tahun Ajaran..
                                </option>
                                <option value="all" {{ $_GET['tahun'] == 'all' ? 'selected' : '' }}>Semua
                                </option>
                                @foreach ($tahun as $th)
                                    <option value="{{ substr($th['tahun_ajaran'], 0, 4) }}"
                                        {{ substr($th['tahun_ajaran'], 0, 4) == $_GET['tahun'] ? 'selected' : '' }}>
                                        {{ substr($th['tahun_ajaran'], 0, 4) }}</option>
                                @endforeach

                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 my-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="btn btn-info m-1" href="{{ route('kepegawaian-mutasi_guru_create') }}"><i
                                    class="fas fa-plus"></i> Mutasi</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-stripped table-hover table-bordered widget-status-table" id="data-tabel">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Tanggal Mutasi</th>
                                    <th rowspan="2">Pegawai</th>
                                    <th rowspan="2">Detail Mutasi</th>
                                    <th colspan="5">Detail Pangkat Mutasi</th>
                                    <th rowspan="2">Aksi</th>
                                </tr>
                                <tr>
                                    <th>Jabatan <br>TMT</th>
                                    <th>Pangkat <br>TMT</th>
                                    <th>Golongan <br>TMT</th>
                                    <th>Gaji <br>TMT</th>
                                    <th>Pensiun <br>TMT</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalMutasi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Tambah Mutasi Masuk</h5>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" id="awal"></a>
                    </li>
                    <li role="presentation">
                        <a href="#step2" data-toggle="tab" id="akhir"></a>
                    </li>
                </ul>
                <form id="formMutasi">
                    <div class="tab-content">
                        <div class="tab-pane active" id="step1">
                            <div class="modal-body py-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="box-title">Detail Pegawai</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l31">NIP</label>
                                            <input class="form-control" name="nip" onkeypress="return hanyaAngka(event)"
                                                type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Nama</label>
                                            <input class="form-control" type="text" name="nama">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">NIK</label>
                                            <input class="form-control" name="nik" onkeypress="return hanyaAngka(event)"
                                                type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">NUPTK</label>
                                            <input class="form-control" name="nuptk" onkeypress="return hanyaAngka(event)"
                                                type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Tempat Lahir</label>
                                            <input class="form-control" type="text" name="tempat_lahir">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Tanggal Lahir</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <select name="tanggal" class="form-control select3">
                                                        <option value="">Tgl</option>
                                                        @for ($i = 1; $i <= 31; $i++)
                                                            <option value="{{ $i }}">{{ $i }}
                                                            </option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="bulan" class="form-control select3">
                                                        <option value="">Bulan</option>
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="tahun" class="form-control select3">
                                                        <option value="">Tahun</option>
                                                        @php
                                                            $firstYear = (int) date('Y');
                                                            $lastYear = $firstYear - 100;
                                                            for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                                echo '<option value=' . $year . '>' . $year . '</option>';
                                                            }
                                                        @endphp
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Jenis kelamin</label>
                                            <div class="form-check my-2">
                                                <input class="form-check-input mx-0" type="radio" name="jenkel" value="l">
                                                <label class="form-check-label" for="inlineRadio1">Laki laki</label>
                                            </div>
                                            <div class="form-check my-2">
                                                <input class="form-check-input mx-0" type="radio" name="jenkel" value="p">
                                                <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Agama</label>
                                            <select name="agama" class="form-control">
                                                <option value="">Pilih Agama</option>
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Alamat</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea name="alamat" class="form-control" rows="2" placeholder="Alamat"></textarea>
                                                </div>
                                                <div class="col-md-3 my-1">
                                                    <input type="text" onkeypress="return hanyaAngka(event)" name="rt"
                                                        placeholder="RT" class="form-control">
                                                </div>
                                                <div class="col-md-3 my-1">
                                                    <input type="text" onkeypress="return hanyaAngka(event)" name="rw"
                                                        placeholder="RW" class="form-control">
                                                </div>
                                                <div class="col-md-6 my-1">
                                                    <input type="text" name="dusun" placeholder="Dusun"
                                                        class="form-control">
                                                </div>
                                                <div class="col-md-7 my-1">
                                                    <input type="text" name="desa" placeholder="Desa"
                                                        class="form-control">
                                                </div>
                                                <div class="col-md-5 my-1">
                                                    <input type="text" onkeypress="return hanyaAngka(event)" name="kode_pos"
                                                        placeholder="Kode Pos" class="form-control">
                                                </div>
                                                <div class="col-md-12 my-1">
                                                    <input type="text" name="kecamatan" placeholder="Kecamatan"
                                                        class="form-control">
                                                </div>
                                                <div class="col-md-12 my-1">
                                                    <input type="text" name="kabupaten" placeholder="Kabupaten"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">SK Terakhir</label>
                                            <select name="sk" class="form-control select3">
                                                <option value="">- SK Terakhir - </option>
                                                <option value="">Kosong</option>
                                                @foreach ($sk as $sk)
                                                    <option value="{{ $sk['id'] }}">{{ $sk['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">NPWP</label>
                                            <input type="text" name="npwp" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l31">Email</label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Golongan</label>
                                            <select name="golongan" class="form-control select3">
                                                <option value="">- Pilih Pangkat/Golongan -</option>
                                                <option value="">Kosong</option>
                                                @foreach ($golongan as $gl)
                                                    <option value="{{ $gl['id'] }}">{{ $gl['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">TMT/Golongan</label>
                                            <div class="input-group">
                                                <input type="text" name="tmt_golongan" value="{{ date('d-m-Y') }}"
                                                    class="form-control datepicker">
                                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Jenis</label>
                                            <select name="id_jenis_golongan" class="form-control select3">
                                                <option value="">- Pilih Jenis Kepegawaian -</option>
                                                <option value="">Kosong</option>
                                                @foreach ($jenis as $jn)
                                                    <option value="{{ $jn['id'] }}">{{ $jn['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">TMT Capeg</label>
                                            <div class="input-group">
                                                <input type="text" name="tmt_capeg" value="{{ date('d-m-Y') }}"
                                                    class="form-control datepicker">
                                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Status</label>
                                            <select name="id_status_pegawai" class="form-control select3">
                                                <option value="">- Pilih Status Kepegawaian -</option>
                                                <option value="">Kosong</option>
                                                @foreach ($status as $st)
                                                    <option value="{{ $st['id'] }}">{{ $st['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Jabatan</label>
                                            <select name="id_jabatan_pegawai" class="form-control select3">
                                                <option value="">- Pilih Jabatan Struktutal -</option>
                                                <option value="">Kosong</option>
                                                @foreach ($jabatan as $jbt)
                                                    <option value="{{ $jbt['id'] }}">{{ $jbt['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Sumber Gaji</label>
                                            <input type="text" name="digaji_menurut" placeholder="Digaji Menurut"
                                                value="PP No 30 Tahun 2015" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Gaji Pokok</label>
                                            <input type="text" name="gaji_pokok" class="form-control ribuan">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Whatsapp</label>
                                            <input type="text" name="telepon" onkeypress="return hanyaAngka(event)"
                                                placeholder="Nomo Whatsapp" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Masa Golongan</label>
                                            <input type="text" name="masa_kerja_golongan" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Masa Keseluruhan</label>
                                            <input type="text" name="masa_kerja_keseluruhan" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info next-step"><i class="fas fa-angle-right"></i>
                                    Lanjutkan</button>
                            </div>
                        </div>
                        <div class="tab-pane" id="step2">
                            <div class="modal-body py-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="box-title">Detail Mutasi</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l31">Tanggal Mutasi</label>
                                            <div class="input-group">
                                                <input type="text" name="tanggal_mutasi" value="{{ date('d-m-Y') }}"
                                                    class="form-control datepicker">
                                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l31">Tahun Ajaran</label>
                                            <select name="tahun_ajaran" class="form-control" id="tahun_ajaran">
                                                <option value="">Pilih Tahun Ajaran..
                                                </option>
                                                @foreach ($tahun as $th)
                                                    <option value="{{ $th['tahun_ajaran'] }}"
                                                        {{ substr($th['tahun_ajaran'], 0, 4) == session('tahun') ? 'selected' : '' }}>
                                                        {{ $th['tahun_ajaran'] }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="l31">Keterangan</label>
                                            <textarea name="keterangan" rows="3" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger prev-step"><i class="fas fa-angle-left"></i>
                                    Kembali</button>
                                <button type="submit" class="btn btn-success" id="saveBtn"><i class="fas fa-save"></i>
                                    Simpan</button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'detail',
                        name: 'detail',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'tahun',
                        name: 'tahun',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jabatan',
                        name: 'jabatan',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'pangkat',
                        name: 'pangkat',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'golongan',
                        name: 'golongan',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'gaji',
                        name: 'gaji',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'pensiun',
                        name: 'pensiun',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'vertical-middle',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $("#tahun_ajaran").change(function() {
                loadDataTahun($(this).val())
            });


            $('#formEditMutasi').on('submit', function(event) {
                event.preventDefault();
                $("#btnMutasi").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#btnMutasi").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-mutasi_guru_update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalEdit').modal('hide');
                            $('#formEditMutasi').trigger("reset");
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-mutasi_guru_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Fakultas");
                        $('#id_mutasi').val(data.id);
                        $('#id_pegawai_edit').val(data.id_pegawai);
                        $('#keterangan').val(data.keterangan);
                        $('#tahun_ajaran_edit').val(data.tahun_ajaran);
                        $('#tgl_mutasi').val(convertDate(data.tgl_mutasi));
                        $('#status_fakultas').val(data.status);
                        $('#modalEdit').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-mutasi_guru_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.kembali', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin mengembalikan data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-mutasi_guru_kembalikan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function loadDataTahun(tahun_ajaran) {
            var empty = tahun_ajaran === null;
            if (!empty) {
                window.location.href = 'mutasi?jenis={{ $_GET['jenis'] }}&tahun=' + tahun_ajaran;
            } else {
                console.log('empty')
            }
        }

        function changeJenis(params) {
            // console.log(params);
            if (params != '') {
                window.location.href = 'mutasi?jenis=' + params + '&tahun={{ $_GET['tahun'] }}';
            } else {
                console.log('empty')
            }
        }
    </script>
@endsection
