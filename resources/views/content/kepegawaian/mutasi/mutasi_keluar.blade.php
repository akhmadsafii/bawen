@extends('content.kepegawaian.mutasi.v_main_mutasi')
@section('content_mutasi')
    <style>
        @media (min-width: 992px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

    </style>
    <div class="card">
        <div class="card-header bg-info">
            <div class="row">
                <div class="col-md-8">
                    <h5 class="box-title my-2 text-white">
                        Data Pegawai
                    </h5>
                </div>
                <div class="col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li class="nav-item">
                            <a class="btn btn-success m-1 {{ $_GET['jenis'] == 'masuk' ? 'active' : '' }}"
                                href="{{ route('kepegawaian-mutasi_guru', ['jenis' => 'masuk', 'tahun' => session('tahun')]) }}"><i
                                    class="fas fa-angle-double-left"></i> Data Mutasi</a>
                        </li>
                        <li class="nav-item"><a
                                class="btn btn-outline-danger m-1 {{ $_GET['jenis'] == 'keluar' ? 'active' : '' }}"
                                href="{{ route('kepegawaian-mutasi_guru', ['jenis' => 'keluar']) }}">Mutasi
                                Keluar</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p class="m-0">Pilih pegawai yang sudah ada dan anda ingin mutasikan</p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="data-tabel">
                            <thead>
                                <tr class="bg-secondary">
                                    <th rowspan="2">No</th>
                                    <th rowspan="2" class="text-center">NIP</th>
                                    <th rowspan="2" class="text-center">Nama</th>
                                    <th rowspan="2" class="text-center">Jabatan</th>
                                    <th rowspan="2" class="text-center">Pangkat/Golongan TMT</th>
                                    <th colspan="4" class="text-center">Perkembangan Mutasi</th>
                                    <th rowspan="2" class="text-center">Aksi</th>
                                </tr>
                                <tr class="bg-secondary">
                                    <th class="text-center">Kenaikan Pangkat TMT</th>
                                    <th class="text-center">Kenaikan Gaji Berkala TMT</th>
                                    <th class="text-center">Pensiuanan TMT</th>
                                    <th class="text-center">Penyesuaian Ijazah TMT</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalMutasi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleMutasi">Mutasikan Pegawai</h5>
                </div>
                <form id="formMutasi">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_mutasi">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="tahun_ajaran" class="form-control" id="tahun_ajaran">
                                            <option value="">Pilih Tahun Ajaran..
                                            </option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['tahun_ajaran'] }}"
                                                    {{ substr($th['tahun_ajaran'], 0, 4) == session('tahun') ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Mutasi</label>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_mutasi" value="{{ date('d-m-Y') }}"
                                                class="form-control datepicker">
                                            <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger ripple text-left" id="btnMutasi"><i
                                class="fas fa-exclamation-circle"></i> Mutasikan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-xl-color-scheme" id="modalJabatan" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleMutasi">Mutasikan Pegawai</h5>
                </div>
                <form id="formMutasi">
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" id="tabel" class="table table-striped table-bordered table-hover">
                                <input type="hidden" name="nip" value="197009261997031007">
                                <thead>
                                    <tr>
                                        <td class="text-center" colspan="4">
                                            <label>Pangkat / Golongan : Pembina Utama Muda / IVc
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <label>Nama Pegawai : Eko Kurniawan, S.Sos, M.SI </label>
                                        </td>
                                        <td colspan="2" class="text-center">
                                            <label>NIP : 197009261997031007
                                            </label>
                                        </td>
                                    </tr>

                                    <tr class="odd bg-gray">

                                        <th colspan="4" class="text-center">
                                            Perkembangan Mutasi
                                        </th>
                                    </tr>
                                    <tr class="odd bg-gray">
                                        <th width="25%" class="text-center">Kenaikan Pangkat
                                            <br> TMT
                                        </th>
                                        <th width="25%">
                                            <center>Kenaikan Gaji Berkala
                                                <br> TMT
                                            </center>
                                        </th>
                                        <th width="20%">
                                            <center>Pensuinan
                                                <br> TMT
                                            </center>
                                        </th>
                                        <th width="25%">
                                            <center>Penyesuaian Ijasah
                                                <br> TMT
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">

                                        <td class="text-center">
                                            <select name="pangkat" style="width: 100%" id="id_pangkat_pegawai"
                                                class="form-control select3">
                                                @foreach ($pangkat as $pk)
                                                    <option value="{{ $pk['id'] }}">{{ $pk['nama'] }}</option>
                                                @endforeach
                                            </select>
                                            <table width="100%">
                                                <td width="50%">
                                                    <select name="bulan" class="form-control select3">
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </td>
                                                <td width="50%" style="padding-left: 5px;">
                                                    <select name="tahun" class="form-control select3">
                                                        @php
                                                            $firstYear = (int) date('Y') + 20;
                                                            $lastYear = $firstYear - 100;
                                                            for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                                echo '<option value=' . $year . '>' . $year . '</option>';
                                                            }
                                                        @endphp
                                                    </select>
                                                </td>
                                            </table>
                                        </td>
                                        <td>
                                            <input name="gaji" id="gaji_pokok" style="width: 100%"
                                                class="form-control ribuan">
                                            <table width="100%">
                                                <td width="100%">
                                                    <div class="input-group">
                                                        <input type="text" name="tmt_golongan"
                                                            value="{{ date('d-m-Y') }}" class="form-control datepicker">
                                                        <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </table>
                                        </td>
                                        <td>
                                            <input type="number" min="0" name="pensiun" class="form-control"
                                                style="width: 100%" value="1">
                                            <table width="100%">
                                                <td width="100%">
                                                    <div class="input-group">
                                                        <input type="text" name="tmt_golongan"
                                                            value="{{ date('d-m-Y') }}" class="form-control datepicker">
                                                        <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </table>
                                        </td>
                                        <td>
                                            <select name="ijasah" style="width: 100%" id="pangkat"
                                                class="form-control select3">
                                                @foreach ($golongan as $gl)
                                                    <option value="{{ $gl['id'] }}">
                                                        {{ $gl['nama'] . ' / ' . $gl['keterangan'] }}</option>
                                                @endforeach
                                            </select>
                                            <table width="100%">
                                                <td width="100%">
                                                    <div class="input-group">
                                                        <input type="text" name="tmt_golongan"
                                                            value="{{ date('d-m-Y') }}" class="form-control datepicker">
                                                        <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger ripple text-left" id="btnMutasi"><i
                                class="fas fa-exclamation-circle"></i> Mutasikan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nip',
                        name: 'nip',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jabatan_pegawai',
                        name: 'jabatan_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'pangkat_pegawai',
                        name: 'pangkat_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jabatan_pegawai',
                        name: 'jabatan_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jabatan_pegawai',
                        name: 'jabatan_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jenis_pegawai',
                        name: 'jenis_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jenis_pegawai',
                        name: 'jenis_pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'vertical-middle',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '.mutasi', function() {
                let id = $(this).data('id');
                $('#id_mutasi').val(id);
                $('#modalMutasi').modal('show');
            });

            $(document).on('click', '.jabatan', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-pegawai_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-sync-alt"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-info-circle"></i>');
                        $('#id_pangkat_pegawai').val(data.id_pangkat_pegawai);
                        // $('#nama').html(data.nama);
                        // $('#tempat_lahir').html(data.tempat_lahir);
                        // $('#tgl_lahir').html(convertDate(data.tgl_lahir));
                        // $('#jenkel').html(data.jenkel);
                        // $('#agama').html(data.agama);
                        // $('#alamat').html(data.alamat);
                        // $('#rt').html(data.rt);
                        // $('#rw').html(data.rw);
                        // $('#kode_pos').html(data.kode_pos);
                        // $('#dusun').html(data.dusun);
                        // $('#kelurahan').html(data.kelurahan);
                        // $('#kecamatan').html(data.kecamatan);
                        // $('#sk').html(data.sk);
                        // $('#npwp').html(data.npwp);
                        // $('#pangkat_pegawai').html(data.pangkat_pegawai);
                        // $('#tmt_golongan').html(convertDate(data.tmt_golongan));
                        // $('#jenis_pegawai').html(data.jenis_pegawai);
                        // $('#tmt_capeg').html(convertDate(data.tmt_capeg));
                        // $('#status_pegawai').html(data.status_pegawai);
                        // $('#jabatan_pegawai').html(data.jabatan_pegawai);
                        // $('#digaji_menurut').html(data.digaji_menurut);
                        // $('#gaji_pokok').html(rubahRibuan(data.gaji_pokok));
                        // $('#telepon').html(data.telepon);
                        // $('#masa_kerja_golongan').html(data.masa_kerja_golongan);
                        // $('#masa_kerja_keseluruhan').html(data.masa_kerja_keseluruhan);
                        $('#modalJabatan').modal('show');
                    }
                });
            });

            $('#formMutasi').on('submit', function(event) {
                event.preventDefault();
                $("#btnMutasi").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#btnMutasi").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-store_mutasi_keluar') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalMutasi').modal('hide');
                            $('#formMutasi').trigger("reset");
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                    }
                });
            });

        });
    </script>
@endsection
