@extends('content.kepegawaian.mutasi.v_main_mutasi')
@section('content_mutasi')
    <style>
        @media (min-width: 992px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        .dataTables_wrapper table.dataTable thead .sorting_asc::before {
            content: '' !important;
        }

    </style>
    <div class="card">
        <div class="card-header bg-info">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="box-title my-2 text-white">
                        {{-- {{ $_GET['tahun'] != 'all' ? 'Data Mutasi ' . $_GET['tahun'] : 'Semua Mutasi Masuk' }} --}}
                    </h5>
                </div>
                <div class="col-md-8">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a class="btn btn-success m-1" id="mutasi_in"><i class="fas fa-sign-in-alt"></i> Masuk</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-danger m-1" id="mutasi_out"><i class="fas fa-sign-out-alt"></i> Keluar</a>
                        </li>
                        <li class="nav-item"><a class="btn btn-purple m-1" id="btn-jabatan"><i
                                    class="fas fa-user-tie"></i> Pangkat</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 my-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-stripped table-hover table-bordered widget-status-table" id="data-tabel">
                            <thead>
                                <tr>
                                    <th class="vertical-middle" rowspan="2">
                                        <input type="checkbox">
                                    </th>
                                    <th rowspan="2">Pegawai</th>
                                    <th rowspan="2">Jabatan</th>
                                    <th colspan="4">Perkembangan Mutasi</th>
                                </tr>
                                <tr>
                                    <th>Jenis <br>Tgl Mutasi</th>
                                    <th>Pangkat<br> TMT</th>
                                    <th>Pensiuanan TMT</th>
                                    <th>Penyesuaian Ijazah TMT</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalIn" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info" id="color-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingMutasi">Tambah Mutasi Masuk</h5>
                </div>
                <form id="formMutasi">
                    <div class="tab-content">
                        <input type="hidden" name="jenis" id="jenis_mutasi" value="masuk">
                        <div class="modal-body py-0">
                            <div class="row">
                                <div class="col-md-12" id="list_pegawai">

                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="l31">Tanggal Mutasi</label>
                                        <div class="input-group">
                                            <input type="text" name="tanggal_mutasi" value="{{ date('d-m-Y') }}"
                                                class="form-control datepicker">
                                            <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="l31">Tahun Ajaran</label>
                                        <select name="tahun_ajaran" class="form-control" id="tahun_ajaran">
                                            <option value="">Pilih Tahun Ajaran..
                                            </option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['tahun_ajaran'] }}"
                                                    {{ substr($th['tahun_ajaran'], 0, 4) == session('tahun') ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="l31">Keterangan</label>
                                        <textarea name="keterangan" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger prev-step"><i class="fas fa-angle-left"></i>
                                Kembali</button>
                            <button type="submit" class="btn btn-success" id="saveBtn"><i class="fas fa-save"></i>
                                Simpan</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalJabatan" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleMutasi">Mutasikan Pegawai</h5>
                </div>
                <form id="formJabatan">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="l0">Tahun Ajaran</label>
                                    <div class="col-md-8">
                                        <select name="tahun_ajaran" class="form-control" id="tahun_ajaran">
                                            <option value="">Pilih Tahun Ajaran..
                                            </option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['tahun_ajaran'] }}"
                                                    {{ substr($th['tahun_ajaran'], 0, 4) == session('tahun') ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="l0">Tanggal Mutasi</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="text" name="tgl_mutasi" value="{{ date('d-m-Y') }}"
                                                class="form-control datepicker">
                                            <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" id="list_employee">
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Kenaikan Jabatan TMT</label>
                            <div class="col-md-4">
                                <select name="id_jabatan" style="width: 100%" class="form-control select3">
                                    @foreach ($jabatan as $jb)
                                        <option value="{{ $jb['id'] }}">
                                            {{ $jb['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" name="tmt_jabatan" value="{{ date('d-m-Y') }}"
                                        class="form-control datepicker">
                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Kenaikan Pangkat TMT</label>
                            <div class="col-md-4">
                                <select name="id_pangkat" style="width: 100%" class="form-control select3">
                                    @foreach ($pangkat as $pk)
                                        <option value="{{ $pk['id'] }}">{{ $pk['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select name="bulan" class="form-control select3">
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select name="tahun" class="form-control select3">
                                    @for ($i = (int) date('Y') + 20; $i >= (int) date('Y') - 80; $i--)
                                        <option value="{{ $i }}" {{ $i == date('Y') ? 'selected' : '' }}>
                                            {{ $i }}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Kenaikan Gaji Berkala TMT</label>
                            <div class="col-md-4">
                                <input name="gaji" id="gaji_pokok" style="width: 100%" class="form-control ribuan">
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" name="tmt_gaji" value="{{ date('d-m-Y') }}"
                                        class="form-control datepicker">
                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Pensuinan TMT</label>
                            <div class="col-md-4">
                                <input type="number" min="0" name="pensiun" class="form-control" value="1">
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" name="tmt_pensiun" value="{{ date('d-m-Y') }}"
                                        class="form-control datepicker">
                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Penyesuaian Ijasah TMT</label>
                            <div class="col-md-4">
                                <select name="id_golongan" style="width: 100%" class="form-control select3">
                                    @foreach ($golongan as $gl)
                                        <option value="{{ $gl['id'] }}">
                                            {{ $gl['nama'] . ' / ' . $gl['keterangan'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" name="tmt_golongan" value="{{ date('d-m-Y') }}"
                                        class="form-control datepicker">
                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Keterangan</label>
                            <div class="col-md-8">
                                <textarea name="keterangan" rows="3" class="form-control"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info ripple text-left" id="btnPangkat"><i
                                class="fas fa-check-circle"></i> Mutasikan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            var pegawai = [];
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'checkbox',
                        name: 'checkbox',
                        className: 'text-center vertical-middle',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'pegawai',
                        name: 'pegawai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jabatan',
                        name: 'jabatan',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'pangkat',
                        name: 'pangkat',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'pensiun',
                        name: 'pensiun',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'golongan',
                        name: 'golongan',
                        className: 'vertical-middle text-center'
                    }
                ]
            });



            $("#mutasi_in").click(function() {
                pegawai = [];
                $("input:checkbox[class=check_pegawai]:checked").each(function() {
                    pegawai.push($(this).val());
                });
                if (pegawai.length > 0) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('kepegawaian-mutasi_guru_get_data') }}",
                        data: {
                            pegawai,
                            method: "mutasi"
                        },
                        beforeSend: function() {
                            $("#mutasi_in").html(
                                '<i class="fa fa-spin fa-sync-alt"></i> Proses..');
                        },
                        success: function(data) {
                            $('#headingMutasi').html(
                                "<i class='fas fa-sign-in-alt'></i> Mutasi Masuk");
                            $("#mutasi_in").html(
                                '<i class="fas fa-sign-in-alt"></i> Masuk');
                            $('#color-header').addClass("bg-info");
                            $('#list_pegawai').html(data);
                            $('#color-header').removeClass("bg-danger");
                            $('#formMutasi').trigger("reset");
                            $('#jenis_mutasi').val("masuk");
                            $('#modalIn').modal('show');
                        }
                    });
                } else {
                    alert('Harap pilih salah satu pegawai dulu mas bro ...')
                }
            });

            $("#mutasi_out").click(function() {
                pegawai = [];
                $("input:checkbox[class=check_pegawai]:checked").each(function() {
                    pegawai.push($(this).val());
                });
                if (pegawai.length > 0) {

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('kepegawaian-mutasi_guru_get_data') }}",
                        data: {
                            pegawai,
                            method: "mutasi"
                        },
                        beforeSend: function() {
                            $("#mutasi_out").html(
                                '<i class="fa fa-spin fa-sync-alt"></i> Proses..');
                        },
                        success: function(data) {
                            $("#mutasi_out").html(
                                '<i class="fas fa-sign-out-alt"></i> Keluar');
                            $('#color-header').addClass("bg-danger");
                            $('#color-header').removeClass("bg-info");
                            $('#formMutasi').trigger("reset");
                            $('#jenis_mutasi').val("keluar");
                            $('#headingMutasi').html(
                                "<i class='fas fa-sign-in-alt'></i> Mutasi Keluar");
                            $('#modalIn').modal('show');
                            $('#list_pegawai').html(data);
                        }
                    });
                } else {
                    alert('Harap pilih salah satu pegawai dulu mas bro ...')
                }
            });

            $("#btn-jabatan").click(function() {
                pegawai = [];
                $("input:checkbox[class=check_pegawai]:checked").each(function() {
                    pegawai.push($(this).val());
                });
                if (pegawai.length > 0) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('kepegawaian-mutasi_guru_get_data') }}",
                        data: {
                            pegawai,
                            method: "pangkat"
                        },
                        beforeSend: function() {
                            $("#btn-jabatan").html(
                                '<i class="fa fa-spin fa-sync-alt"></i> Proses..');
                        },
                        success: function(data) {
                            $("#btn-jabatan").html(
                                '<i class="fas fa-user-tie"></i> Jabatan');
                            $('#formJabatan').trigger("reset");
                            $('#titleMutasi').html(
                                "<i class='fas fa-sign-in-alt'></i> Mutasi Jabatan");
                            $('#modalJabatan').modal('show');
                            $('#list_employee').html(data);
                        }
                    });
                } else {
                    alert('Harap pilih salah satu pegawai dulu mas bro ...')
                }
            });

            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            $('#formMutasi').on('submit', function(event) {
                event.preventDefault();
                $("#btnMutasi").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#btnMutasi").attr("disabled", true);
                var action_url = '';

                if ($('#jenis_mutasi').val() == 'masuk') {
                    action_url = "{{ route('kepegawaian-store_mutasi_masuk') }}";
                }

                if ($('#jenis_mutasi').val() == 'keluar') {
                    action_url = "{{ route('kepegawaian-store_mutasi_keluar') }}";
                }
                $.ajax({
                    url: action_url,
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalIn').modal('hide');
                            $('#formMutasi').trigger("reset");
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnMutasi').html('Simpan');
                        $("#btnMutasi").attr("disabled", false);
                    }
                });
            });

            $('#formJabatan').on('submit', function(event) {
                event.preventDefault();
                $("#btnPangkat").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#btnPangkat").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-store_jabatan_mutasi') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalJabatan').modal('hide');
                            $('#formJabatan').trigger("reset");
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#btnPangkat').html('Simpan');
                        $("#btnPangkat").attr("disabled", false);
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPangkat').html('Simpan');
                        $("#btnPangkat").attr("disabled", false);
                    }
                });
            });



        });
    </script>
@endsection
