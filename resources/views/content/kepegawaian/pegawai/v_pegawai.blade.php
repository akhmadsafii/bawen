@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    @if (session('role') == 'admin-kepegawaian')
                        <div class="row mb-2">
                            <div class="col-12">
                                <a href="{{ route('kepegawaian-pegawai_add') }}" class="btn btn-outline-info mt-1"><i
                                        class="fas fa-plus-circle"></i>
                                    Tambah</a>
                            </div>
                        </div>
                    @endif
                    <div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Gender</th>
                                        <th>Agama</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_pegawai">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" name="password" placeholder="Password Baru"
                                            type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <table width="100%">
                                <!--baris 1-->
                                <tr>
                                    <td width="20%">
                                        <div class="form-group">
                                            Nip
                                        </div>
                                    </td>
                                    <td width="3%">
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td width="30%">
                                        <div class="form-group" id="nip"></div>
                                    </td>
                                    <td width="1%"></td>
                                    <td width="22%">
                                        <div class="form-group">
                                            Pangkat / Golongan
                                        </div>
                                    </td>
                                    <td width="3%">
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td width="23%">
                                        <div class="form-group" id="pangkat_pegawai"></div>
                                    </td>
                                </tr>
                                <!--baris 2-->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Nama
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="nama"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            TMT / Golongan
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="tmt_golongan"></div>
                                    </td>
                                </tr>
                                <!-- baris 3 -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Tempat Lahir
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="tempat_lahir"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Jenis Kepegawaian
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="jenis_pegawai"></div>
                                    </td>
                                </tr>
                                <!-- baris 4 -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Tanggal Lahir
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="tgl_lahir"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            TMT Capeg
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="tmt_capeg"></div>
                                    </td>
                                </tr>
                                <!-- baris 5 -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Gender
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="jenkel"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Status Kepegawaian
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="status_pegawai"></div>
                                    </td>
                                </tr>
                                <!-- baris 6  -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Agama
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="agama"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Jabatan Strukural
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="jabatan_pegawai"></div>
                                    </td>
                                </tr>
                                <!-- baris 6  -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Kebangsaansss
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Digaji Menurut <br>( PP / SK )
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="digaji_menurut"></div>
                                    </td>
                                </tr>
                                <!-- baris 7  -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Jumlah Keluargassss
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            4 </div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Gaji Pokok
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="gaji_pokok"></div>
                                    </td>
                                </tr>
                                <!-- baris 8  -->
                                <tr>
                                    <td rowspan="2">
                                        <div class="form-group">
                                            Alamat
                                        </div>
                                    </td>
                                    <td rowspan="2">
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td rowspan="2">
                                        <div class="form-group">
                                            <span id="alamat"></span> RT. <span id="rt"></span> RW. <span
                                                id="rw"></span>KODE POS. <span id="kode_pos"></span><br>
                                            <span id="dusun"></span> <br> Desa <span id="kelurahan"></span> <br>
                                            Kecamatan <span id="kecamatan"></span>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Besarnya Penghasilan
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            Rp. 4.995.100 </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="3%"></td>
                                    <td>
                                        <div class="form-group">
                                            Nomor WhatsApp
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="telepon"></div>
                                    </td>
                                </tr>
                                <!-- baris 9  -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            SK Terakhir
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="sk"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Masa Kerja Golongan
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="masa_kerja_golongan"></div>
                                    </td>
                                </tr>
                                <!-- baris 10  -->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            NPWP
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="npwp"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-group">
                                            Masa Kerja Keseluruhan
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="masa_kerja_keseluruhan"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal"
                        aria-hidden="true">Close</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jabatan_pegawai',
                        name: 'jabatan_pegawai'
                    },
                    {
                        data: 'jenkel',
                        name: 'jenkel'
                    },
                    {
                        data: 'agama',
                        name: 'agama'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ]
            });

            $('#addData').click(function() {
                $('#formResult').trigger("reset");
                $('.tambahBaris').show('');
                $('.fomAddEkstra').html('');
                $('#modelHeading').html("Tambah Status");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#formResult').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kepegawaian-status_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kepegawaian-status_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formResult').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.reset_pass', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-pegawai_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-sync-alt"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-key"></i>');
                        $('#id_pegawai').val(data.id);
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-pegawai_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-sync-alt"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-info-circle"></i>');
                        $('#nip').html(data.nip);
                        $('#nama').html(data.nama);
                        $('#tempat_lahir').html(data.tempat_lahir);
                        $('#tgl_lahir').html(convertDate(data.tgl_lahir));
                        $('#jenkel').html(data.jenkel);
                        $('#agama').html(data.agama);
                        $('#alamat').html(data.alamat);
                        $('#rt').html(data.rt);
                        $('#rw').html(data.rw);
                        $('#kode_pos').html(data.kode_pos);
                        $('#dusun').html(data.dusun);
                        $('#kelurahan').html(data.kelurahan);
                        $('#kecamatan').html(data.kecamatan);
                        $('#sk').html(data.sk);
                        $('#npwp').html(data.npwp);
                        $('#pangkat_pegawai').html(data.pangkat_pegawai);
                        $('#tmt_golongan').html(convertDate(data.tmt_golongan));
                        $('#jenis_pegawai').html(data.jenis_pegawai);
                        $('#tmt_capeg').html(convertDate(data.tmt_capeg));
                        $('#status_pegawai').html(data.status_pegawai);
                        $('#jabatan_pegawai').html(data.jabatan_pegawai);
                        $('#digaji_menurut').html(data.digaji_menurut);
                        $('#gaji_pokok').html(rubahRibuan(data.gaji_pokok));
                        $('#telepon').html(data.telepon);
                        $('#masa_kerja_golongan').html(data.masa_kerja_golongan);
                        $('#masa_kerja_keseluruhan').html(data.masa_kerja_keseluruhan);
                        $('#modalDetail').modal('show');
                    }
                });
            });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-reset_password_pegawai') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-delete_pegawai') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-sync-alt"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })
    </script>
@endsection
