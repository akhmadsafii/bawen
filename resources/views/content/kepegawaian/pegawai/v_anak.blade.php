@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    @if (session('role') == 'admin-kepegawaian' || session('id') == (new \App\Helpers\Help())->decode($_GET['k']))
        <button type="button" id="btnAdd" class="btn btn-purple mb-3">
            <li class="fas fa-plus-circle"></li> Tambah
        </button>
    @endif

    <br>
    <div class="table-responsive">
        <table class="table table-stripped table-hover" id="data-tabel">
            <thead>
                <tr class="bg-purple">
                    <th class="text-white">No</th>
                    <th class="text-white">Nama Anak</th>
                    <th class="text-white">Tempat, Tgl lahir</th>
                    <th class="text-white">Status Anak</th>
                    <th class="text-white">Dari pasangan ke</th>
                    <th class="text-white">Gender</th>
                    <th class="text-white">Tunjangan/Tidak</th>
                    <th class="text-white">Kawin/Belum</th>
                    <th class="text-white">Bekerja</th>
                    <th class="text-white">Masih/Tidak sekolah/Kuliah</th>
                    <th class="text-white">Putusan Pengadilan (Khusus AA)</th>
                    <th class="text-white">Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAnak" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <form id="formAnak">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id_anak">
                                    <input type="hidden" name="id_pegawai" value="{{ $pegawai['id'] }}">
                                    <label for="l30">Nama Anak</label>
                                    <input class="form-control" id="nama" name="nama" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="l30">NIK</label>
                                    <input class="form-control" id="nik" name="nik" type="text"
                                        onkeypress="return hanyaAngka(event)">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Tempat lahir</label>
                                    <input class="form-control" id="tempat_lahir" name="tempat_lahir" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Tanggal lahir</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="tanggal" id="tanggal" class="form-control select3">
                                                <option value="">Tgl</option>
                                                @for ($i = 1; $i <= 31; $i++)
                                                    <option value="{{ sprintf('%02d', $i) }}">{{ $i }}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="bulan" id="bulan" class="form-control select3">
                                                <option value="">Bulan</option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="tahun" id="tahun" class="form-control select3">
                                                <option value="">Tahun</option>
                                                @php
                                                    $firstYear = (int) date('Y');
                                                    $lastYear = $firstYear - 100;
                                                    for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                        echo '<option value=' . $year . '>' . $year . '</option>';
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Status Anak</label>
                                    <select name="status_anak" id="status_anak" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="AK">Anak Kandung (AK)</option>
                                        <option value="AT">Anak Tiri (AT)</option>
                                        <option value="AA">Anak Angkat (AA)</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Jenis Kelamin</label>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="l30">Dari istri/suami ke</label>
                                    <input class="form-control" id="ke" name="ke" type="number">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Sudah / Belum Kawin</label>
                                    <select name="status_nikah" id="status_nikah" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="sudah">Sudah</option>
                                        <option value="belum">Belum</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Dapat Tunjangan / Tidak</label>
                                    <select name="status_tunjangan" id="status_tunjangan" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="1">Dapat</option>
                                        <option value="0">Tidak</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Bekerja</label>
                                    <select name="status_kerja" id="status_kerja" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="1">Bekerja</option>
                                        <option value="0">Tidak</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Masih / Tidak Sekolah / Kuliah</label>
                                    <select name="sekolah" id="sekolah" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="masih sekolah">Masih Sekolah</option>
                                        <option value="tidak sekolah">Tidak Sekolah</option>
                                        <option value="kuliah">Kuliah</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="l30">Putusan Pengadilan (Khusus AA)</label>
                                    <select name="putusan" id="putusan" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="1">Ya</option>
                                        <option value="0">Tidak</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'ttl',
                        name: 'ttl'
                    },
                    {
                        data: 'status_anak',
                        name: 'status_anak'
                    },
                    {
                        data: 'pasangan_ke',
                        name: 'pasangan_ke'
                    },
                    {
                        data: 'jenkel',
                        name: 'jenkel'
                    },
                    {
                        data: 'status_tunjangan',
                        name: 'status_tunjangan',
                        render: function(data, type, full, meta) {
                            return data == "0" ? "Tidak" : "Dapat";
                        }
                    },
                    {
                        data: 'status_nikah',
                        name: 'status_nikah'
                    },
                    {
                        data: 'status_kerja',
                        name: 'status_kerja',
                        render: function(data, type, full, meta) {
                            return data == "0" ? "Tidak" : "Bekerja";
                        }
                    },
                    {
                        data: 'sekolah',
                        name: 'sekolah'
                    },
                    {
                        data: 'putusan',
                        name: 'putusan',
                        render: function(data, type, full, meta) {
                            return data == "0" ? "Tidak" : "Ya";
                        }
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '#btnAdd', function() {
                $('#formAnak').trigger("reset");
                $('#modelHeading').html("Tambah Data Anak");
                $('#modalAnak').modal('show');
                $('#action').val('Add');
            });

            $('#formAnak').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kepegawaian-anak_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kepegawaian-anak_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formAnak').trigger("reset");
                            $('#modalAnak').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-detail_anak') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Anak");
                        $('#id_anak').val(data.id);
                        $('#id_pegawai').val(data.id_pegawai);
                        $('#nik').val(data.nik);
                        $('#nama').val(data.nama);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#status_anak').val(data.status_anak);
                        $('#jenkel').val(data.jenkel_kode).trigger('change');
                        $('#tanggal').val(data.tanggal).trigger('change');
                        $('#bulan').val(data.bulan).trigger('change');
                        $('#tahun').val(data.tahun).trigger('change');
                        $('#status_nikah').val(data.status_nikah).trigger('change');
                        $('#status_tunjangan').val(data.status_tunjangan).trigger('change');
                        $('#status_kerja').val(data.status_kerja).trigger('change');;
                        $('#sekolah').val(data.sekolah).trigger('change');
                        $('#putusan').val(data.putusan).trigger('change');
                        $('#ke').val(data.pasangan_ke);
                        $('#modalAnak').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-delete_anak') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>
@endsection
