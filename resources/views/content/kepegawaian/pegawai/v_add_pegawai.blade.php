@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <a href="{{ route('kepegawaian-pegawai') }}" class="btn btn-outline-info mt-1"><i
                                    class="fas fa-arrow-alt-circle-left"></i>
                                Kembali</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-info">
                            <i class="fas fa-user-friends"></i> Tambah Pegawai
                        </div>
                        <div class="card-body">
                            <form id="formPegawai">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">NIP</label>
                                            <div class="col-md-9">
                                                <input class="form-control" name="nip"
                                                    onkeypress="return hanyaAngka(event)" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nama</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="nama">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">NIK</label>
                                            <div class="col-md-9">
                                                <input class="form-control" name="nik"
                                                    onkeypress="return hanyaAngka(event)" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">NUPTK</label>
                                            <div class="col-md-9">
                                                <input class="form-control" name="nuptk"
                                                    onkeypress="return hanyaAngka(event)" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="tempat_lahir">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <select name="tanggal" class="form-control select3">
                                                            <option value="">Tgl</option>
                                                            @for ($i = 1; $i <= 31; $i++)
                                                                <option value="{{ $i }}">{{ $i }}
                                                                </option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <select name="bulan" class="form-control select3">
                                                            <option value="">Bulan</option>
                                                            <option value="01">Januari</option>
                                                            <option value="02">Februari</option>
                                                            <option value="03">Maret</option>
                                                            <option value="04">April</option>
                                                            <option value="05">Mei</option>
                                                            <option value="06">Juni</option>
                                                            <option value="07">Juli</option>
                                                            <option value="08">Agustus</option>
                                                            <option value="09">September</option>
                                                            <option value="10">Oktober</option>
                                                            <option value="11">November</option>
                                                            <option value="12">Desember</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <select name="tahun" class="form-control select3">
                                                            <option value="">Tahun</option>
                                                            @php
                                                                $firstYear = (int) date('Y');
                                                                $lastYear = $firstYear - 100;
                                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                                }
                                                            @endphp
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Jenis kelamin</label>
                                            <div class="col-md-9">
                                                <div class="form-check form-check-inline my-2">
                                                    <input class="form-check-input mx-0" type="radio" name="jenkel"
                                                        value="l">
                                                    <label class="form-check-label" for="inlineRadio1">Laki laki</label>
                                                </div>
                                                <div class="form-check form-check-inline my-2">
                                                    <input class="form-check-input mx-0" type="radio" name="jenkel"
                                                        value="p">
                                                    <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                            <div class="col-md-9">
                                                <select name="agama" class="form-control">
                                                    <option value="">Pilih Agama</option>
                                                    <option value="islam">Islam</option>
                                                    <option value="kristen">Kristen</option>
                                                    <option value="hindu">Hindu</option>
                                                    <option value="budha">Budha</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 my-auto col-form-label" for="l0">Alamat</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <textarea name="alamat" class="form-control" rows="2"
                                                            placeholder="Alamat"></textarea>
                                                    </div>
                                                    <div class="col-md-3 my-1">
                                                        <input type="text" onkeypress="return hanyaAngka(event)" name="rt"
                                                            placeholder="RT" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 my-1">
                                                        <input type="text" onkeypress="return hanyaAngka(event)" name="rw"
                                                            placeholder="RW" class="form-control">
                                                    </div>
                                                    <div class="col-md-6 my-1">
                                                        <input type="text" name="dusun" placeholder="Dusun"
                                                            class="form-control">
                                                    </div>
                                                    <div class="col-md-7 my-1">
                                                        <input type="text" name="desa" placeholder="Desa"
                                                            class="form-control">
                                                    </div>
                                                    <div class="col-md-5 my-1">
                                                        <input type="text" onkeypress="return hanyaAngka(event)"
                                                            name="kode_pos" placeholder="Kode Pos" class="form-control">
                                                    </div>
                                                    <div class="col-md-12 my-1">
                                                        <input type="text" name="kecamatan" placeholder="Kecamatan"
                                                            class="form-control">
                                                    </div>
                                                    <div class="col-md-12 my-1">
                                                        <input type="text" name="kabupaten" placeholder="Kabupaten"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">SK Terakhir</label>
                                            <div class="col-md-9">
                                                <select name="sk" class="form-control select3">
                                                    <option value="">- SK Terakhir - </option>
                                                    <option value="">Kosong</option>
                                                    @foreach ($sk as $sk)
                                                        <option value="{{ $sk['id'] }}">{{ $sk['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">NPWP</label>
                                            <div class="col-md-9">
                                                <input type="text" name="npwp" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Password</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input class="form-control" id="password" name="password" type="text">
                                                    <span class="input-group-btn">
                                                        <a class="btn btn-info generate" href="javascript: void(0);"><i
                                                                class="fas fa-sync-alt"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" name="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Golongan</label>
                                            <div class="col-md-9">
                                                <select name="golongan" class="form-control select3">
                                                    <option value="">- Pilih Pangkat/Golongan -</option>
                                                    <option value="">Kosong</option>
                                                    @foreach ($golongan as $gl)
                                                        <option value="{{ $gl['id'] }}">{{ $gl['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">TMT/Golongan</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="tmt_golongan" value="{{ date('d-m-Y') }}"
                                                        class="form-control datepicker">
                                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Jenis</label>
                                            <div class="col-md-9">
                                                <select name="id_jenis_golongan" class="form-control select3">
                                                    <option value="">- Pilih Jenis Kepegawaian -</option>
                                                    <option value="">Kosong</option>
                                                    @foreach ($jenis as $jn)
                                                        <option value="{{ $jn['id'] }}">{{ $jn['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">TMT Capeg</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="tmt_capeg" value="{{ date('d-m-Y') }}"
                                                        class="form-control datepicker">
                                                    <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Status</label>
                                            <div class="col-md-9">
                                                <select name="id_status_pegawai" class="form-control select3">
                                                    <option value="">- Pilih Status Kepegawaian -</option>
                                                    <option value="">Kosong</option>
                                                    @foreach ($status as $st)
                                                        <option value="{{ $st['id'] }}">{{ $st['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Jabatan</label>
                                            <div class="col-md-9">
                                                <select name="id_jabatan_pegawai" class="form-control select3">
                                                    <option value="">- Pilih Jabatan Struktutal -</option>
                                                    <option value="">Kosong</option>
                                                    @foreach ($jabatan as $jbt)
                                                        <option value="{{ $jbt['id'] }}">{{ $jbt['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Sumber Gaji</label>
                                            <div class="col-md-9">
                                                <input type="text" name="digaji_menurut" placeholder="Digaji Menurut"
                                                    value="PP No 30 Tahun 2015" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Gaji Pokok</label>
                                            <div class="col-md-9">
                                                <input type="text" name="gaji_pokok" class="form-control ribuan">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Total Peng</label>
                                            <div class="col-md-9">
                                                <input type="text" name="total" readonly class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Whatsapp</label>
                                            <div class="col-md-9">
                                                <input type="text" name="telepon" onkeypress="return hanyaAngka(event)"
                                                    placeholder="Nomo Whatsapp" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Masa Golongan</label>
                                            <div class="col-md-9">
                                                <input type="text" name="masa_kerja_golongan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Masa Keseluruhan</label>
                                            <div class="col-md-9">
                                                <input type="text" name="masa_kerja_keseluruhan" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="{{ route('kepegawaian-pegawai') }}" class="btn btn-danger"><i
                                                class="fas fa-times-circle"></i> Batal</a>
                                        <button type="submit" class="btn btn-success" id="saveBtn"><i
                                                class="fas fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.generate', function() {
                let randoms = Math.floor(Math.random() * 900000) + 100000;
                $('#password').val(randoms);

            });

            $('#formPegawai').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-pegawai_create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.href = "{{ route('kepegawaian-pegawai') }}";
                        } else {
                            $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        })

        function tambahData() {
            i++;
            $('.fomAddEkstra').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Status</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }
    </script>
@endsection
