@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered widget-status-table" id="data-tabel">
            <thead>
                <tr class="bg-purple">
                    <th class="text-white" rowspan="2">No</th>
                    <th class="text-white" rowspan="2">Jenis</th>
                    <th class="text-white" rowspan="2">Pegawai</th>
                    <th class="text-white" rowspan="2">Jabatan</th>
                    <th class="text-white text-center" colspan="3">Perkembangan Mutasi</th>
                </tr>
                <tr class="bg-purple">
                    <th class="text-white">Pangkat <br>TMT</th>
                    <th class="text-white">Pensiunan <br>TMT</th>
                    <th class="text-white">Penyesuaian Ijazah <br>TMT</th>
                </tr>
            </thead>
        </table>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis'
                    },
                    {
                        data: 'detail',
                        name: 'detail'
                    },
                    {
                        data: 'jabatan',
                        name: 'jabatan'
                    },
                    {
                        data: 'pangkat',
                        name: 'pangkat'
                    },
                    {
                        data: 'golongan',
                        name: 'golongan'
                    },
                    {
                        data: 'pensiun',
                        name: 'pensiun'
                    },
                ]
            });
        });
    </script>
@endsection
