@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template' . $ext . '/app')
@section('content')
    @if (Session::has('message'))
        <script>
            alert('{{ session('message')['message'] }}');
        </script>
    @endif
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <a href="{{ route('kepegawaian-pegawai') }}" class="btn btn-danger mt-1"><i
                                    class="fas fa-arrow-alt-circle-left"></i>
                                Kembali</a>

                            <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'biodata', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'biodata' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-user"></i>
                                Data Diri</a>

                            @if (session('role') != 'pegawai' || (session('role') == 'pegawai' && $pegawai['id'] == session('id')))
                                <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'keluarga', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id'])]) }}"
                                    class="btn btn{{ $_GET['method'] != 'keluarga' ? '-outline' : '' }}-info mt-1"><i
                                        class="fas fa-street-view"></i>
                                    Data Istri/Suami</a>
                                <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'anak', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id'])]) }}"
                                    class="btn btn{{ $_GET['method'] != 'anak' ? '-outline' : '' }}-info mt-1"><i
                                        class="fas fa-child"></i>
                                    Data Anak</a>
                                <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'gaji', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id']), 'bulan' => strtolower((new \App\Helpers\Help())->getNumberMonthIndo(date('m'))), 'tahun' => date('Y')]) }}"
                                    class="btn btn{{ $_GET['method'] != 'gaji' ? '-outline' : '' }}-info mt-1"><i
                                        class="fas fa-money-bill-alt"></i>
                                    Penghasilan</a>
                            @endif

                            <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'berkas', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'berkas' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-file-alt"></i>
                                Lampiran</a>
                            <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'mutasi', 'nip' => $pegawai['nip'], 'k' => (new \App\Helpers\Help())->encode($pegawai['id'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'mutasi' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-chart-line"></i>
                                Mutasi</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-info">
                            <i class="fas fa-user"></i> Data
                            @if ($_GET['method'] == 'biodata')
                                Diri
                            @elseif ($_GET['method'] == 'keluarga')
                                Istri / Suami
                            @elseif ($_GET['method'] == 'anak')
                                Anak
                            @elseif ($_GET['method'] == 'gaji')
                                Penghasilan
                            @else
                                Berkas
                            @endif
                            <div class="pull-right">
                                <label class="label my-0" style="font-size: 15px;"> Pegawai : {{ $pegawai['nama'] }}
                                </label>
                            </div>
                        </div>
                        <div class="card-body">
                            @yield('content_pegawai')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
