@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    <style>
        .table td,
        .table th {
            border: none;
        }

    </style>
    @if (session('role') == 'admin-kepegawaian' || session('id') == (new \App\Helpers\Help())->decode($_GET['k']))
        <button type="button" id="btnEdit" class="btn btn-purple pull-right mb-3">
            <li class="fas fa-pencil-alt"></li> Edit
        </button>
    @endif
    <br>
    <div class="table-responsive">
        <table class="table">
            <!--baris 1-->
            <tr>
                <td width="20%">
                    <div class="form-group">
                        Nip
                    </div>
                </td>
                <td width="3%">
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td width="30%">
                    <div class="form-group">
                        {{ $pegawai['nip'] != null ? $pegawai['nip'] : '-' }} </div>
                </td>
                <td width="1%"></td>
                <td width="22%">
                    <div class="form-group">
                        Pangkat / Golongan
                    </div>
                </td>
                <td width="3%">
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td width="23%">
                    <div class="form-group">
                        {{ $pegawai['pangkat_pegawai'] != null ? $pegawai['pangkat_pegawai'] : '-' }}
                    </div>
                </td>
            </tr>
            <!--baris 2-->
            <tr>
                <td>
                    <div class="form-group">
                        Nama
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['nama'] != null ? $pegawai['nama'] : '-' }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        TMT / Golongan
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group" id="data_1">
                        {{ (new \App\Helpers\Help())->getTanggal($pegawai['tmt_golongan']) }} </div>
                </td>
            </tr>
            <!-- baris 3 -->
            <tr>
                <td>
                    <div class="form-group">
                        Tempat Lahir
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['tempat_lahir'] }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Jenis Kepegawaian
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['jenis_pegawai'] != null ? $pegawai['jenis_pegawai'] : '-' }}
                    </div>
                </td>
            </tr>
            <!-- baris 4 -->
            <tr>
                <td>
                    <div class="form-group">
                        Tanggal Lahir
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ (new \App\Helpers\Help())->getTanggal($pegawai['tgl_lahir']) }}</div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        TMT Capeg
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ (new \App\Helpers\Help())->getTanggal($pegawai['tmt_capeg']) }}</div>
                </td>
            </tr>
            <!-- baris 5 -->
            <tr>
                <td>
                    <div class="form-group">
                        Gender
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['jenkel'] != null ? $pegawai['jenkel'] : '-' }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Status Kepegawaian
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['status_pegawai'] != null ? $pegawai['status_pegawai'] : '-' }}
                    </div>
                </td>
            </tr>
            <!-- baris 6  -->
            <tr>
                <td>
                    <div class="form-group">
                        Agama
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['agama'] != null ? $pegawai['agama'] : '-' }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Jabatan Strukural
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['jabatan_pegawai'] != null ? $pegawai['jabatan_pegawai'] : '-' }}
                    </div>
                </td>
            </tr>
            <!-- baris 6  -->
            <tr>
                <td>
                    <div class="form-group">
                        Kebangsaansss
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                    </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Digaji Menurut <br>( PP / SK )
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['digaji_menurut'] != null ? $pegawai['digaji_menurut'] : '-' }}
                    </div>
                </td>
            </tr>
            <!-- baris 7  -->
            <tr>
                <td>
                    <div class="form-group">
                        Jumlah Keluargassss
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        4 </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Gaji Pokok
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        Rp. {{ str_replace(',', '.', number_format($pegawai['gaji_pokok'])) }} </div>
                </td>
            </tr>
            <!-- baris 8  -->
            <tr>
                <td rowspan="2">
                    <div class="form-group">
                        Alamat
                    </div>
                </td>
                <td rowspan="2">
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td rowspan="2">
                    <div class="form-group">
                        {{ $pegawai['alamat'] != null ? $pegawai['alamat'] : '-' }} RT.
                        {{ $pegawai['rt'] != null ? $pegawai['rt'] : '-' }} RW.
                        {{ $pegawai['rw'] != null ? $pegawai['rw'] : '-' }} KODE POS.
                        {{ $pegawai['kode_pos'] != null ? $pegawai['kode_pos'] : '-' }}<br>
                        {{ $pegawai['dusun'] != null ? $pegawai['dusun'] : '-' }} <br> Desa
                        {{ $pegawai['kelurahan'] != null ? $pegawai['kelurahan'] : '-' }} <br>
                        Kecamatan {{ $pegawai['kecamatan'] != null ? $pegawai['kecamatan'] : '-' }}
                    </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Besarnya Penghasilan
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        Rp. 4.995.100 </div>
                </td>
            </tr>
            <tr>
                <td width="3%"></td>
                <td>
                    <div class="form-group">
                        Nomor WhatsApp
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['telepon'] != null ? $pegawai['telepon'] : '-' }} </div>
                </td>
            </tr>
            <!-- baris 9  -->
            <tr>
                <td>
                    <div class="form-group">
                        SK Terakhir
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['sk'] != null ? $pegawai['sk'] : '-' }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Masa Kerja Golongan
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['masa_kerja_golongan'] != null ? $pegawai['masa_kerja_golongan'] : '-' }}
                    </div>
                </td>
            </tr>
            <!-- baris 10  -->
            <tr>
                <td>
                    <div class="form-group">
                        NPWP
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['npwp'] != null ? $pegawai['npwp'] : '-' }} </div>
                </td>
                <td></td>
                <td>
                    <div class="form-group">
                        Masa Kerja Keseluruhan
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        :
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ $pegawai['masa_kerja_keseluruhan'] != null ? $pegawai['masa_kerja_keseluruhan'] : '-' }}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPegawai" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">DETAIL PEGAWAI</h5>
                </div>
                <div class="modal-body">
                    <form id="formUpdate">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="id" value="{{ $pegawai['id'] }}">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">NIP</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nip" onkeypress="return hanyaAngka(event)"
                                            value="{{ $pegawai['nip'] }}" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Nama</label>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" name="nama"
                                            value="{{ $pegawai['nama'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">NIK</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nik" onkeypress="return hanyaAngka(event)"
                                            type="text" value="{{ $pegawai['nik'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">NUPTK</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nuptk" onkeypress="return hanyaAngka(event)"
                                            type="text" value="{{ $pegawai['nuptk'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" name="tempat_lahir"
                                            value="{{ $pegawai['tempat_lahir'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="tanggal" class="form-control select3">
                                                    <option value="">Tgl</option>
                                                    @for ($i = 1; $i <= 31; $i++)
                                                        <option value="{{ $i }}"
                                                            {{ date('d') == $i ? 'selected' : '' }}>{{ $i }}
                                                        </option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select name="bulan" class="form-control select3">
                                                    <option value="">Bulan</option>
                                                    <option value="01" {{ date('m') == '01' ? 'selected' : '' }}>Januari
                                                    </option>
                                                    <option value="02" {{ date('m') == '02' ? 'selected' : '' }}>Februari
                                                    </option>
                                                    <option value="03" {{ date('m') == '03' ? 'selected' : '' }}>Maret
                                                    </option>
                                                    <option value="04" {{ date('m') == '04' ? 'selected' : '' }}>April
                                                    </option>
                                                    <option value="05" {{ date('m') == '05' ? 'selected' : '' }}>Mei
                                                    </option>
                                                    <option value="06" {{ date('m') == '06' ? 'selected' : '' }}>Juni
                                                    </option>
                                                    <option value="07" {{ date('m') == '07' ? 'selected' : '' }}>Juli
                                                    </option>
                                                    <option value="08" {{ date('m') == '08' ? 'selected' : '' }}>Agustus
                                                    </option>
                                                    <option value="09" {{ date('m') == '09' ? 'selected' : '' }}>
                                                        September
                                                    </option>
                                                    <option value="10" {{ date('m') == '10' ? 'selected' : '' }}>Oktober
                                                    </option>
                                                    <option value="11" {{ date('m') == '11' ? 'selected' : '' }}>November
                                                    </option>
                                                    <option value="12" {{ date('m') == '12' ? 'selected' : '' }}>Desember
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select name="tahun" class="form-control select3">
                                                    <option value="">Tahun</option>
                                                    @php
                                                        $firstYear = (int) date('Y');
                                                        $lastYear = $firstYear - 100;
                                                    @endphp
                                                    @for ($year = $firstYear; $year >= $lastYear; $year--)
                                                        <option value="{{ $year }}"
                                                            {{ date('Y') == $year ? 'selected' : '' }}>
                                                            {{ $year }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Jenis kelamin</label>
                                    <div class="col-md-9">
                                        <div class="form-check form-check-inline my-2">
                                            <input class="form-check-input mx-0" type="radio" name="jenkel" value="l"
                                                {{ $pegawai['jenkel_kode'] == 'l' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Laki laki</label>
                                        </div>
                                        <div class="form-check form-check-inline my-2">
                                            <input class="form-check-input mx-0" type="radio" name="jenkel" value="p"
                                                {{ $pegawai['jenkel_kode'] == 'p' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                    <div class="col-md-9">
                                        <select name="agama" class="form-control">
                                            <option value="">Pilih Agama</option>
                                            <option value="islam" {{ $pegawai['agama'] == 'islam' ? 'selected' : '' }}>
                                                Islam</option>
                                            <option value="kristen"
                                                {{ $pegawai['agama'] == 'kristen' ? 'selected' : '' }}>Kristen</option>
                                            <option value="hindu" {{ $pegawai['agama'] == 'hindu' ? 'selected' : '' }}>
                                                Hindu</option>
                                            <option value="budha" {{ $pegawai['agama'] == 'budha' ? 'selected' : '' }}>
                                                Budha</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 my-auto col-form-label" for="l0">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea name="alamat" class="form-control" rows="2"
                                                    placeholder="Alamat">{{ $pegawai['alamat'] != null ? $pegawai['alamat'] : '-' }}</textarea>
                                            </div>
                                            <div class="col-md-3 my-1">
                                                <input type="text" onkeypress="return hanyaAngka(event)" name="rt"
                                                    placeholder="RT" class="form-control"
                                                    value="{{ $pegawai['rt'] != null ? $pegawai['rt'] : '-' }}">
                                            </div>
                                            <div class="col-md-3 my-1">
                                                <input type="text" onkeypress="return hanyaAngka(event)" name="rw"
                                                    placeholder="RW" class="form-control"
                                                    value="{{ $pegawai['rw'] != null ? $pegawai['rw'] : '-' }}">
                                            </div>
                                            <div class="col-md-6 my-1">
                                                <input type="text" name="dusun" placeholder="Dusun" class="form-control"
                                                    value="{{ $pegawai['dusun'] != null ? $pegawai['dusun'] : '-' }}">
                                            </div>
                                            <div class="col-md-7 my-1">
                                                <input type="text" name="desa" placeholder="Desa" class="form-control"
                                                    value="{{ $pegawai['kelurahan'] != null ? $pegawai['kelurahan'] : '-' }}">
                                            </div>
                                            <div class="col-md-5 my-1">
                                                <input type="text" onkeypress="return hanyaAngka(event)" name="kode_pos"
                                                    placeholder="Kode Pos" class="form-control"
                                                    value="{{ $pegawai['kode_pos'] != null ? $pegawai['kode_pos'] : '-' }}">
                                            </div>
                                            <div class="col-md-12 my-1">
                                                <input type="text" name="kecamatan" placeholder="Kecamatan"
                                                    class="form-control"
                                                    value="{{ $pegawai['kecamatan'] != null ? $pegawai['kecamatan'] : '-' }}">
                                            </div>
                                            <div class="col-md-12 my-1">
                                                <input type="text" name="kabupaten" placeholder="Kabupaten"
                                                    class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">SK Terakhir</label>
                                    <div class="col-md-9">
                                        <select name="sk" class="form-control select3">
                                            <option value="">- SK Terakhir - </option>
                                            <option value="" {{ $pegawai['id_sk'] == null ? 'selected' : '' }}>Kosong
                                            </option>
                                            @foreach ($sk as $sk)
                                                <option value="{{ $sk['id'] }}"
                                                    {{ $pegawai['id_sk'] == $sk['id'] ? 'selected' : '' }}>
                                                    {{ $sk['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">NPWP</label>
                                    <div class="col-md-9">
                                        <input type="text" name="npwp" class="form-control"
                                            value="{{ $pegawai['npwp'] != null ? $pegawai['npwp'] : '-' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control"
                                            value="{{ $pegawai['email'] != null ? $pegawai['email'] : '-' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Golongan</label>
                                    <div class="col-md-9">
                                        <select name="golongan" class="form-control select3" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <option value="">- Pilih Pangkat/Golongan -</option>
                                            <option value=""
                                                {{ $pegawai['id_pangkat_pegawai'] == null ? 'selected' : '' }}>Kosong
                                            </option>
                                            @foreach ($golongan as $gl)
                                                <option value="{{ $gl['id'] }}"
                                                    {{ $pegawai['id_pangkat_pegawai'] == $gl['id'] ? 'selected' : '' }}>
                                                    {{ $gl['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">TMT/Golongan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tmt_golongan"
                                                value="{{ date('d-m-Y', strtotime($pegawai['tmt_golongan'])) }}"
                                                class="form-control datepicker" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Jenis</label>
                                    <div class="col-md-9">
                                        <select name="id_jenis_golongan" class="form-control" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <option value="">- Pilih Jenis Kepegawaian -</option>
                                            <option value=""
                                                {{ $pegawai['id_jenis_pegawai'] == null ? 'selected' : '' }}>Kosong
                                            </option>
                                            @foreach ($jenis as $jn)
                                                <option value="{{ $jn['id'] }}"
                                                    {{ $pegawai['id_jenis_pegawai'] == $jn['id'] ? 'selected' : '' }}>
                                                    {{ $jn['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">TMT Capeg</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tmt_capeg"
                                                value="{{ date('d-m-Y', strtotime($pegawai['tmt_capeg'])) }}"
                                                class="form-control datepicker" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Status</label>
                                    <div class="col-md-9">
                                        <select name="id_status_pegawai" class="form-control select3" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <option value="">- Pilih Status Kepegawaian -</option>
                                            <option value=""
                                                {{ $pegawai['id_status_pegawai'] == null ? 'selected' : '' }}>Kosong
                                            </option>
                                            @foreach ($status as $st)
                                                <option value="{{ $st['id'] }}"
                                                    {{ $pegawai['id_status_pegawai'] == $st['id'] ? 'selected' : '' }}>
                                                    {{ $st['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Jabatan</label>
                                    <div class="col-md-9">
                                        <select name="id_jabatan_pegawai" class="form-control select3" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                            <option value="">- Pilih Jabatan Struktutal -</option>
                                            <option value=""
                                                {{ $pegawai['id_jabatan_pegawai'] == null ? 'selected' : '' }}>Kosong
                                            </option>
                                            @foreach ($jabatan as $jbt)
                                                <option value="{{ $jbt['id'] }}"
                                                    {{ $pegawai['id_jabatan_pegawai'] == $jbt['id'] ? 'selected' : '' }}>
                                                    {{ $jbt['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Sumber Gaji</label>
                                    <div class="col-md-9">
                                        <input type="text" name="digaji_menurut" placeholder="Digaji Menurut"
                                            value="{{ $pegawai['digaji_menurut'] != null ? $pegawai['digaji_menurut'] : '-' }}"
                                            class="form-control" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Gaji Pokok</label>
                                    <div class="col-md-9">
                                        <input type="text" name="gaji_pokok"
                                            value="{{ str_replace(',', '.', number_format($pegawai['gaji_pokok'])) }}"
                                            class="form-control ribuan" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Total Peng</label>
                                    <div class="col-md-9">
                                        <input type="text" name="total" readonly class="form-control" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Whatsapp</label>
                                    <div class="col-md-9">
                                        <input type="text" name="telepon" onkeypress="return hanyaAngka(event)"
                                            placeholder="Nomo Whatsapp"
                                            value="{{ $pegawai['telepon'] != null ? $pegawai['telepon'] : '-' }}"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Masa Golongan</label>
                                    <div class="col-md-9">
                                        <input type="text" name="masa_kerja_golongan"
                                            value="{{ $pegawai['masa_kerja_golongan'] != null ? $pegawai['masa_kerja_golongan'] : '-' }}"
                                            class="form-control" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Masa Keseluruhan</label>
                                    <div class="col-md-9">
                                        <input type="text" name="masa_kerja_keseluruhan"
                                            value="{{ $pegawai['masa_kerja_keseluruhan'] != null ? $pegawai['masa_kerja_keseluruhan'] : '-' }}"
                                            class="form-control" {{ session('role') == 'pegawai' ? 'readonly' : '' }}>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="cancel" class="btn btn-danger"><i class="fas fa-times-circle"></i>
                                        Batal</button>
                                    <button type="submit" class="btn btn-success" id="saveBtn"><i
                                            class="fas fa-sync-alt"></i>
                                        Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '#btnEdit', function() {
                $('#modalPegawai').modal('show');
            });

            $('#formUpdate').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kepegawaian-pegawai_update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        });
    </script>
@endsection
