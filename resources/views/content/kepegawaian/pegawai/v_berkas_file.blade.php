@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    @if (session('role') == 'admin-kepegawaian' || session('id') == (new \App\Helpers\Help())->decode($_GET['k']))
        <button type="button" id="btnAdd" class="btn btn-purple mb-3">
            <li class="fas fa-plus-circle"></li> Tambah
        </button>
    @endif

    <br>
    <div class="table-responsive">
        <table class="table table-stripped table-hover" id="data-tabel">
            <thead>
                <tr class="bg-purple">
                    <th class="text-white">No</th>
                    <th class="text-white">Tanggal Berkas</th>
                    <th class="text-white">Jenis</th>
                    <th class="text-white">Keterangan</th>
                    <th class="text-white">Lampiran</th>
                    <th class="text-white">Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalBerkas" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <form id="formBerkas">
                        <div class="form-group">
                            <input type="hidden" name="id" id="id_berkas">
                            <input type="hidden" name="id_pegawai" id="id_pegawai" value="{{ $pegawai['id'] }}">
                            <label for="l30">Jenis File</label>
                            <select name="id_jenis" id="id_jenis" class="form-control">
                                <option value="">- Pilih Jenis -</option>
                                @foreach ($jenis as $jn)
                                    <option value="{{ $jn['id'] }}">{{ $jn['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" rows="2" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="l39">File Berkas</label>
                            <br>
                            <input id="l39" name="image" type="file">
                            <br><small class="text-muted">Technical information for user</small>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'lampiran',
                        name: 'lampiran',
                        className: 'text-center',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '#btnAdd', function() {
                $('#formBerkas').trigger("reset");
                $('#modelHeading').html("Tambah File Lampiran");
                $('#modalBerkas').modal('show');
                $('#action').val('Add');
            });
            $('body').on('submit', '#formBerkas', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kepegawaian-berkas_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kepegawaian-berkas_update') }}";
                }
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: new FormData(this),
                    cache: false,
                    contentType: false,
                    processData: false,

                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formBerkas').trigger("reset");
                            $('#modalBerkas').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-detail_berkas') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit File Lampiran");
                        $('#id_berkas').val(data.id);
                        $('#id_pegawai').val(data.id_pegawai);
                        $('#id_jenis').val(data.id_jenis_file);
                        $('#keterangan').val(data.keterangan);
                        $('#modalBerkas').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-delete_berkas') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>
@endsection
