@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    <style>
        .my-shadow {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(58 59 69 / 15%) !important;
        }

    </style>

    <div class="row">
        <div class="col-6">
            <select name="bulan" id="bulan" class="form-control select3">
                <option value="">Pilih Bulan..</option>
                <option value="januari" {{ $_GET['bulan'] == 'januari' ? 'selected' : '' }}>
                    Januari</option>
                <option value="februari" {{ $_GET['bulan'] == 'februari' ? 'selected' : '' }}>
                    Februari</option>
                <option value="maret" {{ $_GET['bulan'] == 'maret' ? 'selected' : '' }}>Maret
                </option>
                <option value="april" {{ $_GET['bulan'] == 'april' ? 'selected' : '' }}>April
                </option>
                <option value="mei" {{ $_GET['bulan'] == 'mei' ? 'selected' : '' }}>Mei</option>
                <option value="juni" {{ $_GET['bulan'] == 'juni' ? 'selected' : '' }}>Juni
                </option>
                <option value="juli" {{ $_GET['bulan'] == 'juli' ? 'selected' : '' }}>Juli
                </option>
                <option value="agustus" {{ $_GET['bulan'] == 'agustus' ? 'selected' : '' }}>
                    Agustus</option>
                <option value="september" {{ $_GET['bulan'] == 'september' ? 'selected' : '' }}>
                    September</option>
                <option value="oktober" {{ $_GET['bulan'] == 'oktober' ? 'selected' : '' }}>
                    Oktober</option>
                <option value="november" {{ $_GET['bulan'] == 'november' ? 'selected' : '' }}>
                    November</option>
                <option value="desember" {{ $_GET['bulan'] == 'desember' ? 'selected' : '' }}>
                    Desember</option>
            </select>
        </div>
        <div class="col-md-5">
            @php
                $firstYear = (int) date('Y');
            @endphp
            <select name="tahun" class="form-control select3" id="tahun">
                <option value="">Pilih Tahun..</option>
                @for ($i = $firstYear - 10; $i < $firstYear + 10; $i++)
                    <option value="{{ $i }}" {{ $_GET['tahun'] == $i ? 'selected' : '' }}>
                        {{ $i }}
                    </option>
                @endfor
            </select>
        </div>
        <div class="col">
            <button id="btn-print" class="btn btn-info"><i class="fa-solid fa-print"></i></button>
        </div>
    </div>
    <div id="print-preview" class="p-4">
        <div style="display: flex; justify-content: center; align-items: center;">
            <div style="width: 21cm; height: 30cm; padding: 1cm" class="border my-shadow">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center lh-1 mb-2">
                            <h5 class="box-title my-0">Slip Pembayaran</h5>
                            <span class="fw-normal">Pembayaran untuk bulan
                                {{ ucwords($_GET['bulan']) . ' ' . $_GET['tahun'] }}</span>
                        </div>
                        <div class="d-flex justify-content-end">
                            <span>{{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</span> </div>
                        <div class="row">
                            <input type="hidden" id="id_pegawai" value="{{ $_GET['k'] }}">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">NIP/NIK</span> <small
                                                class="ms-3">{{ !empty($detail) ? $detail['nip'] : '-' }} /
                                                {{ $pegawai['nik'] ?? '-' }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Nama Pegawai </span> <small
                                                class="ms-3">
                                                {{ !empty($detail) ? $detail['nama'] : '-' }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Telepon</span> <small
                                                class="ms-3">{{ $pegawai['telepon'] ?? '-' }}</small> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Jenis Pegawai</span> <small
                                                class="ms-3">{{ $pegawai['jenis_pegawai'] ?? '-' }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Email</span> <small
                                                class="ms-3">{{ $pegawai['email'] ?? '-' }}</small> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Pangkat</span> <small
                                                class="ms-3">{{ $pegawai['pangkat_pegawai'] ?? '-' }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Jenis Kelamin</span> <small
                                                class="ms-3">{{ $pegawai['jenkel'] ?? '-' }}</small> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div> <span class="fw-bolder">Jabatan</span> <small
                                                class="ms-3">{{ $pegawai['jabatan_pegawai'] ?? '-' }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="mt-4 table table-bordered">
                                <tr>
                                    {{-- <th scope="col">Tunjangan</th>
                                        <th scope="col">Nominal</th> --}}
                                    {{-- <th scope="col">Potongan</th>
                                        <th scope="col">Nominal</th> --}}
                                    <td colspan="2" style="width: 50%">
                                        <table class="table">
                                            <thead class="bg-dark text-white">
                                                <tr>
                                                    <th scope="col">Tunjangan</th>
                                                    <th scope="col">Nominal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td scope="row">Gaji Pokok</td>
                                                    <td class="text-right">
                                                        {{ !empty($detail) ? str_replace(',', '.', number_format($detail['gaji_pokok'])) : '-' }}
                                                    </td>
                                                </tr>
                                                @php
                                                    $total_kotor = $detail['gaji_pokok'];
                                                @endphp
                                                @foreach ($detail['tunjangan_potongan'] as $tunjangan)
                                                    @if ($tunjangan['jenis'] == 'tunjangan')
                                                        <tr>
                                                            <td scope="row">{{ $tunjangan['nama'] }}</td>
                                                            <td class="text-right">
                                                                {{ str_replace(',', '.', number_format($tunjangan['nominal'])) }}
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $total_kotor += $tunjangan['nominal'];
                                                        @endphp
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" style="width: 50%">
                                        <table class="table">
                                            <thead class="bg-dark text-white">
                                                <tr>
                                                    <th scope="col">Potongan</th>
                                                    <th scope="col">Nominal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $total_pot = 0;
                                                @endphp
                                                @foreach ($detail['tunjangan_potongan'] as $potongan)
                                                    @if ($potongan['jenis'] == 'potongan')
                                                        <tr>
                                                            <td scope="row">{{ $potongan['nama'] }}</td>
                                                            <td class="text-right">
                                                                {{ str_replace(',', '.', number_format($potongan['nominal'])) }}
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $total_pot += $potongan['nominal'];
                                                        @endphp
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <th scope="col" class="text-center">Penghasilan Kotor (A)</th>
                                    <th class="text-right">{{ str_replace(',', '.', number_format($total_kotor)) }}
                                    </th>
                                    <th scope="col" class="text-center">Total Potongan (B)</th>
                                    <th class="text-right">{{ str_replace(',', '.', number_format($total_pot)) }}
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><span class="fw-bold">Penerimaan Bersih (A - B) : </span>
                                <br>
                                <b>{{ str_replace(',', '.', number_format($total_kotor - $total_pot)) }}</b>
                            </div>
                            <div class="border col-md-8">
                                <div class="d-flex flex-column"> <span>Terbilang</span> <span>
                                        <b>{{ (new \App\Helpers\Help())->terbilang($total_kotor - $total_pot) }}</b></span>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="d-flex justify-content-end">
                            <div class="d-flex flex-column mt-2"> <span class="fw-bolder">For Kalyan Jewellers</span>
                                <span class="mt-4">Authorised Signatory</span>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-md-12">
            <div class="text-center lh-1 mb-2">
                <h6 class="fw-bold">Payslip</h6> <span class="fw-normal">Payment slip for the month of June
                    2021</span>
            </div>
            <div class="d-flex justify-content-end"> <span>Working Branch:ROHINI</span> </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">EMP Code</span> <small class="ms-3">39124</small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">EMP Name</span> <small class="ms-3">Ashok</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">PF No.</span> <small
                                    class="ms-3">101523065714</small> </div>
                        </div>
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">NOD</span> <small class="ms-3">28</small> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">ESI No.</span> <small class="ms-3"></small> </div>
                        </div>
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">Mode of Pay</span> <small class="ms-3">SBI</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">Designation</span> <small class="ms-3">Marketing
                                    Staff (MK)</small> </div>
                        </div>
                        <div class="col-md-6">
                            <div> <span class="fw-bolder">Ac No.</span> <small
                                    class="ms-3">*******0701</small> </div>
                        </div>
                    </div>
                </div>
                <table class="mt-4 table table-bordered">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th scope="col">Earnings</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Deductions</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Basic</th>
                            <td>16250.00</td>
                            <td>PF</td>
                            <td>1800.00</td>
                        </tr>
                        <tr>
                            <th scope="row">DA</th>
                            <td>550.00</td>
                            <td>ESI</td>
                            <td>142.00</td>
                        </tr>
                        <tr>
                            <th scope="row">HRA</th>
                            <td>1650.00 </td>
                            <td>TDS</td>
                            <td>0.00</td>
                        </tr>
                        <tr>
                            <th scope="row">WA</th>
                            <td>120.00 </td>
                            <td>LOP</td>
                            <td>0.00</td>
                        </tr>
                        <tr>
                            <th scope="row">CA</th>
                            <td>0.00 </td>
                            <td>PT</td>
                            <td>0.00</td>
                        </tr>
                        <tr>
                            <th scope="row">CCA</th>
                            <td>0.00 </td>
                            <td>SPL. Deduction</td>
                            <td>500.00</td>
                        </tr>
                        <tr>
                            <th scope="row">MA</th>
                            <td>3000.00</td>
                            <td>EWF</td>
                            <td>0.00</td>
                        </tr>
                        <tr>
                            <th scope="row">Sales Incentive</th>
                            <td>0.00</td>
                            <td>CD</td>
                            <td>0.00</td>
                        </tr>
                        <tr>
                            <th scope="row">Leave Encashment</th>
                            <td>0.00</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <th scope="row">Holiday Wages</th>
                            <td>500.00</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <th scope="row">Special Allowance</th>
                            <td>100.00</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <th scope="row">Bonus</th>
                            <td>1400.00</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <th scope="row">Individual Incentive</th>
                            <td>2400.00</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr class="border-top">
                            <th scope="row">Total Earning</th>
                            <td>25970.00</td>
                            <td>Total Deductions</td>
                            <td>2442.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-4"> <br> <span class="fw-bold">Net Pay : 24528.00</span> </div>
                <div class="border col-md-8">
                    <div class="d-flex flex-column"> <span>In Words</span> <span>Twenty Five thousand nine hundred seventy
                            only</span> </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="d-flex flex-column mt-2"> <span class="fw-bolder">For Kalyan Jewellers</span> <span
                        class="mt-4">Authorised Signatory</span> </div>
            </div>
        </div>
    </div> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#tahun').change(function() {
                loadPegawai($('#bulan').val(), $(this).val())
            });
            $('#bulan').change(function() {
                loadPegawai($(this).val(), $('#tahun').val())
            });

            $("#btn-print").click(function() {
                if ($('#rombel').val() === '') {
                    swa("GAGAL!", "Pilih rombel dulu", "error");
                } else {
                    $('#print-preview').printArea();
                }
            });
        });

        function loadPegawai(bulan, tahun) {
            var tahun_empty = tahun === '';
            var bulan_empty = bulan === '';
            var id_pegawai = $('#id_pegawai').val();
            console.log(id_pegawai);
            if (!bulan_empty && !tahun_empty) {
                window.location.href = 'detail?method=gaji&k=' + id_pegawai + '&bulan=' + bulan + '&tahun=' + tahun;
            } else {
                console.log('empty')
            }
        }
    </script>
@endsection
