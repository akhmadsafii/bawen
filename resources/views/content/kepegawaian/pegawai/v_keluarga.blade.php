@extends('content/kepegawaian/pegawai/v_main_pegawai')
@section('content_pegawai')
    @if (session('role') == 'admin-kepegawaian' || session('id') == (new \App\Helpers\Help())->decode($_GET['k']))
        <button type="button" id="btnAdd" class="btn btn-purple mb-3">
            <li class="fas fa-plus-circle"></li> Tambah
        </button>
    @endif

    <br>
    <div class="table-responsive">
        <table class="table table-stripped table-hover" id="data-tabel">
            <thead>
                <tr class="bg-purple">
                    <th class="text-white">No</th>
                    <th class="text-white">Nama Istri/Suami</th>
                    <th class="text-white">Tempat, Tgl lahir</th>
                    <th class="text-white">NIP/NIK</th>
                    <th class="text-white">Pekerjaan</th>
                    <th class="text-white">Tanggal Nikah</th>
                    <th class="text-white">Istri/Suami ke</th>
                    <th class="text-white">Penghasilan/bulan</th>
                    <th class="text-white">Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalKeluarga" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <form id="formKeluarga">
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_keluarga">
                            <input type="hidden" name="id_pegawai" value="{{ $pegawai['id'] }}">
                            <label class="col-md-3 col-form-label" for="l2">Istri/Suami</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Istri/Suami"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Pekerjaan</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-user-nurse"></i>
                                    </span>
                                    <input class="form-control" id="pekerjaan" name="pekerjaan" placeholder="Pekerjaan"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Tempat lahir</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i>
                                    </span>
                                    <input class="form-control" id="tempat_lahir" name="tempat_lahir"
                                        placeholder="Tempat Lahir" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Tanggal lahir</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-birthday-cake"></i> </span>
                                    <input class="form-control datepicker" id="tgl_lahir" name="tgl_lahir"
                                        placeholder="Tanggal Lahir" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Tanggal Perkawinan</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-heart"></i>
                                    </span>
                                    <input class="form-control datepicker" id="tgl_nikah" name="tgl_nikah"
                                        placeholder="Tanggal Perkawinan" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Pasangan ke</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-restroom"></i>
                                    </span>
                                    <input class="form-control" id="ke" name="ke" placeholder="Istri / Suami ke"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NIP/NIK</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-badge"></i>
                                    </span>
                                    <input class="form-control" onkeypress="return hanyaAngka(event)" id="nik" name="nik"
                                        type="text" placeholder="NIP/NIK">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Pengahasilan/bulan</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-hand-holding-usd"></i> </span>
                                    <input class="form-control ribuan" id="penghasilan" name="penghasilan" type="text"
                                        placeholder="Penghasilan">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'ttl',
                        name: 'ttl'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'pekerjaan',
                        name: 'pekerjaan'
                    },
                    {
                        data: 'tgl_nikah',
                        name: 'tgl_nikah'
                    },
                    {
                        data: 'ke',
                        name: 'ke'
                    },
                    {
                        data: 'penghasilan',
                        name: 'penghasilan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '#btnAdd', function() {
                $('#formKeluarga').trigger("reset");
                $('#modelHeading').html("Tambah Anggota Keluarga");
                $('#modalKeluarga').modal('show');
                $('#action').val('Add');
            });

            $('#formKeluarga').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kepegawaian-keluarga_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kepegawaian-keluarga_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formKeluarga').trigger("reset");
                            $('#modalKeluarga').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-detail_keluarga') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Keluarga Pegawai");
                        $('#id_keluarga').val(data.id);
                        $('#nama').val(data.nama);
                        $('#pekerjaan').val(data.pekerjaan);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tgl_lahir').val(convertDate(data.tgl_lahir));
                        $('#tgl_nikah').val(convertDate(data.tgl_nikah));
                        $('#ke').val(data.ke);
                        $('#nik').val(data.nik);
                        $('#penghasilan').val(data.penghasilan);
                        $('#modalKeluarga').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-delete_keluarga') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>
@endsection
