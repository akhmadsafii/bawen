@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="btn btn-outline-info m-1 active" onclick="changeTitle('tunjangan')"
                                        href="#tunjangan" data-toggle="tab">Tunjangan</a>
                                </li>
                                <li class="nav-item"><a class="btn btn-outline-info m-1" href="#potongan"
                                        onclick="changeTitle('potongan')" data-toggle="tab">Potongan</a></li>
                                <li class="nav-item"><a class="btn btn-outline-info m-1" href="#lembur"
                                        onclick="changeTitle('lembur')" data-toggle="tab">Lembur</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="box-title mr-b-0" id="firstTitle">Data Tunjangan</h5>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <form id="addTunjangan">
                                <div class="form-group row">
                                    <label class="col-md-1 col-form-label" for="l0">Nama</label>
                                    <div class="col-md-11">
                                        <div class="input-group">
                                            <input type="hidden" name="jenis" id="jenis" value="tunjangan">
                                            <input class="form-control" name="nama" placeholder="Masukan Nama"
                                                type="text">
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" type="submit" id="btnPlus"><i
                                                        class="fas fa-plus"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    <form id="formTunjangan">
                                        <table class="table">
                                            <tr class="bg-info">
                                                <th>#</th>
                                                <th>Nama</th>
                                            </tr>
                                            <tbody id="data_tunjangan">
                                                @if (!empty($tunjangan))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($tunjangan as $tnj)
                                                        <tr>
                                                            <input type="hidden" name="id_tunjangan_{{ $no }}"
                                                                value="{{ $tnj['id'] }}">
                                                            <td class="vertical-middle">{{ $no }}</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input class="form-control"
                                                                        name="nama_{{ $no }}"
                                                                        value="{{ $tnj['nama'] }}" type="text">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-danger delete"
                                                                            data-id="{{ $tnj['id'] }}"
                                                                            data-tunjangan="{{ $tnj['jenis'] }}"
                                                                            type="button"><i
                                                                                class="fas fa-trash"></i></button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $no++;
                                                        @endphp
                                                    @endforeach
                                                    <input type="hidden" name="jumlah" value="{{ count($tunjangan) }}">
                                                    <input type="hidden" name="jenis" value="tunjangan">
                                                    <tr>
                                                        <td colspan="2">
                                                            <button type="submit" class="btn btn-info pull-right"
                                                                id="btnUpdate_tunjangan"><i class="fas fa-sync-alt"></i>
                                                                Update</button>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="2" class="text-center">Tunjangan saat ini tidak
                                                            tersedia
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="tab-pane" id="potongan">
                                    <form id="formPotongan">
                                        <table class="table">
                                            <tr class="bg-info">
                                                <th>#</th>
                                                <th>Nama</th>
                                            </tr>
                                            <tbody id="data_potongan">
                                                @if (!empty($potongan))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($potongan as $ptg)
                                                        <tr>
                                                            <input type="hidden" name="id_tunjangan_{{ $no }}"
                                                                value="{{ $ptg['id'] }}">
                                                            <td class="vertical-middle">{{ $no }}</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input class="form-control"
                                                                        name="nama_{{ $no }}"
                                                                        value="{{ $ptg['nama'] }}" type="text">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-danger delete"
                                                                            data-id="{{ $ptg['id'] }}"
                                                                            data-tunjangan="{{ $ptg['jenis'] }}"
                                                                            type="button"><i
                                                                                class="fas fa-trash"></i></button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $no++;
                                                        @endphp
                                                    @endforeach
                                                    <input type="hidden" name="jumlah" value="{{ count($potongan) }}">
                                                    <input type="hidden" name="jenis" value="potongan">
                                                    <tr>
                                                        <td colspan="2">
                                                            <button type="submit" class="btn btn-info pull-right"
                                                                id="btnUpdate_potongan"><i class="fas fa-sync-alt"></i>
                                                                Update</button>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="2" class="text-center">Potongan saat ini tidak
                                                            tersedia
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="tab-pane" id="lembur">
                                    <form id="formLembur">
                                        <table class="table">
                                            <tr class="bg-info">
                                                <th>#</th>
                                                <th>Nama</th>
                                            </tr>
                                            <tbody id="data_lembur">
                                                @if (!empty($lembur))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($lembur as $lmb)
                                                        <tr>
                                                            <input type="hidden" name="id_tunjangan_{{ $no }}"
                                                                value="{{ $lmb['id'] }}">
                                                            <td class="vertical-middle">{{ $no }}</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input class="form-control"
                                                                        name="nama_{{ $no }}"
                                                                        value="{{ $lmb['nama'] }}" type="text">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-danger delete"
                                                                            data-id="{{ $lmb['id'] }}"
                                                                            data-tunjangan="{{ $lmb['jenis'] }}"
                                                                            type="button"><i
                                                                                class="fas fa-trash"></i></button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $no++;
                                                        @endphp
                                                    @endforeach
                                                    <input type="hidden" name="jumlah" value="{{ count($lembur) }}">
                                                    <input type="hidden" name="jenis" value="lembur">
                                                    <tr>
                                                        <td colspan="2">
                                                            <button type="submit" class="btn btn-info pull-right"
                                                                id="btnUpdate_lembur"><i class="fas fa-sync-alt"></i>
                                                                Update</button>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="2" class="text-center">Lemburan saat ini tidak
                                                            tersedia
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#addTunjangan').on('submit', function(event) {
                    event.preventDefault();
                    $("#btnPlus").html(
                        '<i class="fa fa-spin fa-sync-alt"></i>');
                    $("#btnPlus").attr("disabled", true);
                    let tunjangan = $('#jenis').val();
                    $.ajax({
                        url: "{{ route('kepegawaian-tunjangan_potongan_create') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#addTunjangan').trigger("reset");
                                $('#data_' + tunjangan).html(data.tunjangan);
                            }
                            noti(data.icon, data.message);
                            $('#btnPlus').html('<i class="fas fa-plus"></i>');
                            $("#btnPlus").attr("disabled", false);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#btnPlus').html('<i class="fas fa-plus"></i>');
                            $("#btnPlus").attr("disabled", false);
                        }
                    });
                });

                $('#formTunjangan, #formPotongan, #formLembur').on('submit', function(event) {
                    event.preventDefault();
                    let tunjangan = $('#jenis').val();
                    $("#btnUpdate_" + tunjangan).html(
                        '<i class="fa fa-spin fa-sync-alt"></i> Updating..');
                    $("#btnUpdate_" + tunjangan).attr("disabled", true);
                    updateForm($(this).serialize(), tunjangan);
                });

                $(document).on('click', '.delete', function() {
                    let id = $(this).data('id');
                    let tunjangan = $(this).data('tunjangan');
                    let loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        $.ajax({
                            url: "{{ route('kepegawaian-delete_tunjangan_potongan') }}",
                            type: "POST",
                            data: {
                                id,
                                tunjangan
                            },
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-sync-alt"></i>');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#data_' + tunjangan).html(data.tunjangan);
                                }
                                swa(data.status.toUpperCase() + "!", data.message, data
                                    .icon);
                            }
                        })
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    })
                });
            });

            function changeTitle(params) {
                $('#firstTitle').html("Data " + params[0].toUpperCase() + params.slice(1));
                $('#secondTitle').html("Tambah " + params[0].toUpperCase() + params.slice(1));
                $('#formTunjangan').trigger("reset");
                $('#jenis').val(params);
            }

            function updateForm(dataForm, jenis) {
                // console.log(data);
                $.ajax({
                    url: "{{ route('kepegawaian-tunjangan_potongan_update') }}",
                    method: "PUT",
                    data: dataForm,
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data_' + jenis).html(data.tunjangan);
                        }
                        noti(data.icon, data.message);
                        $("#btnUpdate_" + jenis).html(
                            '<i class="fas fa-sync-alt"></i> Update');
                        $("#btnUpdate_" + jenis).attr("disabled", false);
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $("#btnUpdate_" + jenis).html(
                            '<i class="fas fa-sync-alt"></i> Update');
                        $("#btnUpdate_" + jenis).attr("disabled", false);
                    }
                });
            }
        </script>
    @endsection
