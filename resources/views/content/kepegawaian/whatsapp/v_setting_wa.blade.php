@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    <div class="card">
                                        <div class="card-header bg-info">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h5 class="box-title text-white">
                                                        Setting Watshapp
                                                    </h5>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row justify-content-center align-items-center h-100">
                                                <div class="col col-sm-8 col-md-9">
                                                    <form id="formWa">
                                                        <div class="form-group row">
                                                            <input type="hidden" name="id" value="{{ $setting['id'] }}">
                                                            <label class="col-md-2 col-form-label" for="l0">URL
                                                                Whatsapp</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" name="token"
                                                                    placeholder="URL Whatsapp API" type="text"
                                                                    value="{{ $setting['token'] }}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="submit" class="btn btn-success"
                                                                    id="saveBtn"><i class="fas fa-save"></i>
                                                                    Simpan</button>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#formWa').on('submit', function(event) {
                    event.preventDefault();
                    $("#saveBtn").html(
                        '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    $("#saveBtn").attr("disabled", true);
                    $.ajax({
                        url: "{{ route('master_env-update_token') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                            $("#saveBtn").attr("disabled", false);
                            swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                    });
                });
            })
        </script>
    @endsection
