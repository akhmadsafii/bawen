@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js" integrity="sha512-5+FrEmSijjxRArJWeLcCIEgoQYAgU0gSa9MgNMN+tVSS+MPZsEk9a7OkPZr7AzjNJng1Kl+tXOQVtJcsU+Ax0w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js" integrity="sha512-JIy/lz8dAO/TykQs+VjMpd2ytGxPcg+VSqQQ+3i5Gh5oH29bJuXle3El0myalRwxoJV7UnnEGPK9jH9JPMROAA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tunjangan">
                                    <div class="card">
                                        <div class="card-header bg-info">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h5 class="box-title text-white">
                                                        Pesan Massal
                                                    </h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn btn-success float-right" id="saveBtn"><i
                                                            class="fas fa-paper-plane"></i> Kirim</button>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 my-2">
                                                </div>
                                                <div class="col-md-12">
                                                    <form id="formwatshapp">
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <div class="alert alert-warning" role="alert">
                                                                    <button aria-label="Close" class="close"
                                                                        data-dismiss="alert" type="button"><span
                                                                            aria-hidden="true">×</span>
                                                                    </button>
                                                                    <p class="m-0">Pastikan nomor telepon pegawai
                                                                        tidak kosong, untuk
                                                                        menampilkan pegawai dalam list kontak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 col-form-label" for="l0">Nomor
                                                                WhatsApp</label>
                                                            <div class="col-md-10" id="kontak-pegawai">
                                                                <select name="kontak[]" id="input_pegawai"
                                                                    class="select2" multiple>
                                                                    <option value="0">Pilih Pegawai..</option>
                                                                    @foreach ($pegawai as $pg)
                                                                        @if ($pg['telepon'] != null)
                                                                            <option value="{{ $pg['telepon'] }}">
                                                                                {{ $pg['nama'] }} | {{ $pg['nip'] }} |
                                                                                {{ $pg['telepon'] }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-2 col-form-label" for="l10">Pesan</label>
                                                            <div class="col-md-10">
                                                                <textarea name="pesan" rows="3"
                                                                    class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#formwatshapp').on('submit', function(event) {
                    event.preventDefault();
                    $("#saveBtn").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#saveBtn").attr("disabled", true);
                    $.ajax({
                        url: "{{ route('kepegawaian-send_watshapp') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#formwatshapp').trigger("reset");
                                $("#input_pegawai").val(null).trigger('change');
                            }
                            $('#saveBtn').html('<i class="fas fa-paper-plane"></i> Kirim');
                            $("#saveBtn").attr("disabled", false);
                            swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('<i class="fas fa-paper-plane"></i> Kirim');
                            $("#saveBtn").attr("disabled", false);
                        }
                    });
                });

                $('#saveBtn').click(function() {
                    $('#formwatshapp').submit();
                });
            })
        </script>
    @endsection
