@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix border-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 text-uppercase">{{ session('title') }}</h5>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-12">
                            <button id="addData" class="btn btn-outline-info mt-1"><i class="fas fa-plus-circle"></i>
                                Tambah</button>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formResult">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_sk">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="fomAddEkstra">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data_tabel = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#addData').click(function() {
                $('#formResult').trigger("reset");
                $('.tambahBaris').show('');
                $('.fomAddEkstra').html('');
                $('#modelHeading').html("Tambah SK");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#formResult').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kepegawaian-sk_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kepegawaian-sk_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formResult').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data-tabel').DataTable().ajax.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $('#form_result').html('');
                $('.fomAddEkstra').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kepegawaian-detail_sk') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-sync-alt"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit SK");
                        $('#id_sk').val(data.id);
                        $('#nama').val(data.nama);
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kepegawaian-delete_sk') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-sync-alt"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').DataTable().ajax.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddEkstra').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Keterangan</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }
    </script>
@endsection
