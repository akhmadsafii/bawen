@extends('content.supervisor.learning.kelas.v_pilihan')
@section('content_supervisor')
    <div class="row justify-content-md-center">
        @foreach ($rombel as $rmb)
            @php
                $warna = ['secondary', 'success', 'info', 'danger', 'warning'];
            @endphp
            <div class="col-lg-3 col-sm-4 col-12 text-center">
                <a href="{{ route('multiple_rombel', $rmb['id_code']) }}">
                    <div class="row main-box-layout img-thumbnail">
                        <div class="col-lg-12 col-sm-12 col-12 box-icon-section bg-{{ $warna[array_rand($warna, 1)] }}">
                            <i class="fa fa-users" aria-hidden="true"> {{ $rmb['kelas_romawi'] }}</i>
                        </div>
                        <div class="col-lg-12 col-sm-12 col-12 box-text-section">
                            <p>{{ $rmb['nama'] }}</p>
                        </div>
                        <div class="label">
                            <h3><span class="badge badge-pill bg-primary">{{ $rmb['jurusan'] }}</span></h3>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
