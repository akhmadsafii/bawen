<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <h2 class="text-center" style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">PENCAPAIAN
        KOMPETENSI
        PESERTA DIDIK</h2>
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 30px" class="vertical-top">
                                {{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top">VII B</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">
                                {{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">VIRDA AGUSTINA
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">2021-2022</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">212207001 / 81469014</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                        </tr>
                    </thead>
                </table>
                <table class="table">
                    <tr>
                        <th class="text-left" style="width: 20px">A.</th>
                        <th class="text-left" style="width: 150px">SIKAP</th>
                    </tr>
                    <tr>
                        <th class="text-left">1.</th>
                        <th class="text-left">Sikap Spiritual</th>
                    </tr>
                    <tr>
                        <th class="text-center" colspan="2"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Predikat</th>
                        <th class="text-center"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Deskripsi</th>
                    </tr>
                    <tr>
                        <td class="text-center vertical-top" colspan="2" style="border: solid 2px black; padding: 3px;">
                            Baik</td>
                        <td style="border: solid 2px black; padding: 3px;">
                            <p class="m-0">Selalu berdoa sebelum dan sesudah melakukan kegiatan, menjalankan
                                ibadah
                                sesuai
                                dengan agamanya dan memberi salam pada saat awal dan akhir kegiatan. Sedangkan sikap
                                bersyukur atas nikmat dan karunia Tuhan Yang Maha Esa dan berserah diri (tawakal)
                                kepada Tuhan setelah berikhtiar atau melakukan usaha mulai berkembang</p>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">2.</th>
                        <th class="text-left" colspan="2">Sikap Sosial</th>
                    </tr>
                    <tr>
                        <th class="text-center" colspan="2"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Predikat</th>
                        <th class="text-center"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Deskripsi</th>
                    </tr>
                    <tr>
                        <td class="text-center vertical-top" colspan="2" style="border: solid 2px black; padding: 3px;">
                            Baik</td>
                        <td style="border: solid 2px black; padding: 3px;">
                            <p class="m-0">Selalu menunjukkan sikap jujur, disiplin dan tanggung jawab dengan
                                sangat
                                baik, sikap
                                toleransi dan gotong royong dengan baik,. </p>
                        </td>
                    </tr>
                </table>
                <br>
                <table class="table">
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="4">PENGETAHUAN</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">No
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7%">
                                Nilai
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7px">
                                Predikat</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td colspan="5"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Kelompok A</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                                dan Budi Pekerti</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    Memahami
                                    bahwa
                                    Allah menyelamatkan
                                    manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                                    sebagai penebus manusia</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan
                                Pancasila dan
                                Kewarganegaraan</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">89
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    Memahami
                                    norma
                                    yang berlaku dalam
                                    masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                                    sebagai dasar Negara
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">84
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                    Mengidentifikasi
                                    teks
                                    prosedur, cukup kemampuan
                                    dalam Mengidentifikasi informasi teks deskripsi
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Matematika</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">88
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    menjelaskan
                                    bentuk aljabar dan melakukan
                                    operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                                    geometri dalam masalah kontekstual
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Alam</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">83
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    Menganalisis
                                    konsep energi, berbagai
                                    sumber energi, cukup kemampuan dalam Menerapkan konsep pengukuran
                                    berbagai besaran
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Sosial</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">81
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    memahami
                                    konsep
                                    interaksi sosial, perlu
                                    dimaksimalkan kemampuan dalam memahami pengaruh interaksi sosial
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik dalam
                                    Mengidentifikasi
                                    unsur kebahasaan terkait
                                    simple present dan Mengidentifikasi unsur yang terkandung dalam teks Descriptive
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 20px">

                            </td>
                        </tr>
                    </tbody>
                    <thead>

                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>

                </table>
            </td>
        </tr>
    </table>
    <table>
        <thead>
            <tr>
                <td>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 20px">
                                    No
                                </th>
                                <th class="text-center"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 25%">
                                    Mata
                                    Pelajaran</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7%">
                                    Nilai
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7px">
                                    Predikat</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Deskripsi</th>
                            </tr>
                            <tr>
                                <td colspan="5"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Kelompok B</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    92
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Memahami
                                        konsep dasar permainan alat
                                        musik sederhana, baik kemampuan dalam Memahami keunikan gerak tari kreasi
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani, Olah
                                    Raga, dan Kesehatan</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    84
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        Memahami
                                        keterampilan Atletik, cukup
                                        kemampuan dalam Memahami konsep permainan bola kecil
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Prakarya</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    80
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        membuat
                                        sebuah sheet
                                        yang mengandung data,
                                        cukup kemampuan dalam manegenal dat berupa angka dan hasil perhitungan
                                        rumus
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    89
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Menyimak
                                        kaulinan barudak melalui teks
                                        bacaan , cukup kemampuan dalam Menyimak pangalaman sorangan melalui teks
                                        bacaan
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Mandarin</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    86
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Mengidentifikasi kata, frase atau kalimat
                                        dalam suatu konteks , baik kemampuan dalam Pengenalan Pinyin dasar
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    6
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Tata Boga</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    86
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        Memahami ilmu dasar
                                        dan teknik-teknik
                                        pengawetan dan Memahami pengertian dasar dan ruang lingkup ilmu tata boga
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">C.</th>
                                <th class="text-left" colspan="4">KETERAMPILAN</th>
                            </tr>
                            <tr>
                                <th class="text-left"></th>
                                <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
                            </tr>
                            <tr>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    No
                                </th>
                                <th class="text-center"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 25%">
                                    Mata
                                    Pelajaran</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7%">
                                    Nilai
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7px">
                                    Predikat</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Deskripsi</th>
                            </tr>
                            <tr>
                                <td colspan="5"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Kelompok A</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    1
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                                    dan Budi Pekerti</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    96
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    A
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam mampu
                                        bersaksi tentang Yesus sebagai
                                        penebus manusia dan mampu hidup melayani sesama manusia</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    2
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Pendidikan
                                    Pancasila dan
                                    Kewarganegaraan</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    93
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    A
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam Menyaji
                                        sejarah penetapan Pancasila
                                        sebagai dasar negara, baik keterampilan dalam Menyaji proses perumusan Pancasila
                                        sebagai dasar negara
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    91
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam
                                        Menjelaskan isi teks deskripsi objek, baik
                                        keterampilan dalam Menyimpulkan isi teks prosedur
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    4
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Matematika</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    88
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam
                                        menyelesaikan masalah yang berkaitan
                                        dengan bentuk aljabar, cukup keterampilan dalam Menyelesaikan masalah berkaitan
                                        dengan transformasi geometr
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    5
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Alam</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    83
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    C
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam
                                        Menyajikan hasil percobaan tentang
                                        perubahan bentuk energi, cukup keterampilan dalam Menyajikan hasil
                                        pengklasifikasian makhluk hidup dan benda
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    6
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Sosial</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    81
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    C
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam
                                        menjelaskan konsep ruang, baik
                                        keterampilan dalam mengidentifikasi pengaruh interaksi sosial
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    7
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    92
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki keterampilan sangat baik
                                        dalam
                                        Menuturkan ungkapan perkenalan orang
                                        lain dan Menyusun teks terkait kalimat sapaan
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <div class="pagebreak"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="5"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Kelompok B</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    1
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    92
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Memahami
                                        konsep dasar permainan alat
                                        musik sederhana, baik kemampuan dalam Memahami keunikan gerak tari kreasi
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    2
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani, Olah
                                    Raga, dan Kesehatan</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    84
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        Memahami
                                        keterampilan Atletik, cukup
                                        kemampuan dalam Memahami konsep permainan bola kecil
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Prakarya</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    80
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    C
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        membuat
                                        sebuah
                                        sheet
                                        yang mengandung data,
                                        cukup kemampuan dalam manegenal dat berupa angka dan hasil perhitungan
                                        rumus
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    4
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    89
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Menyimak
                                        kaulinan barudak melalui teks
                                        bacaan , cukup kemampuan dalam Menyimak pangalaman sorangan melalui teks
                                        bacaan
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    5
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Bahasa Mandarin</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    86
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                                        dalam
                                        Mengidentifikasi kata, frase atau kalimat
                                        dalam suatu konteks , baik kemampuan dalam Pengenalan Pinyin dasar
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    6
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Tata Boga</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    86
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                                        Memahami ilmu dasar
                                        dan teknik-teknik
                                        pengawetan dan Memahami pengertian dasar dan ruang lingkup ilmu tata boga
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-center vertical-middle" rowspan="2"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    KKM
                                </th>
                                <th colspan="4" class="text-center"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Predikat</th>
                            </tr>
                            <tr>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Kurang (D)
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Cukup (C)
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Baik (B)
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Sangat Baik (A)
                                </th>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    75
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    < 75 </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    75 - 83
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    84 - 92
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    93 - 100
                                </td>

                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">D.</th>
                                <th class="text-left" colspan="3">EKSTRAKURIKULER</th>
                            </tr>
                            <tr>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    No
                                </th>
                                <th class="text-center"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 200px">
                                    Kegiatan Ekstrakurikuler</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 50px">
                                    Nilai
                                </th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Keterangan</th>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    1
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Pendidikan Kepramukaan</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    2
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">English Club</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">Choir</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">KPA</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    {{-- <div class="pagebreak"></div> --}}
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">E.</th>
                                <th class="text-left" colspan="2">PRESTASI</th>
                            </tr>
                            <tr>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    No
                                </th>
                                <th class="text-center"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 200px">
                                    Jenis Prestasi</th>
                                <th class="text-center vertical-middle"
                                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                    Keterangan</th>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    1
                                </td>
                                <td style="border: solid 2px black; padding: 3px;"></td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    2
                                </td>
                                <td style="border: solid 2px black; padding: 3px;"></td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    3
                                </td>
                                <td style="border: solid 2px black; padding: 3px;"></td>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px"></p>
                                </td>
                            </tr>
                        </tbody>
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                    </table>
                    <br>
                    <table>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">F.</th>
                                <th class="text-left" colspan="2">KETIDAKHADIRAN</th>
                            </tr>
                        </tbody>
                    </table>
                    <table style="border-collapse: collapse;">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px; width: 228px">
                                    Sakit
                                </td>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px;  width: 228px">
                                    0 Hari</td>
                            </tr>
                            <tr>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px; width: 228px">
                                    Izin
                                </td>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px;  width: 228px">
                                    0 Hari</td>
                            </tr>
                            <tr>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px; width: 228px">
                                    Tanpa Keterangan
                                </td>
                                <td class="text-left vertical-middle"
                                    style="border: solid 2px black; padding: 3px;  width: 228px">
                                    6 Hari</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="pagebreak"></div>
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">G.</th>
                                <th class="text-left" colspan="2">CATATAN WALI KELAS</th>
                            </tr>
                            <tr>
                                <td class="text-left vertical-middle" colspan="3"
                                    style="border: solid 2px black; padding: 3px;">
                                    <div style="width: 100%; min-height: 60px">
                                        <p class="m-0">Memiliki kemampuan baik dalam
                                            Memahami ilmu dasar
                                            dan teknik-teknik
                                            pengawetan dan Memahami pengertian dasar dan ruang lingkup ilmu tata boga
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table">
                        <thead>
                            <!-- biar bisa ganti lembar otomatis -->
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-left" style="width: 20px">H.</th>
                                <th class="text-left" colspan="2">TANGGAPAN ORANG TUA/WALI</th>
                            </tr>
                            <tr>
                                <td class="text-left vertical-middle" colspan="3"
                                    style="border: solid 2px black; padding: 3px;">
                                    <div style="width: 100%; min-height: 60px">
                                        <p class="m-0">
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table style="border-collapse: collapse;">
                        <tbody>
                            <tr>
                                <td class="text-left vertical-middle" colspan="3"
                                    style="border: solid 2px black; padding: 3px; width: 456px;">
                                    <div style="width: 100%; min-height: 100px">
                                        <b>Keputusan : </b>
                                        <p class="m-0">
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }},
                                            {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                                        <p class="m-0">Wali Kelas,</p>
                                        <br><br><br><br>
                                        <b>Naillil Fitri, S.Pd</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        <br><br><br><br>
                                        <b>Muharrom, S.Pd., M.M</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
    </table>
</body>

</html>
