<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <center>
        {{-- <img src="{{ asset('asset/img/tutwuri.png') }}" alt="" width="200"> --}}
        <h2 class="text-center" style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
            PENILAIAN
            TENGAH SEMESTER GANJIL</h2>
    </center>
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <th style="width: 300px; padding-right: 30px" class="vertical-top text-left">
                                {{ strtoupper($sekolah['nama']) ?? '-' }}
                            </th>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <th class="vertical-top text-left">VII B</th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left" style="width: 300px; padding-right: 30px">
                                {{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </th>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">1 (Satu)</th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left" style="width: 300px; padding-right: 30px">
                                VIRDA AGUSTINA
                            </th>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">2021-2022</th>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">212207005 / 98163335</th>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                        </tr>
                    </thead>
                </table>
                <br>
                <table class="table">
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                No
                            </th>
                            <th class="text-center" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                KKM
                            </th>
                            <th class="text-center vertical-middle" colspan="10"
                                style="border: solid 2px black; padding: 3px;">
                                Hasil Penilaian Harian (HPH)</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px;">HPTS</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px;">
                                KETERANGAN</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" colspan="5"
                                style="border: solid 2px black; padding: 3px;">
                                Pengetahuan</th>
                            <th class="text-center vertical-middle" colspan="5"
                                style="border: solid 2px black; padding: 3px;">
                                Ketrampilan</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                1</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                2</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                3</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                4</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                5</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                1</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                2</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                3</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                4</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                5</th>
                        </tr>
                        <tr>
                            <th colspan="15" class="text-left" style="border: solid 2px black; padding: 3px;">
                                Kelompok A</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pendidikan Agama
                                    dan Budi Pekerti</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pendidikan Pancasila dan
                                    Kewarganegaraan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">100
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">100
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">100
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">100
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">100
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Bahasa Indonesia</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">96
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Matematika</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">96
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Ilmu Pengetahuan Alam</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">96
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Ilmu Pengetahuan Sosial</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">96
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Bahasa Inggris</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">90
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">95
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">94
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">93
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">96
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <th colspan="15" class="text-left" style="border: solid 2px black; padding: 3px;">
                                Kelompok B</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Seni Budaya</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pendidikan Jasmani, Olah
                                    Raga, dan Kesehatan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Prakarya</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Bahasa Sunda</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Bahasa Mandarin</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Tata Boga</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">98
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="15" style="height: 20px">
                                <p class="m-0" style="font-size: 12px"><i>HPTS= Hasil Penilaian Tengah
                                        Semester (khusus pada aspek Pengetahuan)</i></p>
                            </td>
                        </tr>
                    </tbody>
                    <thead>
                    </thead>

                </table>
                <table class="table">
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                KKM
                            </th>
                            <th colspan="4" class="text-center" style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Kurang (D)
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Cukup (C)
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Baik (B)
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Sangat Baik (A)
                            </th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                < 75 </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                75 - 83
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                84 - 92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                93 - 100
                            </td>

                        </tr>
                    </tbody>
                </table>
                <br>
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 40%">
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </td>
                            <td style="width: 20%"></td>
                            <td style="width: 40%;">
                                <div style="float:right">
                                    <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }},
                                        {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    <b>Naillil Fitri, S.Pd</b>
                                    <p class="m-0">NIP. -</p>
                                </div>
                            </td>
                        </tr>

                    </tbody>
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>
