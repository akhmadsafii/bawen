<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="7">
                    <table>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Peserta Didik</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 220px; padding-right: 10px">VIRDA AGUSTINA
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">10-Perawat B</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Bidang Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 220px; padding-right: 10px">Kesehatan
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Program Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 220px; padding-right: 10px">Keperawatan
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">2019/2020</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 220px; padding-right: 10px">0041213795
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <hr style="border: solid 2px #000">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="7" style="text-align: center; font-size: 14pt">LAPORAN HASIL BELAJAR TENGAH SEMESTER
                </th>
            </tr>
            <tr>
                <td style="height: 10px" colspan="7"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">A.</th>
                <th class="text-left" colspan="4">PENGETAHUAN DAN KETERAMPILAN</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    KKM
                </th>
                <th class="text-center vertical-middle" colspan="3" style="border: solid 2px black; padding: 3px;">
                    Pengetahuan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Ketrampilan
                </th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Nilai Harian
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    PTS
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Rata - rata
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Rata - rata
                </th>
            </tr>
            <tr>
                <td colspan="7" style="border: solid 2px black; padding: 3px;">
                    Muatan Nasional</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                    dan Budi Pekerti</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">81.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">81.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">81.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Pancasila danKewarganegaraan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    76
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika (Umum)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    76
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                </td>
                <td style="border: solid 2px black; padding: 3px;">Sejarah Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris dan Bahasa AsingLainnya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    76
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
            </tr>

        </tbody>

        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="7" style="border: solid 2px black; padding: 3px;">
                    Muatan Kewilayahan</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.00
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani, Olahraga, danKesehatan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">83.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">82.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">82.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85.33
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="7" style="border: solid 2px black; padding: 3px;">
                    Muatan Lokal</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    75
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78.00
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="2">PRESTASI</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Jenis Kegiatan</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                2</td>
                            <td style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <thead></thead>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">C.</th>
                <th class="text-left" colspan="6">KETIDAKHADIRAN</th>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="table">
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Sakit</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">Hari
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Izin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Tanpa Keterangan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>

                    </table>
                </td>
            </tr>
        </tbody>
        <thead></thead>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="6">CATATAN WALI KELAS</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="7">
                                <div style="min-height: 50px;">
                                    <p class="m-0 text-justify"></p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

        </tbody>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="6">TANGGAPAN ORANG TUA/WALI</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="7">
                                <div style="min-height: 50px;">
                                    <p class="m-0 text-justify"></p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }}, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                                        <p class="m-0">Wali Kelas,</p>
                                        <br><br><br><br>
                                        <b>Naillil Fitri, S.Pd</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        <br><br><br><br>
                                        <b>Muharrom, S.Pd., M.M</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
