<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Peserta Didik</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">VIRDA AGUSTINA
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">10-Perawat B</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Bidang Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">Kesehatan dan Pekerjaan
                                Sosial
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Program Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">Keperawatan
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">2019/2020</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">0041213795
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">A.</th>
                <th class="text-left" colspan="4">NILAI AKADEMIK</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px; width: 40%">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Pengetahuan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Ketrampilan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Nilai Akhir
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Predikat</th>
            </tr>
            <tr>
                <td colspan="6" style="border: solid 2px black; padding: 3px;">
                    Muatan Nasional</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                    dan Budi Pekerti</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    81
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">87
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B+
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Pancasila dan Kewarganegaraan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B+
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika (Umum)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">80
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B+
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                </td>
                <td style="border: solid 2px black; padding: 3px;">Sejarah Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    86
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">86
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">A-
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris dan Bahasa Asing Lainnya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">A-
                </td>
            </tr>
        </tbody>

        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="6" style="border: solid 2px black; padding: 3px;">
                    Muatan Kewilayahan</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">A-
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani, Olahraga, danKesehatan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    82
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">85
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B+
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="6" style="border: solid 2px black; padding: 3px;">
                    Muatan Lokal</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">78
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">B.</th>
                <th class="text-left" colspan="5">CATATAN AKADEMIK</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;" colspan="6">
                    <div style="min-height: 50px;">
                        <p class="m-0 text-justify" style="">Virda Agustina : <br> Secara konsisten
                            menunjukkan pemahaman yang mendalam pada semua materi Sejarah Indonesia, BahasaInggris dan
                            Seni Budaya.Secara konsisten menunjukkan pemahaman yang mendalam pada sebagian besar materi
                            Fisika, Simulasi danKomunikasi Digital, Konsep Dasar Keperawatan, Matematika, Farmakologi
                            Dasar, Komunikasi Keperawatan,Pendidikan Pancasila dan Kewarganegaraan, Pendidikan Agama
                            Islam dan Budi Pekerti, Bahasa Indonesia, IlmuKesehatan Masyarakat, Pendidikan Jasmani,
                            Olahraga, dan Kesehatan, Biologi, Kimia dan Muatan Lokal BahasaDaerah</p>
                    </div>
                </td>
            </tr>
        </tbody>

        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">C.</th>
                            <th class="text-left" colspan="4">PRAKTIK KERJA LAPANGAN</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Mitra DU/DI</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Lokasi</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Lamanya</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="2">EKSTRAKURIKULER</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Kegiatan Ekstrakurikuler</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td style="border: solid 2px black; padding: 3px;">Ikatan Remaja Masjid</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Sangat Baik,
                                Ananda sangat baik dan Aktif dalam mengikuti segala kegiatanEkstrakulikuler IREMA</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                2</td>
                            <td style="border: solid 2px black; padding: 3px;">Marawis</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Sangat Baik,
                                Ananda bisa menujukan sikap yang sangat baik dan aktif dalam mengikuti setiap kegiatan
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="pagebreak"></div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">E.</th>
                <th class="text-left" colspan="5">KETIDAKHADIRAN</th>
            </tr>
            <tr>
                <td colspan="3">
                    <table class="table">
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Sakit</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">Hari
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Izin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Tanpa Keterangan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }}, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                                        <p class="m-0">Wali Kelas,</p>
                                        <br><br><br><br>
                                        <b>Naillil Fitri, S.Pd</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        <br><br><br><br>
                                        <b>Muharrom, S.Pd., M.M</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="pagebreak"></div>
                </td>
            </tr>
        </tbody>

        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">F.</th>
                <th class="text-left" colspan="5">DESKRIPSI PERKEMBANGAN KARAKTER</th>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Karakter yang dibangun</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;">Religius</td>
                            <td class="text-justify" style="border: solid 2px black; padding: 3px;">mengajak temannya
                                untuk berdoa sebelum memulai pembelajaran</td>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;">Nasionalis</td>
                            <td class="text-justify" style="border: solid 2px black; padding: 3px;">selalu menanamkan
                                rasa jujur, percaya diri, suka menolong dan sudah mampumeningkatkan sikap disiplin</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">G.</th>
                <th class="text-left" colspan="5">CATATAN PERKEMBANGAN KARAKTER</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;" colspan="6">
                    <div style="min-height: 50px;">
                        <p class="m-0 text-justify" style="">Virda Agustina Mempunyai Komitmen Tinggi Untuk Bekerja
                            Dengan Teliti dan Menggunakan Waktu SecaraEfisien Sehingga Dapat Menyelesaikan Tugas Dengan
                            Tepat Waktu, Ia Selalu Berusaha Untuk Positif, SenangBekerja Sama Dengan Orang Lain dan
                            Mandiri Dalam Mengerjakan Tugas</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }}, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                                        <p class="m-0">Wali Kelas,</p>
                                        <br><br><br><br>
                                        <b>Naillil Fitri, S.Pd</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        <br><br><br><br>
                                        <b>Muharrom, S.Pd., M.M</b>
                                        <p class="m-0">NIP. -</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>

    </table>
</body>

</html>
