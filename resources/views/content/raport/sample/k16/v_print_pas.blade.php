<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        td.fitwidth {
            width: 1px;
            white-space: nowrap;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .dontsplit {
            /* page-break-inside: avoid; */
            /* page-break-inside: auto; */
            page-break-before: always;
        }

        .gantiAuto {
            /* page-break-inside: avoid; */
            page-break-inside: auto;
            /* page-break-before: always; */
        }

    </style>

</head>

<body>
    <table>
        <thead>
            <tr>
                <td rowspan="5" style="vertical-align: middle; text-align: center">
                    <img src="{{ $sekolah['file'] }}" style="max-height:138px; width: 128px">
                </td>
                <td style="text-align: center; width: 70%">
                    <b>DINAS PENDIDIKAN KABUPATEN {{ strtoupper($sekolah['kabupaten']) }}</b>
                </td>
                <td rowspan="5" style="vertical-align: middle; text-align: center">
                    <img src="{{ public_path('asset/img/tutwuri.png') }}" style="max-height:138px; width: 128px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <small style="margin: 0px; padding: 0px"><b>UNIT PENDIDIKAN KECAMATAN
                            {{ strtoupper($sekolah['kecamatan'] ?? '-') }}</b></small>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <h2 style="margin: 0">
                        <b style="margin: 0">{{ strtoupper($sekolah['nama'] ?? '-') }}</b>
                    </h2>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <b style="margin: 0">{{ $sekolah['alamat'] ?? '-' }}
                        {{ $sekolah['rt'] ? 'RT ' . $sekolah['rt'] : '' }}
                        {{ $sekolah['rw'] ? 'RW ' . $sekolah['rw'] : '' }} {{ $sekolah['kecamatan'] ?? '' }},
                        {{ $sekolah['kabupaten'] ?? '' }} {{ $sekolah['kode_pos'] ?? '' }}</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <small
                        style="white-space: pre-line; margin: 0">{{ $sekolah['telepon'] ? 'Telp. ' . $sekolah['telepon'] : '' }}</small>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>RAPORT SEMESTER
                        GENAP TAHUN
                        PELAJARAN 2020/2021</u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="3">

                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td width="20%">Nama Peserta Didik</td>
                                <td width="1%">:</td>
                                <td width="39%" class="tbl">Bagus Bekti Pradeka</td>
                                <td width="20%">Kelas / Semester</td>
                                <td width="1%">:</td>
                                <td width="19%" class="tbl">
                                    10 / GENAP
                                </td>
                            </tr>
                            <tr>
                                <td>NIS</td>
                                <td>:</td>
                                <td> 314125535</td>
                                <td>Tahun Pelajaran</td>
                                <td>:</td>
                                <td class="tbl">2020/2021</td>
                            </tr>
                            <tr>
                                <td>NISN</td>
                                <td>:</td>
                                <td class="tbl">42343254566</td>
                                <td>Program Keahlian</td>
                                <td>:</td>
                                <td class="tbl">Teknik Informatika</td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td colspan="4"><b>CAPAIAN KOMPETENSI</b></td>
                            </tr>
                            <tr>
                                <td colspan="6"><b>A. Sikap</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table style="margin-left: 15px; width: 100%">
                                        <tr>
                                            <td width="3%"><b>1.</b></td>
                                            <td width="97%"><b>Sikap Spiritual</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="border: solid 1px #000; padding: 10px">
                                                Deskripsi :
                                                Selalu melakukan sikap :
                                                Baik
                                                Mulai meningkat pada sikap :
                                                Sopan
                                            </td>

                                        </tr>
                                        <tr>
                                            <td width="3%"><b>2.</b></td>
                                            <td width="97%"><b>Sikap Sosial</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="border: solid 1px #000; padding: 10px">
                                                Selalu melakukan sikap : Mandiri.
                                                Mulai meningkat pada sikap :
                                                Disiplin, Jujur;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>B. Pengetahuan dan Keterampilan</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%">No</th>
                                                <th style="width: 50%">Mata Pelajaran</th>
                                                <th style="width: 8%">Nilai Akhir</th>
                                                <th style="width: 37%">Capaian Pembelajaran</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">1</td>
                                                <td class="center">
                                                    Pendidikan Agama
                                                    dan Budi Pekerti
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Memahami dengan baik makna
                                                    komitmen
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">2</td>
                                                <td class="center">
                                                    Pendidikan Pancasila
                                                    dan Kewarganegaraan
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Menganalisis dinamika peran
                                                    Indonesia dalam perdamaian
                                                    dunia sesuai UUD NRI Tahun
                                                    1945
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">3</td>
                                                <td class="center">
                                                    Bahasa Indonesia
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Menganalisis dinamika peran
                                                    Indonesia dalam perdamaian
                                                    dunia sesuai UUD NRI Tahun
                                                    1945
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">4</td>
                                                <td class="center">
                                                    Matematika (Wajib)
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Menganalisis dinamika peran
                                                    Indonesia dalam perdamaian
                                                    dunia sesuai UUD NRI Tahun
                                                    1945
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">5</td>
                                                <td class="center">
                                                    Sejarah Indonesia
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Menganalisis dinamika peran
                                                    Indonesia dalam perdamaian
                                                    dunia sesuai UUD NRI Tahun
                                                    1945
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">6</td>
                                                <td class="center">
                                                    Bahasa Inggris
                                                    (Wajib)
                                                </td>
                                                <td style="text-align: center">
                                                    93
                                                </td>
                                                <td class="center">
                                                    Menganalisis dinamika peran
                                                    Indonesia dalam perdamaian
                                                    dunia sesuai UUD NRI Tahun
                                                    1945
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table>
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>C. Ekstrakurikuler</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="30%">Nama Kegiatan</th>
                                                <th width="10%">Nilai</th>
                                                <th width="55%">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="ctr">1</td>
                                                <td>Pramuka</td>
                                                <td class="ctr">A</td>
                                                <td>Memuaskan, aktif mengikuti kegiatan Pramuka mingguan</td>
                                            </tr>
                                            <tr>
                                                <td class="ctr">2</td>
                                                <td>Drum Band</td>
                                                <td class="ctr">B</td>
                                                <td>Cukup Memuaskan, aktif mengikuti kegiatan Drum Band mingguan</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>D. Ketidakhadiran</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table width="100%" style="margin-left: 15px; width: 100%">
                                        <tr>
                                            <td width="40%">
                                                <table class="table" width="100%">
                                                    <tr>
                                                        <td width="60%">Sakit</td>
                                                        <td width="40%" class="ctr">
                                                            0 &nbsp; hari
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Izin</td>
                                                        <td class="ctr">
                                                            2
                                                            &nbsp;
                                                            hari
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanpa Keterangan</td>
                                                        <td class="ctr">0
                                                            &nbsp;
                                                            hari
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="60%">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <div class="dontsplit"></div>

                    </table>
                    <table style="width: 100%; margin-top: 12px; border-collapse: collapse;">
                        <tbody>
                            <tr>
                                <th colspan="5" style="text-align: center; font-weight: bold; font-size: 14pt;">
                                    <u>CATATAN PRESTASI YANG PERNAH DICAPAI</u>
                                </th>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr style="width: 50%">
                                <th colspan="5">
                                    <table>
                                        <tr>
                                            <th style="text-align: left">Nama Peserta Didik</th>
                                            <th>:</th>
                                            <th style="text-align: left">Bagus Bekti Pradeka</th>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left">NIS/NISN</th>
                                            <th>:</th>
                                            <th style="text-align: left">
                                                3123143535 / 4234354567</th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2" style="height: 10px"></th>
                            </tr>
                            <tr>
                                <th style="width: 5px !important; border: 1px solid; font-weight: regular">No</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4">Prestasi yang
                                    pernah dicapai</th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">1</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4">Juara 1 Pramuka TIngakat
                                    Provinsi
                                </th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">2</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">3</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">4</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">5</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">6</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">7</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">8</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">9</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">10</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">11</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">12</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">13</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">14</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid; font-weight: regular">15</th>
                                <th style="border: 1px solid; font-weight: regular" colspan="4"></th>
                            </tr>
                        </tbody>
                    </table>
                    <table style="width: 100%; margin-top: 12px">
                        <tbody>
                            <tr>
                                <td colspan="6" style="text-align: center; font-weight: bold; font-size: 14pt">
                                    <u>CATATAN WALI KELAS</u>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div style="padding: 20px 10px; width: 98%">
                                                        Jangan cepat menyerah dalam belajar, dan harus percaya diri
                                                        sendiri bahwa kamu mampu!
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-top: 12px; page-break-after: always">
        <tr>
            <td colspan="2">
                <table style="width: 100%; margin-top: 12px;">
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">KETERANGAN DIRI
                            TENTANG
                            PESERTA DIDIK</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">
                            {{ strtoupper($sekolah['nama']) ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">TAHUN PELAJARAN
                            2021/2022</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px"></td>
                    </tr>
                    <tr>
                        <td width="3%">1.</td>
                        <td width="40%">Nama Peserta Didik (Lengkap)</td>
                        <td width="2%">:</td>
                        <td width="55%">
                            Bagus Bekti Pradeka
                        </td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>NIS / NISN</td>
                        <td>:</td>
                        <td>2133323 / 232134324
                        </td>
                    </tr>

                    <tr>
                        <td>3.</td>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>:</td>
                        <td>Magelang, 27 Maret 2000
                        </td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>Laki - laki</td>
                    </tr>
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tr class="gantiAuto">
                        <td>5.</td>
                        <td>Agama</td>
                        <td>:</td>
                        <td>Islam
                        </td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Status dalam Keluarga</td>
                        <td>:</td>
                        <td>Anak Kandung
                        </td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Anak ke</td>
                        <td>:</td>
                        <td>3 (Tiga)
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">8.</td>
                        <td style="vertical-align: top">Alamat Peserta Didik</td>
                        <td style="vertical-align: top">:</td>
                        <td>
                            Jl Pahlawan No 21 Jakarta Utara
                            <br>
                            Telepon : -
                        </td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Diterima di Sekolah ini</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Di Kelas</td>
                        <td>:</td>
                        <td>X MOA
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Pada Tanggal</td>
                        <td>:</td>
                        <td>20 April 2021
                        </td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Sekolah Asal</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Nama Sekolah</td>
                        <td>:</td>
                        <td>SMP NEGERI 1 MAGELANG</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Alamat Sekolah</td>
                        <td>:</td>
                        <td>Jl. Gajah Mada No 21
                        </td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td>Surat Tanda Lulus</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Tahun</td>
                        <td>:</td>
                        <td>2021
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Nomor</td>
                        <td>:</td>
                        <td>1234.42323.2123
                        </td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>Nama Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>Adli Pratama
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>Prima Oktavia
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">13.</td>
                        <td style="vertical-align: top">Alamat Orang Tua</td>
                        <td style="vertical-align: top">:</td>
                        <td>
                            Jl Pahlawan No 12 Jakarta Utara
                            <br>
                            Telepon : 012321332434
                        </td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Pekerjaan Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>Wiraswasta
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>Pegawai Negri Sipil
                        </td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Nama Wali</td>
                        <td>:</td>
                        <td>Denanda Nuky Prakoso
                        </td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Alamat Wali</td>
                        <td>:</td>
                        <td>
                            Jl. Sersan Aswan No. 3 Bekasi
                            <br>
                            Telepon : 0856453213233
                        </td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>Pekerjaan Wali</td>
                        <td>:</td>
                        <td>Petani
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 24px"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> <br>
                            <div style=" width: 3cm; height: 3.7cm; border: solid 1px #000">
                        </td>
                        <td></td>
                        <td> <br>
                            <div>
                                Kabupaten {{ $sekolah['kabupaten'] }},
                                {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}
                                <br>
                                Kepala Sekolah,
                                <br><br><br><br><br>
                                <b><u>Muharom, S.Pd.</u></b><br>
                                NIP.
                                121322434
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
