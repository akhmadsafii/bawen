<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th colspan="6">
                                <h3 class="text-center"
                                    style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
                                    LAPORAN
                                    HASIL PENILAIAN AKHIR SEMESTER</h3>
                            </th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 30px" class="vertical-top">
                                {{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top">VII</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">
                                {{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">VIRDA AGUSTINA
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">2021-2022</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">212207001 / 81469014</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">A.</th>
                            <th class="text-left" style="width: 150px">Sikap</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" colspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                Deskripsi</td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0">Teruslah belajar dan tingkatkan semangat belajarmu</p>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td style="height: 10px" colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                No
                            </th>
                            <th class="text-center" rowspan="2" style="border: solid 2px black; padding: 3px;">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;"
                                colspan="4">
                                Pengetahuan
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;"
                                colspan="4">
                                Keterampilan</th>
                        </tr>
                        <tr>
                            <th style="border: solid 2px black; padding: 3px;">
                                KKM</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Angka</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                KKM</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Angka</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">1</td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                                dan Budi Pekerti</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">2</td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Pancasila
                                dan Kewarganegaraan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">3</td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">4</td>
                            <td style="border: solid 2px black; padding: 3px;">Matematika (Wajib)</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">5</td>
                            <td style="border: solid 2px black; padding: 3px;">Sejarah Indonesia</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">6</td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris (Wajib)</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">7</td>
                            <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">8</td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani,
                                Olahraga dan Kesehatan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">9</td>
                            <td style="border: solid 2px black; padding: 3px;">Prakarya dan
                                Kewirausahaan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">10</td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">11</td>
                            <td style="border: solid 2px black; padding: 3px;">Biologi</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">68</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Belum Terlampaui
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">12</td>
                            <td style="border: solid 2px black; padding: 3px;">Fisika</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">36</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Belum Terlampaui
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">13</td>
                            <td style="border: solid 2px black; padding: 3px;">Kimia</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">14</td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Mandarin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">15</td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris (Minat)</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">16</td>
                            <td style="border: solid 2px black; padding: 3px;">Matematika (Minat)</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">97</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">A</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Terlampaui</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border: solid 2px black; padding: 3px;">Jumlah Nilai</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">1120</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">1318</td>
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border: solid 2px black; padding: 3px;">Rata-Rata</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">83</td>
                            <td colspan="6"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th style="width: 25%"></th>
                            <th class="text-center">Mengetahui</th>
                            <th class="text-center" style="width: 30%">Kabupaten {{ $sekolah['kabupaten'] }},
                                {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</th>
                        </tr>
                        <tr>
                            <th>
                                <div>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Kepala {{ strtoupper($sekolah['nama']) ?? '-' }}</p>
                                    <br><br><br><br>
                                    Muharrom, S.Pd.
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    Nailil Fitri, S.Pd.
                                </div>
                            </th>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
