<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th colspan="6">
                                <h3 class="text-center"
                                    style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
                                    LAPORAN
                                    HASIL PENILAIAN AKHIR SEMESTER</h3>
                            </th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 30px" class="vertical-top">
                                {{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top">VII</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">
                                {{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">VIRDA AGUSTINA
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">2021-2022</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">212207001 / 81469014</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">A.</th>
                            <th class="text-left" style="width: 150px"></th>
                        </tr>
                        <tr>
                            <th class="text-left">1.</th>
                            <th class="text-left">Sikap Spiritual</th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="2" style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" colspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                Baik</td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0">Selalu bersyukur, selalu berdoa sebelum memulai dan mengakhiri
                                    kegiatan dan
                                    toleran terhadap pemeluk agama ; ketaatan beribadah sudah berkembang</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 10px"></td>
                        </tr>
                        <tr>
                            <th class="text-left">2.</th>
                            <th class="text-left" colspan="2">Sikap Sosial</th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="2" style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" colspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                Baik</td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0">Selalu menunjukkan sikap jujur, disiplin dan tanggung jawab
                                    dengan
                                    sangat
                                    baik, sikap
                                    toleransi dan gotong royong dengan baik,. </p>
                            </td>
                        </tr>
                    </table>
                    <div class="pagebreak"></div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">B.</th>
                <th class="text-left" colspan="4">PENGETAHUAN</th>
            </tr>
            <tr>
                <th class="text-left"></th>
                <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px; width: 25%">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 7%">
                    Nilai
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 7px">
                    Predikat</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Deskripsi</th>
            </tr>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok A</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                    dan Budi Pekerti</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan
                    Pancasila dan
                    Kewarganegaraan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                </td>
                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Alam</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    83
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Menganalisis
                        konsep energi, berbagai
                        sumber energi, cukup kemampuan dalam Menerapkan konsep pengukuran
                        berbagai besaran
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                </td>
                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Sosial</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    81
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        memahami
                        konsep
                        interaksi sosial, perlu
                        dimaksimalkan kemampuan dalam memahami pengaruh interaksi sosial
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Mengidentifikasi
                        unsur kebahasaan terkait
                        simple present dan Mengidentifikasi unsur yang terkandung dalam teks Descriptive
                    </p>
                </td>
            </tr>
        </tbody>

        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok B</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani,
                    Olahraga dan Kesehatan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Prakarya dan
                    Kewirausahaan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok C</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika (Minat)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Biologi</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Fisika</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Kimia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Pilihan Lintas Kelompok Peminatan</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris (Minat)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Mandarin</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">C.</th>
                <th class="text-left" colspan="4">Keterampilan</th>
            </tr>
            <tr>
                <th class="text-left"></th>
                <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px; width: 25%">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 7%">
                    Nilai
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 7px">
                    Predikat</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Deskripsi</th>
            </tr>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok A</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama
                    dan Budi Pekerti</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan
                    Pancasila dan
                    Kewarganegaraan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                </td>
                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Alam</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    83
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Menganalisis
                        konsep energi, berbagai
                        sumber energi, cukup kemampuan dalam Menerapkan konsep pengukuran
                        berbagai besaran
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                </td>
                <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Sosial</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    81
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">C
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        memahami
                        konsep
                        interaksi sosial, perlu
                        dimaksimalkan kemampuan dalam memahami pengaruh interaksi sosial
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Mengidentifikasi
                        unsur kebahasaan terkait
                        simple present dan Mengidentifikasi unsur yang terkandung dalam teks Descriptive
                    </p>
                </td>
            </tr>
        </tbody>

        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok B</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Seni Budaya</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani,
                    Olahraga dan Kesehatan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Prakarya dan
                    Kewirausahaan</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Sunda</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Kelompok C</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Matematika (Minat)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Biologi</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    3
                </td>
                <td style="border: solid 2px black; padding: 3px;">Fisika</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    84
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan baik dalam
                        Mengidentifikasi
                        teks
                        prosedur, cukup kemampuan
                        dalam Mengidentifikasi informasi teks deskripsi
                    </p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    4
                </td>
                <td style="border: solid 2px black; padding: 3px;">Kimia</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    88
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        menjelaskan
                        bentuk aljabar dan melakukan
                        operasi bentuk aljabar, cukup kemampuan dalam Menjelaskan transformasi
                        geometri dalam masalah kontekstual
                    </p>
                </td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                    Pilihan Lintas Kelompok Peminatan</td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    1
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris (Minat)</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    92
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        bahwa
                        Allah menyelamatkan
                        manusia melalui Yesus Kristus. dan memahami Yesus Kristus datang ke dunia
                        sebagai penebus manusia</p>
                </td>
            </tr>
            <tr>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    2
                </td>
                <td style="border: solid 2px black; padding: 3px;">Bahasa Mandarin</td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    89
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    B
                </td>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Memiliki kemampuan sangat baik
                        dalam
                        Memahami
                        norma
                        yang berlaku dalam
                        masyarakat, baik kemampuan dalam menganalisis proses perumusan Pancasila
                        sebagai dasar Negara
                    </p>
                </td>
            </tr>
            <tr>
                <td style="height: 10px" colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5" class="text-center"><b>Tabel Interval Predikat</b></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table style=" margin-left:15%; margin-right:15%; width: 70%; border-collapse: collapse">
                        <tbody>
                            <tr>
                                <th class="text-center vertical-middle" colspan="4"
                                    style="border: solid 2px black; padding: 3px;">
                                    Predikat
                                </th>
                            </tr>

                            <tr>
                                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    D
                                </th>
                                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    C
                                </th>
                                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    B
                                </th>
                                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    A
                                </th>
                            </tr>
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    < 70 </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    70 <= N <=80 </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    81 <= N <=90 </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    91 <= N <=100 </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div class="pagebreak"></div>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="3">EKSTRA KURIKULER</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 200px">
                                Kegiatan Ekstrakulikuler</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 70px">
                                Predikat</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td style="border: solid 2px black; padding: 3px;">Pramuka</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                2</td>
                            <td style="border: solid 2px black; padding: 3px;">Drum Band</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                3</td>
                            <td style="border: solid 2px black; padding: 3px;">KPA</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                4</td>
                            <td style="border: solid 2px black; padding: 3px;">Choir</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                5</td>
                            <td style="border: solid 2px black; padding: 3px;">English Club</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 10px"></td>
                        </tr>
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="3">PRESTASI</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" colspan="2"
                                style="border: solid 2px black; padding: 3px; width: 200px">
                                Jenis Kegiatan</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                2</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                3</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">F.</th>
                <th class="text-left" colspan="3">Ketidak hadiran</th>
                <td class="text-left vertical-top;" rowspan="3">
                    <div style="border: solid 2px black; margin-right: 20px; margin-left: 20px; padding: 5px">
                        <b>Keputusan :</b>
                        <p class="m-0">Berdasarkan pencapaian seluruh kompetensi, Peserta
                            didik dinyatakan ;</p>
                        <p>Naik / Tinggal * Ke kelas ............ (.........) </p>
                        <small>*) Coret yang tidak perlu </small>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">
                    <table class="table">
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Sakit</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">Hari
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Izin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Tanpa Keterangan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">0
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">G.</th>
                <th class="text-left" colspan="4">Catatan Wali Kelas</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;" colspan="5">
                    <div style="min-height: 50px;">
                        <p class="m-0 text-center" style=" line-height:35px;">Jangan cepat menyerah dalam belajar, dan
                            harus percaya diri sendiri
                            bahwa kamu mampu!</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div class="pagebreak"></div>
                </td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">H.</th>
                <th class="text-left" colspan="4">Tanggapan Orang tua/Wali</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;" colspan="5">
                    <div style="min-height: 50px;">
                        <p class="m-0 text-center" style="line-height:35px;"></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th style="width: 25%"></th>
                            <th class="text-center">Mengetahui</th>
                            <th class="text-center" style="width: 30%">Kabupaten {{ $sekolah['kabupaten'] }},
                                {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</th>
                        </tr>
                        <tr>
                            <th>
                                <div>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Kepala {{ strtoupper($sekolah['nama']) ?? '-' }}</p>
                                    <br><br><br><br>
                                    Muharrom, S.Pd.
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    Nailil Fitri, S.Pd.
                                </div>
                            </th>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
