<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table>
        <tr>
            <td>
                <table class="table" style="border: solid 2px black;">
                    <thead>
                        <tr>
                            <th colspan="6"
                                style="border: solid 2px black; padding: 3px; background-color: rgb(228, 227, 227);">
                                RAPOR PESERTA DIDIK DAN PROFIL PESERTA DIDIK</th>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="padding: 3px; width: 130px">Nama Peserta Didik</td>
                            <td style="width: 10px; padding: 3px;" class="vertical-top">:</td>
                            <td style="width: 250px; padding: 3px; padding-right: 30px" class="vertical-top">VIRDA
                                AGUSTINA
                            </td>
                            <td class="vertical-top" style="padding: 3px;">Kelas</td>
                            <td style="width: 10px; padding: 3px;" class="vertical-top">:</td>
                            <td class="vertical-top" style="padding: 3px;">VII B</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="padding: 3px;">NISN/NIS</td>
                            <td class="vertical-top" style="padding: 3px;">:</td>
                            <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                                424242525 / 41251523523
                            </td>
                            <td class="vertical-top" style="padding: 3px;">Semester</td>
                            <td class="vertical-top" style="padding: 3px;">:</td>
                            <td class="vertical-top" style="padding: 3px;">1 (Satu)</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="padding: 3px;">Nama Sekolah</td>
                            <td class="vertical-top" style="padding: 3px;">:</td>
                            <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                                {{ strtoupper($sekolah['nama']) ?? '-' }}
                            </td>
                            <td class="vertical-top" style="padding: 3px;">Tahun Pelajaran</td>
                            <td class="vertical-top" style="padding: 3px;">:</td>
                            <td class="vertical-top" style="padding: 3px;">2021-2022</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="padding: 3px;">Alamat Sekolah</td>
                            <td class="vertical-top" style="padding: 3px;">:</td>
                            <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                                {{ strtoupper($sekolah['alamat']) ?? '-' }}
                            </td>
                            <td class="vertical-top" colspan="3"></td>
                        </tr>
                    </thead>
                </table>
                <table class="table">
                    <tr>
                        <th class="text-left" style="width: 20px">A.</th>
                        <th class="text-left" style="width: 150px">SIKAP</th>
                    </tr>
                    <tr>
                        <th class="text-center" colspan="3"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Deskripsi</th>
                    </tr>
                    <tr>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            1
                        </td>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            <p class="m-0" style="font-size: 13px">Sikap Spiritual</p>
                        </td>
                        <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                            <p class="m-0" style="font-size: 13px">0 sangat baik dalam ketaatan beribadah,
                                berdo'a sebelum dan sesudah melakukan kegiatan, toleransi, bersyukur, </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            2
                        </td>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            <p class="m-0" style="font-size: 13px">Sikap Sosial</p>
                        </td>
                        <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                            <p class="m-0" style="font-size: 13px">0 sangat baik dalam sikap jujur, santun,
                                peduli, sudah baik dalam sikap disiplin, tanggung jawab, percaya diri.</p>
                        </td>
                    </tr>


                </table>
                <br>
                <table class="table">
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="7">PENGETAHUAN DAN KETRAMPILAN</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="7">KKM Satuan Pendidikan= 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                                No
                            </th>
                            <th class="text-center" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-left: none; padding:3px; width: 140px">
                                Muatan Pelajaran</th>
                            <th class="text-center vertical-middle" colspan="3"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Pengetahuan
                            </th>
                            <th class="text-center vertical-middle" colspan="3"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Ketrampilan</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 50px">
                                Nilai
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding:3px; width: 50px">
                                Predikat</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Deskripsi
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 50px">
                                Nilai
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding:3px; width: 50px">
                                Predikat</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Deskripsi
                            </th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pendidikan Agama dan
                                    Budi Pekerti.</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami
                                    kemahakuasaan Allah dalam berbagai
                                    peristiwa rantai kehidupan manusia
                                    disekitarnya, dan sangat baik dalam
                                    memahami keterbatasannya sebagai
                                    manusia</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menyajikan contoh
                                    sederhana yang berkaitan dengan
                                    prilaku bersyukur dalam berbagai
                                    peristiwa kehidupan, dan sangat baik
                                    dalam membuat karya yang
                                    mengekspresikan keterbatasannya
                                    sebagai manusia</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pend. Pancasila dan
                                    Kewarganegaraan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Bahasa Indonesia</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Matematika</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Ilmu Pengetahuan Alam</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Ilmu Pengetahuan Sosial</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Seni Budaya dan
                                    Prakarya</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">8
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Pend. Jasmani, Olah
                                    Raga, dan Kesehatan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam memahami makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila, dan sangat baik dalam
                                    mengidentifikasi berbagai bentuk
                                    keberagaman suku bangsa, sosial, dan
                                    budaya di Indonesia yang terikat
                                    persatuan dan kesatuan</p>
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">B
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">0 sangat baik dalam menjelaskan makna
                                    hubungan simbol dengan sila-sila
                                    Pancasila sebagai satu kesatuan dalam
                                    kehidupan sehari-hari, dan sangat baik
                                    dalam menyajikan hasil identifikasi
                                    pelaksanaan kewajiban dan hak sebagai
                                    warga masyarakat dalam kehidupan
                                    sehari-hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="height: 20px">

                            </td>
                        </tr>
                    </tbody>
                    <thead>

                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>

                </table>

            </td>
        </tr>
    </table>
    <div class="pagebreak"></div>
    <table>
        <tr>
            <th class="text-left" style="width: 20px;">C.</th>
            <th class="text-left">Ekstrakurikuler</th>
        </tr>
    </table>
    <table class="table" style="width: 100%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">Kegiatan
                    Ekstrakurikuler
                </th>
                <th class="text-center"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Keterangan</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Pramuka</p>
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Hafal dan mengerti isi Dasadharma dan Trisatya
                    </p>
                </td>

            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">English</p>
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    <p class="m-0" style="font-size: 12px">Dapat menguasai vocabulary dan tata bahasa dengan
                        baik dan benar</p>
                </td>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 8px;">
                </td>
            </tr>
        </tbody>
        <thead>
        </thead>

    </table>
    <br>
    <table class="table">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">D.</th>
                <th class="text-left" colspan="2">Saran-saran</th>
            </tr>
            <tr>
                <td class="text-left vertical-middle" colspan="3" style="border: solid 2px black; padding: 3px;">
                    <div style="width: 100%; min-height: 60px">
                        <p class="m-0">Pertahankan prestasi ananda!
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table" style="width: 100%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">E.</th>
                <th class="text-left" colspan="2">Perkembangan Fisik</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" rowspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                    No
                </th>
                <th class="text-center vertical-middle" rowspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Aspek Yang Dinilai
                </th>
                <th class="text-center" colspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Semester</th>
            </tr>
            <tr>
                <th class="text-center"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">1</th>
                <th class="text-center"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">2</th>

            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    1
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Tinggi Badan
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    142 cm
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    - cm
                </td>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    2
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Berat Badan
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    36 kg
                </td>
                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    - kg
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table" style="width: 100%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">F.</th>
                <th class="text-left" colspan="2">Kondisi Kesehatan</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                    No
                </th>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Aspek Fisik
                </th>
                <th class="text-center"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Keterangan</th>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    1
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Pendengaran
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Baik
                </td>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    2
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Penglihatan
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Baik
                </td>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    3
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Gigi
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Baik
                </td>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    4
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Lainnya
                </td>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                    -
                </td>
            </tr>

        </tbody>
    </table>
    <br>
    <table class="table" style="width: 100%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">G.</th>
                <th class="text-left" colspan="2">Catatan Prestasi</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" rowspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                    No
                </th>
                <th class="text-center vertical-middle" colspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Semester 1
                </th>
                <th class="text-center vertical-middle" colspan="2"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Semester 2
                </th>
            </tr>
            <tr>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Jenis Prestasi
                </th>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Keterangan
                </th>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Jenis Prestasi
                </th>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Keterangan
                </th>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    1
                </td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
            </tr>
            <tr>
                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                    2
                </td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
                <td style="border: solid 2px black; padding: 8px;"></td>
            </tr>


        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <th class="text-left" style="width: 20px">H.</th>
            <th class="text-left">Ketidakhadiran</th>
        </tr>
    </table>
    <table style="border-collapse: collapse;">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Ketidakhadiran
                </th>
                <th class="text-center vertical-middle"
                    style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                    Keterangan
                </th>
            </tr>
            <tr>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px; width: 228px">
                    Sakit
                </td>
                <td class="text-right vertical-middle" style="border: solid 2px black; padding: 3px;  width: 228px">
                    <p class="m-0" style="padding-right: 80px">Hari</p>
                </td>
            </tr>
            <tr>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px; width: 228px">
                    Izin
                </td>
                <td class="text-right vertical-middle" style="border: solid 2px black; padding: 3px;  width: 228px">
                    <p class="m-0" style="padding-right: 80px">Hari</p>
                </td>
            </tr>
            <tr>
                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px; width: 228px">
                    Tanpa Keterangan
                </td>
                <td class="text-right vertical-middle" style="border: solid 2px black; padding: 3px;  width: 228px">
                    <p class="m-0" style="padding-right: 80px">Hari</p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td style="width: 30%">
                    <div>
                        <p class="m-0">Mengetahui :</p>
                        <p class="m-0">Orang Tua/Wali</p>
                        <br><br><br><br>
                        ................................
                    </div>
                </td>
                <td style="width: 40%"></td>
                <td style="width: 40%">
                    <div style="float: right">
                        <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }}, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                        <p class="m-0">Wali Kelas,</p>
                        <br><br><br><br>
                        <b>Naillil Fitri, S.Pd</b>
                        <p class="m-0">NIP. -</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div>
                        <p class="m-0">Mengetahui :</p>
                        <p class="m-0">Kepala Sekolah</p>
                        <br><br><br><br>
                        <b>Muharrom, S.Pd., M.M</b>
                        <p class="m-0">NIP. -</p>
                    </div>

                </td>
                <td></td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
    </table>

</body>

</html>
