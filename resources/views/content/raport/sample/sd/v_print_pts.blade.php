<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <h2 class="text-center" style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">LAPORAN
        NILAI <br>ULANGAN TENGAH SEMESTER</h2>
    <table>
        <tr>
            <td>
                <table>
                    <thead>
                        <tr>
                            <td class="vertical-top">Nama Peserta Didik</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 200px" class="vertical-top">VIRDA AGUSTINA
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nomor Induk / NISN</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="" class="vertical-top">
                                42342424 / 42343242424
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="" class="vertical-top">VII B
                            </td>
                        </tr>

                    </thead>
                </table>
                <br>
                <table class="table">
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px"></th>
                            <th class="text-left" colspan="4">PENILAIAN KI.3 ( PENGETAHUAN )</th>
                        </tr>
                        <tr>
                            <td style="height: 5px" colspan="5"></td>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px;">No
                            </th>
                            <th class="text-center" rowspan="2" style="border: solid 2px black; padding: 3px;">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Skala
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 70px">
                                0 - 100</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 70px">
                                KKM 75</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">1
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Agama</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Pendidikan Agama</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">2
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Pancasila dan Kewarganegaraan
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Pendidikan Pancasila & Kewarganegaraan</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">3
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Indonesia</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Bahasa Indonesia</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">4
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Matematika</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Matematika</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">5
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Alam</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Ilmu Pengetahuan Alam</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">6
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Ilmu Pengetahuan Sosial
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Ilmu Pengetahuan Sosial</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">7
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Seni Budaya dan Prakarya</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Seni Budaya & Prakarya</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">8
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Pendidikan Jasmani, Olah Raga dan
                                Kesehatan</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Pendidikan Jasmani, Olah Raga dan
                                    Kesehatan</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">9
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Bahasa Inggris</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">D
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px">Kurang dalam memahami dan menerapkan
                                    pengetahuan
                                    faktual dan konsep Bahasa Inggris</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" colspan="2"
                                style="border: solid 2px black; padding: 3px;">Jumlah Nilai Prestrasi
                            </td>
                            <td style="border: solid 2px black; padding: 3px;" colspan="2"></td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;"
                                rowspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" colspan="2"
                                style="border: solid 2px black; padding: 3px;">Rata - rata Nilai Prestrasi
                            </td>
                            <td style="border: solid 2px black; padding: 3px;" colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">10
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">Teknologi Informasi dan Komunikasi</td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            </td>
                            <td style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 12px"></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;"
                                colspan="5">
                                <span><b>Catatan : </b></span><span><i> Konsentrasi dan semangat belajar
                                        ditingkatkan</i></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 20px">

                            </td>
                        </tr>
                    </tbody>
                    <thead>

                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>

                </table>
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 40%" class="text-center">
                                <div>
                                    <p class="m-0">MENGETAHUI</p>
                                    <p class="m-0">Kepala</p>
                                    <br><br><br><br>
                                </div>
                            </td>
                            <td style="width: 20%"></td>
                            <td style="width: 40%;" class="text-center">
                                <div style="float:right">
                                    <p class="m-0">Kabupaten {{ $sekolah['kabupaten'] }}, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>

                                </div>
                            </td>
                        </tr>

                    </tbody>
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
