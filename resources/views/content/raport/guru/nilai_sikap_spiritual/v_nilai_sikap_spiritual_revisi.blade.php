@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <a href="{{ URL::previous() }}" class="btn btn-info"><i class="fa fa-arrow-left"></i>
                                Kembali</a>
                            <a href="{{ route('raport-cetak_sikap_spiritual') }}"
                            class="btn btn-warning" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                            {{-- <a href="http://raport.mysch.web.id/n_sikap_sp/cetak/" class="btn btn-warning"
                                target="_blank"><i class="fa fa-print"></i> Cetak</a> --}}
                        </p>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Nilai Sikap Spiritual </h4>
                            </div>
                            <form action="javascript:void(0);" id="f_nilai_ssp">
                                @csrf
                                <div class="content">
                                    <table class="table table-condensed table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="15%">Nama</th>
                                                <th width="20%">NISN</th>
                                                <th width="40%">Selalu Dilakukan</th>
                                                <th width="20%">Mulai Meningkat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <input type="hidden" name="id_guru_mapel" value="">
                                            <input type="hidden" name="mode_form" value="{{ $form }}">
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($siswa as $sis)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $sis['nama'] }}</td>
                                                    <td>{{ $sis['nisn'] }}</td>
                                                    <td>
                                                        <input type="hidden" name="id_siswa_{{ $no }}"
                                                            value="{{ $sis['id_kelas_siswa'] }}">
                                                        <textarea name="ssp1_{{ $no }}"
                                                            id="ssp1_{{ $no }}" cols="30" rows="3"
                                                            class="form-control">{{ $sis['selalu'] }}</textarea>
                                                    </td>
                                                    <td>
                                                        <textarea name="ssp2_{{ $no }}"
                                                            id="ssp3_{{ $no }}" cols="30" rows="3"
                                                            class="form-control">{{ $sis['meningkat'] }}</textarea>
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        </tbody>

                                    </table>
                                    <input type="hidden" name="jumlah" value="{{ count($siswa) }}">
                                    <button type="submit" class="btn btn-success" id="tbsimpan"><i class="fa fa-check"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#f_nilai_ssp").on("submit", function() {
                var data = $(this).serialize();
                $.ajax({
                    type: 'POST',
                    url: "nilai-sikap_spr/simpan_manual",
                    data: data,
                    success: function(data) {
                        noti(data.icon, data.success);
                    }
                });
            });
        })
    </script>

@endsection
