<!DOCTYPE html>
<html>

<head>
    <title>Cetak Raport</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .pagebreak {
            page-break-before: always;
        }

        .text-center {
            text-align: center
        }

    </style>
</head>

<body>
    <table style="width: 100%">
        <tr>
            <td>
                <div class="sampul-awal">
                    <center>
                        <br><br><br>
                        <img src="{{ $sampul['file'] }}" alt="" height="150">
                        <h2 style="margin: 0">{!! $sampul['title'] !!}</h2>
                        <h2 style="margin: 0">{!! $sampul['sub_title'] !!}</h2>
                        <br><br><br>
                        <img src="{{ $sampul['file1'] }}" alt="" height="150">
                        <br>
                        <h3 style="margin-bottom: 3px"><b>Nama Peserta Didik</b></h3>
                        <div
                            style="border: 2px solid black; padding: 8px; margin-left:auto; margin-right: auto; width: 80%;">
                            <h3 style="margin: 0"><b>{{ strtoupper($siswa['nama']) }}</b></h3>
                        </div>
                        <br>
                        <h3 style="margin-bottom: 3px"><b>NISN/NIS</b></h3>
                        <div
                            style="border: 2px solid black; padding: 8px; margin-left:auto; margin-right: auto; width: 80%;">
                            <h3 style="margin: 0"><b>{{ $siswa['nisn'] ?? '-' }} / {{ $siswa['nis'] ?? '-' }}</b>
                            </h3>
                        </div>
                        <br><br><br><br><br><br><br><br>
                        <h2 style="margin: 0">{!! $sampul['footer'] !!}</h2>
                    </center>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="pagebreak"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <h3 class="text-center">PETUNJUK PENGISIAN</h3>
                    <div class="instruksi" style="text-align: justify">
                        {!! $sampul['instruksi'] !!}
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="pagebreak"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <h2 style="margin: 0" class="text-center">RAPOR SISWA</h2>
                    <h2 style="margin: 0" class="text-center">{!! $sampul['sub_title'] !!}</h2>
                </div>
                <div class="data-siswa" style="margin-top: 20px">
                    <table style="border-collapse: separate; border-spacing: 0 15px;">
                        <tr>
                            <td style="width: 170px">Nama Sekolah</td>
                            <td style="width: 20px">:</td>
                            <td>{{ strtoupper($sekolah['nama']) }}</td>
                        </tr>
                        <tr>
                            <td>NPSN</td>
                            <td>:</td>
                            <td>{{ $sekolah['npsn'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>NIS/NSS/NDS</td>
                            <td>:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">Alamat Sekolah</td>
                            <td style="vertical-align: top">:</td>
                            <td>{{ $sekolah['alamat'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>Kelurahan / Desa</td>
                            <td>:</td>
                            <td>{{ $sekolah['kelurahan'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>:</td>
                            <td>{{ $sekolah['kecamatan'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>Kota/Kabupaten</td>
                            <td>:</td>
                            <td>{{ $sekolah['kabupaten'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>:</td>
                            <td>{{ $sekolah['provinsi'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>Website</td>
                            <td>:</td>
                            <td>{{ $sekolah['website'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td>:</td>
                            <td>{{ $sekolah['email'] ?? '-' }}</td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <h3 class="text-center">KETERANGAN TENTANG DIRI SISWA</h3>
                    <div class="data-diri" style="text-align: justify">
                        <table style="border-collapse: separate; border-spacing: 0 9px;">
                            <tr>
                                <td style="width: 25px">1.</td>
                                <td>Nama Peserta Didik (Lengkap)</td>
                                <td style="width: 20px">:</td>
                                <td>{{ strtoupper($siswa['nama']) }}</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Nomor Induk/NISN</td>
                                <td>:</td>
                                <td>{{ $siswa['nis'] ?? '-' }}/{{ $siswa['nisn'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Tempat ,Tanggal Lahir</td>
                                <td>:</td>
                                <td>{{ ucwords($siswa['tempat_lahir']) }},
                                    {{ (new \App\Helpers\Help())->getTanggal($siswa['tgl_lahir']) }}</td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>{{ $siswa['jenkel'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Agama/Kepercayaan</td>
                                <td>:</td>
                                <td>{{ ucwords($siswa['agama']) ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Status dalam Keluarga</td>
                                <td>:</td>
                                <td>{{ $siswa['status_keluarga'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Anak ke</td>
                                <td>:</td>
                                <td>{{ $siswa['anak_ke'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">8.</td>
                                <td style="vertical-align: top">Alamat Peserta Didik</td>
                                <td style="vertical-align: top">:</td>
                                <td>{{ $siswa['alamat'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Nomor Telepon Rumah</td>
                                <td>:</td>
                                <td>{{ $siswa['telepon'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Sekolah Asal</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td>Diterima di sekolah ini</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Di kelas</td>
                                <td>:</td>
                                <td>{{ $siswa['kls_diterima'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Pada tanggal</td>
                                <td>:</td>
                                <td>{{ $siswa['tgl_diterima'] == null ? '-' : (new \App\Helpers\Help())->getTanggal($siswa['tgl_diterima']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td>Nama Orang Tua</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>a. Ayah</td>
                                <td>:</td>
                                <td>{{ strtoupper($siswa['nama_ayah']) ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>b. Ibu</td>
                                <td>:</td>
                                <td>{{ strtoupper($siswa['nama_ibu']) ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>13.</td>
                                <td>Alamat Orang Tua</td>
                                <td>:</td>
                                <td>{{ $siswa['alamat_wali'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Nomor Telepon Rumah</td>
                                <td>:</td>
                                <td>{{ $siswa['telp_wali'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>14.</td>
                                <td>Pekerjaan Orang Tua</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>a. Ayah</td>
                                <td>:</td>
                                <td>{{ $siswa['pekerjaan_ayah'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>b. Ibu</td>
                                <td>:</td>
                                <td>{{ $siswa['pekerjaan_ibu'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>15.</td>
                                <td>Nama Wali Peserta Didik</td>
                                <td>:</td>
                                <td>{{ strtoupper($siswa['nama_wali']) }}</td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td>Alamat Wali Peserta Didik</td>
                                <td>:</td>
                                <td>
                                    <div style="min-height: 25px;">
                                        <p class="m-0 text-justify">{{ $siswa['alamat_wali'] ?? '-' }}
                                        </p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Nomor Telepon Rumah</td>
                                <td>:</td>
                                <td>{{ $siswa['telp_wali'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td>Pekerjaan Wali Peserta Didik</td>
                                <td>:</td>
                                <td>{{ $siswa['pekerjaan_wali'] ?? '-' }}</td>
                            </tr>
                        </table>
                        <br>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%">
                                    <div
                                        style="display: inline; float: right; width: 3cm; height: 3.7cm; border: solid 1px #000; margin-right: 120px;">
                                </td>
                                <td>
                                    <div style="margin-left: 50px; display: inline; float: left;">
                                        {{ $config['kabupaten'] ?? 'Kota belum diset' }}, {{ (new \App\Helpers\Help())->getTanggal($config['tgl_raport']) }}
                                        <br>
                                        Kepala Sekolah<br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <b><u>{{ $config['kepsek'] }}</u></b><br>
                                        NIP. {{ $config['nip_kepsek'] }}
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="pagebreak"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <h3 class="text-center">KETERANGAN PINDAH SEKOLAH</h3>
                    <p>Nama Peserta Didik : .....................................</p>
                    <table style="width: 100%;  border-collapse: collapse;">
                        <tr>
                            <th colspan="4" style="border: solid 2px black;" class="text-center">KELUAR</th>
                        </tr>
                        <tr>
                            <th style="width: 15%; border: solid 2px black;">Tanggal</th>
                            <th style="width: 15%; border: solid 2px black;">Kelas yang ditinggalkan</th>
                            <th style="width: 30%; border: solid 2px black;">Sebab-sebab Keluar atau Atas
                                Permintaan(Tertulis)</th>
                            <th style="width: 40%; border: solid 2px black;">Tanda Tangan Kepala Sekolah,Stempel
                                Sekolah, dan Tanda Tangan Orang Tua/Wali</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black; padding: 10px">
                                <div>
                                    ............................., .............................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                                <div>
                                    ............................., .............................
                                    <br>
                                    Orang Tua/Wali<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black; padding: 10px">
                                <div>
                                    ............................., .............................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                                <div>
                                    ............................., .............................
                                    <br>
                                    Orang Tua/Wali<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black;"></td>
                            <td style="border: solid 2px black; padding: 10px">
                                <div>
                                    ............................., .............................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                                <div>
                                    ............................., .............................
                                    <br>
                                    Orang Tua/Wali<br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <h3 class="text-center">KETERANGAN PINDAH SEKOLAH</h3>
                    <p>Nama Peserta Didik : .....................................</p>
                    <table style="width: 100%;  border-collapse: collapse;">
                        <tr>
                            <th style="border: solid 2px black;">No</th>
                            <th style="border: solid 2px black;" colspan="3">Masuk</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 10px 0">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="text-center">1.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5.</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 0">
                                <table>
                                    <tr>
                                        <td>Nama Peserta Didik</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Induk</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Sekolah</td>
                                    </tr>
                                    <tr>
                                        <td>Masuk di Sekolah ini:</td>
                                    </tr>
                                    <tr>
                                        <td>a. Tanggal</td>
                                    </tr>
                                    <tr>
                                        <td>b. Di Kelas</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Pelajaran</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 10px">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 35%; vertical-align: middle; padding : 0 10px">
                                <div>
                                    ........................, ..........................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 10px 0">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="text-center">1.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5.</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 0">
                                <table>
                                    <tr>
                                        <td>Nama Peserta Didik</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Induk</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Sekolah</td>
                                    </tr>
                                    <tr>
                                        <td>Masuk di Sekolah ini:</td>
                                    </tr>
                                    <tr>
                                        <td>a. Tanggal</td>
                                    </tr>
                                    <tr>
                                        <td>b. Di Kelas</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Pelajaran</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 10px">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 35%; vertical-align: middle; padding : 0 10px">
                                <div>
                                    ........................, ..........................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 10px 0">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="text-center">1.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3.</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5.</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 0">
                                <table>
                                    <tr>
                                        <td>Nama Peserta Didik</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Induk</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Sekolah</td>
                                    </tr>
                                    <tr>
                                        <td>Masuk di Sekolah ini:</td>
                                    </tr>
                                    <tr>
                                        <td>a. Tanggal</td>
                                    </tr>
                                    <tr>
                                        <td>b. Di Kelas</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Pelajaran</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 30%; padding: 10px 10px">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: solid 1px black;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: solid 2px black; width: 35%; vertical-align: middle; padding : 0 10px">
                                <div>
                                    ........................, ..........................
                                    <br>
                                    Kepala Sekolah<br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>....................................................</u></b><br>
                                    NIP.
                                </div>
                            </td>
                        </tr>


                    </table>
                </div>
            </td>
        </tr>

    </table>
</body>

</html>
