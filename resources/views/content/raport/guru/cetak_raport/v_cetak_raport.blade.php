@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">



                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="box-title mr-b-0 text-white">Cetak Raport</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-inline float-right">
                                            <div class="form-group">
                                                <label for="inputPassword6">Tahun Ajaran</label>
                                                <select name="tahun" id="tahun" class="form-control mx-sm-3">
                                                    <option value="">Pilih Tahun Ajaran</option>
                                                    @foreach ($year as $yr)
                                                        <option value="{{ substr($yr['tahun_ajaran'], 0, 4) }}"
                                                            {{ substr($yr['tahun_ajaran'], 0, 4) == $_GET['tahun'] ? 'selected' : '' }}>
                                                            {{ $yr['tahun_ajaran'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                @if (session('role') == 'walikelas')
                                    <div class="col-md-12">
                                        @if (!empty($temp))
                                            <div class="alert alert-info" role="alert">
                                                <p class="mb-1">Berikut adalah settingan template dari rombel ini:
                                                </p>
                                                <table>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>{{ strtoupper($temp['jurusan']) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis</td>
                                                        <td>:</td>
                                                        <td>{{ strtoupper($temp['jenis']) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Template</td>
                                                        <td>:</td>
                                                        <td>{{ strtoupper($temp['template']) }}</td>
                                                    </tr>
                                                </table>

                                            </div>
                                        @else
                                            <div class="alert alert-danger" role="alert">
                                                <p class="mb-1">Admin Belum Memilih template untuk jurusan anda
                                                </p>
                                            </div>
                                        @endif

                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover" id="table-data">
                                        <thead>
                                            <tr>
                                                <th width="5%" rowspan="2">No</th>
                                                <th width="40%" rowspan="2">Profil</th>
                                                <th width="50%" class="text-center" colspan="{{ count($tahun) }}">TAHUN
                                                    AJARAN
                                                    @php
                                                        $next_th = $_GET['tahun'] + 1;
                                                    @endphp
                                                    {{ $_GET['tahun'] . '/' . $next_th }}</th>
                                            </tr>
                                            <tr>
                                                @foreach ($tahun as $th)
                                                    <th class="text-center">{{ $th['semester'] }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($siswa as $ssw)
                                                <tr>
                                                    <td class="vertical-middle">{{ $no }}</td>
                                                    <td class="vertical-middle"><b>{{ $ssw['nama'] }}</b>
                                                        <p class="m-0">NIS/NISN.
                                                            {{ $ssw['nis'] . '/' . $ssw['nisn'] }}</p>
                                                    </td>
                                                    @foreach ($tahun as $thn)
                                                        <td class="vertical-middle text-center">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <a href="{{ route('raport-cetak_sampul', ['ks' => (new \App\Helpers\Help())->encode($ssw['id']),'ta_sm' => (new \App\Helpers\Help())->encode($thn['id'])]) }}"
                                                                        target="_blank" class="text-info float-right"><i
                                                                            class="fas fa-file-contract fa-2x"></i><br>
                                                                        <i class="far fa-file-pdf"></i> Raport</a>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <a href="{{ route('raport-cetak_sampul_awal', ['ks' => (new \App\Helpers\Help())->encode($ssw['id']),'ta_sm' => (new \App\Helpers\Help())->encode($thn['id'])]) }}"
                                                                        target="_blank" class="text-success float-left"><i
                                                                            class="far fa-file-alt fa-2x"></i><br>
                                                                        <i class="far fa-file-pdf"></i> Sampul</a>
                                                                </div>
                                                            </div>


                                                        </td>
                                                    @endforeach
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $("#tahun").change(function() {
                var tahun = $(this).val();
                window.location.href = "cetak-raport?tahun=" + tahun;
            });
        })
    </script>
@endsection
