<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($lihat['sekolah']) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($lihat['alamat_sekolah']) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama Peserta Didik</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">
                                {{ strtoupper($lihat['nama']) }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['rombel'] }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Program Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">
                                {{ $lihat['jurusan'] }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">
                                {{ $lihat['angka_semester'] . ' (' . $lihat['semester'] . ')' }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">{{ $lihat['nisn'] }}
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['tahun_ajaran'] }}</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">A.</th>
                <th class="text-left" colspan="4">NILAI AKADEMIK</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px; width: 40%">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Pengetahuan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Ketrampilan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Nilai Akhir
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px; width: 15%">
                    Predikat</th>
            </tr>
            @if (!empty($lihat['nilai_mapel']))
                @foreach ($lihat['nilai_mapel'] as $mapel)
                    <tr>
                        <td colspan="6" style="border: solid 2px black; padding: 3px;">
                            Kelompok {{ $mapel['kelompok'] }}</td>
                    </tr>
                    @if (!empty($mapel['mapel']))
                        @php
                            $no_mp = 1;
                        @endphp
                        @foreach ($mapel['mapel'] as $mp)
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $no_mp++ }}
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">{{ $mp['mapel'] }}</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $mp['nilai_akhir_pengetahuan'] }}
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $mp['nilai_akhir_kerampilan'] }}
                                </td>
                                @php
                                    $nilai_akhir = ($mp['nilai_akhir_pengetahuan'] + $mp['nilai_akhir_kerampilan']) / 2;
                                @endphp
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $nilai_akhir }}
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ (new \App\Helpers\Help())->getPredikat($nilai_akhir) }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="6" class="text-center" style="border: solid 2px black; padding: 3px;">
                            Belum ada nilai yang terinput</td>
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="6" class="text-center" style="border: solid 2px black; padding: 3px;">
                        Belum ada nilai yang terinput</td>
                </tr>
            @endif
        </tbody>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">B.</th>
                <th class="text-left" colspan="5">CATATAN AKADEMIK</th>
            </tr>
            <tr>
                <td style="border: solid 2px black; padding: 3px;" colspan="6">
                    <div style="min-height: 50px;">
                        <p class="m-0 text-justify">{{ $lihat['nama'] }} : <br>
                            @if (!empty($lihat['catatan']))
                                {{ $lihat['catatan']['isi'] }}
                            @else
                                -
                            @endif
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>

        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">C.</th>
                            <th class="text-left" colspan="4">PRAKTIK KERJA LAPANGAN</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Mitra DU/DI</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Lokasi</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Lamanya</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                1</td>
                            <td style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="3">EKSTRA KURIKULER</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 200px">
                                Kegiatan Ekstrakulikuler</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 70px">
                                Predikat</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        @php
                            $no_ekstra = 1;
                        @endphp
                        @foreach ($lihat['nilai_ekstra'] as $ne)
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    {{ $no_ekstra++ }}</td>
                                <td style="border: solid 2px black; padding: 3px;">{{ $ne['nama'] }}</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                    {{ $ne['nilai'] != null ? $ne['nilai'] : '-' }}</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                    {{ $ne['deskripsi'] != null ? $ne['deskripsi'] : '-' }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="pagebreak"></div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">E.</th>
                <th class="text-left" colspan="5">KETIDAKHADIRAN</th>
            </tr>
            <tr>
                <td colspan="3">
                    <table class="table">
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Sakit</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">
                                {{ $lihat['nilai_absensi']['sakit'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">Hari
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Izin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $lihat['nilai_absensi']['izin'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Tanpa Keterangan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $lihat['nilai_absensi']['alpha'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                            {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                        </p>
                                        <p class="m-0">Wali Kelas,</p>
                                        @if (!empty($lihat['wali_kelas']['paraf']))
                                            <img src="{{ $lihat['wali_kelas']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                        <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        @if (!empty($lihat['raport_config']['paraf']))
                                            <img src="{{ $lihat['raport_config']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                        <p class="m-0">NIP.
                                            {{ $lihat['raport_config']['nip_kepala_sekolah'] ?? '-' }}</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="pagebreak"></div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="6" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">F.</th>
                            <th class="text-left" style="width: 100%">DESKRIPSI PERKEMBANGAN KARAKTER</th>
                        </tr>
                        <tr>
                            <th class="text-left">1.</th>
                            <th class="text-left">Sikap Spiritual</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="3">
                                <p class="m-0" style="min-height: 60px">
                                    @if ($lihat['nilai_spiritual']['selalu'] != null)
                                        Selalu melakukan sikap :
                                        {{ $lihat['nilai_spiritual']['selalu'] }} <br>
                                        Mulai meningkat pada sikap :
                                        {{ $lihat['nilai_spiritual']['meningkat'] }}
                                    @else
                                        Belum diinput
                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 10px"></td>
                        </tr>
                        <tr>
                            <th class="text-left">2.</th>
                            <th class="text-left" colspan="2">Sikap Sosial</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="3">
                                <p class="m-0" style="min-height: 60px">
                                    @if ($lihat['nilai_sosial']['selalu'] != null)
                                        @php
                                            $selalu = $lihat['nilai_sosial']['selalu'];
                                            $ruwet = [];
                                            foreach ($selalu as $sl) {
                                                $ruwet[] = $sl['nama'];
                                            }
                                            $so_text_selalu = implode(', ', $ruwet);
                                        @endphp
                                        Selalu melakukan sikap : {{ $so_text_selalu }}.
                                        Mulai meningkat pada sikap :
                                        {{ $lihat['nilai_sosial']['meningkat'] }};
                                    @else
                                        Belum diinput
                                    @endif
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                            {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                        </p>
                                        <p class="m-0">Wali Kelas,</p>
                                        @if (!empty($lihat['wali_kelas']['paraf']))
                                            <img src="{{ $lihat['wali_kelas']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                        <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        @if (!empty($lihat['raport_config']['paraf']))
                                            <img src="{{ $lihat['raport_config']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                        <p class="m-0">NIP.
                                            {{ $lihat['raport_config']['nip_kepala_sekolah'] ?? '-' }}</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>

    </table>
</body>

</html>
