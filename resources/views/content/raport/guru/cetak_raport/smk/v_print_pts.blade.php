<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="7">
                    <table>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($lihat['sekolah']) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" colspan="4">{{ strtoupper($lihat['alamat_sekolah']) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama Peserta Didik</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">
                                {{ strtoupper($lihat['nama']) }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['rombel'] }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">{{ $lihat['nisn'] }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">
                                {{ $lihat['angka_semester'] . ' (' . $lihat['semester'] . ')' }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Program Keahlian</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 250px; padding-right: 10px">
                                {{ $lihat['jurusan'] }}
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['tahun_ajaran'] }}</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="text-left" style="width: 20px">A.</th>
                <th class="text-left" colspan="4">PENGETAHUAN DAN KETERAMPILAN</th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    No
                </th>
                <th class="text-center" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    Mata
                    Pelajaran</th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;" rowspan="2">
                    KKM
                </th>
                <th class="text-center vertical-middle" colspan="3" style="border: solid 2px black; padding: 3px;">
                    Pengetahuan
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Ketrampilan
                </th>
            </tr>
            <tr>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Nilai Harian
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    PTS
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Rata - rata
                </th>
                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                    Rata - rata
                </th>
            </tr>
            @if (!empty($lihat['nilai_mapel']))
                @foreach ($lihat['nilai_mapel'] as $mapel)
                    <tr>
                        <td colspan="7" style="border: solid 2px black; padding: 3px;">
                            Kelompok {{ $mapel['kelompok'] }}</td>
                    </tr>
                    @if (!empty($mapel['mapel']))
                        @php
                            $no_mp = 1;
                        @endphp
                        @foreach ($mapel['mapel'] as $mp)
                            <tr>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $no_mp++ }}
                                </td>
                                <td style="border: solid 2px black; padding: 3px;">{{ $mp['mapel'] }}</td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $mp['kkm'] }}
                                </td>

                                @php
                                    $harian = 0;
                                    if (!empty($mp['nilai_pengetahuan'])) {
                                        foreach ($mp['nilai_pengetahuan'] as $np) {
                                            $harian += $np['nilai'];
                                        }
                                        $harian = $harian / count($mp['nilai_pengetahuan']);
                                    }
                                @endphp
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $harian }}
                                </td>
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $mp['hasil_pts'] }}
                                </td>
                                @php
                                    $rt_peng = ($harian + $mp['hasil_pts']) / 2;
                                @endphp
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $rt_peng }}
                                </td>
                                @php
                                    $ketrampilan = 0;
                                    if (!empty($mp['nilai_keterampilan'])) {
                                        foreach ($mp['nilai_keterampilan'] as $nk) {
                                            $ketrampilan += $nk['nilai'];
                                        }
                                        $ketrampilan = $ketrampilan / count($mp['nilai_keterampilan']);
                                    }
                                @endphp
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $ketrampilan }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center" style="border: solid 2px black; padding: 3px;">
                                Belum ada nilai yang terinput</td>
                        </tr>
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="7" class="text-center" style="border: solid 2px black; padding: 3px;">
                        Belum ada nilai yang terinput</td>
                </tr>
            @endif
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
        </tbody>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>

            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="3">PRESTASI</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" colspan="2"
                                style="border: solid 2px black; padding: 3px; width: 200px">
                                Jenis Kegiatan</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        @php
                            $max_pres = 3;
                        @endphp
                        @if (!empty($lihat['nilai_dt_prestasi']))

                            @if (count($lihat['nilai_dt_prestasi']) <= 3)
                                @php
                                    $no_npm = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $npm)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_npm }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                            colspan="2">{{ $npm['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $npm['keterangan'] }}</td>
                                    </tr>
                                    @php
                                        $no_npm++;
                                    @endphp
                                    @for ($i = $no_npm; $i <= 3; $i++)
                                        <tr>
                                            <td class="text-center vertical-top"
                                                style="border: solid 2px black; padding: 3px;">
                                                {{ $i }}</td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                                colspan="2">-
                                            </td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                            </td>
                                        </tr>
                                    @endfor
                                @endforeach
                            @else
                                @php
                                    $no_np = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $np)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_np++ }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                            colspan="2">{{ $np['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $np['keterangan'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @else
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    1</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    2</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    3</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </tbody>
        <thead></thead>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <th class="text-left" style="width: 20px">C.</th>
                <th class="text-left" colspan="6">KETIDAKHADIRAN</th>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="table">
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Sakit</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">
                                {{ $lihat['nilai_absensi']['sakit'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px; width: 20%">Hari
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Izin</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $lihat['nilai_absensi']['izin'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                Tanpa Keterangan</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $lihat['nilai_absensi']['alpha'] }}
                            </td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari</td>
                        </tr>

                    </table>
                </td>
            </tr>
        </tbody>
        <thead></thead>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="6">CATATAN WALI KELAS</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="7">
                                <div style="min-height: 50px;">
                                    <p class="m-0 text-justify">
                                        @if (!empty($lihat['catatan']))
                                            {{ $lihat['catatan']['isi'] }}
                                        @else
                                            -
                                        @endif
                                    </p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

        </tbody>
        <tbody>
            <tr>
                <td colspan="7" style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="7">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="6">TANGGAPAN ORANG TUA/WALI</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="7">
                                <div style="min-height: 70px;">
                                    <p class="m-0 text-justify"></p>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                            {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                        </p>
                                        <p class="m-0">Wali Kelas,</p>
                                        @if (!empty($lihat['wali_kelas']['paraf']))
                                            <img src="{{ $lihat['wali_kelas']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                        <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        @if (!empty($lihat['raport_config']['paraf']))
                                            <img src="{{ $lihat['raport_config']['paraf'] }}" alt="" height="150">
                                        @else
                                            <br><br><br><br>
                                        @endif
                                        <b>{{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                        <p class="m-0">NIP.
                                            {{ $lihat['raport_config']['nip_kepala_sekolah'] ?? '-' }}</p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
