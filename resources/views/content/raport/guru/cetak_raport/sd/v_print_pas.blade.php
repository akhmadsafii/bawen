<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%;">
        <tr>
            <td>
                <table class="table" style="border: solid 2px black;">
                    {{-- <thead> --}}
                    <tr>
                        <th colspan="6"
                            style="border: solid 2px black; padding: 3px; background-color: rgb(228, 227, 227);">
                            RAPOR PESERTA DIDIK DAN PROFIL PESERTA DIDIK</th>
                    </tr>
                    <tr>
                        <td class="vertical-top" style="padding: 3px; width: 130px">Nama Peserta Didik</td>
                        <td style="width: 10px; padding: 3px;" class="vertical-top">:</td>
                        <td style="padding: 3px; padding-right: 30px" class="vertical-top">
                            {{ strtoupper($lihat['nama']) ?? '-' }}
                        </td>
                        <td class="vertical-top" style="padding: 3px; width: 100px">Kelas</td>
                        <td style="width: 10px; padding: 3px;" class="vertical-top">:</td>
                        <td class="vertical-top" style="padding: 3px; width: 100px">
                            {{ (new \App\Helpers\Help())->getKelas($lihat['kelas']) }}</td>
                    </tr>
                    <tr>
                        <td class="vertical-top" style="padding: 3px;">NISN/NIS</td>
                        <td class="vertical-top" style="padding: 3px;">:</td>
                        <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                            {{ $lihat['nis'] ?? '-' }} / {{ $lihat['nisn'] ?? '-' }}
                        </td>
                        <td class="vertical-top" style="padding: 3px;">Semester</td>
                        <td class="vertical-top" style="padding: 3px;">:</td>
                        <td class="vertical-top" style="padding: 3px;">{{ $lihat['angka_semester'] }}
                            ({{ $lihat['semester'] }})</td>
                    </tr>
                    <tr>
                        <td class="vertical-top" style="padding: 3px;">Nama Sekolah</td>
                        <td class="vertical-top" style="padding: 3px;">:</td>
                        <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                            {{ strtoupper($lihat['sekolah']) }}
                        </td>
                        <td class="vertical-top" style="padding: 3px;">Tahun Pelajaran</td>
                        <td class="vertical-top" style="padding: 3px;">:</td>
                        <td class="vertical-top" style="padding: 3px;">
                            {{ str_replace('/', '-', $lihat['tahun_ajaran']) }}</td>
                    </tr>
                    <tr>
                        <td class="vertical-top" style="padding: 3px;">Alamat Sekolah</td>
                        <td class="vertical-top" style="padding: 3px;">:</td>
                        <td class="vertical-top" style="width: 250px; padding: 3px; padding-right: 30px">
                            {{ $lihat['alamat_sekolah'] ?? '-' }}
                        </td>
                        <td class="vertical-top" colspan="3"></td>
                    </tr>

                    {{-- </thead> --}}
                </table>
                <br>
                <table class="table">
                    <tr>
                        <th class="text-left" style="width: 20px">A.</th>
                        <th class="text-left" style="width: 150px">SIKAP</th>
                    </tr>
                    <tr>
                        <th class="text-center" colspan="3"
                            style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                            Deskripsi</th>
                    </tr>
                    <tr>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            1
                        </td>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            <p class="m-0" style="font-size: 13px">Sikap Spiritual</p>
                        </td>
                        <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                            <p class="m-0" style="font-size: 13px">
                                @if ($lihat['nilai_spiritual']['selalu'] != null)
                                    Selalu melakukan sikap :
                                    {{ $lihat['nilai_spiritual']['selalu'] }} <br>
                                    Mulai meningkat pada sikap :
                                    {{ $lihat['nilai_spiritual']['meningkat'] }}
                                @else
                                    Belum diinput
                                @endif
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            2
                        </td>
                        <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                            <p class="m-0" style="font-size: 13px">Sikap Sosial</p>
                        </td>
                        <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                            <p class="m-0" style="font-size: 13px">
                                @if ($lihat['nilai_sosial']['selalu'] != null)
                                    @php
                                        $selalu = $lihat['nilai_sosial']['selalu'];
                                        $ruwet = [];
                                        foreach ($selalu as $sl) {
                                            $ruwet[] = $sl['nama'];
                                        }
                                        $so_text_selalu = implode(', ', $ruwet);
                                    @endphp
                                    Selalu melakukan sikap : {{ $so_text_selalu }}.
                                    Mulai meningkat pada sikap :
                                    {{ $lihat['nilai_sosial']['meningkat'] }};
                                @else
                                    Belum diinput
                                @endif
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="7">PENGETAHUAN DAN KETRAMPILAN</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="7">KKM Satuan Pendidikan= 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                                No
                            </th>
                            <th class="text-center" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-left: none; padding:3px; width: 140px">
                                Muatan Pelajaran</th>
                            <th class="text-center vertical-middle" colspan="3"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Pengetahuan
                            </th>
                            <th class="text-center vertical-middle" colspan="3"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Ketrampilan</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 50px">
                                Nilai
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding:3px; width: 50px">
                                Predikat</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 140px">
                                Deskripsi
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 50px">
                                Nilai
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding:3px; width: 50px">
                                Predikat</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 140px">
                                Deskripsi
                            </th>
                        </tr>
                        @if (!empty($lihat['data_mapel']))
                            @php
                                $no_dm = 1;
                            @endphp
                            @foreach ($lihat['data_mapel'] as $dm)
                                <tr>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $no_dm++ }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 13px">{{ $dm['mapel'] }}</p>
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $dm['nilai_akhir_pengetahuan'] }}
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $dm['nilai_predikat_pengetahuan'] }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 12px">
                                            {{ $dm['deskripsi_pengetahuan'] }}</p>
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $dm['nilai_akhir_kerampilan'] }}
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $dm['predikat_kerampilan'] }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 12px">
                                            {{ $dm['deskripsi_keterampilan'] }}</p>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td style="border: solid 2px black; padding: 3px;" colspan="8" class="text-center">
                                Belum ada mapel yang tersedia
                            </td>
                        @endif
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <th class="text-left" style="width: 20px;">C.</th>
                        <th class="text-left">Ekstrakurikuler</th>
                    </tr>
                </table>
                <table class="table" style="width: 100%">

                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 35%">
                                Kegiatan
                                Ekstrakurikuler
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>

                        @foreach ($lihat['nilai_ekstra'] as $ne)
                            <tr>
                                <td style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">{{ $ne['nama'] }}</p>
                                </td>
                                <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    <p class="m-0" style="font-size: 12px">
                                        {{ $ne['deskripsi'] != null ? $ne['deskripsi'] : '-' }}
                                    </p>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="2" style="width: 700px">Saran-saran</th>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" colspan="3"
                                style="border: solid 2px black; padding: 3px; width: 100%">
                                <div style="width: 100%; min-height: 60px">
                                    <p class="m-0">
                                        {{ !empty($lihat['catatan']) ? $lihat['catatan']['isi'] : '-' }}
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table" style="width: 100%">

                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="2">Perkembangan Fisik</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                                No
                            </th>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Aspek Yang Dinilai
                            </th>
                            <th class="text-center" colspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Semester</th>
                        </tr>
                        <tr>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 120px">
                                1
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 120px">
                                2
                            </th>

                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                1
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Tinggi Badan
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                142 cm
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                - cm
                            </td>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                2
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Berat Badan
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                36 kg
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                - kg
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table" style="width: 100%">

                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">F.</th>
                            <th class="text-left" colspan="2">Kondisi Kesehatan</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                                No
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Aspek Fisik
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                1
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Pendengaran
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Baik
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                2
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Penglihatan
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Baik
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                3
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Gigi
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Baik
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                4
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Lainnya
                            </td>
                            <td class="text-left vertical-middle" style="border: solid 2px black; padding: 3px;">
                                -
                            </td>
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table" style="width: 100%">

                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">G.</th>
                            <th class="text-left" colspan="2">Catatan Prestasi</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; border-right: none; padding: 3px;">
                                No
                            </th>
                            <th class="text-center vertical-middle" colspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Semester 1
                            </th>
                            <th class="text-center vertical-middle" colspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Semester 2
                            </th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Jenis Prestasi
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Keterangan
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Jenis Prestasi
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Keterangan
                            </th>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                1
                            </td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                2
                            </td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                            <td style="border: solid 2px black; padding: 8px;"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <th class="text-left" style="width: 20px">H.</th>
                        <th class="text-left">Ketidakhadiran</th>
                    </tr>
                </table>
                <table style="border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Ketidakhadiran
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Keterangan
                            </th>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Sakit
                            </td>
                            <td class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                <p class="m-0">
                                    {{ $lihat['nilai_absensi']['sakit'] }} Hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Izin
                            </td>
                            <td class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                <p class="m-0">
                                    {{ $lihat['nilai_absensi']['izin'] }} Hari</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Tanpa Keterangan
                            </td>
                            <td class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                <p class="m-0">
                                    {{ $lihat['nilai_absensi']['alpha'] }} Hari</p>
                            </td>
                        </tr>
                    </tbody>

                </table>
                <br>
                <table style="width: 100%">

                    <tbody>
                        <tr>
                            <td style="width: 30%">
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </td>
                            <td style="width: 40%"></td>
                            <td style="width: 40%">
                                <div style="float: right">
                                    <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                        {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                    </p>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                    <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Kepala Sekolah</p>
                                    <br><br><br><br>
                                    <b>{{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                    <p class="m-0">NIP.
                                        {{ $lihat['raport_config']['nip_kepala_sekolah'] ?? '-' }}</p>
                                </div>

                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
