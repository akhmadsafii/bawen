<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            border: solid 2px;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 2px;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        td.fitwidth {
            width: 1px;
            white-space: nowrap;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .dontsplit {
            /* page-break-inside: avoid; */
            /* page-break-inside: auto; */
            page-break-before: always;
        }

        .gantiAuto {
            /* page-break-inside: avoid; */
            page-break-inside: auto;
            /* page-break-before: always; */
        }

    </style>

</head>

<body>
    <table>
        <thead>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td rowspan="5" style="vertical-align: middle; text-align: center; padding: 0">
                                @if ($kop['kop']['file'] != null)
                                    <img src="{{ $kop['kop']['file'] }}" style="max-height:138px; width: 128px">
                                @endif
                            </td>
                            <td style="text-align: center; width: 70%; padding: 0">
                                <b>{{ $kop['kop']['teks1'] }}</b>
                            </td>
                            <td rowspan=" 5" style="vertical-align: middle; text-align: center; padding: 0">
                                @if ($kop['kop']['file1'] != null)
                                    <img src="{{ $kop['kop']['file1'] }}" style="max-height:138px; width: 128px">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; padding: 0">
                                <small style="margin: 0px; padding: 0px"><b>{{ $kop['kop']['teks2'] }}</b></small>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; padding: 0">
                                <h2 style="margin: 0">
                                    <b>{{ $kop['kop']['teks3'] }}</b>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; padding: 0">
                                <b>{{ $kop['kop']['teks4'] }}</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; padding: 0">
                                <small style="white-space: pre-line">{{ $kop['kop']['teks5'] }}</small>
                            </td>
                        </tr>
                    </table>
                </td>
            <tr>
                <td>
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: center; font-weight: bold; font-size: 14pt"><u>RAPORT SEMESTER
                        {{ strtoupper($lihat['semester']) }} TAHUN
                        PELAJARAN {{ $lihat['tahun_ajaran'] }}</u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td width="20%">Nama Peserta Didik</td>
                                <td width="1%">:</td>
                                <td width="39%" class="tbl"> {{ strtoupper($lihat['nama']) }}</td>
                                <td width="20%">Kelas / Semester</td>
                                <td width="1%">:</td>
                                <td width="19%" class="tbl">
                                    {{ (new \App\Helpers\Help())->getKelas($lihat['kelas']) . ' / ' . $lihat['semester'] }}
                                </td>
                            </tr>
                            <tr>
                                <td>NIS</td>
                                <td>:</td>
                                <td> {{ strtoupper($lihat['nis']) }}</td>
                                <td>Tahun Pelajaran</td>
                                <td>:</td>
                                <td class="tbl">{{ $lihat['tahun_ajaran'] }}</td>
                            </tr>
                            <tr>
                                <td>NISN</td>
                                <td>:</td>
                                <td class="tbl">{{ $lihat['nisn'] }}</td>
                                <td>Program Keahlian</td>
                                <td>:</td>
                                <td class="tbl">{{ $lihat['jurusan'] }}</td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td colspan="4"><b>CAPAIAN KOMPETENSI</b></td>
                            </tr>
                            <tr>
                                <td colspan="6"><b>A. Sikap</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table style="margin-left: 15px; width: 100%">
                                        <tr>
                                            <td width="3%"><b>1.</b></td>
                                            <td width="97%"><b>Sikap Spiritual</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            @if ($lihat['nilai_spiritual']['selalu'] != null)
                                                <td style="border: solid 1px #000; padding: 10px">
                                                    Deskripsi :
                                                    Selalu melakukan sikap :
                                                    {{ $lihat['nilai_spiritual']['selalu'] }}
                                                    Mulai meningkat pada sikap :
                                                    {{ $lihat['nilai_spiritual']['meningkat'] }}
                                                </td>
                                            @else
                                                <td style="border: solid 1px #000; padding: 10px">
                                                    Belum diinput
                                                </td>
                                            @endif

                                        </tr>
                                        <tr>
                                            <td width="3%"><b>2.</b></td>
                                            <td width="97%"><b>Sikap Sosial</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            @if ($lihat['nilai_sosial']['selalu'] != null)
                                                @php
                                                    $selalu = $lihat['nilai_sosial']['selalu'];
                                                    $ruwet = [];
                                                    foreach ($selalu as $sl) {
                                                        $ruwet[] = $sl['nama'];
                                                    }
                                                    $so_text_selalu = implode(', ', $ruwet);
                                                @endphp
                                                <td style="border: solid 1px #000; padding: 10px">
                                                    Selalu melakukan sikap : {{ $so_text_selalu }}.
                                                    Mulai meningkat pada sikap :
                                                    {{ $lihat['nilai_sosial']['meningkat'] }};
                                                </td>
                                            @else
                                                <td style="border: solid 1px #000; padding: 10px">Belum diinput</td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>B. Pengetahuan dan Keterampilan</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%">No</th>
                                                <th style="width: 50%">Mata Pelajaran</th>
                                                <th style="width: 8%">Nilai Akhir</th>
                                                <th style="width: 37%">Capaian Pembelajaran</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($lihat['nilai_mapel'] as $nu)
                                                <tr>
                                                    <td class="center">{{ $no++ }}</td>
                                                    <td class="center">
                                                        {{ $nu['mapel'] != null ? $nu['mapel'] : '-' }}
                                                    </td>
                                                    <td style="text-align: center">
                                                        {{ $nu['nilai'] != null ? $nu['nilai'] : '-' }}
                                                    </td>
                                                    <td class="center">
                                                        {{ $nu['capaian_hasil'] != null ? $nu['capaian_hasil'] : '-' }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>C. Ekstrakurikuler</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="30%">Nama Kegiatan</th>
                                                <th width="10%">Nilai</th>
                                                <th width="55%">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($lihat['nilai_ekstra']))
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($lihat['nilai_ekstra'] as $ne)
                                                    @php
                                                        $desk = '';
                                                        if ($ne['nilai'] == 'A') {
                                                            $desk = 'Memuaskan, aktif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                                        } elseif ($ne['nilai'] == 'B') {
                                                            $desk = 'Cukup Memuaskan, aktif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                                        } elseif ($ne['nilai'] == 'C') {
                                                            $desk = 'Kurang Memuaskan, pasif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                                        } else {
                                                            $desk = '-';
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td class="ctr">{{ $no }}</td>
                                                        <td>{{ $ne['nama'] }}</td>
                                                        <td class="ctr">{{ $ne['nilai'] }}</td>
                                                        <td>{{ $desk }}</td>
                                                    </tr>
                                                @endforeach
                                                @php
                                                    $no++;
                                                @endphp
                                            @else
                                                <tr>
                                                    <td colspan="4">-</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td colspan="6"><br><b>D. Ketidakhadiran</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table width="100%" style="margin-left: 15px; width: 100%">
                                        <tr>
                                            <td width="40%">
                                                <table class="table" width="100%">
                                                    <tr>
                                                        <td width="60%">Sakit</td>
                                                        @if ($lihat['nilai_absensi'] != null)
                                                            <td width="40%" class="ctr">
                                                                {{ $lihat['nilai_absensi']['sakit'] }} &nbsp; hari
                                                            </td>
                                                        @else
                                                            <td width="40%" class="ctr">
                                                                Data belum diset
                                                            </td>
                                                        @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Izin</td>
                                                        @if ($lihat['nilai_absensi'] != null)
                                                            <td class="ctr">
                                                                {{ $lihat['nilai_absensi']['izin'] }}
                                                                &nbsp;
                                                                hari
                                                            </td>
                                                        @else
                                                            <td class="ctr">Data belum diset</td>
                                                        @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Tanpa Keterangan</td>
                                                        @if ($lihat['nilai_absensi'] != null)
                                                            <td class="ctr">
                                                                {{ $lihat['nilai_absensi']['alpha'] }}
                                                                &nbsp;
                                                                hari
                                                            </td>
                                                        @else
                                                            <td class="ctr">Data belum diset</td>
                                                        @endif
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="60%">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <div class="dontsplit"></div>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h3 style="text-align: center"><u>CATATAN PRESTASI YANG PERNAH DICAPAI</u></h3>
                    <table style="width: 100%; margin-top: 12px; border-collapse: collapse">
                        <tbody>
                            <tr>
                                <th colspan="2">
                                    <table>
                                        <tr>
                                            <th style="text-align: left">Nama Peserta Didik</th>
                                            <th>:</th>
                                            <th style="text-align: left">{{ strtoupper($lihat['nama']) }}</th>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left">NIS/NISN</th>
                                            <th>:</th>
                                            <th style="text-align: left">
                                                {{ $lihat['nis'] . ' / ' . $lihat['nisn'] }}</th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2" style="height: 10px"></th>
                            </tr>
                            <tr>
                                <th style="width: 5px !important; border: 2px solid; width: 20px">No</th>
                                <th style="border: 2px solid; width: 100%">Prestasi yang
                                    pernah dicapai</th>
                            </tr>
                            @php
                                $nomor = [];
                                for ($i = 1; $i <= 15; $i++) {
                                    $nomor[] = $i;
                                }
                                $final_array = [];
                                foreach ($nomor as $key => $val) {
                                    if ($key == null && empty($lihat['nilai_prestasi'][$key])) {
                                        $final_array[$key] = '-';
                                    } elseif ($key != null && empty($lihat['nilai_prestasi'][$key])) {
                                        $final_array[$key] = '-';
                                    } elseif ($key != null && !empty($lihat['nilai_prestasi'][$key])) {
                                        $final_array[$key] = $lihat['nilai_prestasi'][$key]['nama'];
                                    } else {
                                        $final_array[$key] = $lihat['nilai_prestasi'][$key]['nama'];
                                    }
                                }

                                $number = 1;
                            @endphp
                            @foreach ($final_array as $pres)
                                <tr>
                                    <td style="border: 2px solid">{{ $number++ }}</td>
                                    <td style="border: 2px solid;">
                                        <p style="padding: 2px; margin: 0">{{ $pres }}</p>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; margin-top: 12px">
                        <tbody>
                            <tr>
                                <td colspan="6" style="text-align: center; font-weight: bold; font-size: 14pt">
                                    <u>CATATAN WALI KELAS</u>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table" style="margin-left: 15px; width: 100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div style="padding: 20px 10px; width: 98%">
                                                        {{ !empty($lihat['catatan']) ? $lihat['catatan']['isi'] : '-' }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; margin-top: 12px; page-break-after: always">

                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-top: 12px; page-break-after: always">
        <tr>
            <td>
                <table style="width: 100%; margin-top: 12px; page-break-after: always">
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">KETERANGAN
                            DIRI TENTANG
                            PESERTA DIDIK</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">
                            {{ strtoupper($lihat['sekolah']) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14pt">TAHUN
                            PELAJARAN
                            {{ $lihat['tahun_ajaran'] }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px"></td>
                    </tr>
                    <tr>
                        <td width="3%">1.</td>
                        <td width="35%">Nama Peserta Didik (Lengkap)</td>
                        <td width="2%">:</td>
                        <td width="60%">
                            {{ $lihat['detail_siswa']['nama'] != null ? strtoupper($lihat['detail_siswa']['nama']) : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>NIS / NISN</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['nis'] != null? $lihat['detail_siswa']['nis'] . ' / ' . $lihat['detail_siswa']['nisn']: '-' }}
                        </td>
                    </tr>

                    <tr>
                        <td>3.</td>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['tempat_lahir'] .', ' .\Carbon\Carbon::parse($lihat['detail_siswa']['tgl_lahir'])->translatedFormat('j F, Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['jenkel'] }}</td>
                    </tr>
                    <thead>
                        <!-- biar bisa ganti lembar otomatis -->
                    </thead>
                    <tr class="gantiAuto">
                        <td>5.</td>
                        <td>Agama</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['agama'] != null ? $lihat['detail_siswa']['agama'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Status dalam Keluarga</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['status_keluarga'] != null ? $lihat['detail_siswa']['status_keluarga'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Anak ke</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['anak_ke'] != null? $lihat['detail_siswa']['anak_ke'] .'(' .(new \App\Helpers\Help())->nama_angka($detail['sisswa']['anak_ke']) .')': '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">8.</td>
                        <td style="vertical-align: top">Alamat Peserta Didik</td>
                        <td style="vertical-align: top">:</td>
                        <td>
                            {{ $lihat['detail_siswa']['alamat'] != null ? $lihat['detail_siswa']['alamat'] : '-' }}
                            <br>
                            {{ $lihat['detail_siswa']['telepon'] != null ? 'Telepon : ' . $lihat['detail_siswa']['telepon'] : 'Telepon : -' }}
                        </td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Diterima di Sekolah ini</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Di Kelas</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['kls_diterima'] != null ? $lihat['detail_siswa']['kls_diterima'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Pada Tanggal</td>
                        <td>:</td>
                        <td>{{ \Carbon\Carbon::parse($lihat['detail_siswa']['tgl_diterima'])->translatedFormat('j F, Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Sekolah Asal</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Nama Sekolah</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Alamat Sekolah</td>
                        <td>:</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td>Surat Tanda Lulus</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Tahun</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['th_ijazah'] != null ? $lihat['detail_siswa']['th_ijazah'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Nomor</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['no_ijazah'] != null ? $lihat['detail_siswa']['no_ijazah'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>Nama Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['nama_ayah'] != null ? $lihat['detail_siswa']['nama_ayah'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['nama_ibu'] != null ? $lihat['detail_siswa']['nama_ibu'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">13.</td>
                        <td style="vertical-align: top">Alamat Orang Tua</td>
                        <td style="vertical-align: top">:</td>
                        <td>
                            {{ $lihat['detail_siswa']['alamat_wali'] != null ? $lihat['detail_siswa']['alamat_wali'] : '-' }}
                            <br>
                            {{ $lihat['detail_siswa']['telp_wali'] != null? 'Telepon : ' . $lihat['detail_siswa']['telp_wali']: 'Telepon : -' }}
                        </td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Pekerjaan Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['pekerjaan_ayah'] != null ? $lihat['detail_siswa']['pekerjaan_ayah'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['pekerjaan_ibu'] != null ? $lihat['detail_siswa']['pekerjaan_ibu'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Nama Wali</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['nama_wali'] != null ? $lihat['detail_siswa']['nama_wali'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Alamat Wali</td>
                        <td>:</td>
                        <td>
                            {{ $lihat['detail_siswa']['alamat_wali'] != null ? $lihat['detail_siswa']['alamat_wali'] : '-' }}
                            <br>
                            {{ $lihat['detail_siswa']['telp_wali'] != null? 'Telepon : ' . $lihat['detail_siswa']['telp_wali']: 'Telepon : -' }}
                        </td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>Pekerjaan Wali</td>
                        <td>:</td>
                        <td>{{ $lihat['detail_siswa']['pekerjaan_wali'] != null ? $lihat['detail_siswa']['pekerjaan_wali'] : '-' }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 24px"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div style=" width: 3cm; height: 3.7cm; border: solid 1px #000">
                        </td>
                        <td></td>
                        <td>
                            <div>
                                {{ $lihat['kabupaten'] != null ? ucfirst($lihat['kabupaten']) : '-' }},
                                {{ $lihat['raport_config']['tgl_raport'] != null ? $lihat['raport_config']['tgl_raport'] : '-' }}
                                <br>
                                Kepala Sekolah, <br>
                                @if ($lihat['raport_config']['paraf'] != null)
                                    <img src="{{ $lihat['raport_config']['paraf'] }}" alt="" style="height: 68px">
                                    <br>
                                @else
                                    <br><br><br><br>
                                @endif
                                <b><u>{{ $lihat['raport_config']['kepala_sekolah'] != null ? $lihat['raport_config']['kepala_sekolah'] : '-' }}</u></b><br>
                                NIP.
                                {{ $lihat['raport_config']['nip_kepala_sekolah'] != null ? $lihat['raport_config']['nip_kepala_sekolah'] : '-' }}
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
