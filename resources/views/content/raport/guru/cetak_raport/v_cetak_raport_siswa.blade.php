@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">

                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="box-title mb-0">{{ session('title') }}</h5>
                            </div>
                            <div class="col-md-6">
                                <form class="form-inline float-right">
                                    <div class="form-group">
                                        <label for="inputPassword6">Tahun Ajaran</label>
                                        <select name="tahun" id="tahun" class="form-control mx-sm-3">
                                            <option value="">Pilih Tahun Ajaran</option>
                                            @foreach ($year as $yr)
                                                <option value="{{ substr($yr['tahun_ajaran'], 0, 4) }}"
                                                    {{ substr($yr['tahun_ajaran'], 0, 4) == $_GET['tahun'] ? 'selected' : '' }}>
                                                    {{ $yr['tahun_ajaran'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="alert alert-info" role="alert">
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <p class="m-0">Selamat Datang {{ session('username') }}</p>
                            <p class="m-0">Berikut adalah settingan raport anda</p>
                            <ul class="">
                                <li>Tahun Ajaran : {{ $temp['tahun_ajaran'] }}, Semester {{ $temp['semester'] }}
                                </li>
                                <li>Output Raport : Jenis {{ strtoupper($temp['jenis']) }}, Template
                                    {{ strtoupper($temp['template']) }}</li>
                            </ul>
                        </div>
                        <div class="row d-flex justify-content-center">
                            @foreach ($tahun as $th)
                                <div class="col-md-4">
                                    <div class="card border border border-white bg-default">
                                        <div class="card-body">
                                            <center>
                                                <i class="fas fa-file-contract fa-9x"></i> <br>
                                                <b class="">Tahun Ajaran {{ $th['tahun_ajaran'] }}</b>
                                                <p class="m-0">Semester {{ $th['semester'] }}</p>
                                                <a class="text-info"
                                                    href="{{ route('raport-cetak_sampul', ['ta_sm' => (new \App\Helpers\Help())->encode($th['id'])]) }}"
                                                    target="_blank"> &raquo; Lihat Raport</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $("#tahun").change(function() {
                var tahun = $(this).val();
                window.location.href = "cetak-raport?tahun=" + tahun;
            });
        })
    </script>
@endsection
