<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td>
                    <table>
                        <tr>
                            <th colspan="6">
                                <h3 class="text-center"
                                    style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
                                    LAPORAN
                                    HASIL PENILAIAN AKHIR SEMESTER</h3>
                            </th>
                        </tr>
                        <tr>
                            <td class="vertical-top" style="width: 100px">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 20px" class="vertical-top">
                                {{ strtoupper($lihat['sekolah']) }}
                            </td>
                            <td class="vertical-top" style="width: 110px">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 140px">{{ $lihat['rombel'] }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="padding-right: 30px">
                                {{ $lihat['alamat'] ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['angka_semester'] }} ({{ $lihat['semester'] }})
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="padding-right: 30px">
                                {{ strtoupper($lihat['nama']) }}
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ str_replace('/', '-', $lihat['tahun_ajaran']) }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['nis'] ?? '-' }} / {{ $lihat['nisn'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">A.</th>
                            <th class="text-left" style="width: 100%"></th>
                        </tr>
                        <tr>
                            <th class="text-left">1.</th>
                            <th class="text-left">Sikap Spiritual</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="3">
                                <p class="m-0" style="min-height: 60px">
                                    @if ($lihat['nilai_spiritual']['selalu'] != null)
                                        Selalu melakukan sikap :
                                        {{ $lihat['nilai_spiritual']['selalu'] }} <br>
                                        Mulai meningkat pada sikap :
                                        {{ $lihat['nilai_spiritual']['meningkat'] }}
                                    @else
                                        Belum diinput
                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 10px"></td>
                        </tr>
                        <tr>
                            <th class="text-left">2.</th>
                            <th class="text-left" colspan="2">Sikap Sosial</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="3">
                                <p class="m-0" style="min-height: 60px">
                                    @if ($lihat['nilai_sosial']['selalu'] != null)
                                        @php
                                            $selalu = $lihat['nilai_sosial']['selalu'];
                                            $ruwet = [];
                                            foreach ($selalu as $sl) {
                                                $ruwet[] = $sl['nama'];
                                            }
                                            $so_text_selalu = implode(', ', $ruwet);
                                        @endphp
                                        Selalu melakukan sikap : {{ $so_text_selalu }}.
                                        Mulai meningkat pada sikap :
                                        {{ $lihat['nilai_sosial']['meningkat'] }};
                                    @else
                                        Belum diinput
                                    @endif
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="4">PENGETAHUAN</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                No
                            </th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 7%">
                                Nilai
                            </th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 7px">
                                Predikat</th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        @foreach ($lihat['nilai_mapel'] as $nm)
                            <tr>
                                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                                    Kelompok {{ $nm['kelompok'] }}</td>
                            </tr>
                            @php
                                $no_mapel = 1;
                            @endphp
                            @foreach ($nm['mapel'] as $mapel)
                                <tr>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $no_mapel++ }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">{{ $mapel['mapel'] }}</td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $mapel['nilai_akhir_pengetahuan'] }}
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $mapel['nilai_predikat_pengetahuan'] }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 12px">
                                            {{ $mapel['deskripsi_pengetahuan'] }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">C.</th>
                            <th class="text-left" colspan="4">Keterampilan</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                No
                            </th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 7%">
                                Nilai
                            </th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 7px">
                                Predikat</th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        @foreach ($lihat['nilai_mapel'] as $nmk)
                            <tr>
                                <td colspan="5" style="border: solid 2px black; padding: 3px;">
                                    Kelompok {{ $nmk['kelompok'] }}</td>
                            </tr>
                            @php
                                $no_ketrampilan = 1;
                            @endphp
                            @foreach ($nmk['mapel'] as $ktr)
                                <tr>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $no_ketrampilan++ }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">{{ $ktr['mapel'] }}</td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $ktr['nilai_akhir_kerampilan'] }}
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $ktr['predikat_kerampilan'] }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 12px">
                                            {{ $ktr['deskripsi_keterampilan'] }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <td colspan="5" class="text-center"><b>Tabel Interval Predikat</b></td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <table
                                    style=" margin-left:10%; margin-right:10%; width: 80%; border-collapse: collapse">
                                    <tbody>
                                        <tr>
                                            <th class="text-center vertical-middle" colspan="{{ count($predikat) }}"
                                                style="border: solid 2px black; padding: 3px;">
                                                Predikat
                                            </th>
                                        </tr>

                                        <tr>
                                            @foreach ($predikat as $pr)
                                                <th class="text-center vertical-middle"
                                                    style="border: solid 2px black; padding: 3px;">
                                                    {{ $pr['predikat'] }}
                                                </th>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            @foreach ($predikat as $prd)
                                                <td class="text-center vertical-middle"
                                                    style="border: solid 2px black; padding: 3px;">
                                                    {{ $prd['skor1'] ?? '-' }} - {{ $prd['skor2'] ?? '-' }}</td>
                                            @endforeach
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="3">EKSTRA KURIKULER</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 200px">
                                Kegiatan Ekstrakulikuler</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px; width: 70px">
                                Predikat</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        @php
                            $no_ekstra = 1;
                        @endphp
                        @foreach ($lihat['nilai_ekstra'] as $ne)
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    {{ $no_ekstra++ }}</td>
                                <td style="border: solid 2px black; padding: 3px;">{{ $ne['nama'] }}</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                    {{ $ne['nilai'] != null ? $ne['nilai'] : '-' }}</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                    {{ $ne['deskripsi'] != null ? $ne['deskripsi'] : '-' }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="3">PRESTASI</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                No</th>
                            <th class="text-center" colspan="2"
                                style="border: solid 2px black; padding: 3px; width: 200px">
                                Jenis Kegiatan</th>
                            <th class="text-center" style="border: solid 2px black; padding: 3px;">
                                Keterangan</th>
                        </tr>
                        @php
                            $max_pres = 3;
                        @endphp
                        @if (!empty($lihat['nilai_dt_prestasi']))

                            @if (count($lihat['nilai_dt_prestasi']) <= 3)
                                @php
                                    $no_npm = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $npm)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_npm }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                            colspan="2">{{ $npm['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $npm['keterangan'] }}</td>
                                    </tr>
                                    @php
                                        $no_npm++;
                                    @endphp
                                    @for ($i = $no_npm; $i <= 3; $i++)
                                        <tr>
                                            <td class="text-center vertical-top"
                                                style="border: solid 2px black; padding: 3px;">
                                                {{ $i }}</td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                                colspan="2">-
                                            </td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                            </td>
                                        </tr>
                                    @endfor
                                @endforeach
                            @else
                                @php
                                    $no_np = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $np)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_np++ }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;"
                                            colspan="2">{{ $np['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $np['keterangan'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @else
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    1</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    2</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    3</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="2">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">F.</th>
                            <th class="text-left" colspan="3">Ketidak hadiran</th>
                            <td class="text-left vertical-top;" rowspan="3">
                                <div
                                    style="border: solid 2px black; margin-right: 20px; margin-left: 20px; padding: 5px">
                                    <b>Keputusan :</b>
                                    <p class="m-0">Berdasarkan pencapaian seluruh kompetensi, Peserta
                                        didik dinyatakan ;</p>
                                    @if (!empty($lihat['catatan']))
                                        <p>
                                            @if ($lihat['catatan']['naik_kelas'] == 'Y')
                                                @php
                                                    $kelas = $lihat['kelas'] != null ? $lihat['kelas'] + 1 : null;
                                                @endphp
                                                Naik / <s>Tinggal</s>
                                            @else
                                                @php
                                                    $kelas = $lihat['kelas'] != null ? $lihat['kelas'] : null;
                                                @endphp
                                                <s>Naik</s> / Tinggal
                                            @endif
                                            * Ke
                                            kelas {{ $kelas != null ? $kelas : '............' }}
                                            ({{ $kelas != null ? (new \App\Helpers\Help())->nama_angka($kelas) : '.........' }})
                                        </p>
                                    @else
                                        <p>Naik / Tinggal * Ke kelas ............ (.........) </p>
                                    @endif
                                    <small>*) Coret yang tidak perlu </small>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="4">
                                <table class="table">
                                    <tr>
                                        <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                            Sakit</td>
                                        <td class="text-center"
                                            style="border: solid 2px black; padding: 3px; width: 20%">
                                            {{ $lihat['nilai_absensi']['sakit'] }}
                                        </td>
                                        <td class="text-center"
                                            style="border: solid 2px black; padding: 3px; width: 20%">Hari
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                            Izin</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $lihat['nilai_absensi']['izin'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="vertical-top" style="border: solid 2px black; padding: 3px;">
                                            Tanpa Keterangan</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $lihat['nilai_absensi']['alpha'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">Hari
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">G.</th>
                            <th class="text-left" colspan="4" style="width: 700px">Catatan Wali Kelas</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px; width: 700px" colspan="5">
                                <div style="min-height: 50px;">
                                    <p class="m-0 text-center" style=" line-height:35px;">
                                        {{ !empty($lihat['catatan']) ? $lihat['catatan']['isi'] : '-' }}</p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">H.</th>
                            <th class="text-left" colspan="4">Tanggapan Orang tua/Wali</th>
                        </tr>
                        <tr>
                            <td style="border: solid 2px black; padding: 3px;" colspan="5">
                                <div style="min-height: 50px;">
                                    <p class="m-0 text-center" style="line-height:35px;"></p>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Orang Tua/Wali</p>
                                        <br><br><br><br>
                                        ................................
                                    </div>
                                </td>
                                <td style="width: 40%"></td>
                                <td style="width: 40%">
                                    <div>
                                        <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                            {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                        </p>
                                        <p class="m-0">Wali Kelas,</p>
                                        <br><br><br><br>
                                        <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div>
                                        <p class="m-0">Mengetahui :</p>
                                        <p class="m-0">Kepala Sekolah</p>
                                        <br><br><br><br>
                                        <b> {{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
