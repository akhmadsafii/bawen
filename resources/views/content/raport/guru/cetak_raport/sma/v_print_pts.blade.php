<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%" class="table">
        <thead>
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th colspan="6">
                                <h3 class="text-center"
                                    style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
                                    LAPORAN
                                    HASIL PENILAIAN AKHIR SEMESTER</h3>
                            </th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 30px" class="vertical-top">
                                {{ strtoupper($lihat['sekolah']) }}
                            </td>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top"> {{ (new \App\Helpers\Help())->getKelas($lihat['kelas']) }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">
                                {{ $lihat['alamat'] ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['angka_semester'] }} ({{ $lihat['semester'] }})
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 30px">
                                {{ strtoupper($lihat['nama']) }}
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ str_replace('/', '-', $lihat['tahun_ajaran']) }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['nis'] ?? '-' }} / {{ $lihat['nisn'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-left" style="width: 20px">A.</th>
                            <th class="text-left" style="width: 150px">SIKAP</th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                1
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Sikap Spiritual</p>
                            </td>
                            <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                                <p class="m-0" style="font-size: 13px">
                                    @if ($lihat['nilai_spiritual']['selalu'] != null)
                                        Selalu melakukan sikap :
                                        {{ $lihat['nilai_spiritual']['selalu'] }} <br>
                                        Mulai meningkat pada sikap :
                                        {{ $lihat['nilai_spiritual']['meningkat'] }}
                                    @else
                                        Belum diinput
                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                2
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                <p class="m-0" style="font-size: 13px">Sikap Sosial</p>
                            </td>
                            <td style="border: solid 2px black; padding: 3px;" class="vertical-middle">
                                <p class="m-0" style="font-size: 13px">
                                    @if ($lihat['nilai_sosial']['selalu'] != null)
                                    @php
                                        $selalu = $lihat['nilai_sosial']['selalu'];
                                        $ruwet = [];
                                        foreach ($selalu as $sl) {
                                            $ruwet[] = $sl['nama'];
                                        }
                                        $so_text_selalu = implode(', ', $ruwet);
                                    @endphp
                                    Selalu melakukan sikap : {{ $so_text_selalu }}.
                                    Mulai meningkat pada sikap :
                                    {{ $lihat['nilai_sosial']['meningkat'] }};
                                @else
                                    Belum diinput
                                @endif
                                </p>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td style="height: 10px" colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                No
                            </th>
                            <th class="text-center" rowspan="2" style="border: solid 2px black; padding: 3px;">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;"
                                colspan="4">
                                Pengetahuan
                            </th>
                            <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;"
                                colspan="4">
                                Keterampilan</th>
                        </tr>
                        <tr>
                            <th style="border: solid 2px black; padding: 3px;">
                                KKM</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Angka</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                KKM</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Angka</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                            <th style="border: solid 2px black; padding: 3px;">
                                Deskripsi</th>
                        </tr>
                        @if (!empty($lihat['nilai_mapel']))
                            @foreach ($lihat['nilai_mapel'] as $mapel)
                                @php
                                    $no_mp = 1;
                                @endphp
                                @foreach ($mapel['mapel'] as $mp)
                                    <tr>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $no_mp++ }}</td>
                                        <td style="border: solid 2px black; padding: 3px;">{{ $mp['mapel'] }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['kkm'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['nilai_akhir_pengetahuan'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['predikat_pengetahuan'] }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['deskripsi_pengetahuan'] }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['kkm'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['nilai_akhir_kerampilan'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['predikat_kerampilan'] }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $mp['deskripsi_keterampilan'] }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="10">
                                    Belum ada Mapel yang tersedia</td>
                            </tr>
                        @endif


                        <tr>
                            <td colspan="2" style="border: solid 2px black; padding: 3px;">Jumlah Nilai</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">1120</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">1318</td>
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border: solid 2px black; padding: 3px;">Rata-Rata</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">70</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">83</td>
                            <td colspan="6"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        <tr>
                            <th style="width: 25%"></th>
                            <th class="text-center">Mengetahui</th>
                            <th class="text-center" style="width: 30%">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }}, 25 Juni 2021</th>
                        </tr>
                        <tr>
                            <th>
                                <div>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Kepala {{ $lihat['sekolah'] }}</p>
                                    <br><br><br><br>
                                    {{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}
                                </div>
                            </th>
                            <th>
                                <div>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    {{ $lihat['wali_kelas']['nama'] ?? '-' }}
                                </div>
                            </th>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
