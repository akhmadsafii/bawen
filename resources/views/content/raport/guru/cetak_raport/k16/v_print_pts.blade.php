<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <center>
        {{-- <img src="{{ asset('asset/img/tutwuri.png') }}" alt="" width="200"> --}}
        <h2 class="text-center" style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">
            PENILAIAN
            TENGAH SEMESTER GANJIL</h2>
    </center>
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <th style="width: 300px; padding-right: 20px" class="vertical-top text-left">
                                {{ strtoupper($lihat['sekolah']) }}
                            </th>
                            <td class="vertical-top">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <th class="vertical-top text-left">
                                {{ $lihat['rombel'] }}</th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left" style="width: 300px; padding-right: 20px">
                                {{ $lihat['alamat_sekolah'] ?? '-' }}
                            </th>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">{{ $lihat['angka_semester'] }}
                                ({{ $lihat['semester'] }})</th>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left" style="width: 300px; padding-right: 20px">
                                {{ strtoupper($lihat['nama']) }}
                            </th>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">{{ str_replace('/', '-', $lihat['tahun_ajaran']) }}
                            </th>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <th class="vertical-top text-left">{{ $lihat['nis'] ?? '-' }} /
                                {{ $lihat['nisn'] ?? '-' }}</th>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                        </tr>
                    </thead>
                </table>
                <br>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                No
                            </th>
                            <th class="text-center" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                KKM
                            </th>
                            <th class="text-center vertical-middle" colspan="10"
                                style="border: solid 2px black; padding: 3px;">
                                Hasil Penilaian Harian (HPH)</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px;">HPTS</th>
                            <th class="text-center vertical-middle" rowspan="3"
                                style="border: solid 2px black; padding: 3px;">
                                KETERANGAN</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle" colspan="5"
                                style="border: solid 2px black; padding: 3px;">
                                Pengetahuan</th>
                            <th class="text-center vertical-middle" colspan="5"
                                style="border: solid 2px black; padding: 3px;">
                                Ketrampilan</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                1</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                2</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                3</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                4</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                5</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                1</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                2</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                3</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                4</th>
                            <th class="text-center vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 20px">
                                5</th>
                        </tr>
                        @if (!empty($lihat['nilai_mapel']))
                            @foreach ($lihat['nilai_mapel'] as $nilai)
                                <tr>
                                    <th colspan="15" class="text-left"
                                        style="border: solid 2px black; padding: 3px;">
                                        Kelompok {{ $nilai['kelompok'] }}</th>
                                </tr>
                                @if (!empty($nilai['mapel']))
                                    @php
                                        $no_mapel = 1;
                                    @endphp
                                    @foreach ($nilai['mapel'] as $mapel)
                                        <tr>
                                            <td class="text-center vertical-middle"
                                                style="border: solid 2px black; padding: 3px;">{{ $no_mapel++ }}
                                            </td>
                                            <td style="border: solid 2px black; padding: 3px;">
                                                <p class="m-0" style="font-size: 13px">
                                                    {{ $mapel['mapel'] }}
                                                </p>
                                            </td>
                                            <td class="text-center vertical-middle"
                                                style="border: solid 2px black; padding: 3px;">{{ $mapel['kkm'] }}
                                            </td>
                                            @php
                                                $nomorp = [];
                                                $nomork = [];
                                                for ($i = 1; $i <= 5; $i++) {
                                                    $nomorp[] = $i;
                                                    $nomork[] = $i;
                                                }
                                                $np = [];
                                                $nk = [];
                                                foreach ($nomorp as $key => $val) {
                                                    if ($key == null && empty($mapel['nilai_pengetahuan'][$key])) {
                                                        $np[$key] = '';
                                                    } elseif ($key != null && empty($mapel['nilai_pengetahuan'][$key])) {
                                                        $np[$key] = '';
                                                    } elseif ($key != null && !empty($mapel['nilai_pengetahuan'][$key])) {
                                                        $np[$key] = $mapel['nilai_pengetahuan'][$key]['nilai'];
                                                    } else {
                                                        $np[$key] = $mapel['nilai_pengetahuan'][$key]['nilai'];
                                                    }
                                                }
                                                foreach ($nomork as $ket => $val) {
                                                    if ($ket == null && empty($mapel['nilai_keterampilan'][$ket])) {
                                                        $nk[$ket] = '';
                                                    } elseif ($ket != null && empty($mapel['nilai_keterampilan'][$ket])) {
                                                        $nk[$ket] = '';
                                                    } elseif ($ket != null && !empty($mapel['nilai_keterampilan'][$ket])) {
                                                        $nk[$ket] = $mapel['nilai_keterampilan'][$ket]['nilai'];
                                                    } else {
                                                        $nk[$ket] = $mapel['nilai_keterampilan'][$ket]['nilai'];
                                                    }
                                                }
                                            @endphp
                                            @foreach ($np as $pengetahuan)
                                                <td class="text-center vertical-middle"
                                                    style="border: solid 2px black; padding: 3px;">{{ $pengetahuan }}
                                                </td>
                                            @endforeach
                                            @foreach ($nk as $ketrampilan)
                                                <td class="text-center vertical-middle"
                                                    style="border: solid 2px black; padding: 3px;">{{ $ketrampilan }}
                                                </td>
                                            @endforeach
                                            <td class="text-center vertical-middle"
                                                style="border: solid 2px black; padding: 3px;">
                                                {{ $mapel['hasil_pts'] }}
                                            </td>
                                            <td class="text-center vertical-middle"
                                                style="border: solid 2px black; padding: 3px;">
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;" colspan="15">Belum ada mapel yang
                                        tersedia
                                    </td>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="15" class="text-left" style="border: solid 2px black; padding: 3px;">
                                    Tidak ada mapel yang tersedia</td>
                            </tr>
                        @endif
                        <tr>
                            <td colspan="15" style="height: 20px">
                                <p class="m-0" style="font-size: 12px"><i>HPTS= Hasil Penilaian Tengah
                                        Semester (khusus pada aspek Pengetahuan)</i></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                KKM
                            </th>
                            <th colspan="{{ count($predikat) }}" class="text-center"
                                style="border: solid 2px black; padding: 3px;">
                                Predikat</th>
                        </tr>
                        <tr>
                            @foreach ($predikat as $pd)
                                <th class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $pd['keterangan'] }} ({{ $pd['predikat'] }})
                                </th>
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                75
                            </td>
                            @foreach ($predikat as $prd)
                                <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                    {{ $prd['skor1'] . ' - ' . $prd['skor2'] }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
                <br>
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 40%">
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </td>
                            <td style="width: 20%"></td>
                            <td style="width: 40%;">
                                <div style="float:right">
                                    <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                        {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                    </p>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                    <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>
