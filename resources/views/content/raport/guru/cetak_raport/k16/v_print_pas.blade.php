<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width: 100%
        }


        .vertical-middle {
            vertical-align: middle
        }

        .vertical-top {
            vertical-align: top
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .m-0 {
            margin: 0
        }

        .pagebreak {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <h2 class="text-center" style="font-family: Comic Sans MS, Comic Sans, cursive; font-weight: regular">PENCAPAIAN
        KOMPETENSI
        PESERTA DIDIK</h2>
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <td class="vertical-top">Nama Sekolah</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td style="width: 300px; padding-right: 20px" class="vertical-top">
                                {{ strtoupper($lihat['sekolah']) }}
                            </td>
                            <td class="vertical-top;">Kelas</td>
                            <td style="width: 10px" class="vertical-top">:</td>
                            <td class="vertical-top">{{ (new \App\Helpers\Help())->getKelas($lihat['kelas']) }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Alamat</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 20px">
                                {{ $lihat['alamat_sekolah'] ?? '-' }}
                            </td>
                            <td class="vertical-top">Semester</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['angka_semester'] }} ({{ $lihat['semester'] }})
                            </td>
                        </tr>
                        <tr>
                            <td class="vertical-top">Nama</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top" style="width: 300px; padding-right: 20px">
                                {{ strtoupper($lihat['nama']) }}
                            </td>
                            <td class="vertical-top">Tahun Pelajaran</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ str_replace('/', '-', $lihat['tahun_ajaran']) }}</td>
                        </tr>
                        <tr>
                            <td class="vertical-top">NIS / NISN</td>
                            <td class="vertical-top">:</td>
                            <td class="vertical-top">{{ $lihat['nis'] ?? '-' }} / {{ $lihat['nisn'] ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tr>
                        <th class="text-left" style="border: solid 2px black; width: 10px !important">A.</th>
                        <th class="text-left" style="border: solid 2px black;">SIKAP</th>
                    </tr>
                    <tr>
                        <th class="text-left" style="border: solid 2px black;">1.</th>
                        <th class="text-left" style="border: solid 2px black;">Sikap Spiritual</th>
                    </tr>
                    <tr>
                        <td style="border: solid 2px black; padding: 3px;" colspan="2">
                            <p class="m-0" style="min-height: 60px">
                                @if ($lihat['nilai_spiritual']['selalu'] != null)
                                    Selalu melakukan sikap :
                                    {{ $lihat['nilai_spiritual']['selalu'] }} <br>
                                    Mulai meningkat pada sikap :
                                    {{ $lihat['nilai_spiritual']['meningkat'] }}
                                @else
                                    Belum diinput
                                @endif
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">2.</th>
                        <th class="text-left">Sikap Sosial</th>
                    </tr>
                    <tr>
                        <td style="border: solid 2px black; padding: 3px;" colspan="2">
                            <p class="m-0" style="min-height: 60px">
                                @if ($lihat['nilai_sosial']['selalu'] != null)
                                    @php
                                        $selalu = $lihat['nilai_sosial']['selalu'];
                                        $ruwet = [];
                                        foreach ($selalu as $sl) {
                                            $ruwet[] = $sl['nama'];
                                        }
                                        $so_text_selalu = implode(', ', $ruwet);
                                    @endphp
                                    Selalu melakukan sikap : {{ $so_text_selalu }}.
                                    Mulai meningkat pada sikap :
                                    {{ $lihat['nilai_sosial']['meningkat'] }};
                                @else
                                    Belum diinput
                                @endif
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">B.</th>
                            <th class="text-left" colspan="4">PENGETAHUAN</th>
                        </tr>
                        <tr>
                            <th class="text-left"></th>
                            <th class="text-left" colspan="4">Ketuntasan Belajar Minimal 75</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">No
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 25%">
                                Mata
                                Pelajaran</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7%">
                                Nilai
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 7px">
                                KKM</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 100%">
                                Deskripsi</th>
                        </tr>
                        @if (!empty($lihat['nilai_mapel']))
                            @php
                                $no_mapel = 1;
                            @endphp
                            @foreach ($lihat['nilai_mapel'] as $nm)
                                <tr>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;"> {{ $no_mapel++ }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">{{ $nm['mapel'] }}</td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $nm['nilai'] }}
                                    </td>
                                    <td class="text-center vertical-middle"
                                        style="border: solid 2px black; padding: 3px;">
                                        {{ $nm['kkm'] }}
                                    </td>
                                    <td style="border: solid 2px black; padding: 3px;">
                                        <p class="m-0" style="font-size: 12px">
                                            {{ $nm['capaian_hasil'] }}</p>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;" colspan="10">
                                    Belum ada Mapel yang tersedia</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-center vertical-middle" rowspan="2"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                KKM
                            </th>
                            <th colspan="4" class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Predikat</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Kurang (D)
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Cukup (C)
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Baik (B)
                            </th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                Sangat Baik (A)
                            </th>
                        </tr>
                        <tr>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                75
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                < 75 </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                75 - 83
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                84 - 92
                            </td>
                            <td class="text-center vertical-middle" style="border: solid 2px black; padding: 3px;">
                                93 - 100
                            </td>

                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tr>
                        <th class="text-left" style="width: 20px">C.</th>
                        <th class="text-left" colspan="3">EKSTRA KURIKULER</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="border: solid 2px black; padding: 3px;">
                            No</th>
                        <th class="text-center" style="border: solid 2px black; padding: 3px; width: 200px">
                            Kegiatan Ekstrakulikuler</th>
                        <th class="text-center" style="border: solid 2px black; padding: 3px; width: 70px">
                            Predikat</th>
                        <th class="text-center" style="border: solid 2px black; padding: 3px;">
                            Keterangan</th>
                    </tr>
                    @php
                        $no_ekstra = 1;
                    @endphp
                    @foreach ($lihat['nilai_ekstra'] as $ne)
                        <tr>
                            <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                {{ $no_ekstra++ }}</td>
                            <td style="border: solid 2px black; padding: 3px;">{{ $ne['nama'] }}</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $ne['nilai'] != null ? $ne['nilai'] : '-' }}</td>
                            <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                {{ $ne['deskripsi'] != null ? $ne['deskripsi'] : '-' }}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">D.</th>
                            <th class="text-left" colspan="2">PRESTASI</th>
                        </tr>
                        <tr>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px;">
                                No
                            </th>
                            <th class="text-center"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 200px">
                                Jenis Prestasi</th>
                            <th class="text-center vertical-middle"
                                style="background-color: rgb(228, 227, 227); border: solid 2px black; padding: 3px; width: 100%">
                                Keterangan</th>
                        </tr>
                        @php
                            $max_pres = 3;
                        @endphp
                        @if (!empty($lihat['nilai_dt_prestasi']))

                            @if (count($lihat['nilai_dt_prestasi']) <= 3)
                                @php
                                    $no_npm = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $npm)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_npm }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $npm['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $npm['keterangan'] }}</td>
                                    </tr>
                                    @php
                                        $no_npm++;
                                    @endphp
                                    @for ($i = $no_npm; $i <= 3; $i++)
                                        <tr>
                                            <td class="text-center vertical-top"
                                                style="border: solid 2px black; padding: 3px;">
                                                {{ $i }}</td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                            </td>
                                            <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                            </td>
                                        </tr>
                                    @endfor
                                @endforeach
                            @else
                                @php
                                    $no_np = 1;
                                @endphp
                                @foreach ($lihat['nilai_dt_prestasi'] as $np)
                                    <tr>
                                        <td class="text-center vertical-top"
                                            style="border: solid 2px black; padding: 3px;">
                                            {{ $no_np++ }}</td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $np['nama'] }}
                                        </td>
                                        <td class="text-center" style="border: solid 2px black; padding: 3px;">
                                            {{ $np['keterangan'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @else
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    1</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    2</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                            <tr>
                                <td class="text-center vertical-top" style="border: solid 2px black; padding: 3px;">
                                    3</td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-
                                </td>
                                <td class="text-center" style="border: solid 2px black; padding: 3px;">-</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">E.</th>
                            <th class="text-left" colspan="2">KETIDAKHADIRAN</th>
                        </tr>
                    </tbody>
                </table>
                <table style="border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Sakit
                            </td>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                {{ $lihat['nilai_absensi']['sakit'] }} Hari</td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Izin
                            </td>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                {{ $lihat['nilai_absensi']['izin'] }} Hari</td>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px; width: 228px">
                                Tanpa Keterangan
                            </td>
                            <td class="text-left vertical-middle"
                                style="border: solid 2px black; padding: 3px;  width: 228px">
                                {{ $lihat['nilai_absensi']['alpha'] }} Hari</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">F.</th>
                            <th class="text-left" style="width: 100%">CATATAN WALI KELAS</th>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" colspan="2"
                                style="border: solid 2px black; padding: 3px;">
                                <div style="width: 100%; min-height: 60px">
                                    <p class="m-0">
                                        {{ !empty($lihat['catatan']) ? $lihat['catatan']['isi'] : '-' }}
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width: 20px">G.</th>
                            <th class="text-left" colspan="2">TANGGAPAN ORANG TUA/WALI</th>
                        </tr>
                        <tr>
                            <td class="text-left vertical-middle" colspan="3"
                                style="border: solid 2px black; padding: 3px;">
                                <div style="width: 100%; min-height: 60px">
                                    <p class="m-0">
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px"></td>
        </tr>
        <tr>
            <td>
                <table style="border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <td class="text-left vertical-middle" colspan="3"
                                style="border: solid 2px black; padding: 3px; width: 456px;">
                                <div style="width: 100%; min-height: 100px">
                                    <b>Keputusan : </b>
                                    <p class="m-0">
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 30%">
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Orang Tua/Wali</p>
                                    <br><br><br><br>
                                    ................................
                                </div>
                            </td>
                            <td style="width: 40%"></td>
                            <td style="width: 40%">
                                <div>
                                    <p class="m-0">{{ $lihat['kabupaten'] ?? 'Kota belum diset' }},
                                        {{ (new \App\Helpers\Help())->getTanggal($lihat['raport_config']['tgl_raport']) }}
                                    </p>
                                    <p class="m-0">Wali Kelas,</p>
                                    <br><br><br><br>
                                    <b>{{ $lihat['wali_kelas']['nama'] ?? '-' }}</b>
                                    <p class="m-0">NIP. {{ $lihat['wali_kelas']['nip'] ?? '-' }}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div>
                                    <p class="m-0">Mengetahui :</p>
                                    <p class="m-0">Kepala Sekolah</p>
                                    <br><br><br><br>
                                    <b> {{ $lihat['raport_config']['kepala_sekolah'] ?? '-' }}</b>
                                    <p class="m-0">NIP.
                                        {{ $lihat['raport_config']['nip_kepala_sekolah'] ?? '-' }}</p>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>

</html>
