@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">Riwayat pengajaran anda, Tahun {{ $tahun['tahun_ajaran'] }} Semester
                            {{ $tahun['semester'] }}</h5>
                        <p class="text-muted">History Pengajaran anda.</p>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#</th>
                                        <th>Waktu</th>
                                        <th>Kelas</th>
                                        <th>Mata Pelajaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($mapel))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($mapel as $mp)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td class="vertical-middle">{{ (new \App\Helpers\Help())->getTanggalLengkap($mp['created_at']) }}</td>
                                                <td>
                                                    <b>{{ $mp['rombel'] }}</b><br><small>{{ $mp['jurusan'] }}</small>
                                                </td>
                                                <td class="vertical-middle">{{ $mp['mapel'] }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Berlum ada data yang tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>

@endsection
