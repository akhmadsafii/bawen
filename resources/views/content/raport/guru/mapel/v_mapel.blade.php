@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <b>Petunjuk : </b><br>
                            Menu ini digunakan untuk menginput nilai pada setiap masing-masing mata pelajaran diampu.
                            Silakan klik menu <b><i>Nilai Pengetahuan</i></b> untuk menginput nilai pengetahuan, dan
                            <b><i>Nilai Keterampilan</i></b> untuk menginput nilai keterampilan.
                        </div>
                    </div>
                    @if (!empty($mapel))
                        @foreach ($mapel as $item)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h5 class="m-0 text-white">
                                            {{ $item['mapel'] . ' - ' . $item['rombel'] }}</h5>
                                        <small class="text-white">{{ $item['jurusan'] }}</small>
                                    </div>
                                    <div class="card-body">
                                        <div class="alert alert-info" role="alert">
                                            @if ($item['config_template'] != null)
                                                @php
                                                    $nilai = '';
                                                    $jenis = '';
                                                    if ($item['config_template']['jenis'] == 'pas') {
                                                        $jenis = 'PAS';
                                                        if ($item['config_template']['template'] == 'k16') {
                                                            $nilai = 'Nilai K16';
                                                        } elseif ($item['config_template']['template'] == 'manual') {
                                                            $nilai = 'Nilai Manual';
                                                        } else {
                                                            $nilai = 'Nilai Pengetahuan & Ketrampilan';
                                                        }
                                                    } else {
                                                        $jenis = 'PTS';
                                                        if ($item['config_template']['template'] == 'k16') {
                                                            $nilai = 'Nilai K16';
                                                        } elseif ($item['config_template']['template'] == 'manual' || $item['config_template']['template'] == 'sd') {
                                                            $nilai = 'Nilai Manual';
                                                        } else {
                                                            $nilai = 'Nilai Pengetahuan & Ketrampilan';
                                                        }
                                                    }

                                                @endphp
                                                <p class="text-info">Saat ini Rombel menggunakan settingan raport
                                                    jenis <b
                                                        class="text-success">{{ strtoupper($item['config_template']['jenis']) }}</b>,
                                                    template <b
                                                        class="text-success">{{ strtoupper($item['config_template']['template']) }}</b>
                                                </p>
                                                <p class="text-justify">Jadi pastikan <b class="text-danger">{{ $nilai }}</b> yang
                                                    berada di kolom <b class="text-danger">Nilai {{ $jenis }}</b>
                                                    sudah terisi</p>
                                            @else
                                                <p class="text-danger m-0">Admin Belum setting template untuk output raport
                                                </p>
                                            @endif
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered hover">
                                                <tr>
                                                    <th class="text-center">Nilai PTS</th>
                                                    <th class="text-center">Nilai PAS</th>
                                                </tr>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <a
                                                                href="{{ route('raport-nilai_uts_k16', ['gp' => (new \App\Helpers\Help())->encode($item['id']),'th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}">
                                                                &raquo; Nilai K16
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a
                                                                href="{{ route('raport-nilai_uas_k16', ['gp' => (new \App\Helpers\Help())->encode($item['id'])]) }}">
                                                                &raquo; Nilai K16
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a
                                                                href="{{ route('raport-nilai_pts_manual', ['gp' => (new \App\Helpers\Help())->encode($item['id']),'th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}">
                                                                &raquo; Nilai Manual
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a
                                                                href="{{ route('raport-nilai_pas_manual', ['gp' => (new \App\Helpers\Help())->encode($item['id']),'th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}">
                                                                &raquo; Nilai Manual
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <a
                                                                href="{{ route('raport-nilai_pengetahuan', (new \App\Helpers\Help())->encode($item['id'])) }}">
                                                                &raquo; Nilai Pengetahuan
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <a
                                                                href="{{ route('raport-nilai_ketrampilan', (new \App\Helpers\Help())->encode($item['id'])) }}">
                                                                &raquo; Nilai Ketrampilan
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-info">Belum ada mapel yang diampu..</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
