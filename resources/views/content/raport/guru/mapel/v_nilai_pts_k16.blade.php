@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: none;
        }

    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="m-2"><b>Nilai K16 &raquo; <?php echo $guru['mapel'] . ' - ' . $guru['rombel']; ?> </b>
                            </h5>
                        </div>
                        <div class="col-md-4">
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                                {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p><strong>Selamat Datang {{ $guru['guru'] }}</strong>
                        </p>
                        <p class="m-0">Saat ini settingan raport anda berada di perhitungan</p>
                        <ul>
                            <li>Nilai Ulangan Harian {{ $config['nilai_uh_rata'] }}%</li>
                            <li>Nilai Ulangan Tengah Semester {{ $config['nilai_uts'] }}%</li>
                            <li><b>Total {{ $config['nilai_uh_rata'] + $config['nilai_uts'] }}%</b></li>
                        </ul>
                        <p class="m-2 text-danger">*)Bila total keseluruhan tidak 100% Harap hubungi Admin</p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="data-tabel">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">No</th>
                                    <th>Profile</th>
                                    <th class="text-center">Nilai</th>
                                    <th>Deskripsi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="config_uts" value="{{ $config['nilai_uts'] }}">
    <input type="hidden" id="config_uh" value="{{ $config['nilai_uh_rata'] }}">
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalNilai" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formNilai">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id_kelas_siswa" id="id_kelas_siswa">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Nilai Harian</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nilai_uh_rata" name="nilai_uh_rata"
                                    onkeypress="return hanyaAngka(event)" type="text">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-3 col-form-label" for="l0">Nilai UTS</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nilai_uts" name="nilai_uts"
                                    onkeypress="return hanyaAngka(event)" type="text">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-10 col-form-label" for="l0">
                                <hr>
                            </label>
                            <div class="col-md-1 my-auto">
                                <a href="javascript:void(0)" class="btn btn-purple btn-sm"
                                    onclick="hitungAkhir()">Hitung</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Nilai Akhir</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nilai_akhir" name="nilai_akhir"
                                    onkeypress="return hanyaAngka(event)" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Deskripsi</label>
                            <div class="col-md-9">
                                <textarea name="capaian_hasil" id="capaian_hasil" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'profile',
                        name: 'profile',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nilai',
                        name: 'nilai',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi',
                    },
                ]
            });

            $(document).on('click', '.edit', function() {
                $('#formNilai').trigger("reset");
                $('#modelHeading').html("Update Nilai");
                $('#id_kelas_siswa').val($(this).data('id'));
                $('#nilai_uh_rata').val($(this).data('uh'));
                $('#nilai_uts').val($(this).data('uts'));
                $('#nilai_akhir').val($(this).data('akhir'));
                $('#capaian_hasil').val($(this).data('keterangan'));
                $('#modalNilai').modal('show');
            });

            $("#formNilai").on("submit", function() {
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var data = $(this).serialize() +
                    "&id_ta_sm={{ $_GET['th'] }}&id_guru_pelajaran={{ $_GET['gp'] }}";
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{ route('simpan-raport_pts_nilai_k16') }}",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formNilai').trigger("reset");
                            $('#modalNilai').modal('hide');
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
                return false;
            });
        })


        function hitungAkhir() {
            let harian = parseFloat($("#nilai_uh_rata").val()) * parseInt($("#config_uh").val()) / 100;
            // console.log("harian " + harian);
            let uts = parseFloat($("#nilai_uts").val()) * parseInt($("#config_uts").val()) / 100;
            // console.log("uts " + uts);
            let hasil_juml = harian + uts;
            let hasil = parseInt(hasil_juml);
            $('#nilai_akhir').val(hasil);

        }
    </script>
@endsection
