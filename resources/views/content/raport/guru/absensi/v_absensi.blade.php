@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }


        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <a href="http://raport.mysch.web.id/n_absensi/cetak/2" class="btn btn-warning"
                                target="_blank"><i class="fa fa-print"></i> Cetak</a>
                            <a href="javascript:void(0)" id="import" class="btn btn-info"><i class="fa fa-upload"></i>
                                Import Absensi</a>
                            {{-- <a href="http://raport.mysch.web.id/n_absensi/cetak/2" class="btn btn-warning"
                                target="_blank"><i class="fa fa-print"></i> Cetak</a> --}}
                        </p>
                    </div>
                    <div class="col-md-12" id="load_absensi">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Nilai Absensi </h4>
                            </div>
                            <div class="content">
                                <form method="post" id="n_absensi">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="6%">No</th>
                                                <th width="20%">Nama</th>
                                                <th width="20%">NISN</th>
                                                <th width="17%">Sakit</th>
                                                <th width="17%">Izin</th>
                                                <th width="20%">Tanpa Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($siswa as $ssw)
                                                <input type="hidden" name="id_siswa_{{ $no }}"
                                                    value="{{ $ssw['id_kelas_siswa'] }}">
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $ssw['nama'] }}</td>
                                                    <td>{{ $ssw['nisn'] }}</td>
                                                    <td>
                                                        <input type="number" min="0" max="100" name="s_{{ $no }}"
                                                            value="{{ $ssw['sakit'] }}" class="form-control input-sm"
                                                            required id="s_{{ $no }}">
                                                    </td>
                                                    <td>
                                                        <input type="number" min="0" max="100" name="i_{{ $no }}"
                                                            value="{{ $ssw['izin'] }}" class="form-control input-sm"
                                                            required id="i_{{ $no }}">
                                                    </td>
                                                    <td>
                                                        <input type="number" min="0" max="100" name="a_{{ $no }}"
                                                            value="{{ $ssw['alpha'] }}" class="form-control input-sm"
                                                            required id="a_{{ $no }}">
                                                    </td>

                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="jumlah" value="{{ count($siswa) + 1 }}">
                                    <button type="submit" class="btn btn-success" id="tbsimpan"><i class="fa fa-check"></i>
                                        Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form action="javascript:void(0)" id="formImport" name="formImport" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="javascript:void(0)" onclick="return template()"
                                                style="color:#03a9f3">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        // var url_import = "{{ route('raport-import_absensi') }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#n_absensi").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "absensi/simpan",
                beforeSend: function() {
                    $("#tbsimpan").attr("disabled", true);
                },
                success: function(r) {
                    $("#tbsimpan").attr("disabled", false);
                    if (r.status == "ok") {
                        noti("success", r.data);
                    } else {
                        noti("danger", r.data);
                    }
                }
            });
            return false;
        });

        $('#import').click(function() {
            $('#importModal').modal('show');
            $('#file-chosen').html('No file choosen');
            $('#HeadingImport').html('Import Nilai Absensi');
        });

        $('body').on('submit', '#formImport', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $('#importBtn').html('Sending..');
            var formDatas = new FormData(document.getElementById("formImport"));
            $.ajax({
                type: "POST",
                url: "{{ route('raport-import_absensi') }}",
                data: formDatas,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    if (data.status == 'berhasil') {
                        $('#importFile').trigger("reset");
                        $('#importModal').modal('hide');
                        $("#load_absensi").load(" #load_absensi");
                    }
                    $('#importBtn').html('Simpan');
                    noti(data.icon, data.success);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#importBtn').html('Simpan');
                }
            });
        });

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })

        function template() {
            window.location.href = "{{ $url }}";
        }
    </script>

@endsection
