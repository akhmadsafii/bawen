@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        Status Naik Kelas dan Catatan Wali
                    </div>
                    <div class="card-body">
                        <div class="content">
                            <form method="post" id="n_catatan">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="15%">Nama</th>
                                            <th width="15%">NISN</th>
                                            <th width="10%">Naik Kelas</th>
                                            <th width="45%">Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <input type="hidden" name="mode_form" value="{{ $form }}">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @if (!empty($siswa))
                                            @foreach ($siswa as $ssw)
                                                <input type="hidden" name="id_siswa_{{ $no }}"
                                                    value="{{ $ssw['id'] }}">
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $ssw['nama'] }}</td>
                                                    <td>{{ $ssw['nisn'] }}</td>
                                                    <td>
                                                        <select name="naik_{{ $no }}"
                                                            class="form-control input-sm" required
                                                            id="naik_{{ $no }}">
                                                            @foreach ($naik as $key => $na)
                                                                <option value="{{ $key }}"
                                                                    {{ $key == $ssw['naik_kelas'] ? 'selected' : '' }}>
                                                                    {{ $na }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="catatan_{{ $no }}"
                                                            value="{{ $ssw['isi'] }}" class="form-control input-sm"
                                                            id="catatan_{{ $no }}">
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">Belum ada data siswa</td>
                                            </tr>
                                        @endif
                                    </tbody>

                                </table>

                                <input type="hidden" name="jumlah" value="{{ count($siswa) + 1 }}">
                                <button type="submit" class="btn btn-success" id="tbsimpan"><i class="fa fa-check"></i>
                                    Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#n_catatan").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "catatan/simpan",
                beforeSend: function() {
                    $("#tbsimpan").attr("disabled", true);
                },
                success: function(r) {
                    $("#tbsimpan").attr("disabled", false);
                    if (r.status == "ok") {
                        noti("success", r.data);
                    } else {
                        noti("danger", r.data);
                    }
                }
            });

            return false;
        });

    </script>

@endsection
