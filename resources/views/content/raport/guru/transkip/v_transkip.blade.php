@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        span.text-inverse {
            text-transform: uppercase;
            letter-spacing: 0.19048em;
            padding: 0.5 rem !important;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="box-title">{{ Session::get('title') }}</h3>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Tahun Ajaran</label>
                    <select name="tahun_ajar" class="form-control" id="tahun_ajar">
                        @foreach ($tahun as $th)
                            @php
                                $ajar = explode('/', $th['tahun_ajaran']);
                            @endphp
                            <option value="{{ $ajar[0] }}" {{ $ajar[0] == $tahuns ? 'selected' : '' }}>
                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Jurusan</label>
                    <select name="id_jurusan" class="form-control" id="id_jurusan">
                        <option value="">Pilih jurusan..</option>
                        @foreach ($jurusan as $jr)
                            @php
                                $select = '';
                                if ($select_jurusan == (new \App\Helpers\Help())->encode($jr['id'])) {
                                    $select = 'selected';
                                }
                            @endphp
                            <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}" {{ $select }}>
                                {{ $jr['nama'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">NIS</label>
                    <input type="text" name="nis" id="nis" class="form-control" value="{{ $search }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp; &nbsp;</label>
                    <button onclick="search_siswa('{{ $routes }}')" class="btn btn-info btn-block">Pencarian</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>NISN</th>
                                <th>NIS</th>
                                <th>Kelas</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (empty($siswa))
                                <tr>
                                    <td colspan="7" class="text-center">Data siswa saat ini tidak tersedia</td>
                                </tr>
                            @else
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($siswa as $sw)
                                    <tr>
                                        <td class="vertical-middle">{{ $no++ }}</td>
                                        <td class="vertical-middle">{{ Str::upper($sw['nama']) }}</td>
                                        <td class="vertical-middle">{{ $sw['nisn'] }}</td>
                                        <td class="vertical-middle">{{ $sw['nis'] }}</td>
                                        <td class="vertical-middle">{{ $sw['rombel'] }}</td>
                                        @php
                                            $status = '';
                                            if ($sw['status'] == 1) {
                                                $status = 'success';
                                            } else {
                                                $status = 'danger';
                                            }
                                        @endphp
                                        <td class="vertical-middle">
                                            <span
                                                class="badge badge-{{ $status }} text-inverse">{{ $sw['status'] == 1 ? 'Aktif' : 'Tidak Aktif' }}</span>
                                        </td>
                                        <td class="vertical-middle">
                                            <button data-toggle="collapse" data-target="#transkip{{ $sw['id'] }}"
                                                class="btn btn-sm btn-info accordion-toggle"><span
                                                    class="fas fa-file-signature"></span></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="hiddenRow">
                                            <div class="accordian-body collapse" id="transkip{{ $sw['id'] }}">
                                                @if (session('role') != 'supervisor')
                                                    <button class="btn btn-info btn-sm my-3 pull-right"
                                                        onclick="tambahTranskip({{ $sw['id'] }})"><i
                                                            class="fas fa-plus-circle"></i>
                                                        Tambah Transkip
                                                    </button>
                                                @endif
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">No</th>
                                                            <th class="text-center">Nama</th>
                                                            <th class="text-center">Keterangan</th>
                                                            <th class="text-center">Opsi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dataTranskrip{{ $sw['id'] }}">
                                                        @if (empty($sw['transkrips']))
                                                            <tr>
                                                                <td colspan="5" class="text-center">Data saat ini kosong
                                                                </td>
                                                            </tr>
                                                        @else
                                                            @php
                                                                $nomor = 1;
                                                            @endphp
                                                            @foreach ($sw['transkrips'] as $tr)
                                                                <tr>
                                                                    <td class="text-center">{{ $nomor++ }}</td>
                                                                    <td class="text-center">{{ $tr['nama'] }}</td>
                                                                    <td class="text-center">
                                                                        {{ $tr['keterangan'] }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if (session('role') != 'supervisor')
                                                                            <a href="javascript:void(0)"
                                                                                data-id="{{ $tr['id'] }}"
                                                                                data-siswa="{{ $sw['id'] }}"
                                                                                class="btn btn-info btn-sm edit"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <button class="btn btn-danger btn-sm delete"
                                                                                data-id="{{ $tr['id'] }}"
                                                                                data-siswa="{{ $sw['id'] }}">
                                                                                <i class="fas fa-trash"></i></button>
                                                                        @endif
                                                                        <a href="{{ route('raport_transkip-download', (new \App\Helpers\Help())->encode($tr['id'])) }}"
                                                                            target="_blank" class="btn btn-purple btn-sm"><i
                                                                                class="fas fa-file-download"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $pagination !!}
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTranskip" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingTranskrip"></h5>
                </div>
                <form id="formTranskip" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_transkip">
                                <input type="hidden" name="id_kelas_siswa" id="id_kelas_siswa">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" id="nama" name="nama" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="id_ta_sm" id="id_tahun_ajar" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-12 control-label">File Transkrip</label>
                                    <div class="col-sm-12">
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="application/pdf, image/png, image/jpeg" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_jurusan"]').on('change', function() {
                let id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Memproses data kelas...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                            $('#id_rombel').attr('disabled', 'disabled');
                            $('#id_rombel').html('<option>Pilih Rombel..</option>');
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Memproses data Rombel...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('raport_transkip-get_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#formTranskip', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport_transkip-save') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport_transkip-update') }}";
                }
                let id_siswa = $('#id_kelas_siswa').val();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formTranskip').trigger("reset");
                            $('#modalTranskip').modal('hide');
                            $('#dataTranskrip' + id_siswa).html(data.html);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('raport_transkip-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#headingTranskrip').html("Edit Transkrip Siswa");
                        $('#id_transkip').val(data.id);
                        $('#id_kelas_siswa').val(data.id_kelas_siswa);
                        $('#nama').val(data.nama);
                        $('#id_tahun_ajar').val(data.id_ta_sm);
                        $('#keterangan').val(data.keterangan);
                        $('#modalTranskip').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let id_kelas_siswa = $(this).data('siswa');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('raport_transkip-delete') }}",
                        type: "POST",
                        data: {
                            id,
                            id_kelas_siswa
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataTranskrip' + id_kelas_siswa).html(data.html);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function tambahTranskip(id) {
            $('#formTranskip').trigger("reset");
            $('#id_kelas_siswa').val(id);
            $('#headingTranskrip').html("Tambah Transkrip");
            $('#modalTranskip').modal('show');
            $('#action').val('Add');
        }

        function search_siswa(routes) {
            var nis = (document.getElementById("nis") != null) ? document.getElementById("nis").value : "";
            var jurusan = (document.getElementById("id_jurusan") != null) ? document.getElementById("id_jurusan").value :
                "";
            var tahun = (document.getElementById("tahun_ajar") != null) ? document.getElementById("tahun_ajar").value : "";
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?tahun=" + tahun + "&jurusan=" + jurusan + "&nis=" + nis;
            document.location = url;
        }
    </script>
@endsection
