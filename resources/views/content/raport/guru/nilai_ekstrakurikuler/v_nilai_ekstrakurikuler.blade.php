@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        th {
            text-align: center;
            vertical-align: middle;
        }

        .list-group-item.active {
            z-index: 2;
            color: #fff;
            background-color: #e0e0e0;
            border-color: #ffffff;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h2 class="box-title text-white my-0" style="font-size: 14px !important">Daftar Ekstrakurikuler</h2>                                
                            </div>
                            <div class="card-body">
                                <ul class="list-group list-group-flush" id="list_kd">
                                    @foreach ($ekstra as $eks)
                                        <li class="list-group-item" style="cursor: pointer"
                                            onclick="return view_kd({{ $eks['id'] }}, '{{ $eks['nama'] }}');"> <a
                                                href="javascript:void(0)" class="text-info">
                                                <i class="fa fa-chevron-right"></i> {{ $eks['nama'] }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h2 class="box-title text-white my-0" style="font-size: 14px !important">Input Nilai Ekstrakurikuler</h2> 
                            </div>
                            <div class="card-body">
                                <form name="n_ekstra" method="post" action="#" id="n_ekstra">
                                    <input type="hidden" name="id_ekstra" id="id_ekstra" value="">
                                    <div id="load_nilai">

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        nama_ekstra = "";
        id_rombel = "{{ $id_rombel }}";
        view_kd(0, "");
        $('#list_kd li').on('click', function() {
            $('li.active').removeClass('active');
            $(this).addClass('active');
        });

        $("#n_ekstra").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "{{ route('raport-store_nilai_ekstrakurikuler') }}",
                beforeSend: function() {
                    $("#tbsimpan").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading'
                    );
                },
                success: function(r) {
                    $("#tbsimpan").html(
                        '<i class="fa fa-check"></i> Simpan'
                    );
                    $.toast({
                        icon: r.icon,
                        text: r.success,
                        hideAfter: 5000,
                        showConfirmButton: false,
                        position: 'bottom-right',
                    });

                }
            });
            return false;
        });

        function view_kd(id, nama) {
            nama_ekstra = nama;
            if (id == 0) {
                $("#load_nilai").html('<div class="alert alert-info">Silakan pilih Ekstrakurikuler di samping</div>');
            } else {
                $("#id_ekstra").val(id);

                $("#load_nilai").html(
                    '<center><i class="fa fa-spin fa-spinner"></i> Loading</center>'
                );
                $.ajax({
                    url: "nilai-ekstrakurikuler/get_siswa",
                    type: "POST",
                    data: {
                        id_ekstra: id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // console.log(data);
                        $("#load_nilai").show('slow');
                        html =
                            '<table class="table table-bordered"><thead><tr><th width="10%" rowspan="2" style="text-align: left; vertical-align: middle">No</th><th width="15%" rowspan="2" style="text-align: left; vertical-align: middle">Nama</th><th width="20%" rowspan="2" style="text-align: left; vertical-align: middle">NISN</th><th width="60%" colspan="2">Nilai</th></tr><tr><th width="10%">Nilai</th><th width="35%">Deskripsi</th></thead><tbody>';
                        var i = 1;
                        $.each(data.data, function(k, v) {
                            var pnilai = ["-", "A", "B", "C"];
                            html += '<tr><td>' + i + '</td><td>' + v.nama +
                                '</td><td>' + v.nisn +
                                '</td><td><input name="id_siswa[]" type="hidden" value="' + v.id_kelas_siswa +
                                '"><select name="nilai[]" id="nilai_' + i +
                                '" onchange="return ganti_deskripsi(' + i +
                                ');" style="padding: 5px" class="form-control input-sm" required>';
                            for (var x = 0; x < pnilai.length; x++) {
                                html += v.nilai == pnilai[x] ? '<option value="' + pnilai[x] +
                                    '" selected>' + pnilai[x] + '</option>' : '<option value="' +
                                    pnilai[x] + '">' + pnilai[x] + '</option>';
                            }
                            var ides = v.deskripsi == "" ? "-" : v.deskripsi;
                            html +=
                                '</select></td><td><input name="nilai_d[]" type="text" class="form-control input-sm desk" id="desk_' +
                                i + '" value="' + ides + '"></td></tr>';
                            i++;
                        });
                        html +=
                            '</tbody></table><p><button type="submit" class="btn btn-success" id="tbsimpan"><i class="fa fa-check"></i> Simpan</button></p>';
                        $("#load_nilai").html(html);
                    },
                })

            }
            return false;
        }

        function ganti_deskripsi(id) {
            var nilai = $("#nilai_" + id).val();
            var desk = "";
            if (nilai === "A") {
                desk = "Memuaskan, aktif megikuti kegiatan " + nama_ekstra + " mingguan";
            } else if (nilai === "B") {
                desk = "Cukup memuaskan, aktif mengikuti kegiatan " + nama_ekstra + " mingguan";
            } else if (nilai === "C") {
                desk = "Kurang memuaskan, pasif mengikuti kegiatan " + nama_ekstra + " mingguan";
            } else {
                desk = "-";
            }
            $("#desk_" + id).val(desk);
        }

    </script>

@endsection
