@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        th {
            text-align: center;
        }

        .table thead th {
            vertical-align: middle;
        }

        .text-center label {
            display: inline-block;
            margin-bottom: 0;
        }

        .table th,
        .table td {
            padding: 0.57143em;
            vertical-align: middle;
            border-top: 1px solid #efefef;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <a href="{{ URL::previous() }}" class="btn btn-info"><i class="fa fa-arrow-left"></i>
                                Kembali</a>
                            <a href="{{ route('raport-cetak_nilai_sikap_sosial') }}" class="btn btn-warning"
                                target="_blank"><i class="fa fa-print"></i> Cetak</a>
                        </p>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Nilai Sikap Sosial </h4>
                            </div>
                            <div class="content">
                                <form action="javascript:void(0);" id="f_nilai_sso">
                                    @csrf
                                    <table class="table table-condensed table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%" rowspan="2">No</th>
                                                <th width="15%" rowspan="2">Nama</th>
                                                <th width="20%" rowspan="2">NISN</th>
                                                <th width="40%" colspan="{{ count($sikap) }}">Selalu Dilakukan <br> <small style="color: red">* wajib diisi jika ingin diproses simpan</small></th>
                                                <th width="20%" rowspan="2">Mulai Meningkat</th>
                                            </tr>
                                            <tr>
                                                @foreach ($sikap as $skp)
                                                    <th>{{ $skp['nama'] }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <input type="hidden" name="id_guru_mapel" value="">
                                            <input type="hidden" name="mode_form" value="{{ $form }}">
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($siswa as $ssw)
                                                {{-- {{ dd($ssw['selalu']) }} --}}
                                                @php
                                                    $pc_selalu = '';
                                                    if ($ssw['selalu'] != null) {
                                                        // $pc_selalu = "tes";
                                                        $pc_selalu = explode('<br>', $ssw['selalu']);
                                                        // dd($pc_selalu);
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $ssw['nama'] }}</td>
                                                    <td>{{ $ssw['nisn'] }}</td>
                                                    <input type="hidden" name="id_siswa_{{ $no }}"
                                                        value="{{ $ssw['id_kelas_siswa'] }}">
                                                    @foreach ($sikap as $skp)
                                                        <td class="text-center">
                                                            <label class="nso">
                                                                {{-- {{ dd($pc_selalu) }} --}}
                                                                @if (!empty($pc_selalu))
                                                                    @if (in_array($skp['id'], $pc_selalu))
                                                                        <input type="checkbox" name="selalu_{{ $no }}[]"
                                                                            value="{{ $skp['id'] }}" checked>
                                                                    @else
                                                                        <input type="checkbox" name="selalu_{{ $no }}[]"
                                                                            value="{{ $skp['id'] }}">
                                                                    @endif
                                                                @else
                                                                    <input type="checkbox" name="selalu_{{ $no }}[]"
                                                                        value="{{ $skp['id'] }}">
                                                                @endif

                                                            </label>
                                                        </td>
                                                    @endforeach
                                                    <td>
                                                        <select name="meningkat_{{ $no }}" class="form-control"
                                                            id="meningkat">
                                                            <option value=""> - </option>
                                                            @foreach ($sikap as $skp)
                                                                {{-- @if ($form == 'edit') --}}
                                                                <option value="{{ $skp['id'] }}"
                                                                    {{ $skp['id'] == $ssw['meningkat'] ? 'selected' : '' }}>
                                                                    {{ $skp['nama'] }}</option>
                                                                {{-- @else
                                                                <option value="{{ $skp['id'] }}">{{ $skp['nama'] }}</option>
                                                            @endif --}}

                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="jumlah" value="{{ count($siswa) + 1 }}">
                                    <button type="submit" class="btn btn-success" id="tbsimpan"><i class="fa fa-check"></i>
                                        Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#f_nilai_sso").on("submit", function() {
            console.log("tes");
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "nilai-sikap_sosial/store",
                beforeSend: function() {
                    $("#tbsimpan").attr("disabled", true);
                },
                success: function(r) {
                    $("#tbsimpan").attr("disabled", false);
                    if (r.status == "ok") {
                        noti("success", r.data);
                    } else {
                        noti("error", r.data);
                    }
                }
            });
            return false;
        });

    </script>

@endsection
