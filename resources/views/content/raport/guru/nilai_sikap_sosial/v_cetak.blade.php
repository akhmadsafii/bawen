<!DOCTYPE html>
<html>

<head>
    <title>Cetak Nilai Sikap Sosial</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>


    <p align="center"><b>REKAP NILAI SIKAP SOSIAL</b>
        <br>Kelas : {{ $profile['rombel'] }}, Nama Wali : {{ $profile['nama'] }}
    </p>

    <table class="table">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th width="20%">Nama</th>
                <th width="30%">Selalu Dilakukan</th>
                <th width="15%">Mulai Meningkat</th>
            </tr>
        </thead>

        <tbody>
            @if (empty($siswa))
                <tr>
                    <td colspan="4" class="ctr">Belum ada data Siswa</td>
                </tr>
            @else
                @php
                    $no = 1;
                @endphp
                @foreach ($siswa as $sw)
                    <tr>
                        <td class="ctr">{{ $no++ }}</td>
                        <td>{{ ucwords($sw['nama']) }}</td>
                        <td>{{ $sw['selalu'] }}</td>
                        <td>{{ $sw['meningkat'] }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

</body>

</html>
