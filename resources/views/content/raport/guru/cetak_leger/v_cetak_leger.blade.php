@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                        <div class="col-md-6">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                                {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-4">
                            <div class="card border border border-white bg-default">
                                <div class="card-body">
                                    <center>
                                        <a href="{{ route('raport-print_leger', ['th' => $_GET['th']]) }}"
                                            target="_blank">
                                            <i class="fas fa-file-medical-alt fa-9x text-info"></i> <br>
                                            <b class="text-info">Nilai Pengetahuan dan Ketrampilan</b><br>
                                            (<i class="far fa-eye"></i> preview)</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card border border border-white bg-default">
                                <div class="card-body">
                                    <center>
                                        <a href="{{ route('raport-print_leger_ekstra', ['th' => $_GET['th']]) }}"
                                            target="_blank">
                                            <i class="fas fa-file-signature fa-9x text-info"></i> <br>
                                            <b class="text-info">Nilai Ekstra dan Absensi</b><br>
                                            (<i class="far fa-eye"></i> preview)</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
