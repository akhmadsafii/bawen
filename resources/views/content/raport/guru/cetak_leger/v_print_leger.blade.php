<!DOCTYPE html>
<html>

<head>
    <title>Cetak Leger Nilai Pengetahuan</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

    </style>
</head>

<body>
    <p align="left"><b>LEGER NILAI PENGETAHUAN & KETERAMPILAN</b>
        <br>
        Kelas : {{ $wali['rombel'] }}, Nama Wali : {{ $wali['nama'] }}, Tahun Pelajaran
        {{ $tahun['tahun_ajaran'] }}
        <hr style="border: solid 1px #000; margin-top: -10px">
    </p>
    <table class="table">
        <tr>
            <td>Nama</td>
            @foreach ($cetak['mapel'] as $mapel)
                <td class="ctr">{{ $mapel['kode_mapel'] }}</td>
            @endforeach
            <td>Jumlah</td>
            <td>Ranking</td>
        </tr>
        @if (!empty($cetak['nilai']))
            @foreach ($cetak['nilai'] as $nilai)
                <tr>
                    <td>{{ $nilai['nama'] }}</td>
                    @php
                        $jumlah = 0;
                    @endphp
                    @foreach ($nilai['nilai'] as $nl)
                        <td class="ctr">{{ $nl['nilai'] }}</td>
                        @php
                            $jumlah += $nl['nilai'];
                        @endphp
                    @endforeach
                    <td>{{ $jumlah }}</td>
                    <td></td>
                </tr>
            @endforeach
        @else
        @endif
        {{-- <tr>
            <td>Candra</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">81</td>
            <td class="ctr">50</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">73</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">204</td>
            <td></td>
        </tr>
        <tr>
            <td>Dedi</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">88</td>
            <td class="ctr">65</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">0</td>
            <td class="ctr">153</td>
            <td></td>
        </tr> --}}
    </table>
    <p align="right" style="font-size: 10pt; font-style: italic;">Rendered in 0.1299</p>
</body>

</html>
