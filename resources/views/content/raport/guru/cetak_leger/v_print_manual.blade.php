<!DOCTYPE html>
<html>

<head>
    <title>Cetak Leger Nilai Pengetahuan</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

    </style>
</head>

<body>
    <p align="left"><b>LEGER NILAI EKSTRAKURIKULER & ABSENSI</b>
        <br>
        Kelas : {{ $wali['rombel'] }}, Nama Wali : {{ $wali['nama'] }}, Tahun Pelajaran
        {{ $tahun['tahun_ajaran'] }} Semester {{ $tahun['semester'] }}
        <hr style="border: solid 1px #000; margin-top: -10px">
    </p>
    <table class="table">
        <tr>
            <th rowspan="2">Nama</th>
            @foreach ($cetak['ekstra'] as $ekstra)
                <th class="ctr" colspan="2">{{ $ekstra['nama'] }}</th>
            @endforeach
        </tr>
        <tr>

        </tr>
        @foreach ($cetak['nilai'] as $nilai)
            <tr>
                <td>{{ $nilai['nama'] }}</td>
                @foreach ($nilai['nilai'] as $nl)
                    <td class="ctr">{{ $nl['nilai'] ?? '-' }}</td>
                    <td class="ctr">{{ $nl['deskripsi'] ?? '-' }}</td>
                @endforeach
            </tr>
        @endforeach
    </table>
    <p align="right" style="font-size: 10pt; font-style: italic;">Rendered in 0.1299</p>
</body>

</html>
