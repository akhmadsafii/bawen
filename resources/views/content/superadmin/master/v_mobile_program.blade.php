@extends('content.admin.master.v_data_master')
@section('content_master')
    <style>
        .pace {
            display: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div class="row justify-content-around">
                @foreach ($mobile as $mb)
                    <div class="col-md-3 col-4" style="min-height: 60px">
                        <a href="javascript:void(0)" class="detail-mobile" data-id="{{ $mb['id'] }}">
                            <div class="info-box p-1"
                                style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                <div class="info-box-content p-1 bg-success">
                                    <center id="loader_{{ $mb['id'] }}">
                                        <i class="far fa-user fa-4x my-2"></i>
                                        <h3 class="box-title my-0 text-white">{{ $mb['role'] }}</h3>
                                        <p>{!! str_limit($mb['keterangan'], 50) !!}</p>
                                    </center>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formProgram">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12" id="dataForm">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formProgram', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('admin-update_program_mobile') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formProgram').trigger("reset");
                            $('#ajaxModel').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.detail-mobile', function() {
                var id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin-detail_program_mobile') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $('#loader_' + id).html(
                            '<i class="fa fa-spin fa-spinner fa-3x"></i>');
                    },
                    success: function(data) {
                        var str = data.keterangan;
                        if(str){
                            if (str.length > 50) str = str.substring(0, 50);
                        }else{
                            str = '';
                        }
                        $('#loader_' + id).html(
                            '<i class="far fa-user fa-4x my-2"></i> <h3 class="box-title my-0 text-white">' +
                            data.role + '</h3><p>' + str +
                            '</p>');
                        $('#modelHeading').html("DETAIL ROLE " + data.role.toUpperCase());
                        $('#dataForm').html(data.isi);
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
