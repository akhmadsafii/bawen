@extends('content.admin.master.v_data_master')
@section('content_master')
    <style>
        .pace {
            display: none;
        }

    </style>
        <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Link</th>
                                    <th>Deskripsi</th>
                                    <th>File</th>
                                    <th>Urutan</th>
                                    <th>Tampilkan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_program">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Program</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                                <div class="form-group" id="kode_program" style="display: none">
                                    <label for="name" class="col-sm-12 control-label">Kode Program</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kode" name="kode">
                                        <small class="text-danger">* merubah isi dapat menyebabkan gangguan
                                            sistem</small>
                                    </div>
                                </div>
                                <div id="tampilLink">
                                    <div class="form-group">
                                        <small for="name" class="col-sm-12 control-label"><a data-toggle="collapse"
                                                href="#links" role="button" aria-expanded="false"
                                                aria-controls="collapseExample">Upload dari sumber lain?</a></small>
                                    </div>
                                    <div id="links" class="collapse">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Link Program</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" placeholder="https://mysch.my.id"
                                                    id="link" name="link">
                                                <small class="text-danger">* merubah isi dapat menyebabkan gangguan
                                                    sistem</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Deskripsi</label>
                                    <div class="col-sm-12">
                                        <textarea name="deskripsi" id="deskripsi" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-0 ml-0">
                                    <label for="name" class="col-sm-12 control-label">Gambar</label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.program_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin-update_status_program') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            var table = $('#data-tabel').DataTable({
                buttons: [{
                    text: 'Tambah Data',
                    className: 'btn btn-sm btn-facebook',
                    attr: {
                        title: 'Tambah Data',
                        id: 'createNewCustomer'
                    }
                }, ],
                paging: false,
                searching: false,
                processing: true,
                serverSide: true,
                responsive: true,
                dom: `rt`,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'link',
                        name: 'link'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'sort_order',
                        name: 'sort_order'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Program");
                $('#kode_program').hide();
                $('#tampilLink').show();
                $('#saveBtn').attr('disabled', false);
                $('#ajaxModel').modal('show');
                $('#delete_foto').html('');
                $('#action').val('Add');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('admin-store_program') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('admin-update_program') }}";
                    method_url = "PUT";
                }
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        $("#saveBtn").attr("disabled", false);
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin-edit_program') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data  Program");
                        $('#id_program').val(data.id);
                        $('#id_kategori').val(data.id_kategori_program).trigger('change');
                        $('#nama').val(data.nama);
                        $('#deskripsi').val(data.deskripsi);
                        $('#kode').val(data.kode);
                        $('#link').val(data.link);
                        if (data.status == 1) {
                            $('#saveBtn').attr('disabled', false);
                            $('#kode_program').show();
                            $('#tampilLink').hide();
                        } else if (data.status == 2) {
                            $('#saveBtn').attr('disabled', false);
                            $('#kode_program').hide();
                            var element = document.getElementById("links");
                            element.classList.add("show")
                            $('#tampilLink').show();
                        } else {
                            $('#saveBtn').attr('disabled', true);
                            $('#kode_program').show();
                            $('#tampilLink').hide();
                        }
                        $('#old_image').val(data.old_image);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "program/trash/" + id,
                            type: "POST",
                            data: {
                                '_method': 'DELETE'
                            },
                            beforeSend: function() {
                                $(".delete-" + id).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    data.message,
                                    'success'
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    'Proses Penghapusan Gagal :)',
                                    'error'
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }

        function updateSort(id, params) {
            console.log(id);
            console.log(params);
            let loader = $(this);
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "{{ route('admin-update_sort_program') }}",
                data: {
                    id,
                    params
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(data) {
                    // if (data.status == 'berhasil') {
                    $('#data-tabel').dataTable().fnDraw(false);
                    // }
                    swa(data.status + "!", data.message, data.icon);

                }
            });
        }




        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }
    </script>
@endsection
