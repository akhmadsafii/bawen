<!DOCTYPE html>
<html>

<head>
    <title>Cetak Kelompok Prakerin</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px rgb(0, 0, 0);
            padding: 3px;
            font-size: 12px
        }

        .no_border tr td,
        .no_border tr th {
            border: none !important;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: middle;
            text-align: center
        }

        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center style="border-bottom: 4px solid">
        <h1 style="margin: 2px">DINAS PENDIDIKAN DAN KEBUDAYAAN</h1>
        <h1 style="margin: 2px">{{ strtoupper($kaprodi['sekolah']) }}</h1>
        <h3 style="margin: 2px">{{ $kaprodi['alamat_sekolah'] }}</h3>
    </center>
    <center style="margin-top: 30px;">
        <h3 style="margin-bottom: 0; margin-top: 20px">{{ Str::upper($kelompok['nama']) }}</h3>
        <h3 style="margin: 0">JURUSAN {{ Str::upper($kaprodi['jurusan']) }}</h3>
        <table class="table" style="width: 100%; border: 1px solid">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">Tujuan/Tempat PKL</th>
                    <th colspan="2">Periode PKL</th>
                    <th rowspan="2">Guru Pembimbing</th>
                </tr>
                <tr>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                </tr>
            </thead>
            <tbody>
                @if (empty($kelompok['anggota']))
                    <tr>
                        <td colspan="6" style="text-align: center">Belum ada anggota yang terdaftar</td>
                    </tr>
                @else
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($kelompok['anggota'] as $agt)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ Str::upper($agt['nama']) }}</td>
                            <td>{{ Str::upper($agt['industri']) }}</td>
                            <td>{{ (new \App\Helpers\Help())->getTanggal($agt['tgl_mulai']) }}</td>
                            <td>{{ (new \App\Helpers\Help())->getTanggal($agt['tgl_selesai']) }}</td>
                            <td>{{ Str::upper($agt['pemb_sekolah']) }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <br><br>
        <table style="width: 100%; margin-bottom: 20px">
            <tr>
                <td style="width: 60%"></td>
                <td style="text-align: center; width: 40%">
                    <div class="tempat_paraf" style="width: 300px">
                        <p style="margin: 0; color: {{ $kaprodi['kabupaten_sekolah'] != null ? 'black' : 'red' }}">
                            {{ $kaprodi['kabupaten_sekolah'] != null ? Str::ucfirst($kaprodi['kabupaten_sekolah']) : 'Kota/Kabupaten Sekolah belum diset oleh admin' }}, <br>
                            {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}</p>
                        <br><br><br><br><br>
                        {{ Str::upper($kaprodi['nama']) }} <br>
                        KA. JURUSAN
                    </div>
                </td>
            </tr>
        </table>
    </center>

</body>

</html>
