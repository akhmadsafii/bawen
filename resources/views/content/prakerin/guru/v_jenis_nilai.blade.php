@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .profile-card {
            background: linear-gradient(to bottom, rgba(39, 170, 225, .8), rgba(28, 117, 188, .8));
            background-size: cover;
            width: 100%;
            min-height: 90px;
            border-radius: 4px;
            padding: 10px 20px;
            color: #fff;
            margin-bottom: 40px;
        }

        .active {
            background: #f2f4f8;
        }

        .profile-card img.profile-photo {
            border: 7px solid #fff;
            float: left;
            margin-right: 20px;
            position: relative;
            top: -30px;
            height: 70px;
            width: 70px;
            border-radius: 50%;
        }

        .profile-card h5 a {
            font-size: 15px;
        }

        .profile-card a {
            font-size: 12px;
        }

        /*Newsfeed Links CSS*/

        ul.nav-news-feed {
            padding-left: 20px;
            padding-right: 20px;
            margin: 0 0 40px 0;
            background: #fff;
            padding-top: 20px;
        }

        ul.nav-news-feed li {
            list-style: none;
            display: block;
            padding: 15px 0;
        }

        ul.nav-news-feed li div {
            position: relative;
            margin-left: 30px;
        }

        ul.nav-news-feed li div::after {
            content: "";
            width: 100%;
            height: 1px;
            border-top: 1px solid #f1f2f2;
            position: absolute;
            bottom: -15px;
            left: 0;
        }

        ul.nav-news-feed li a {
            color: #6d6e71;
        }

        ul.nav-news-feed li i {
            font-size: 18px;
            margin-right: 15px;
            float: left;
        }

        ul.nav-news-feed i.icon1 {
            color: #8dc63f;
        }

        ul.nav-news-feed i.icon2 {
            color: #662d91;
        }

        ul.nav-news-feed i.icon3 {
            color: #ee2a7b;
        }

        ul.nav-news-fee i.icon4 {
            color: #f7941e;
        }

        ul.nav-news-fee i.con5 {
            color: #1c75bc;
        }

        ul.nav-news-feed i.icon6 {
            color: #9e1f63;
        }

        /*Chat Block CSS*/

        #chat-block {
            margin: 0 0 40px 0;
            text-align: center;
            background: #fff;
            padding-top: 20px;
        }

        #chat-block .title {
            background: #8dc63f;
            padding: 2px 20px;
            width: 70%;
            height: 30px;
            border-radius: 15px;
            position: relative;
            margin: 0 auto 20px;
            color: #fff;
            font-size: 14px;
            font-weight: 600;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        ul.online-users li {
            list-style: none;
            position: relative;
            margin: 3px auto !important;
            padding-left: 2px;
            padding-right: 2px;
        }

        ul.online-users li span.online-dot {
            background: linear-gradient(to bottom, rgba(141, 198, 63, 1), rgba(0, 148, 68, 1));
            border: none;
            height: 12px;
            width: 12px;
            border-radius: 50%;
            position: absolute;
            bottom: -6px;
            border: 2px solid #fff;
            left: 0;
            right: 0;
            margin: auto;
        }

        img.profile-photo {
            height: 58px;
            width: 58px;
            border-radius: 50%;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        .list-inline {
            padding-left: 0;
            margin-left: -5px;
            list-style: none;
        }

        .list-inline>li {
            display: inline-block;
            padding-right: 5px;
            padding-left: 5px;
        }

        .text-white {
            color: #fff;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-md-12 my-2">
            <div class="alert alert-info border-info" role="alert">
                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                        aria-hidden="true">×</span>
                </button>
                <p><strong>Detail Siswa:</strong>
                </p>
                <table class="w-100">
                    <tr>
                        <th>Industri</th>
                        <td>{{ $siswa['industri'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Alamat Industri</th>
                        <td>{{ $siswa['alamat_industri'] }}</td>
                    </tr>
                    <tr>
                        <th>Pembimbing Industri</th>
                        <td>{{ $siswa['pemb_industri'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Pembimbing Sekolah</th>
                        <td>{{ $siswa['pemb_sekolah'] }}</td>
                    </tr>
                    <tr>
                        <th>Nama Siswa</th>
                        <td>{{ $siswa['nama'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Email</th>
                        <td>{{ $siswa['email'] }}</td>
                    </tr>
                    <tr>
                        <th>NIS</th>
                        <td>{{ $siswa['nis'] }}</td>
                        <td></td>
                        <td></td>
                        <th>NISN</th>
                        <td>{{ $siswa['nisn'] }}</td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>{{ $siswa['alamat'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Telepon</th>
                        <td>{{ $siswa['telepon'] }}</td>
                    </tr>
                    <tr>
                        <th>Rombel</th>
                        <td>{{ $siswa['rombel'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Kelas</th>
                        <td>{{ $siswa['kelas'] }}</td>
                    </tr>
                    <tr>
                        <th>Jurusan</th>
                        <td>{{ $siswa['jurusan'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Sekolah</th>
                        <td>{{ $siswa['sekolah'] }}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Mulai</th>
                        <td>{{ $siswa['tgl_mulai'] }}</td>
                        <td></td>
                        <td></td>
                        <th>Tanggal Selesai</th>
                        <td>{{ $siswa['tgl_selesai'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-4 static">
            <div class="profile-card">
                <img src="{{ $siswa['file'] }}" alt="user" class="profile-photo">
                <h5><a href="#" class="text-white">{{ ucwords($siswa['nama']) }}</a></h5>
                <a href="#" class="text-white"><i class="fa fa-user"></i> NISN {{ $siswa['nisn'] }}</a>
            </div>
            <ul class="nav-news-feed">
                @foreach ($jenis as $jn)
                    <li onclick="return lihatJenis({{ $jn['id'] }},'nilai')" class="">
                        <i class="fa fa-list-alt icon1"></i>
                        <div>
                            <a href="javascript:void(0)">{{ $jn['nama'] }}</a>
                        </div>
                    </li>
                @endforeach
                <li>
                    <a href="{{ route('pkl_nilai_siswa-show_sertifikat', (new \App\Helpers\Help())->encode($siswa['id'])) }}"
                        target="_blank">
                        <i class="fa fa-list-alt icon1"></i>
                        <div>
                            Sertifikat
                        </div>
                    </a>
                </li>
            </ul>
            <div id="chat-block">
                <div class="title">Siswa Yang lain</div>
                <ul class="online-users list-inline">
                    @foreach ($siswas as $ssw)
                        <li>
                            <a href="{{ url('program/sistem_pkl/nilai_siswa/jenis', $ssw['id_code']) }}"
                                title="Linda Lohan"><img src="{{ $ssw['file'] }}" alt="user"
                                    class="img-responsive profile-photo">
                                <span class="online-dot"></span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            <form action="javascript:void(0)" name="formAddNilai" id="formAddNilai" method="post">
                <input type="hidden" name="id_siswa" value="{{ $siswa['id'] }}">
                <div class="wadah_nilai">
                    <div class="alert alert-warning border-warning" style="width: 100%" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p><strong>Harap mengklik dulu tombol disamping, untuk membuka halaman ini:</strong>
                        </p>
                        <ul class="mr-t-10">
                            <li>Paraf => Memberikan paraf &amp; mencetak(print) kegiatan siswa</li>
                            <li>Detail => Melihat detail Siswa Prakerin</li>
                            <li>Guru => Melihat pembimbing sekolah dari siswa prakerin</li>
                            <li>Kegiatan => Melihat agenda siswa dan catatan dari dari pembimbing sekolah</li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.nav-news-feed li').on('click', function() {
            $('li.active').removeClass('active');
            $(this).addClass('active');
        });

        function lihatJenis(id_jenis, params) {
            if (params == 'nilai') {
                action_url = "{{ route('pkl_nilai_siswa-nilai_by_jenis') }}";
            }

            if (params == 'sertifikat') {
                action_url = "{{ route('pkl_nilai_siswa-lihat_nilai') }}";
            }
            $.ajax({
                type: 'POST',
                url: action_url,
                data: {
                    id_jenis: id_jenis,
                    id: "{{ $siswa['id'] }}",
                    params
                },
                beforeSend: function() {
                    $(".wadah_nilai").html(
                        '<div id="loading" style="" ></div>');
                },
                success: function(data) {
                    console.log(data);
                    $('.wadah_nilai').html(data);
                }
            });
        }

        $('#formAddNilai').on('submit', function(event) {
            event.preventDefault();
            $("#saveNilai").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveNilai").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_nilai_siswa-create') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    noti(data.success, data.message);
                    $('#saveNilai').html('Simpan');
                    $("#saveNilai").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>


@endsection
