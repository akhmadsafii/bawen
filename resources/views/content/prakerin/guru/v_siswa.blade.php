@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-md-6 col-12 mb-2">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        </div>
                        <div class="col-md-6 col-12 mb-2">
                            <div class="pull-right">
                                <button id="addData" class="btn btn-outline-info mt-1"><i class="fas fa-plus-circle"></i>
                                    Tambah</button>
                                <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                                    Import</button>
                                <button id="data_trash" class="btn btn-outline-danger mt-1"><i
                                        class="fas fa-trash-restore"></i>
                                    Trash</button>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9"></div>
                        <div class="col-sm-3 col-md-3">
                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    @php
                                        $serc = str_replace('-', ' ', $search);
                                    @endphp
                                    <input type="text" value="{{ $serc }}" id="search" name="search"
                                        class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                            class="btn btn-info"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>Rombel</th>
                                        <th>Jurusan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_siswa">
                                    @if (!empty($siswa))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $sw)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $sw['nama_siswa'] }}</td>
                                                <td>{{ $sw['nis'] }}</td>
                                                <td>{{ $sw['nama_rombel'] }}</td>
                                                <td>{{ $sw['nama_jurusan'] }}</td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)"
                                                        class="btn btn-info btn-sm accordion-toggle" data-toggle="collapse"
                                                        data-target="#demo{{ $sw['id'] }}"><i
                                                            class="fas fa-info-circle"></i></a>
                                                    <a href="{{ route('pkl_nilai_siswa-jenis', (new \App\Helpers\Help())->encode($sw['id'])) }}" class="delete btn btn-sm btn-success" >Masukan Nilai <i
                                                            class="fas fa-arrow-circle-right"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                                        <table class="table">
                                                            <tr>
                                                                <th>Nama</th>
                                                                <td>{{ $sw['nama_siswa'] }}</td>
                                                                <td></td>
                                                                <th>Email</th>
                                                                <td>{{ $sw['nama_siswa'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>NIS</th>
                                                                <td>{{ $sw['nis'] }}</td>
                                                                <td></td>
                                                                <th>Jurusan</th>
                                                                <td>{{ $sw['nama_jurusan'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Industri</th>
                                                                <td>{{ $sw['industri'] }}</td>
                                                                <td></td>
                                                                <th>Pembimbing Industri</th>
                                                                <td>{{ $sw['nama_pembimbing_industri'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Pembimbing Sekolah</th>
                                                                <td>{{ $sw['nama_pembimbing_sekolah'] }}</td>
                                                                <td></td>
                                                                <th>Kelompok Prakerin</th>
                                                                <td>{{ $sw['nama_kelompok'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Mulai Prakerin</th>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_mulai']) }}</td>
                                                                <td></td>
                                                                <th>Selesai Prakerin</th>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_selesai']) }}</td>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="6">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {!! $pagination !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{-- <div class="widget-bg">
        <div class="table-responsive">
            <p></p>
            <table class="table table-striped- table-bordered table-hover" id="table-data">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>NIS</th>
                        <th>NISN</th>
                        <th>Rombel</th>
                        <th>Jurusan</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody id="data-siswa">
                    <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td> <button data-toggle="collapse" data-target="#demo1"
                                class="btn btn-sm btn-success accordion-toggle"><i class="fas fa-info-circle"></i></button>
                        </td>
                    </tr>
                    <tr>
                    <tr>
                        <td colspan="7" class="hiddenRow">
                            <div class="accordian-body collapse" id="demo1">
                                <table class="table table-striped">
                                    <tr class="bg-secondary">
                                        <th class="text-center">Jenis</th>
                                        <th class="text-center">Nilai</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center vertical-middle" rowspan="3">
                                            <h3 class="box-title">Pelaksanaan</h3>
                                        </td>
                                        <td class="text-center">
                                            <table class="table table-borderd">
                                                <thead>
                                                    <tr class="bg-secondary">
                                                        <th class="text-center">No</th>
                                                        <th class="text-center">Nama Nilai</th>
                                                        <th class="text-center">Masukan Nilai</th>
                                                        <th class="text-center">Opsi</th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td class="text-center">1</td>
                                                    <td class="text-center">2</td>
                                                    <td width="200px" class="text-center"><input type="text" name="nilai"
                                                            id="" class="form-control"></td>
                                                    <td width="200px" class="vertical-middle text-center" rowspan="3">
                                                        <button class="btn btn-success">Simpan</button></td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td width="200px"><input type="text" name="nilai" id=""
                                                            class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td width="200px"><input type="text" name="nilai" id=""
                                                            class="form-control"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </td>
                    </tr>';
                    </tr>
                </tbody>
            </table>
        </div>
    </div> --}}
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                if (id_jurusan) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id_jurusan
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Memproses data kelas...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">--Pilih Kelas--</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Memproses data Rombel...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_nilai_siswa-get_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        })
    </script>
@endsection
