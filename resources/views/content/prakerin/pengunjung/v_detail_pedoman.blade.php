@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <style>
        .footer {
            position: relative;
        }

    </style>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12" id="wadah_industri">
                <div class="widget-holder">
                    <div class="widget-bg">
                        <h2 class="box-title text-uppercase">{{ $pedoman['nama'] }}</h2>
                        <hr>
                        <small class="text-danger">*Harap matikan Internet Download Manager bila preview tidak bisa di
                            buka</small>
                        <object data="{{ $pedoman['file'] }}" type="application/pdf" width="100%" height="800px">
                            <p>It appears you don't have a PDF plugin for this browser.
                                No biggie... you can <a href="{{ $pedoman['file'] }}">click here to
                                    download the PDF file.</a></p>
                        </object>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
