@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <style>
        .carousel-item {
            height: 100vh;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .carousel-caption {
            position: absolute;
            top: 25%;
            left: 0;
            right: 0;
            bottom: 0;
            transform: translateY(-32%);
            text-align: center;
            max-height: 30vh;
        }

        .shadow {
            color: #D5E2D6;
            text-align: center;
            text-shadow: 1px 1px #1e262d, 2px 2px #1e262d, 3px 3px #1e262d, 4px 4px #1e262d, 5px 5px #1e262d, 6px 6px #1e262d, 7px 7px #1e262d, 8px 8px #1e262d;
        }

    </style>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            @php
                $first_data = reset($slider);
                array_shift($slider);
            @endphp
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active" style="background-image: url({{ !empty($first_data['file']) ?  $first_data['file'] : 'https://hrdspot.com/wp-content/uploads/job-description.jpg'  }})">
                    <div class="carousel-caption d-none d-md-block">
                    <img src="{{ !empty($config) ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <h3 class="shadow text-light my-0">{{ !empty($first_data['keterangan']) ? $first_data['keterangan'] : "Sistem PKL"  }}</h3>
                        <h2 class="shadow text-light display-4">{{ !empty($first_data['judul']) ? $first_data['judul'] : $sekolah['nama']  }}</h2>
                    </div>
                </div>
                @foreach ($slider as $sd)
                    <div class="carousel-item"
                        style="background-image: url({{ $sd['file'] }})">
                        <div class="carousel-caption d-none d-md-block">
                            <img src="{{ !empty($config) ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                            <h3 class="shadow text-light my-0">{{ $sd['keterangan'] }}</h3>
                            <h2 class="shadow text-light display-4">{{ $sd['judul'] }}</h2>
                        </div>
                    </div>
                @endforeach

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
@endsection
