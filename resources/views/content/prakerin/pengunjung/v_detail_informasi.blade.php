@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <style>
        .footer {
            position: relative;
        }

    </style>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12" id="wadah_industri">
                <div class="widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <div class="accordion accordion-minimal" id="accordion1" role="tablist"
                                aria-multiselectable="true">
                                <div class="d-flex mb-4 mt-2">
                                    <div class="mr-3"><i class="fa fa-file-text-o fs-24 text-info"></i>
                                    </div>
                                    <div>
                                        <h5 class="mt-0 mb-1 fs-22 fw-400 sub-heading-font-family text-uppercase">
                                            {{ $informasi['judul'] }}
                                        </h5>
                                        <small>Lorem ipsum dolor sit amet.</small>
                                    </div>
                                </div>
                                <div class="card">
                                    <textarea name="isi" id="isi" class="form-control"
                                        rows="10">{{ $informasi['isi'] }}</textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
        integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        tinymce.init({
            selector: "textarea",
            width: '100%',
            height : "700",
            readonly: 1,
            menubar: false,
            statusbar: false,
            toolbar: false
        });
    </script>

@endsection
