@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Sebaran Lulusan</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('bursa_kerja-beranda') }}">Bursa Kerja</a>
                </li>
                <li class="breadcrumb-item active">Sebaran Lulusan</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row widget-bg">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-primary">
                        <div class="text-center">
                            <h3 style="color:#2C3E50"><span class="fa fa-info-circle"></span> AGENDA</h3>
                            <h4> <label for="Choose Report" style="color:#E74C3C">AGENDA KEGIATAN PRAKERIN</label></h4>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="115px">19 Oktober 2018</th>
                                        <th class="text-center" width="115px">19 Oktober 2018</th>
                                        <th class="text-center" width="115px">19 Oktober 2018</th>
                                        <th class="text-center" width="115px">Date</th>
                                        <th class="text-center" width="115px">Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td class="text-center" width="150px">1</td>
                                        <td class="text-center" width="150px">Mai Ahmed</td>
                                        <td class="text-center" width="150px">500</span></td>
                                        <td class="text-center" width="150px">3/12/2016</span></td>
                                        <td class="text-center" width="150px">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">2</td>
                                        <td class="text-center">Mai Ahmed</td>
                                        <td class="text-center">60</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Appoitment</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Ahmed Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Ahmed Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                    <tr>

                                        <td class="text-center">3</td>
                                        <td class="text-center">Waled</td>
                                        <td class="text-center">180</span></td>
                                        <td class="text-center">3/12/2016</span></td>
                                        <td class="text-center">Prescription</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <h4> <label style="color:#E74C3C" for="Total">Total :</label>7740</h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endsection
