@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <style>
        .footer {
            position: relative;
        }


        .search {
            float: none;
            position: relative;
            box-shadow: 0 0 40px rgba(51, 51, 51, .1)
        }

        .search input {
            height: 60px;
            text-indent: 25px;
            border: 2px solid #d6d4d4
        }

        .search input:focus {
            box-shadow: none;
            border: 2px solid blue
        }

        .search .fa-search {
            position: absolute;
            top: 20px;
            left: 16px
        }

        .search button {
            position: absolute;
            top: 5px;
            right: 5px;
            height: 50px;
            width: 110px;
            background: blue
        }

        .icon-container {
            border-radius: 7px
        }

        .stars i {
            margin-right: 2px;
            color: red;
            font-size: 13px
        }

        .rating-number {
            font-size: 20px;
            font-weight: 700;
            margin-bottom: 2px
        }

        .number-ratings {
            font-size: 12px
        }

        .listing-title {
            margin-bottom: -7px
        }

        .progress-bar {
            background: green
        }

        .progress {
            height: 16px
        }

        .tags span {
            margin-right: 9px;
            border: 1px solid green;
            padding-right: 9px;
            padding-left: 9px;
            padding-top: 2px;
            padding-bottom: 4px;
            border-radius: 7px;
            background-color: green;
            color: #fff
        }


        .search-result h3 {
            margin-bottom: 0;
            color: #1E0FBE;
        }

        .search-result .search-link {
            color: #006621;
        }

        .search-result p {
            font-size: 12px;
            margin-top: 5px;
        }

        .hr-line-dashed {
            border-top: 1px dashed #E7EAEC;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin: 20px 0;
        }

    </style>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="row height d-flex justify-content-center align-items-center animated fadeInDown">
                    <div class="col-md-8">
                        <form action="javascript:void(0)" id="search_industri" name="search_industri">
                            <div class="search row">
                                <input type="text" name="nama" class="form-control col-md-10 col-12"
                                    placeholder="Masukan nama perusahaan" required>
                                <div class="col-md-2 col-12">
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="wadah_industri">

                @if (!empty($industri))
                    @foreach ($industri as $ind)
                        <div class="bg-white p-3 rounded mt-2">
                            <div class="row">
                                <div class="col-md-3" style="display: flex; align-items: center; justify-content: center;">
                                    <div>
                                        <img src="{{ $ind['file'] }}" alt="" style="width: 50%">
                                    </div>
                                </div>
                                <div class="col-md-6 border-right">
                                    <div class="listing-title">
                                        <h5>{{ $ind['nama'] }}</h5>
                                    </div>
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="level mr-2"><span>Lokasi:</span><span class="font-weight-bold">&nbsp;
                                                Temanggung</span>
                                        </div>
                                        <div class="level mr-1"><span>Telepon:</span><span class="font-weight-bold">&nbsp;
                                                {{ $ind['telepon'] }}</span>
                                        </div>
                                    </div>
                                    <div class="description">
                                        <p class="mb-0"><strong>Alamat perusahaan :</strong></p>
                                        <p>{{ $ind['alamat'] != null ? $ind['alamat'] : '-' }}</p>
                                    </div>
                                </div>
                                <div class="d-flex col-md-3"
                                    style="display: flex; align-items: center; justify-content: center;">
                                    <div class="d-flex flex-column justify-content-start user-profile w-100">
                                        <div class="d-flex user"><img class="rounded-circle"
                                                src="http://localhost/smart_school/public/images/default.png" width="50">
                                            <div class="about ml-1"><span
                                                    class="d-block text-black font-weight-bold">{{ $ind['pimpinan'] != null ? ucwords($ind['pimpinan']) : '-' }}</span><span>Pimpinan</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <hr>
                    <div class="hr-line-dashed"></div>
                    <div class="search-result">
                        <h3><a href="#">Tidak ada Data Industri</a></h3>
                        <a href="#" class="search-link">Data industri saat ini belum tersedia</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#search_industri').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ route('pkl_industri-search_name') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    // console.log(data);
                    $('#wadah_industri').html(data);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>
@endsection
