@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .circle-tile {
            margin-bottom: 15px;
            text-align: center;
        }

        .circle-tile-heading {
            border: 3px solid rgba(255, 255, 255, 0.3);
            border-radius: 100%;
            color: #FFFFFF;
            height: 80px;
            margin: 0 auto -40px;
            position: relative;
            transition: all 0.3s ease-in-out 0s;
            width: 80px;
        }

        .circle-tile-heading .fa {
            line-height: 80px;
        }

        .circle-tile-content {
            padding-top: 50px;
        }

        .circle-tile-number {
            font-size: 26px;
            font-weight: 700;
            line-height: 1;
            padding: 5px 0 15px;
        }

        .circle-tile-description {
            text-transform: uppercase;
        }

        .circle-tile-footer {
            background-color: rgba(0, 0, 0, 0.1);
            color: rgba(255, 255, 255, 0.5);
            display: block;
            padding: 5px;
            transition: all 0.3s ease-in-out 0s;
        }

        .circle-tile-footer:hover {
            background-color: rgba(0, 0, 0, 0.2);
            color: rgba(255, 255, 255, 0.5);
            text-decoration: none;
        }

        .circle-tile-heading.dark-blue:hover {
            background-color: #2E4154;
        }

        .circle-tile-heading.green:hover {
            background-color: #138F77;
        }

        .circle-tile-heading.orange:hover {
            background-color: #DA8C10;
        }

        .circle-tile-heading.blue:hover {
            background-color: #2473A6;
        }

        .circle-tile-heading.red:hover {
            background-color: #CF4435;
        }

        .circle-tile-heading.purple:hover {
            background-color: #7F3D9B;
        }

        .tile-img {
            text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.9);
        }

        .dark-blue {
            background-color: #34495E;
        }

        .green {
            background-color: #16A085;
        }

        .blue {
            background-color: #2980B9;
        }

        .orange {
            background-color: #F39C12;
        }

        .red {
            background-color: #E74C3C;
        }

        .purple {
            background-color: #8E44AD;
        }

        .dark-gray {
            background-color: #7F8C8D;
        }

        .gray {
            background-color: #95A5A6;
        }

        .light-gray {
            background-color: #BDC3C7;
        }

        .yellow {
            background-color: #F1C40F;
        }

        .text-dark-blue {
            color: #34495E;
        }

        .text-green {
            color: #16A085;
        }

        .text-blue {
            color: #2980B9;
        }

        .text-orange {
            color: #F39C12;
        }

        .text-red {
            color: #E74C3C;
        }

        .text-purple {
            color: #8E44AD;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }





        .profile-card {
            background: linear-gradient(to bottom, rgba(39, 170, 225, .8), rgba(28, 117, 188, .8));
            background-size: cover;
            width: 100%;
            min-height: 90px;
            border-radius: 4px;
            padding: 10px 20px;
            color: #fff;
            /* margin-bottom: 40px; */
        }

        .active {
            background: #f2f4f8;
        }

        .profile-card img.profile-photo {
            border: 7px solid #fff;
            float: left;
            margin-right: 20px;
            position: relative;
            top: -30px;
            height: 70px;
            width: 70px;
            border-radius: 50%;
        }

        .profile-card h5 a {
            font-size: 15px;
        }

        .profile-card a {
            font-size: 12px;
        }

        /*Newsfeed Links CSS*/

        ul.nav-news-feed {
            padding-left: 20px;
            padding-right: 20px;
            margin: 0 0 40px 0;
            background: #fff;
            padding-top: 20px;
        }

        ul.nav-news-feed li {
            list-style: none;
            display: block;
            padding: 15px 0;
        }

        ul.nav-news-feed li div {
            position: relative;
            margin-left: 30px;
        }

        ul.nav-news-feed li div::after {
            content: "";
            width: 100%;
            height: 1px;
            border-top: 1px solid #f1f2f2;
            position: absolute;
            bottom: -15px;
            left: 0;
        }

        ul.nav-news-feed li a {
            color: #6d6e71;
        }

        ul.nav-news-feed li i {
            font-size: 18px;
            margin-right: 15px;
            float: left;
        }

        ul.nav-news-feed i.icon1 {
            color: #8dc63f;
        }

        ul.nav-news-feed i.icon2 {
            color: #662d91;
        }

        ul.nav-news-feed i.icon3 {
            color: #ee2a7b;
        }

        ul.nav-news-fee i.icon4 {
            color: #f7941e;
        }

        ul.nav-news-fee i.con5 {
            color: #1c75bc;
        }

        ul.nav-news-feed i.icon6 {
            color: #9e1f63;
        }

        /*Chat Block CSS*/

        #chat-block {
            margin: 0 0 40px 0;
            text-align: center;
            background: #fff;
            padding-top: 20px;
        }

        #chat-block .title {
            background: #8dc63f;
            padding: 2px 20px;
            width: 70%;
            height: 30px;
            border-radius: 15px;
            position: relative;
            margin: 0 auto 20px;
            color: #fff;
            font-size: 14px;
            font-weight: 600;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        ul.online-users li {
            list-style: none;
            position: relative;
            margin: 3px auto !important;
            padding-left: 2px;
            padding-right: 2px;
        }

        ul.online-users li span.online-dot {
            background: linear-gradient(to bottom, rgba(141, 198, 63, 1), rgba(0, 148, 68, 1));
            border: none;
            height: 12px;
            width: 12px;
            border-radius: 50%;
            position: absolute;
            bottom: -6px;
            border: 2px solid #fff;
            left: 0;
            right: 0;
            margin: auto;
        }

        img.profile-photo {
            height: 58px;
            width: 58px;
            border-radius: 50%;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        .list-inline {
            padding-left: 0;
            margin-left: -5px;
            list-style: none;
        }

        .list-inline>li {
            display: inline-block;
            padding-right: 5px;
            padding-left: 5px;
        }

        .text-white {
            color: #fff;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-md-4 static">
            <div class="profile-card">
                <img src="{{ $siswa['file'] }}" alt="user" class="profile-photo">
                <h5><a href="#" class="text-white">{{ ucwords($siswa['nama']) }}</a></h5>
                <a href="#" class="text-white"><i class="fa fa-user"></i> NISN {{ $siswa['nisn'] }}</a>
            </div>
            <ul class="nav-news-feed">
                <li class="" onclick="return lihatJenis({{ $siswa['id'] }}, 'nilai')">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="javascrip:void(0)">Nilai Siswa</a>
                    </div>
                </li>
                <li class="" onclick="return lihatJenis({{ $siswa['id'] }}, 'sertifikat')">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="javascrip:void(0)">Sertifikat Siswa</a>
                    </div>
                </li>
                <li class="" onclick="return lihatJenis({{ $siswa['id'] }}, 'tugas')">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="javascript:void(0)">Laporan Kegiatan Siswa</a>
                    </div>
                </li>

            </ul>
        </div>
        <div class="col-md-8">
            <form action="javascript:void(0)" name="formAddNilai" id="formAddNilai" method="post">
                <div class="wadah_nilai">
                    <div class="alert alert-info border-info" style="width: 100%" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p><strong>Harap mengklik dulu tombol disamping, untuk membuka halaman ini:</strong>
                        </p>
                        <ul class="mr-t-10">
                            <li>Paraf => Memberikan paraf &amp; mencetak(print) kegiatan siswa</li>
                            <li>Detail => Melihat detail Siswa Prakerin</li>
                            <li>Guru => Melihat pembimbing sekolah dari siswa prakerin</li>
                            <li>Kegiatan => Melihat agenda siswa dan catatan dari dari pembimbing sekolah</li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.nav-news-feed li').on('click', function() {
            $('li.active').removeClass('active');
            $(this).addClass('active');
        });

        function lihatJenis(id, params) {
            console.log("lihat jenis");
            if (params == 'nilai') {
                action_url = "{{ route('pkl_nilai_siswa-lihat_nilai') }}";
            }

            if (params == 'tugas') {
                action_url = "{{ route('pkl_nilai_siswa-lihat_tugas') }}";
            }

            if (params == 'sertifikat') {
                action_url = "{{ route('pkl_nilai_siswa-lihat_nilai') }}";
            }
            $.ajax({
                type: 'POST',
                url: action_url,
                data: {
                    id,
                    params
                },
                success: function(data) {
                    $('.wadah_nilai').html(data);
                }
            });
        }

        $('#formAddNilai').on('submit', function(event) {
            event.preventDefault();
            $("#saveNilai").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveNilai").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_nilai_siswa-create') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    noti(data.success, data.message);
                    $('#saveNilai').html('Simpan');
                    $("#saveNilai").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>


@endsection
