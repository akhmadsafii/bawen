@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .list-inline>li {
            display: inline-block;
            padding-right: 5px;
            padding-left: 5px;
        }


        /*==================================================
      Friend List CSS = Newsfeed and Timeline
      ==================================================*/

        .friend-list .friend-card {
            border-radius: 4px;
            border-bottom: 1px solid #f1f2f2;
            overflow: hidden;
            margin-bottom: 20px;
        }

        .friend-list .friend-card .card-info {
            padding: 0 20px 10px;
        }

        .friend-list .friend-card .card-info img.profile-photo-lg {
            margin-top: -60px;
            border: 7px solid #fff;
        }

        img.profile-photo-lg {
            height: 80px;
            width: 80px;
            border-radius: 50%;
        }

        .text-green {
            color: #8dc63f;
        }

    </style>
    <div class="friend-list">
        <div class="row">
            {{-- {{ dd($siswa) }} --}}
            @foreach ($siswa as $ssw)
            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/6495ED" alt="profile-cover" class="img-responsive cover" style="width: 100%">
                    <div class="card-info widget-bg">
                        <img src="{{ $ssw['file'] }}" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="{{ url('program/sistem_pkl/nilai_siswa/jenis', $ssw['id_code']) }}" class="pull-right btn btn-info btn-sm" style="">Lihat nilai</a>
                            <h5><a href="timeline.html" class="profile-link">{{ ucwords($ssw['nama']) }}</a></h5>
                            <p>{{ $ssw['industri'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
{{-- 
            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/008B8B" alt="profile-cover" class="img-responsive cover">
                    <div class="card-info">
                        <img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="#" class="pull-right text-green">My Friend</a>
                            <h5><a href="timeline.html" class="profile-link">Sophia Lee</a></h5>
                            <p>Student at Harvard</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/9932CC" alt="profile-cover" class="img-responsive cover">
                    <div class="card-info">
                        <img src="https://bootdey.com/img/Content/avatar/avatar5.png" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="#" class="pull-right text-green">My Friend</a>
                            <h5><a href="timeline.html" class="profile-link">Sophia Lee</a></h5>
                            <p>Student at Harvard</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/228B22" alt="profile-cover" class="img-responsive cover">
                    <div class="card-info">
                        <img src="https://bootdey.com/img/Content/avatar/avatar4.png" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="#" class="pull-right text-green">My Friend</a>
                            <h5><a href="timeline.html" class="profile-link">Sophia Lee</a></h5>
                            <p>Student at Harvard</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/20B2AA" alt="profile-cover" class="img-responsive cover">
                    <div class="card-info">
                        <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="#" class="pull-right text-green">My Friend</a>
                            <h5><a href="timeline.html" class="profile-link">Sophia Lee</a></h5>
                            <p>Student at Harvard</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="friend-card">
                    <img src="https://via.placeholder.com/400x100/FF4500" alt="profile-cover" class="img-responsive cover">
                    <div class="card-info">
                        <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="user" class="profile-photo-lg">
                        <div class="friend-info">
                            <a href="#" class="pull-right text-green">My Friend</a>
                            <h5><a href="timeline.html" class="profile-link">Sophia Lee</a></h5>
                            <p>Student at Harvard</p>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

@endsection
