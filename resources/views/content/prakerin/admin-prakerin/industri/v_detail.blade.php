@extends('content.prakerin.admin-prakerin.industri.v_main')
@section('content_industri')
    <form id="formIndustri">
        <div class="form-group row">
            <input type="hidden" name="id" value="{{ $industri['id'] }}">
            <label class="col-md-3 col-form-label" for="l0">Nama Industri</label>
            <div class="col-md-9">
                <input class="form-control" id="nama" name="nama" placeholder="Nama" type="text"
                    value="{{ $industri['nama'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Email</label>
            <div class="col-md-9">
                <input class="form-control" id="email" name="email" placeholder="Email" type="email"
                    value="{{ $industri['email'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Telepon</label>
            <div class="col-md-9">
                <input class="form-control" id="telepon" name="telepon" placeholder="Telepon" type="text"
                    value="{{ $industri['telepon'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Pimpinan</label>
            <div class="col-md-9">
                <input class="form-control" id="pimpinan" name="pimpinan" placeholder="Pimpinan" type="text"
                    value="{{ $industri['pimpinan'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Provinsi</label>
            <div class="col-md-9">
                <select name="id_provinsi" id="id_provinsi" class="form-control">
                    <option value="">PILIH PROVINSI..</option>
                    @foreach ($provinsi as $pr)
                        <option value="{{ $pr['id'] }}" {{ $industri['id_provinsi'] == $pr['id'] ? 'selected' : '' }}>
                            {{ ucwords($pr['name']) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Kabupaten</label>
            <div class="col-md-9">
                <select name="id_kabupaten" id="id_kabupaten" class="form-control" disabled>
                    <option value="">PILIH KABUPATEN..</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Alamat</label>
            <div class="col-md-9">
                <textarea name="alamat" id="alamat" rows="3" class="form-control">{{ $industri['alamat'] }}</textarea>
            </div>
        </div>
        <div class="form-actions btn-list text-center">
            <button class="btn btn-danger" type="reset"><i class="fas fa-broom"></i> Bersihkan</button>
            <button class="btn btn-info" id="saveBtn" type="submit"><i class="fas fa-sync"></i> Perbarui</button>
        </div>

    </form>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var provinsi = "{{ $industri['id_provinsi'] }}";
            var kabupaten = "{{ $industri['id_kabupaten'] }}";

            $('body').on('submit', '#formIndustri', function(e) {
                e.preventDefault();
                $('#saveBtn').html('<i class="fa fa-spin fa-sync"></i> Updating..');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_industri-update') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $('select[name="id_provinsi"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $('select[name="id_kabupaten"]').attr('disabled', 'disabled')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('get_kabupaten-provinsi') }}",
                        dataType: "json",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('select[name="id_kabupaten"]').html(
                                '<option value="">MEMPROSES KABUPATEN...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">PILIH KABUPATEN..</option>';
                            $.each(data, function(k, v) {
                                s += '<option value="' + v.id + '">' + v.name +
                                    '</option>';
                            })
                            $('select[name="id_kabupaten"]').removeAttr('disabled');
                            $('select[name="id_kabupaten"]').html(s)
                        }
                    });
                }
            })

            if (provinsi && kabupaten) {
                edit_kabupaten(provinsi, kabupaten);
            }

        })

        function edit_kabupaten(id_provinsi, id_kabupaten) {
            $("#id_kabupaten").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_industri-load_kabupaten_by_industri') }}",
                type: "POST",
                data: {
                    id_provinsi,
                    id_kabupaten
                },
                beforeSend: function() {
                    $('#id_kabupaten').html('<option value="">Memproses Kabupaten..</option>');
                },
                success: function(fb) {
                    $('#id_kabupaten').html('');
                    $('#id_kabupaten').html(fb);
                    $("#id_kabupaten").attr("disabled", false);
                }
            });
            return false;
        }
    </script>
@endsection
