@extends('content.prakerin.admin-prakerin.industri.v_main')
@section('content_industri')
    <div class="table-responsive">
        <table class="table table-bordered" id="data-tabel">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jurusan</th>
                    <th>Tahun Ajaran</th>
                    <th>Jumlah Anggota</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalAdmin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAdmin">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id_kelompok">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Kelompok</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-user-friends"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Lengkap"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Tahun Ajar</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="far fa-calendar-alt"></i>
                                    </span>
                                    <select name="id_tahun_ajar" id="id_tahun_ajar" class="form-control">
                                        <option value="">Pilih Tahun Ajaran..</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ $th['id'] }}"
                                                {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleSiswa">Daftar Anggota</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped widget-status-table" id="data-detail">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>NISN</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Rombel</th>
                                    <th>Jurusan</th>
                                    <th>Tgl Prakerin</th>
                                    <th>Pembimbing Industri</th>
                                    <th>Pembimbing Sekolah</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'addData'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'tahun_ajar',
                        name: 'tahun_ajar',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'anggota',
                        name: 'anggota',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'vertical-middle text-center'
                    },
                ]
            });

            $('#addData').click(function() {
                $('#formAdmin').trigger("reset");
                $('#modelHeading').html("Tambah Kelompok");
                $('#action').val('Add');
                $('#modalAdmin').modal('show');
            });

            $('#formAdmin').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_kelompok-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_kelompok-update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize() + "&id_industri={{ $_GET['k'] }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formAdmin').trigger("reset");
                            $('#modalAdmin').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_kelompok-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Kelompok");
                        $('#id_kelompok').val(data.id);
                        $('#nama').val(data.nama);
                        $('#id_tahun_ajar').val(data.id_tahun_ajaran);
                        $('#action').val('Edit');
                        $('#modalAdmin').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete-pembimbing', function() {
                var id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_kelompok-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                var oTable = $('#data-tabel').dataTable();
                                oTable.fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).data('id');
                var nama = $(this).data('nama');
                let loader = $(this);
                if (id) {
                    $('#data-detail').DataTable().destroy();
                    fill_detail(id);
                    $('#modalDetail').modal('show');
                }
            });
        });

        function fill_detail(id_kelompok = '') {
            var table = $('#data-detail').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('pkl_kelompok-get_anggota_datatable') }}",
                    type: "POST",
                    data: {
                        id_kelompok
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'email',
                        name: 'email',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'pemb_industri',
                        name: 'pemb_industri',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'pemb_sekolah',
                        name: 'pemb_sekolah',
                        className: 'vertical-middle'
                    },
                ]
            });
        }
    </script>
@endsection
