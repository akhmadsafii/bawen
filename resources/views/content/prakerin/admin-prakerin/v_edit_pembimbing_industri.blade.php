@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == "university")
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        img.rounded-circle.mt-2 {
            border: 3px solid #ADB5BE;
            padding: 5px;
            width: 140px;
            height: 140px;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT PEMBIMBING INDUSTRI</h5>
        </div>
        <div class="page-title-right">
            <a href=" route('user-siswa') " class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Pembimbing Industri</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <img src="{{ $pembimbing['file'] }}" id="image_preview" class="rounded-circle mt-2"
                                    alt="User avatar">
                                <h4 class="my-2 font-weight-bold">{{ ucwords($pembimbing['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id=""><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $pembimbing['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-success btn-block" id="btnReset"
                                                data-id="{{ $pembimbing['id'] }}"><i class="fa fa-pencil"></i> Edit
                                                Password
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#tentang"
                                        data-toggle="tab">Tentang</a>
                                </li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#detail"
                                        data-toggle="tab">Detail</a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#paraf"
                                        data-toggle="tab">Paraf</a></li>
                            </ul>
                            <div class="aksi">
                                <button type="reset" class="btn btn-danger"><i class="fas fa-sync-alt"></i> Reset</button>
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Simpan</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tentang">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $pembimbing['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Industri</label>
                                        <div class="col-md-9">
                                            <select name="industri" id="industri" class="form-control">
                                                <option value="">Pilih Industri..</option>
                                                @foreach ($industri as $ind)
                                                    <option value="{{ $ind['id'] }}"
                                                        {{ $ind['id'] == $pembimbing['id_industri'] ? 'selected' : '' }}>
                                                        {{ $ind['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nama</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama"
                                                placeholder="Nama Pembimbing" type="text"
                                                value="{{ $pembimbing['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Telepon</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="telepon" name="telepon"
                                                placeholder="Telepon Pembimbing" type="number"
                                                value="{{ $pembimbing['telepon'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="detail">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                placeholder="Tempat Lahir" type="text"
                                                value="{{ $pembimbing['tempat_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                placeholder="Tanggal Lahir" type="date"
                                                value="{{ $pembimbing['tgl_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jenis Kelamin</label>
                                        <div class="col-md-9">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="" disabled>Pilih Jenis Kelamin..</option>
                                                <option value="l" {{ $pembimbing['jenkel'] == 'l' ? 'selected' : '' }}>
                                                    Laki - laki</option>
                                                <option value="p" {{ $pembimbing['jenkel'] == 'p' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat</label>
                                        <div class="col-md-9">
                                            <input type="text" name="alamat" id="alamat"
                                                value="{{ $pembimbing['alamat'] }}" placeholder="Alamat"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="paraf">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l16">Paraf</label>
                                        <div class="col-md-9">
                                            <img id="modal-preview"
                                                src="{{ $pembimbing['paraf'] != null ? $pembimbing['paraf'] : 'https://via.placeholder.com/150' }}"
                                                alt="Preview" class="form-group mb-1" width="150" style="margin-top: 10px">
                                            <br>
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <br><small class="text-muted">Pilih paraf dari pembimbing industri</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $pembimbing['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formEdit', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);

                let formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_pembimbing_industri-update') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });

            // $('#formEdit').on('submit', function(event) {
            //     event.preventDefault();
            //     $("#saveBtn").html(
            //         '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
            //     $("#saveBtn").attr("disabled", true);
            //     $.ajax({
            //         url: " route('update-siswa_user') ",
            //         method: "POST",
            //         data: $(this).serialize(),
            //         dataType: "json",
            //         success: function(data) {
            //             $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
            //             $("#saveBtn").attr("disabled", false);
            //             swa(data.status + "!", data.message, data.icon);
            //         },
            //         error: function(data) {
            //             console.log('Error:', data);
            //             $('#saveBtn').html('Simpan');
            //         }
            //     });
            // });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pkl_pembimbing_industri-reset_password') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                let formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_pembimbing_industri-upload_profile') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#image_preview').attr('src', data.image);
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_pembimbing_industri-del_profile') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#image_preview').attr('src', data.image);
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });

            $(document).on('click', '#btnReset', function() {
                $('#formReset').trigger("reset");
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_pembimbing_industri-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: function(data) {
                        // console.log(data);
                        $(loader).html('<i class="fa fa-pencil"></i> Edit Username / Password');
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });

            });
        })
    </script>
@endsection
