@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="widget-bg">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('pkl_siswa-beranda') }}" class="btn btn-warning"> <i class="fas fa-arrow-circle-left"></i>
                    Kembali</a>
                <h3 class="box-title">{{ Session::get('title') }}</h3>
                <hr>
                <form action="javascript:void(0)" id="formSearchSiswa" method="post">
                    <div class="row p-3 rounded">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Jurusan</label>
                                <select name="id_jurusan" id="id_jurusan" class="form-control">
                                    <option value="">--Pilih Jurusan--</option>
                                    @foreach ($jurusan as $jr)
                                        <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">
                                            {{ $jr['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Kelas</label>
                                <select name="id_kelas" id="id_kelas" class="form-control" disabled>
                                    <option value="">Pilih Kelas..</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Rombel</label>
                                <select name="id_rombel" id="id_rombel" class="form-control" disabled>
                                    <option value="">Pilih Rombel..</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">&nbsp;</label>
                                <input type="submit" class="btn btn-success btn-block border border-light"
                                    value="Mulai Cari..">
                            </div>
                        </div>
                    </div>
                </form>

                <div style="width: 100%;">
                    <p></p>
                    <form action="javascript:void(0)" id="formSiswa" onsubmit="addSiswa(this)" method="post">
                        {{-- <form action="javascript:void(0)" id="formSiswa'.$sw['id'].'" onsubmit="addSiswa(this)" method="post"> --}}
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>NISN</th>
                                        <th>Rombel</th>
                                        <th>Jurusan</th>
                                        <th>
                                            <div class="form-check m-0">
                                                <input class="form-check-input mx-0" type="checkbox" id="checkAll">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Pilih Semua
                                                    </label>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="data-siswa">
                                    <tr>
                                        <td colspan="7" class="text-center">Data akan terbuka bila sudah memilih rombel
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
            $('select[name="id_jurusan"]').on('change', function() {
                let id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Memproses data kelas...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                            $("#id_rombel").attr("disabled", true);
                            $("#id_rombel").html('<option value="">Pilih Rombel..</option>');
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Memproses data Rombel...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih Rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_siswa-get_kelas_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });

        function addSiswa(obj) {
            $("#addPrakerinSiswa").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#addPrakerinSiswa").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_siswa-save_siswa') }}",
                method: "POST",
                data: $("#" + obj.id).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        window.location.replace(data.url);
                    }
                    $("#addPrakerinSiswa").html('Tambahkan');
                    $("#addPrakerinSiswa").attr("disabled", false);
                    swa(data.status + "!", data.message, data.icon);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }

        function getPembimbing(id_industri) {
            let id = id_industri.value;
            let id_siswa = $(id_industri).data('id');
            if (id) {
                $.ajax({
                    url: "{{ route('pkl_pembimbing_industri-by_industri') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $('#pembimbing_industri' + id_siswa).html(
                            '<option value="">Memproses data pembimbing...</option>');
                    },
                    success: function(data) {
                        if (!$.trim(data)) {
                            $('#pembimbing_industri' + id_siswa).html(
                                '<option value="">--- No Pembimbing Found ---</option>');
                        } else {
                            var s = '<option value="">Pilih Pembimbing Industri</option>';
                            // data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row
                                    .nama +
                                    '</option>';

                            })
                            $('#pembimbing_industri' + id_siswa).removeAttr('disabled');
                        }
                        $('#pembimbing_industri' + id_siswa).html(s);
                        $.ajax({
                            url: "{{ route('pkl_kelompok-by_industri') }}",
                            type: "POST",
                            data: {
                                id
                            },
                            beforeSend: function() {
                                $('#kelompok' + id_siswa).html(
                                    '<option value="">Memproses data...</option>');
                            },
                            success: function(response) {
                                if (!$.trim(response)) {
                                    $('#kelompok' + id_siswa).html(
                                        '<option value="">--- No Kelompok ---</option>');
                                } else {
                                    var input = '<option value="">Pilih Kelompok</option>';
                                    // data = JSON.parse(data);
                                    response.forEach(function(rows) {
                                        input += '<option value="' + rows.id + '">' + rows
                                            .nama +
                                            '</option>';

                                    })
                                    $('#kelompok' + id_siswa).removeAttr('disabled');
                                }
                                $('#kelompok' + id_siswa).html(input);
                            }
                        });
                    }
                });
            }
        }
    </script>
@endsection
