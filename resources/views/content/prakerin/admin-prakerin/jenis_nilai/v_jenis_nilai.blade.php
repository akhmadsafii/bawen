@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th class="text-center">Jurusan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="addModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Tambah Jenis</h5>
                </div>
                <form id="formJenis" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jurusan</label>
                                    <div class="col-sm-12">
                                        <select name="id_jurusan[]" id="id_jurusan" class="form-control select2" multiple>
                                            <option value="Pilih Jurusan">Pilih Jurusan..</option>
                                            @foreach ($jurusan as $jr)
                                                <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Jenis Nilai</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="nama[]" id="nama" value="" required>
                                    </div>
                                </div>
                                <input type="hidden" name="old_name" id="old_name">
                                <div class="fomAddJenis"></div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="addBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama_jenis',
                        name: 'nama_jenis'
                    },
                    {
                        data: 'jumlah',
                        name: 'jumlah',
                        className: 'text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $("#id_jurusan").val('').trigger('change')
                $('#formJenis').trigger("reset");
                $('.modal-title').html("Tambah Jenis Nilai");
                $('.tambahBaris').show('');
                $('.fomAddJenis').html('');
                $('#addModal').modal('show');
                $('#action').val('Add');
            });

            $('#formJenis').on('submit', function(event) {
                $("#addBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Sending..');
                $("#addBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_jenis_nilai-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_jenis_nilai-update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formJenis').trigger("reset");
                            $('#addModal').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#addBtn').html('Simpan');
                        $("#addBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#addBtn').html('Simpan');
                        $("#addBtn").attr("disabled", false);
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let nama = $(this).data('nama');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_jenis_nilai-by_name') }}",
                    data: {
                        nama
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        // console.log(data);
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('.modal-title').html("Edit Jenis Nilai");
                        $('#nama').val(nama);
                        $('#old_name').val(nama);
                        $('.tambahBaris').hide('');
                        $('.fomAddJenis').html('');
                        var array_jurusan = [];
                        $.each(data, function(k, v) {
                            array_jurusan.push(v.id_jurusan);
                        });
                        $('#id_jurusan').val(array_jurusan);
                        $('#id_jurusan').trigger('change');

                        $('#action').val('Edit');
                        $('#addModal').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let nama = $(this).data('nama');
                let loader = $(this);
                // alert(jenis);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_jenis_nilai-delete') }}",
                        type: "POST",
                        data: {
                            nama
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })

            });
        });

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddJenis').append(
                ' <div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama Jensi Nilai</label><div class="col-sm-12"><input type="text" class="form-control" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
