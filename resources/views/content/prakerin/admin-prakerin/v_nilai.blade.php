@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-3">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                        <div class="col-md-9">
                            <form class="form-inline float-right">
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Pilih Jurusan</label>
                                <select class="custom-select my-1 mr-sm-2" id="id_jurusan" name="id_jurusan">
                                    <option value="" selected>Pilih Jurusan...</option>
                                    @foreach ($jurusan as $jr)
                                        <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                    @endforeach
                                </select>
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Pilih Jenis</label>
                                <select class="custom-select my-1 mr-sm-2" id="id_jenis" name="id_jenis" disabled>
                                    <option value="" selected>Pilih Jenis...</option>
                                </select>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Nilai</th>
                                    <th>Jenis</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalNilai" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formNilai" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_nilai">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" value="" required>
                                    </div>
                                </div>
                                <div class="fomAddNilai"></div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id_jenis = '';
            fill_jenis();
            $('select[name="id_jurusan"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('pkl_jenis_nilai-by_jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_jenis"]').html(
                                    '<option value="">-- Tidak ada jenis --</option>'
                                );
                                $('select[name="id_jenis"]').prop('disabled', true);
                            } else {
                                var s = '<option value="">Pilih Jenis..</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama + '</option>';

                                })
                                $('select[name="id_jenis"]').prop('disabled', false);
                            }
                            $('select[name="id_jenis"]').html(s)
                        }
                    });
                } else {
                    $('select[name="id_jenis"]').html(
                        '<option value="">Pilih Jenis..</option>'
                    );
                    $('select[name="id_jenis"]').prop('disabled', true);
                }
            })

            $('select[name="id_jenis"]').on('change', function() {
                id_jenis = $(this).val();
                // console.log(id_jenis);
                if (id_jenis) {
                    $('#data-tabel').DataTable().destroy();
                    fill_jenis(id_jenis);
                }
            })



            $(document).on('click', '#createNewCustomer ', function() {
                let jenis = $('#id_jenis').val();
                if (id_jenis) {
                    $('#formNilai').trigger("reset");
                    $('#jenis').val(id_jenis);
                    $('#modelHeading').html("Tambah Data Nilai");
                    $('#modalNilai').modal('show');
                    $('#action').val('Add');
                } else {
                    alert('Harap memilih jenis nilai dahulu');
                }
            });



            // $('#searchJurusan').on('submit', function(event) {
            //     $('#btnFilter').html('Sending..');
            //     event.preventDefault();
            //     $.ajax({
            //         url: "{{ route('jenis_nilai-searh_jurusan') }}",
            //         method: "POST",
            //         data: $(this).serialize(),
            //         dataType: "json",
            //         success: function(data) {
            //             // console.log(data);
            //             $('#btnFilter').html('Pencarian..');
            //             $('#data_jenis').html(data);
            //         },
            //         error: function(data) {
            //             console.log('Error:', data);
            //             $('#saveBtn').html('Simpan');
            //         }
            //     });
            // });

            $('#formNilai').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                console.log(id_jenis);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_nilai-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_nilai-update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize() + "&id_jenis=" + id_jenis,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formNilai').trigger("reset");
                            $('#modalNilai').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddNilai').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_nilai-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Nilai Prakerin");
                        $('#id_nilai').val(data.id);
                        $('#nama').val(data.nama);
                        $('#jenis').val(data.id_jenis_nilai);
                        $('#action').val('Edit');
                        $('#modalNilai').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_nilai-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });





        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddNilai').append(
                ' <div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        function fill_jenis(id = '') {
            // console.log(id);
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('jenis_nilai-searh_by_jenis_datatable') }}",
                    type: "POST",
                    data: {
                        id
                    }
                },
                buttons: [{
                    text: '<i class="fas fa-plus"></i>',
                    className: 'btn btn-sm btn-facebook',
                    attr: {
                        title: 'Tambah Data',
                        id: 'createNewCustomer'
                    }
                }, ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'vertical-middle'
                    },
                ]
            });
        }
    </script>
@endsection
