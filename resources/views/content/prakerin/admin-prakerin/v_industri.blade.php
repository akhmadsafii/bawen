@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Pimpinan</th>
                                        <th>Telepon</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <form id="CustomerForm">
                <div class="modal-content">
                    <div class="modal-header text-inverse bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="modelHeading"></h5>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_industri">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Nama</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" name="nama" id="nama" autocomplete="off" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" name="telepon" autocomplete="off" id="telepon"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Pimpinan</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" autocomplete="off" name="pimpinan" id="pimpinan"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Pelamar</h5>
                </div>
                <form id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons text-info" style="font-size: 4.5rem;">file_download</i><br>
                                        <b class="text-info">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" style="cursor: pointer"
                                            class="text-success">
                                            <i class="material-icons text-success">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        text: '<i class="fa fa-upload"></i>',
                        className: 'btn btn-sm',
                        attr: {
                            title: 'Import Data',
                            id: 'import'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'pimpinan',
                        name: 'pimpinan'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'alamat',
                        name: 'alamat'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('body').on('click', '#import', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Perusahaan');
            });

            $('#createNewCustomer').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Industri");
                $('#ajaxModel').modal('show');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                $('#saveBtn').html('Sending..');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_industri-create') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_industri-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_industri').html(data.industri);
                                var oTable = $('#data-tabel').dataTable();
                                oTable.fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("formImport"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-industri_prakerin') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        }
                        swa(data.status + "!", data.message, data.icon);
                    }
                });
            });
        });
    </script>
@endsection
