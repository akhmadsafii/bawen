@extends('content.prakerin.admin-prakerin.pembimbing.v_main')
@section('content_pembimbing')
    <form id="formParaf">
        <center>
            <input type="hidden" name="id" value="{{ (new \App\Helpers\Help())->decode($_GET['k']) }}">
            <img id="modal-preview"
                src="{{ $pembimbing['paraf'] != null ? $pembimbing['paraf'] : 'https://via.placeholder.com/150' }}"
                alt="Preview" class="form-group" width="300" style="margin-top: 10px">
            <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);" class="form-control-file">
            <button type="submit" class="btn btn-success my-2" id="saveBtn"><i class="fas fa-sync"></i> Perbarui</button>
        </center>
    </form>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('body').on('submit', '#formParaf', function(e) {
                e.preventDefault();
                $('#saveBtn').html('<i class="fa fa-spin fa-sync"></i> Updating..');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_pembimbing_industri-upload_paraf') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
