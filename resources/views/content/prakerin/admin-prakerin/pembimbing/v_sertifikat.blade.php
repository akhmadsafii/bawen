@extends('content.prakerin.admin-prakerin.pembimbing.v_main')
@section('content_pembimbing')
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-weight: 200;
        }

        h1 {
            text-align: center;
            padding-bottom: 10px;
            border-bottom: 2px solid #2fcc71;
            max-width: 40%;
            margin: 20px auto;
        }

        /* CONTAINERS */

        .container {
            max-width: 850px;
            width: 100%;
            margin: 0 auto;
        }

        .four {
            width: 32.26%;
            max-width: 32.26%;
        }


        /* COLUMNS */

        .col {
            display: block;
            float: left;
            margin: 1% 0 1% 1.6%;
        }

        .col:first-of-type {
            margin-left: 0;
        }

        /* CLEARFIX */

        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }

        .cf:after {
            clear: both;
        }

        .cf {
            *zoom: 1;
        }

        /* FORM */

        .form .plan input,
        .form .payment-plan input,
        .form .payment-type input {
            display: none;
        }

        .form label {
            position: relative;
            color: #fff;
            background-color: #aaa;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid transparent;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .form .plan input:checked+label,
        .form .payment-plan input:checked+label,
        .form .payment-type input:checked+label {
            border: 3px solid #333;
            background-color: #2fcc71;
        }

        .form .plan input:checked+label:after,
        form .payment-plan input:checked+label:after,
        .form .payment-type input:checked+label:after {
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #2fcc71;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
        }

        .submit {
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }

        .submit:hover {
            cursor: pointer;
            transform: rotateX(360deg);
        }

    </style>
    <div class="container">
        <form class="form cf" id="formSertifikat">
            <section class="plan cf">
                <h2 class="my-0">Choose a Template:</h2>
                @php
                    $no = 1;
                @endphp
                @foreach ($sertifikat as $srt)
                    <input type="radio" name="id_template" id="free_{{ $no }}" value="{{ $srt['id'] }}"
                        {{ !empty($serind) && $serind['id_template'] == $srt['id'] ? 'checked' : '' }}>
                    <label class="free_{{ $no }}-label four col"
                        for="free_{{ $no }}">{{ ucwords($srt['nama']) }}</label>
                    @php
                        $no++;
                    @endphp
                @endforeach
            </section>
            <input class="submit" type="submit" id="saveBtn" value="Submit">
        </form>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formSertifikat').on('submit', function(event) {
                event.preventDefault();
                $('#saveBtn').val('Submiting..');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pembimbing_sertifikat-set_sertifikat') }}",
                    method: "POST",
                    data: $(this).serialize() + "&id_pembimbing={{ $_GET['k'] }}",
                    dataType: "json",
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('#saveBtn').val('Submit');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').val('Submit');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
