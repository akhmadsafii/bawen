@extends('content.prakerin.admin-prakerin.pembimbing.v_main')
@section('content_pembimbing')
    <form id="formIndustri">
        <div class="form-group row">
            <input type="hidden" name="id" value="{{ $pembimbing['id'] }}">
            <label class="col-md-3 col-form-label" for="l0">Industri</label>
            <div class="col-md-9">
                <select name="id_industri" id="id_industri" class="form-control select3">
                    <option value="">Pilih Industri..</option>
                    @foreach ($industri as $ind)
                        <option value="{{ $ind['id'] }}"
                            {{ $ind['id'] == $pembimbing['id_industri'] ? 'selected' : '' }}>{{ $ind['nama'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Nama Pembimbing</label>
            <div class="col-md-9">
                <input class="form-control" id="nama" name="nama" placeholder="Nama" type="text"
                    value="{{ $pembimbing['nama'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Email</label>
            <div class="col-md-9">
                <input class="form-control" id="email" name="email" placeholder="Email" type="email"
                    value="{{ $pembimbing['email'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Telepon</label>
            <div class="col-md-9">
                <input class="form-control" id="telepon" name="telepon" placeholder="Telepon" type="text"
                    value="{{ $pembimbing['telepon'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Jenkel</label>
            <div class="col-md-9">
                <select name="jenkel" id="jenkel" class="form-control">
                    <option value="l" {{ $pembimbing['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                    <option value="p" {{ $pembimbing['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                    value="{{ $pembimbing['tempat_lahir'] }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
            <div class="col-md-9">
                <input type="text" class="form-control datepicker" name="tanggal_lahir" id="tanggal_lahir" readonly
                    value="{{ date('d-m-Y', strtotime($pembimbing['tgl_lahir'])) }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 col-form-label" for="l0">Alamat</label>
            <div class="col-md-9">
                <textarea name="alamat" id="alamat" rows="3" class="form-control">{{ $pembimbing['alamat'] }}</textarea>
            </div>
        </div>
        <div class="form-actions btn-list text-center">
            <button class="btn btn-danger" type="reset"><i class="fas fa-broom"></i> Bersihkan</button>
            <button class="btn btn-info" id="saveBtn" type="submit"><i class="fas fa-sync"></i> Perbarui</button>
        </div>

    </form>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('body').on('submit', '#formIndustri', function(e) {
                e.preventDefault();
                $('#saveBtn').html('<i class="fa fa-spin fa-sync"></i> Updating..');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('pkl_pembimbing_industri-update') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-sync"></i> Perbarui');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
