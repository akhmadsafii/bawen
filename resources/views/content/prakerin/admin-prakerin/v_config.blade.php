@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="row">
        <div class="content" style="width: 100%; padding: 0 31px;">
            <div class="container-fluid">
                <form action="javascript:void(0)" name="formConfig" id="formConfig" enctype="multipart/form-data">
                    @csrf
                    <div class="row mt-60 widget-bg">
                        <div class="col-md-12">
                            <h4 class="dark-grey m-0">Website</h4>
                            <hr>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Title Halaman Awal</label>
                                <div class="col-md-7">
                                    <input type="hidden" name="" id="action" value="{{ $aksi }}">
                                    @if ($aksi == 'edit')
                                        <textarea name="judul" id="judul" class="form-control"
                                            rows="3">{{ $config['judul'] }}</textarea>
                                        <input type="hidden" name="id" value="{{ $config['id'] }}">
                                    @else
                                        <textarea name="judul" id="judul" class="form-control" rows="3"></textarea>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Fav Icon</label>
                                <div class="col-md-7">
                                    <div class="row">
                                        @if ($aksi == 'edit')
                                            <div class="col-md-2">
                                                <img id="modal-preview" src="{{ $config['fav_icon'] }}" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            @php
                                                $fav = explode('/', $config['fav_icon']);
                                            @endphp
                                            @if (end($fav) != 'bkk-fav.png')
                                                <div class="col-md-6" style="position: relative">
                                                    <div id="delete_foto" style="position: absolute; bottom: 0"> <input
                                                            type="checkbox" name="remove_photo[]" value="file" /> Remove
                                                        photo
                                                    </div>
                                                </div>
                                            @endif

                                        @else
                                            <div class="col-md-2">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        @endif
                                    </div>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    <small>
                                        <p>Gunakan file PNG Transparan, width dan height sama, misal 64px x 64px</p>
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Logo Aplikasi</label>
                                <div class="col-md-7">
                                    <div class="row">
                                        @if ($aksi == 'edit')
                                            <div class="col-md-2">
                                                <img id="modal-logo" src="{{ $config['logo'] }}" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            @php
                                                $logo = explode('/', $config['logo']);
                                            @endphp
                                            @if (end($logo) != 'bkk-logo.png')
                                                <div class="col-md-6" style="position: relative">
                                                    <div id="delete_logo" style="position: absolute; bottom: 0"><input
                                                            type="checkbox" name="remove_photo[]" value="file1" /> Remove
                                                        photo</div>
                                                </div>
                                            @endif

                                        @else
                                            <div class="col-md-2">
                                                <img id="modal-logo" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_logo" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        @endif
                                    </div>
                                    <input id="image" type="file" name="image1" accept="image/*" onchange="readLogo(this);">
                                    <small>
                                        <p>Gunakan file PNG Transparan, Lebar maksimal 150px, maksimal 300kb. Minimal 50px x
                                            50px, Tipe file JPG, JPEG, dan PNG</p>
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Footer</label>
                                <div class="col-md-7">
                                    @if ($aksi == 'edit')
                                        <textarea name="footer" id="footer" class="form-control"
                                            rows="3">{{ $config['footer'] }}</textarea>
                                    @else
                                        <textarea name="footer" id="footer" class="form-control" rows="3"></textarea>

                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10"></label>
                                <div class="col-md-7">
                                    <button type="submit" class="btn btn-primary" id="saveConfig">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function readLogo(input, id) {
            id = id || '#modal-logo';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-logo').removeClass('hidden');
                $('#start').hide();
            }
        }

        $('body').on('submit', '#formConfig', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $("#saveConfig").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveConfig").attr("disabled", true);

            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('pkl_config-create') }}",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        location.reload(true);
                    }
                    noti(data.icon, data.message);
                    $('#saveConfig').html('Simpan');
                    $("#saveConfig").attr("disabled", false);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveConfig').html('Simpan');
                }
            });
        });
    </script>

@endsection
