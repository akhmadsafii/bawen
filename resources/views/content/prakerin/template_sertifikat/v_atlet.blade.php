<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Saira+Condensed:700');

        /* @media print { */
        /* @page {
                margin: 0;
            } */

        /* body {
                margin: 1.6cm;
            } */
        /* } */

        hr {
            background-color: #be2d24;
            height: 3px;
            margin: 5px;
        }

        div#cert-footer {
            position: absolute;
            width: 96%;
            top: 466px;
            text-align: center;
        }

        div#cert-issued-by,
        div#cert-ceo-design {
            display: inline-block;
            float: right;
        }

        div#cert-ceo-design {
            margin-leftcert-ceo-design: 10%;
        }

        h1 {
            font-family: 'math';
            margin: 5px 0px;
        }

        p {
            font-family: 'Arial', sans-serif;
            font-size: 18px;
            margin: 5px 0px;
        }

        html {
            display: inline-block;
            width: 1200px;
            height: 723px;
            background: #eee url("{{ asset('asset/img/sertifikat/template2.jpg') }}") no-repeat;
            background-size: 100%;
            position: relative;
        }

        h1#cert-holder {
            font-size: 50px;
            color: #be2d24;
        }

        p.smaller {
            font-size: 22px !important;
        }

        div#cert-verify {
            opacity: 1;
            position: absolute;
            top: 680px;
            left: 60%;
            font-size: 12px;
            color: #c9c6c6;
        }

    </style>
    <div class="tulisan" style="width: 765px; height: 500px; position: absolute; left: 111px">
        <br><br>
        <center>
            <img id="cert-stamp" src="{{ $siswa['file_sekolah'] != null ? $siswa['file_sekolah'] : asset('asset/img/sma.png') }}"
                style="width: 109px; height: 109px;">

        </center>
        <br>
        <h1 style="text-align: center; width: 99%; font-size: 54px; color: #29702b">
            SERTIFIKAT
        </h1>
        <p class="smaller" id='cert-completed-line' style="text-align: center; left: 36%">
            <b>Diberikan kepada</b>
        </p>
        <br>
        <h1 id="cert-holder" style="text-align: center; font-family: cursive; font-size: 46px; left: 17%">
            {{ $siswa['nama'] != null ? strtoupper($siswa['nama']) : "FIRSTNAME LASTNAME" }}
        </h1>
        <p class="smaller" id='cert-completed-line'
            style="text-align: center; font-family: auto; margin-top: 10px">
            <b>NIS :  {{ $siswa['nis'] != null ? $siswa['nis'] : "-" }}</b>
        </p>
        <p class="smaller" id='cert-completed-line' style="text-align: center; margin-top: 10px">
            {{ $siswa['jurusan'] != null ? $siswa['jurusan'] : "-" }}
        </p>
        <p class="smaller" id='cert-completed-line' style="text-align: center; margin-top: 10px">
            <b> {{ $siswa['sekolah'] != null ? $siswa['sekolah'] : "-" }}</b>
        </p>

        <p style="font-size: larger; text-align: center">Telah melaksanakan Praktek Kerja Industri (PRAKERIN) mulai dari tanggal {{ (new \App\Helpers\Help())->getDayMonth($siswa['tgl_mulai']) }} s.d. {{ (new \App\Helpers\Help())->getTanggal($siswa['tgl_selesai']) }} tahun pelajaran {{ $siswa['tahun_ajaran'] }}
            di {{ $siswa['industri'] }}</p>
        <br><br>
        <p style="text-align: center">Pembimbing Industri</p>
        <br><br><br>
        <p style="text-align: center">{{ $siswa['pemb_industri'] }}</p>
        <p style="text-align: center">NIP. -</p>
    </div>
</body>

</html>
