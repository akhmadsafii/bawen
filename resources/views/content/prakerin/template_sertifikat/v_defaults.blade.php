<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Saira+Condensed:700');

        /* @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        } */

        hr {
            background-color: #be2d24;
            height: 3px;
            margin: 5px;
        }

        div#cert-footer {
            position: absolute;
            width: 92%;
            top: 466px;
            text-align: center;
        }

        div#cert-issued-by,
        div#cert-ceo-design {
            display: inline-block;
            float: right;
        }

        div#cert-ceo-design {
            margin-leftcert-ceo-design: 10%;
        }

        h1 {
            font-family: 'Saira Condensed', sans-serif;
            margin: 5px 0px;
        }

        p {
            font-family: 'Arial', sans-serif;
            font-size: 18px;
            margin: 5px 0px;
        }

        html {
            display: inline-block;
            width: 1024px;
            height: 723px;
            background: #eee url("{{ asset('asset/img/sertifikat/default.jpg') }}") no-repeat;
            background-size: 100%;
            position: relative;
        }

        h1#cert-holder {
            font-size: 50px;
            color: #be2d24;
        }

        p.smaller {
            font-size: 17px !important;
        }

        div#cert-verify {
            opacity: 1;
            position: absolute;
            top: 680px;
            left: 60%;
            font-size: 12px;
            color: #c9c6c6;
        }

    </style>
    <img id="cert-stamp" src="{{ $siswa['file_sekolah'] != null ? $siswa['file_sekolah'] : asset('asset/img/sma.png') }}"
        style="width: 109px; height: 109px; position: absolute; right: 88px; top: 36px;">
    <br><br><br><br>
    <br><br><br><br>
    <h1 id="cert-title" style="position: absolute; width: 99%; text-align: center; color: #2887b3">
        Diberikan Kepada
    </h1>
    <br><br><br>
    <h1 id="cert-holder" style="text-align: center;  text-decoration:underline;
    text-decoration-style: dotted;">
        {{ $siswa['nama'] != null ? strtoupper($siswa['nama']) : "FIRSTNAME LASTNAME" }}
    </h1>

    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Telah melaksanakan Praktek Kerja Industri yang bertempat di
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
        {{ $siswa['industri'] != null ? $siswa['industri'] : "NAMA INDUSTRI" }}
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Terhitung mulai dari tanggal, {{ $siswa['tgl_mulai'] != null ? (new \App\Helpers\Help())->getMonthYear($siswa['tgl_mulai']) : "TGL MULAI" }} sampai dengan {{ $siswa['tgl_selesai'] != null ? (new \App\Helpers\Help())->getMonthYear($siswa['tgl_selesai']) : "TGL MULAI" }}
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Dengan hasil <b>"Baik / Sangat Baik"</b>
    </p>

    <div id="cert-footer">
        <div id="cert-ceo-design">
            <p class="smaller" id='cert-issued' style="text-align: left">
                {{ $siswa['alamat_industri'] != null ? $siswa['alamat_industri'] : "Temanggung" }}, {{ (new \App\Helpers\Help())->getMonthYear(date('Y-m-d')) }} <br>
                Pembimbing Industri <br>
                {{ $siswa['industri'] != null ? $siswa['industri'] : "NAMA INDUSTRI" }}
            </p>
            <br><br><br><br>
            <hr>
            <p style="text-align: left">{{ $siswa['pemb_industri'] != null ? $siswa['pemb_industri'] : "PEMBIMBING INDUSTRI" }}</p>
        </div>
    </div>

    <div id="cert-verify">
        Verify at companywebsite.ai/verify/XYZ12ER56129F. <br>
        Company has confirmed the participation of this individual in the course.
    </div>



</body>

</html>
