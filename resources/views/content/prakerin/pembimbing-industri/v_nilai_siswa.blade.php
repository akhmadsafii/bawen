@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .groups__item,
        .messages {
            background-color: rgb(40 171 250);
            border-radius: 2px;
            box-shadow: 0 1px 5px rgba(0, 0, 0, .1)
        }

        @media (max-width:575px) {
            .groups {
                margin: 0 -5px
            }

            .groups [class*=col-] {
                padding: 0 5px
            }

            .groups .groups__item {
                margin-bottom: 10px
            }
        }

        .groups__item {
            text-align: center;
            padding: 2rem 1rem 1.5rem;
            margin-bottom: 30px
        }

        .groups__item:hover .actions {
            opacity: 1
        }

        .groups__item .actions {
            position: absolute;
            top: .7rem;
            right: .5rem;
            z-index: 1;
            opacity: 0
        }

        .groups__img {
            width: 6.5rem;
            display: inline-block
        }

        .groups__img .avatar-img {
            display: inline-block;
            margin: 0 1px 4px 0;
            vertical-align: top
        }

        .avatar-char,
        .avatar-img {
            border-radius: 2px;
            width: 3rem;
            height: 3rem;
            margin-right: 1.2rem;
        }

        .avatar-char {
            color: #fff;
            background-color: rgba(255, 255, 255, .08);
        }

        .avatar-char {
            line-height: 2.9rem;
            font-size: 1.2rem;
            text-align: center;
            font-style: normal;
        }

        .groups__info {
            margin-top: 1rem
        }

        .groups__info>strong {
            color: #fff;
            display: block;
            font-weight: 600
        }

        .messages,
        .messages__body {
            display: -webkit-box;
            display: -ms-flexbox;
            -webkit-box-direction: normal
        }

        .groups__info>small {
            font-size: .9rem;
            color: rgba(255, 255, 255, .7)
        }


        .toolbar {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            height: 4.5rem;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding: .05rem 2.2rem 0;
            position: relative
        }

        .toolbar:not(.toolbar--inner) {
            background-color: rgb(40 171 250);
            border-radius: 2px;
            margin-bottom: 30px;
            box-shadow: 0 1px 5px rgba(0, 0, 0, .1)
        }

        .toolbar .actions {
            margin: .05rem -.8rem 0 auto
        }

        .toolbar--inner {
            margin-bottom: 1rem;
            border-radius: 2px 2px 0 0;
            background-color: rgba(0, 0, 0, .1)
        }

        .toolbar__nav {
            white-space: nowrap;
            overflow-x: auto
        }

        .toolbar__nav>a {
            display: inline-block;
            transition: color .3s
        }

        .toolbar__nav>a+a {
            padding-left: 1rem
        }

        .toolbar__nav>a.active,
        .toolbar__nav>a:hover {
            color: rgba(255, 255, 255, .85)
        }

        .toolbar__search {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            border-radius: 2px;
            padding-left: 3rem;
            display: none;
            background-color: rgba(0, 0, 0, .96)
        }

        .toolbar__search input[type=text] {
            width: 100%;
            height: 100%;
            border: 0;
            padding: 0 1.6rem;
            font-size: 1.2rem;
            background-color: transparent;
            color: rgba(255, 255, 255, .85)
        }

        .toolbar__search input[type=text]::-webkit-input-placeholder {
            color: rgba(255, 255, 255, .5)
        }

        .toolbar__search input[type=text]:-moz-placeholder {
            color: rgba(255, 255, 255, .5)
        }

        .toolbar__search input[type=text]::-moz-placeholder {
            color: rgba(255, 255, 255, .5)
        }

        .toolbar__search input[type=text]:-ms-input-placeholder {
            color: rgba(255, 255, 255, .5)
        }

        .toolbar__search__close,
        .toolbar__search__close:hover {
            color: rgba(255, 255, 255, .85)
        }

        .toolbar__search__close {
            transition: color .3s;
            cursor: pointer;
            position: absolute;
            top: 1.5rem;
            left: 1.8rem;
            font-size: 1.5rem
        }

        .toolbar__label {
            margin: 0;
            font-size: 1.1rem;
            color: #fff;
        }

    </style>
    <div class="row">
        <div class="toolbar" style="width: 100%;">
            <div class="toolbar__label">
                {{ count($kelompok) }} Groups
            </div>

            <div class="actions">
                <i class="actions__item zmdi zmdi-search" data-sa-action="toolbar-search-open"></i>
                <a href="" class="actions__item zmdi zmdi-info-outline hidden-xs-down"></a>

                <div class="dropdown actions__item hidden-xs-down">
                    <i class="zmdi zmdi-more-vert" data-toggle="dropdown"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="" class="dropdown-item">Refresh</a>
                        <a href="" class="dropdown-item">Delete</a>
                        <a href="" class="dropdown-item">Settings</a>
                    </div>
                </div>
            </div>

            <div class="toolbar__search">
                <input type="text" placeholder="Search...">

                <i class="toolbar__search__close zmdi zmdi-long-arrow-left" data-sa-action="toolbar-search-close"></i>
            </div>
        </div>

        <div class="row groups" style="width: 100%">
            @foreach ($kelompok as $kl)
                <div class="col-xl-2 col-lg-3 col-sm-4 col-6">
                    {{-- <div class="m-1">
                        <div class="float-right m-2">
                            <a href="javascript:void(0)" data-id="{{ $kl['id'] }}"
                                class="edit btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <a href="javascript:void(0)" data-id="{{ $kl['id'] }}"
                                class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>

                        </div>
                    </div> --}}
                    <div class="groups__item">
                        <a href="javascript:void(0)" onclick="loadKelompok({{ $kl['id'] }})">
                            <div class="groups__img">
                                @foreach ($kl['anggota'] as $angg)
                                    @php
                                        $ex_file = explode('/', $angg['file']);
                                        $file = end($ex_file);
                                        $nama = Str::substr($angg['nama'], 0, 1);
                                    @endphp

                                    @if ($file != 'default.png')
                                        <img class="avatar-img" src="{{ $angg['file'] }}" alt="">
                                    @else
                                        <div class="avatar-img avatar-char">{{ ucwords($nama) }}</div>
                                    @endif
                                @endforeach
                            </div>

                            <div class="groups__info">
                                <strong>{{ $kl['nama'] }}</strong>
                                <small>{{ count($kl['anggota']) }} Contacts</small>
                            </div>
                            <div id="detail_{{ $kl['id'] }}">

                            </div>
                        </a>

                        <div class="actions">
                            <div class="dropdown actions__item">
                                <i class="zmdi zmdi-more-vert" data-toggle="dropdown"></i>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="">Edit</a>
                                    <a class="dropdown-item" href="" data-demo-action="delete-listing">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal modal-color-scheme  fade bs-modal-md-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content widget-bg">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title color-info" id="modelTitle">Info Detail</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-body">
                                    <div class="box box-info">
                                        <div class="box-body" id="profileDetail">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })


        function beriNilai(id) {
            $.ajax({
                type: 'POST',
                url: "nilai_siswa/get_nilai",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".editData-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $('#profileDetail').html(data);
                    $('#modalDetail').modal('show');
                }
            });
        }

        function loadKelompok(id) {
            $.ajax({
                type: 'POST',
                url: "peserta/kelompok",
                data: {
                    id
                },
                beforeSend: function() {
                    $("#detail_" + id).html(
                        '<p class="m-0" style="color: #fff">Mohon tunggu sebentar...</p>');
                },
                success: function(data) {
                    $("#detail_" + id).html('');
                    $('#profileDetail').html(data);
                    $('#modalDetail').modal('show');
                }
            });

        }

        $(document).on('click', '.lihatNilai', function() {
            var id = $(this).data('id');
            let loader = $(this);
            $(loader).html('<i class="fa fa-spin fa-spinner"></i> Loading');
        });
    </script>
@endsection
