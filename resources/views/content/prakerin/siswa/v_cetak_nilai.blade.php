<!DOCTYPE html>
<html>

<head>
    <title>Cetak Nilai Prakerind</title>
    @include('includes.head')

    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            // console.log(document.readyState);
            if (document.readyState == "complete") {
                window.close();
                setTimeout(() => {
                    window.close();
                }, 2000);
                // open(location, '_self').close();
                // window.location.href = "{{ url()->previous() }}";
                // window.close();
                // setTimeout(window.close, 0);
                // window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body style="background-color: #fff">
    <style>
        .pace {
            display: none;
        }

    </style>
    <table style="width: 100%; margin-bottom: 12px">
        <tr>
            <th style="text-align: center">
                <h5 class="m-0">DAFTAR NILAI PRAKTEK KERJA INDUSTRI</h5>
            </th>
        </tr>
        <tr>
            <th style="text-align: center">
                <h5 class="m-0">SEKOLAH MENENGAH KEJURUAN (SMK) DARUT TAQWA PURWOSARI</h5>
            </th>
        </tr>
        <tr>
            <th style="text-align: center">
                <h5 class="m-0">TAHUN PELAJARAN 2012/2013</h5>
            </th>
        </tr>
    </table>
    <table style="margin-bottom: 12px">
        <tr>
            <td>Nama</td>
            <td>&nbsp;&nbsp;&nbsp;: {{ ucwords($cetak['nama']) }}</td>
        </tr>
        <tr>
            <td>NISN</td>
            <td>&nbsp;&nbsp;&nbsp;: {{ $cetak['nisn'] }}</td>
        </tr>
        <tr>
            <td>Tingkat/ Program Keahlian</td>
            <td>&nbsp;&nbsp;&nbsp;:
                {{ (new \App\Helpers\Help())->getKelas($cetak['kelas']) . ' / ' . ucwords($cetak['jurusan']) }}
            </td>
        </tr>
        <tr>
            <td>Nama dunia usaha / Dunia Industri</td>
            <td>&nbsp;&nbsp;&nbsp;: AA Komputer</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>&nbsp;&nbsp;&nbsp;: AA Komputer</td>
        </tr>
        <tr>
            <td>Waktu Pelaksanaan</td>
            <td>&nbsp;&nbsp;&nbsp;:
                {{ (new \App\Helpers\Help())->getDayMonth($cetak['tgl_mulai']) . ' - ' . (new \App\Helpers\Help())->getTanggal($cetak['tgl_selesai']) }}
            </td>
        </tr>
    </table>
    @php
        $numb = 1;
    @endphp
    @foreach ($cetak['nilai'] as $jenil)
        <table class="table table-bordered" style="margin-bottom: 12px">
            <tr>
                <th colspan="3"><b>{{ (new \App\Helpers\Help())->getKelas($numb++) }}.
                        {{ $jenil['jenis_nilai'] }}</b></th>
            </tr>
            <tr>
                <th>No</th>
                <th>Aspek yang dinilai</th>
                <th>Nilai</th>
            </tr>
            @php
                $no = 1;
            @endphp
            <div style="display: none">
                {{ $total = 0 }}
            </div>
            @foreach ($jenil['nilai'] as $nl)
                <tr>
                    <td>{{ $no++ }}.</td>
                    <td>{{ ucfirst($nl['nama']) }}</td>
                    <td>{{ $nl['nilai_siswa'] }}</td>
                    <div style="display: none">
                        {{ $total += $nl['nilai_siswa'] }}
                    </div>
                </tr>
            @endforeach
            <tr>
                @php
                    $rata = $total / count($jenil['nilai']);
                @endphp
                <td colspan="2" style="text-align: center"><b>Nilai Rata-rata</b></td>
                <td><b>{{ number_format($rata, 0, '.', ',') }}</b></td>
            </tr>
        </table>
    @endforeach
    <br><br>
    <div style=" display: inline; float: left;">
        Keterangan Penilaian: <br>
        Nilai yang diburuhkan adalah nilai angka, dengan rentang sebagai berikut <br>
        <table class="table table-bordered" style="width: 50%">
            <tr>
                <td>A</td>
                <td>90 - 100</td>
            </tr>
            <tr>
                <td>B</td>
                <td>75 - 89</td>
            </tr>
            <tr>
                <td>C</td>
                <td>60 - 74</td>
            </tr>
            <tr>
                <td>D</td>
                <td>0 - 59</td>
            </tr>
        </table>
    </div>
    <div style=" display: inline; float: right;">
        Temanggung, 20 Maret 2012<br>
        Dibuat Oleh<br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <b><u>Drs. Sukarlan</u></b><br>
        Pimpinan/Pembimbing
    </div>


</body>

</html>
