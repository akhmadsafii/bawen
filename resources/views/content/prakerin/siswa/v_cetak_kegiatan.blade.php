<!DOCTYPE html>
<html>

<head>
    <title>Cetak Nilai Prakerind</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 11pt;
            width: 8.5in
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #000;
            padding: 3px;
        }

        .table tr th {
            font-weight: bold;
            text-align: center
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        .tbl {
            font-weight: bold
        }

        table tr td {
            vertical-align: top
        }

        .font_kecil {
            font-size: 12px
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            // console.log(document.readyState);
            if (document.readyState == "complete") {
                window.close();
                setTimeout(() => {
                    window.close();
                }, 2000);
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <table style="width: 96%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td rowspan="2" style="text-align: center; vertical-align: middle;"><img
                        src="https://sman1longmesangat.files.wordpress.com/2011/05/logo-sma-lm-web2.jpg" alt=""
                        style="width: 70px;"></td>
                <td rowspan="2" style="text-align: center; vertical-align: middle;">
                    <h4>LAPORAN KEGIATAN PRAKTEK KERJA
                        LAPANGAN
                </td>
                </h4>
                <td style="text-align: center; vertical-align: middle;">
                    <h3 style="margin: 0">PROGRAM STUDI ADMINISTRASI BISNIS D3
                        DEPARTEMEN ADMINISTRASI NIAGA POLITEKNIK NEGERI BANDUNG</h3>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; vertical-align: middle;">
                    <h5 style="margin: 0">FORM LB-D1</h5>
                </td>
            </tr>


        </tbody>
    </table>
    <table style="width: 96%">
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold">
                    <p>
                    <h3>DAFTAR KEGIATAN SISWA PRAKTIK KERJA INDUSTRI</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td width="30%">Nama Siswa</td>
                <td colspan="2" width="69%" class="tbl">: {{ ucwords($siswa['nama']) }}</td>
            </tr>
            <tr>
                <td width="30%">NIS</td>
                <td colspan="2" width="69%" class="tbl">: {{ $siswa['nis'] }}</td>
            </tr>
            <tr>
                <td width="30%">Perusahaan Tempat Bekerja</td>
                <td colspan="2" width="69%" class="tbl">: {{ $siswa['industri'] }}</td>
            </tr>
            <tr>
                <td width="30%">Alamat</td>
                <td colspan="2" width="69%" class="tbl">: {{ $siswa['alamat_industri'] }}</td>
            </tr>
            <tr>
                <td width="30%">Pembimbing Lapangan</td>
                <td colspan="2" width="69%" class="tbl">: {{ ucwords($siswa['pemb_industri']) }}</td>
            </tr>
            <tr>
                <td width="30%">Pembimbing Industri</td>
                <td colspan="2" width="69%" class="tbl">: {{ ucwords($siswa['pemb_sekolah']) }}</td>
            </tr>


        </tbody>
    </table>
    <h4>&nbsp;</h4>
    <table class="table" style="width: 96%">
        <thead>
            {{-- {{ dd($paraf) }} --}}
            <tr>
                <th>No</th>
                <th>Hari, Tanggal</th>
                <th>Kegiatan</th>
                <th>Paraf</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($agenda as $ag)
                @if ($ag['nama'] == null)
                    <tr style="background: #f8a6a6">
                        <td style="color: blue">{{ $no++ }}</td>
                        <td style="color: blue">{{ (new \App\Helpers\Help())->getDay($ag['waktu']) }}</td>
                        <td style="color: blue">Tidak ada kegiatan siswa yang diinput</td>
                        <td>
                        </td>
                    </tr>
                @else
                    <tr>
                        <td style="vertical-align: middle">{{ $no++ }}</td>
                        <td style="vertical-align: middle">{{ (new \App\Helpers\Help())->getDay($ag['waktu']) }}</td>
                        <td style="vertical-align: middle">{{ $ag['nama'] }}</td>
                        <td style="text-align: center; vertical-align: middle">
                            @if ($ag['status'] == 4)
                                <img src="{{ $paraf }}" alt="" style="height: 29px;">
                            @endif

                        </td>
                    <tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <br><br>
    <div style=" display: inline; float: right; margin-right: 30px;">
        Temanggung, {{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d')) }}<br>
        Pembimbing Prakerind<br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <b><u>{{ $siswa['pemb_sekolah'] }}</u></b><br>
        {{ ucwords($siswa['jurusan']) }}
    </div>


</body>

</html>
