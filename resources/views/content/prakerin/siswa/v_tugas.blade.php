@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .catatanGuru {
            white-space: normal;
        }

        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div id="calendar"></div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="javascript:void(0)" id="formUpdateKalender">
                        <div class="row">
                            <div class="col-md-12" id="informasi">
                                <p class="mb-1">Anda bisa menambahkan data kegiatan di halaman ini.</p>
                                <p><small>Klik tambah untuk menambahkan data</small></p>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="form-label mb-0">Kegiatan Anda Hari ini</label>
                            </div>
                            <div class="col-md-7">
                                <input type="hidden" id="action" value="add">
                                <input type="hidden" name="id" id="id_tugas">
                                <input type="hidden" name="start" id="start">
                                <select name="title[]" id="title" class="select2" multiple required></select>
                                <small class="text-muted">Gunakan enter untuk membuat item multiple</small>
                            </div>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-success btn-update">Update</button>
                                <a href="javascript:void(0)" id="deleteKalender" onclick="deleteKalender()"
                                    class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            var calendar = $('#calendar').fullCalendar({
                customButtons: {
                    myCustomButton: {
                        text: 'Cetak Kegiatan',
                        click: function(event, jsEvent, view) {
                            window.open("{{ url('program/sistem_pkl/nilai_siswa/cetak/kegiatan', $id_code) }}", "_blank");
                        },
                    }
                },
                header: {
                    left: 'prev,next today myCustomButton',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: 'tugas',
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['waktu'],
                                    title: value['nama'],
                                    catatan: value['catatan'],
                                    ulang: value['loop'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.catatan != null) {
                        element.children().last().append(
                            "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Catatan : " +
                            event
                            .catatan + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: "tugas/check",
                        data: 'tanggal=' + tanggal,
                        type: "POST",
                        success: function(response) {
                            if (response == true) {
                                $('#action').val('add');
                                $('.btn-update').html('Tambah');
                                $('.select2').val(null).trigger('change');
                                $('#formUpdateKalender').trigger("reset");
                                $('#deleteKalender').hide();
                                $('#informasi').html('');
                                $('#informasi').html(
                                    '<p class="mb-1">Anda bisa menambahkan data kegiatan di halaman ini.</p><p><small>Klik tambah untuk menambahkan data</small></p>'
                                );
                                $('#start').val($.fullCalendar.formatDate(start,
                                    "Y-MM-DD HH:mm:ss"));
                                $('#ajaxModel').modal('show');
                            } else {
                                alert("mohon maaf. 1 hari hanya dapat ngirim 1 inputan. bila ingin nambah kegiatan, anda bisa mengupdatenya di kegiatan!");
                                    $('#calendar').fullCalendar('unselect');
                            }

                        }
                    });

                },
                // eventClick: function(event, jsEvent, view) {
                //     var title = prompt('Event Title:', event.title);
                //     if (title) {
                //         event.title = title;
                //         var start = $.fullCalendar.formatDate(event.start,
                //             "Y-MM-DD HH:mm:ss");
                //         $.ajax({
                //             url: base_url + "calendar/edit_events",
                //             data: 'title=' + title + '&start=' + start + '&end=' +
                //                 end +
                //                 '&id=' + event.id,
                //             type: "POST",
                //             success: function(response) {
                //                 displayMessage("Updated Successfully");
                //                 window.location.href = base_url +
                //                     "calendar/events"

                //             }
                //         });
                //         calendar.fullCalendar('renderEvent', {
                //                 title: title,
                //                 start: start,
                //                 end: end,
                //             },
                //             true
                //         );

                //     }
                //     calendar.fullCalendar('unselect');
                // },
                eventClick: function(event, jsEvent, view) {
                    $('#informasi').html('');
                    $('#action').val('edit');
                    $('.btn-update').html('Update');
                    $('#deleteKalender').show();
                    $('#title').val(event.title);
                    $('#informasi').html(
                        '<p class="mb-1">Anda bisa mengupdate dan menghapus tugas anda hari ini disini dari database.</p><p><small>Update untuk proses update, dan delete untuk menghapus data</small></p>'
                    );
                    $('#id_tugas').val(event.id);
                    $('#start').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    var s2 = $('#title');
                    var vals = event.ulang;
                    console.log(vals);
                    vals.forEach(function(key) {
                        if (!s2.find('option:contains(' + key + ')').length)
                            s2.append($('<option>').text(key));
                    });
                    s2.val(vals).trigger("change");
                    $('#ajaxModel').modal('show');
                },
            });
        });

        $('#formUpdateKalender').on('submit', function(event) {
            event.preventDefault();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".btn-update").attr("disabled", true);
            var action_url = '';

            if ($('#action').val() == 'add') {
                action_url = "{{ route('pkl_tugas-create') }}";
            }

            if ($('#action').val() == 'edit') {
                action_url = "{{ route('pkl_tugas-update') }}";
            }
            $.ajax({
                url: action_url,
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formUpdateKalender').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                    noti(data.icon, data.success);
                    $('.btn-update').html('Update');
                    $(".btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function deleteKalender() {
            var id = $('#id_tugas').val();
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                    $.ajax({
                        type: 'POST',
                        url: "tugas/trash",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $("#deleteKalender").html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#ajaxModel').modal('hide');
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                            swa(data.status + "!", data.message, data.success);
                            $("#deleteKalender").html('Delete');
                        }
                    });
                },
                function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
        }
    </script>
@endsection
