<!DOCTYPE html>
<html>

<head>
    <title>Cetak Nilai Prakerind</title>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            // console.log(document.readyState);
            if (document.readyState == "complete") {
                window.close();
                setTimeout(() => {
                    window.close();
                }, 2000);
                // open(location, '_self').close();
                // window.location.href = "{{ url()->previous() }}";
                // window.close();
                // setTimeout(window.close, 0);
                // window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <style>
        {{ $template['sourcode'] }}
    </style>
    <div class="sertifikat">
        <div class="wadah_sertifikat">
            <span class="title_sertifikat">Sertifikat PRAKERIND</span>
            <br><br>
            <span class="bidang_sertifikat"><i>This is to certify that</i></span>
            <br><br>
            <span class="siswa_sertifikat"><b>{{ ucwords($siswa['nama']) }}</b></span><br /><br />
            <span class="sertifikat_selesai"><i>telah menyelesaikan prakerind di</i></span> <br /><br />
            <span class="kursus_sertifikat">{{ $siswa['industri'] }}</span> <br /><br />
            <span class="skore_sertifikat">dengan rata-rata <b>90</b></span>
            <br /><br /><br /><br />
            <span class="tgl_ser"><i>tanggal</i></span><br>
            <span class="tgl_sertifikat">{{ (new \App\Helpers\Help())->getTanggal(date('Y-m-d H:i:s')) }}</span>
            <br /><br /><br /><br />
        </div>
    </div>

    <script>
        var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';
        style.media = 'print';

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);

        window.print();
    </script>




</body>

</html>
