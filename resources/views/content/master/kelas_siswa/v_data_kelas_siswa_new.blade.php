@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .dataTables_wrapper table.dataTable thead .sorting_asc::before {
            display: none !important;
        }

    </style>

    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header bg-info">
                <div class="row">
                    <div class="col-md-2 my-auto">
                        <h2 class="box-title mt-1 text-white">{{ Session::get('title') }}</h2>
                    </div>
                    <div class="col-md-10">
                        <form class="form-inline mx-2 float-right">
                            <div class="form-group">
                                <label for="inputPassword6">Asal</label>
                                <select name="asal_data" id="asal_data" class="form-control mx-1">
                                    <option value="">Pilih Asal Data..</option>
                                    <option value="siswa" {{ $_GET['based'] == 'siswa' ? 'selected' : '' }}>Siswa Baru
                                    </option>
                                    <option value="kelas" {{ $_GET['based'] == 'kelas' ? 'selected' : '' }}>Kelas Siswa
                                    </option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-5">
                                <h5 class="box-title">List Data</h5>
                            </div>
                            <div class="col-md-7 {{ $_GET['based'] == 'siswa' ? 'd-none' : '' }}" id="filter_siswa">
                                <form class="form-inline mx-2 float-right">
                                    <div class="form-group mx-2">
                                        <label for="inputPassword6">Tahun</label>
                                        <select name="tahun" id="tahun" class="form-control mx-1">
                                            <option value="">Pilih Tahun...</option>
                                            @foreach ($tahun as $thn)
                                                <option value="{{ substr($thn['tahun_ajaran'], 0, 4) }}"
                                                    {{ $_GET['based'] == 'kelas' && $_GET['tahun'] == substr($thn['tahun_ajaran'], 0, 4) ? 'selected' : '' }}>
                                                    {{ $thn['tahun_ajaran'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword6">Rombel</label>
                                        <select name="rombel" id="rombel" class="form-control mx-1">
                                            <option value="">Pilih Rombel..</option>
                                            @foreach ($jurusan as $jr)
                                                <optgroup label="{{ $jr['nama'] }}">
                                                    @foreach ($jr['kelas'] as $kelas)
                                                <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                    @foreach ($kelas['rombel'] as $rombel)
                                                        <option
                                                            value="{{ (new \App\Helpers\Help())->encode($rombel['id']) }}"
                                                            {{ $_GET['based'] == 'kelas' && (new \App\Helpers\Help())->encode($rombel['id']) == $_GET['rombel']? 'selected': '' }}>
                                                            {{ $rombel['nama'] }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <input type="hidden" name="tahun_ajar" id="tahun_ajar"
                            value="{{ $_GET['based'] == 'kelas' ? $_GET['tahun'] : '' }}">

                        <hr>
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <form class="form-inline" id="formMove">
                                    <button class="btn btn-success mb-2" id="import"><i
                                            class="fas fa-file-import"></i>
                                        Import</button>
                                    <div id="aksiTambahan" class="d-none">
                                        <select id="tindakan" class="form-control mb-2 mr-sm-2 mx-2">
                                            <option value="">-- Pilih Tindakan --</option>
                                            <option value="hapus">Hapus yang dipilih</option>
                                            <option value="pindah">Pindahkan</option>
                                        </select>
                                        <select name="id_rombel" id="id_rombel"
                                            class="form-control mb-2 mr-sm-2 mx-2 d-none">
                                            <option value="">-- Pilih Rombel --</option>
                                            @foreach ($jurusan as $jr)
                                                <optgroup label="{{ $jr['nama'] }}">
                                                    @foreach ($jr['kelas'] as $kelas)
                                                <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                    @foreach ($kelas['rombel'] as $rombel)
                                                        <option
                                                            value="{{ (new \App\Helpers\Help())->encode($rombel['id']) }}">
                                                            {{ $rombel['nama'] }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                        <select name="tahun" id="next_tahun" class="form-control mb-2 mr-sm-2 mx-2 d-none">
                                            <option value="">Pilih Tahun...</option>
                                            @foreach ($tahun as $thn)
                                                <option value="{{ substr($thn['tahun_ajaran'], 0, 4) }}">
                                                    {{ $thn['tahun_ajaran'] }}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-info mb-2" id="saveBtn"><i
                                                class="far fa-play-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="data-tabel">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="select_all" value="1"
                                                        id="example-select-all">
                                                </th>
                                                <th>#</th>
                                                <th>Siswa</th>
                                                <th>Jenkel</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                <th>Tempat, Tgl Lahir</th>
                                                <th width="50"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form id="importKelasSiswa" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row justify-content-center">
                            <div class="col-md-9">
                                <center>
                                    <i class="material-icons text-info">file_download</i><br>
                                    <b class="text-info">Choose the file to be imported</b>
                                    <p class="mb-0">[only xls, xlsx and csv formats are supported]</p>
                                    <p>Maximum upload file size is 5 MB.</p>
                                    <div class="input_kelas"></div>
                                    <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                        accept="*" />
                                    <br>
                                    <u id="template_import">
                                        <a href="{{ $url }}" target="_blank" class="text-info">Download
                                            sample template
                                            for
                                            import
                                        </a>
                                    </u>
                                </center>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#asal_data').on('change', function() {
                var asal = $(this).val();
                if (asal == 'siswa') {
                    $('.filter_siswa').hide();
                    window.location.href = "kelas_siswa?based=" + asal;
                } else {
                    $('#filter_siswa').removeClass('d-none');
                }
            })

            $('#tahun').change(function() {
                loadSiswa('kelas', $(this).val(), $('#rombel').val());
            });


            $('#rombel').change(function() {
                loadSiswa('kelas', $('#tahun').val(), $(this).val());
            });

            $('#tindakan').change(function() {
                if ($(this).val() == 'pindah') {
                    $('#id_rombel').removeClass('d-none');
                    $('#next_tahun').removeClass('d-none');
                } else {
                    $('#id_rombel').addClass('d-none');
                    $('#next_tahun').addClass('d-none');
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                columns: [{
                        data: 'check',
                        name: 'check',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'siswa',
                        name: 'siswa'
                    },
                    {
                        data: 'jenkel',
                        name: 'jenkel'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'ttl',
                        name: 'ttl'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });


            $("#example-select-all").click(function(e) {
                var table = $(e.target).closest('table');
                $('td input:checkbox', table).prop('checked', this.checked);
                if (this.checked) {
                    $('#aksiTambahan').removeClass('d-none');
                } else {
                    $('#aksiTambahan').addClass('d-none');
                }
            });

            $(document).on('change', '.manual_entry_cb', function() {
                var check_box = $('input[name="siswa[]"]:checked').length > 0;
                if (check_box) {
                    $('#aksiTambahan').removeClass('d-none');
                } else {
                    $('#aksiTambahan').addClass('d-none');
                }
            });

            $('#formMove').on('submit', function(event) {
                event.preventDefault();
                var id_kelas_siswa = [];
                $("input:checkbox[class=manual_entry_cb]:checked").each(function() {
                    id_kelas_siswa.push($(this).val());
                });
                if (id_kelas_siswa.length > 0) {
                    var action_url = '';
                    var dataPost = '';
                    var notifi = '';
                    if ($('#tindakan').val() == 'pindah') {
                        notifi = 'Apa anda yakin ingin memindahkan siswa ke kelas tersebut?';
                        action_url = "{{ route('update-kelas_siswa') }}";
                        dataPost = "id_siswa=" + JSON.stringify(id_kelas_siswa) + "&based=" + $(
                                '#asal_data').val() + "&rombel=" + $('#id_rombel').val() +
                            "&tahun=" + $('#next_tahun').val();

                    } else {
                        notifi = 'Apa anda yakin ingin menghapus siswa yang dipilih?';
                        action_url = "{{ route('kelas_siswa-soft_delete') }}";
                        dataPost = "id_siswa=" + JSON.stringify(id_kelas_siswa) + "&based=" + $(
                            '#asal_data').val();
                    }
                    console.log(dataPost);
                    let moveConfirm = confirm(notifi);
                    if (moveConfirm == true) {
                        $.ajax({
                            url: action_url,
                            type: "POST",
                            data: dataPost,
                            beforeSend: function() {
                                $('#saveBtn').html(
                                    '<i class="fa fa-spin fa-sync-alt"></i>');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    var oTable = $('#data-tabel').dataTable();
                                    oTable.fnDraw(false);
                                    $("#example-select-all").attr("checked", false);
                                }
                                swa(data.status + "!", data.message, data.icon);
                                $('#saveBtn').html('<i class="far fa-play-circle"></i>');
                                $("#saveBtn").attr("disabled", false);
                            },
                            error: function(data) {
                                console.log('Error:', data);
                                $('#saveBtn').html('<i class="far fa-play-circle"></i>');
                                $("#saveBtn").attr("disabled", false);
                            }
                        });
                    }
                }
            })

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });


            $('body').on('submit', '#importKelasSiswa', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importKelasSiswa"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-kelas_siswa') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload()
                        }
                        $('#importBtn').html('Simpan');
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });
        })

        function loadSiswa(based, tahun, rombel) {
            var notempty = based && tahun && rombel;
            if (notempty) {
                window.location.href = "kelas_siswa?based=" + based + "&tahun=" + tahun + "&rombel=" + rombel;
            }
        }
    </script>
@endsection
