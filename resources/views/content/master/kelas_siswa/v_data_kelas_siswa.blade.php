@extends('template/template_default/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

        #data_trash {
            background: #820084 !important;
            color: #fff !important;
        }

        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        table.dataTable tr th.select-checkbox.selected::after {
            content: "✔";
            margin-top: -11px;
            margin-left: -4px;
            text-align: center;
            text-shadow: rgb(176, 190, 217) 1px 1px, rgb(176, 190, 217) -1px -1px, rgb(176, 190, 217) 1px -1px, rgb(176, 190, 217) -1px 1px;
        }

        th#checks select,
        th#nomor select,
        th#aksion select {
            display: contents;
        }

        tfoot tr th select {
            font-family: inherit;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            display: block;
            width: 100%;
            max-width: 320px;
            height: 36px;
            float: right;
            margin: 5px 0px;
            padding: 0px 24px;
            font-size: 16px;
            line-height: 1.75;
            color: #333;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            -ms-word-break: normal;
            word-break: normal;
        }

        tfoot tr th select:after {
            content: '<>';
            font: 17px "Consolas", monospace;
            color: #333;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
            padding: 0 0 2px;
            border-bottom: 1px solid #999;
            pointer-events: none;
        }

        @media (max-width: 960px) {
            .hp {
                position: inherit !important;
            }
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                                <th>#</th>
                                <th>Nama</th>
                                <th>NIS</th>
                                <th>NISN</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Rombel</th>
                                <th>Aksi</th>
                            </tr>
                        <tfoot>
                            <tr>
                                <th id="checks"></th>
                                <th id="nomor">#</th>
                                <th>Nama</th>
                                <th>NIS</th>
                                <th>NISN</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Rombel</th>
                                <th id="aksion">Aksi</th>
                            </tr>
                        </tfoot>
                        </thead>
                    </table>
                </div>
            </div>
            <form class="form-inline"
                style="margin-bottom: 12px; -webkit-box-pack: center; display: -webkit-box !important">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kode" class="control-label">Jurusan</label>
                            <select name="id_jurusan" id="id_jurusan" class="form-control id_rombel">
                                <option value="">--- Pilih Jurusan ---</option>
                                @foreach ($jurusan as $item)
                                    <option value="{{ $item['id'] }}">{{ $item['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kode" class="control-label">Tingkat</label>
                            <select name="kelas" id="kelas" class="form-control">
                                <option value="">--- Pilih Kelas ---</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kode" class="control-label">Kelas</label>
                            <select name="rombel" id="rombel" class="form-control id_rombel">
                                <option value="">--- Pilih Rombel ---</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" style="position: relative">
                        <div class="form-group hp" style="position: absolute; bottom: 0">
                            <label for="kode" class="control-label"></label>
                            <a class="btn btn-primary btn-block" href="javascript: void(0);" id="delete_record">Pindah
                                Data</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>



    <script type="text/javascript">
        var rombel;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select><option value="" class="form-control"></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                },
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        action: function(e, dt, button, config) {
                            window.location = 'rombel-trash';
                        },
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: "",
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
                columns: [{
                        data: 'checkbox',
                        name: 'checkbox'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nis',
                        name: 'nis'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });

            $('#example-select-all').on('click', function() {
                var rows = table.rows({
                    'search': 'applied'
                }).nodes();
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });


            $('#example tbody').on('change', 'input[type="checkbox"]', function() {
                if (!this.checked) {
                    var el = $('#example-select-all').get(0);
                    if (el && el.checked && ('indeterminate' in el)) {
                        el.indeterminate = true;
                    }
                }
            });


            $('#delete_record').click(function() {
                if ($('input[name="siswa[]"]:checked').length < 1 || $('.id_rombel').val() == '') {
                    alert('Rombel dan checkbox harus terisi');
                } else {
                    $('#delete_record').html('Sending..');
                    var id_kelas_siswa = [];
                    $("input:checkbox[class=manual_entry_cb]:checked").each(function() {
                        id_kelas_siswa.push($(this).val());
                    });
                    // var id_rombel = $('.id_rombel').val();
                    if (id_kelas_siswa.length > 0) {

                        var confirmdelete = confirm("Do you really want move class student?");
                        if (confirmdelete == true) {
                            $.ajax({
                                url: "kelas_siswa/update",
                                type: 'post',
                                data: {
                                    id_kelas_siswa: id_kelas_siswa,
                                    id_rombel: rombel
                                },
                                success: function(data) {
                                    // dataTable.ajax.reload();
                                    var oTable = $('#data-tabel').dataTable();
                                    oTable.fnDraw(false);
                                    $('#example-select-all').prop('checked', false);
                                    noti(data.icon, data.success);
                                    $('#delete_record').html('Pindah Data');
                                }
                            });
                        }
                    }
                }
            });
        });

        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            if (id_jurusan) {
                $('select[name="kelas"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('get-jurusan') }}",
                    type: "POST",
                    data: {
                        id_jurusan: id_jurusan
                    },
                    beforeSend: function() {
                        $('select[name="kelas"]').html('<option value="">--Load data kelas--</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">--Pilih Kelas--</option>';
                        data = JSON.parse(data);
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama + '</option>';
                        })
                        $('select[name="kelas"]').removeAttr('disabled')
                        $('select[name="kelas"]').html(s)
                    }
                });
            }
        })


        $('select[name="kelas"]').on('change', function() {
            var id_kelas = $(this).val();
            // console.log(id_kelas)
            if (id_kelas) {
                $('select[name="rombel"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('get-rombel_kelas') }}",
                    type: "POST",
                    data: {
                        id_kelas: id_kelas
                    },
                    beforeSend: function() {
                        $('#rombel').html('<option value="">--Load data Rombel--</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">--Pilih Rombel--</option>';
                        data = JSON.parse(data);
                        data.forEach(function(val) {
                            s += '<option value="' + val.id + '">' + val.nama + '</option>';
                        })
                        $('#rombel').removeAttr('disabled')
                        $('#rombel').html(s);
                    }
                });
            }
        })

        $('#rombel').on('change', function(e) {
            rombel = $('#rombel').val()
        });

    </script>
@endsection
