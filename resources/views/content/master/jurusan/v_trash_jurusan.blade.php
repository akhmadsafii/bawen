@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        #popup {
            width: 100%;
            height: 100%;
            position: fixed;
            background: rgba(0, 0, 0, .7);
            top: 0;
            left: 0;
            z-index: 9999;
            visibility: hidden;
        }

        .window {
            width: 375px;
            height: 186px;
            background: #313a46;
            border-radius: 10px;
            position: relative;
            padding: 10px;
            box-shadow: 0 0 5px rgba(0, 0, 0, .4);
            text-align: center;
            margin: 15% auto;
            border: 2px solid #fff;
        }

        .close-button {
            font-size: 1.28571em;
            position: absolute;
            height: 2em;
            width: 2em;
            background-color: #313a46;
            opacity: 1;
            border: 2px solid #ffffff;
            text-shadow: none;
            color: #ffffff;
            border-radius: 50%;
            text-align: center;
            line-height: 1.83333em;
            position: absolute;
            top: -10px;
            right: -10px;
        }

        #popup:target {
            visibility: visible;
        }

        @media (max-width: 960px) {
            .download {
                float: left !important;
            }

        }

    </style>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>{{ Session::get('title') }}</h5>
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body clearfix">
                    <div class="mr-b-20 btn-list">
                        <a href="{{ route('jurusan') }}" class="btn btn-sm btn-facebook"><i class="fa fa-backward"></i>
                            Kembali</a>
                    </div>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Dibuat</th>
                                        <th>Terakhir Diupdate</th>
                                        <th>Dihapus</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_jurusan">

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Jurusan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="popup">
        <div class="window">
            <a href="#" class="close-button" title="Close">X</a>
            <h1 style="color:#fff">Import Excel</h1>
            <form action="" method="post">
                <label for="upload_file" class="btn btn-sm btn-facebook"
                    style="background-color: #049c1e; border: 1px solid #fff;"><i class="fa fa-upload"></i> Import
                    Excel</label>
                <input class="margin" type="file" formnovalidate id="upload_file" name="file" accept="*"
                    style="display: none;">
            </form>
            <a href="#" class="btn btn-sm btn-facebook" style="background-color: #049c1e; border: 1px solid #fff;"><i
                    class="fa fa-download"></i> Download Template Excel</a>
        </div>
    </div>

    <script type="text/javascript">


        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'deleted_at',
                        name: 'deleted_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Jurusan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-jurusan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-jurusan') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'jurusan/edit/' + id, function(data) {
                    console.log(id);
                    $('#modelHeading').html("Edit Data Jurusan");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#id_jurusan').val(data.id);
                    $('#nama').val(data.nama);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                });
            });
        });

        function deleteData(id) {

            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('master/jurusan/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function(data) {
                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )

                    }
                })
                $("#data-tabel").dataTable().fnDraw()
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'Data kamu masih aman :)',
                        'error'
                    )
                }
            })
        }

        function restoreData(id) {

            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then(function() {
                $.ajax({
                    url: "{{ url('master/jurusan/restore') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'PATCH'
                    },
                    success: function(data) {
                        swal(
                            'Berhasil!',
                            'Data anda sudah dipulihkan.',
                            'success'
                        )

                    }
                })
                $("#data-tabel").dataTable().fnDraw()
            })
        }

    </script>
@endsection
