@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="card">
                <div class="card-header alert-info my-auto">
                    <div class="row">
                        <div class="col-md-5">
                            <h5 class="box-title">{{ session('title') }}
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                                {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword6">Rombel</label>
                                    <select name="id_rombel" id="id_rombel" class="form-control mx-sm-3">
                                        <option value="all" {{ $_GET['rb'] == 'all' ? 'selected' : '' }}>All</option>
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ (new \App\Helpers\Help())->encode($rombel['id']) }}"
                                                        {{ $_GET['rb'] != 'all' && $rombel['id'] == (new \App\Helpers\Help())->decode($_GET['rb']) ? 'selected' : '' }}>
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div class="aksi">
                            <button class="btn btn-info" id="addData"><i class="fas fa-plus"></i> Tambah Data</button>
                            <button class="btn btn-success" id="import"><i class="fas fa-file-upload"></i> Import</button>
                        </div>
                        <hr>
                        <table class="table table-hover" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mapel</th>
                                    <th>Guru</th>
                                    <th>Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalMapel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formMapel" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" id="id_guru_pelajaran" name="id">
                            <label class="col-md-3 col-form-label" for="l0">Mapel</label>
                            <div class="col-md-9">
                                <select name="mapel" id="mapel" class="form-control" required>
                                    <option value="">Pilih Mapel..</option>
                                    @foreach ($mapel as $mp)
                                        <option value="{{ $mp['id'] }}">{{ $mp['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Guru</label>
                            <div class="col-md-9">
                                <select name="id_guru" id="id_guru" class="form-control" required>
                                    <option value="">Pilih Guru..</option>
                                    @foreach ($guru as $gr)
                                        <option value="{{ $gr['id'] }}">
                                            {{ $gr['nama'] . ' NIP ' . $gr['nip'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form id="importPelajaran" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-9">
                                <center>
                                    <div class="centr">
                                        <i class="fas fa-download fa-3x text-info"></i><br>
                                        <b class="text-info">Choose the file to be imported</b>
                                        <p class="m-0">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" name="image" accept="*" />
                                        <br>
                                        <u id="template_import">
                                            <a href="javascript:void(0)" onclick="return template()"
                                                class="text-info">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var url_import = "{{ route('import-guru_pelajaran') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'guru',
                        name: 'guru'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'tahun',
                        name: 'tahun'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#formMapel').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-guru_pelajaran') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-guru_pelajaran') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize() +
                        '&tahun_ajaran={{ $_GET['th'] }}&rombel={{ $_GET['rb'] }}',
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#formMapel').trigger("reset");
                            $('#modalMapel').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $('body').on('submit', '#importPelajaran', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importPelajaran"));
                formDatas.append("th", "{{ $_GET['th'] }}");
                $.ajax({
                    type: "POST",
                    url: url_import,
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#importFile').trigger("reset");
                            $('#importModal').modal('hide');
                            $('#importBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#importBtn').html('Simpan');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                var loader = $(this);
                if ($('#id_rombel').val() != 'all' && $('#tahun_ajaran').val()) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('edit-guru_pelajaran') }}",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $(loader).html(
                                '<i class="fas fa-pencil-alt"></i>');
                            $('#modelHeading').html("Edit Guru Pelajaran");
                            $('#id_guru_pelajaran').val(data.id);
                            $('#mapel').val(data.id_mapel);
                            $('#id_guru').val(data.id_guru);
                            $('#action').val('Edit');
                            $('#modalMapel').modal('show');
                        }
                    });
                } else {
                    alert('Harap Memilih Tahun Ajaran atau Rombel yang lebih spesifik')
                }
            })

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('trash-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            })


            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Guru Pelajaran');
            });

            $('#addData').click(function() {
                if ($('#id_rombel').val() != 'all' && $('#tahun_ajaran').val()) {
                    $('#formMapel').trigger("reset");
                    $('#modelHeading').html("Tambah Guru Pelajaran");
                    $('#modalMapel').modal('show');
                    $('#action').val('Add');
                } else {
                    alert('Harap Memilih Tahun Ajaran atau Rombel yang lebih spesifik')
                }
            });

            $("#tahun_ajaran").change(function() {
                loadPage($(this).val(), $("#id_rombel").val())
            });

            $("#id_rombel").change(function() {
                loadPage($("#tahun_ajaran").val(), $(this).val())
            });


        })

        function loadPage(tahun, rombel) {
            var notempty = tahun && rombel;
            if (notempty) {
                window.location.href = "guru_pelajaran?th=" + tahun + "&rb=" + rombel;
            }
        }

        function template() {
            window.location.href = "{{ $url }}";
        }
    </script>
@endsection
