@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')

    <style>
        .pace {
            display: none;
        }

        .left {
            display: none !important;
        }

        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            .fc-scroller.fc-day-grid-container {
                height: max-content !important;
            }

            .widget-heading.clearfix {
                padding-left: 23px;
            }

            h5 {
                font-size: 21px !important;
                font-weight: 600 !important;
            }

            .left {
                display: none !important;
            }
        }

        #popup {
            width: 100%;
            height: 100%;
            position: fixed;
            background: rgba(0, 0, 0, .7);
            top: 0;
            left: 0;
            z-index: 9999;
            visibility: hidden;
        }

        .window {
            width: 375px;
            height: 186px;
            background: #313a46;
            border-radius: 10px;
            position: relative;
            padding: 10px;
            box-shadow: 0 0 5px rgba(0, 0, 0, .4);
            text-align: center;
            margin: 15% auto;
            border: 2px solid #fff;
        }

        .close-button {
            font-size: 1.28571em;
            position: absolute;
            height: 2em;
            width: 2em;
            background-color: #313a46;
            opacity: 1;
            border: 2px solid #ffffff;
            text-shadow: none;
            color: #ffffff;
            border-radius: 50%;
            text-align: center;
            line-height: 1.83333em;
            position: absolute;
            top: -10px;
            right: -10px;
        }

        #popup:target {
            visibility: visible;
        }

        .swal2-modal.swal2-show {
            background: rgb(255 255 255 / 84%) !important;
        }

        @media (max-width: 960px) {
            .download {
                float: left !important;
            }

        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
        type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>{{ Session::get('title') }}</h5>
                </div>
                <div class="row p-4">
                    <div id="calendar"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_jurusan">

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Jurusan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxData" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-inline" action="#" method="post">
                                @csrf
                                <label for="" style="width: 140px;">Kelas yang terkoneksi</label>
                                <select class="form-control" multiple="multiple" data-toggle="select2"
                                    data-plugin-options='{"minimumResultsForSearch": -1}'
                                    style="display:inline; width:48%; margin-left: 12px;">
                                    <option selected="selected">Mustard</option>
                                    <option selected="selected">Ketchup</option>
                                    <option>Relish</option>
                                </select>
                                {{-- <input type="text" id="search" name="search" class="input-small form-control" placeholder="Pilih Data Kelas" style="display:inline; width:48%; margin-left: 12px;"> --}}
                                <button type="submit" class="btn btn-sm btn-success" style="margin-left: 15px;"><i
                                        class="fa fa-refresh"></i> Update</button>
                                <a href="#" class="btn btn-sm btn-primary" style="margin-left: 12px;"><i
                                        class="fa fa-share"></i> Tambah Data Kelas</a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: false,
                events: "kalendar",
                displayEventTime: false,
                eventColor: '#51d2b7',
                eventTextColor: '#FFF',
                editable: false,
                eventRender: function(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true
            });
        });
    </script>
@endsection
