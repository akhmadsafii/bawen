@extends('template.template_horizontal_nav_icons.app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        div#tabel_admin_filter {
            margin: 1.07143em auto;
        }

        span.badge {
            text-transform: uppercase;
            letter-spacing: 0.19048em;
            border-radius: 1.42857em;
            padding: 0.47619em 1.42857em;
        }

        #radioBtn .notActive {
            color: #3276b1;
            background-color: #fff;
        }

        .dataTables_wrapper table.dataTable thead .sorting_asc::before {
            display: none
        }

        .content-info {
            /* background: #f9f9f9; */
            padding: 40px 0;
            background-size: cover !important;
            background-position: top center !important;
            background-repeat: no-repeat !important;
            position: relative;
            padding-bottom: 100px
        }

        #filterStatus{
            position: absolute;
            left: 55%;
            z-index:2;
        }

        table {
            width: 100%;
            background: #fff;
            border: 1px solid #dedede
        }

        table thead tr th {
            padding: 20px;
            border: 1px solid #dedede;
            color: #000
        }

        table.table-striped tbody tr:nth-of-type(odd) {
            background: #f9f9f9
        }

        table.result-point tr td.number {
            width: 100px;
            position: relative
        }

        .text-left {
            text-align: left !important
        }

        table tr td {
            padding: 10px 20px;
            border: 1px solid #dedede
        }

        table tr td {
            padding: 10px 40px;
            border: 1px solid #dedede
        }

        table tr td img {
            max-width: 32px;
            float: left;
            margin-right: 11px;
            margin-top: 1px;
            border: 1px solid #dedede
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                
                <div class="widget-heading clearfix">
                    <h5>Blank Starter Page</h5>
                </div>

                <div class="widget-body clearfix">
                    <section class="content-info">
                        <div class="container paddings-mini">
                            <div class="row">
                                <div class="col-lg-12">
                                
                                
                                    
                                    <table class="table-striped table-responsive table-hover result-point" id="data-tabel">
                                    
                                    <span id="filterStatus" class="d-flex align-items-center">
                                        <label><strong></strong></label>
                                        <select id='status' data-column="4" class="form-control" style="width: 200px">
                                            <option value="">--Select Status--</option>
                                            <option value="Aktif">Aktif</option>
                                            <option value="Mati">Tidak Aktif</option>
                                        </select>
                                    </span>

                                        <thead class="point-table-head">
                                            <tr>
                                                <th class="text-center"><em class="fa fa-cog"></em></th>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Status</th>
                                                <th>Ubah Status</th>
                                            </tr>
                                        </thead>
                                    </table>

                                </span>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalInfo" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle"></h5>
                </div>
                <div id="infoContent">

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var table = $('#data-tabel').DataTable({
                ...settingTable,
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {

                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'aktifasi',
                        name: 'aktifasi'
                    },
                ]
            });

            $('#status').change(function(){
                table.columns(3)
                .search($(this).val()).draw();
            }); 

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('.tambahBaris').show('');
                $('#modelHeading').html("Tambah Data Jurusan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-jurusan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-jurusan') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.user_admin', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $(loader).html('<i class="fa fa-spin fa-spinner"></i> Loading');

                $('#modelTitle').html('Daftar Admin')
                $('#infoContent').html(
                    '<div class="row"><div class="col-lg-12"><table class="table-striped table-responsive table-hover result-point" id="tabel_admin"><thead class="point-table-head"><tr><th class="text-center"><em class="fa fa-cog"></em></th><th>No</th><th>Nama</th><th>Telepon</th><th>Status</th></tr></thead></table></div></div>'
                );
                loadAdmin(id, loader);
                $('#modalInfo').modal('show');
                
            });

            $(document).on('click', '.program_sekolah', function() {
                var id = $(this).data('id');
                $('#modelTitle').html('Daftar Program')
                $('#infoContent').html(
                    '<div class="row"><div class="col-lg-12"><table class="table-striped table-responsive table-hover result-point" id="tabel_admin"><thead class="point-table-head"><tr><th class="text-center"><em class="fa fa-cog"></em></th><th>No</th><th>Program</th><th>Template</th><th>Status</th></tr></thead></table></div></div>'
                );
                load_program(id);
                $('#modalInfo').modal('show');
            });
        });

        function loadAdmin(id, loader) {
            $(loader).html('<i class="fa fa-user"></i>');
            var table = $('#tabel_admin').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('supermaster-load_admin') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.id = id;
                    },
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                ]
            });
        }

        function load_program(id) {
            var table = $('#tabel_admin').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('supermaster-load_program') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.id = id;
                    },
                },
                columns: [{
                        data: 'aktifasi',
                        name: 'aktifasi',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'template',
                        name: 'template'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                ]
            });
        }

        function changeStatus(val, id) {
            if (val == 1) {
                $('a[data-toggle="pilihan_check' + id + '"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id, "sekolah");
            } else {
                $('a[data-toggle="pilihan_check' + id + '"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check' + id + '"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id, "sekolah");
            }
        }

        function changeStatusProgram(val, id) {
            if (val == 1) {
                $('a[data-toggle="pilihan_check_program' + id + '"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check_program' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id, "program");
            } else {
                $('a[data-toggle="pilihan_check_program' + id + '"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check_program' + id + '"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                    updateStatus(val, id, "program");
            }
        }

        function changeProgramStatus(id,val){
            if (id == 1) {
                $('a[data-toggle="pilihan_program_check'+val+'"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_program_check'+val+'"][data-title="N"]')
                    .removeClass('active').addClass('notActive');

                console.log(val);
            } else {
                $('a[data-toggle="pilihan_program_check'+val+'"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_program_check'+val+'"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                    console.log("not");
            }
        }

        function changeAdminStatus(id,val){
            if (id == 1) {
                $('a[data-toggle="pilihan_admin_check'+val+'"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_admin_check'+val+'"][data-title="N"]')
                    .removeClass('active').addClass('notActive');

                console.log(val);
            } else {
                $('a[data-toggle="pilihan_admin_check'+val+'"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_admin_check'+val+'"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                    console.log("not");
            }
        }

        function changeStatusAdmin(val, id) {
            if (val == 1) {
                $('a[data-toggle="pilihan_check_admin' + id + '"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check_admin' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id, "admin");
            } else {
                $('a[data-toggle="pilihan_check_admin' + id + '"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check_admin' + id + '"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id, "admin");
            }
        }


        function updateStatus(value, id, sumber) {
            var action_url = '';
            if (sumber == 'sekolah') {
                action_url = "{{ route('update_status-sekolah') }}";
            }

            if (sumber == 'program') {
                action_url = "{{ route('update-program_template') }}";
            }

            if (sumber == 'admin') {
                action_url = "{{ route('update_status-admin_sekolah') }}";
            }

            $.ajax({
                type: "POST",
                url: action_url,
                data: {
                    value,
                    id
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        if (sumber == 'sekolah') {
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        } else {
                            var oTable = $('#tabel_admin').dataTable();
                            oTable.fnDraw(false);
                        }
                    }
                    noti(data.success, data.message);
                }
            });
        }

        function changeTemplate(value, id) {
            $.ajax({
                type: 'POST',
                url: '{{ route('update-template_ajax') }}',
                data: {
                    value,
                    id
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        var oTable = $('#tabel_admin').dataTable();
                        oTable.fnDraw(false);
                    }
                    noti(data.success, data.message);
                }
            });
        }

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "sekolah/delete/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>

@endsection
