@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }
    </style>

<div class="row">
    <div class="col-md-12 widget-holder">
        <div class="widget-bg">
            <div class="widget-body clearfix">
                <div class="d-flex no-gutters pos-relative">
                    @yield('content_chat')
                    @include('content.master.pesan.list_message')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
