@extends('content.master.pesan.v_data_pesan_revisi')
@section('content_chat')
    <style>
        .chat-left {
            height: calc(111vh - 6.625rem);
        }

        form textarea {
            /* resize: none; */
            border: none;
            display: block;
            width: 100%;
            height: 80px;
            border-radius: 3px;
            padding: 20px;
            font-size: 13px;
            margin-bottom: 13px;
        }

        form textarea::placeholder {
            color: #ddd;
        }

        form img {
            height: 30px;
            cursor: pointer;
        }

        /* 
                    form button {
                        text-decoration: none;
                        text-transform: uppercase;
                        font-weight: bold;
                        color: #6fbced;
                        vertical-align: top;
                        margin-left: 333px;
                        margin-top: 5px;
                        display: inline-block;
                    } */
        .clearfix {
            zoom: 1
        }

        .clearfix:after {
            content: '.';
            clear: both;
            display: block;
            height: 0;
            visibility: hidden;
        }

    </style>
    <div class="col-sm-12 col-md-9 chat-left">
        <header class="d-flex justify-content-between"><a href="#" class="btn btn-sm btn-secondary">Back</a>
            <h4 class="my-0 sub-heading-font-family fw-400">{{ ucwords($profile['nama']) }}</h4><a href="#"
                class="btn btn-sm btn-secondary">View Profile</a>
        </header>
        <section class="scrollbar-enabled scroll-to-bottom">
            <ul class="list-unstyled pd-t-20 mr-l-20" id="load_chat">
                @foreach ($chat as $ch)
                    @if ($ch['email_pengirim'] != session('email_pengirim'))
                        <li class="media user-left">
                            <div class="d-flex mr-3">
                                <a href="#" class="center-cropped rounded-circle"
                                    style="background-image: url({{ $ch['file'] }}); margin-top:6px">
                                </a>
                            </div>
                            <div class="media-body">
                                <p class="mt-1 mb-2">{{ $ch['pesan'] }}</p>
                                <small class="block">{{ $ch['waktu'] }}</small>
                            </div>
                        </li>
                    @else
                        <li class="media user-right">
                            <div class="d-flex ml-3">
                                <a href="#" class="center-cropped rounded-circle"
                                    style="background-image: url({{ $file }}); margin-top:6px">
                                </a>
                            </div>
                            <div class="media-body text-right">
                                <p class="text-justify mt-1 mb-2">{{ $ch['pesan'] }}</p>
                                <small class="block">{{ $ch['waktu'] }}</small>
                            </div>
                        </li>
                    @endif
                @endforeach

            </ul>
        </section>
        <footer class="d-flex mt-4 mx-3">
            <form action="{{ url('program/' . Request::segment(2) . '/pesan/create_chat') }}" method="post" style="width: 100%">
                @csrf
                <div class="row">
                    <div class="col-md-10">
                        <input type="hidden" name="email_penerima" value="{{ $profile['email'] }}">
                        <textarea placeholder="Type your message" name="pesan" rows="10" cols="30"></textarea>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" style=""><i
                                    class="fa fa-paper-plane"></i>
                                Send
                                Message</button>
                        </div>
                    </div>
                </div>


            </form>
        </footer>
    </div>
    <div class="clearfix"></div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        const messaging = firebase.messaging();
        messaging.getToken({
            vapidKey: "BIcPxtrCV22Fiegga8Gt4miltCzILbdoUFzBhJuHB8dbVJS03H8RF_MC0nvsiN4YMouJkuXqRY-8a1NpdBE9XsM"
        });

        function sendTokenToServer(fcm_token) {
            $.ajax({
                type: 'POST',
                url: "{{ url('program/' . Request::segment(2) . '/pesan/save_token') }}",
                data: {
                    fcm_token
                },
                success: function(data) {
                    console.log(data.message)
                }
            });
        }

        function retrieveToken() {
            messaging.getToken().then((currentToken) => {
                if (currentToken) {
                    sendTokenToServer(currentToken);
                    // updateUIForPushEnabled(currentToken);
                } else {
                    alert('You should allow notification!');
                    // Show permission request UI
                    // console.log('No registration token available. Request permission to generate one.');
                    // updateUIForPushPermissionRequired();
                    // setTokenSentToServer();
                }
            }).catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
                // showToken('Error retrieving Instance ID Token. ', err);
                // sendTokenToServer(false);
            });
        }

        retrieveToken();

        messaging.onTokenRefresh(() => {
            // console.log("refresh");
            retrieveToken();
        });

        messaging.onMessage((payload) => {
            console.log('Message received.');
            console.log(payload);
            location.reload();
            // $("#load_chat" ).load(window.location.href + " #load_chat" );
        })

    </script>
@endsection
