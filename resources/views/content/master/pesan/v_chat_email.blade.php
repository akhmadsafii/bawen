@extends('content.master.pesan.v_data_pesan_revisi')
@section('content_chat')
    <div class="col-sm-12 col-md-9 chat-left">
        <div class="emptyState emptyState--withImage emptyState--frameless">
            <div class="emptyState__visual">
                <img src="assets/images/cloud.png" alt="image placeholder" />
            </div>

            <h2 class="emptyState__title">
                Your device is not onboarded
            </h2>
            <div class="emptyState__description">
                Go to your local device to upload an onboarding key
            </div>
            <div class="emptyState__call2action">
                <a class="button button--primary" href="#"><span class="iconMdsp add" aria-hidden="true"></span>Upload
                    key</a>
            </div>
        </div>
    </div>
@endsection
