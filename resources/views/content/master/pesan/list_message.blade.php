{{-- {{ dd($user) }} --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <script type="text/javascript" src="https://repo.rachmat.id/jquery-1.12.4.js"></script> --}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .chat-right li {
        padding-top: 1.07143em;
        padding-left: 1.42857em;
        cursor: auto;
    }

</style>
<div class="chat-right col-md-3 d-none d-sm-block scrollbar-enabled">
    <div class="chat-contact-list">
        <ul class="list-unstyled widget-user-list">
            {{-- <li class="media"> --}}
            <form action="{{ url('program/' . Request::segment(2) . '/pesan/new_create') }}" method="post"
                style="margin-bottom: 30px; padding: 12px;">
                @csrf
                <input type="text" name="email_penerima" placeholder="Masukan Email Penerima" id="tags"
                    class="form-control">
                <button type="submit" class="btn btn-info btn-block"
                    style="margin-top: 10px; color: #fff; margin-left: 0px !important" id="import"><i
                        class="material-icons list-icon mr-2">send</i>
                    Tulis Pesan</button>
            </form>
            {{-- </li> --}}
            @if (empty($user))
                {{-- <li class="media"> --}}
                Data kosong
                {{-- </li> --}}
            @endif
            @foreach ($user as $us)
                @php
                    $clr = '';
                    if ($us['role'] == 'guru') {
                        $clr = 'red';
                    } elseif ($us['role'] == 'siswa') {
                        $clr = 'green';
                    }
                    
                @endphp
                <li class="media">
                    <div class="d-flex mr-3">
                        <a href="{{ url('program/' . Request::segment(2) . '/pesan/create', encrypt($us['email'])) }}"
                            class="block user--busy thumb-xs center-cropped rounded-circle"
                            style="background-image: url({{ $us['file'] }}); margin-top:6px; height: 54px; width: 54px">
                            {{-- <img src="{{ $us['file'] }}" class="rounded-circle" alt="" style="margin-top: 18px;"> --}}
                        </a>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading mb-0 d-flex">
                            <a href="{{ url('program/' . Request::segment(2) . '/pesan/create', encrypt($us['email'])) }}"
                                class="mr-auto"
                                style="margin-bottom: 0;">{{ ucwords($us['nama']) . ' ' . $us['kode'] }}</a>
                            <span class="text-muted mr-4 fw-300 fs-14 d-none d-sm-block"
                                style="margin-top: 0.28571em">{{ $us['waktu'] }}</span>
                        </h5>
                        <small>{{ $us['email'] }}</small><small style="color: {{ $clr }}">status :
                            {{ $us['role'] }}</small>
                        <small class="mr-4">{{ $us['pesan_terakhir'] }}.</small>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<script>
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ url('program/' . Request::segment(2) . '/pesan/all/email') }}",
            success: function(result) {
                $("#tags").autocomplete({
                    source: result
                });
                
            }
        });
    })

</script>
