@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  <h4 class="mt-4 mb-2 title-content">Daftar {{$data["nama"]}} {{$data["kode"]}}</h4>
  <div class="row my-3">
      <div class="col-3">
          <p class="my-0">Jenis</p>
          <p class="my-0">Kategori</p>
          <p class="my-0">Satuan</p>
          <p class="my-0">Tarif</p>
      </div>
      <div class="col-7">
          <p class="my-0">: {{$data["jenis"]}}</p>
          <p class="my-0">: {{$data["kategori"]}}</p>
          <p class="my-0">: {{$data["satuan"]}}</p>
          <p class="my-0">: {{$data["tarif"]}}</p>
      </div>
      <div class="col-2">
        <button type="button" class="btn btn-info btn-sm" onclick="modalCreateItem()">
          <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
        </button>
      </div>
  </div>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-items-barang">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Lokasi</th>
                    <th>Kondisi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>

<!-- modal create-->
  <div class="modal fade" id="modal-create-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>        

            <div class="modal-body">
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-create-item">
                @csrf

                <div class="form-group">
                  <input type="hidden" name="id_barang" id="barang" value="{{$data["id"]}}" class="form-control" > 
                  <h5>{{$data["nama"]}}</h5>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="recipient-name" class="col-form-label">Kode</label>
                        <input type="text" name="kode" class="form-control" >   
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Lokasi</label>
                      <select name="id_lokasi" id="lokasi" class="form-control" id="input-lokasi">
                        @foreach($lokasis as $lokasi)
                            <option value="{{$lokasi['id']}}">{{$lokasi['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kondisi</label>
                      <select name="kondisi" id="kondisi" class="form-control" id="input-kondisi">
                        <option value="baik">baik</option>
                        <option value="rusak ringan">rusak ringan</option>
                        <option value="rusak berat">rusak berat</option>
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Jumlah</label>
                      <input type="number" name="jumlah" class="form-control" >  
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Tanggal Diterima</label>
                      <input type="date" name="tgl_diterima" class="form-control" >  
                    </div>
                </div>

                

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->


<!-- modal edit-->
  <div class="modal fade" id="modal-edit-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-edit-item" >
                @csrf

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="input-nama">   
                    <input type="hidden" name="id" class="form-control" id="input-id"> 
                    
                    <input type="hidden" name="kode" class="form-control" id="input-kode">
                </div>
                
                <input type="hidden" name="id_pengadaan" class="form-control" id="id-ada"> 
                <input type="hidden" name="id_barang" class="form-control" id="id-barang"> 

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Lokasi</label>
                      <select name="id_lokasi" id="lokasi" class="form-control" id="input-lokasi">
                        @foreach($lokasis as $lokasi)
                            <option value="{{$lokasi['id']}}">{{$lokasi['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kondisi</label>
                      <select name="kondisi" id="kondisi" class="form-control" id="input-kondisi">
                        <option value="baik">baik</option>
                        <option value="rusak">rusak</option>
                      </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->


<!-- modal detail-->
  <div class="modal fade bd-example-modal-lg" id="modal-detail-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div id="modal-detail-body">
                
              </div>
              
                

            </div>

          </div>

        </div>
  </div>
<!-- end modal -->

@include('content.sarpras.layout_sarpras')

<script>


    let tabel = $('#data-items-barang');
    
    $(function(){
        let urls = document.URL;
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "40%",
                    orderable :true
                },
                {
                    data: 'kode',
                    name: 'kode'
                },
                {
                    data: 'lokasi',
                    name: 'lokasi'
                },
                {
                    data: 'kondisi',
                    name: 'kondisi'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ];

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3,4 ]
                    },
                    title : 'Data item'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3,4 ]
                    },
                    title : 'Data item'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3,4 ]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Data item'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,urls,column,buttons);
    })

      

        
        function modalCreateItem(){
            $('#modal-create-item').modal('show');
        }

        function modalEditItem(id){       
            $.ajax({
                type : "post",
                url  : "{{route('detail-item')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                    $('#modal-edit-item').modal('show');
                    $('#input-id').val(data.data.id);
                    $('#id-barang').val(data.data.id_barang);
                    $('#id-ada').val(data.data.id_pengadaan);
                    $('#input-kode').val(data.data.kode);
                    $('#input-nama').val(data.data.nama);
                    $('#input-lokasi').val(data.data.id_lokasi);
                    
                },
                error : function(data){
                    
                }
            });

        }

        function modalDetailItem(id){   
            console.log("detail");    
            $.ajax({
                type : "post",
                url  : "{{route('detail-item')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                  data = data.data;
                    $('#modal-detail-body').html(`
                      <div class="container">
                        <h4 class="mb-2">${data.nama}</h4>
                        <h5 class="mb-2">Kode : ${data.kode}</h5>
                        <div class="row">
                          <div class="col-4">
                            <p class="my-1">Kategori</p>
                            <p class="my-1">Jenis</p>
                            <p class="my-1">Kondisi</p>
                            <p class="my-1">Lokasi</p>
                          </div>
                          <div class="col-8">
                            <p class="my-1">: ${data.kategori}</p>
                            <p class="my-1">: ${data.jenis}</p>
                            <p class="my-1">: ${data.kondisi}</p>
                            <p class="my-1">: ${data.lokasi}</p>
                          </div>
                        </div>
                      </div>
                    `);
                    $('#modal-detail-item').modal('show');
                },
                error : function(data){
                    
                }
            });

        }

        function deleteItem(id){
          $.ajax({
                type : "get",
                url  : "{{route('hapus-item')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    $('#data-item').DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
          });

        }

        function riwayatItem(id){
          $.ajax({
                type : "post",
                url  : "{{route('redirect-item-history')}}",
                data: {
                    'id' : id
                },
                success : (data) =>{
                    // console.log(data);
                    window.location = data;
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

         $('#form-create-item').on('submit',function(event) {
            let data = $(this).serialize();
            console.log(data);
            $.ajax({
                type : "post",
                url  : "{{route('buat-item')}}",
                data : data,
                success : function(data){
                    $('#data-item').DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modal-create-item").modal('hide');
                },
                error : function(data){
                    noti(data.icon, data.message);
                }
            });
        });

        $('#form-edit-item').on('submit',function(event) {
            let data = $(this).serialize();
            console.log(data);
            $.ajax({
                type : "post",
                url  : "{{route('edit-item')}}",
                data : data,
                success : function(data){
                  console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modal-edit-item").modal('hide');
                },
                error : function(data){
                    noti(data.icon, data.message);
                }
            });
        });

        $('#modal-detail-item').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

        $('#modal-edit-item').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

    




    

</script>

@endsection

