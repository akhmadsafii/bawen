@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  @if (Session::has('message'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>    
      </div>
    @endif
  <h4 class="my-2 title-content">Daftar Barang</h4>

	<div class="row justify-content-between align-items-end my-4">
      <section class="col-10 d-flex flex-row g-1">
          <div class="col-md-3">
            <label for="inputEmail4">Kategori</label>
            <select id="filter_kategori" class="form-control">
              <option value="">Semua</option>
              @foreach($kategoris as $kategori)
                  <option value="{{$kategori['id']}}">{{$kategori['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Jenis</label>
            <select id="filter_jenis" class="form-control">
              <option value="">Semua</option>
              @foreach($jeniss as $jenis)
                  <option value="{{$jenis['id']}}">{{$jenis['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Satuan</label>
            <select id="filter_satuan" class="form-control">
              <option value="">Semua</option>
              @foreach($satuans as $satuan)
                  <option value="{{$satuan['nama']}}">{{$satuan['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Suplyyer</label>
            <select id="filter_supllyer" class="form-control">
              <option value="">Semua</option>
              @foreach($supllyers as $supllyer)
                  <option value="{{$supllyer['id']}}">{{$supllyer['nama']}}</option>
              @endforeach
            </select>
          </div>
      </section>
      

      <button type="button" class="btn btn-info " onclick="modalCreateBarang()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
      </button>

	</div>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-barang">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Kategori</th>
                    <th>Id_Jenis</th>
                    <th>Id_Kategori</th>
                    <th>Supllyer</th>
                    <th>Satuan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


<!-- modal create-->
  <div class="modal fade bd-example-modal-lg" id="modal-create-barang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-create-barang">
                @csrf

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Kode</label>
                    <input type="text" name="kode" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" >   
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Satuan</label>
                      <select name="id_satuan" id="satuan" class="form-control">
                        @foreach($satuans as $satuan)
                            <option value="{{$satuan['id']}}">{{$satuan['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Supllyer</label>
                      <select name="id_supllyer" id="supllyer" class="form-control">
                        @foreach($supllyers as $supllyer)
                            <option value="{{$supllyer['id']}}">{{$supllyer['nama']}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kategori</label>
                      <select name="id_kategori" id="kategori" class="form-control">
                        @foreach($kategoris as $kategori)
                            <option value="{{$kategori['id']}}">{{$kategori['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Jenis</label>
                      <select name="id_jenis" id="jenis" class="form-control">
                        @foreach($jeniss as $jenis)
                            <option value="{{$jenis['id']}}">{{$jenis['nama']}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="inputEmail4">Jumlah</label>
                      <input type="number" name="jumlah" class="form-control" >  
                    </div>

                    <div class="form-group col-md-9">
                      <label for="inputEmail4">Tarif</label>
                      <input type="number" name="tarif" class="form-control" >  
                    </div>
                </div>

                

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal edit-->
  <div class="modal fade bd-example-modal-lg" id="modal-edit-barang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-edit-barang">
                @csrf

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="input-nama">   
                    <input type="hidden" name="id" class="form-control" id="input-id"> 
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Kode</label>
                    <input type="text" name="kode" class="form-control" id="input-kode">   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" id="input-keterangan">   
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Satuan</label>
                      <select name="id_satuan" id="satuan" class="form-control" id="input-satuan">
                        @foreach($satuans as $satuan)
                            <option value="{{$satuan['id']}}">{{$satuan['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Supllyer</label>
                      <select name="id_supllyer" id="supllyer" class="form-control" id="input-supllyer">
                        @foreach($supllyers as $supllyer)
                            <option value="{{$supllyer['id']}}">{{$supllyer['nama']}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kategori</label>
                      <select name="id_kategori" id="kategori" class="form-control" id="input-kategori">
                        @foreach($kategoris as $kategori)
                            <option value="{{$kategori['id']}}">{{$kategori['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Jenis</label>
                      <select name="id_jenis" id="jenis" class="form-control" id="input-jenis">
                        @foreach($jeniss as $jenis)
                            <option value="{{$jenis['id']}}">{{$jenis['nama']}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Jumlah</label>
                      <input type="number" name="jumlah" class="form-control" id="input-jumlah">  
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Tarif</label>
                      <input type="number" name="tarif" class="form-control" id="input-tarif">  
                    </div>
                </div>

                

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal detail-->
  <div class="modal fade bd-example-modal-lg" id="modal-detail-barang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div id="modal-detail-body">
                
              </div>
              
                

            </div>

          </div>

        </div>
  </div>
<!-- end modal -->

@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-barang');
    
    $(function(){
      let url = "{{ route('daftar-barang') }}";
      let column = 
          [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "5%"
              },
              {
                  data: 'nama',
                  name: 'nama',
                  width: "40%",
                  orderable :true
              },
              {
                  data: 'jenis',
                  name: 'jenis',
                  width: "20%",
                  orderable :true,
                  visible: true
              },
              {
                  data: 'kategori',
                  name: 'kategori',
                  width: "20%",
                  orderable :true,
                  visible: true,
                  searchable: true
              },
              {
                  data: 'id_jenis',
                  name: 'id_jenis',
                  width: "10%",
                  visible: false,
                  searchable: true
              },
              {
                  data: 'id_kategori',
                  name: 'id_kategori',
                  width: "10%",
                  visible: false,
                  searchable: true
              },
              {
                  data: 'id_suplayer',
                  name: 'id_suplayer',
                  width: "10%",
                  visible: false,
                  searchable: true
              },
              {
                  data: 'satuan',
                  name: 'satuan',
                  width: "10%",
                  visible: false,
                  searchable: true
              },
              {
                  data: 'aksi',
                  name: 'aksi',
                  width: "10%"
              }
          ];
      let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 , 3 ]
                  },
                  title : 'Daftar barang'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 , 3 ]
                  },
                  title : 'Daftar barang'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 , 3 ]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar barang'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

      MakeFullTable(tabel,url,column,buttons);

    });


      function modalEditBarang(id){
        console.log(id);
          $.ajax({
              type : "post",
              url  : "{{route('detail-barang')}}",
              data : {
                'id' : id
              },
              beforeSend : function(){
                $("#btnEdit").html(
                      '<i class="fa fa-spin fa-spinner"></i>'
                  );
              },
              success : function(data){
                console.log(data.data);
                  $('#modal-edit-barang').modal('show');
                  $('#input-id').val(data.data.id);
                  $('#input-kode').val(data.data.kode);
                  $('#input-nama').val(data.data.nama);
                  $('#input-keterangan').val(data.data.keterangan);
                  $('#input-jenis').val(data.data.id_jenis);
                  $('#input-satuan').val(data.data.id_satuan);
                  $('#input-supllyer').val(data.data.id_supllyer);
                  $('#input-kategori').val(data.data.id_kategori);
                  $('#input-tarif').val(data.data.tarif);
                  $('#input-jumlah').val(data.data.jumlah);
                  $("#btnEdit").html(
                      '<i class="fa fa-pencil ml-1 mt-2" aria-hidden="true"></i>'
                  );
              },
              error : function(data){
                  
              }
          });
      }

      function modalCreateBarang(){
          $('#modal-create-barang').modal('show');
      }

      $('#detail-create-barang').on('hidden.bs.modal', function() {
        $(this).find('form').trigger('reset');
      });

     
      function modalDetailBarang (id){       
          $.ajax({
              type : "post",
              url  : "{{route('detail-barang')}}",
              data : {
                'id' : id
              },
              success : function(data){
                console.log(data.data);
                data = data.data;
                let id = data.id
                
                console.log(id);
                
                $('#modal-detail-barang').modal('show');
                $('#modal-detail-body').html(`
                    <div class="container">
                      <div class="row">
                        <div class="col-8">
                          <h4 class="mb-2">${data.nama}</h4>
                          <h6>Jumlah : ${data.items.length}</h6>
                        </div>
                        <div class="col-4">
                          
                        </div>
                      </div>
                      

                      <div class="row">
                        <div class="col-4">
                          <p class="my-1">Kategori</p>
                          <p class="my-1">Satuan</p>
                          <p class="my-1">Jenis</p>
                          <p class="my-1">Jumlah</p>
                          <p class="my-1">Tarif</p>
                        </div>
                        <div class="col-8">
                          <p class="my-1">${data.kategori}</p>
                          <p class="my-1">${data.satuan}</p>
                          <p class="my-1">${data.jenis}</p>
                          <p class="my-1">${data.jumlah}</p>
                          <p class="my-1">${data.tarif}</p>
                        </div>
                      </div>
                    </div>
                `);
              },
              error : function(data){
                  
              }
          });

      }

      function detailBarang (id){
        $.ajax({
              type : "post",
              url  : "{{route('redirect-barang')}}",
              data: {
                  'id' : id
              },
              beforeSend : function(){
                $("#btnDetail").html(
                      '<i class="fa fa-spin fa-spinner"></i>'
                  );
              },
              success : (data) =>{
                  // console.log(data);
                  window.location = data;
                  $("#btnDetail").html(
                      '<i class="fa fa-info ml-2 mt-1" aria-hidden="true"></i>'
                  );
              },
              error: function(data) {
                  noti(data.icon, data.message);   
              } 
          }); 
      }

      function deleteBarang(id){
        $.ajax({
              type : "get",
              url  : "{{route('hapus-barang')}}",
              data: {
                  'id' : id
              },
              success : (data) =>{
                  console.log(data);
                  tabel.DataTable().ajax.reload();
                  noti(data.icon, data.message);
              },
              error: function(data) {
                  noti(data.icon, data.message);   
              } 
          }); 
      }

      $('#form-create-barang').on('submit',function(event) {
          let data = $(this).serialize();
          console.log(data);
          $.ajax({
              type : "post",
              url  : "{{route('buat-barang')}}",
              data : data,
              success : function(data){
                  $('#data-barang').DataTable().ajax.reload();
                  noti(data.icon, data.message);
                  $("#modal-create-barang").modal('hide');
              },
              error : function(data){
                  noti(data.icon, data.message);
              }
          });
      });

      $('#form-edit-barang').on('submit',function(event) {
          let data = $(this).serialize();
          console.log(data);
          $.ajax({
              type : "post",
              url  : "{{route('edit-barang')}}",
              data : data,
              success : function(data){
                console.log(data);
                  $('#data-barang').DataTable().ajax.reload();
                  noti(data.icon, data.message);
                  $("#modal-edit-barang").modal('hide');
              },
              error : function(data){
                  noti(data.icon, data.message);
              }
          });
      });

      $('#filter_jenis').change(function( ){
          $('#data-barang').DataTable().columns(4)
          .search($(this).val()).draw();
      }); 

      $('#filter_kategori').change(function( ){
          $('#data-barang').DataTable().columns(5)
          .search($(this).val()).draw();
      }); 

      $('#filter_supllyer').change(function( ){
          $('#data-barang').DataTable().columns(6)
          .search($(this).val()).draw();
      }); 

      $('#filter_satuan').change(function( ){
          $('#data-barang').DataTable().columns(7)
          .search($(this).val()).draw();
      });  

    



</script>

@endsection

