@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  
  <h4 class="my-2 title-content">Cetak barcode barang</h4>

	<div class="row justify-content-between align-items-end my-4">
      <section class="col-10 d-flex flex-row g-1">
          <div class="col-md-3">
            <label for="inputEmail4">Kategori</label>
            <select id="filter_kategori" class="form-control">
              <option value="">Semua</option>
              @foreach($kategoris as $kategori)
                  <option value="{{$kategori['id']}}">{{$kategori['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Jenis</label>
            <select id="filter_jenis" class="form-control">
              <option value="">Semua</option>
              @foreach($jeniss as $jenis)
                  <option value="{{$jenis['id']}}">{{$jenis['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Satuan</label>
            <select id="filter_satuan" class="form-control">
              <option value="">Semua</option>
              @foreach($satuans as $satuan)
                  <option value="{{$satuan['nama']}}">{{$satuan['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Suplyyer</label>
            <select id="filter_supllyer" class="form-control">
              <option value="">Semua</option>
              @foreach($supllyers as $supllyer)
                  <option value="{{$supllyer['id']}}">{{$supllyer['nama']}}</option>
              @endforeach
            </select>
          </div>
      </section>
      
	</div>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-barang">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Kategori</th>
                    <th>Id_Jenis</th>
                    <th>Id_Kategori</th>
                    <th>Supllyer</th>
                    <th>Satuan</th>
                    <th>Jumlah</th>
                    <th>Cetak</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>

@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-barang');

    $(function(){
        let url = "{{ route('daftar-barang-cetak') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "40%",
                    orderable :true
                },
                {
                    data: 'jenis',
                    name: 'jenis',
                    width: "20%",
                    orderable :true,
                    visible: true
                },
                {
                    data: 'kategori',
                    name: 'kategori',
                    width: "20%",
                    orderable :true,
                    visible: true,
                    searchable: true
                },
                {
                    data: 'id_jenis',
                    name: 'id_jenis',
                    width: "10%",
                    visible: false,
                    searchable: true
                },
                {
                    data: 'id_kategori',
                    name: 'id_kategori',
                    width: "10%",
                    visible: false,
                    searchable: true
                },
                {
                    data: 'id_suplayer',
                    name: 'id_suplayer',
                    width: "10%",
                    visible: false,
                    searchable: true
                },
                {
                    data: 'satuan',
                    name: 'satuan',
                    width: "10%",
                    visible: false,
                    searchable: true
                },
                {
                    data: 'jumlah_item',
                    name: 'jumlah_item',
                    width: "5%",
                    visible: true
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ];
       
        tabel.DataTable({
                ...konfigUmum,
                dom : 'rt',
                ajax: {
                    "url": url,
                    "method": "GET"
                },
                columns: column
        });
     })

        function detailBarang(id){
          $.ajax({
                type : "post",
                url  : "{{route('redirect-cetak-barang')}}",
                data: {
                    'id' : id
                },
                success : (data) =>{
                    // console.log(data);
                    window.location = data;
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

        $('#filter_jenis').change(function( ){
            $('#data-barang').DataTable().columns(4)
            .search($(this).val()).draw();
        }); 

        $('#filter_kategori').change(function( ){
            $('#data-barang').DataTable().columns(5)
            .search($(this).val()).draw();
        }); 

        $('#filter_supllyer').change(function( ){
            $('#data-barang').DataTable().columns(6)
            .search($(this).val()).draw();
        }); 

        $('#filter_satuan').change(function( ){
            $('#data-barang').DataTable().columns(7)
            .search($(this).val()).draw();
        });  

   

</script>

@endsection

