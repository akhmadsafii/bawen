@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg .tg-0lax{text-align:left;vertical-align:top}
</style>

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan pengadaan item</h4>
		<span>
			
      <button class="btn btn-danger btn-sm" onclick="PrintWindow()">
        <i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
        Download Pdf
      </button>
		</span>
		
	</div>
</section>


<section class="container bg-white" id="printArea">
	<div class="row flex-column justify-content-center flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">DAFTAR ITEM</h4>
		<h4 class="mt-1">{{$sekolah['nama']}}</h4>
	</div>


	<div class="row">
		<h5 class="ml-4">Daftar item</h5>
	</div>

	<div class="container">
        @foreach($datas as $data)
        <div class="container">
            @if(!empty($data["kategori"]))
                @foreach($data["kategori"] as $kat)
                @if(!empty($kat["items"]))
                <h5><strong>{{$data["jenis"]}}</strong></h5> 

        		<table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Kategori</th>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Kondisi</th>
                      <th scope="col">Lokasi</th>
                    </tr>
                  </thead>
                    @break
                @endif
                @endforeach
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($data["kategori"] as $kategoris)
                        @foreach($kategoris["items"] as $items)
                            <tr>
                              <td>{{$no++}}</td>
                              <td>{{$items["kategori"]}}</td>
                              <td>{{$items["kode"]}}</td>
                              <td>{{$items["nama"]}}</td>
                              <td>{{$items["kondisi"]}}</td>
                              <td>{{$items["lokasi"]}}</td>
                            </tr>
            
                        @endforeach
                    @endforeach
                  </tbody>
                </table> 
            @endif
        </div>
        @endforeach
	</div>

</section>


@include('content.sarpras.layout_sarpras')

<script>
  function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}
</script>


@endsection