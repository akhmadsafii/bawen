@extends('content.sarpras.laporan.template_export')
@section('content')

<style type="text/css">
		.jenis{
			margin-bottom: 5px;
		}
	</style>
  

	<div class="container">
        <div class="container">
        		<table class="tg" width="100%">
                  <thead>
                    <tr>
                      <th class="tg-0lax">No</th>
                      <th class="tg-0lax">Nama</th>
                      <th class="tg-0lax">Jenis</th>
                      <th class="tg-0lax">Kategori</th>
                      <th class="tg-0lax">Kode</th>
                      @if($filter === "kondisi")
                        <th class="tg-0lax">Lokasi</th>
                      @else
                        <th class="tg-0lax">Kondisi</th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($items as $item)
                      <tr>
                        <td class="tg-0lax">{{$no++}}</td>
                        <td class="tg-0lax">{{$item["nama"]}}</td>
                        <td class="tg-0lax">{{$item["jenis"]}}</td>
                        <td class="tg-0lax">{{$item["kategori"]}}</td>
                        <td class="tg-0lax">{{$item["kode"]}}</td>
                        @if($filter === "kondisi")
                          <td class="tg-0lax">{{$item["lokasi"]}}</td>
                        @else
                          <td class="tg-0lax">{{$item["kondisi"]}}</td>
                        @endif
                        
                        
                      </tr>    
                    @endforeach                  
                  </tbody>
                </table>
        </div>
	</div>

@endsection

