@extends('content.sarpras.laporan.template_export')
@section('content')

<style type="text/css">
		.jenis{
			margin-bottom: 5px;
		}
	</style>

	<div class="container">
        @foreach($datas as $data)
        <div class="container">
            @if(!empty($data["kategori"]))
              @foreach($data["kategori"] as $kat)
                {{-- @if(!empty($kat["items"])) --}}
                  <h4 class="jenis">Jenis {{$data["jenis"]}}</h4> 
                  <table class="tg" width="100%">
                    <thead>
                      <tr>
                        <th class="tg-0lax">No</th>
                        <th class="tg-0lax">Kategori</th>
                        <th class="tg-0lax">Kode</th>
                        <th class="tg-0lax">Nama</th>
                        <th class="tg-0lax">Kondisi</th>
                        <th class="tg-0lax">Lokasi</th>
                      </tr>
                    </thead>
                {{-- @break
                @endif --}}

              @endforeach
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($data["kategori"] as $kategoris)
                        @foreach($kategoris["items"] as $items)
                            <tr>
                              <td class="tg-0lax">{{$no}}</td>
                              <td class="tg-0lax">{{$items["kategori"]}}</td>
                              <td class="tg-0lax">{{$items["kode"]}}</td>
                              <td class="tg-0lax">{{$items["nama"]}}</td>
                              <td class="tg-0lax">{{$items["kondisi"]}}</td>
                              <td class="tg-0lax">{{$items["lokasi"]}}</td>
                            </tr>
                            @php
                                ++$no;
                            @endphp
                        @endforeach
                    @endforeach
                  </tbody>
                </table>
            @endif
        </div>
        @endforeach
	</div>

@endsection

