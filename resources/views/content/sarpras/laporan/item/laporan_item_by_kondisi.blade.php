@extends('template.template_default.app')
@section('content')

<style type="text/css">
    .pace{
        display: none;
    }
</style>

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan pengadaan item berdasarkan kondisi</h4>
		<span>
            <div class="dropdown">
              <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download Pdf
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item">-------- Kondisi --------</a>
                <a class="dropdown-item" href="{{route("print-daftar-laporan-item-by-kondisi", 'baik')}}"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>baik</a>
                <a class="dropdown-item" href="{{route("print-daftar-laporan-item-by-kondisi", 'rusak-ringan')}}"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>rusak ringan</a>
                <a class="dropdown-item" href="{{route("print-daftar-laporan-item-by-kondisi", 'rusak-berat')}}"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>rusak berat</a>
              </div>
            </div>	
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
	<div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">DATA ITEM</h4>
		<h4 class="mt-1">{{$sekolah['nama']}}</h4>
	</div>

	<div class="container">
        @foreach($datas as $data)
        <div class="container">
            @if(!empty($data["kategori"]))
                @foreach($data["kategori"] as $kat)
                @if(!empty($kat["items"]))
                <h5><strong>{{$data["jenis"]}}</strong></h5>   
            <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Kategori</th>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Kondisi</th>
                      <th scope="col">Lokasi</th>
                    </tr>
                  </thead>
                    @break
                @endif
                @endforeach
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($data["kategori"] as $kategoris)
                        @foreach($kategoris["items"] as $items)
                            <tr>
                              <td>{{$no++}}</td>
                              <td>{{$items["kategori"]}}</td>
                              <td>{{$items["kode"]}}</td>
                              <td>{{$items["nama"]}}</td>
                              <td>{{$items["kondisi"]}}</td>
                              <td>{{$items["lokasi"]}}</td>
                            </tr>
            
                        @endforeach
                    @endforeach
                  </tbody>
                </table>
                
            @endif
        </div>
        @endforeach
  </div>

</section>


@include('content.sarpras.layout_sarpras')

@endsection