@extends('template.template_default.app')
@section('content')

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan peminjaman belum kembali</h4>
		<span>
			<a href="{{route('daftar-laporan-peminjaman-dipinjam',['download'=>'pdf'])}}" class="btn btn-danger btn-sm">
				<i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
			Download Pdf
			</a>
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
	<div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">DATA PEMINJAMAN BELUM KEMBALI</h4>
		<h4 class="mt-1">{{$sekolah['nama']}}</h4>
	</div>

	 <div class="container my-3">

        <table class="table table-bordered mb-2">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Kode item</th>
                <th scope="col">Penyewa</th>
                <th scope="col">Role</th>
                <th scope="col">Tanggal sewa</th>
                <th scope="col">Tanggal kembali</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($peminjamans as $peminjaman)
              <tr>
                <td>{{$no}}</td>
                <td>{{ $peminjaman['nama_barang'] }}</td>
                <td>{{ $peminjaman['kode_item'] }}</td>
                <td>{{ $peminjaman['penyewa'] }}</td>
                <td>{{ $peminjaman['role'] }}</td>
                <td>{{ $peminjaman['tgl_sewa'] }}</td>
                <td>{{ $peminjaman['tgl_kembali'] }}</td>
                <td>{{ $peminjaman['status_kembali'] }}</td>
              </tr>
                @php
                    ++$no;
                @endphp
            @endforeach
            </tbody>
        </table>
    </div>

</section>


@include('content.sarpras.layout_sarpras')


@endsection