@extends('template.template_default.app')
@section('content')

<style type="text/css">
    #collapseExample{
        position: absolute;
        z-index: 5;
    }
</style>

<section class="container">
    <div class="row justify-content-between">
        <h4>Laporan peminjaman</h4>    
    </div>
</section>

<div class="container"> 
      <form action="{{ route('daftar-laporan-peminjaman-by-time') }}" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="row align-items-center">
            <div class="">
              <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i> Download Pdf</button>
            </div>
            <div class="col-1">
              <button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
                <div class="collapse" id="collapseExample">
                  <div class="card card-body">
                    <div class="row justify-content-end">
                        <div class="col-12">
                          <label for="exampleInputEmail1">Tanggal sewa</label>
                          <input type="date" name="start" class="form-control" id="start_date" placeholder="tanggal">
                        </div>
                        <div class="col-12 mt-1">
                          <label for="exampleInputEmail1">Tanggal kembali</label>
                          <input type="date" name="end" class="form-control" id="end_date" placeholder="tanggal">
                        </div>
                        <div class="col-12 d-flex justify-content-end mt-2 px-4">
                            <div class="col-3 mr-2">
                                <button type="button" class="btn btn-success btn-sm">Atur</button>
                            </div>
                            <div class="col-3 mr-2">
                                <button type="button" onclick="clear_date()" class="btn btn-primary btn-sm">Reset</button>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        
      </div>
      </form>
</div>


<section class="container bg-white py-3">
    <div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
        <h4 class="mb-0">DATA PEMINJAMAN</h4>
        <h4 class="mt-1">{{$sekolah['nama']}}</h4>
    </div>

    <div class="container my-3">

        <table class="table table-bordered mb-2">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Kode item</th>
                <th scope="col">Penyewa</th>
                <th scope="col">Role</th>
                <th scope="col">Tanggal sewa</th>
                <th scope="col">Tanggal kembali</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($peminjamans as $peminjaman)
              <tr>
                <td>{{$no}}</td>
                <td>{{ $peminjaman['nama_barang'] }}</td>
                <td>{{ $peminjaman['kode_item'] }}</td>
                <td>{{ $peminjaman['penyewa'] }}</td>
                <td>{{ $peminjaman['role'] }}</td>
                <td>{{ $peminjaman['tgl_sewa'] }}</td>
                <td>{{ $peminjaman['tgl_kembali'] }}</td>
                <td>{{ $peminjaman['status_kembali'] }}</td>
              </tr>
                @php
                    ++$no;
                @endphp
            @endforeach
            </tbody>
        </table>
    </div>

</section>


@include('content.sarpras.layout_sarpras')

<script type="text/javascript">

    
   

function clear_date(){
    let start,end;
    start = $('#start_date').val(null);
    end = $('#end_date').val(null);

}



</script>

@endsection