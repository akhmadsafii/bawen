@extends('content.sarpras.laporan.template_export')
@section('content')

	<table class="tg" width="100%">
			<thead>
			  <tr>
			    <th class="tg-0lax">No</th>
			    <th class="tg-0lax">Nama Barang</th>
			    <th class="tg-0lax">Kode</th>
			    <th class="tg-0lax">Penyewa</th>
			    <th class="tg-0lax">Role</th>
			    <th class="tg-0lax">Tanggal sewa</th>
			    <th class="tg-0lax">Tanggal kembali</th>
			    <th class="tg-0lax">Status</th>
			  </tr>
			</thead>
			<tbody>
			@php
			    $no = 1;
			@endphp
			@foreach($peminjamans as $peminjaman)
			  <tr>
			    <td class="tg-0lax">{{$no}}</td>
			    <td class="tg-0lax">{{ $peminjaman['nama_barang'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['kode_item'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['penyewa'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['role'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['tgl_sewa'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['tgl_kembali'] }}</td>
			    <td class="tg-0lax">{{ $peminjaman['status_kembali'] }}</td>
			  </tr>
			  	@php
			    	++$no;
				@endphp
			@endforeach
			</tbody>
		</table>

@endsection

