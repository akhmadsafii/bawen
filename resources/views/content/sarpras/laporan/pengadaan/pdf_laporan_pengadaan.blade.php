@extends('content.sarpras.laporan.template_export')
@section('content')

	<table class="tg" width="100%">
			<thead>
			  <tr>
			    <th class="tg-0lax">No</th>
			    <th class="tg-0lax">Nama Barang</th>
			    <th class="tg-0lax">Kode</th>
			    <th class="tg-0lax">Jenis</th>
			    <th class="tg-0lax">Jumlah</th>
			    <th class="tg-0lax">Total harga</th>
			    <th class="tg-0lax">Tanggal pengajuan</th>
			    <th class="tg-0lax">Tanggal diterima</th>
			    <th class="tg-0lax">Status</th>
			  </tr>
			</thead>
			<tbody>
			@php
			    $no = 1;
			@endphp
			@foreach($pengadaans as $pengadaan)
			  <tr>
			    <td class="tg-0lax">{{$no}}</td>
			    <td class="tg-0lax">{{ $pengadaan['nama'] }}</td>
			    <td class="tg-0lax">{{ $pengadaan['kode'] }}</td>
			    <td class="tg-0lax">{{ $pengadaan['jenis'] }}</td>
			    <td class="tg-0lax">{{ $pengadaan['jumlah'] }}</td>
			    
			    <td class="tg-0lax"><?php echo number_format($pengadaan['total_harga']) ?></td>
			    
			    <td class="tg-0lax">{{ $pengadaan['tgl_pengajuan'] }}</td>
			    <td class="tg-0lax">{{ $pengadaan['tgl_diterima'] }}</td>
			    <td class="tg-0lax">{{ $pengadaan['status_diterima'] }}</td>
			  </tr>
			  	@php
			    	++$no;
				@endphp
			@endforeach
			  <tr>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky">Total harga</td>
			    <td class="tharga-0pky">Rp. <?php echo number_format($harga) ?></td>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky"></td>
			    <td class="tharga-0pky"></td>
			  </tr>
			</tbody>
		</table>

@endsection

