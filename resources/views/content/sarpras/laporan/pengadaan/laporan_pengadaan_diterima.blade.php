@extends('template.template_default.app')
@section('content')

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan usulan pengadaan diterima</h4>
		<span>
			<a href="{{route('daftar-laporan-pengadaan-diterima',['download'=>'pdf'])}}" class="btn btn-danger btn-sm">
				<i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
			Download Pdf
			</a>	
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
    <div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
        <h4 class="mb-0">DATA PENGADAAN DITERIMA </h4>
        <h4 class="mt-1">{{$sekolah['nama']}}</h4>
    </div>

    <div class="container">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Kode</th>
                <th scope="col">Barang</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Total harga</th>
                <th scope="col">Tanggal pengajuan</th>
                <th scope="col">Tanggal diterima</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($pengadaans as $pengadaan)
              <tr>
                <td>{{$no}}</td>
                <td>{{ $pengadaan['nama'] }}</td>
                <td>{{ $pengadaan['kode'] }}</td>
                <td>{{ $pengadaan['barang'] }}</td>
                <td>{{ $pengadaan['jumlah'] }}</td>
                <td><?php echo number_format($pengadaan['total_harga']) ?></td>
                <td>{{ $pengadaan['tgl_pengajuan'] }}</td>
                <td>{{ $pengadaan['tgl_diterima'] }}</td>
                <td>{{ $pengadaan['status_diterima'] }}</td>
              </tr>
                @php
                    ++$no;
                @endphp
            @endforeach
            </tbody>
        </table>
        <h6>Total harga : Rp. <?php echo number_format($harga) ?></h6>
    </div>

</section>



@include('content.sarpras.layout_sarpras')

<script type="text/javascript">
    
    let tabel = $('#data-laporan-pengadaan');

    $(function(){

        let url = "{{ route('daftar-laporan-pengadaan-diterima') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'kode',
                    name: 'kode',
                    width: "10%"
                },
                {
                    data: 'jenis',
                    name: 'jenis',
                    width: "15%"
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    width: "5%"
                },
                {
                    data: 'total_harga',
                    name: 'total_harga',
                    width: "15%"
                },
                {
                    data: 'tgl_pengajuan',
                    name: 'tgl_pengajuan',
                    width: "10%"
                },
                {
                    data: 'tgl_diterima',
                    name: 'tgl_diterima',
                    width: "10%"
                },
                {
                    data: 'status_diterima',
                    name: 'status_diterima',
                    width: "10%"
                }
            ]
        ;

        tabel.DataTable({
            ...konfigUmum,
            dom : 'rt<"float-right"p>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    })


</script>

@endsection