@extends('template.template_default.app')
@section('content')

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan pemusnahan</h4>
		<span>
			<a href="{{route('daftar-laporan-pemusnahan-diterima',['download'=>'pdf'])}}" class="btn btn-danger btn-sm">
				<i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
			Download Pdf
			</a>
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
	<div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">LAPORAN PEMUSNAHAN DITERIMA<h4>
		<h4 class="mt-0">{{$sekolah['nama']}}</h4>
	</div>


	<div class="container py-3">

		<table class="table table-bordered">

            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Kode</th>
                <th scope="col">Jenis</th>
                <th scope="col">Kategori</th>
                <th scope="col">Tanggal pemusnahan</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($pemusnahans as $pemusnahan)
              <tr>
                <td>{{$no}}</td>
                <td>{{ $pemusnahan['barang'] }}</td>
                <td>{{ $pemusnahan['kode'] }}</td>
                <td>{{ $pemusnahan['jenis'] }}</td>
                <td>{{ $pemusnahan['kategori'] }}</td>
                <td>{{ $pemusnahan['tgl_pemusnahan'] }}</td>
                <td>{{ $pemusnahan['keterangan'] }}</td>
                <td>{{ $pemusnahan['status_pemusnahan'] }}</td>
              </tr>
                @php
                    ++$no;
                @endphp
            @endforeach
            </tbody>
        </table>

	</div>

</section>


@include('content.sarpras.layout_sarpras')


@endsection