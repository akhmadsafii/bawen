<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Export to pdf</title>
</head>

<style type="text/css">
	body{
		font-family:Arial, sans-serif;
	}
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg .tg-0lax{text-align:left;vertical-align:top}

	.tl  {border-collapse:collapse;border-spacing:0;margin-top: 10px;}
	.tl td{border-color:transparent;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  overflow:hidden;padding:10px 5px;word-break:normal;}
	.tl th{border-color:transparent;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
	.tl .tl-hfk9 {
	    background-color: transparent;
	    border-color: transparent;
	    text-align: left;
	    vertical-align: top;
	    padding: 0;
	}

	.tt  {border-collapse:collapse;border-spacing:0;width: 100%}
	.tt td{border-color:transparent;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  overflow:hidden;padding:10px 5px;word-break:normal;}
	.tt th{border-color:transparent;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
	.tt .tt-baqh{border-color:transparent;font-weight:bold;text-align:center;vertical-align:top,padding:0px;}
	.tt .tt-c3ow{border-color:transparent;font-weight:bold;text-align:center;vertical-align:top,padding:5px 0;}

	.tharga-0pky  {border-collapse:collapse;border-spacing:0;}
	.tg tbody tr td .tharga-0pky {border-color:transparent !important;border-style:solid;border-width:0;
	overflow:hidden;padding:10px 5px;word-break:normal;}
	.tharga-0pky{text-align:left;vertical-align:top}

	.title-doc{
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}
	.title-text{
		margin: 30px 0px;
	}

	.desc-doc{
		display: flex;
		flex-direction: row;
	}

	.container-main{
		padding: 25px;
	}


</style>

<body>
	<section class="container-main">

		<h2>Daftar {{$keterangan}}</h2>
		
		<div>
			@yield('content')
		</div>
	</section>

</body>

</html>

