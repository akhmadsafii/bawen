@extends('template.template_default.app')
@section('content')


<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
    .filter-option-inner-inner{
        color:black;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Peminjaman barang melewati batas waktu</h3>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-peminjaman-expired">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Peminjam</th>
                    <th>Role</th>
                    <th>Tanggal sewa</th>
                    <th>Tanggal kembali</th>         
                    <th>Status</th>                  
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
            </tbody>
        </table>
    </div>
</section>


<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-peminjaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

@include('content.sarpras.layout_sarpras')
@include('content.sarpras.peminjaman.modal_peminjaman')

<script>

    let tabel = $('#data-peminjaman-expired');

    $(function(){
        
        let url = "{{ route('expired-peminjaman') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama_barang',
                    name: 'nama_barang',
                    width: "15%"
                },
                {
                    data: 'kode_item',
                    name: 'kode_item',
                    width: "10%"
                },
                {
                    data: 'penyewa',
                    name: 'penyewa',
                    width: "15%"
                },
                {
                    data: 'role',
                    name: 'role',
                    width: "10%"
                },
                {
                    data: 'tgl_sewa',
                    name: 'tgl_sewa',
                    width: "10%"
                },
                {
                    data: 'tgl_kembali',
                    name: 'tgl_kembali',
                    width: "10%"
                },
                {
                    data: 'status_kembali',
                    name: 'status_kembali',
                    width: "10%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    text: '<i class="fa fa-upload"></i>',
                    className: 'btn btn-sm',
                    attr: {
                        title: 'Import Data',
                        id: 'import'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6,7]
                    },
                    title : 'Peminjaman barang melewati batas waktu'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6,7]
                    },
                    title : 'Peminjaman barang melewati batas waktu'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6,7]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Peminjaman barang melewati batas waktu'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);
    })


    


   
</script>

@endsection