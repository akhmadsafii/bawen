@extends('template.template_default.app')
@section('content')
<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</head>

<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
    .filter-option-inner-inner{
        color:black;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Peminjaman Barang</h3>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCreatePeminjaman">Request</button>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-peminjaman">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Peminjam</th>
                    <th>Role</th>
                    <th>Tanggal sewa</th>
                    <th>Tanggal kembali</th>         
                    <th>Status</th>                  
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modalCreatePeminjaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Reqest peminjaman barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" id="form-create-peminjaman">
                    @csrf

                    <div class="form-group ">
                          <label for="inputEmail4">Peminjam</label>
                          <select name="id_user" class="form-control selectpicker text-dark" data-live-search="true">
                            @foreach($users as $user)
                                <option value="{{$user['id']}}">{{$user['nama']}}</option>
                            @endforeach
                          </select>
                    </div>

                    
                    <div class="form-group ">
                          <label for="inputEmail4">Barang</label>
                          <select name="id_item[]" class="form-control selectpicker text-dark" data-live-search="true" multiple>
                            @foreach($items as $item)
                                <option value="{{$item['id']}}">{{$item['nama']}} {{$item['kode']}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-row">
                        <div class="col">
                            <label for="recipient-name" class="col-form-label">Tanggal sewa</label>
                            <input type="date" name="tgl_sewa" class="form-control" >   
                        </div>
                        <div class="col">
                            <label for="recipient-name" class="col-form-label">Tanggal kembali</label>
                            <input type="date" name="tgl_kembali" class="form-control" >  
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <!-- <input type="text" name="keterangan" class="form-control" >   --> 
                        <textarea type="text" class="form-control" name="keterangan" rows="4" cols="50">
                        </textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Request</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Create -->

<!-- Modal Edit-->
<div class="modal fade bd-example-modal-lg" id="modal-edit-peminjaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-peminjaman" action="javascript:void(0)">
                    @csrf
                    <input type="hidden" name="id" id="input_id_sewa" class="form-control" >  
                    <input type="hidden" name="id_user" id="input_id_user" class="form-control" >
                    

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Peminjam</label>
                        <input type="text" name="" id="input_nama_user" class="form-control" readonly>  
                    </div>

                    <div class="form-group ">
                          <label for="inputEmail4">Barang</label>
                          <select name="id_item" class="form-control selectpicker" data-live-search="true" id="input_barang">
                            @foreach($items as $item)
                                <option value="{{$item['id']}}">{{$item['nama']}} {{$item['kode']}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal sewa</label>
                        <input type="date" name="tgl_sewa" id="input_tgl_sewa" class="form-control" >  
                    </div>

                     <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal kembali</label>
                        <input type="date" name="tgl_kembali" id="input_tgl_kembali" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" id="input_keterangan" class="form-control" >   
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

            </form>
        </div>
    </div>
</div>



@include('content.sarpras.layout_sarpras')
@include('content.sarpras.peminjaman.modal_peminjaman')

<script>
    
    let tabel = $('#data-peminjaman');

    $(function(){
        let url = "{{ route('admin-page-peminjaman') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama_barang',
                    name: 'nama_barang',
                    width: "15%"
                },
                {
                    data: 'kode_item',
                    name: 'kode_item',
                    width: "10%"
                },
                {
                    data: 'penyewa',
                    name: 'penyewa',
                    width: "15%"
                },
                {
                    data: 'role',
                    name: 'role',
                    width: "8%"
                },
                {
                    data: 'tgl_sewa',
                    name: 'tgl_sewa',
                    width: "12%"
                },
                {
                    data: 'tgl_kembali',
                    name: 'tgl_kembali',
                    width: "12%"
                },
                {
                    data: 'status_kembali',
                    name: 'status_kembali',
                    width: "8%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    title : 'peminjaman barang berjalan'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    title : 'peminjaman barang berjalan'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'peminjaman barang berjalan'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];
        MakeFullTable(tabel,url,column,buttons);
    })


        $('#form-create-peminjaman').on('submit',function(){
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type : "post",
                url  : "{{route('request-peminjaman')}}",
                data : data,
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modalCreatePeminjaman").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        });

        $('#form-edit-peminjaman').on('submit',function(){
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type : "post",
                url  : "{{route('edit-peminjaman')}}",
                data : data,
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modal-edit-peminjaman").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        });

        function modalEditPeminjaman(id){
            $.ajax({
                type : "get",
                url  : "{{route('detail-peminjaman')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                    $('#modal-edit-peminjaman').modal('show');
                    $('#input_keterangan').val(data.data.keterangan);
                    $('#input_id_item').val(data.data.id_item);
                    $('#input_id_user').val(data.data.id_user);
                    $('#input_id_sewa').val(id);
                    $('#input_nama_user').val(data.data.penyewa);
                    $('#input_barang').val(data.data.id_item);
                    $('#input_tgl_sewa').val(data.data.tgl_sewa);
                    $('#input_tgl_kembali').val(data.data.tgl_kembali);
                },
                error : function(data){
                    
                }
            });
        }

        function detailPeminjaman(id){
             $.ajax({
                type : "get",
                url  : "{{route('detail-peminjaman')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                    $('#modal-detail-peminjaman').modal('show');
                    $('#modal-detail').html(`
                        <div class="container">
                            <div class="row justify-content-between">
                                <section>
                                    <h4 class="card-title">${data.data.nama_barang}</h4>
                                    <p class="card-text">Kode : ${data.data.kode}</p>
                                    <h6 class="card-subtitle mb-2 text-muted">Keterangan : ${data.data.keterangan}</h6>
                                    
                                </section>

                                <section class="col-5">
                                    <div class="row">
                                        <div class="col-5">
                                            <p class="card-text mb-0">Peminjam</p>
                                            <p class="card-text">Role</p>
                                        </div>
                                        <div class="col-7">
                                            <p class="card-text mt-0">: ${data.data.penyewa}</p>
                                            <p class="card-text">: ${data.data.role}</p>
                                        </div>
                                    </div>
                                    
                                </section>
                            </div>
                            

                            <div class="container px-0 mt-2">

                                <div class="row">
                                    <div class="col-2 px-0">
                                        <p class="card-text mb-0">Tanggal sewa</p>
                                        <p class="card-text">Tanggal kembali</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="card-text mb-0">:  ${data.data.tgl_sewa}</p>
                                        <p class="card-text">:  ${data.data.tgl_kembali}</p>
                                    </div>
                                </div>

                            <div>

                            <div class="row flex-row-reverse">
                                <button onclick="kembaliPeminjaman(${data.data.id})" class="btn btn-success float-left">Barang kembali</button>
                            </div>
                            
                            
                        <div>
                    `);
                },
                error : function(data){
                    
                }
            });
        }

        function kembaliPeminjaman(id){
            $.ajax({
                type : "post",
                url  : "{{route('kembali-peminjaman')}}",
                data: {
                    'id' : id
                },
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $('#modal-edit-peminjaman').modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

</script>

@endsection