<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-peminjaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail peminjaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

<script type="text/javascript">


    function detailPeminjaman(id){
         $.ajax({
            type : "get",
            url  : "{{route('detail-peminjaman')}}",
            data : {
              'id' : id
            },
            success : function(data){
                console.log(data.data);
                $.ajax({
                    type : "get",
                    url  : "{{route('detail-user-barang')}}",
                    data : {
                      'id' : data.data.id_user
                    },
                    success : function(user){
                        console.log(user);
                        $('#modal-detail-peminjaman').modal('show');
                        $('#modal-detail').html(`
                            <div class="row justify-content-between px-3">
                                <section>
                                    <h4 class="card-title">${data.data.nama_barang}</h4>
                                    <p class="card-text">Kode : ${data.data.kode_item}</p>  
                                </section>
                                <section>
                                    <span class="badge badge-pill badge-primary">${data.data.status_kembali}</span>
                                </section>
                                     
                            </div>

                            <div class="row">
                                <div class="col-6 d-flex flex-column">
                                    <h5>Keterangan : </h5>
                                    <p>${data.data.keterangan}</p>
                                    <div class="row">
                                        <div class="col-5">
                                            <p class="card-text mb-0">Tanggal sewa</p>
                                            <p class="card-text">Tanggal kembali</p>
                                            <p class="card-text mb-0">Tarif</p>
                                        </div>
                                        <div class="col-7">
                                            <p class="card-text mb-0">: ${data.data.tgl_sewa}</p>
                                            <p class="card-text">: ${data.data.tgl_kembali}</p>
                                            <p class="card-text">: ${data.data.tarif_barang}</p>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="col-6 d-flex flex-column">
                                    <h5>Data Peminjam</h5>
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="card-text mb-0">Nama</p>
                                            <p class="card-text">Role</p>
                                            <p class="card-text">Telepon</p>
                                            <p class="card-text">Terakhir Login</p>
                                        </div>
                                        <div class="col-6">
                                            <p class="card-text mb-0">: ${user.data.nama}</p>
                                            <p class="card-text">: ${user.data.role}</p>
                                            <p class="card-text">: ${user.data.telepon}</p>
                                            <p class="card-text">: ${user.data.last_login}</p>
                                        </div>
                                        
                                    </div>
                                <div>
                                
                            <div>
                        `);
                    }
                });  
            },
            error : function(data){
                
            }
        });
    }

    function detailUser(id){
        $.ajax({
            type : "get",
            url  : "{{route('detail-user-barang')}}",
            data : {
              'id' : id
            },
            success : function(data){
                console.log(data.data);
            }
        });  
    }


</script>