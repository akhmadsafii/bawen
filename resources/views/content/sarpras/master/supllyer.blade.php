@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
    @if (Session::has('message'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>    
      </div>
    @endif
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Daftar Supllyer Barang</h4>
		<button type="button" class="btn btn-info" onclick="modalCreateSupllyer()">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-supllyer">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>File</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>
@include('content.sarpras.master.modal')
@include('content.sarpras.layout_sarpras')

<!-- modal create-->
  <div class="modal fade" id="modal-create-supllyer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <!-- <input type="text" name="routes" id="routes-create" class="form-control" > -->
              <form action="{{ route('buat-supllyer-barang') }}" method="POST" id="create-modal" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Telepon</label>
                    <input type="number" name="telepon" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">File</label>
                    <input type="file" name="image" class="form-control" required>   
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal edit-->
  <div class="modal fade" id="modal-edit-supllyer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <!-- <input type="text" name="routes" id="routes-edit" class="form-control" > -->
              <form action="{{ route('edit-supllyer-barang') }}" method="POST" id="edit-modal" enctype="multipart/form-data">
                @csrf
                <div id="modal-body-edit">
                  

                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal detail-->
  <div class="modal fade" id="modal-detail-supllyer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Detail Supllyer</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="modal-detail-body">

                
              </div>
            </div>

          </div>

        </div>
  </div>
<!-- end modal -->


<script>

    let tabel = $('#data-supllyer');

    $(function(){
      let url = "{{ route('main-supllyer-barang') }}";
      let column = 
          [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "10%"
              },
              {
                  data: 'nama',
                  name: 'nama',
                  width: "40%"
              },
              {
                  data: 'files',
                  name: 'files',
                  width: "20%"
              },
              {
                  data: 'aksi',
                  name: 'aksi',
                  width: "10%"
              }
          ]
      ;

      MakeTable(tabel,url,column);

    })

      function modalCreateSupllyer(){
          $('#modal-create-supllyer').modal('show');
      }

      function modalEditSupllyer(id){
        $('#modal-edit-supllyer').modal('show');


        $.ajax({
          type : "GET",
          url : "{{route('detail-supllyer-barang')}}",
          data : {
            'id' : id
          },
          success: function(data) {
            console.log(data);
            data = data.data;
            $('#modal-body-edit').html(`
                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nama</label>
                      <input type="text" name="nama" class="form-control" value="${data.nama}">  
                      <input type="hidden" name="id" class="form-control" value="${data.id}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Keterangan</label>
                      <input type="text" name="keterangan" class="form-control" value="${data.keterangan}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Telepon</label>
                      <input type="number" name="telepon" class="form-control" value="${data.telepon}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Alamat</label>
                      <input type="text" name="alamat" class="form-control" value="${data.alamat}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">File</label>
                      <input type="file" name="image" class="form-control" value="${data.file}">   
                  </div>
            `);
          }

        });
      }

      function deleteSupllyer(id){
        deleteItem("supllyer",id);
      }

      function modalDetailSupllyer(id){
        $('#modal-detail-supllyer').modal('show');


        $.ajax({
          type : "GET",
          url : "{{route('detail-supllyer-barang')}}",
          data : {
            'id' : id
          },
          success: function(data) {
            console.log(data);
            data = data.data;
            $('#modal-detail-body').html(`
              <div class="container">
                <h4>${data.nama}</h4>
                <div> 
                  <img src="${data.file}" class="img-radius" alt="User-Profile-Image"> 
                </div>
                <div class="row">
                  <div class="col-4">
                    <p>Keterangan</p>
                    <p>Alamat</p>
                    <p>Telepon</p>
                  </div>
                  <div class="col-8">
                    <p>${data.keterangan}</p>
                    <p>${data.alamat}</p>
                    <p>${data.telepon}</p>
                  </div>
                </div>
              </div>
            `);
          }
        });
      }

    


    

</script>

@endsection

