@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Daftar Lokasi Barang</h4>
		<button type="button" class="btn btn-info " onclick="modalCreate('lokasi')">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-lokasi">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>
@include('content.sarpras.master.modal')
@include('content.sarpras.layout_sarpras')

<script>
    
    $(function(){

        let tabel = $('#data-lokasi');
        let url = "{{ route('main-lokasi-barang') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "60%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        MakeTable(tabel,url,column);

    })
        
        function modalEditLokasi(id){
            $.ajax({
                type: "GET",
                url:"{{ route('detail-lokasi-barang') }}",
                data : {
                    'id' : id
                },
                success: function(data) {
                  console.log(data);
                  $('#nama-edit').val(data.data.nama);
                  $('#id-edit').val(id);
                }
            });
            $('#routes-edit').val("lokasi");
            $("#modal-edit").modal("show");
        }

        function deleteLokasi(id){
            deleteItem("lokasi",id);
        }

</script>

@endsection

