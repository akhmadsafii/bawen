@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Daftar Kategori Barang</h4>
		<button type="button" class="btn btn-info " onclick="modalCreateKategori()">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-kategori">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>
@include('content.sarpras.master.modal')
@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-kategori');
    
    $(function(){
        let url = "{{ route('main-kategori-barang') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'jenis',
                    name: 'jenis',
                    width: "10%"
                },
                {
                    data: 'keterangan',
                    name: 'keterangan',
                    width: "40%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        MakeTable(tabel,url,column);
    })

        modalCreateKategori = function (){
            $('#modal-create').modal('show');
            $('#routes-create').val("kategori");

            $('#modal-create-body').html(`
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" >   
                </div>

                <div class="form-group ">
                    <label for="recipient-name" class="col-form-label">Jenis</label>
                    <select name="id_jenis" class="form-control">
                      @foreach($jenis as $js)
                      <option value="{{$js['id']}}">{{$js['nama']}}</option>
                      @endforeach
                    </select>
                </div>
            `);
        }

        modalEditKategori = function (id){
            $('#routes-edit').val("kategori");
            $.ajax({
                type: "GET",
                url:"{{ route('detail-kategori-barang') }}",
                data : {
                    'id' : id
                },
                success: function(data) {
                  console.log(data);
                  $('#nama-edit').val(data.data.nama);
                  $('#id-edit').val(id);
                  $('#modal-edit-kategori').html(`
                    <div class="form-group ">
                        <label for="recipient-name" class="col-form-label">Jenis</label>
                        <select name="id_jenis" class="form-control" value="${data.data.id_jenis}">
                          @foreach($jenis as $js)
                          <option value="{{$js['id']}}">{{$js['nama']}}</option>
                          @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" value="${data.data.keterangan}" >   
                    </div>
                  `);
                }
            });
            $("#modal-edit").modal("show");
        }

        modalDetailKategori = function (id){
            $('#modal-detail').modal('show');
            $.ajax({
                type: "GET",
                url:"{{ route('detail-kategori-barang') }}",
                data : {
                    'id' : id
                },
                success: function(data) {
                    $('#content-detail').html(`
                        <h4 class="card-title" id="nama-detail">${data.data.nama}</h4>
                        <h5 class="card-text" id="keterangan-detail">${data.data.jenis}</h5>
                        <h5 class="card-text" id="jenis-detail">${data.data.keterangan}</h5>
                    `);
                },
            });
            
        }

        deleteKategori = function (id){
            deleteItem("kategori",id);
        }

</script>

@endsection

