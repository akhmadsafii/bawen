<!-- modal create-->
  <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="routes" id="routes-create" class="form-control" >
              <form action="javascript:void(0)" id="create-modal" enctype="multipart/form-data">
                @csrf
                <div id="modal-create-body">
                  


                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal detail -->
  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <section id="content-detail" class="container">
            
          </section>
        </div>
      </div>
    </div>
  </div>
<!-- end modal detail -->

<!-- modal edit kategori -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-edit-modal">
              <input type="hidden" name="nama" id="routes-edit" class="form-control" >
              <form action="javascript:void(0)" id="edit-modal" enctype="multipart/form-data">
                @csrf
                <div id="modal-edit-body">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama-edit">
                    <input type="hidden" name="id" class="form-control" id="id-edit">
                  </div>
                </div>

                <div id="modal-edit-kategori"></div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="addsub-kategori" class="btn btn-success">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

            </div>
        </div>
    </div>
<!-- end modal -->