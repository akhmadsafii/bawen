@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  <h4 class="my-2 title-content">Daftar Item</h4>

  <div class="row justify-content-between align-items-end my-4">
      <section class="col-10 d-flex flex-row g-1">
          <div class="col-md-3">
            <label for="inputEmail4">Kategori</label>
            <select id="filter_kategori" class="form-control">
              <option value="">Semua</option>
              @foreach($kategoris as $kategori)
                  <option value="{{$kategori['nama']}}">{{$kategori['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Jenis</label>
            <select id="filter_jenis" class="form-control">
              <option value="">Semua</option>
              @foreach($jeniss as $jenis)
                  <option value="{{$jenis['nama']}}">{{$jenis['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Lokasi</label>
            <select id="filter_lokasi" class="form-control">
              <option value="">Semua</option>
              @foreach($lokasis as $lokasi)
                  <option value="{{$lokasi['nama']}}">{{$lokasi['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Kondisi</label>
            <select id="filter_kondisi" class="form-control">
              <option value="">Semua</option>
              <option value="baik">baik</option>
              <option value="rusak ringan">rusak ringan</option>
              <option value="rusak berat">rusak berat</option>
            </select>
          </div>
      </section>
      

      <button type="button" class="btn btn-info " onclick="modalCreateItem()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
      </button>
    

  </div>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-item">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Jenis</th>
                    <th>Kategori</th>
                    <th>Kondisi</th>
                    <th>Lokasi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


<!-- modal create-->
  <div class="modal fade" id="modal-create-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>        

            <div class="modal-body">
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-create-item">
                @csrf

                <div class="form-group">
                  <label for="inputEmail4">Barang</label>
                  <select name="id_barang" id="barang" class="form-control">
                    @foreach($barangs as $barang)
                        <option value="{{$barang['id']}}">{{$barang['nama']}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-row">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Kode</label>
                        <input type="text" name="kode" class="form-control" >   
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Lokasi</label>
                      <select name="id_lokasi" id="lokasi" class="form-control" id="input-lokasi">
                        @foreach($lokasis as $lokasi)
                            <option value="{{$lokasi['id']}}">{{$lokasi['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kondisi</label>
                      <select name="kondisi" id="kondisi" class="form-control" id="input-kondisi">
                        <option value="baik">baik</option>
                        <option value="rusak ringan">rusak ringan</option>
                        <option value="rusak berat">rusak berat</option>
                      </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Jumlah</label>
                      <input type="number" name="jumlah" class="form-control" >  
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Tanggal Diterima</label>
                      <input type="date" name="tgl_diterima" class="form-control" >  
                    </div>
                </div>

                

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal edit-->
  <div class="modal fade" id="modal-edit-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="javascript:void(0)" enctype="multipart/form-data" id="form-edit-item" >
                @csrf

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="input-nama" readonly>   
                    <input type="hidden" name="id" class="form-control" id="input-id"> 
                    <label for="recipient-name" class="col-form-label">Kode</label>
                    <input type="text" name="kode" class="form-control" id="input-kode" readonly>
                </div>
                
                <input type="hidden" name="id_pengadaan" class="form-control" id="id-ada"> 
                
                <input type="hidden" name="id_barang" class="form-control" id="id-barang"> 



                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Lokasi</label>
                      <select name="id_lokasi" id="lokasi" class="form-control" id="input-lokasi">
                        @foreach($lokasis as $lokasi)
                            <option value="{{$lokasi['id']}}">{{$lokasi['nama']}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Kondisi</label>
                      <select name="kondisi" id="kondisi" class="form-control" id="input-kondisi">
                        <option value="baik">baik</option>
                        <option value="rusak ringan">rusak ringan</option>
                        <option value="rusak berat">rusak berat</option>
                      </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

@include('content.sarpras.layout_sarpras')

<script>


    let tabel = $('#data-item');

    $(function(){
      let url = "{{ route('daftar-item') }}";
      let column = 
          [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "10%"
              },
              {
                  data: 'nama',
                  name: 'nama',
                  width: "25%"
              },
              {
                  data: 'kode',
                  name: 'kode',
                  width: "10%"
              },
              {
                  data: 'jenis',
                  name: 'jenis',
                  width: "10%",
                  visible: false,
                  searchable: true
              },
              {
                  data: 'kategori',
                  name: 'kategori',
                  width: "15%",
                  visible: true,
                  searchable: true
              },
              {
                  data: 'kondisi',
                  name: 'kondisi',
                  width: "10%",
                  visible: true,
                  searchable: true
              },
              {
                  data: 'lokasi',
                  name: 'lokasi',
                  width: "10%",
                  visible: true,
                  searchable: true
              },
              {
                  data: 'aksi',
                  name: 'aksi',
                  width: "10%"
              }
          ];

      let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 , 4 , 5 , 6]
                  },
                  title : 'Daftar item'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 , 4 , 5 , 6]
                  },
                  title : 'Daftar item'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 , 4 , 5 , 6]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar item'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

      MakeFullTable(tabel,url,column,buttons);

    })

    

      function modalCreateItem(){
          $('#modal-create-item').modal('show');
      }

      $('#detail-create-item').on('hidden.bs.modal', function() {
        $(this).find('form').trigger('reset');
      });
      $('#detail-edit-item').on('hidden.bs.modal', function() {
        $(this).find('form').trigger('reset');
      });

      function modalEditItem(id){       
          $.ajax({
              type : "post",
              url  : "{{route('detail-item')}}",
              data : {
                'id' : id
              },
              success : function(data){
                console.log(data.data);
                  $('#modal-edit-item').modal('show');
                  $('#input-id').val(data.data.id);
                  $('#id-barang').val(data.data.id_barang);
                  $('#id-ada').val(data.data.id_pengadaan);
                  $('#input-kode').val(data.data.kode);
                  $('#input-nama').val(data.data.nama);
                  $('#input-lokasi').val(data.data.id_lokasi);
                  
              },
              error : function(data){
                  
              }
          });

      }

      function riwayatItem(id){
        $.ajax({
              type : "post",
              url  : "{{route('redirect-item-history')}}",
              data: {
                  'id' : id
              },
              success : (data) =>{
                  // console.log(data);
                  window.location = data;
              },
              error: function(data) {
                  noti(data.icon, data.message);   
              } 
          }); 
      }

      function deleteItem(id){
        $.ajax({
              type : "get",
              url  : "{{route('hapus-item')}}",
              data : {
                'id' : id
              },
              success : function(data){
                  $('#data-item').DataTable().ajax.reload();
                  noti(data.icon, data.message);
              }
        });

      }

      $('#form-create-item').on('submit',function(event) {
          let data = $(this).serialize();
          console.log(data);
          $.ajax({
              type : "post",
              url  : "{{route('buat-item')}}",
              data : data,
              success : function(data){
                  $('#data-item').DataTable().ajax.reload();
                  noti(data.icon, data.message);
                  $("#modal-create-item").modal('hide');
              },
              error : function(data){
                  noti(data.icon, data.message);
              }
          });
      });

      $('#form-edit-item').on('submit',function(event) {
          let data = $(this).serialize();
          console.log(data);
          $.ajax({
              type : "post",
              url  : "{{route('edit-item')}}",
              data : data,
              success : function(data){
                console.log(data);
                  $('#data-item').DataTable().ajax.reload();
                  noti(data.icon, data.message);
                  $("#modal-edit-item").modal('hide');
              },
              error : function(data){
                  noti(data.icon, data.message);
              }
          });
      });

      $('#filter_jenis').change(function( ){
          $('#data-item').DataTable().columns(3)
          .search($(this).val()).draw();
      }); 

      $('#filter_kategori').change(function( ){
          $('#data-item').DataTable().columns(4)
          .search($(this).val()).draw();
      }); 

      $('#filter_kondisi').change(function( ){
          $('#data-item').DataTable().columns(5)
          .search($(this).val()).draw();
      }); 

      $('#filter_lokasi').change(function( ){
          $('#data-item').DataTable().columns(6)
          .search($(this).val()).draw();
      }); 


    

</script>

@endsection

