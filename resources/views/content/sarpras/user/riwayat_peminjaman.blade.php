@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  <h4 class="mt-3 title-content">Riwayat peminjaman {{$user['nama']}}</h4>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-riwayat-user">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Barang</th>
                    <th>Kode</th>
                    <th>Tanggal sewa</th>
                    <th>Tanggal Kembali</th>
                    <th>Keterangan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-riwayat-user');

    $(function(){

        
        let urls = document.URL;
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama_barang',
                    name: 'nama_barang',
                    width: "20%",
                    orderable :true
                },
                {
                    data: 'kode_item',
                    name: 'kode_item'
                },
                {
                    data: 'tgl_sewa',
                    name: 'tgl_sewa'
                },
                {
                    data: 'tgl_kembali',
                    name: 'tgl_kembali'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'status_kembali',
                    name: 'status_kembali',
                    width: "10%"
                }
            ];
        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,urls,column,buttons);

    })



</script>

@endsection

