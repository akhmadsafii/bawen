
<!-- modal create admin-->
  <div class="modal fade bd-example-modal-lg" id="modal-create-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="routes" id="routes-create-user" class="form-control" >
              <form action="{{route('buat-admin-barang')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="inputNama" class="">Upload Foto : </label>
                    <div id="inputNama" class="col-sm-12 clearfix">
                      <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*" onchange="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Username</label>
                    <input type="text" name="username" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Email</label>
                    <input type="text" name="email" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Password</label>
                    <input type="text" name="first_password" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="inputState">Jenis Kelamin</label>
                    <select name="jenkel" id="inputState" class="form-control">
                      <option value="l">Laki-laki</option>
                      <option value="p">Permepuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="">Agama</label>
                      <select name="agama" id="agama" class="form-control">
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                      </select>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Tempat lahir</label>
                      <input type="text" name="tempat_lahir" class="form-control" >   
                    </div>

                    <div class="form-group col-md-6">
                      <label for="tgl_lahir">Tanggal lahir</label>
                      <input type="date" name="tgl_lahir" class="form-control" >   
                    </div>
                </div>


            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal create user-->
  <div class="modal fade bd-example-modal-lg" id="modal-create-users" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="routes" id="routes-create-user" class="form-control" >
              <form action="{{route('buat-user-barang')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="inputNama" class="">Upload Foto : </label>      
                    <div id="inputNama" class="col-sm-12 clearfix">
                      <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*" onchange="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Username</label>
                    <input type="text" name="username" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Email</label>
                    <input type="text" name="email" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Password</label>
                    <input type="text" name="first_password" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="inputState">Jenis Kelamin</label>
                    <select name="jenkel" id="inputState" class="form-control">
                      <option value="l">Laki-laki</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="">Agama</label>
                      <select name="agama" id="agama" class="form-control">
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                      </select>
                </div>

                <div class="form-group">
                    <label class="">Role</label>
                      <select name="role" id="role" class="form-control">
                        <option value="siswa">siswa</option>
                        <option value="guru">guru</option>
                      </select>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Tempat lahir</label>
                      <input type="text" name="tempat_lahir" class="form-control" >   
                    </div>

                    <div class="form-group col-md-6">
                      <label for="tgl_lahir">Tanggal lahir</label>
                      <input type="date" name="tgl_lahir" class="form-control" >   
                    </div>
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- modal detail -->
      <div class="modal fade bd-example-modal-lg" id="modal-detail-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="" id="detailUser">

              </div>
            </div>

          </div>
        </div>
      </div>
<!-- end modal detail -->
