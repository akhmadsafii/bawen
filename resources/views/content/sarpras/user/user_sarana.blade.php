@extends('template.template_default.app')
@section('content')


<style type="text/css">
  .pace{
    display: none;
  }

  .input-group a{
      color: #fff !important;
   }

  body {
      background-color: #f9f9fa
  }

  .padding {
      padding: 3rem !important
  }

  .user-card-full {
      overflow: hidden
  }

  .card {
      border: none;
      margin-bottom: 0;
  }

  .m-r-0 {
      margin-right: 0px
  }

  .m-l-0 {
      margin-left: 0px
  }

  .user-card-full .user-profile {
      border-radius: 5px 0 0 5px
  }

  .bg-c-lite-green {
      background: -webkit-gradient(linear, left top, right top, from(#f29263), to(#ee5a6f));
      background: linear-gradient(to right, #ee5a6f, #f29263)
  }

  .user-profile {
      padding: 20px 0
  }

  .card-block {
      padding: 1.25rem
  }

  .m-b-25 {
      margin-bottom: 25px
  }

  .img-radius {
      border-radius: 5px
  }

  h6 {
      font-size: 14px
  }

  .card .card-block p {
      line-height: 25px
  }

  @media only screen and (min-width: 1400px) {
      p {
          font-size: 14px
      }
  }

  .card-block {
      padding: 1.25rem
  }

  .card-text p {
    line-height: 8px !important;
  }


  .b-b-default {
      border-bottom: 1px solid #e0e0e0
  }

  .m-b-20 {
      margin-bottom: 20px
  }

  .p-b-5 {
      padding-bottom: 5px !important
  }

  .card .card-block p {
      line-height: 25px
  }

  .m-b-10 {
      margin-bottom: 0;
  }

  .text-muted {
      color: #919aa3 !important
  }

  .b-b-default {
      border-bottom: 1px solid #e0e0e0
  }

  .f-w-600 {
      font-weight: 600
  }

  .m-b-20 {
      margin-bottom: 20px
  }

  .m-t-40 {
      margin-top: 20px
  }

  .p-b-5 {
      padding-bottom: 5px !important
  }

  .m-b-10 {
      margin-bottom: 10px
  }

  .m-t-40 {
      margin-top: 20px
  }

  .user-card-full .social-link li {
      display: inline-block
  }

  .user-card-full .social-link li a {
      font-size: 20px;
      margin: 0 10px 0 0;
      -webkit-transition: all 0.3s ease-in-out;
      transition: all 0.3s ease-in-out
  }

</style>

<div class="container bg-white p-5">
  @if (Session::has('message'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>    
      </div>
    @endif
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Daftar User</h4>
		<button type="button" class="btn btn-info " onclick="modalCreateUser('user') ">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-user">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>

<!-- modal edit user-->
      <div class="modal fade bd-example-modal-lg" id="modal-edit-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
          	<!-- modal header -->
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- modal body -->
            <div class="modal-body px-4">
              <form action="{{route('edit-user-barang')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div id="content-edit">
	               
	            </div>
	            <!-- end content edit -->
            </div>
           <!--  end modal body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->


@include('content.sarpras.user.modal_user')
@include('content.sarpras.layout_sarpras')

<script>


  let tabel = $('#data-user');

  $(function(){

      let url = "{{ route('main-user-barang') }}";
      let column = 
          [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "10%"
              },
              {
                  data: 'nama',
                  name: 'nama',
                  width: "60%"
              },
              {
                  data: 'aksi',
                  name: 'aksi',
                  width: "10%"
              }
          ]
      ;

      MakeTable(tabel,url,column);
  })



      function modalEditUser(id){
          $.ajax({
              type: "GET",
              url:"{{ route('detail-user-barang') }}",
              data : {
                  'id' : id
              },
              success: function(data) {
                console.log(data);
                data = data.data;
                	$('#content-edit').html(`
                	<div class="form-group">
                      <label for="inputNama" class="">Upload Foto : </label>
                      
                      
                      <div id="inputNama" class="col-sm-12 clearfix">
                        <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*" onchange="">
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nama</label>
                      <input type="text" name="nama" class="form-control" value="${data.nama}">  
                      <input type="hidden" name="id" class="form-control" value="${data.id}">  
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Username</label>
                      <input type="text" name="username" class="form-control" value="${data.username}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Email</label>
                      <input type="text" name="email" class="form-control" value="${data.email}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Alamat</label>
                      <input type="text" name="alamat" class="form-control" value="${data.alamat}">   
                  </div>

                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Telepon</label>
                      <input type="text" name="telepon" class="form-control" value="${data.telepon}">   
                  </div>

                  <div class="form-group">
                      <label for="inputState">Jenis Kelamin</label>
                      <select name="jenkel" id="inputState" class="form-control" value="${data.jenkel}">
                        <option value="l">Laki-laki</option>
                      </select>
                  </div>

                  <div class="form-group">
                      <label class="">Agama</label>
                        <select name="agama" id="agama" class="form-control" value="${data.agama}">
                          <option value="islam">Islam</option>
                          <option value="kristen">Kristen</option>
                          <option value="hindu">Hindu</option>
                          <option value="budha">Budha</option>
                        </select>
                  </div>

                  <div class="form-group">
                      <label class="">Role</label>
                        <select name="role" id="role" class="form-control" value="${data.role}">
                          <option value="siswa">siswa</option>
                          <option value="guru">guru</option>
                        </select>
                  </div>

                  <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Tempat lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control" value="${data.tempat_lahir}">   
                      </div>

                      <div class="form-group col-md-6">
                        <label for="tgl_lahir">Tanggal lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control" value="${data.tgl_lahir}">   
                      </div>
                  </div>

                `);
                

                $('#id-edit').val(id);
              }
          });

          $("#modal-edit-user").modal("show");
      }

      function deleteJenis(id){
          deleteItem("jenis",id);
      }

      function riwayatUser(id){
        $.ajax({
              type : "post",
              url  : "{{route('redirect-user-history')}}",
              data: {
                  'id' : id
              },
              success : (data) =>{
                  // console.log(data);
                  window.location = data;
              },
              error: function(data) {
                  noti(data.icon, data.message);   
              } 
          }); 
      }

  

</script>

@endsection

