@extends('template.template_default.app')
@section('content')
<style type="text/css">
    .pace{
        display: none;
    }
</style>

<section class="container">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Usulan Penambahan Barang</h3>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCreatePengadaan">Request</button>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pengadaan-admin">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Tanggal pengajuan</th>
                    <th>Total harga</th>
                    <th>Prioritas</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</section>

<!-- Modal Detail (tidak jadi,mengunggu alur yg jelas--> 
<div class="modal fade" id="modal-detail-pengadaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                <form id="form-action-pengadaan" action="javascript:void(0)">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal diterima</label>
                        <input type="date" name="tgl_diterima" id="input_tgl" class="form-control" > 
                        <input type="text" name="id" id="input_id" class="form-control" >  
                        <input type="text" name="kode" id="input_kode" class="form-control" >    
                    </div>
                </form>
                <button type="button" class="btn btn-primary btn-sm" onclick="action(1)">Terima</button>
                <button type="button" class="btn btn-primary btn-sm" onclick="action(2)">Tolak</button>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Detail -->


@include('content.sarpras.layout_sarpras')

<script>
    let tabel = $('#data-pengadaan-admin');

    $(function(){

        let url = "{{ route('admin-page-pengadaan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'barang',
                    name: 'barang',
                    width: "20%"
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    width: "5%"
                },
                {
                    data: 'tgl_pengajuan',
                    name: 'tgl_pengajuan',
                    width: "20%"
                },
                {
                    data: 'total_harga',
                    name: 'total_harga',
                    width: "20%"
                },
                {
                    data: 'prioritas',
                    name: 'prioritas',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    text: '<i class="fa fa-upload"></i>',
                    className: 'btn btn-sm',
                    attr: {
                        title: 'Import Data',
                        id: 'import'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];


        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column,
            buttons: buttons,
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var totalHarga = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                }, 0 );

                $( api.column( 4 ).footer() ).html('Total harga : ');
                $( api.column( 5 ).footer() ).html(totalHarga);
            }
            
        });
    });

        

        function approve(id){

            $.ajax({
                type : "post",
                url  : "{{route('approve-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

        function reject(id){
            $.ajax({
                type : "post",
                url  : "{{route('reject-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

        function aksi(id){
            $('#modal-detail-pengadaan').modal('show');

             $.ajax({
                type : "get",
                url  : "{{route('detail-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    data = data.data;
                    $('#input_id').val(data.id);
                    $('#input_kode').val(data.kode);
                }
            });

            
        }

        function action(id){
            console.log(id)
            let id_pengadaan = $('#input_id').val();
            let kode_pengadaan = $('#input_kode').val();
            let tgl = $('#input_tgl').val();

            $.ajax({
                type : "post",
                url  : "{{route('aksi-pengadaan')}}",
                data : {
                  'id_aksi' : id,
                  'id_pengadaan' : id_pengadaan,
                  'tgl_diterima' : tgl,
                  'kode_pengadaan' : kode_pengadaan
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

    
</script>

@endsection