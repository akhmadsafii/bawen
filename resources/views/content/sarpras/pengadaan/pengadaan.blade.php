@extends('template.template_default.app')
@section('content')
<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Usulan pengadaan barang</h3>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCreatePengadaan">Request</button>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pengadaan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Tanggal pengajuan</th>
                    <th>Total harga</th>
                    <th>Prioritas</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
                        
			</tbody>
        </table>
    </div>
</section>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modalCreatePengadaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Request pengadaan barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" id="form-create-pengadaan">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Kode</label>
                        <input type="text" name="kode" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Prioritas</label>
                        <select id="inputState" name="prioritas" class="form-control">
                            <option value="normal">Normal</option>
                            <option value="mendesak">Mendesak</option>
                        </select>
                    </div>

                    <div class="form-group ">
                          <label for="inputEmail4">Barang</label>
                          <select name="id_barang" id="satuan" class="form-control">
                            @foreach($barangs as $barang)
                                <option value="{{$barang['id']}}">{{$barang['nama']}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal diterima</label>
                        <input type="date" name="tgl_diterima" id="input_tgl" class="form-control" > 
                    </div>

                    <div class="form-row">
                        <div class="col-4">
                            <label for="recipient-name" class="col-form-label">Jumlah</label>
                            <input type="number" name="jumlah" class="form-control" placeholder="">
                        </div>
                        <div class="col-8">
                            <label for="recipient-name" class="col-form-label">Harga</label>
                            <input type="number" class="form-control" placeholder="" name="harga_satuan">
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Request</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Create -->

<!-- Modal Edit-->
<div class="modal fade" id="modal-edit-pengadaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-pengadaan" action="javascript:void(0)">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama</label>
                        <input type="text" name="nama" id="input_nama" class="form-control" > 
                        <input type="hidden" name="id" id="input_id" class="form-control" >    
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Kode</label>
                        <input type="text" name="kode" id="input_kode" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" id="input_keterangan" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Prioritas</label>
                        <select id="inputState" name="prioritas" id="input_prior" class="form-control">
                            <option value="normal">Normal</option>
                            <option value="mendesak">Mendesak</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal diterima</label>
                        <input type="date" name="tgl_diterima" id="input_tgl_diterima" class="form-control" > 
                    </div>

                    <div class="form-group ">
                          <label for="inputEmail4">Barang</label>
                          <select name="id_barang" id="input_barang" class="form-control">
                            @foreach($barangs as $barang)
                                <option value="{{$barang['id']}}">{{$barang['nama']}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-row">
                        <div class="col-4">
                            <label for="recipient-name" class="col-form-label">Jumlah</label>
                            <input type="number" name="jumlah" id="input_jumlah" class="form-control" placeholder="">
                        </div>
                        <div class="col-8">
                            <label for="recipient-name" class="col-form-label">Harga</label>
                            <input type="number" class="form-control" id="input_harga" placeholder="" name="harga_satuan">
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

            </form>
        </div>
    </div>
</div>

<!-- Modal Detail-->
<div class="modal fade" id="modal-detail-pengadaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-pengadaan');

    $(function(){  
        let url = "{{ route('daftar-pengadaan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'barang',
                    name: 'barang',
                    width: "20%"
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    width: "5%"
                },
                {
                    data: 'tgl_pengajuan',
                    name: 'tgl_pengajuan',
                    width: "20%"
                },
                {
                    data: 'total_harga',
                    name: 'total_harga',
                    width: "20%"
                },
                {
                    data: 'prioritas',
                    name: 'prioritas',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ];

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    title : 'Daftar usulan pengadaan'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    title : 'Daftar usulan pengadaan'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Daftar usulan pengadaan'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        //MakeFullTable(tabel,url,column,buttons);
        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column,
            buttons: buttons,
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var totalHarga = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                }, 0 );

                $( api.column( 4 ).footer() ).html('Total harga : ');
                $( api.column( 5 ).footer() ).html(totalHarga.toLocaleString());
            },
            columnDefs:
                [
                    {
                        targets: 5,
                        render: $.fn.dataTable.render.number(',', '.', 0, '')
                    },
            ]
            
        });
    });

        $('#form-create-pengadaan').on('submit',function(){
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type : "post",
                url  : "{{route('request-pengadaan')}}",
                data : data,
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modalCreatePengadaan").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        });

        $('#form-edit-pengadaan').on('submit',function(){
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type : "post",
                url  : "{{route('edit-pengadaan')}}",
                data : data,
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modal-edit-pengadaan").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        });

        $('#modalCreatePengadaan').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

        $('#modal-edit-pengadaan').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

        function modalEditPengadaan(id){
            $.ajax({
                type : "get",
                url  : "{{route('detail-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                    $('#modal-edit-pengadaan').modal('show');
                    $('#input_id').val(data.data.id);
                    $('#input_kode').val(data.data.kode);
                    $('#input_tgl_diterima').val(data.data.tgl_diterima);
                    $('#input_nama').val(data.data.nama);
                    $('#input_keterangan').val(data.data.keterangan);
                    $('#input_prior').val(data.data.prioritas);
                    $('#input_barang').val(data.data.id_barang);
                    $('#input_harga').val(data.data.harga_satuan);
                    $('#input_jumlah').val(data.data.jumlah);
                },
                error : function(data){
                    
                }
            });
        }

        function detailPengadaan(id){
             $.ajax({
                type : "get",
                url  : "{{route('detail-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                  console.log(data.data);
                    $('#modal-detail-pengadaan').modal('show');
                    $('#modal-detail').html(`
                        <div class="container">
                            <h5 class="card-title">${data.data.nama}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Keterangan : ${data.data.keterangan}</h6>
                            <p class="card-text">Kode : ${data.data.kode}</p>

                            <div class="container px-0">
                                <div class="row">
                                    <div class="col-4">
                                        <p class="card-text">Harga satuan</p>
                                        <p class="card-text">Jumlah</p>
                                        <p class="card-text">Total harga</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="card-text">: ${data.data.harga_satuan}</p>
                                        <p class="card-text">: ${data.data.jumlah}</p>
                                        <p class="card-text">: Rp. ${data.data.total_harga.toLocaleString()}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-4">
                                        <p class="card-text">Prioritas</p>
                                        <p class="card-text">Tanggal pengajuan</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="card-text">: ${data.data.prioritas}</p>
                                        <p class="card-text">: ${data.data.tgl_pengajuan}</p>
                                    </div>
                                </div>
                            <div>
                            
                            <div class="row justify-content-end">
                                <button type="button" class="btn btn-success btn-sm mr-1" onclick="approve(${data.data.id})">Terima</button>
                                <button type="button" class="btn btn-primary btn-sm" onclick="reject(${data.data.id})">Tolak</button>
                            </div>
                        <div>
                    `);
                },
                error : function(data){
                    
                }
            });
        }

        function deletePengadaan(id){
            $.ajax({
                type : "get",
                url  : "{{route('delete-pengadaan')}}",
                data: {
                    'id' : id
                },
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

        function approve(id){

            $.ajax({
                type : "post",
                url  : "{{route('approve-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $('#modal-detail-pengadaan').modal('hide');
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

        function reject(id){
            $.ajax({
                type : "post",
                url  : "{{route('reject-pengadaan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $('#modal-detail-pengadaan').modal('hide');
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

    

</script>

@endsection