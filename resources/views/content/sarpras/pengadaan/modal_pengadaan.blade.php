<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-pengadaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

<script type="text/javascript">
    
    function detailPengadaan(id){
        $.ajax({
            type : "get",
            url  : "{{route('detail-pengadaan')}}",
            data : {
              'id' : id
            },
            success : function(data){
              console.log(data.data);
                $('#modal-detail-pengadaan').modal('show');
                $('#modal-detail').html(`
                    <div class="container">
                        <div class="row">
                            <div class="col-9">
                                <h5 class="card-title text-dark">${data.data.nama}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">Keterangan : ${data.data.keterangan}</h6>
                            </div>
                            <div class="col-3">
                                <h5 class="card-title text-dark">Kode : ${data.data.kode}</h5>
                            </div>
                        </div>
                        

                        <div class="row px-0 mt-2">
                            <div class="col-6 d-flex flex-row px-0">
                                <div class="col-4">
                                    <p class="card-text">Harga satuan</p>
                                    <p class="card-text">Jumlah</p>
                                    <p class="card-text">Total harga</p>
                                </div>
                                <div class="col-6">
                                    <p class="card-text">: ${data.data.harga_satuan}</p>
                                    <p class="card-text">: ${data.data.jumlah}</p>
                                    <p class="card-text">: ${data.data.total_harga}</p>
                                </div>
                            </div>

                            <div class="col-6 d-flex flex-row px-0">
                                <div class="col-6">
                                    <p class="card-text">Prioritas</p>
                                    <p class="card-text">Tanggal pengajuan</p>
                                    <p class="card-text">Tanggal diterima</p>
                                </div>
                                <div class="col-6">
                                    <p class="card-text">: ${data.data.prioritas}</p>
                                    <p class="card-text">: ${data.data.tgl_pengajuan}</p>
                                    <p class="card-text">: ${data.data.tgl_diterima}</p>
                                </div>
                            </div>
                        <div>
                        
                        
                    <div>
                `);
            },
            error : function(data){
                
            }
        });
    }

</script>