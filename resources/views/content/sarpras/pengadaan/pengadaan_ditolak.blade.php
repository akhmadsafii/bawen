@extends('template.template_default.app')
@section('content')
<style type="text/css">
    .pace{
        display: none;
    }
    p {
        margin-bottom: 5px !important;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center bg-white">
        <h3>Usulan Barang Ditolak</h3>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pengadaan-rejected">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Tanggal pengajuan</th>
                    <th>Prioritas</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>

@include('content.sarpras.layout_sarpras')
@include('content.sarpras.pengadaan.modal_pengadaan')

<script>

    let tabel = $('#data-pengadaan-rejected');

    $(function(){
        let url = "{{ route('rejected-pengadaan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'barang',
                    name: 'barang',
                    width: "20%"
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    width: "5%"
                },
                {
                    data: 'tgl_pengajuan',
                    name: 'tgl_pengajuan',
                    width: "20%"
                },
                {
                    data: 'prioritas',
                    name: 'prioritas',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                },
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    title : 'Daftar usulan pengadaan ditolak'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    title : 'Daftar usulan pengadaan ditolak'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Daftar usulan pengadaan ditolak'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);

    })



</script>

@endsection