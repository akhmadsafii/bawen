@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success pull-right mx-1" id="addJadwal"><i class="fas fa-plus-circle"></i>
                                Tambah</button>
                        </div>
                    </div>
                    <hr>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="vertical-middle text-center">Hari</th>
                                    <th colspan="2" class="text-center">Jam Sekolah</th>
                                    <th rowspan="2" class="vertical-middle text-center">Keterangan</th>
                                    <th rowspan="2" class="vertical-middle text-center">Status</th>
                                    <th rowspan="2" class="vertical-middle text-center">Aksi</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Jam Masuk</th>
                                    <th class="text-center">Jam Pulang</th>
                                </tr>
                            </thead>
                            <tbody id="data_jadwal">
                                @if (!empty($jadwal))
                                    @foreach ($jadwal as $jd)
                                        <tr>
                                            <td class="text-center">{{ $jd['hari'] }}</td>
                                            <td class="text-center">{{ $jd['jam_masuk'] }}</td>
                                            <td class="text-center">{{ $jd['jam_pulang'] }}</td>
                                            <td>{{ $jd['keterangan'] }}</td>
                                            <td class="text-center">
                                                <label class="switch">
                                                    <input type="checkbox"
                                                        {{ $jd['status'] == true ? 'checked' : '' }} class="check_jadwal" data-id="{{ $jd['id'] }}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td class="text-center">
                                                <a href="javascript:void(0)" class="btn btn-info btn-sm edit" data-id="{{ $jd['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="{{ $jd['id'] }}"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Data saat ini tidak tersedia</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalJadwal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="modelHeading"></h5>
            </div>
            <form id="formJadwal" class="form-horizontal">
                @csrf
                <div class="modal-body">
                    <span id="form_result"></span>
                    <input type="hidden" name="id" id="id_jadwal">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="l34">Hari</label>
                                <div class="input-group">
                                    <select name="hari" id="hari" class="form-control">
                                        <option value="">Pilih Hari</option>
                                        <option value="Senin">Senin</option>
                                        <option value="Selasa">Selasa</option>
                                        <option value="Rabu">Rabu</option>
                                        <option value="Kamis">Kamis</option>
                                        <option value="Jumat">Jumat</option>
                                        <option value="Sabtu">Sabtu</option>
                                        <option value="Minggu">Minggu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="l36">Jam Masuk</label>
                                <input class="form-control" id="jam_masuk" name="jam_masuk" type="time">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="l37">Jam Pulang</label>
                                <input class="form-control" id="jam_pulang" name="jam_pulang"  type="time">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="l34">Keterangan</label>
                                <div class="input-group">
                                    <textarea name="keterangan" id="keterangan" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                </div>
            </form>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '#addJadwal', function() {
                $('#formLibur').trigger("reset");
                $('#modelHeading').html("Tambah Setting Jadwal");
                $('#modalJadwal').modal('show');
                $('#action').val('Add');
            });

            $('#formJadwal').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('absensi-jadwal_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('absensi-jadwal_create') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalJadwal').modal('hide');
                            $('#formJadwal').trigger("reset");
                            $('#data_jadwal').html(data.jadwal);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-jadwal_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Libur");
                        $('#id_jadwal').val(data.id);
                        $('#hari').val(data.hari);
                        $('#jam_masuk').val(data.jam_masuk);
                        $('#jam_pulang').val(data.jam_pulang);
                        $('#keterangan').val(data.keterangan);
                        $('#action').val('Edit');
                        $('#modalJadwal').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-jadwal_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_jadwal').html(data.jadwal);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.check_jadwal', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-jadwal_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });


            
        })
    </script>
@endsection
