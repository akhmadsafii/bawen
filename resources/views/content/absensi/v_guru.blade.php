@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_length {
            margin: 0 !important;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover responsive nowrap" id="data-tabel"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Profile</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '.guru_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-guru_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.addGuru', function() {
                let id = $(this).data('id');
                let loader = $(this);
                let role = 'guru';
                if (confirm('Apa kamu yakin ingin menyicronkan guru ke akun absensi?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-siswa_create') }}",
                        data: {
                            id,
                            role
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fas fa-sync-alt fa-spin"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            $(loader).html('<i class="fas fa-sync-alt"></i>');
                            noti(data.icon, data.message);
                        }
                    });

                }
            });

            $(document).on('click', '.reset_pass', function() {
                let id = $(this).data('id');
                let loader = $(this);
                let role = 'siswa';
                if (confirm('Apakah anda yakin ingin mereset password siswa?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-siswa_create') }}",
                        data: {
                            id,
                            role
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fas fa-sync-alt fa-spin"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_siswa').html(data.siswa);
                            } else {
                                $(loader).html('<i class="fas fa-sync-alt"></i>');
                            }
                            noti(data.icon, data.message);
                        }
                    });

                }
            });
        })

        function checkAll(bx) {
            var cbs = $('.check_siswa');
            for (var i = 0; i < cbs.length; i++) {
                if (cbs[i].type == 'checkbox') {
                    cbs[i].checked = bx.checked;
                }
            }
        }
    </script>
@endsection
