<html>

<head>
    <title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style>
        .center {
            margin: auto;
            width: 431px;
            padding: 10px;
            margin-top: 20px
        }

    </style>
    <div class="center">
        <div class="barcode">
            {!! DNS2D::getBarcodeHTML($profile['akun_absen'], 'QRCODE', 20,20) !!}
        </div>
        <h2 style="text-align: center; margin-top: 20px">{{ ucwords($profile['nama']) }}</h2>
        <p style="text-align: center">{{ $profile['akun_absen'] }}</p>
    </div>
</body>

</html>
