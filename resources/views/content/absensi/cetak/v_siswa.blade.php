<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        td.fitwidth {
            width: 1px;
            white-space: nowrap;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .dontsplit {
            /* page-break-inside: avoid; */
            /* page-break-inside: auto; */
            page-break-before: always;
        }

        .gantiAuto {
            /* page-break-inside: avoid; */
            page-break-inside: auto;
            /* page-break-before: always; */
        }

    </style>

</head>

<body>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">ABSENSI SISWA <br>
                    SEMESTER
                    {{ strtoupper($tahun['semester']) }} TAHUN
                    PELAJARAN {{ $tahun['tahun_ajaran'] }}
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="3">

                    <table style="width: 100%; border: 1px solid; border-collapse: collapse;">
                        <thead>
                            <tr style="background: #f2f4f8">
                                <th rowspan="2" style="border: 1px solid; border-collapse: collapse;">No</th>
                                <th rowspan="2" style="text-align: left; border: 1px solid; border-collapse: collapse;">
                                    Nama</th>
                                <th rowspan="2" style="text-align: left; border: 1px solid; border-collapse: collapse;">
                                    NIS/NISN</th>
                                <th colspan="4" class="text-center"
                                    style="border: 1px solid; border-collapse: collapse;">Keterangan</th>
                            </tr>
                            <tr style="background: #f2f4f8">
                                <th class="text-center" style="border: 1px solid; border-collapse: collapse;">Hadir
                                </th>
                                <th class="text-center" style="border: 1px solid; border-collapse: collapse;">S</th>
                                <th class="text-center" style="border: 1px solid; border-collapse: collapse;">I</th>
                                <th class="text-center" style="border: 1px solid; border-collapse: collapse;">A</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($siswa))
                                @php
                                    $no = 1;
                                    $total_hadir = 0;
                                    $total_sakit = 0;
                                    $total_ijin = 0;
                                    $total_alfa = 0;
                                @endphp
                                @foreach ($siswa as $sw)
                                    <tr>
                                        <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                            {{ $no++ }}</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">
                                            {{ ucwords($sw['nama']) }}</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">{{ $sw['nis']." / ".$sw['nisn'] }}
                                        </td>
                                        @if (!empty($sw['kehadiran']))
                                            <td style="text-align: center; border: 1px solid; border-collapse: collapse;"
                                                class="text-center">{{ $sw['kehadiran']['hadir'] }}</td>
                                            <td style="text-align: center; border: 1px solid; border-collapse: collapse;"
                                                class="text-center">{{ $sw['kehadiran']['sakit'] }}</td>
                                            <td style="text-align: center; border: 1px solid; border-collapse: collapse;"
                                                class="text-center">{{ $sw['kehadiran']['izin'] }}</td>
                                            <td style="text-align: center; border: 1px solid; border-collapse: collapse;"
                                                class="text-center">{{ $sw['kehadiran']['alpha'] }}</td>
                                            @php
                                                $total_hadir  +=$sw['kehadiran']['hadir'];
                                                $total_sakit  +=$sw['kehadiran']['sakit'];
                                                $total_ijin  +=$sw['kehadiran']['izin'];
                                                $total_alfa  +=$sw['kehadiran']['alpha'];
                                            @endphp
                                        @else
                                            <td style="text-align: center; border: 1px solid; border-collapse: collapse;"
                                                colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                        @endif
                                    </tr>
                                @endforeach
                                <tr style="background: #f2f4f8">
                                    <td colspan="3"
                                        style="text-align: center; border: 1px solid; border-collapse: collapse;">Jumlah
                                    </td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">{{ $total_hadir }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">{{ $total_sakit }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">{{ $total_ijin }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">{{ $total_alfa }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="7" style="text-align: center;">Saat ini data tidak tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
