@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">

                    <div class="row mt-3">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h3 class="box-title">{{ Session::get('title') }}</h3>
                            <div class="tombol">
                                <button class="btn btn-info" id="addAdmin"><i class="fas fa-user-plus"></i> Tambah
                                    Admin</button>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-info vertical-middle">
                                                <th>No</th>
                                                <th>Profile</th>
                                                <th>Status</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody id="data_admin">
                                            @if (empty($admin))
                                                <tr>
                                                    <td colspan="8" class="text-center">Data saat ini kosong</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($admin as $adm)
                                                    <tr>
                                                        <td class="vertical-middle">{{ $no++ }}</td>

                                                        <td class="vertical-middle"><b>{{ ucwords($adm['nama']) }}
                                                                <br><small>email : {{ $adm['email'] }}</small></b>
                                                        </td>
                                                        <td class="vertical-middle">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                    {{ $adm['status'] == 1 ? 'checked' : '' }}
                                                                    class="admin_check" data-id="{{ $adm['id'] }}">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </td>
                                                        <td class="text-center vertical-middle">

                                                            <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                                                                data-id="{{ $adm['id'] }}"><i
                                                                    class="fas fa-pencil-alt"></i></a>
                                                            <a href="javascript:void(0)"
                                                                class="btn btn-purple btn-sm reset_pass"
                                                                data-id="{{ $adm['id'] }}"><i
                                                                    class="fas fa-key"></i></a>
                                                            <a href="javascript:void(0)"
                                                                class="btn btn-danger btn-sm delete"
                                                                data-id="{{ $adm['id'] }}"><i
                                                                    class="fas fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalAdmin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAdmin" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_admin">
                            <label class="col-md-3 col-form-label" for="l2">Nama Admin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Admin"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-envelope-open-text"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" placeholder="Email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">File</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-image"></i></span>
                                    <input id="image" type="file" name="image" accept="image/*" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="form_pass">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password_add" name="password" placeholder="Password"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                                <small class="text-danger">Password acak akan berisi nomer random</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-purple">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="res_id_siswa">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-purple btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#addAdmin').click(function() {
                $('#modelHeading').html("Tambah {{ session('title') }}");
                $('#form_pass').show('');
                $('#modalAdmin').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#formAdmin', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('absensi-admin_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('absensi-admin_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#saveBtn').html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#modalAdmin').modal('hide');
                            $('#formAdmin').trigger("reset");
                            $('#data_admin').html(data.admin);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-admin_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data {{ session('title') }}");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#email').val(data.email);
                        $('#modalAdmin').modal('show');
                        $('#password').val('');
                        $('#form_pass').hide('');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.generate', function() {
                let password = Math.floor(Math.random() * 90000) + 10000;
                $('#password_add').val(password);

            });

            $(document).on('click', '.reset_pass', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#formReset').trigger("reset");
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-admin_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-key"></i>');
                        $('#res_id_siswa').val(data.id);
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });
            });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('reset_password_manual-admin_absensi') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-admin_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $("#data_admin").html(data.admin);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.admin_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-admin_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });
        });
    </script>
@endsection
