@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <div class="d-none d-sm-inline-flex justify-center align-items-center">
                <a href="{{ route('absensi-hadir_siswa') }}" class="btn btn-linkedin mr-l-20 hidden-xs hidden-sm ripple"><i
                        class="fas fa-users"></i> Detail Absensi Siswa</a>
                <a href="{{ route('absensi-hadir_guru') }}" class="btn btn-facebook mr-l-20 hidden-xs hidden-sm ripple"><i
                        class="fas fa-user-tie"></i> Detail Absensi Guru</a>

            </div>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="row mt-3">
        <div class="col-sm-12 col-md-6 widget-holder">
            <div class="card">
                <div class="card-header bg-info p-1 d-flex justify-content-between">
                    <h2 class="box-title m-1 text-white">
                        Absensi Guru
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="mr-t-0 mr-b-5 fw-700">Tahun Ajaran {{ $tahun['tahun_ajaran'] }}</h6>
                            <p class="text-muted"> Semester {{ $tahun['semester'] }}</p>
                        </div>
                        <div class="col-md-4">
                            <a href="{{ route('absensi-hadir_print_pdf', ['based' => 'guru']) }}" target="_blank" class="btn btn-pinterest pull-right"><i class="fas fa-file-pdf"></i> Cetak PDF</a>
                        </div>
                    </div>
                   
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2">Nama</th>
                                    <th rowspan="2">NIP</th>
                                    <th colspan="4" class="text-center">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Hadir</th>
                                    <th class="text-center">S</th>
                                    <th class="text-center">I</th>
                                    <th class="text-center">A</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($guru))
                                    @foreach ($guru as $gr)
                                        <tr>
                                            <td>{{ ucwords($gr['nama']) }}</td>
                                            <td>{{ $gr['nip'] }}</td>
                                            @if (!empty($gr['kehadiran']))
                                                <td class="text-center">{{ $gr['kehadiran']['hadir'] }}</td>
                                                <td class="text-center">{{ $gr['kehadiran']['sakit'] }}</td>
                                                <td class="text-center">{{ $gr['kehadiran']['izin'] }}</td>
                                                <td class="text-center">{{ $gr['kehadiran']['alpha'] }}</td>
                                            @else
                                                <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Saat ini data tidak tersedia</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 widget-holder">
            <div class="card">
                <div class="card-header bg-info p-1 d-flex justify-content-between">
                    <h2 class="box-title m-1 text-white"><strong>Absensi Siswa
                            {{ $tahun['tahun_ajaran'] . ' ' . $tahun['semester'] }}</strong></h2>
                </div>
                <div class="card-body">
                    <div class="row alert-info m-1 py-2 pl-2">
                        <div class="col-md-8">
                            <form id="filterSiswa">
                                <div class="form-row align-items-center">
                                    <div class="col-auto my-1">
                                        <label for="l31">Rombel</label>
                                        <select class="custom-select mr-sm-2" name="id_rombel" id="id_rombel">
                                            <option selected>Pilih Rombel</option>
                                            @foreach ($rombel as $rmb)
                                                <option value="{{ $rmb['id'] }}">{{ $rmb['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-auto my-1">
                                        <button type="submit" class="btn btn-info" id="btnSearch"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <div class="download" style="display: none">
                                {{-- <a href="{{ route('absensi-hadir_print_pdf', ['based' => 'guru']) }}" target="_blank" class="btn btn-pinterest pull-right"><i class="fas fa-file-pdf"></i> Cetak PDF</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Nama</th>
                                            <th rowspan="2">NIS</th>
                                            <th colspan="4" class="text-center">Keterangan</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Hadir</th>
                                            <th class="text-center">S</th>
                                            <th class="text-center">I</th>
                                            <th class="text-center">A</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_siswa">
                                        <tr>
                                            <td colspan="6" class="text-center">Pilih Filter terlebih dulu</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#filterSiswa').on('submit', function(event) {
                event.preventDefault();
                $("#btnSearch").html(
                    '<i class="fa fa-spin fa-spinner"></i>');
                $("#btnSearch").attr("disabled", true);
                let rombel = $('#id_rombel').val();
               
                $.ajax({
                    url: "{{ route('absensi-get_filter_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#data_siswa').html(data);
                        $('#btnSearch').html('<i class="fas fa-search"></i>');
                        $("#btnSearch").attr("disabled", false);
                        let url = '{{ route("absensi-hadir_print_pdf", ["based" => "siswa", "rombel" => "value_rombel"]) }}';
                        url = url.replace('value_rombel', rombel);
                        $(".download").html('<a href="'+url+'" target="_blank" class="btn btn-pinterest pull-right"><i class="fas fa-file-pdf"></i> Cetak PDF</a>');
                        $(".download").show("");
                    },
                    error: function(data) {
                        $('#btnSearch').html('<i class="fas fa-search"></i>');
                        $("#btnSearch").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
