@extends('template/template_default/app')
@section('content')
    <style>

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60">
                    <div class="col-md-12">
                        <form action="javascript:void(0)" name="lokerCreate" id="lokerCreate">
                            @csrf
                            <div class="table-responsive">
                                <table class="table widget-bg" id="data-tabel" width="100%" cellspacing="0">
                                    <tr style="background: #0047a2;">
                                        <td colspan="3" style="color: #fff"><b>Input Loker</b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">Nama Industri</td>
                                        <td>
                                            <select name="id_industri" class="select3" id=""
                                                style="width: 50%; height: 33px;">
                                                <option value="">Pilih Industri</option>
                                                @foreach ($perusahaan as $pr)
                                                    <option value="{{ $pr['id'] }}">{{ ucwords($pr['nama']) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width: 34%"></td>
                                    </tr>
                                    <tr>
                                        <td>Judul Lowongan</td>
                                        <td><input type="text" name="judul" id="" style="width: 100%"></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>Link Loker</td>
                                        <td><input type="text" name="link" id="" style="width: 100%"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Bidang Lowongan</td>
                                        <td>
                                            <select name="id_bidang_loker" class="select3" id=""
                                                style="width: 50%; height: 33px;" multiple="multiple" data-toggle="select2" data-plugin-options='{"minimumResultsForSearch": -1}' >
                                                <option value="">Pilih Industri</option>
                                                @foreach ($bidang as $bd)
                                                    <option value="{{ $bd['id'] }}">{{ ucwords($bd['nama']) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi Lowongan</td>
                                        <td><textarea id="" name="deskripsi" rows="3" style="width: 100%"></textarea></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Syarat</td>
                                        <td><textarea id="" name="syarat" rows="3" style="width: 100%"></textarea></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Range Gaji</td>
                                        <td><input type="text" class="ribuan" name="gaji_awal"> sampai <input class="ribuan"
                                                type="text" name="gaji_sampai">
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal dibuka</td>
                                        <td><input type="text" class="datepicker" style="width: 100%"
                                                value="{{ date('d-m-Y') }}" name="tgl_buka"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ditutup</td>
                                        <td><input type="text" class="datepicker" name="tgl_tutup" style="width: 100%"
                                                value="{{ date('d-m-Y') }}"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="hidden" name="optionAksi" id="optionAksi" value="add">
                                            <button class="btn btn-success" type="submit" id="btnSave">Simpan</button>
                                            <a href="" class="btn btn-danger">Batal</a>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#lokerCreate').on('submit', function(event) {
            // $('#nextStep').html('Sending..');
            $("#btnSave").attr("disabled", true);
            event.preventDefault();
            var action_url = '';
            var method_url = '';
            if ($('#optionAksi').val() == 'add') {
                action_url = "{{ url('/program/bursa_kerja/lowongan/save') }}";
                method_url = "POST";
            }

            if ($('#optionAksi').val() == 'update') {
                action_url = "{{ route('bkk_loker-update') }}";
                method_url = "PUT";
            }
            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $("#btnSave").html(
                        '<i class="fa fa-spin fa-spinner"></i> Processing Save');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        window.location = "/program/bursa_kerja/lowongan";
                    } else {
                        $("#btnSave").attr("disabled", false);
                    }
                    swa(data.status + "!", data.message, data.success);
                    // noti(data.success, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btnSave').html('Simpan');
                }
            });
        });

    </script>

@endsection
