@extends('template/template_default/app')
@section('content')
    <style>
        button.btn.btn-danger.btn-sm,
        a.btn.btn-info.btn-sm {
            margin-top: 3px;
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60 widget-bg">
                    <div class="col-md-12">
                        <div class="card card-outline-info">
                            <div class="card-header p-1">
                                <h5 class="panel-title m-2"><i class="fa fa-database" aria-hidden="true"></i> Data
                                    {{ Session::get('title') }}</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="data-tabel" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Industri</th>
                                                <th>Nama Pemilik</th>
                                                <th>Email</th>
                                                <th>No Telepon</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="addForm" name="addForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_industri">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih user pemilik</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="id_user" id="id_user" class="form-control">
                                                <option value="">Program Sekolah</option>
                                                @foreach ($user as $us)
                                                    <option value="{{ $us['id'] }}">{{ $us['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="old_image" id="old_image">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                        </div>
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group" width="100%" style="margin-top: 10px">
                                        <div id="delete_foto" style="text-align: center"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Provinsi</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="provinsi" id="provinsi" class="form-control">
                                                <option value="">--Pilih Provinsi--</option>
                                                @foreach ($provinsi as $pro)
                                                    <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Deskripsi</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="deskripsi" id="deskripsi" class="form-control"
                                                rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Website</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="website" id="website" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createData'
                        }
                    },
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'pemilik',
                        name: 'pemilik'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createData').click(function() {
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Industri");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#addForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                // ("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('bkk_industi-save') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('bkk_industi-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#addForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.success, data.message);
                        // $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "industri/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // console.log(data);
                        $(loader).html('<i class="fa fa-edit"></i>');
                        $('#modelHeading').html("Edit Data Industri");
                        $('#id_industri').val(data.id);
                        $('#nama').val(data.nama);
                        $('#id_user').val(data.id_user).trigger('change');
                        $('#provinsi').val(data.id_provinsi).trigger('change');
                        $('#alamat').val(data.alamat);
                        $('#telepon').val(data.telepon);
                        $('#deskripsi').val(data.deskripsi);
                        $('#website').val(data.website);
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            if (data.file_edit != 'factory.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo when saving'); // remove photo
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }

                });
            });
        })


        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('bkk_industi-soft_delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>

@endsection
