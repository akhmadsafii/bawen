@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    @if (session('role') == 'bkk-admin')
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info addPelamar"><i class="fas fa-plus-circle"></i> Tambah
                                    User Industri</button>
                                <button class="btn btn-purple" id="import"><i class="fas fa-file-upload"></i> Import
                                    User Industri</button>
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <form class="navbar-form pull-right" role="search">
                                <div class="form-row align-items-center">
                                    <div class="col-auto">
                                        <select name="status" id="status" class="form-control">
                                            <option value="all" {{ $status == 'all' ? 'selected' : '' }}>Semua..</option>
                                            <option value="active" {{ $status == 'active' ? 'selected' : '' }}>Aktif
                                            </option>
                                            <option value="deactive" {{ $status == 'deactive' ? 'selected' : '' }}>Tidak
                                                Aktif/Pending</option>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <div class="input-group">
                                            @php
                                                $serc = str_replace('-', ' ', $search);
                                            @endphp
                                            <input type="text" value="{{ $serc }}" id="search" name="search"
                                                class="form-control" placeholder="Search">
                                            <div class="input-group-btn">
                                                <a href="javascript:void(0)" id="fil"
                                                    onclick="filter('{{ $routes }}')" class="btn btn-info"><i
                                                        class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($pelamar))
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($pelamar as $plm)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($plm['nama']) }}</td>
                                            <td>{{ $plm['telepon'] }}</td>
                                            <td>{{ $plm['email'] }}</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" {{ $plm['status'] == 1 ? 'checked' : '' }}
                                                        class="user_check" data-id="{{ $plm['id'] }}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" data-toggle="collapse"
                                                    data-target="#demo{{ $plm['id'] }}" class="btn btn-sm btn-purple"><i
                                                        class="fas fa-info-circle"></i></a>
                                                @if (session('role') == 'bkk-admin')
                                                    <a href="javascript:void(0)" data-id="{{ $plm['id'] }}"
                                                        class="btn btn-sm btn-info edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                        data-id="{{ $plm['id'] }}"><i class="fas fa-trash-alt"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="demo{{ $plm['id'] }}">
                                                    <table class="table table-striped">
                                                        <tr class="bg-purple">
                                                            <th colspan="5" class="text-white"><i
                                                                    class="fas fa-user-alt"></i> Profile Alumni</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="3" rowspan="5" class="text-center vertical-middle">
                                                                <img src="{{ $plm['file'] }}"
                                                                    alt="" style="max-width: 200px">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Nama</th>
                                                            <td class="vertical-middle">{{ Str::upper($plm['nama']) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Email</th>
                                                            <td class="vertical-middle">{{ $plm['email'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Password Reset</th>
                                                            <td class="vertical-middle">{{ $plm['first_password'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Telepon</th>
                                                            <td class="vertical-middle">{{ $plm['telepon'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Agama</th>
                                                            <td class="vertical-middle">{{ $plm['agama'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Jenkel</th>
                                                            <td class="vertical-middle">
                                                                {{ $plm['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Tempat Lahir</th>
                                                            <td class="vertical-middle">{{ $plm['tempat_lahir'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Tanggal Lahir</th>
                                                            <td>{{ $plm['tgl_lahir'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Pendidikan Akhir</th>
                                                            <td class="vertical-middle">
                                                                {{ Str::upper($plm['pendidikan_akhir']) }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Terakhir Login</th>
                                                            <td class="vertical-middle">{{ $plm['last_login'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Alamat</th>
                                                            <td colspan="4" class="vertical-middle">{{ $plm['alamat'] }}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Dibuat</th>
                                                            <td class="vertical-middle">{{ $plm['created_at'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Diupdate</th>
                                                            <td class="vertical-middle">{{ $plm['updated_at'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $pagination !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_pelamar">
                        <input type="hidden" name="role" value="industri">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Telepon/WA</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fab fa-whatsapp"></i>
                                    </span>
                                    <input class="form-control" id="telepon" name="telepon" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-envelope-open"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Perusahaan</label>
                            <div class="col-md-9">
                                <div class="input-group" id="select_industri"><span class="input-group-addon"><i
                                            class="fas fa-building"></i>
                                    </span>
                                    <select name="id_industri" id="id_industri" class="form-control select3">
                                        <option value="">Pilih Industri..</option>
                                        @foreach ($industri as $ind)
                                            <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group d-none" id="input_industri"><span class="input-group-addon"><i
                                            class="fas fa-building"></i>
                                    </span>
                                    <input type="text" name="industri" id="industri" class="form-control">
                                </div>
                                <small class="text-danger">Belum terdaftar? <a href="javascript:void(0)"
                                        class="ind_manual"> Tambah Industri</a> <a href="javascript:void(0)"
                                        class="ind_oto d-none"> Pilih yang sudah ada</a></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Gambar</label>
                            <div class="col-md-9">
                                <input name="image" accept="image/*" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password" name="password" placeholder="PASSWORD"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Pelamar</h5>
                </div>
                <form id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons color-info">file_download</i><br>
                                        <b class="color-info">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" />
                                        <input type="hidden" name="role" value="industri">
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.user_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('user_bkk-update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.generate', function() {
                $('#password').val(Math.floor(Math.random() * 90000) + 10000);

            });

            $(document).on('click', '.ind_manual', function() {
                $('#input_industri').removeClass('d-none');
                $('#select_industri').addClass('d-none');
                $('#id_industri').val(null);
                $(this).addClass('d-none');
                $('.ind_oto').removeClass('d-none');

            });
            $(document).on('click', '.ind_oto', function() {
                $('#input_industri').addClass('d-none');
                $('#select_industri').removeClass('d-none');
                $('#industri').val(null);
                $(this).addClass('d-none');
                $('.ind_manual').removeClass('d-none');

            });

            $('body').on('click', '.addPelamar', function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah User Industri");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('click', '#import', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import User Industri');
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("formImport"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-user_bkk') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formImport').trigger("reset");
                            $('#importModal').modal('hide');
                            window.location.reload()
                        }
                        noti(data.icon, data.success);
                        $('#importBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $('body').on('click', '.edit', function() {
                var id = $(this).data('id');
                const loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('user_bkk-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $('#modelHeading').html("Detail Data User Industri");
                        $('#id_pelamar').val(data.id);
                        $('#nama').val(data.nama);
                        $('#email').val(data.email);
                        $('#input_industri').addClass('d-none');
                        $('#select_industri').removeClass('d-none');
                        $('#telepon').val(data.telepon);
                        $('#id_industri').val(data.id_industri).trigger('change');
                        $('#action').val('Edit');
                        $(loader).html(
                            '<i class="fas fa-pencil-alt"></i>');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('user_bkk-create_industri') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('user_bkk-update_industri') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $('body').on('click', '.delete', function() {
                let id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('user_bkk-sof_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash-alt"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?status=" + status + "&search=" + search;
            document.location = url;
        }
    </script>
@endsection
