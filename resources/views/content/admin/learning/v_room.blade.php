@extends('template/template_default/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-5">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                        <div class="col-md-7">
                            <form class="form-inline mx-2 float-right">
                                <div class="form-group mx-2">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun" id="tahun" class="form-control mx-1">
                                        <option value="">Pilih Tahun Ajaran...</option>
                                        @foreach ($tahun as $thn)
                                            <option value="{{ (new \App\Helpers\Help())->encode($thn['id']) }}"
                                                {{ $_GET['th'] == (new \App\Helpers\Help())->encode($thn['id']) ? 'selected' : '' }}>
                                                {{ $thn['tahun_ajaran'] . ' ' . $thn['semester'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword6">Rombel</label>
                                    <select name="rombel" id="rombel" class="form-control mx-1">
                                        <option value="all">Pilih Rombel..</option>
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ (new \App\Helpers\Help())->encode($rombel['id']) }}"
                                                        {{ (new \App\Helpers\Help())->encode($rombel['id']) == $_GET['rb'] ? 'selected' : '' }}>
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped hover" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Mapel</th>
                                    <th>Guru</th>
                                    <th>Rombel</th>
                                    <th class="text-center">Status Terbuka</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_room">
                                <input type="hidden" name="id_rombel" id="id_rombel">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Mapel</label>
                                    <div class="col-sm-12">
                                        <select name="id_mapel" id="id_mapel" class="form-control">
                                            <option value="">Pilih Mapel..</option>
                                            @foreach ($mapel as $item)
                                                <option value="{{ $item['id'] }}">
                                                    {{ $item['kode_mapel'] . ' | ' . $item['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Guru</label>
                                    <div class="col-sm-12">
                                        <select name="id_guru" id="id_guru" class="form-control" disabled>
                                            <option value="">Pilih Guru..</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih File</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <input type="hidden" name="hidden_image" id="hidden_image">
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-sm-3">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('click', '#createNewCustomer', function() {
                $('#CustomerForm').trigger("reset");
                $('#id_rombel').val("{{ $_GET['rb'] }}");
                $('#modelHeading').html("Tambah Room");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            })

            $(document).on('click', '.room_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 2;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin_room-update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            var buttons = [];
            var based = '{{ $_GET['rb'] }}';
            if (based != 'all') {
                buttons.push({
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    }, {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }, {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    }, {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis');
            }
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: buttons,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'guru',
                        name: 'guru'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'vertical-middle text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'vertical-middle text-center',
                        orderable: false,
                        searchable: false
                    },
                ]
            });


            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('admin_learning-store_room') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('admin_learning-update_room') }}";
                }
                var formData = new FormData(this);
                formData.append('id_ta_sm', '{{ $_GET['th'] }}');
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('select[name="id_mapel"]').on('change', function() {
                let id_mapel = $(this).val();
                if (id_mapel) {
                    $.ajax({
                        url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id_mapel
                        },
                        beforeSend: function() {
                            $('#id_guru').html(
                                '<option value="">Memproses Pengajar...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('#id_guru').html(
                                    '<option value="">Guru untuk mapel ini belum diset</option>'
                                );
                            } else {
                                var s = '<option value="">Pilih Guru..</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_guru + '">' + row
                                        .guru +
                                        '</option>';

                                })
                                $('#id_guru').removeAttr('disabled');
                            }
                            $('#id_guru').html(s)
                        }
                    });
                }
            })

            $('#tahun').change(function() {
                loadSiswa($(this).val(), $('#rombel').val());
            });


            $('#rombel').change(function() {
                loadSiswa($('#tahun').val(), $(this).val());
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin_learning-edit_room') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Room");
                        $('#id_room').val(data.id);
                        $('#id_mapel').val(data.id_mapel);
                        $('#id_rombel').val(data.id_rombel);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            if (data.file_edit != 'study.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        edit_mapel(data.id_guru, data.id_mapel);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('admin_learning-delete_room') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.open_key', function() {
                let id = $(this).data('id');
                let isi = $(this).data('value');
                let id_rombel = $(this).data('rombel');
                let loader = $(this);
                let notif_key = "ingin membuka room ini!";
                let hasil = 1;
                if (isi == 1) {
                    notif_key = "ingin menutup room ini!";
                    hasil = 2
                }
                swal({
                    title: "Apa kamu yakin?",
                    text: notif_key,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('admin_room-update_status') }}",
                        type: "POST",
                        data: {
                            id,
                            hasil,
                            id_rombel
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_room' + id_rombel).html(data.room);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function loadSiswa(tahun, rombel) {
            var notempty = tahun && rombel;
            if (notempty) {
                window.location.href = "room?th=" + tahun + "&rb=" + rombel;
            }
        }

        function edit_mapel(id_guru, id_mapel) {
            $.ajax({
                url: "{{ route('get_by_mapel-guru_pelajaran_load_select') }}",
                type: "POST",
                data: {
                    id_guru,
                    id_mapel
                },
                beforeSend: function() {
                    $('#id_guru').html('<option value="">Memproses Guru</option>');
                },
                success: function(fb) {
                    $('#id_guru').html('');
                    $('#id_guru').html(fb);
                    $("#id_guru").attr("disabled", false);
                }
            });
            return false;
        }
    </script>
@endsection
