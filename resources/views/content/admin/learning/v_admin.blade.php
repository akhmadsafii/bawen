@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success" id="addData"><i class="fas fa-plus-circle"></i> Tambah</button>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">No</th>
                                    <th class="text-center">NIP</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Telepon</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_admin">
                                @if (!empty($admin))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($admin as $adm)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $adm['nip'] }}</td>
                                            <td>{{ Str::upper($adm['nama']) }}</td>
                                            <td>{{ $adm['email'] }}</td>
                                            <td>{{ $adm['telepon'] }}</td>
                                            <td>
                                                <button data-toggle="collapse" data-target="#detail{{ $adm['id'] }}"
                                                    class="btn btn-info btn-sm">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="edit btn btn-purple btn-sm">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="delete btn btn-danger btn-sm">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="reset_pass btn btn-warning btn-sm">
                                                    <i class="fas fa-key"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="detail{{ $adm['id'] }}">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td rowspan="5" colspan="3"
                                                                    class="vertical-middle text-center"><img
                                                                        src="{{ $adm['file'] }}" alt="" height="134px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle">Nama</th>
                                                                <td class="vertical-middle">{{ $adm['nama'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle">NIK</th>
                                                                <td class="vertical-middle">{{ $adm['nik'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle">NIP</th>
                                                                <td class="vertical-middle">{{ $adm['nip'] }}</td>

                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle">NUPTK</th>
                                                                <td class="vertical-middle">{{ $adm['nuptk'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle" class="vertical-middle">Agama
                                                                </th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ Str::ucfirst($adm['agama']) }}</td>
                                                                <td></td>
                                                                <th class="vertical-middle" class="vertical-middle">Jenis
                                                                    Kelamin</th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle" class="vertical-middle">Telepon
                                                                </th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['telepon'] }}</td>
                                                                <td></td>
                                                                <th class="vertical-middle" class="vertical-middle">Email
                                                                </th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['email'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle" class="vertical-middle">Tempat
                                                                    lahir</th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['tempat_lahir'] }}</td>
                                                                <td></td>
                                                                <th class="vertical-middle" class="vertical-middle">Tanggal
                                                                    Lahir</th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['tgl_lahir'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle" class="vertical-middle">
                                                                    Password Reset</th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['first_password'] }}</td>
                                                                <td></td>
                                                                <th class="vertical-middle" class="vertical-middle">Terakhir
                                                                    Login</th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['last_login'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th class="vertical-middle">Status</th>
                                                                <td class="vertical-middle">
                                                                    <p
                                                                        class="mb-0 text-{{ $adm['status'] == 0 ? 'danger' : 'success' }}">
                                                                        {{ $adm['status'] == 0 ? 'Disabled' : 'Enabled' }}
                                                                    </p>
                                                                </td>
                                                                <td></td>
                                                                <th class="vertical-middle" class="vertical-middle">Dibuat
                                                                </th>
                                                                <td class="vertical-middle" class="vertical-middle">
                                                                    {{ $adm['created_at'] }}</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini kosong</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="adminForm" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_admin">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nik" id="nik" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NUPTK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nuptk" id="nuptk" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIP</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nip" id="nip" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="old_image" id="old_image">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group" width="100%" style="margin-top: 10px">
                                                
                                            </div>
                                            <div class="col-md-12">
                                                <div id="delete_foto"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="status" id="status" class="form-control">
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#addData').click(function() {
                $('#adminForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Admin E learning");
                $('#ajaxModal').modal('show');
                $('#action').val('Add');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });

            $('body').on('submit', '#adminForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('learning_admin-save') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('learning_admin-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#adminForm').trigger("reset");
                            $('#ajaxModal').modal('hide');
                            $('#data_admin').html(data.admin);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });




            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                var loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('learning_admin-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Admin Learning");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nik').val(data.nik);
                        $('#nuptk').val(data.nuptk);
                        $('#nip').val(data.nip);
                        $('#agama').val(data.agama).trigger("change");
                        $('#jenkel').val(data.jenkel).trigger("change");
                        $('#status').val(data.status).trigger("change");
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#alamat').val(data.alamat);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tanggal_lahir').val(data.tgl_lahir);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != "default.png") {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo when saving');
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#ajaxModal').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                var loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('learning_admin-soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_admin').html(data.admin);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.reset_pass', function() {
                var id = $(this).data('id');
                var loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin Mereset password data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('reset_password-admin_learning') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_admin').html(data.admin);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-key"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>
@endsection
