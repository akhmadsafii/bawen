@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <h3 class="box-title">{{ Session::get('title') }}</h3>
                        </div>
                    </div>

                    <hr>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-sm-9 col-md-9"></div>
                            <div class="col-sm-3 col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover widget-status-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>NIP</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Status</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (empty($guru))
                                                <tr>
                                                    <td colspan="7" class="text-center">Data saat ini kosong</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($guru as $gr)
                                                    <tr>
                                                        <td class="vertical-middle">{{ $no++ }}</td>
                                                        <td class="vertical-middle">{{ Str::upper($gr['nama']) }}</td>
                                                        <td class="vertical-middle">{{ $gr['nip'] }}</td>
                                                        <td class="vertical-middle">{{ $gr['email'] }}</td>
                                                        <td class="vertical-middle">{{ $gr['telepon'] }}</td>
                                                        <td class="vertical-middle">
                                                            {{ $gr['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}</td>
                                                        @php
                                                            $status = '';
                                                            if ($gr['status'] == 1) {
                                                                $status = 'success';
                                                            } else {
                                                                $status = 'danger';
                                                            }
                                                        @endphp
                                                        <td class="vertical-middle">
                                                            <span
                                                                class="badge badge-{{ $status }} text-inverse">{{ $gr['status'] == 1 ? 'Aktif' : 'Tidak Aktif' }}</span>
                                                        </td>
                                                        <td class="vertical-middle">
                                                            <button data-toggle="collapse"
                                                                data-target="#demo{{ $gr['id'] }}"
                                                                class="btn btn-sm btn-info accordion-toggle"><span
                                                                    class="fas fa-info-circle p-0"></span></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" class="hiddenRow">
                                                            <div class="accordian-body collapse"
                                                                id="demo{{ $gr['id'] }}">
                                                                <table class="table table-striped">
                                                                    <tr class="bg-info">
                                                                        <th colspan="5"><i class="fas fa-user-alt"></i>
                                                                            Detail Guru</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Nama</th>
                                                                        <td>{{ Str::upper($gr['nama']) }}</td>
                                                                        <td></td>
                                                                        <th>NIK</th>
                                                                        <td>{{ $gr['nik'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>NIP</th>
                                                                        <td>{{ $gr['nip'] }}</td>
                                                                        <td></td>
                                                                        <th>NUPTK</th>
                                                                        <td>{{ $gr['nuptk'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Jenis Kelamin</th>
                                                                        <td>{{ $gr['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}
                                                                        </td>
                                                                        <td></td>
                                                                        <th>Agama</th>
                                                                        <td>{{ ucwords($gr['agama']) }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Telepon</th>
                                                                        <td>{{ $gr['telepon'] }}</td>
                                                                        <td></td>
                                                                        <th>Email</th>
                                                                        <td>{{ $gr['email'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Tempat Lahir</th>
                                                                        <td>{{ ucwords($gr['tempat_lahir']) }}</td>
                                                                        <td></td>
                                                                        <th>Tanggal Lahir</th>
                                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($gr['tgl_lahir']) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Alamat</th>
                                                                        <td colspan="4">{{ $gr['alamat'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Informasi Lain</th>
                                                                        <td colspan="4">{{ $gr['informasi_lain'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Tahun Masuk</th>
                                                                        <td>{{ $gr['tahun_masuk'] }}</td>
                                                                        <td></td>
                                                                        <th>Terakhir Login</th>
                                                                        <td>{{ $gr['last_login'] }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    {!! $pagination !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
