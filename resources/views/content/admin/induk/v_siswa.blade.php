@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-12 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Tahun Ajaran</label>
                    <select name="tahun_ajar" class="form-control" id="tahun_ajar">
                        @foreach ($tahun as $th)
                            @php
                                $ajar = explode('/', $th['tahun_ajaran']);
                            @endphp
                            <option value="{{ $ajar[0] }}" {{ $ajar[0] == $tahuns ? 'selected' : '' }}>
                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Jurusan</label>
                    <select name="id_jurusan" class="form-control" id="id_jurusan">
                        <option value="">Pilih jurusan..</option>
                        @foreach ($jurusan as $jr)
                            @php
                                $select = '';
                                if ($select_jurusan == (new \App\Helpers\Help())->encode($jr['id'])) {
                                    $select = 'selected';
                                }
                            @endphp
                            <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}" {{ $select }}>
                                {{ $jr['nama'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">NIS</label>
                    <input type="text" name="nis" id="nis" class="form-control" value="{{ $search }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp; &nbsp;</label>
                    <button onclick="search_siswa('{{ $routes }}')" class="btn btn-info btn-block">Pencarian</button>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover widget-status-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>NISN</th>
                                <th>NIS</th>
                                <th>Kelas</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (empty($siswa))
                                <tr>
                                    <td colspan="7" class="text-center">Data siswa saat ini tidak tersedia</td>
                                </tr>
                            @else
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($siswa as $sw)
                                    <tr>
                                        <td class="vertical-middle">{{ $no++ }}</td>
                                        <td class="vertical-middle">{{ Str::upper($sw['nama']) }}</td>
                                        <td class="vertical-middle">{{ $sw['nisn'] }}</td>
                                        <td class="vertical-middle">{{ $sw['nis'] }}</td>
                                        <td class="vertical-middle">{{ $sw['rombel'] }}</td>
                                        @php
                                            $status = '';
                                            if ($sw['status'] == 1) {
                                                $status = 'success';
                                            } else {
                                                $status = 'danger';
                                            }
                                        @endphp
                                        <td class="vertical-middle">
                                            <span
                                                class="badge badge-{{ $status }} text-inverse">{{ $sw['status'] == 1 ? 'Aktif' : 'Tidak Aktif' }}</span>
                                        </td>
                                        <td class="vertical-middle">
                                            <button data-toggle="collapse" data-target="#demo{{ $sw['id'] }}"
                                                class="btn btn-sm btn-success accordion-toggle"><span
                                                    class="fas fa-info-circle p-0"></span></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="hiddenRow">
                                            <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                                <table class="table table-striped">
                                                    <tr class="bg-success">
                                                        <th colspan="5"><i class="fas fa-user-alt"></i> Detail Siswa</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <td>{{ Str::upper($sw['nama']) }}</td>
                                                        <td></td>
                                                        <th>NIK</th>
                                                        <td>{{ $sw['nik'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>NIS</th>
                                                        <td>{{ $sw['nis'] }}</td>
                                                        <td></td>
                                                        <th>NISN</th>
                                                        <td>{{ $sw['nisn'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Jenis Kelamin</th>
                                                        <td>{{ $sw['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}</td>
                                                        <td></td>
                                                        <th>Agama</th>
                                                        <td>{{ ucwords($sw['agama']) }}</td>
                                                    </tr>
                                                    <tr>
                                                        {{-- <th>Telepon</th>
                                                        <td>{{ $sw['telepon'] }}</td> --}}
                                                        <td></td>
                                                        {{-- <th>Email</th>
                                                        <td>{{ $sw['email'] }}</td> --}}
                                                    </tr>
                                                    <tr>
                                                        <th>Tempat Lahir</th>
                                                        <td>{{ ucwords($sw['tempat_lahir']) }}</td>
                                                        <td></td>
                                                        <th>Tanggal Lahir</th>
                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_lahir']) }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat</th>
                                                        <td colspan="4">{{ $sw['alamat'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Anak Ke</th>
                                                        <td>{{ $sw['anak_ke'] }}</td>
                                                        <td></td>
                                                        <th>Status Keluarga</th>
                                                        <td>{{ $sw['status_keluarga'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kelas Diterima</th>
                                                        <td>{{ $sw['kls_diterima'] }}</td>
                                                        <td></td>
                                                        <th>Tanggal diterima</th>
                                                        <td>{{ $sw['tgl_diterima'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nomor Ijazah</th>
                                                        <td>{{ $sw['no_ijazah'] }}</td>
                                                        <td></td>
                                                        <th>Tahun Ijazah</th>
                                                        <td>{{ $sw['th_ijazah'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nomor SKHUN</th>
                                                        <td>{{ $sw['no_skhun'] }}</td>
                                                        <td></td>
                                                        <th>Tahun SKHUN</th>
                                                        <td>{{ $sw['th_skhun'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nama Ayah</th>
                                                        <td>{{ $sw['nama_ayah'] }}</td>
                                                        <td></td>
                                                        <th>pekerjaan Ayah</th>
                                                        <td>{{ $sw['pekerjaan_ayah'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nama Ibu</th>
                                                        <td>{{ $sw['nama_ibu'] }}</td>
                                                        <td></td>
                                                        <th>pekerjaan Ibu</th>
                                                        <td>{{ $sw['pekerjaan_ibu'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nama Wali</th>
                                                        <td>{{ $sw['nama_wali'] }}</td>
                                                        <td></td>
                                                        <th>pekerjaan Wali</th>
                                                        <td>{{ $sw['pekerjaan_wali'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat Wali</th>
                                                        <td>{{ $sw['alamat_wali'] }}</td>
                                                        <td></td>
                                                        <th>Telepon Wali</th>
                                                        <td>{{ $sw['telp_wali'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tahun Angkatan</th>
                                                        <td>{{ $sw['tahun_angkatan'] }}</td>
                                                        <td></td>
                                                    </tr>
                                                    @if (!empty($sw['transkrips']))
                                                        <tr class="bg-purple" data-toggle="collapse"
                                                            data-target="#transkip{{ $sw['id'] }}"
                                                            style="cursor: pointer;">
                                                            <th class="text-white text-center" colspan="5"><i
                                                                    class="fas fa-info-circle"></i> Lihat Transkip</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">
                                                                <div class="accordian-body collapse"
                                                                    id="transkip{{ $sw['id'] }}">
                                                                    <table class="table">
                                                                        <tr>
                                                                            <th>Nomor</th>
                                                                            <th>Nama</th>
                                                                            <th>Keterangan</th>
                                                                            <th>File</th>
                                                                        </tr>
                                                                        <tbody>
                                                                            @php
                                                                                $nomor = 1;
                                                                            @endphp
                                                                            @foreach ($sw['transkrips'] as $tr)
                                                                                <tr>
                                                                                    <td>{{ $nomor++ }}</td>
                                                                                    <td>{{ $tr['nama'] }}</td>
                                                                                    <td>{{ $tr['keterangan'] }}</td>
                                                                                    <td>
                                                                                        @if ($tr['file'] != null)
                                                                                            <a class="btn btn-purple btn-sm text-white"
                                                                                                href="{{ route('raport_transkip-download', (new \App\Helpers\Help())->encode($tr['id'])) }}"><i
                                                                                                    class="fas fa-file-download"></i>
                                                                                                Download Transkip</a>
                                                                                        @else
                                                                                            <small
                                                                                                class="text-danger">*tidak
                                                                                                ada file</small>
                                                                                        @endif
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif

                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $pagination !!}
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function search_siswa(routes) {
            var nis = (document.getElementById("nis") != null) ? document.getElementById("nis").value : "";
            var jurusan = (document.getElementById("id_jurusan") != null) ? document.getElementById("id_jurusan").value :
                "";
            var tahun = (document.getElementById("tahun_ajar") != null) ? document.getElementById("tahun_ajar").value : "";
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?tahun=" + tahun + "&jurusan=" + jurusan + "&nis=" + nis;
            document.location = url;
        }
    </script>
@endsection
