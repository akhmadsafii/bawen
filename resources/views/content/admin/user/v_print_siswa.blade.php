<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        /* @page {
            size: 22cm 38cm;
            margin-left: 0;
            margin-right: 0;
        } */

        .page-break {
            page-break-before: always;
        }

    </style>

</head>

<body>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>LEMBAR PENGISIAN BUKU
                    </u>
                    <br>
                    NIS : {{ $siswa['nis'] != null ? $siswa['nis'] : '.....................' }}
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <th style="width: 20px">A.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN TENTANG DIRI SAYA</th>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="width: 20px; vertical-align: top">1. </td>
                                <td colspan="2" style="vertical-align: top">Nama</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="width: 270px">a. Nama Lengkap</td>
                                <td>:
                                    {{ $siswa['nama'] != null ? strtoupper($siswa['nama']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>b. Nama Panggilan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">2. </td>
                                <td>Jenis Kelamin</td>
                                <td>:
                                    {{ $siswa['jenkel'] != null ? strtoupper($siswa['jenkel']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">3. </td>
                                <td>Tempat / Tanggal Lahir</td>
                                <td>:
                                    {{ $siswa['tempat_lahir'] != null && $siswa['tgl_lahir'] != null ? ucwords($siswa['tempat_lahir']) . ', ' . (new \App\Helpers\Help())->getTanggal($siswa['tgl_lahir']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">4. </td>
                                <td>Agama</td>
                                <td>:
                                    {{ $siswa['agama'] != null ? strtoupper($siswa['agama']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">5. </td>
                                <td>Kewarganegaraan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">6. </td>
                                <td>Anak ke Berapa</td>
                                <td>:
                                    {{ $siswa['anak_ke'] != null ? $siswa['anak_ke'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">7. </td>
                                <td>Jumlah Saudara Kandung</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">8. </td>
                                <td>Jumlah Saudara Tiri</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">9. </td>
                                <td>Jumlah Saudara Angkat</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">10. </td>
                                <td>Anak Yatim / Piatu / Yatim Piatu</td>
                                <td>:
                                    @if ($siswa['status_ortu'] == 'normal')
                                        -
                                    @elseif($siswa['status_ortu'] == 'yatim_piatu')
                                        YATIM PIATU
                                    @elseif($siswa['status_ortu'] == null)
                                        ..............................................................................
                                    @else
                                        {{ strtoupper($siswa['status_ortu']) }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">11. </td>
                                <td>Bahasa Sehari-hari di Rumah</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">B.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px; vertical-align: top">12. </td>
                                <td style="vertical-align: top">Alamat</td>
                                <td>:
                                    {{ $siswa['alamat'] != null ? $siswa['alamat'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">13. </td>
                                <td>Nomor Telepon</td>
                                <td>:
                                    {{ $siswa['telepon'] != null ? $siswa['telepon'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px; vertical-align: top">14. </td>
                                <td>Tinggal dengan Orang Tua / Saudara / di Asrama / Kost</td>
                                <td style="vertical-align: bottom">:
                                    @if ($siswa['jenis_tinggal'] == 'ortu')
                                        BERSAMA ORANG TUA
                                    @elseif($siswa['jenis_tinggal'] == 'wali')
                                        WALI
                                    @elseif($siswa['jenis_tinggal'] == 'panti')
                                        PANTI ASUHAN
                                    @elseif($siswa['jenis_tinggal'] == 'pesantren')
                                        PESANTREN
                                    @elseif($siswa['jenis_tinggal'] == 'asrama')
                                        ASRAMA
                                    @elseif($siswa['jenis_tinggal'] == 'lainnya')
                                        LAINNYA
                                    @else
                                        ..............................................................................
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">15. </td>
                                <td>Jarak Tempat Tinggal ke Sekolah</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">C.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN KESEHATAN SISWA</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">16. </td>
                                <td>Golongan Darah</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px; vertical-align: top">17. </td>
                                <td>Penyakit yang pernah diderita, <br> TBC / Cacar / Malaria dll</td>
                                <td style="vertical-align: bottom">:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">18. </td>
                                <td>Kelainan Jasmani</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">19. </td>
                                <td>Tinggi dan Berat Badan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">D.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">20. </td>
                                <td colspan="2">Pendidikan Sebelumnya</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="width: 250px">a. Lulusan dari</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>b. Tanggal dan Nomor STTB</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>c. Lama Belajar</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">21. </td>
                                <td colspan="2">Pindahan</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="width: 250px">a. Dari Sekolah</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>b. Alasan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">22. </td>
                                <td colspan="2">Diterima di Sekolah ini</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="width: 250px">a. Di Kelas</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>b. Kelompok</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>c. Kompetensi Keahlian</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>d. Tanggal</td>
                                <td>:
                                    {{ $siswa['tgl_diterima'] != null ? (new \App\Helpers\Help())->getTanggal($siswa['tgl_diterima']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <div style="page-break-before:always;"> </div>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">E.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN TENTANG AYAH KANDUNG</th>
                            </tr>
                            <tr>
                                <td style="width: 20px"></td>
                                <td style="width: 20px">23. </td>
                                <td style="width: 270px">Nama</td>
                                <td>:
                                    {{ $siswa['nama_ayah'] != null ? strtoupper($siswa['nama_ayah']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px;">24. </td>
                                <td>Tempat / Tanggal Lahir</td>
                                <td style="vertical-align: bottom">:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">25. </td>
                                <td>Agama</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">26. </td>
                                <td>Kewarganegaraan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">27. </td>
                                <td>Pendidikan</td>
                                <td>:
                                    {{ $siswa['pendidikan_ayah'] != null ? strtoupper($siswa['pendidikan_ayah']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">28. </td>
                                <td>Pekerjaan</td>
                                <td>:
                                    {{ $siswa['pekerjaan_ayah'] != null ? strtoupper($siswa['pekerjaan_ayah']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">29. </td>
                                <td>Penghasilan per Bulan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px; vertical-align: top">30. </td>
                                <td style="vertical-align: top">Alamat Rumah & Nomor Telepon</td>
                                <td>:
                                    {{ $siswa['alamat_ayah'] != null ? $siswa['alamat_ayah'] . ' & ' . $siswa['telepon_ayah'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">31. </td>
                                <td>Masih Hidup / Meninggal Dunia Tahun</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">F.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN TENTANG IBU KANDUNG</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">32. </td>
                                <td style="width: 270px">Nama</td>
                                <td>:
                                    {{ $siswa['nama_ibu'] != null ? strtoupper($siswa['nama_ibu']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px;">33. </td>
                                <td>Tempat / Tanggal Lahir</td>
                                <td style="vertical-align: bottom">:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">34. </td>
                                <td>Agama</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">35. </td>
                                <td>Kewarganegaraan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">36. </td>
                                <td>Pendidikan</td>
                                <td>:
                                    {{ $siswa['pendidikan_ibu'] != null ? strtoupper($siswa['pendidikan_ibu']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">37. </td>
                                <td>Pekerjaan</td>
                                <td>:
                                    {{ $siswa['pekerjaan_ibu'] != null ? strtoupper($siswa['pekerjaan_ibu']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">38. </td>
                                <td>Penghasilan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px; vertical-align: top">39. </td>
                                <td style="vertical-align: top">Alamat Rumah & Nomor Telepon</td>
                                <td>:
                                    {{ $siswa['alamat_ibu'] != null ? $siswa['alamat_ibu'] . ' & ' . $siswa['telepon_ibu'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">40. </td>
                                <td>Masih Hidup / Meninggal Dunia Tahun</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">G.</th>
                                <th colspan="3" style="text-align: left">KETERANGAN TENTANG WALI SISWA</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">41. </td>
                                <td style="width: 270px">Nama</td>
                                <td>:
                                    {{ $siswa['nama_wali'] != null ? strtoupper($siswa['nama_wali']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px;">42. </td>
                                <td>Tempat / Tanggal Lahir</td>
                                <td style="vertical-align: bottom">:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">43. </td>
                                <td>Agama</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">44. </td>
                                <td>Kewarganegaraan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">45. </td>
                                <td>Pendidikan</td>
                                <td>:
                                    {{ $siswa['pendidikan_wali'] != null ? strtoupper($siswa['pendidikan_wali']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">46. </td>
                                <td>Pekerjaan</td>
                                <td>:
                                    {{ $siswa['pekerjaan_wali'] != null ? strtoupper($siswa['pekerjaan_wali']) : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">47. </td>
                                <td>Penghasilan per Bulan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">48. </td>
                                <td>Alamat Rumah & Nomor Telepon</td>
                                <td>:
                                    {{ $siswa['alamat_wali'] != null ? $siswa['alamat_wali'] . ' & ' . $siswa['telp_wali'] : '..............................................................................' }}
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <th style="width: 20px">H.</th>
                                <th colspan="3" style="text-align: left">KEGEMARAN SISWA</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">49. </td>
                                <td style="width: 270px">Kesenian</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px;">50. </td>
                                <td>Olahraga</td>
                                <td style="vertical-align: bottom">:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">51. </td>
                                <td>Kemasyarakatan</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 20px">52. </td>
                                <td>Lain-lain</td>
                                <td>:
                                    ..............................................................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 35px"></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>
                                    &nbsp;&nbsp; ..............................,
                                    .............................................
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 5px"></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: center">
                                                Orang Tua Siswa
                                                <br><br><br><br>
                                                <u>..........................</u>
                                            </td>
                                            <td style="text-align: center">
                                                Wali Siswa
                                                <br><br><br><br>
                                                <u>..........................</u>
                                            </td>
                                            <td style="text-align: center">
                                                Siswa
                                                <br><br><br><br>
                                                <u>..........................</u>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
