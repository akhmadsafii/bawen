@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT GURU</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('user-guru') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Data Siswa</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $guru['file'] }});">
                                </div>
                                <h4 class="my-2">{{ ucwords($guru['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id="{{ $guru['id'] }}"><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $guru['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-success btn-block" id="btnReset"
                                                data-id="{{ $guru['id'] }}"><i class="fa fa-pencil"></i> Edit Username
                                                /
                                                Password
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-7 col-12">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link btn-purple m-1 active"
                                                href="#datasiswa" data-toggle="tab">Informasi</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link btn-purple m-1" href="#biosiswa"
                                                data-toggle="tab">Detail</a></li>
                                        <li class="nav-item"><a class="nav-link btn-purple m-1" href="#tab_alamat"
                                                data-toggle="tab">Alamat</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-5 col-12">
                                    <div class="float-right">
                                        <button type="reset" class="btn btn-danger"><i class="fas fa-sync-alt"></i>
                                            Reset</button>
                                        <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="aksi">

                            </div> --}}
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="datasiswa">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $guru['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Lengkap</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Lengkap"
                                                type="text" value="{{ $guru['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIP</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nip" name="nip" placeholder="NIP"
                                                type="number" value="{{ $guru['nip'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NUPTK</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nuptk" name="nuptk" placeholder="NUPTK"
                                                type="number" value="{{ $guru['nuptk'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tahun Masuk</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tahun_masuk" name="tahun_masuk"
                                                placeholder="Tahun Masuk" type="number"
                                                value="{{ $guru['tahun_masuk'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Status Pegawai</label>
                                        <div class="col-md-9">
                                            <select name="status_pegawai" id="status_pegawai" class="form-control">
                                                <option value="" disabled>Pilih Status Pegawai..</option>
                                                <option value="pns"
                                                    {{ $guru['status_pegawai'] == 'pns' ? 'selected' : '' }}>PNS</option>
                                                <option value="honorer"
                                                    {{ $guru['status_pegawai'] == 'honorer' ? 'selected' : '' }}>Honor
                                                    Daerah TK</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">jenis PTK</label>
                                        <div class="col-md-9">
                                            <select name="jenis_ptk" id="jenis_ptk" class="form-control">
                                                <option value="" disabled>Pilih Jenis..</option>
                                                <option value="mapel"
                                                    {{ $guru['jenis_ptk'] == 'mapel' ? 'selected' : '' }}>Guru Mapel
                                                </option>
                                                <option value="bk" {{ $guru['jenis_ptk'] == 'bk' ? 'selected' : '' }}>
                                                    Guru BK</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Informasi Lain</label>
                                        <div class="col-md-9">
                                            <textarea name="informasi_lain" id="informasi_lain" rows="3"
                                                class="form-control"
                                                placeholder="Informasi lain">{{ $guru['informasi_lain'] }}</textarea>
                                            <small class="text-info">*Bisa diisi dengan tugas tambahan</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="biosiswa">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIK</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nik" name="nik" placeholder="NIK"
                                                type="number" value="{{ $guru['nik'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Email</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="email" name="email" placeholder="Email"
                                                type="email" value="{{ $guru['email'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                placeholder="Tempat Lahir" type="text"
                                                value="{{ $guru['tempat_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                placeholder="Tanggal Lahir" type="date" value="{{ $guru['tgl_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jenis Kelamin</label>
                                        <div class="col-md-9">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="" disabled>Pilih Jenis Kelamin..</option>
                                                <option value="l" {{ $guru['jenkel'] == 'l' ? 'selected' : '' }}>Laki -
                                                    laki</option>
                                                <option value="p" {{ $guru['jenkel'] == 'p' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                        <div class="col-md-9">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="">Pilih Agama</option>
                                                <option value="islam" {{ $guru['agama'] == 'islam' ? 'selected' : '' }}>
                                                    Islam</option>
                                                <option value="kristen"
                                                    {{ $guru['agama'] == 'kristen' ? 'selected' : '' }}>Kristen</option>
                                                <option value="hindu" {{ $guru['agama'] == 'hindu' ? 'selected' : '' }}>
                                                    Hindu</option>
                                                <option value="budha" {{ $guru['agama'] == 'budha' ? 'selected' : '' }}>
                                                    Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No. HP</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="hp" name="hp" value="{{ $guru['hp'] }}"
                                                type="number" placeholder="Nomor HP">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No. Telepon</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="telepon" name="telepon"
                                                value="{{ $guru['telepon'] }}" type="number" placeholder="Nomor Telepon">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_alamat">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat Jalan</label>
                                        <div class="col-md-9">
                                            <textarea name="alamat" id="alamat" rows="3"
                                                class="form-control">{{ $guru['alamat'] }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">RT</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input class="form-control" id="rt" name="rt"
                                                        value="{{ $guru['rt'] }}" type="number" placeholder="RT">
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-froup row">
                                                        <label class="col-md-3 col-form-label" for="l0">RW</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" id="rw" name="rw"
                                                                value="{{ $guru['rw'] }}" type="number" placeholder="RW">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Dusun</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="dusun" name="dusun"
                                                value="{{ $guru['dusun'] }}" type="text" placeholder="Dusun">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kelurahan</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kelurahan" name="kelurahan"
                                                value="{{ $guru['kelurahan'] }}" type="text" placeholder="Kelurahan">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kecamatan</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kecamatan" name="kecamatan"
                                                value="{{ $guru['kecamatan'] }}" type="text" placeholder="Kecamatan">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kode Pos</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kode_pos" name="kode_pos"
                                                value="{{ $guru['kode_pos'] }}" type="number" placeholder="Kode Pos">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $guru['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update-guru') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('reset_password_manual-guru') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('upload_profile-guru') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $(".profile-img").css("background-image", "url(" + data.image +
                                ")");
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });

            $('#form_upload').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update-siswa_user') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete_profile-guru') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".profile-img").css("background-image", "url(" + data
                                    .image + ")");
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });

            $(document).on('click', '#btnReset', function() {
                $('#formReset').trigger("reset");
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-guru') }}",
                    data: {
                        id_guru: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-pencil"></i> Edit Username / Password');
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });

            });
        })
    </script>
@endsection
