@extends('content.admin.user.v_data_user')
@section('content_user')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NUPTK</th>
                                <th>Nama</th>
                                <th>Jenis PTK</th>
                                <th>Status Kepegawaian</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formTu">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Pegawai"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NUPTK</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-card"></i>
                                    </span>
                                    <input class="form-control" id="nuptk" name="nuptk"
                                        placeholder="Nomor Unik Pendidik dan Tenaga Kependidikan" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NIP</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-card"></i>
                                    </span>
                                    <input class="form-control" id="nip" name="nip" placeholder="NIP" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-at"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" placeholder="Email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-venus-mars"></i> </span>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="" disabled>Pilih Jenis Kelamin..</option>
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Pegawai</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-user-check"></i>
                                    </span>
                                    <select name="status_pegawai" id="status_pegawai" class="form-control">
                                        <option value="" disabled>Pilih Status Pegawai..</option>
                                        <option value="pns">PNS</option>
                                        <option value="honorer">Honor Daerah TK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis PTK</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user-cog"></i>
                                    </span>
                                    <select name="jenis_ptk" id="jenis_ptk" class="form-control">
                                        <option value="" disabled>Pilih Jenis..</option>
                                        <option value="laboran">Laboran</option>
                                        <option value="perpus">Tenaga Perpustakaan</option>
                                        <option value="admin">Tenaga Administrasi Sekolah</option>
                                        <option value="security">Petugas Keamanan</option>
                                        <option value="ob">pesuruh / Office Boy</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Telepon</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-phone"></i>
                                    </span>
                                    <input class="form-control" id="telepon" name="telepon"
                                        placeholder="No HP / No. Telepon" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password" name="password" placeholder="Password"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                                <small class="text-danger">Password acak akan berisi NIP dari pegawai</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Supervisor</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Agama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Jenkel</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-tata_usaha') }}";
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.generate', function() {
                let nip = $('#nip').val();
                if (nip) {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    $('#password').val(nip);
                } else {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    alert('Harap isi NIP terlebih dahulu');
                }

            });

            $(document).on('click', '.tu_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-tata_usaha') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });


            var table = $('#data-tabel').DataTable({
                ...settingTable,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        className: "text-center"
                    },
                    {
                        data: 'status_pegawai',
                        name: 'status_pegawai',
                        className: "text-center"
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-tata_usaha') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'agama',
                        name: 'agama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'jenkel',
                        name: 'jenkel'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Tata Usaha");
                $('#ajaxModel').modal('show');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Tata Usaha');
            });

            $('body').on('submit', '#formTu', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('store-tata_usaha') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formTu').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete-tata_usaha') }}",
                        type: "POST",
                        data: {
                            id_tata_usaha: id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });




        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ route('restore-tata_usaha') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                ...alert_swal,
            }).then(function() {
                $.ajax({
                    url: "{{ route('hard_delete-tata_usaha') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }
    </script>
@endsection
