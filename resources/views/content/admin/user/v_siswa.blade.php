@extends('content.admin.user.v_data_user')
@section('content_user')
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet"> --}}
    <style>
        .pace {
            display: none;
        }


        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="row mt-3">
                <div class="col-md-12 d-flex justify-content-between">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    @if (session('role') == 'admin' || session('role') == 'bk' || session('role') == 'admin-cbt')
                        <div class="tombol">
                            <button class="btn btn-info" id="addSiswa"><i class="fas fa-user-plus"></i> Tambah
                                Siswa</button>
                            <button id="btnImport" class="btn btn-purple"><i class="fas fa-file-import"></i>
                                Import</button>
                            <button id="trashSiswa" class="btn btn-warning"><i class="fas fa-trash-restore"></i>
                                Sampah</button>
                        </div>
                    @endif

                </div>
            </div>
            <hr>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-between">
                        @if (session('role') == 'admin')
                            <button class="btn btn-danger" id="delete_select"><i class="fas fa-trash"></i>
                                Hapus
                                Terpilih</button>
                        @else
                            <div></div>
                        @endif
                        <form class="navbar-form pull-right" role="search">
                            <div class="input-group">
                                @php
                                    $serc = str_replace('-', ' ', $search);
                                @endphp
                                <input type="text" value="{{ $serc }}" id="search" name="search"
                                    class="form-control" placeholder="Search">
                                <div class="input-group-btn">
                                    <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                        class="btn btn-info"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info vertical-middle">
                                        @if (session('role') == 'admin')
                                            <th class="text-center">
                                                <div class="form-check m-0">
                                                    <input class="form-check-input m-0" type="checkbox" id="checkAll">
                                                </div>
                                            </th>
                                        @endif
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        @if (session('role') == 'admin' || session('role') == 'bk' || session('role') == 'admin-cbt' || session('role') == 'learning-admin')
                                            <th></th>
                                        @endif

                                    </tr>
                                </thead>
                                <tbody>
                                    @if (empty($data))
                                        <tr>
                                            <td colspan="8" class="text-center">Data saat ini kosong</td>
                                        </tr>
                                    @else
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($data as $item)
                                            @php
                                                $status = 'Aktif';

                                                $color = 'badge badge-success text-inverse';
                                                if ($item['status'] == '0') {
                                                    $status = 'Tidak Aktif';
                                                    $color = 'badge badge-danger text-inverse';
                                                }
                                            @endphp
                                            <tr>
                                                @if (session('role') == 'admin')
                                                    <td class="text-center vertical-middle">
                                                        <div class="form-check m-0">
                                                            <input class="form-check-input m-0 siswa_entry" type="checkbox"
                                                                name="siswa[]" value="{{ $item['id'] }}">
                                                        </div>
                                                    </td>
                                                @endif
                                                <td class="vertical-middle">{{ $no++ }}</td>

                                                <td class="vertical-middle">{{ $item['nisn'] }}</td>
                                                <td class="vertical-middle">{{ $item['nis'] }}</td>
                                                <td class="vertical-middle"><b>{{ ucwords($item['nama']) }}</b>
                                                </td>
                                                <td class="vertical-middle"><span
                                                        class="{{ $color }}">{{ $status }}</span></td>
                                                @if (session('role') == 'admin' || session('role') == 'bk' || session('role') == 'admin-cbt' || session('role') == 'learning-admin')
                                                    <td class="text-center">
                                                        <a href="{{ route('edit_detail-siswa', ['k' => (new \App\Helpers\Help())->encode($item['id']),'key' => str_slug($item['nama'])]) }}"
                                                            class="btn btn-info btn-sm"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                        <a href="{{ route('cetak_pdf-siswa', ['k' => (new \App\Helpers\Help())->encode($item['id']),'key' => str_slug($item['nama'])]) }}"
                                                            target="_blank" class="btn btn-purple btn-sm"><i
                                                                class="fas fa-cloud-download-alt"></i></a>
                                                    </td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                            {!! $pagination !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalSiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formSiswa">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama Siswa</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="NAMA SISWA"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NIS</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-card"></i>
                                    </span>
                                    <input class="form-control" id="nis" name="nis" placeholder="NIS" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NISN</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-address-card"></i> </span>
                                    <input class="form-control" id="nisn" name="nisn" placeholder="NISN" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-venus-mars"></i> </span>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="" disabled>Pilih Jenis Kelamin..</option>
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Kelas Awal</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-graduation-cap"></i> </span>
                                    <select name="kls_diterima" id="kls_diterima" class="form-control">
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ $rombel['id'] }}">
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Tgl Diterima</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-calendar-alt"></i> </span>
                                    <input class="form-control" id="tgl_diterima" name="tgl_diterima" type="date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password" name="password" placeholder="PASSWORD"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                                <small class="text-danger">Password acak akan berisi NIS dari siswa</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash User Siswa</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Password</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Tahun Angkatan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form action="javascript:void(0)" id="importSiswa">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="javascript:void(0)" onclick="return template()"
                                                style="color:#03a9f3">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $('#delete_select').prop('disabled', !$('.siswa_entry:checked')
                .length);
            $('input[type=checkbox]').click(function() {
                if ($('.siswa_entry:checkbox:checked').length > 0) {
                    $('#delete_select').prop('disabled', false);
                } else {
                    $('#delete_select').prop('disabled', true);
                }
            });

            $(document).on('click', '#delete_select', function() {
                var id_siswa = [];
                $('input[name="siswa[]"]:checked').each(function() {
                    id_siswa.push($(this).val());
                });
                console.log(id_siswa);
                if (id_siswa.length > 0) {
                    var confirmdelete = confirm("Apa kamu yakin ingin menghapus siswa?");
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('delete_multiple-siswa') }}",
                            type: 'post',
                            data: {
                                id_siswa,
                            },
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    location.reload();
                                }
                                noti(data.icon, data.message);

                            }
                        });
                    }
                }
            });

            $('body').on('submit', '#importSiswa', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');

                var formDatas = new FormData(document.getElementById("importSiswa"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-siswa') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#importBtn').html('Simpan');
                        }
                        swa(data.status + "!", data.success, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-siswa') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'tahun_angkatan',
                        name: 'tahun_angkatan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#trashSiswa').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#addSiswa').click(function() {
                $('#formSiswa').trigger("reset");
                $('#modelHeading').html("Tambah Data Siswa");
                $('#modalSiswa').modal('show');
            });

            $(document).on('click', '.generate', function() {
                $(".generate").html(
                    '<i class="fas fa-spin fa-sync-alt"></i>');
                let nis = $('#nis').val();
                if (nis) {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    $('#password').val(nis);
                } else {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    alert('Harap isi NIS terlebih dahulu');
                }

            });


            $('#btnImport').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });

            $('#formSiswa').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('store-siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }

                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });

        // function deleteData(id) {
        //     swal({
        //         title: "Apa kamu yakin?",
        //         text: "ingin menghapus data ini!",
        //         type: "warning",
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!',
        //         cancelButtonText: 'No, cancel!',
        //         confirmButtonClass: 'btn btn-success',
        //         cancelButtonClass: 'btn btn-danger',
        //         buttonsStyling: false
        //     }).then(function() {
        //         let loader = $(this);
        //         console.log(loader);
        //         $.ajax({
        //             url: "siswa/delete/" + id,
        //             type: "POST",
        //             data: {
        //                 '_method': 'DELETE'
        //             },

        //             beforeSend: function() {
        //                 $(loader).html(
        //                     '<i class="fa fa-spin fa-spinner"></i> Loading');
        //             },
        //             success: function(data) {
        //                 if (data.status == 'berhasil') {
        //                     swa("Berhasil!", data.message, data.success);
        //                     location.reload();
        //                     $('#data-trash').dataTable().fnDraw(false);
        //                 } else {
        //                     $(loader).html(
        //                         '<i class="fa fa-trash"></i> Hapus');
        //                     swa("Gagal!", data.message, data.success);
        //                 }
        //             }
        //         })
        //     }, function(dismiss) {
        //         if (dismiss === 'cancel') {
        //             swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
        //         }
        //     })

        // }

        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            if (id_jurusan) {
                $('select[name="id_kelas"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ url('program/kelas/jurusan') }}" + '/' + id_jurusan,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        var s = '<option value="">---select---</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama + '</option>';

                        })
                        $('select[name="id_kelas"]').removeAttr('disabled')
                        $('select[name="id_kelas"]').html(s)
                    }
                });
            }
        })

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });


        function template() {
            window.location.href = "{{ $url }}";
        }

        // function restoreData(id) {
        //     swal(
        //         'Pulihkan?',
        //         'data anda akan kembali pulih',
        //         'question'
        //     ).then((lanjut) => {
        //         if (lanjut) {
        //             $.ajax({
        //                 url: "{{ url('admin/user/siswa/restore') }}" + '/' + id,
        //                 type: "POST",
        //                 data: {
        //                     '_method': 'PATCH'
        //                 },
        //                 beforeSend: function() {
        //                     $(".restore-" + id).html(
        //                         '<i class="fa fa-spin fa-spinner"></i> Loading');
        //                 },
        //                 success: function(data) {
        //                     if (data.status == 'berhasil') {
        //                         swa("Berhasil!", data.message, data.success);
        //                         $('#data-tabel').dataTable().fnDraw(false);
        //                         $('#data-trash').dataTable().fnDraw(false);
        //                     } else {
        //                         swa("Gagal!", data.message, data.success);
        //                     }
        //                 }
        //             });
        //         } else {
        //             swal("Proses dibatalkan!");
        //         }
        //     });
        // }

        // function forceDelete(id) {
        //     swal({
        //         title: 'Apa anda yakin?',
        //         text: "Data anda nantinya tidak dapat dipulihkan lagi!",
        //         type: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!',
        //         cancelButtonText: 'No, cancel!',
        //         confirmButtonClass: 'btn btn-success',
        //         cancelButtonClass: 'btn btn-danger',
        //         buttonsStyling: false
        //     }).then(function() {
        //         $.ajax({
        //             url: "{{ url('admin/user/siswa/hard_delete') }}" + '/' + id,
        //             type: "POST",
        //             data: {
        //                 '_method': 'DELETE'
        //             },
        //             beforeSend: function() {
        //                 $(".hardDelete-" + id).html(
        //                     '<i class="fa fa-spin fa-spinner"></i> Loading');
        //             },
        //             success: function(data) {
        //                 if (data.status == 'berhasil') {
        //                     swa("Berhasil!", data.message, data.success);
        //                     $('#data-tabel').dataTable().fnDraw(false);
        //                     $('#data-trash').dataTable().fnDraw(false);
        //                 } else {
        //                     swa("Gagal!", data.message, data.success);
        //                 }
        //             }
        //         })
        //     }, function(dismiss) {
        //         if (dismiss === 'cancel') {
        //             swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
        //         }
        //     })
        // }

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            // var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
            // var sort = (document.getElementById("sort") != null) ? document.getElementById("sort").value : "";
            // var per_page = (document.getElementById("per_page") != null) ? document.getElementById("per_page").value : "10";

            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
