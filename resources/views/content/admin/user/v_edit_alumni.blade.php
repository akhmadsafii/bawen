@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT ALUMNI</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('user-alumni') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Data Alumni</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $alumni['file'] }});">
                                </div>
                                <h4 class="my-2">{{ ucwords($alumni['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id="{{ $alumni['id'] }}"><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $alumni['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-success btn-block" id="btnReset"
                                                data-id="{{ $alumni['id'] }}"><i class="fa fa-pencil"></i> Edit Username
                                                /
                                                Password
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#informasi"
                                        data-toggle="tab">Informasi</a>
                                </li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#detail"
                                        data-toggle="tab">Detail Alumni</a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#status_pekerjaan"
                                        data-toggle="tab">Status Pekerjaan/Sekolah</a></li>
                            </ul>
                            <div class="aksi">
                                <button type="reset" class="btn btn-danger"><i class="fas fa-sync-alt"></i> Reset</button>
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Simpan</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="informasi">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $alumni['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Lengkap</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Lengkap"
                                                type="text" value="{{ $alumni['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIS</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nis" name="nis" placeholder="NIS"
                                                type="number" value="{{ $alumni['nis'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NISN</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nisn" name="nisn" placeholder="NISN"
                                                type="number" value="{{ $alumni['nisn'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jenis Kelamin</label>
                                        <div class="col-md-9">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="" disabled>Pilih Jenis Kelamin..</option>
                                                <option value="l" {{ $alumni['jenkel'] == 'l' ? 'selected' : '' }}>Laki -
                                                    laki</option>
                                                <option value="p" {{ $alumni['jenkel'] == 'p' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jurusan</label>
                                        <div class="col-md-9">
                                            <select name="jurusan" id="jurusan" class="form-control">
                                                <option value="">Pilih Jurusan..</option>
                                                @foreach ($jurusan as $jr)
                                                    <option value="{{ $jr['id'] }}"
                                                        {{ $jr['id'] == $alumni['id_jurusan'] ? 'selected' : '' }}>
                                                        {{ $jr['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Email</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="email" name="email"
                                                value="{{ $alumni['email'] }}" type="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Angkatan</label>
                                        <div class="col-md-5">
                                            @php
                                                $angkatan = explode('/', $alumni['angkatan']);
                                            @endphp
                                            <input class="form-control datepickerYear" name="tahun_angkatan1"
                                                value="{{ head($angkatan) }}" readonly onchange="getYear(this.value)"
                                                type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control" id="tahun_ajaran2" name="tahun_ajaran2"
                                                value="{{ end($angkatan) }}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No. HP</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="ponsel" name="ponsel"
                                                value="{{ $alumni['ponsel'] }}" type="number">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="detail">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                placeholder="Tempat Lahir" type="text"
                                                value="{{ $alumni['tempat_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                placeholder="Tanggal Lahir" type="date"
                                                value="{{ $alumni['tgl_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                        <div class="col-md-9">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="">Pilih Agama</option>
                                                <option value="islam"
                                                    {{ $alumni['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                                <option value="kristen"
                                                    {{ $alumni['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                                </option>
                                                <option value="hindu"
                                                    {{ $alumni['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                                <option value="budha"
                                                    {{ $alumni['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="status_pekerjaan">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Pekerjaan</label>
                                        <div class="col-md-9">
                                            <select name="pekerjaan" class="form-control" id="pekerjaan"
                                                onchange="changePekerjaan(this)">
                                                <option value="kerja"
                                                    {{ $alumni['pekerjaan'] == 'kerja' ? 'selected' : '' }}>Bekerja
                                                </option>
                                                <option value="usaha"
                                                    {{ $alumni['pekerjaan'] == 'usaha' ? 'selected' : '' }}>Wirausaha
                                                </option>
                                                <option value="kuliah"
                                                    {{ $alumni['pekerjaan'] == 'kuliah' ? 'selected' : '' }}>
                                                    Kuliah/Lanjut Sekolah</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="usaha" style="display: none">
                                        <label class="col-md-3 col-form-label" for="l0">Bidang Usaha</label>
                                        <div class="col-md-9">
                                            <input type="text" name="bidang_usaha" id="bidang_usaha" class="form-control"
                                                value="{{ $alumni['bidang_usaha'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row" id="kerja">
                                        <label class="col-md-3 col-form-label" for="l0">Sesuai Jurusan ?</label>
                                        <div class="col-md-9">
                                            <select name="sesuai_jurusan" id="sesuai_jurusan" class="form-control">
                                                <option value="1" {{ $alumni['sesuai_jurusan'] == 1 ? 'selected' : '' }}>
                                                    Ya</option>
                                                <option value="2" {{ $alumni['sesuai_jurusan'] == 2 ? 'selected' : '' }}>
                                                    Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="kuliah" style="display: none">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Sekolah</label>
                                            <div class="col-md-9">
                                                <input type="text" name="universitas" id="universitas"
                                                    class="form-control" value="{{ $alumni['universitas'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Program Studi</label>
                                            <div class="col-md-9">
                                                <input type="text" name="program_studi" id="program_studi"
                                                    class="form-control" value="{{ $alumni['universitas'] }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $alumni['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update-alumni') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('reset_password_manual-alumni') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('upload_profile-alumni') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $(".profile-img").css("background-image", "url(" + data.image +
                            ")");
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete_profile-alumni') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".profile-img").css("background-image", "url(" + data
                                    .image + ")");
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });

            $(document).on('click', '#btnReset', function() {
                $('#formReset').trigger("reset");
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-alumni') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-pencil"></i> Edit Username / Password');
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });

            });
        })


        function changePekerjaan(isi) {
            const pekerjaan = isi.value;
            if (pekerjaan == 'kerja') {
                $('#kuliah').hide();
                $('#kerja').show();
                $('#usaha').hide();
            } else if (pekerjaan == 'usaha') {
                $('#kuliah').hide();
                $('#kerja').hide();
                $('#usaha').show();
            } else {
                $('#kuliah').show();
                $('#kerja').hide();
                $('#usaha').hide();
            }
        }

        function getYear(value) {
            var yearsend = parseInt(value) + 1;
            $("#tahun_ajaran2").val(yearsend);
        }
    </script>
@endsection
