@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT SISWA</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('user-siswa') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Data Siswa</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $siswa['file'] }});">
                                </div>
                                <h4 class="my-2">{{ ucwords($siswa['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id="{{ $siswa['id'] }}"><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $siswa['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-success btn-block" id="btnReset"
                                                data-id="{{ $siswa['id'] }}"><i class="fa fa-pencil"></i> Edit Username
                                                /
                                                Password
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-8 col-12 my-auto">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link btn-purple m-1 active"
                                                href="#datasiswa" data-toggle="tab">Siswa</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link btn-purple m-1" href="#biosiswa"
                                                data-toggle="tab">Detail</a></li>
                                        <li class="nav-item"><a class="nav-link btn-purple m-1" href="#tab_alamat"
                                                data-toggle="tab">Alamat</a></li>
                                        <li class="nav-item" id="st_ortu"><a class="m-1 nav-link btn-purple"
                                                href="#ortusiswa" data-toggle="tab">Orang
                                                Tua</a></li>
                                        <li class="nav-item"><a class="m-1 nav-link btn-purple" href="#walisiswa"
                                                data-toggle="tab">Wali</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-12 my-auto">
                                    <div class="float-right">
                                        <button type="reset" class="btn btn-danger btn-sm"><i class="fas fa-sync-alt"></i>
                                            Reset</button>
                                        <button type="submit" class="btn btn-info btn-sm" id="saveBtn"><i
                                                class="fas fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="datasiswa">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $siswa['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Lengkap</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Lengkap"
                                                type="text" value="{{ $siswa['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIK</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nik" name="nik" placeholder="NIK"
                                                type="number" value="{{ $siswa['nik'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Email</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="email" name="email" placeholder="Email"
                                                type="email" value="{{ $siswa['email'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIS</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nis" name="nis" placeholder="NIS"
                                                type="number" value="{{ $siswa['nis'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NISN</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nisn" name="nisn" placeholder="NISN"
                                                type="number" value="{{ $siswa['nisn'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jenis Kelamin</label>
                                        <div class="col-md-9">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="" disabled>Pilih Jenis Kelamin..</option>
                                                <option value="l" {{ $siswa['gender'] == 'l' ? 'selected' : '' }}>Laki -
                                                    laki</option>
                                                <option value="p" {{ $siswa['gender'] == 'p' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Diterima di kelas</label>
                                        <div class="col-md-9">
                                            <select name="kls_diterima" id="kls_diterima" class="form-control">
                                                @foreach ($jurusan as $jr)
                                                    <optgroup label="{{ $jr['nama'] }}">
                                                        @foreach ($jr['kelas'] as $kelas)
                                                    <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                        @foreach ($kelas['rombel'] as $rombel)
                                                            <option value="{{ $rombel['id'] }}"
                                                                {{ $rombel['id'] == $siswa['kls_diterima'] ? 'selected' : '' }}>
                                                                {{ $rombel['nama'] }}
                                                            </option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Diterima</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tgl_diterima" name="tgl_diterima"
                                                value="{{ $siswa['tgl_diterima'] }}" type="date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No Ijazah</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input class="form-control" id="no_ijazah" name="no_ijazah"
                                                        value="{{ $siswa['no_ijazah'] }}" type="text">
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-froup row">
                                                        <label class="col-md-4 col-form-label" for="l0">Th Ijazah</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" id="th_ijazah" name="th_ijazah"
                                                                value="{{ $siswa['th_ijazah'] }}" type="number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No SKHUN</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input class="form-control" id="no_skhun" name="no_skhun"
                                                        value="{{ $siswa['no_skhun'] }}" type="text">
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-froup row">
                                                        <label class="col-md-4 col-form-label" for="l0">Th SKHUN</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" id="th_skhun" name="th_skhun"
                                                                value="{{ $siswa['th_skhun'] }}" type="number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="biosiswa">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Status Ortu</label>
                                        <div class="col-md-9">
                                            <select name="status_ortu" id="status_ortu" class="form-control">
                                                <option value="normal"
                                                    {{ $siswa['status_ortu'] == 'normal' ? 'selected' : '' }}>Normal
                                                </option>
                                                <option value="yatim"
                                                    {{ $siswa['status_ortu'] == 'yatim' ? 'selected' : '' }}>Yatim
                                                </option>
                                                <option value="piatu"
                                                    {{ $siswa['status_ortu'] == 'piatu' ? 'selected' : '' }}>Piatu
                                                </option>
                                                <option value="yatim_piatu"
                                                    {{ $siswa['status_ortu'] == 'yatim_piatu' ? 'selected' : '' }}>Yatim
                                                    Piatu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Anak ke</label>
                                        <div class="col-md-9">
                                            <input type="number" name="anak_ke" id="anak_ke" class="form-control"
                                                placeholder="Anak ke Berapa" value="{{ $siswa['anak_ke'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jenis Tinggal</label>
                                        <div class="col-md-9">
                                            <select name="jenis_tinggal" id="jenis_tinggal" class="form-control">
                                                <option value="ortu"
                                                    {{ $siswa['jenis_tinggal'] == 'ortu' ? 'selected' : '' }}>Bersama
                                                    Orang Tua</option>
                                                <option value="wali"
                                                    {{ $siswa['jenis_tinggal'] == 'wali' ? 'selected' : '' }}>Wali
                                                </option>
                                                <option value="panti"
                                                    {{ $siswa['jenis_tinggal'] == 'panti' ? 'selected' : '' }}>Panti
                                                    Asuhan</option>
                                                <option value="pesantren"
                                                    {{ $siswa['jenis_tinggal'] == 'pesantren' ? 'selected' : '' }}>
                                                    Pesantren</option>
                                                <option value="asrama"
                                                    {{ $siswa['jenis_tinggal'] == 'asrama' ? 'selected' : '' }}>Asrama
                                                </option>
                                                <option value="lainnya"
                                                    {{ $siswa['jenis_tinggal'] == 'lainnya' ? 'selected' : '' }}>Lainnya
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Status Keluarga</label>
                                        <div class="col-md-9">
                                            <select name="status_keluarga" id="status_keluarga" class="form-control">
                                                <option value="kandung"
                                                    {{ $siswa['status_keluarga'] == 'kandung' ? 'selected' : '' }}>
                                                    Kandung</option>
                                                <option value="angkat"
                                                    {{ $siswa['status_keluarga'] == 'angkat' ? 'selected' : '' }}>Angkat
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alat Transportasi</label>
                                        <div class="col-md-9">
                                            <select name="alat_transportasi" id="alat_transportasi" class="form-control">
                                                <option value="angkutan"
                                                    {{ $siswa['alat_transportasi'] == 'angkutan' ? 'selected' : '' }}>
                                                    Angkutan Umum / Bus / Pete-pete</option>
                                                <option value="mobil"
                                                    {{ $siswa['alat_transportasi'] == 'mobil' ? 'selected' : '' }}>Mobil
                                                    / Bus antar jemput</option>
                                                <option value="jalan_kaki"
                                                    {{ $siswa['alat_transportasi'] == 'jalan_kaki' ? 'selected' : '' }}>
                                                    Jalan Kaki</option>
                                                <option value="ojek"
                                                    {{ $siswa['alat_transportasi'] == 'ojek' ? 'selected' : '' }}>Ojek
                                                </option>
                                                <option value="kendaraan_pribadi"
                                                    {{ $siswa['alat_transportasi'] == 'kendaraan_pribadi' ? 'selected' : '' }}>
                                                    Kendaraan Pribadi</option>
                                                <option value="motor"
                                                    {{ $siswa['alat_transportasi'] == 'motor' ? 'selected' : '' }}>Sepeda
                                                    Motor</option>
                                                <option value="sepeda"
                                                    {{ $siswa['alat_transportasi'] == 'sepeda' ? 'selected' : '' }}>
                                                    Sepeda</option>
                                                <option value="lainnya"
                                                    {{ $siswa['alat_transportasi'] == 'lainnya' ? 'selected' : '' }}>
                                                    Lainnya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                placeholder="Tempat Lahir" type="text"
                                                value="{{ $siswa['tempat_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                placeholder="Tanggal Lahir" type="date"
                                                value="{{ $siswa['tgl_lahir'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                        <div class="col-md-9">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="">Pilih Agama</option>
                                                <option value="islam" {{ $siswa['agama'] == 'islam' ? 'selected' : '' }}>
                                                    Islam</option>
                                                <option value="kristen"
                                                    {{ $siswa['agama'] == 'kristen' ? 'selected' : '' }}>Kristen</option>
                                                <option value="hindu" {{ $siswa['agama'] == 'hindu' ? 'selected' : '' }}>
                                                    Hindu</option>
                                                <option value="budha" {{ $siswa['agama'] == 'budha' ? 'selected' : '' }}>
                                                    Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">HP</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="telepon" name="telepon"
                                                value="{{ $siswa['telepon'] }}" type="number" placeholder="HP Siswa">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tahun Angkatan</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="tahun_angkatan" name="tahun_angkatan"
                                                value="{{ $siswa['tahun_angkatan'] }}" type="number"
                                                placeholder="Tahun Angkatan">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_alamat">

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat</label>
                                        <div class="col-md-9">
                                            <input type="text" name="alamat" id="alamat" value="{{ $siswa['alamat'] }}"
                                                placeholder="Alamat" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Dusun</label>
                                        <div class="col-md-9">
                                            <input type="text" id="dusun" name="dusun" value="{{ $siswa['dusun'] }}"
                                                placeholder="Dusun" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">RT</label>
                                        <div class="col-md-9">
                                            <input type="number" id="rt" name="rt" value="{{ $siswa['rt'] }}"
                                                placeholder="RT" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">RW</label>
                                        <div class="col-md-9">
                                            <input type="number" id="rw" name="rw" value="{{ $siswa['rw'] }}"
                                                placeholder="RW" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kelurahan/Desa</label>
                                        <div class="col-md-9">
                                            <input type="text" id="kelurahan" name="kelurahan"
                                                value="{{ $siswa['kelurahan'] }}" placeholder="Kelurahan"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kecamatan</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kecamatan" placeholder="Kecamatan"
                                                name="kecamatan" value="{{ $siswa['kecamatan'] }}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kabupaten/Kota</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kabupaten" placeholder="Kabupaten"
                                                name="kabupaten" placeholder="Kabupaten"
                                                value="{{ $siswa['kabupaten'] }}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kodepos</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="kode_pos" name="kode_pos"
                                                value="{{ $siswa['kode_pos'] }}" placeholder="Kode Pos" type="number">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Lintang</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input class="form-control" id="lintang" name="lintang"
                                                        value="{{ $siswa['lintang'] }}" type="text">
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-froup row">
                                                        <label class="col-md-3 col-form-label" for="l0">Bujur</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" id="bujur" name="bujur"
                                                                value="{{ $siswa['bujur'] }}" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="ortusiswa">
                                    <div class="ayah">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nama Ayah</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="nama_ayah" name="nama_ayah"
                                                    placeholder="Nama Ayah" type="text"
                                                    value="{{ $siswa['nama_ayah'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tahun Lahir</label>
                                            <div class="col-md-9">
                                                <input type="text" name="tahun_lahir_ayah" id="tahun_lahir_ayah"
                                                    value="{{ $siswa['tahun_lahir_ayah'] }}"
                                                    placeholder="Tahun Lahir Ayah" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Pendidikan Ayah</label>
                                            <div class="col-md-9">
                                                <select name="pendidikan_ayah" id="pendidikan_ayah" class="form-control">
                                                    <option value="tidak sekolah"
                                                        {{ $siswa['pendidikan_ayah'] == 'tidak sekolah' ? 'selected' : '' }}>
                                                        Tidak Sekolah</option>
                                                    <option value="putus sd"
                                                        {{ $siswa['pendidikan_ayah'] == 'putus sd' ? 'selected' : '' }}>
                                                        Putus
                                                        SD</option>
                                                    <option value="sd"
                                                        {{ $siswa['pendidikan_ayah'] == 'sd' ? 'selected' : '' }}>SD /
                                                        Sederjat</option>
                                                    <option value="smp"
                                                        {{ $siswa['pendidikan_ayah'] == 'smp' ? 'selected' : '' }}>SMP /
                                                        Sederjat</option>
                                                    <option value="sma"
                                                        {{ $siswa['pendidikan_ayah'] == 'sma' || $siswa['pendidikan_ayah'] == null ? 'selected' : '' }}>
                                                        SMA /
                                                        Sederjat</option>
                                                    <option value="d3"
                                                        {{ $siswa['pendidikan_ayah'] == 'd3' ? 'selected' : '' }}>Diploma
                                                    </option>
                                                    <option value="s1"
                                                        {{ $siswa['pendidikan_ayah'] == 's1' ? 'selected' : '' }}>S1
                                                    </option>
                                                    <option value="s2"
                                                        {{ $siswa['pendidikan_ayah'] == 's2' ? 'selected' : '' }}>S2
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Pekerjaan Ayah</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="pekerjaan_ayah" name="pekerjaan_ayah"
                                                    placeholder="Pekerjaan Ayah" type="text"
                                                    value="{{ $siswa['pekerjaan_ayah'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Penghasilan Ayah</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" id="penghasilan_dari"
                                                        name="penghasilan_dari"> <span
                                                        class="input-group-addon bg-success text-inverse">sampai</span>
                                                    <input type="number" class="form-control" id="penghasilan_sampai"
                                                        name="penghasilan_sampai">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">No. HP Ayah</label>
                                            <div class="col-md-9">
                                                <input type="number" id="telepon_ayah" name="telepon_ayah"
                                                    value="{{ $siswa['telepon_ayah'] }}" placeholder="HP Ayah"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Alamat Ayah</label>
                                            <div class="col-md-9">
                                                <input type="text" name="alamat_ayah" id="alamat_ayah"
                                                    value="{{ $siswa['alamat_ayah'] }}" placeholder="Alamat Ayah"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <hr class="mr-tb-50">
                                    </div>
                                    <div class="ibu">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nama Ibu</label>
                                            <div class="col-md-9">
                                                <input type="text" name="nama_ibu" id="nama_ibu"
                                                    value="{{ $siswa['nama_ibu'] }}" placeholder="Nama Ibu"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tahun Lahir</label>
                                            <div class="col-md-9">
                                                <input type="text" name="tahun_lahir_ibu" id="tahun_lahir_ibu"
                                                    value="{{ $siswa['tahun_lahir_ibu'] }}" placeholder="Tahun Lahir Ibu"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Pendidikan Ibu</label>
                                            <div class="col-md-9">
                                                <select name="pendidikan_ibu" id="pendidikan_ibu" class="form-control">
                                                    <option value="tidak sekolah"
                                                        {{ $siswa['pendidikan_ibu'] == 'tidak sekolah' ? 'selected' : '' }}>
                                                        Tidak Sekolah</option>
                                                    <option value="putus sd"
                                                        {{ $siswa['pendidikan_ibu'] == 'putus sd' ? 'selected' : '' }}>
                                                        Putus
                                                        SD</option>
                                                    <option value="sd"
                                                        {{ $siswa['pendidikan_ibu'] == 'sd' ? 'selected' : '' }}>SD /
                                                        Sederjat</option>
                                                    <option value="smp"
                                                        {{ $siswa['pendidikan_ibu'] == 'smp' ? 'selected' : '' }}>SMP /
                                                        Sederjat</option>
                                                    <option value="sma"
                                                        {{ $siswa['pendidikan_ibu'] == 'sma' || $siswa['pendidikan_ibu'] == null ? 'selected' : '' }}>
                                                        SMA /
                                                        Sederjat</option>
                                                    <option value="d3"
                                                        {{ $siswa['pendidikan_ibu'] == 'd3' ? 'selected' : '' }}>Diploma
                                                    </option>
                                                    <option value="s1"
                                                        {{ $siswa['pendidikan_ibu'] == 's1' ? 'selected' : '' }}>S1
                                                    </option>
                                                    <option value="s2"
                                                        {{ $siswa['pendidikan_ibu'] == 's2' ? 'selected' : '' }}>S2
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Pekerjaan Ibu</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="pekerjaan_ibu" name="pekerjaan_ibu"
                                                    value="{{ $siswa['pekerjaan_ibu'] }}" type="text"
                                                    placeholder="Pekerjaan Ibu">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">No. HP Ibu</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="telepon_ibu" name="telepon_ibu"
                                                    value="{{ $siswa['telepon_ibu'] }}" type="text" placeholder="HP Ibu">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Alamat Ibu</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="alamat_ibu" name="alamat_ibu"
                                                    value="{{ $siswa['alamat_ibu'] }}" type="text"
                                                    placeholder="Alamat Ibu">
                                            </div>
                                        </div>
                                        <hr class="mr-tb-50">
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Keterangan Ortu</label>
                                        <div class="col-md-9">
                                            <textarea name="keterangan_ortu" id="keterangan_ortu" rows="3"
                                                class="form-control">{{ $siswa['keterangan_ortu'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="walisiswa">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Wali</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama_wali" name="nama_wali"
                                                placeholder="Nama Wali" type="text" value="{{ $siswa['nama_wali'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Pendidikan Wali</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="pendidikan_wali" name="pendidikan_wali"
                                                placeholder="Pendidikan Wali" type="text"
                                                value="{{ $siswa['pendidikan_wali'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Pekerjaan Wali</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="pekerjaan_wali" name="pekerjaan_wali"
                                                placeholder="Pekerjaan Wali" type="text"
                                                value="{{ $siswa['pekerjaan_wali'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No. HP Wali</label>
                                        <div class="col-md-9">
                                            <input type="text" name="telp_wali" id="telp_wali"
                                                value="{{ $siswa['telp_wali'] }}" class="form-control"
                                                placeholder="HP Wali">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat Wali</label>
                                        <div class="col-md-9">
                                            <input type="text" name="alamat_wali" id="alamat_wali"
                                                value="{{ $siswa['alamat_wali'] }}" class="form-control"
                                                placeholder="Alamat Wali">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $siswa['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#status_ortu').on('change', function() {
                let ortu = this.value;
                if (ortu == 'yatim') {
                    $('.ayah').hide();
                    $('.ibu').show();
                    $('#st_ortu').show();
                } else if (ortu == 'piatu') {
                    $('.ibu').hide();
                    $('.ayah').show();
                    $('#st_ortu').show();
                } else if (ortu == 'yatim_piatu') {
                    $('.ibu').hide();
                    $('.ayah').hide();
                    $('#st_ortu').hide();
                } else {
                    $('.ibu').show();
                    $('.ayah').show();
                    $('#st_ortu').show();
                }
            });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update-siswa_user') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formReset').on('submit', function(event) {
                event.preventDefault();
                $("#btnUpdateReset").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mengupdate');
                $("#btnUpdateReset").attr("disabled", true);
                $.ajax({
                    url: "{{ route('reset_password_manual-siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalReset').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpdateReset').html('Update');
                        $("#btnUpdateReset").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('upload_profile-siswa') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $(".profile-img").css("background-image", "url(" + data.image +
                                ")");
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });

            $('#form_upload').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update-siswa_user') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete_profile-siswa') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".profile-img").css("background-image", "url(" + data
                                    .image + ")");
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });

            $(document).on('click', '#btnReset', function() {
                $('#formReset').trigger("reset");
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-siswa') }}",
                    data: {
                        id_siswa: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-pencil"></i> Edit Username / Password');
                        $('#old_pass').val(data.first_password);
                        $('#modalReset').modal('show');
                    }
                });

            });
        })
    </script>
@endsection
