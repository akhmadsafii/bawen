@extends('content.admin.user.v_data_user')
@section('content_user')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover responsive nowrap" id="data-tabel"
                        style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Jenis PTK</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formGuru">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama Guru</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Guru" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NIP</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-card"></i>
                                    </span>
                                    <input class="form-control" id="nip" name="nip" placeholder="NIP" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-at"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" placeholder="Email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-venus-mars"></i> </span>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="" disabled>Pilih Jenis Kelamin..</option>
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Pegawai</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-user-check"></i>
                                    </span>
                                    <select name="status_pegawai" id="status_pegawai" class="form-control">
                                        <option value="" disabled>Pilih Status Pegawai..</option>
                                        <option value="pns">PNS</option>
                                        <option value="honorer">Honor Daerah TK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis PTK</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user-cog"></i>
                                    </span>
                                    <select name="jenis_ptk" id="jenis_ptk" class="form-control">
                                        <option value="" disabled>Pilih Jenis..</option>
                                        <option value="mapel">Guru Mapel</option>
                                        <option value="bk">Guru BK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password" name="password" placeholder="PASSWORD"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                                <small class="text-danger">Password acak akan berisi NIP dari guru</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash User Guru</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetailGuru" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">DETAIL GURU</h5>
                </div>
                <div style="width: 100%;">
                    <div class="team-single">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="font-size38 sm-font-size32 xs-font-size30 text-capitalize text-info"
                                    id="dt_nama_atas">
                                    Buckle Giarza</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 xs-margin-30px-bottom"
                                style="display: flex; align-items: center; justify-content: center;">
                                <div class="team-single-img my-2">
                                    <img id="gambar_guru" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt=""
                                        width="200">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="team-single-text padding-50px-left sm-no-padding-left">

                                    <div class="bg-info p-1">
                                        <h5 class="font-size24 text-white m-1 sm-font-size22 xs-font-size20">Detail
                                            Informasi</h5>
                                    </div>
                                    <div class="contact-info-section margin-40px-tb border border-info p-2 mb-3">
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Nama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIK:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nik">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIP:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nip">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NUPTK:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nuptk">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Agama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_agama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Alamat.:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_alamat">4 Year in Education</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Email:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_email">Design Category</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Jenis Kelamin:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_jenkel">Regina ST, London, SK.</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong
                                                    class="margin-10px-left xs-margin-four-left text-info">Telephone:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_telepon">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Tempat,
                                                    Tanggal Lahir:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="ttl"><a href="javascript:void(0)">addyour@emailhere</a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Tahun
                                                    Masuk:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_tahun_masuk">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Informasi
                                                    Lain:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_informasi_lain">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-guru') }}";
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.guru_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-guru') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            var table = $('#data-tabel').DataTable({
                ...settingTable,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: "text-center"
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        className: "text-center"
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-guru') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Guru");
                $('#ajaxModel').modal('show');
            });



            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Guru');
            });

            $('body').on('submit', '#formGuru', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('store-guru') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formGuru').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.generate', function() {
                $(".generate").html(
                    '<i class="fas fa-spin fa-sync-alt"></i>');
                let nis = $('#nip').val();
                if (nis) {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    $('#password').val(nis);
                } else {
                    $(".generate").html(
                        '<i class="fas fa-sync-alt"></i>');
                    alert('Harap isi NIP terlebih dahulu');
                }

            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete-guru') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });



        });

        function detailGuru(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('edit-guru') }}",
                data: {
                    id_guru: id
                },
                beforeSend: function() {
                    $(".dt-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".dt-" + id).html('<i class="fa fa-info-circle"></i> Detail');
                    $('#dt_nama_atas').html(data.nama == null ? '-' : data.nama);
                    $('#dt_nik').html(data.nik == null ? '-' : data.nik);
                    $('#dt_nip').html(data.nip == null ? '-' : data.nip);
                    $('#dt_nuptk').html(data.nuptk == null ? '-' : data.nuptk);
                    $('#dt_jenkel').html(data.jenkel == null ? '-' : data.jenkel);
                    $('#dt_telepon').html(data.telepon == null ? '-' : data.telepon);
                    $('#dt_nama').html(data.nama == null ? '-' : data.nama);
                    $('#dt_agama').html(data.agama == null ? '-' : data.agama);
                    $('#dt_alamat').html(data.alamat == null ? '-' : data.alamat);
                    $('#dt_email').html(data.email == null ? '-' : data.email);
                    $('#ttl').html(data.tempat_lahir + ", " + data.tgl_lahir);
                    $('#dt_tahun_masuk').html(data.tahun_masuk == null ? '-' : data.tahun_masuk);
                    $('#dt_informasi_lain').html(data.informasi_lain == null ? '-' : data.informasi_lain);
                    $('#gambar_guru').attr('src', data.file);
                    $('#modalDetailGuru').modal('show');
                }
            });

        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ route('restore-guru') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data.success);
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('hard_delete-guru') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {}
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
