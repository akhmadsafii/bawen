@extends('template/template_default/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

        #data_trash {
            background: #820084 !important;
            color: #fff !important;
        }

    </style>
    @if (Session::has('error_api'))
        <script>
            swal({
                title: 'Gagal!',
                text: "{{ session('error_api')['message'] }}",
                timer: 5000,
                type: "{{ session('error_api')['icon'] }}"
            }).then((value) => {
                //location.reload();
            }).catch(swal.noop);
        </script>
    @endif
    <div class="row widget-bg">
        <div class="col-lg-12">
            @yield('content_setting')
        </div>
    </div>
@endsection
