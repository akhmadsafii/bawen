@extends('content.admin.setting.v_data_setting')
@section('content_setting')
    <style>
        .pace {
            display: none;
        }

        .card {
            border: none;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <form id="sekolahForm">
                <div class="row">
                    <div class="col-md-4">
                        <input type="hidden" name="id" value="{{ $sekolah['id'] }}">
                        <div class="form-group">
                            <label for="kode" class="control-label">NPSN</label>
                            <input type="text" name="npsn" value="{{ $sekolah['npsn'] }}" class="form-control" id="npsn">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Jenjang</label>
                            <select name="jenjang" id="jenjang" class="form-control">
                                <option value="dasar" {{ $sekolah['jenjang'] == 'dasar' ? 'selected' : '' }}>Dasar
                                </option>
                                <option value="menengah" {{ $sekolah['jenjang'] == 'menengah' ? 'selected' : '' }}>
                                    Menengah</option>
                                <option value="atas" {{ $sekolah['jenjang'] == 'atas' ? 'selected' : '' }}>Atas</option>
                                <option value="kejuruan" {{ $sekolah['jenjang'] == 'kejuruan' ? 'selected' : '' }}>
                                    Kejuruan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="kode" class="control-label">Nama Sekolah</label>
                                <input type="text" name="nama" value="{{ $sekolah['nama'] }}" class="form-control"
                                    id="nama" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Telepon</label>
                            <input type="text" name="telepon" value="{{ $sekolah['telepon'] }}" class="form-control"
                                id="telepon">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Email</label>
                            <input type="text" name="email" value="{{ $sekolah['email'] }}" class="form-control"
                                id="email">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Status Sekolah</label>
                            <select name="status_sekolah" class="form-control" id="status_sekolah" required>
                                <option value="negeri">Negeri</option>
                                <option value="swasta">Swasta</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Logo</label>
                            <input type="file" name="image" accept="image/*" onchange="readURL(this);"
                                class="form-control" id="image">
                        </div>
                        <center>
                            @if ($sekolah['file'] == null)
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group" width="150">
                            @else
                                <img id="modal-preview" src="{{ $sekolah['file'] }}" alt="Preview" class="form-group"
                                    width="150">
                            @endif
                        </center>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="kode" class="control-label">Alamat</label>
                            <input type="text" name="alamat" value="{{ $sekolah['alamat'] }}" class="form-control"
                                id="alamat">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Dusun</label>
                            <input type="text" name="dusun" value="{{ $sekolah['dusun'] }}" class="form-control"
                                id="dusun">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kode" class="control-label">RT</label>
                                    <input type="text" name="rt" value="{{ $sekolah['rt'] }}" class="form-control"
                                        id="rt">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kode" class="control-label">RW</label>
                                    <input type="text" name="rw" value="{{ $sekolah['rw'] }}" class="form-control"
                                        id="rw">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Kelurahan</label>
                            <input type="text" name="kelurahan" value="{{ $sekolah['kelurahan'] }}"
                                class="form-control" id="kelurahan">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Kecamatan</label>
                            <input type="text" name="kecamatan" value="{{ $sekolah['kecamatan'] }}"
                                class="form-control" id="kecamatan">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Kabupaten</label>
                            <input type="text" name="kabupaten" value="{{ $sekolah['kabupaten'] }}"
                                class="form-control" id="kabupaten">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Provinsi</label>
                            <input type="text" name="provinsi" value="{{ $sekolah['provinsi'] }}" class="form-control"
                                id="provinsi">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Kode Pos</label>
                            <input type="text" name="kode_pos" value="{{ $sekolah['kode_pos'] }}" class="form-control"
                                id="kode_pos">
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="kode" class="control-label">Negara</label>
                            <input type="text" name="negara" value="{{ $sekolah['negara'] }}" class="form-control"
                                id="negara">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Lintang</label>
                            <input type="text" name="lintang" value="{{ $sekolah['lintang'] }}" class="form-control"
                                id="lintang">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Bujur</label>
                            <input type="text" name="bujur" value="{{ $sekolah['bujur'] }}" class="form-control"
                                id="bujur">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">SK Pendirian</label>
                            <input type="text" name="sk_pendirian" value="{{ $sekolah['sk_pendirian'] }}"
                                class="form-control" id="sk_pendirian">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">Tanggal SK Pendirian</label>
                            <input type="text" name="tgl_sk" value="{{ $sekolah['tgl_sk_pendirian'] }}"
                                class="form-control" id="tgl_sk">
                        </div>
                        <div class="form-group">
                            <label for="kode" class="control-label">SK Izin Operasional</label>
                            <input type="text" name="sk_izin" value="" class="form-control" id="sk_izin">
                        </div>

                        <div class="form-group">
                            <label for="kode" class="control-label">Fax</label>
                            <input type="text" name="fax" value="{{ $sekolah['fax'] }}" class="form-control" id="fax">
                        </div>

                        <div class="form-group">
                            <label for="kode" class="control-label">Website</label>
                            <input type="text" name="website" value="{{ $sekolah['website'] }}" class="form-control"
                                id="website">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-actions btn-list">
                            <center>
                                <button class="btn btn-info" id="saveBtn" type="submit"><i class="fas fa-sync-alt"></i> Update</button>
                                <button class="btn btn-outline-default" type="button">Cancel</button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#sekolahForm', function(e) {
                e.preventDefault();
                $('#saveBtn').html('<i class="fa fa-spin fa-sync-alt"></i> Sending..');
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('update-admin_sekolah') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        $('#saveBtn').html('<i class="fas fa-sync-alt"></i> Update');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-sync-alt"></i> Update');
                    }
                });
            });
        });

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }
    </script>
@endsection
