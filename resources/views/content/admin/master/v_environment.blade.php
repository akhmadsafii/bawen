@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        @foreach ($envi as $envr)
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0 text-center">{{ $envr['jenis'] }}</h5>
                        <hr>
                        <form action="javascript:void(0)" onsubmit="formUpdate(this)" id="{{ $envr['jenis'] }}">
                            @foreach ($envr['env'] as $env)
                                <div class="card my-3">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <a href="javasript:void(0)" class="float-right" data-toggle="collapse"
                                                    data-target="#instruksi{{ $env['id'] }}">
                                                    <small class="text-info">
                                                        Lihat intruksi Pengisian.
                                                    </small>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="form-row accordian-body collapse" id="instruksi{{ $env['id'] }}">
                                            <div class="form-group col-md-12">
                                                {!! $env['instruksi'] !!}
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <input type="hidden" name="jenis" value="{{ $envr['jenis'] }}">
                                            <input type="hidden" name="instruksi[]" value="{{ $env['instruksi'] }}">
                                            <input type="hidden" name="id[]" value="{{ $env['id'] }}">
                                            <div class="form-group col-md-3">
                                                <label for="inputEmail4">Kode</label>
                                                <input type="text" readonly class="form-control" name="kode[]"
                                                    value="{{ $env['kode'] }}"
                                                    placeholder="KODE {{ strtoupper($envr['jenis']) }}">

                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="inputPassword4">Nama</label>
                                                <input type="text" readonly class="form-control" name="nama[]"
                                                    value="{{ $env['nama'] }}"
                                                    placeholder="NAMA {{ strtoupper($envr['jenis']) }}">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4">Token</label>
                                                <textarea name="token[]" id="" rows="3" class="form-control"
                                                    placeholder="TOKEN {{ strtoupper($envr['jenis']) }}">{{ $env['token'] }}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            <button type="submit" class="btn btn-purple float-right" id="btn{{ $envr['jenis'] }}"><i
                                    class="fas fa-sync-alt"></i>
                                Update</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        function formUpdate(obj) {
            console.log(obj.id);

            var formData = $("#" + obj.id).serialize();
            console.log(formData);
            $.ajax({
                url: "{{ route('master_env-update') }}",
                type: "POST",
                data: formData,
                beforeSend: function() {
                    $("#btn" + obj.id).html(
                        '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                },
                success: function(data) {
                    $("#btn" + obj.id).html(
                        '<i class="fas fa-sync-alt"></i> Update');
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }
    </script>

@endsection
