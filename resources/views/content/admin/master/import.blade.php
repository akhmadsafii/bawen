<div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background-color: #03a9f3">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
            </div>
            <form action="javascript:void(0)" id="importFile" name="importFile" class="form-horizontal"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-9">
                            <center>
                                <div class="centr">
                                    <i class="fas fa-download fa-3x text-info"></i><br>
                                    <b class="text-info">Choose the file to be imported</b>
                                    <p class="m-0">[only xls, xlsx and csv formats are supported]</p>
                                    <p>Maximum upload file size is 5 MB.</p>
                                    <div class="input_kelas"></div>

                                    <input type="file" id="actual-btn" class="margin" name="image" accept="*" />
                                    <br>
                                    <u id="template_import">
                                        <a href="javascript:void(0)" onclick="return template()"
                                            class="text-info">Download sample template
                                            for
                                            import
                                        </a>
                                    </u>
                                </div>
                            </center>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                        value="create">Simpan</a>
                </div>
            </form>
        </div>
    </div>
</div>
