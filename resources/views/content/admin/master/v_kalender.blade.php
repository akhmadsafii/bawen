@extends('content.admin.master.v_data_master')
@section('content_master')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div id="calendar"></div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateKalender">
                        <div class="row">
                            <div class="col-md-12" id="informasi">
                                <p class="mb-1">Anda bisa menambahkan data kegiatan di halaman ini.</p>
                                <p><small>Klik tambah untuk menambahkan data</small></p>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="form-label mb-0">Judul Acara</label>
                            </div>
                            <div class="col-md-7">
                                <input type="hidden" id="action" value="add">
                                <input type="hidden" name="id" id="id_kalender">
                                <input type="hidden" name="start" id="start">
                                <input type="hidden" name="end" id="end">
                                <input type="text" name="title" class="form-control" id="title">
                            </div>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-success btn-update">Update</button>
                                <a href="javascript:void(0)" id="deleteKalender" onclick="deleteKalender()"
                                    class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            var calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next myCustomButton',
                    center: 'title',
                },
                editable: true,
                events: "kalendar",
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    $('#action').val('add');
                    $('.btn-update').html('Tambah');
                    $('#formUpdateKalender').trigger("reset");
                    $('#deleteKalender').hide();
                    $('#informasi').html('');
                    $('#informasi').html(
                        '<p class="mb-1">Anda bisa menambahkan data kegiatan di halaman ini.</p><p><small>Klik tambah untuk menambahkan data</small></p>'
                    );
                    $('#start').val($.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss"));
                    $('#ajaxModel').modal('show');
                },
                eventClick: function(event, jsEvent, view) {
                    var title = prompt('Event Title:', event.title);
                    if (title) {
                        event.title = title;
                        var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                        $.ajax({
                            url: base_url + "calendar/edit_events",
                            data: 'title=' + title + '&start=' + start + '&end=' + end +
                                '&id=' + event.id,
                            type: "POST",
                            success: function(response) {
                                displayMessage("Updated Successfully");
                                window.location.href = base_url + "calendar/events"

                            }
                        });
                        calendar.fullCalendar('renderEvent', {
                                title: title,
                                start: start,
                                end: end,
                            },
                            true
                        );

                    }
                    calendar.fullCalendar('unselect');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'kalendar/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id,
                        type: "PUT",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                noti(data.icon, data.success)
                            } else {
                                noti(data.icon, data.success)
                            }
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    $('#informasi').html('');
                    $('#action').val('edit');
                    $('.btn-update').html('Update');
                    $('#deleteKalender').show();
                    $('#title').val(event.title);
                    $('#informasi').html(
                        '<p class="mb-1">Anda bisa mengupdate dan menghapus data kalender disini dari database.</p><p><small>Update untuk proses update, dan delete untuk menghapus data</small></p>'
                        );
                    $('#id_kalender').val(event.id);
                    $('#start').val($.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss"));
                    $('#ajaxModel').modal('show');
                },
            });
        });

        $('#formUpdateKalender').on('submit', function(event) {
            event.preventDefault();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".btn-update").attr("disabled", true);
            var action_url = '';

            if ($('#action').val() == 'add') {
                action_url = "kalendar/store";
            }

            if ($('#action').val() == 'edit') {
                action_url = "kalendar/edit_klik";
            }
            $.ajax({
                url: action_url,
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formUpdateKalender').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                    noti(data.icon, data.success);
                    $('.btn-update').html('Update');
                    $(".btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function deleteKalender() {
            var id = $('#id_kalender').val();
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                    $.ajax({
                        type: 'POST',
                        url: "kalendar/trash",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $("#deleteKalender").html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#ajaxModel').modal('hide');
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                            swa(data.status + "!", data.success, data.icon);
                            $("#deleteKalender").html('Delete');
                        }
                    });
                },
                function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
        }
    </script>
@endsection
