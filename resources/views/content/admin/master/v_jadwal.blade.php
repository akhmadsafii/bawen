@extends('template/template_default/app')
@section('content')
    <style>
        .demo-timetable {
            margin-top: 50px
        }

        .demo-timetable .table td,
        .demo-timetable .table th {
            text-align: center
        }

        .demo-timetable .bg {
            background: #ddd
        }

        .demo-timetable .draggable {
            text-align: center;
            color: #fff;
            width: 100%;
            line-height: 40px;
            margin-bottom: 10px;
            z-index: 10
        }

        .demo-timetable .droppable {
            padding: 0
        }

        .demo-timetable .droppable .draggable {
            margin: 0
        }

        .demo-timetable .table td.ht {
            background: #ff0
        }

        .demo-cropping {
            margin-top: 50px;
            height: 400px;
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAIAAAD8GO2jAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2lpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpGM0Y4MTEyRTI0ODExMUU1OUNBRDk0OUIyODdDQzc1MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCMzk2NDNGNzI0ODYxMUU1QTlGNjhENzZBQ0Q1RDg0QiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCMzk2NDNGNjI0ODYxMUU1QTlGNjhENzZBQ0Q1RDg0QiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N2Y1MDE4ZTYtODM1MS03MDRmLWEwMzctMWIwMDI1NThiMzI4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkYzRjgxMTJFMjQ4MTExRTU5Q0FEOTQ5QjI4N0NDNzUyIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/HrpjQAAAD5JREFUeNrs1sEJADAMQtG0ZFL3MrO20BmEHr7g1Xd12a5k+lZSaH1mdoUDAAAAAAAAAAAA8AvQ7wPngCPAACi7BvVl0SS4AAAAAElFTkSuQmCC) repeat center center;
            border: 2px solid #9D9D9D;
            position: relative
        }

        .demo-cropping .selection {
            width: 151px;
            height: 151px;
            border: 1px solid #ff0;
            background: rgba(255, 255, 0, .4);
            position: absolute;
            top: 50px;
            left: 50px
        }

        .app-docs {
            padding-top: 4rem
        }

        .sidenav {
            text-align: right;
            margin-bottom: 4rem
        }

        .sidenav li {
            list-style: none;
            margin: 0
        }

        .sidenav li a {
            color: #888;
            text-decoration: none;
            display: block;
            padding: .6rem 3rem .6rem 0
        }

        .sidenav li a:hover {
            background: #f7f7f7;
            color: #000
        }

        .sidenav li.active a {
            background: #f7f7f7;
            color: #000
        }

        .sidenav .side-nav-head {
            text-transform: uppercase;
            font-size: 1.4rem;
            padding: .6rem 3rem .6rem 0;
            font-weight: 700
        }

        .docs {
            line-height: 1.8
        }

        .docs blockquote {
            border-left-color: #4390EE
        }

        .docs blockquote p {
            color: #a0a0a0
        }

        .docs h2:first-child {
            padding-top: 0;
            margin-top: 0;
            margin-bottom: 3rem
        }

        .docs h3 {
            margin: 3.6rem 0
        }

        .docs ul {
            margin-left: .5rem;
            padding-left: 1rem
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div class="demo-timetable row">
                <div class="col-md-2 classes">
                    @php
                        // $rand = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
                        // $color = '#' . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)];
                        $color = ['blue', 'red', 'orange', 'green', 'pink', 'yellow', 'brown'];
                    @endphp
                    @foreach ($mapel as $mp)
                        <div class="draggable bg-{{ $color[array_rand($color)] }}">{{ $mp['nama'] }}</div>
                    @endforeach
                </div>
                <div class="col-md-10">
                    <table class="table table-bordered table-responsive" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="bg" style="width: 11%;"></th>
                                <th class="bg" style="width: 13%;">Senin</th>
                                <th class="bg" style="width: 13%;">Selasa</th>
                                <th class="bg" style="width: 13%;">Rabu</th>
                                <th class="bg" style="width: 13%;">Kamis</th>
                                <th class="bg" style="width: 13%;">Jumat</th>
                                <th class="bg" style="width: 13%;">Sabtu</th>
                            </tr>
                            <tr>
                                <td class="bg">08:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">09:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">10:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">11:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">12:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">12:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">13:00</td>
                                <td colspan="6" class="bg">Istirahat</td>
                            </tr>
                            <tr>
                                <td class="bg">14:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">15:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                            <tr>
                                <td class="bg">16:00</td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                                <td class="droppable"></td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.draggable').hqyDraggable({
                revert: true,
                proxy: 'clone'
            });

            $('.droppable').hqyDroppable({
                accept: '.draggable',
                onDragEnter: function() {
                    $(this).addClass('ht');
                },
                onDragLeave: function() {
                    $(this).removeClass('ht');
                },
                onDrop: function(e, source) {
                    $(this).removeClass('ht');

                    if ($(source).hasClass('assigned')) {
                        $(this).append(source);
                    } else {
                        var c = $(source).clone().addClass('assigned');
                        c.css('position', 'static');
                        $(this).empty().append(c);

                        c.hqyDraggable({
                            revert: true
                        });
                    }
                }
            });



            $('.classes').hqyDroppable({
                accept: '.assigned',
                onDragEnter: function(e, source) {
                    $(source).addClass('trash');
                },
                onDragLeave: function(e, source) {
                    $(source).removeClass('trash');
                },
                onDrop: function(e, source) {
                    $(source).remove();
                }
            });


        });

    </script>
@endsection
