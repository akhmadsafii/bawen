@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-8">
                <h5 class="box-title">{{ session('title') }}
                    {{ !empty($config) ? 'TAHUN AJARAN ' . $config['tahun_ajaran'] . ' SEMESTER ' . $config['semester'] : '' }}
                </h5>
            </div>
            <div class="col-md-4">
                <form class="form-inline float-right">
                    <div class="form-group">
                        <label for="inputPassword6">Tahun Ajaran</label>
                        <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                            <option value="">Pilih Tahun Ajaran</option>
                            @foreach ($tahun as $th)
                                <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                    {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['key']) ? 'selected' : '' }}>
                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <form id="formConfig">

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Tanggal PTS </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="tgl_pts"
                                    value="{{ !empty($config) ? date('d-m-Y', strtotime($config['tgl_pts'])) : date('d-m-Y') }}"
                                    class="form-control datepicker">
                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">PTS Kelas Akhir</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="tgl_pts_kelas_akhir"
                                    value="{{ !empty($config) ? date('d-m-Y', strtotime($config['tgl_pts_kelas_akhir'])) : date('d-m-Y') }}"
                                    class="form-control datepicker">
                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Tanggal Raport </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="tgl_raport"
                                    value="{{ !empty($config) ? date('d-m-Y', strtotime($config['tgl_raport'])) : date('d-m-Y') }}"
                                    class="form-control datepicker">
                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Tanggal Kelas Akhir</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="tgl_kelas_akhir"
                                    value="{{ !empty($config) ? date('d-m-Y', strtotime($config['tgl_raport_kelas_akhir'])) : date('d-m-Y') }}"
                                    class="form-control datepicker">
                                <div class="input-group-addon"><i class="fas fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Kepala Sekolah</label>
                        <div class="col-md-9">
                            <input type="hidden" name="jenis" value="kd">
                            <input class="form-control" name="kepala_sekolah" type="text"
                                value="{{ !empty($config) ? $config['kepsek'] : '' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">NIP KepSek</label>
                        <div class="col-md-9">
                            <input class="form-control" name="nip_kepsek" type="text"
                                value="{{ !empty($config) ? $config['nip_kepsek'] : '' }}">
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="form-group row">
                            <div class="col-sm-9 ml-auto btn-list">
                                <img id="modal-preview"
                                    src="{{ !empty($config) && $config['paraf'] != null ? $config['paraf'] : 'https://via.placeholder.com/150' }}"
                                    alt="Preview" width="150">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Paraf KepSek</label>
                        <div class="col-md-9">
                            <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="form-group row">
                            <div class="col-sm-9 ml-auto btn-list">
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Update</button>
                                <button type="button" class="btn btn-danger"><i class="fas fa-exclamation-circle"></i>
                                    Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#tahun_ajaran").change(function() {
                var tahun = $(this).val();
                window.location.href = "config?key=" + tahun;
            });

            $('body').on('submit', '#formConfig', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var tahun_ajaran = $('#tahun_ajaran').val();
                var formData = new FormData(this);
                formData.append('tahun_ajaran', tahun_ajaran);
                console.log(formData);
                $.ajax({
                    type: "POST",
                    url: "{{ route('raport-save_config') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                            $("#saveBtn").attr("disabled", false);
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data
                            .icon);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        });
    </script>
@endsection
