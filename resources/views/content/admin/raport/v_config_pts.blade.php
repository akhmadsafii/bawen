@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-8">
                <h5 class="box-title">{{ session('title') }}
                    TAHUN AJARAN {{ $ajar['tahun_ajaran'] }} SEMESTER {{ $ajar['semester'] }}
                </h5>
            </div>
            <div class="col-md-4">
                <form class="form-inline float-right">
                    <div class="form-group">
                        <label for="inputPassword6">Tahun Ajaran</label>
                        <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                            <option value="">Pilih Tahun Ajaran</option>
                            @foreach ($tahun as $th)
                                <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                    {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <form id="formConfig">

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nilai UH Rata</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="nilai_uh" id="nilai_uh"
                                    value="{{ !empty($config) ? $config['nilai_uh_rata'] : '' }}" class="form-control" onblur="findTotal()">
                                <div class="input-group-addon"><i class="fas fa-percent"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nilai UTS</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" name="nilai_uts" id="nilai_uts"
                                    value="{{ !empty($config) ? $config['nilai_uts'] : '' }}" class="form-control" onblur="findTotal()">
                                <div class="input-group-addon"><i class="fas fa-percent"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Jumlah</label>
                        <label class="col-md-9 col-form-label" for="l0" id="hasil">(*) <span class="text-success">100</span>%</label>
                    </div>
                    <div class="form-actions">
                        <div class="form-group row">
                            <div class="col-sm-9 ml-auto btn-list">
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Update</button>
                                <button type="button" class="btn btn-danger"><i class="fas fa-exclamation-circle"></i>
                                    Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#tahun_ajaran").change(function() {
                var tahun = $(this).val();
                if (tahun) {
                    window.location.href = "config_pts?th=" + tahun;e
                }
            });

            $('#formConfig').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();

                $.ajax({
                    url: "{{ route('raport-save_config_pts') }}",
                    method: "POST",
                    data: $(this).serialize() +
                        "&id_ta_sm={{ $_GET['th'] }}",
                    dataType: "json",
                    success: function(data) {
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        });

        function findTotal() {
            let uh = $("#nilai_uh").val() == '' ? 0 : $("#nilai_uh").val();
            let uts = $("#nilai_uts").val() == '' ? 0 : $("#nilai_uts").val();
            let tot = parseInt(uh) + parseInt(uts);
            let data = '';
            if (tot == 100) {
                data = '(*) <span class="text-success">' + tot + '</span>';
            } else {
                data = '(*) <span class="text-danger">' + tot + '</span>';
            }
            $("#hasil").html(data);
        }
    </script>
@endsection
