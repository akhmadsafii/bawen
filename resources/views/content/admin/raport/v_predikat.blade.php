@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <p class="text-muted">Digunakan untuk memberikan predikat nilai dan deskripsi otomatis saat cetak
                            raport.</p>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-outline-info" id="addData"><i class="fas fa-plus"></i>
                                    Predikat</button>
                                <a href="javascript:void(0)" class="btn btn-outline-purple" id="importData"><i
                                        class="fas fa-file-import"></i>
                                    Predikat</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center">Predikat</th>
                                        <th>Nilai</th>
                                        <th>Bobot Nilai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_predikat">
                                    @if (empty($predikat))
                                        <tr>
                                            <td colspan="5" style="text-align: center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @else
                                        @foreach ($predikat as $pd)
                                            <tr>
                                                <td class="text-center vertical-middle"><b>{{ $pd['predikat'] }}</b></td>
                                                <td>
                                                    <b>{{ $pd['skor1'] . ' - ' . $pd['skor2'] }}</b>
                                                    <br><small>Keterangan : {{ $pd['keterangan'] }}</small>
                                                </td>
                                                <td>{{ $pd['nilai_predikat'] }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" data-id="{{ $pd['id'] }}"
                                                        class="btn btn-sm btn-info edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $pd['id'] }}"
                                                        class="btn btn-sm btn-danger delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalPredikat" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titlePredikat"></h5>
                </div>
                <form id="formPredikat" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Predikat</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="predikat" name="predikat"
                                            placeholder="A/B/C/D/E" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Skor Minimal</label>
                                    <div class="col-sm-12">
                                        <input type="hidden" name="id" id="id_predikat">
                                        <input class="form-control" type="number" name="skor1" id="skor1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Skor Maksimal</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="number" name="skor2" id="skor2">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Bobot Nilai</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nilai_predikat" name="nilai_predikat"
                                            placeholder="contoh 3.66" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Deskripsi</label>
                                    <div class="col-sm-12">
                                        <textarea name="deskripsi" id="deskripsi" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-purple">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Predikat</h5>
                </div>
                <form action="javascript:void(0)" id="importPredikat" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons text-purple">file_download</i><br>
                                        <b class="text-purple">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" class="text-purple" style="cursor: pointer"> <i class="material-icons text-purple">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" class="text-purple" target="_blank">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var url_import = "{{ route('import-nilai_predikat') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#addData').click(function() {
                $('#formPredikat').trigger("reset");
                $('#titlePredikat').html("Tambah Predikat");
                $('#modalPredikat').modal('show');
                $('#action').val('Add');
            });

            $('#importData').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Predikat Raport');
            });

            $('#formPredikat').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-store_predikat') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_predikat') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formPredikat').trigger("reset");
                            $('#modalPredikat').modal('hide');
                            $('#data_predikat').html(data.predikat);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('raport-edit_predikat') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePredikat').html("Edit Predikat");
                        $('#id_predikat').val(data.id);
                        $('#skor1').val(data.skor1);
                        $('#skor2').val(data.skor2);
                        $('#predikat').val(data.predikat);
                        $('#nilai_predikat').val(data.nilai_predikat);
                        $('#deskripsi').val(data.keterangan);
                        $('#action').val('Edit');
                        $('#modalPredikat').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('raport-trash_predikat') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_predikat').html(data.predikat);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('body').on('submit', '#importPredikat', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importPredikat"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-nilai_predikat') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#importFile').trigger("reset");
                            $('#importModal').modal('hide');
                            $('#data_predikat').html(data.predikat);
                        }
                        $('#importBtn').html('Simpan');
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });
        });

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })
    </script>
@endsection
