@extends('template/template_default/app')
@section('content')
    <style>
        .my-shadow {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(58 59 69 / 15%) !important;
        }

        .bg-lime {
            background-color: #01ff70 !important;
        }

    </style>
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"><b>SETTING KOP SURAT RAPORT</b></h5>
        </div>
        <div class="page-title-right">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card my-shadow">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title mr-b-0">Setting Kop</h5>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="card-tools btn bg-success text-white float-right"
                                onclick="submitKop()" id="btnSave">
                                <i class="fas fa-save mr-1"></i> Simpan
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="set-kop">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 1</label>
                                    <input id="header-1" class="form-control" name="header_1" type="text"
                                        placeholder="KOTA BANDUNG" value="{{ !empty($kop) ? $kop['teks1'] : '' }}">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 2</label>
                                    <input id="header-2" class="form-control" name="header_2" type="text"
                                        placeholder="DINAS PENDIDIKAN" value="{{ !empty($kop) ? $kop['teks2'] : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 3</label>
                                    <input id="header-3" class="form-control" name="header_3" type="text" placeholder="SMK NEGERI 1 BANDUNG" value="{{ !empty($kop) ? $kop['teks3'] : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 4</label>
                                    <input id="header-4" class="form-control" name="header_4" type="text" placeholder="KECAMATAN BANDUNG" value="{{ !empty($kop) ? $kop['teks4'] : '' }}">

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Header 5</label>
                                    <textarea id="header-5" class="form-control" name="header_5" rows="3"
                                        placeholder="STATUS TERAKREDITASI B N88:301.640.303 NPBN:3095959 &#10;Jl Wastukencana No.3, Babakan Ciamis Telp : (022) 4204514  Kode Pos: 40117&#10;Web: http://www.smknegeri1bandung.com/ Email: sekolah@gmail.com">
                                        {{ !empty($kop) ? $kop['teks5'] : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-4">
                            <form id="set-logo-kiri">
                                <div class="form-group">
                                    <label for="l39">Logo Kiri</label>
                                    <br>
                                    <input type="file" name="image" id="logo-kiri" accept="image/*">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form id="set-logo-kanan">
                                <div class="form-group">
                                    <label for="l39">Logo Kanan</label>
                                    <br>
                                    <input type="file" name="image" id="logo-kanan" accept="image/*">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-2">
                <div class="card-body">
                    <br>
                    <div id="print-preview" class="p-4">
                        <div style="display: flex; justify-content: center; align-items: center;">
                            <div style="width: 21cm; height: 6cm; padding: 1cm" class="border my-shadow">
                                <table id="table-header-print" style="width: 100%; border: 0;">
                                    <tr>
                                        <td style="width:15%;">
                                            <img alt="logo kiri" id="prev-logo-kiri-print"
                                                src="{{ !empty($kop) ? $kop['file'] : '' }}"
                                                style="width:85px; height:85px; margin: 6px;">
                                        </td>
                                        <td style="width:70%; text-align: center;">
                                            <div id="prev-header-1"
                                                style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 16pt">
                                                {{ !empty($kop) ? $kop['teks1'] : '' }}
                                            </div>
                                            <div id="prev-header-2"
                                                style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 12pt">
                                                {{ !empty($kop) ? $kop['teks2'] : '' }}
                                            </div>
                                            <div style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 18pt">
                                                <b id="prev-header-3">{{ !empty($kop) ? $kop['teks3'] : '' }}</b>
                                            </div>
                                            <div id="prev-header-4"
                                                style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 13pt">
                                                {{ !empty($kop) ? $kop['teks4'] : '' }}
                                            </div>
                                            <div id="prev-header-5"
                                                style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 10pt">
                                                {{ !empty($kop) ? $kop['teks5'] : '' }}
                                            </div>
                                        </td>
                                        <td style="width:15%;">
                                            <img alt="logo kanan" id="prev-logo-kanan-print"
                                                src="{{ !empty($kop) ? $kop['file1'] : '' }}"
                                                style="width:85px; height:85px; margin: 6px; border-style: none">
                                        </td>
                                    </tr>
                                </table>
                                <hr style="border: 1px solid; margin-bottom: 6px">
                                <br>
                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        });

        var kop = {!! json_encode($kop) !!};

        var oldVal1 = kop != null ? kop['header1'] : '';
        var oldVal2 = kop != null ? kop['header2'] : '';
        var oldVal3 = kop != null ? kop['header3'] : '';
        var oldVal4 = kop != null ? kop['header4'] : '';
        var oldVal5 = kop != null ? kop['header5'] : '';

        var logoKanan = kop != null ? kop['file2'] : '';
        var logoKiri = kop != null ? kop['file'] : '';

        function submitKop() {
            $('#set-kop').submit();
        }

        $(document).ready(function() {

            $("#logo-kanan").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kanan-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kanan')[0]);
                    uploadAttach("{{ route('raport-kop_sampul_logo_kanan') }}", form);
                }
            });
            $("#logo-kiri").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kiri-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kiri')[0]);
                    uploadAttach("{{ route('raport-kop_sampul_logo_kiri') }}", form);
                }
            });

            $("#header-1").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal1) {
                    return;
                }

                oldVal1 = currentVal;
                $('#prev-header-1').html(currentVal);
                //alert("changed!");
            });
            $("#header-2").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal2) {
                    return;
                }

                oldVal2 = currentVal;
                $('#prev-header-2').text(currentVal);
                //alert("changed!");
            });
            $("#header-3").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal3) {
                    return;
                }

                oldVal3 = currentVal;
                $('#prev-header-3').text(currentVal);
                //alert("changed!");
            });
            $("#header-4").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal4) {
                    return;
                }
                oldVal4 = currentVal;
                $('#prev-header-4').text(currentVal);
            });

            $("#header-5").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal5) {
                    return;
                }
                oldVal4 = currentVal;
                $('#prev-header-5').text(currentVal);
            });

            $('#set-kop').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                $.ajax({
                    url: "{{ route('raport-save_kop_sampul') }}",
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        $('#btnSave').html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    },
                    success: function(response) {
                        if (response.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#btnSave').html(
                                '<i class="fas fa-save mr-1"></i> Simpan');
                        }
                        //history.back();
                        swa(response.status.toUpperCase() + "!", response.message, response
                            .icon);
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
            });

            function uploadAttach(action, data) {
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        if (data.src.includes('kanan')) {
                            logoKanan = data.src;
                        } else {
                            logoKiri = data.src;
                        }
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $.toast({
                            heading: "ERROR!!",
                            text: "file tidak terbaca",
                            icon: 'error',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 5000,
                            position: 'top-right'
                        });
                    }
                });
            }

        })
    </script>
@endsection
