@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-8">
                <h5 class="box-title">{{ session('title') }}
                    {{ !empty($config) ? 'TAHUN AJARAN ' . $config['tahun_ajaran'] . ' SEMESTER ' . $config['semester'] : '' }}
                </h5>
            </div>
            <div class="col-md-4">
                <form class="form-inline float-right">
                    <div class="form-group">
                        <label for="inputPassword6">Tahun Ajaran</label>
                        <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                            <option value="">Pilih Tahun Ajaran</option>
                            @foreach ($tahun as $th)
                                <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                    {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['key']) ? 'selected' : '' }}>
                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row justify-content-md-center">
            <div class="col-md-9">
                <form id="formConfig">
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($template as $tmpl)
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">{{ $tmpl['jurusan'] }}</label>
                            <input type="hidden" name="id_jurusan_{{ $no }}" value="{{ $tmpl['id_jurusan'] }}">
                            <div class="col-md-4">
                                <select name="template_{{ $no }}" id="template_{{ $no }}"
                                    class="form-control">
                                    <option value="">Pilih Jenjang Template..</option>
                                    <option value="sd" {{ $tmpl['template'] == 'sd' ? 'selected' : '' }}>SD</option>
                                    <option value="smp" {{ $tmpl['template'] == 'smp' ? 'selected' : '' }}>SMP</option>
                                    <option value="sma" {{ $tmpl['template'] == 'sma' ? 'selected' : '' }}>SMA</option>
                                    <option value="smk" {{ $tmpl['template'] == 'smk' ? 'selected' : '' }}>SMK</option>
                                    <option value="kd" {{ $tmpl['template'] == 'kd' ? 'selected' : '' }}>KD (Kompetensi
                                        Dasar)</option>
                                    <option value="manual" {{ $tmpl['template'] == 'manual' ? 'selected' : '' }}>Manual
                                    </option>
                                    <option value="k16" {{ $tmpl['template'] == 'k16' ? 'selected' : '' }}>K16</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="jenis_{{ $no }}" id="jenis_{{ $no }}"
                                    class="form-control">
                                    <option value="">Pilih Jenis..</option>
                                    <option value="pas" {{ $tmpl['jenis'] == 'pas' ? 'selected' : '' }}>Penilain Akhir
                                        Semester - PAS</option>
                                    <option value="pts" {{ $tmpl['jenis'] == 'pts' ? 'selected' : '' }}>Penilain Tengah
                                        Semester - PTS</option>
                                </select>
                            </div>
                            <div class="col-md-1 m-auto">
                                <a onclick="sampleTemplate({{ $no }})" href="javascript:void(0)"><small
                                        class="text-info"><u>Preview</u></small></a>
                            </div>
                        </div>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                    <input type="hidden" name="jumlah" value="{{ count($template) }}">
                    <div class="form-actions">
                        <div class="form-group row">
                            <div class="col-sm-8 ml-auto btn-list">
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Update</button>
                                <button type="button" class="btn btn-danger"><i class="fas fa-exclamation-circle"></i>
                                    Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#tahun_ajaran").change(function() {
                var tahun = $(this).val();
                window.location.href = "config-template?key=" + tahun;
            });

            $('#formConfig').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                let tahun_ajaran = $('#tahun_ajaran').val();
                $.ajax({
                    url: "{{ route('raport-save_config_template') }}",
                    method: "POST",
                    data: $(this).serialize() + '&tahun_ajaran=' + tahun_ajaran,
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status.toUpperCase() + "!", data.message, data
                            .icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        });

        function sampleTemplate(params) {
            var level = $('#template_' + params).val();
            var jenis = $('#jenis_' + params).val();
            // window.location.href = "config-template/preview?level=" + level + "&jenis=" + jenis;
            window.open('config-template/preview?level=' + level + '&jenis=' + jenis, '_blank')
        }
    </script>
@endsection
