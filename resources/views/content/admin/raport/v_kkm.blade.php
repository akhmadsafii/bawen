@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-4">
                <h5 class="box-title">KKM
                </h5>
            </div>
            <div class="col-md-8">
                <form class="form-inline float-right">
                    <div class="form-group">
                        <label for="inputPassword6">Tahun Ajaran</label>
                        <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                            <option value="">Pilih Tahun Ajaran</option>
                            @foreach ($tahun as $th)
                                <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                    {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword6">Jurusan</label>
                        <select name="jurusan" id="jurusan" class="form-control mx-sm-3">
                            <option value="">Pilih Jurusan</option>
                            @foreach ($jurusan as $jr)
                                <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}"
                                    {{ $_GET['jr'] != 'null' && $jr['id'] == (new \App\Helpers\Help())->decode($_GET['jr']) ? 'selected' : '' }}>
                                    {{ $jr['nama'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        @if ($_GET['jr'] != 'null')
            <center>
                <h5 class="text-uppercase mb-0"><b>{{ $jrsn['nama'] }}</b></h5>
                <h5 class="text-uppercase mt-0"><b>{{ str_replace('/', ' - ', $ajar['tahun_ajaran']) }} Semester
                        {{ $ajar['semester'] }}</b></h5>
            </center>
            <div class="row justify-content-md-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-3 m-auto">
                                    <span class="text-danger">*Pilih Kelas disamping</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="float-right">
                                        @foreach ($kelas as $kl)
                                            <a href="{{ route('raport-kkm', ['th' => $_GET['th'],'jr' => $_GET['jr'],'kl' => (new \App\Helpers\Help())->encode($kl['id'])]) }}"
                                                class="btn btn-purple active">
                                                {{ $kl['nama'] }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($_GET['kl'] != 'null')
                                <p class="lead m-0">Kelas : {{ $klas['nama_romawi'] }}</p>
                                <p>Jurusan : {{ $jrsn['nama'] }}</p>
                                <button class="btn btn-info" id="addData"><i class="fas fa-plus"></i>
                                    Tambah</button>
                                <div class="table-responsive">
                                    <table class="table table-striped" id="data-tabel">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mapel</th>
                                                <th>Semester</th>
                                                <th>Nilai</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            @else
                                <center>
                                    <h5 class="text-uppercase">Harap pilih Kelas terlebih dulu</h5>
                                </center>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row justify-content-md-center">
                <div class="col-md-10">
                    <center>
                        <h5 class="text-uppercase">Harap pilih Jurusan terlebih dulu</h5>
                    </center>
                </div>
            </div>
        @endif
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalKkm" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formKkm">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_kkm">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Mapel</label>
                                    <div class="col-sm-12">
                                        <select name="id_mapel" id="id_mapel" class="form-control">
                                            <option value="">Pilih Mapel</option>
                                            @foreach ($mapel as $mp)
                                                <option value="{{ $mp['id'] }}">{{ $mp['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nilai</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="nilai" id="nilai" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#addData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah KKM");
                $('#modalKkm').modal('show');
                $('#action').val('Add');
            });

            $('#formKkm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-save_kkm') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_kkm') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize() +
                        "&id_ta_sm={{ $_GET['th'] }}&id_kelas={{ $_GET['kl'] }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formKkm').trigger("reset");
                            $('#modalKkm').modal('hide');
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('raport-edit_kkm') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit KKM");
                        $('#id_kkm').val(data.id);
                        $('#nilai').val(data.nilai);
                        $('#id_mapel').val(data.id_mapel).trigger('change');
                        $('#action').val('Edit');
                        $('#modalKkm').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: 'Apa anda yakin?',
                    text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('raport-delete_kkm') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            $("#tahun_ajaran").change(function() {
                loadKelas($(this).val(), $('#jurusan').val());
            });

            $('#jurusan').change(function() {
                loadKelas($("#tahun_ajaran").val(), $(this).val());
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'tahun_ajaran',
                        name: 'tahun_ajaran'
                    },
                    {
                        data: 'nilai',
                        name: 'nilai'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

        });

        function loadKelas(tahun, jurusan) {
            var notempty = tahun && jurusan;
            if (notempty) {
                window.location.href = "kkm?th=" + tahun + "&jr=" + jurusan + "&kl=null";
            }
        }

        var i = 1;

        function tambahData() {
            i++;
            var mapel = $('#mapel_temp').html();
            var nilai = $('#nilai_temp').html();
            var copy_file = '<div class="row align-items-end"><div class="col-md-1"><h1 class="m-0">' + i +
                '</h1></div><div class="col-md-7">' + mapel +
                '</div><div class="col-md-4">' + nilai + '</div></div>';
            var select_opsi = $('#list_select').html();
            let delete_opsi =
                '<div class="row align-items-end"><div class="col-md-11 mx-0 mb-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            let seleksi = '<div id="row' + i + '">' + copy_file + '' + delete_opsi + '<div>';
            $('#show_select').append(seleksi);
        }


        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
