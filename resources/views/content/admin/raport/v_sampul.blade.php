@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="row">
                        <div class="col-md-8 my-auto">
                            <h3 class="box-title m-0">{{ Session::get('title') }}</h3>
                        </div>
                        <div class="col-md-4">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                                {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['key']) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <button id="btnSave" onclick="submitKop()" class="btn btn-success float-right"><i
                                    class="fas fa-sync-alt"></i>
                                Perbarui</button>
                        </div>
                        <div class="col-md-12">
                            <input type="text" value="*Ketikan <br> Untuk mengganti baris" class="form-control my-2"
                                disabled>
                            <form id="setSampul">
                                <div class="form-group">
                                    <label for="l30">Title 1</label>
                                    <input class="form-control" id="title1" name="title1" value="{!! !empty($sampul) ? $sampul['title'] : '' !!}"
                                        placeholder="RAPOR" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Title 2</label>
                                    <input class="form-control" id="title2" name="title2"
                                        placeholder="SEKOLAH MENENGAH KEJURUAN <br> (SMK)" value="{!! !empty($sampul) ? $sampul['sub_title'] : '' !!}"
                                        type="text">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Footer</label>
                                    <input class="form-control" id="footer"
                                        placeholder="KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN <br> REPUBLIK INDONESIA"
                                        name="footer" value="{!! !empty($sampul) ? $sampul['footer'] : '' !!}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="l30">Petunjuk Pengisian</label>
                                    <textarea name="instruksi" id="instruksi" class="editor">{!! !empty($sampul) ? $sampul['instruksi'] : '' !!}</textarea>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form id="set-logo-atas">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Logo Atas</label>
                                    <input type="file" class="form-control-file" name="image" id="logo-atas"
                                        accept="image/*">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form id="set-logo-tengah">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Logo Tengah</label>
                                    <input type="file" class="form-control-file" name="image" id="logo-tengah"
                                        accept="image/*">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div id="print-preview" class="p-4">
                        <div style="display: flex; justify-content: center; align-items: center;">
                            <div style="width: 21cm; height: 17cm; padding: 1cm" class="border my-shadow">
                                <br>
                                <center>
                                    <img class="logo-expand" id="prev-logo-atas" alt=""
                                        src="{{ !empty($sampul) ? $sampul['file'] : asset('asset/img/sma.png') }}"
                                        style="max-height: 59px;">
                                    <br><br>
                                    <h5 class="my-0"><b id="prevTitle">{!! !empty($sampul) ? $sampul['title'] : '' !!}</b></h5>
                                    <h5 class="my-0"><b id="prevTitle2">{!! !empty($sampul) ? $sampul['sub_title'] : '' !!}</b></h5>
                                    <br>
                                    <img class="logo-expand" id="prev-logo-tengah" alt=""
                                        src="{{ !empty($sampul) ? $sampul['file1'] : asset('asset/img/sma.png') }}"
                                        style="max-height: 59px;">
                                    <br><br>
                                    <h6><b>Nama Peserta Didik</b></h6>
                                    <div style="border: 1px solid black; padding: 12px">
                                        <h6 class="m-0"><b>AKHMAD SAFI'I</b></h6>
                                    </div>
                                    <br>
                                    <h6><b>NISN/NIS</b></h6>
                                    <div style="border: 1px solid black; padding: 12px">
                                        <h6 class="m-0"><b>123232434/13123244</b></h6>
                                    </div>
                                    <br><br><br>

                                    <h6 class="my-0"><b id="prevFooter">{!! !empty($sampul) ? $sampul['footer'] : '' !!}</b></h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div id="print-preview" class="p-4">
                        <div style="display: flex; justify-content: center; align-items: center;">
                            <div style="width: 21cm; height: 33cm; padding: 1cm" class="border my-shadow">
                                <br>
                                <h5 class="text-center mt-0"><b>PETUNJUK PENGISIAN</b></h5>
                                <div class="instruksi text-justify">
                                    @if (!empty($sampul))
                                        {!! $sampul['instruksi'] !!}
                                    @else
                                        <div class="float-right text-center">
                                            <a class="copy-instriksi text-info" href="javascript:void(0)"
                                                onclick="CopyToClipboard('sampulInstruksi')">
                                                <i class="fas fa-copy fa-2x"></i> <br>
                                                <small class="text-info">"Copy instruksi"</small>
                                            </a>
                                        </div>
                                        <div id="sampulInstruksi">
                                            <table style="width:100%; font-family: 'Times New Roman';">
                                                <tr>
                                                    <td class="align-top" width="20">1.</td>
                                                    <td class="text-justify">Rapor merupakan ringkasan hasil penilaian
                                                        terhadap
                                                        seluruh aktivitas
                                                        pembelajaran yangdilakukan peserta didik dalam kurun waktu tertentu;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">2.</td>
                                                    <td class="text-justify">Rapor dipergunakan selama siswa yang
                                                        bersangkutan
                                                        mengikuti seluruh program pembelajaran diSekolah Menengah Kejuruan
                                                        tersebut;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">3.</td>
                                                    <td class="text-justify">Identitas Sekolah diisi dengan data yang
                                                        sesuai
                                                        dengan
                                                        keberadaan Sekolah Menengah Kejuruan;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">4.</td>
                                                    <td class="text-justify">Keterangan tentang diri Peserta didik diisi
                                                        lengkap
                                                        sesuai ijazah sebelumnya atau akta kelahiran;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">5.</td>
                                                    <td class="text-justify">Rapor harus dilengkapi dengan pas foto
                                                        berwarna
                                                        dengan
                                                        latar belakang merah (3 x 4) sertamenggunakan baju putih seragam dan
                                                        pengisiannya dilakukan oleh Wali Kelas;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">6.</td>
                                                    <td class="text-justify">Capaian peserta didik dalam kompetensi
                                                        pengetahuan
                                                        dan
                                                        kompetensi keterampilan ditulis dalambentuk angka dan predikat untuk
                                                        masing-masing mata pelajaran;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">6.</td>
                                                    <td class="text-justify">Capaian peserta didik dalam kompetensi
                                                        pengetahuan
                                                        dan
                                                        kompetensi keterampilan ditulis dalambentuk angka dan predikat untuk
                                                        masing-masing mata pelajaran;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">7.</td>
                                                    <td class="text-justify">Predikat ditulis dalam bentuk huruf sesuai
                                                        kriteria;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">8.</td>
                                                    <td class="text-justify">Catatan akademik ditulis dengan kalimat
                                                        positif
                                                        sesuai
                                                        capaian yang diperoleh pesertadidik</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">9.</td>
                                                    <td class="text-justify">Penjelasan lebih detil mengenai capaian
                                                        kompetensi
                                                        peserta didik dapat dilihat pada leger</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">10.</td>
                                                    <td class="text-justify">Laporan Praktik Kerja Lapangan diisi
                                                        berdasarkan
                                                        kegiatan
                                                        praktik kerja yang diikuti oleh pesertadidik di industri/perusahaan
                                                        mitra;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">11.</td>
                                                    <td class="text-justify">Laporan Ekstrakurikuler diisi berdasarkan
                                                        kegiatan
                                                        ekstrakurikuler yang diikuti oleh peserta didik;</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">12.</td>
                                                    <td class="text-justify">Ketidakhadiran diisi dengan data akumulasi
                                                        ketidakhadiran
                                                        peserta didik karena sakit, izin, atautanpa keterangan selama satu
                                                        semester
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">13.</td>
                                                    <td class="text-justify">Keterangan kenaikan kelas diisi dengan
                                                        putusan
                                                        apakah
                                                        peserta didik naik kelas yang ditentukanmelalui rapat dewan guru.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">14.</td>
                                                    <td class="text-justify">Deskripsi perkembangan karakter diisi dengan
                                                        simpulan
                                                        perkembangan peserta didik terkaitpenumbuhan karakter baik yang
                                                        dilakukan
                                                        secara
                                                        terprogram oleh sekolah maupun yang munculsecara spontan dari
                                                        peserta
                                                        didik
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">15.</td>
                                                    <td class="text-justify">Catatan perkembangan karakter diisikan
                                                        hal-hal
                                                        yang
                                                        tidak
                                                        tercantum pada deskripsiperkembangan karakter termasuk prestasi yang
                                                        diraih
                                                        peserta didik pada semester berjalan dansimpulan dari perkembangan
                                                        karakter
                                                        peserta didik pada semester berjalan jika dikomparasidengan semester
                                                        sebelumnya
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top" width="20">16.</td>
                                                    <td class="text-justify">Keterangan pindah keluar sekolah diisi dengan
                                                        alasan
                                                        kepindahan. Sedangkan pindah masuk diisidengan sekolah asal.</td>
                                                </tr>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
        integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var sampul = {!! json_encode($sampul) !!};

            var title = sampul != null ? sampul['title'] : '';
            var sub_title = sampul != null ? sampul['sub_title'] : '';
            var instruksi = sampul != null ? sampul['instruksi'] : '';
            var footer = sampul != null ? sampul['footer'] : '';

            var file = sampul != null ? sampul['file'] : '';
            var file1 = sampul != null ? sampul['file1'] : '';

            editor();

            $('#setSampul').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                tinyMCE.triggerSave();
                $.ajax({
                    url: "{{ route('raport-save_template_sampul') }}",
                    type: 'POST',
                    data: $(this).serialize() + "&id_ta_sm={{ $_GET['key'] }}",
                    beforeSend: function() {
                        $('#btnSave').html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    },
                    success: function(response) {
                        $('#btnSave').html(
                            '<i class="fas fa-sync-alt"></i> Perbarui');
                        swa(response.status.toUpperCase() + "!", response.message, response
                            .icon);
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $("#tahun_ajaran").change(function() {
                var tahun = $(this).val();
                window.location.href = "template_sampul?key=" + tahun;
            });

            $("#logo-atas").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-atas').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-atas')[0]);
                    form.append('tahun_ajaran', '{{ $_GET['key'] }}');
                    uploadAttach("{{ route('raport_upload-template_logo_atas') }}", form);
                }
            });
            $("#logo-tengah").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-tengah').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-tengah')[0]);
                    form.append('tahun_ajaran', '{{ $_GET['key'] }}');
                    uploadAttach("{{ route('raport_upload-template_logo_tengah') }}", form);
                }
            });

            $("#title1").on("change keyup paste", function() {
                var currentTitle = $(this).val();
                if (currentTitle === title) {
                    return;
                }
                title = currentTitle;
                $('#prevTitle').html(currentTitle);
            });

            $("#title2").on("change keyup paste", function() {
                var currentTitle = $(this).val();
                if (currentTitle === sub_title) {
                    return;
                }
                sub_title = currentTitle;
                $('#prevTitle2').html(currentTitle);
            });

            $("#footer").on("change keyup paste", function() {
                var currentTitle = $(this).val();
                if (currentTitle === footer) {
                    return;
                }
                footer = currentTitle;
                $('#prevFooter').html(currentTitle);
            });
        });

        function submitKop() {
            $('#setSampul').submit();
        }

        function editor() {
            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "200",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');
                    input.onchange = function() {

                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                        tinymce.triggerSave();
                    };

                    input.click();
                }
            });
        }

        function uploadAttach(action, data) {
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: action,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    if (data.src.includes('kanan')) {
                        file = data.src;
                    } else {
                        file1 = data.src;
                    }
                },
                error: function(e) {
                    console.log("error", e.responseText);
                    $.toast({
                        heading: "ERROR!!",
                        text: "file tidak terbaca",
                        icon: 'error',
                        showHideTransition: 'fade',
                        allowToastClose: true,
                        hideAfter: 5000,
                        position: 'top-right'
                    });
                }
            });
        }

        function CopyToClipboard(containerid) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select().createTextRange();
                document.execCommand("copy");
            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
                document.execCommand("copy");
                alert("Instruksi sudah tersalin, silahkan tempelkan di kolom instruksi samping kiri")
            }
        }
    </script>
@endsection
