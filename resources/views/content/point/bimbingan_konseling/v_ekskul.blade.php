@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template' . $ext . '/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                    </div>
                    <div class="col-md-6">
                        <div class="float-right">
                            <form class="form-inline">
                                <label class="col-form-label" for="inlineFormInputName2">Filter: </label>
                                <select name="tahun" id="tahun" class="form-control mx-sm-2">
                                    <option value="">Pilih Tahun ..</option>
                                    @foreach ($tahun as $thn)
                                        <option value="{{ substr($thn['tahun_ajaran'], 0, 4) }}"
                                            {{ $_GET['tahun'] == substr($thn['tahun_ajaran'], 0, 4) ? 'selected' : '' }}>
                                            {{ $thn['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="table-responsive">
                    <p></p>
                    <table class="table table-striped table-bordered table-hover" id="data-tabel">
                        <thead>
                            <tr class="bg-info">
                                <th rowspan="2" class="text-center">#</th>
                                <th rowspan="2" class="text-center">Nama</th>
                                <th colspan="2" class="text-center">Jumlah</th>
                                <th rowspan="2" class="text-center">Aksi</th>
                            </tr>
                            <tr class="bg-info">
                                <th class="text-center">Laki - laki</th>
                                <th class="text-center">Perempuan</th>
                            </tr>
                        </thead>
                        {{-- <tbody id="result_data">
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($anggota as $item)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ $item['nama'] }}</td>
                                    <td class="text-center">{{ $item['l'] != null ? $item['l'] : '0' }}</td>
                                    <td class="text-center">{{ $item['p'] != null ? $item['p'] : '0' }}</td>
                                    @if (session('role') != 'supervisor')
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="edit"
                                                data-id="{{ $item['id'] }}">
                                                <i class="material-icons list-icon md-18 text-info">edit</i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody> --}}
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_anggota">
                        <input type="hidden" name="id_ekstra" id="id_ekstra">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Laki-laki</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon">
                                        <i class="fas fa-male"></i>
                                    </span>
                                    <input class="form-control" id="l" name="l" placeholder="Jumlah Anggota"
                                        type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Perempuan</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon">
                                        <i class="fas fa-female"></i>
                                    </span>
                                    <input class="form-control" id="p" name="p" placeholder="Jumlah Anggota"
                                        type="number">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'l',
                        defaultContent: "0",
                        name: 'l',
                        className: 'text-center'
                    },
                    {
                        data: 'p',
                        defaultContent: "0",
                        name: 'p',
                        className: 'text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ]
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kesiswaan_update-anggota_ekskul') }}",
                    method: "PUT",
                    data: $(this).serialize() + "&tahun={{ $_GET['tahun'] }}",
                    dataType: "json",
                    beforeSend: function() {
                        $('#result_data').html(
                            '<tr><td colspan="5" class="text-center"><i class="fa fa-spin fa-spinner"></i>Sedang Mengupdate Data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#CustomerForm').trigger("reset");
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#tahun').change(function() {
                let tahun = $(this).val();
                if (tahun) {
                    window.location.href = "?tahun=" + tahun;
                }
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_detail-anggota_ekskul') }}",
                    data: {
                        id,
                        tahun: "{{ $_GET['tahun'] }}"
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#modelHeading').html("Edit Jumlah Anggota");
                        $('#id_anggota').val(data.id);
                        $('#id_ekstra').val(data.id_ekstra);
                        $('#l').val(data.l);
                        $('#p').val(data.p);
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        })

        // function editEkskul(id) {
        //     $.ajax({
        //         type: 'POST',
        //         url: "{{ route('kesiswaan_detail-anggota_ekskul') }}",
        //         data: {
        //             id
        //         },
        //         beforeSend: function() {
        //             $('.edit' + id).html('<i class="fa fa-spin fa-spinner text-info"></i>');
        //         },
        //         success: function(data) {
        //             $('.edit' + id).html('<i class="material-icons list-icon md-18 text-info">edit</i>');
        //             $('#modelHeading').html("Edit Data");
        //             $('#saveBtn').val("edit-user");
        //             $('#id_anggota').val(data.id);
        //             $('#ekskul').val(data.id_ekstra).trigger('change');
        //             $('#l').val(data.l);
        //             $('#p').val(data.p);
        //             $('#tahun_ajaran').val(data.tahun);
        //             $('#ajaxModel').modal('show');
        //             $('#action').val('Edit')
        //         }
        //     });
        // }
    </script>
@endsection
