@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-6 col-12">
                <div class="float-right">
                    <button class="btn btn-outline-info mt-1" id="addData"><i class="fas fa-plus-circle"></i>
                        Tambah</button>
                    <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                        Import</button>
                    <button id="btnTrash" class="btn btn-outline-danger mt-1"><i class="fas fa-trash-restore"></i>
                        Trash</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="data_jenis">
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($jenis as $jn)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $jn['nama'] }}</td>
                                <td>{{ $jn['kategori'] }}</td>
                                <td>{{ $jn['keterangan'] }}</td>
                                <td>
                                    <a href="javascript:void(0)" class="edit" data-id="{{ $jn['id'] }}">
                                        <i class="material-icons list-icon md-18 text-info">edit</i>
                                    </a>
                                    <a href="javascript:void(0)" class="delete" data-id="{{ $jn['id'] }}">
                                        <i class="material-icons list-icon md-18 text-danger">delete</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formData" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_jenis_beasiswa">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Jenis</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kategori</label>
                                    <div class="col-sm-12">
                                        <select name="kategori" id="kategori" class="form-control">
                                            <option value="akademik">Akademik</option>
                                            <option value="non-akademik" selected>Non Akademik</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" class="form-control" id="keterangan"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="0">Disabled</option>
                                            <option value="1" selected>Enabled</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title"><i class="fas fa-trash-restore"></i> Data Trash</h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="height: 400px; overflow-y: auto;">
                        <p></p>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Keterangan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_trash">
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark btn-rounded ripple text-left" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form action="javascript:void(0)" id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons text-info" style="font-size: 4.5rem;">file_download</i><br>
                                        <b class="text-info">Choose the file to be imported</b>
                                        <p class="mb-0">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" class="text-info p-1 rounded"> <i
                                                class="material-icons text-info">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template for import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const actualBtn = document.getElementById('actual-btn');
            const fileChosen = document.getElementById('file-chosen');
            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            });

            $('#addData').click(function() {
                $('#formData').trigger("reset");
                $('#modelHeading').html("Tambah Data");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Jenis Beasiswa');
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('point_jenis_beasiswa-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#modelHeading').html("Edit Data");
                        $('#id_jenis_beasiswa').val(data.id);
                        $('#nama').val(data.nama);
                        $('#kategori').val(data.kategori).trigger('change');
                        $('#keterangan').val(data.keterangan);
                        $('#status').val(data.status).trigger('change');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '#btnTrash', function() {
                $.ajax({
                    type: 'GET',
                    url: "{{ route('point_jenis_beasiswa-data_trash') }}",
                    beforeSend: function() {
                        $('#data_trash').html(
                            '<tr><td colspan="5" class="text-center">sedang memproses data trash...</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data_trash').html(data);
                        $('#modalTrash').modal('show');
                    }
                });
            });

            $('#formData').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('point_jenis_beasiswa-simpan') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('point_jenis_beasiswa-update') }}";
                }
                $.ajax({
                    url: action_url,
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#formData').trigger("reset");
                        }
                        $('#data_jenis').html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_jenis_beasiswa-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_jenis').html(data.html);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.restore', function() {
                let id = $(this).data('id');
                let loader = $(this);
                let konfirmasi = confirm("Apa kamu yakin ingin mengembalikan data ini?");
                if (konfirmasi == true) {
                    $.ajax({
                        url: "{{ route('point_jenis_beasiswa-restore') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_trash').html(data.trash);
                            $('#data_jenis').html(data.jenis);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                } else {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            });

            $(document).on('click', '.hardDelete ', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_jenis_beasiswa-hard_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_trash').html(data.trash);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                let actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("formImport"));
                // let formData = new FormData($('#formImport'));
                $.ajax({
                    type: "POST",
                    url: "{{ route('point_jenis_beasiswa-import') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formImport').trigger("reset");
                            $('#importModal').modal('hide');
                        }
                        $('#importBtn').html('Simpan');
                        $('#data_jenis').html(data.jenis);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });
        });
    </script>


@endsection
