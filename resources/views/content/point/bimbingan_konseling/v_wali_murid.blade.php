@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <h2 class="box-title mt-1">{{ session('title') }}</h2>
                @if (session('role') != 'supervisor')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="addMutasi">
                                <button class="btn btn-info addData"><i class="fas fa-plus-circle"></i> Tambah</button>
                                <a href="javascript:void(0)" class="btn btn-purple import"><i class="fas fa-file-import"></i>
                                    Import</a>
                            </div>
                        </div>
                    </div>
                @endif
                <hr>
                <form action="javascript:void(0)" method="post" id="formSearchSiswa">
                    <div class="row mx-0 my-3">
                        <div class="col-md-2 pl-0">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Filter
                                Berdasarkan:</label>
                            <select id="asal_data" name="asal_data" class="form-control">
                                <option value="" disabled>Pilih Asal data..</option>
                                <option value="ortu" {{ $asal == 'ortu' ? 'selected' : '' }}>Orang Tua</option>
                                <option value="siswa" {{ $asal == 'siswa' ? 'selected' : '' }}>Siswa</option>
                            </select>
                        </div>
                        <div class="col-md-2 pl-0">
                            <div class="form-group">
                                <label for="">Tahun Ajaran</label>
                                <select name="tahun_ajar" class="form-control" id="tahun_ajar">
                                    @foreach ($tahun as $th)
                                        @php
                                            $ajar = explode('/', $th['tahun_ajaran']);
                                        @endphp
                                        <option value="{{ $ajar[0] }}" {{ $ajar[0] == $tahuns ? 'selected' : '' }}>
                                            {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 mb-2 filter_siswa" style="display: {{ $asal == 'siswa' ? ' ' : 'none' }}">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Jurusan:</label>
                            <select id="id_jurusan" name="id_jurusan" class="form-control">
                                <option value="">--Pilih Jurusan--</option>
                                @foreach ($jurusan as $jr)
                                    @php
                                        $select = '';
                                        if ($select_jurusan == (new \App\Helpers\Help())->encode($jr['id'])) {
                                            $select = 'selected';
                                        }
                                    @endphp
                                    <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}"
                                        {{ $select }}>
                                        {{ $jr['nama'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Keyword
                                Pencarian:</label>
                            <input type="text" name="search" id="search" class="form-control"
                                value="{{ $search }}">
                        </div>
                        <div class="col-lg-2">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">&nbsp;</label>
                            <input type="submit" onclick="search_siswa('{{ $routes }}')" value="Mulai Cari"
                                class="btn btn-outline-info btn-block">
                        </div>
                    </div>
                </form>
                <hr>
                <div class="widget-body clearfix">
                    <div class="table-responsive">
                        <table class="table table-bordered mr-b-0 table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nama Siswa</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($ortu))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($ortu as $ort)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($ort['nama']) }}</td>
                                            <td>{{ Str::upper($ort['siswa']) }}</td>
                                            <td>{{ $ort['telepon'] }}</td>
                                            <td>{{ $ort['email'] }}</td>
                                            <td>
                                                <button data-toggle="collapse" data-target="#detail{{ $ort['id'] }}"
                                                    class="btn btn-sm btn-purple"><i
                                                        class="fas fa-info-circle"></i></button>
                                                @if (session('role') != 'supervisor')
                                                    <a href="javascript:void(0)" data-id="{{ $ort['id'] }}"
                                                        class="edit btn btn-sm btn-info"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $ort['id'] }}"
                                                        class="delete btn btn-sm btn-danger"><i
                                                            class="fas fa-trash"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $ort['id'] }}"
                                                        class="reset_key btn btn-sm btn-warning"><i
                                                            class="fas fa-key"></i></a>
                                                @endif

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="detail{{ $ort['id'] }}">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" rowspan="5"
                                                                    class="vertical-middle text-center"><img
                                                                        src="{{ $ort['file'] }}" alt="" height="150px">
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <th>Nama</th>
                                                                <td>{{ Str::upper($ort['nama']) }}</td>

                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <th>NIK</th>
                                                                <td>{{ $ort['nik'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <th>Agama</th>
                                                                <td>{{ $ort['agama'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <th>Telepon</th>
                                                                <td>{{ $ort['telepon'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Email</th>
                                                                <td>{{ Str::upper($ort['email']) }}</td>
                                                                <td></td>
                                                                <th>Reset Pasword</th>
                                                                <td>{{ $ort['first_password'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Di Buat</th>
                                                                <td>{{ $ort['created_at'] }}</td>
                                                                <td></td>
                                                                <th>Pekerjaan</th>
                                                                <td>{{ $ort['pekerjaan'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>NIS Siswa</th>
                                                                <td>{{ Str::upper($ort['nis']) }}</td>
                                                                <td></td>
                                                                <th>NISN Siswa</th>
                                                                <td>{{ $ort['nisn'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Jurusan Siswa</th>
                                                                <td>{{ Str::upper($ort['jurusan']) }}</td>
                                                                <td></td>
                                                                <th>Rombel Siswa</th>
                                                                <td>{{ $ort['rombel'] }}</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="dataTranskrip' . $sw['id'] . '">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $pagination !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalOrtu" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formOrtu" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_ortu">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <input type="text" name="nama" id="nama" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <input type="text" name="nik" id="nik" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Rombel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="rombel" id="rombel" class="form-control" required>
                                                <option value="">Pilih Rombel</option>
                                                @foreach ($rombel as $r)
                                                    <option value="{{ $r['id'] }}">{{ $r['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Siswa</label>
                                    <div class="col-md-9">
                                        <select name="siswa" id="siswa" class="form-control" required disabled>
                                            <option value="">Pilih Siswa..</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pekerjaan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Ortu</h5>
                </div>
                <form action="javascript:void(0)" id="importOrtu" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" style="color:#03a9f3">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>







    <script type="text/javascript">
        $(function() {
            const actualBtn = document.getElementById('actual-btn');
            const fileChosen = document.getElementById('file-chosen');
            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="rombel"]').on('change', function() {
                let id_rombel = $(this).val();
                if (id_rombel) {
                    $.ajax({
                        url: "{{ route('rombel-kelas_siswa') }}",
                        type: "POST",
                        data: {
                            id_rombel
                        },
                        beforeSend: function() {
                            $('#siswa').html(
                                '<option value="">Memproses data siswa...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="siswa"]').html(
                                    '<option value="">No siswa found</option>');
                            } else {
                                var s = '<option value="">Pilih siswa..</option>';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_siswa + '">' + row
                                        .nama +
                                        '</option>';

                                })
                                $('select[name="siswa"]').removeAttr('disabled');
                            }
                            $('select[name="siswa"]').html(s)
                        }
                    });
                }
            })

            $('#asal_data').on('change', function() {
                let asal = $(this).val();
                if (asal == 'ortu') {
                    $('.filter_siswa').hide();
                } else {
                    $('.filter_siswa').show();
                }
            })

            $(document).on('click', '.addData', function() {
                $('#formOrtu').trigger("reset");
                $('#modelHeading').html("Tambah Orang Tua");
                $('#modalOrtu').modal('show');
                $('#action').val('Add');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });

            $(document).on('click', '.import', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
            });

            $('body').on('submit', '#importOrtu', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importOrtu"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-ortu') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#importBtn').html('Simpan');
                        }
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-orang_tua') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Orang Tua");
                        $('#id_ortu').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nik').val(data.nik);
                        $('#rombel').val(data.id_rombel).trigger("change");
                        $('#agama').val(data.agama).trigger("change");
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#pekerjaan').val(data.pekerjaan);
                        $('#old_image').val(data.old_image);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        ortu_edit(data.id_siswa, data.id_rombel);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#modalOrtu').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete-orang_tua') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.reset_key', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin mereset Password!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('reset_password-ortu') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-key"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('body').on('submit', '#formOrtu', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var actionType = $('#saveBtn').val();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-orang_tua') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-orang_tua') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status != "gagal") {
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



        });

        function ortu_edit(id_siswa, id_rombel) {
            $.ajax({
                url: "{{ route('edit_ortu-orang_tua') }}",
                type: "POST",
                data: {
                    id_siswa,
                    id_rombel
                },
                beforeSend: function() {
                    $('#siswa').append(
                        '<option value="">--- No Siswa Found ---</option>');
                },
                success: function(fb) {
                    // console.log(fb);
                    $('#siswa').html(fb);
                    $('select[name="siswa"]').removeAttr('disabled');
                }
            });
            return false;
        }

        function search_siswa(routes) {
            var asal = (document.getElementById("asal_data") != null) ? document.getElementById("asal_data").value : "";
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var tahun = (document.getElementById("tahun_ajar") != null) ? document.getElementById("tahun_ajar").value : "";
            var jurusan = (document.getElementById("id_jurusan") != null) ? document.getElementById("id_jurusan").value :
                "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?based=" + asal + "&tahun=" + tahun + "&jurusan=" + jurusan + "&search=" + search;
            document.location = url;
        }
    </script>


@endsection
