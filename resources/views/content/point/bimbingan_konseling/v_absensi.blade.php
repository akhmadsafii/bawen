@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .dataTables_wrapper .dataTables_paginate {
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #9768a0;">
                                <div class="card-header" role="tab" id="heading4" style="background: #9768a0;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            {{ Session::get('title') }}
                                        </a>
                                    </h5>
                                </div>
                                <form action="javascript:void(0)" method="post" id="formSearchSiswa">
                                    <div class="row mx-0 my-3">
                                        <div class="col-lg-6 mb-1">
                                            <label class="filter-col" style="margin-right:0;"
                                                for="pref-search">Rombel:</label>
                                            <select id="id_rombel" name="id_rombel" class="form-control">
                                                <option value="">--Pilih Rombel--</option>
                                                @foreach ($rombel as $rm)
                                                    <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-6 mb-1">
                                            <label class="filter-col" style="margin-right:0;"
                                                for="pref-orderby">Mapel:</label>
                                            <select id="id_mapel" name="id_mapel" class="form-control" disabled>
                                                <option value="">--Pilih Mapel--</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-outline-info btn-block">
                                                Mulai Cari</button>
                                        </div>
                                    </div>
                                </form>

                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <div style="width: 100%;">
                                            <div class="table-responsive">
                                                <table class="table table-condensed table-hover"
                                                    style="border-collapse:collapse;">
                                                    <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>Nama</th>
                                                            <th>NIS</th>
                                                            <th>NISN</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="data-siswa">
                                                        <tr>
                                                            <td colspan="4" class="text-center">Silahkan masukan filter diatas</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_rombel"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('kesiswaan_absensi-mapel') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $("#id_mapel").html(
                                '<option value="">Loading...</option>');
                            $("#id_mapel").attr("disabled", true);
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_mapel"]').html(
                                    '<option value="">--- No Mapel Found ---</option>');
                            } else {
                                var s = '';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_mapel + '">' + row
                                        .mapel + '</option>';
                                    $("#id_mapel").attr("disabled", true);

                                })
                                $('select[name="id_mapel"]').removeAttr('disabled');
                            }
                            $('select[name="id_mapel"]').html(s)
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                $.ajax({
                    url: "{{ route('kesiswaan_absensi-get_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $("#data-siswa").html(
                            '<tr><td colspan="4" class="text-center">Sedang memproses data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        if (data.status) {
                            noti(data.icon, data.message);
                            $('#data-siswa').html('<tr><td colspan="4" class="text-center">Filter yang anda masukan bermasalah</td></tr>');
                        } else {
                            $('#data-siswa').html(data);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('program/point/sanksi/delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>


@endsection
