@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
        integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js">
    </script>

    <style>
        button,
        input[type="submit"],
        input[type="reset"] {
            background: #575580;
            color: #fff;
            border: none;
            /* padding: 0; */
            font: inherit;
            cursor: pointer;
            outline: inherit;
        }

        .pace {
            display: none;
        }

        .chat {
            margin-top: auto;
            margin-bottom: auto;
        }

        .card {
            min-height: 500px;
            /* height: 629px; */
            max-height: 629px;
            background-color: rgb(62 57 154 / 63%) !important;
        }

        .contacts_body {
            padding: 0.75rem 0 !important;
            overflow-y: auto;
            white-space: nowrap;
        }

        .msg_card_body {
            overflow-y: auto;
            margin-bottom: 90px;
        }

        .card-header {
            border-radius: 15px 15px 0 0 !important;
            border-bottom: 0 !important;
        }

        .card-footer {
            width: 90%;
            position: absolute;
            bottom: 0;
        }

        .container {
            align-content: center;
        }

        .search {
            border-radius: 15px 0 0 15px !important;
            background-color: rgba(0, 0, 0, 0.3) !important;
            border: 0 !important;
            color: white !important;
        }

        .search:focus {
            box-shadow: none !important;
            outline: 0px !important;
        }

        .type_msg {
            background-color: rgba(0, 0, 0, 0.3) !important;
            border: 0 !important;
            color: white !important;
            height: 60px !important;
            overflow-y: auto;
        }

        .type_msg:focus {
            box-shadow: none !important;
            outline: 0px !important;
        }

        .attach_btn {
            border-radius: 15px 0 0 15px !important;
            background-color: rgba(0, 0, 0, 0.3) !important;
            border: 0 !important;
            color: white !important;
            cursor: pointer;
        }

        .send_btn {
            border-radius: 0 15px 15px 0 !important;
            background-color: rgba(0, 0, 0, 0.3) !important;
            border: 0 !important;
            color: white !important;
            cursor: pointer;
        }

        .search_btn {
            border-radius: 0 15px 15px 0 !important;
            background-color: rgba(0, 0, 0, 0.3) !important;
            border: 0 !important;
            color: white !important;
            cursor: pointer;
        }

        .contacts {
            list-style: none;
            padding: 0;
        }

        .contacts li {
            width: 100% !important;
            padding: 5px 10px;
            margin-bottom: 15px !important;
        }

        .active {
            background-color: rgba(0, 0, 0, 0.3);
        }

        .user_img {
            height: 70px;
            width: 70px;
            border: 1.5px solid #f5f6fa;

        }

        .user_img_msg {
            height: 40px;
            width: 40px;
            border: 1.5px solid #f5f6fa;

        }

        .img_cont {
            position: relative;
            height: 70px;
            width: 70px;
        }

        .img_cont_msg {
            height: 40px;
            width: 40px;
        }

        .online_icon {
            position: absolute;
            height: 15px;
            width: 15px;
            background-color: #4cd137;
            border-radius: 50%;
            bottom: 0.2em;
            right: 0.4em;
            border: 1.5px solid white;
        }

        .offline {
            background-color: #c23616 !important;
        }

        .user_info {
            margin-top: auto;
            margin-bottom: auto;
            margin-left: 15px;
        }

        .user_info span {
            font-size: 20px;
            color: white;
        }

        .user_info p {
            font-size: 10px;
            color: rgba(255, 255, 255, 0.6);
        }

        .video_cam {
            margin-left: 50px;
            margin-top: 5px;
        }

        .video_cam span {
            color: white;
            font-size: 20px;
            cursor: pointer;
            margin-right: 20px;
        }

        .msg_cotainer {
            margin-top: auto;
            margin-bottom: auto;
            margin-left: 10px;
            border-radius: 25px;
            background-color: #fff;
            padding: 10px;
            position: relative;
            max-width: 88%;

        }

        .msg_cotainer_send {
            margin-top: auto;
            margin-bottom: auto;
            margin-right: 10px;
            border-radius: 25px;
            background-color: #78e08f;
            padding: 10px;
            position: relative;
            max-width: 88%;

        }

        .msg_time {
            position: absolute;
            left: 0;
            bottom: -15px;
            color: rgba(255, 255, 255, 0.5);
            font-size: 10px;
        }

        .msg_time_send {
            position: absolute;
            right: 0;
            bottom: -15px;
            color: rgba(255, 255, 255, 0.5);
            font-size: 10px;
        }

        .msg_head {
            position: relative;
        }

        #action_menu_btn {
            position: absolute;
            right: 10px;
            top: 10px;
            color: white;
            cursor: pointer;
            font-size: 20px;
        }

        .action_menu {
            z-index: 1;
            position: absolute;
            padding: 15px 0;
            background-color: rgba(0, 0, 0, 0.5);
            color: white;
            border-radius: 15px;
            top: 30px;
            right: 15px;
            display: none;
        }

        .action_menu ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .action_menu ul li {
            width: 100%;
            padding: 10px 15px;
            margin-bottom: 5px;
        }

        .action_menu ul li i {
            padding-right: 10px;

        }

        .action_menu ul li:hover {
            cursor: pointer;
            background-color: rgba(0, 0, 0, 0.2);
        }

        .input-group-text {
            display: flex;
            align-items: center;
            margin-bottom: 0px;
            font-size: 20px;
            ;
            font-weight: 400;
            line-height: 1.5;
            color: rgb(73, 80, 87);
            text-align: center;
            white-space: nowrap;
            background-color: rgb(233, 236, 239);
            padding: 0.375rem 0.75rem;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(206, 212, 218);
            border-image: initial;
            border-radius: 0.25rem;
        }

        @media(max-width: 576px) {
            .contacts_card {
                margin-bottom: 15px !important;
            }
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row h-100">
                    <div class="col-md-4 chat">
                        <div class="card mb-sm-3 mb-md-0 contacts_card">
                            <div class="card-header">
                                <div class="input-group">
                                    <input type="text" placeholder="Search..." name="" class="form-control search">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text search_btn"><i class="fas fa-search"
                                                style="padding: 10px 0.75rem;"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body contacts_body">
                                <ul class="contacts">
                                    @foreach ($list_chat as $lc)
                                        <li class="active"
                                            onclick="return view_chat('{{ $lc['nama'] }}', '{{ $lc['email'] }}', '{{ $lc['file'] }}');">
                                            <div class="d-flex bd-highlight">
                                                <div class="img_cont">
                                                    <img src="{{ $lc['file'] }}" class="rounded-circle user_img">
                                                    <span class="online_icon"></span>
                                                </div>
                                                <div class="user_info">
                                                    <span>{{ $lc['nama'] }}</span>
                                                    @if (session('role') == 'bk')
                                                        <p>Orang tua dari {{ $lc['siswa'] }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-footer"></div>
                        </div>
                    </div>
                    <div class="col-md-8 chat">
                        <form action="javascript:void(0)" id="input_chat" name="input_chat" method="post">
                            <div class="card view_chat">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}


    <script type="text/javascript">
        // $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // window.scrollTo(0,document.querySelector(".msg_card_body").scrollHeight);

        var load_name = '';
        var load_email = '';
        var load_gambar = '';

        const messaging = firebase.messaging();
        messaging.getToken({
            vapidKey: "BIcPxtrCV22Fiegga8Gt4miltCzILbdoUFzBhJuHB8dbVJS03H8RF_MC0nvsiN4YMouJkuXqRY-8a1NpdBE9XsM"
        });

        function sendTokenToServer(fcm_token) {
            $.ajax({
                type: 'POST',
                url: "{{ url('program/' . Request::segment(2) . '/pesan/save_token') }}",
                data: {
                    fcm_token
                },
                success: function(data) {
                    // console.log(data.message)
                }
            });
        }

        function retrieveToken() {
            messaging.getToken().then((currentToken) => {
                if (currentToken) {
                    sendTokenToServer(currentToken);
                } else {
                    alert('You should allow notification!');
                }
            }).catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
            });
        }

        retrieveToken();

        messaging.onTokenRefresh(() => {
            // console.log("refresh");
            retrieveToken();
        });

        messaging.onMessage((payload) => {
            // console.log('Message received.');
            // console.log(payload);
            view_chat(load_name, load_email, load_gambar);
            // $('.msg_card_body').scrollTop($('.msg_card_body')[0].scrollHeight);
            // $(".view_chat" ).load(window.location.href + " .view_chat" );
        })

        view_chat("", "", "");

        $("#input_chat").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "pesan_point/save_chat",
                beforeSend: function() {
                    $("#send_msg").html(
                        '<i class="fa fa-spin fa-spinner" style="padding: 14px"></i> Loading'
                    );
                },
                success: function(r) {
                    if (r.status == 'berhasil') {
                        $("#send_msg").html(
                            '<i class="fas fa-location-arrow" style="padding: 14px 0.75rem;"></i>'
                        );
                        noti(r.icon, r.success);
                    } else {
                        noti(r.icon, r.success);
                        $("#send_msg").html(
                            '<i class="fas fa-location-arrow" style="padding: 14px 0.75rem;"></i>'
                        );
                    }
                }
            });
            return false;
        });

        function view_chat(nama, email, gambar) {
            load_name = nama;
            load_email = email;
            load_gambar = gambar;
            // nama_ekstra = nama;
            if (nama == "") {
                $(".view_chat").html(
                    '<div class="alert alert-warning" style="margin-bottom: 0; color: #ffffff; background-color: #605ca891;">Silakan Pilih Wali Murid  di samping</div>'
                );
            } else {

                // $(".view_chat").html(
                //     '<center><i class="fas fa-spin fa-spinner"></i> Loading</center>'
                // );
                $.ajax({
                    url: "pesan_point/get_chat",
                    type: "POST",
                    data: {
                        email_penerima: email
                    },
                    success: function(data) {
                        // console.log(data);
                        $(".view_chat").show('slow');
                        html =
                            '<div class="card-header msg_head"> <div class="d-flex bd-highlight"> <div class="img_cont"> <img src="' +
                            gambar +
                            '" class="rounded-circle user_img"> <span class="online_icon"></span> </div> <div class="user_info"><span>Chat with ' +
                            nama +
                            '</span><p>1767  Messages<p> </div> </div> <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span> <div class="action_menu"> <ul> <li><i class="fas fa-user-circle"></i> View profile</li> <li><i class="fas fa-users"></i> Add to close friends</li> <li><i class="fas fa-plus"></i> Add to group</li> <li><i class="fas fa-ban"></i> Block</li> </ul> </div> </div> <div class="card-body msg_card_body">';
                        $.each(data, function(k, v) {
                            if (v.email_pengirim === email) {
                                html +=
                                    '<div class="d-flex justify-content-start mb-4"> <div class="img_cont_msg"> <img src="' +
                                    v.file +
                                    '" class="rounded-circle user_img_msg"> </div> <div class="msg_cotainer"> ' +
                                    v.pesan + ' <span class="msg_time">' + v.waktu +
                                    '</span> </div> </div>';
                            } else {
                                html +=
                                    '<div class="d-flex justify-content-end mb-4">  <div  class="msg_cotainer_send"> ' +
                                    v.pesan + ' <span class="msg_time_send">' + v.waktu +
                                    '</span> </div><div class="img_cont_msg"> <img src="' +
                                    v.file +
                                    '" class="rounded-circle user_img_msg"> </div> </div>';
                            }
                        });
                        html +=
                            '<div class="card-footer"> <div class="input-group"><input type="hidden" name="email_penerima" value="' +
                            email +
                            '"> <textarea name="message" class="form-control type_msg" placeholder="Type your message..." style="overflow:hidden"></textarea><div class="input-group-append"><span class="input-group-text send_btn" style="padding: 0"></span><button type="submit" id="send_msg" style="padding: 9px"><i class="fas fa-location-arrow" style="padding: 14px 0.75rem;"></i></button></div></div></div>';
                        $(".view_chat").html(html);
                    },
                })

            }
            return false;
        }

        // });

    </script>


@endsection
