@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .dataTables_wrapper .dataTables_paginate {
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #9768a0;">
                                <div class="card-header" role="tab" id="heading4" style="background: #9768a0;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            {{ Session::get('title') }}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <div style="width: 100%;">
                                            <div class="table-responsive">
                                                <table class="table table-striped" id="data-tabel">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Kriteria Pelanggaran</th>
                                                            <th>Bobot</th>
                                                            <th>Sanksi</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
   

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'bobot',
                        name: 'bobot'
                    },
                    {
                        data: 'sanksi',
                        name: 'sanksi'
                    },
                ]
            });            
        });

    </script>
@endsection
