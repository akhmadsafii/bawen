@extends('template/template_default/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div class="">
                        <div class="float-right widget-holder">
                            <form class="form-inline" onsubmit="reloadPage('{{ $routes }}')">
                                <div class="form-group">
                                    <label for="inputPassword6">Filter Pencarian :</label>
                                    <select name="rombel" id="rombel" class="form-control mx-sm-3">
                                        <option value="">Pilih Rombel..</option>
                                        <option value="all">All</option>
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rmb)
                                                    <option value="{{ $rmb['id'] }}"
                                                        {{ $rmb['id'] == $rombel ? 'selected' : '' }}>
                                                        {{ $rmb['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                    <input type="text" id="search" class="form-control mx-sm-3"
                                        placeholder="keyword nisn atau nama siswa" value="{{ $search }}">
                                    <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Informasi</th>
                                        <th>Telepon</th>
                                        <th>Email</th>
                                        <th>Rombel</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($siswa))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $ssw)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td>{{ $ssw['nama'] }} <p class="m-0">NIS/NISN.
                                                        {{ $ssw['nis'] ?? ('-' . ' / ' . $ssw['nisn'] ?? '-') }}</p>
                                                </td>
                                                <td class="vertical-middle">{{ $ssw['telepon'] ?? '-' }}</td>
                                                <td class="vertical-middle">{{ $ssw['email'] ?? '-' }}</td>
                                                <td>
                                                    <b>{{ $ssw['rombel'] }}</b>
                                                    <p class="m-0">Jurusan. {{ $ssw['jurusan'] ?? '-' }}</p>
                                                </td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" class="id_kelas_siswa"
                                                            value="{{ (new \App\Helpers\Help())->encode($ssw['id']) }}">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!! $pagination !!}
                            </div>
                            <div class="col-md-12">
                                @if (!empty($siswa))
                                    <div class="float-right">
                                        <button class="btn btn-info" id="btnProgress"><i class="fas fa-check-circle"></i>
                                            Proses</button>

                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var kelas_siswa = [];
        $(function() {
            $(document).on('click', '.id_kelas_siswa', function() {
                $('.id_kelas_siswa:checked').each(function(i) {
                    kelas_siswa[i] = $(this).val();
                });
            })

            $(document).on('click', '#btnProgress', function() {
                if (kelas_siswa.length === 0) {
                    alert('Harap pilih siswa dahulu');
                } else {
                    kelas_siswa = kelas_siswa.join('-');
                    filter();
                }
            })

        })

        function filter(routes = null) {
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "/step2?siswa=" + kelas_siswa;
            document.location = url;
        }

        function reloadPage(routes) {
            $(".btnSearch").attr("disabled", true);
            $(".btnSearch").html(
                '<i class="fa fa-spin fa-spinner"></i> Searching..');
            var searchs = (($('#search') != null) ? $('#search').val() : '');
            var id_rombel = (($('#rombel') != null) ? $('#rombel').val() : '');
            var tahun = "{{ session('tahun') }}";
            var search = searchs.replace(/ /g, '-')
            var url_aslis = window.location.href;
            var explode_urls = url_aslis.split("?")[0];
            var urls = explode_urls + "?tahun=" + tahun + "&rombel=" + id_rombel + "&search=" + search;
            document.location = urls;
        }
    </script>
@endsection
