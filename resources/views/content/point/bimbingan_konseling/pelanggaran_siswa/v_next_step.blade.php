@extends('template/template_default/app')
@section('content')
    @if ($_GET['siswa'] == '')
        <script>
            window.location = "{{ route('point_pelanggaran_siswa-beranda') }}";
        </script>
    @endif
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="row">
                        <div class="col-md-6 my-auto">
                            <h3 class="box-title m-0">{{ Session::get('title') }}</h3>
                        </div>
                        <div class="col-md-6">
                            <div class="float-right">
                                <a href="{{ route('point_pelanggaran_siswa-beranda') }}" class="btn btn-danger"><i
                                        class="far fa-arrow-alt-circle-left"></i> Kembali</a>
                                <button class="btn btn-success" id="addData"><i class="fas fa-plus"></i> Tambah
                                    Data</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="widget-holder">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Informasi</th>
                                        <th>Telepon</th>
                                        <th>Email</th>
                                        <th>Rombel</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($siswa))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $ssw)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td>
                                                    {{ $ssw['nama'] }} <p class="m-0">NIS/NISN.
                                                        {{ $ssw['nis'] ?? ('-' . ' / ' . $ssw['nisn'] ?? '-') }}</p>
                                                </td>
                                                <td class="vertical-middle">{{ $ssw['telepon'] ?? '-' }}</td>
                                                <td class="vertical-middle">{{ $ssw['email'] ?? '-' }}</td>
                                                <td>
                                                    <b>{{ $ssw['rombel'] }}</b>
                                                    <p class="m-0">Jurusan. {{ $ssw['jurusan'] ?? '-' }}</p>
                                                </td>
                                                <td class="vertical-middle">
                                                    <a href="javascript:void(0)"
                                                        data-id="{{ (new \App\Helpers\Help())->encode($ssw['id']) }}"
                                                        class="btn btn-sm btn-danger delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-12 my-auto">
                                    <ul class="nav nav-pills">
                                        @php
                                            $count = 0;
                                        @endphp
                                        @foreach ($kategori as $kt)
                                            @php
                                                $count++;
                                            @endphp
                                            <li class="nav-item"><a
                                                    class="nav-link btn-purple m-1 {{ $count == 1 ? ' active' : '' }}"
                                                    href="#data{{ $kt['id'] }}"
                                                    data-toggle="tab">{{ $kt['nama'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-md-4 col-12 my-auto">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-info" id="saveBtn"><i
                                                class="fas fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="formPelanggaran">
                            <div class="card-body">
                                <div class="tab-content pt-2">
                                    @php
                                        $count_ktg = 0;
                                    @endphp
                                    @foreach ($kategori as $ktg)
                                        @php
                                            $count_ktg++;
                                        @endphp
                                        <div class="tab-pane {{ $count_ktg == 1 ? 'active' : '' }}"
                                            id="data{{ $ktg['id'] }}">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Bentuk Pelanggaran</th>
                                                            <th>Bobot</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if (!empty($ktg['pelanggaran']))
                                                            @php
                                                                $no = 1;
                                                            @endphp
                                                            @foreach ($ktg['pelanggaran'] as $plg)
                                                                <tr>
                                                                    <td class="vertical-middle">{{ $no++ }}</td>
                                                                    <td class="vertical-middle">{{ $plg['nama'] }}</td>
                                                                    <td class="vertical-middle">{{ $plg['point'] }}</td>
                                                                    <td>
                                                                        <label class="switch">
                                                                            <input type="checkbox" class="id_kelas_siswa"
                                                                                name="id_pelanggaran[]"
                                                                                value="{{ $plg['id'] }}">
                                                                            <span class="slider round"></span>
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan="4" class="text-center">Data saat ini tidak
                                                                    tersedia</td>
                                                            </tr>
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="siswaModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline float-right" id="formSearch">
                                <div class="form-group mb-2">
                                    <label class="col-form-label" for="l0">Filter</label>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <select name="id_rombel" id="" class="form-control">
                                        <option value="">Pilih Rombel..</option>
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ $rombel['id'] }}">
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info mb-2"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Informasi</th>
                                            <th>Telepon</th>
                                            <th>Email</th>
                                            <th>Rombel</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data-kelas_siswa">
                                        <tr>
                                            <td colspan="6" class="text-center">Data saat ini belum tersedia</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveSiswa"
                        value="create">Simpan</a>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
    <script>
        var siswa = "{{ $_GET['siswa'] }}";

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '#saveSiswa', function() {
                // console.log(siswa);
                arr_siswas = siswa.split("-");
                $('.kelas_siswa_check:checked').each(function(i) {
                    arr_siswas.push($(this).val());
                    // arr_siswas = ;
                });
                if (arr_siswas.length === 0) {
                    alert('Harap pilih siswa dahulu');
                } else {
                    siswa = arr_siswas.join('-');
                    filter();
                }
            })

            $('#addData').click(function() {
                $('#modelHeading').html("Tambah " + "{{ session('title') }}");
                $('#siswaModal').modal('show');
                $('#action').val('Add');
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    arr_siswa = siswa.split("-");
                    siswa = arr_siswa.filter(function(e) {
                        return e !== id
                    })
                    siswa = siswa.join('-');
                    filter();
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('#formPelanggaran').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('kesiswaan_pelanggaran_siswa-simpan_siswa') }}",
                    method: "POST",
                    data: $(this).serialize() + "&siswa={{ $_GET['siswa'] }}",
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $("input[type=checkbox]").prop('checked', false);
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formSearch').on('submit', function(event) {
                event.preventDefault();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('rombel-kelas_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        let html = '';
                        if (!$.trim(data)) {
                            html +=
                                '<tr><td colspan="6" class="text-center">Mohon maaf data kosong</td></tr>';
                        } else {
                            var i = 1;
                            $.each(data, function(k, v) {
                                html += '<tr><td class="vertical-middle">' + i +
                                    '</td><td>' + v.nama +
                                    '<p class="m-0">NIS/NISN.' + v.nis + ' / ' + v
                                    .nisn + '</p></td><td class="vertical-middle">' +
                                    v.telepon +
                                    '</td><td class="vertical-middle">' + v.email +
                                    '</td><td><b>' + v.rombel +
                                    '</b><p class="m-0">Jurusan. ' + v.jurusan +
                                    '</p></td><td><label class="switch"><input type="checkbox" class="kelas_siswa_check" value="' +
                                    v.id_code +
                                    '"><span class="slider round"></span></label></td></tr>';
                                i++;
                            });
                        }
                        $("#data-kelas_siswa").html(html);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#saveBtn').click(function() {
                $('#formPelanggaran').submit();
            });
        })

        function filter(routes = null) {
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?siswa=" + siswa;
            document.location = url;
        }
    </script>
@endsection
