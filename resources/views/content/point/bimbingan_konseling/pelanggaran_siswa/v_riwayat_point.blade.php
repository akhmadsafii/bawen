@extends('template/template_default/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <form class="form-material" id="formSearchSiswa">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <select class="form-control" id="id_rombel">
                                                    <option value="">Pilih Rombel..</option>
                                                    <option value="all">All</option>
                                                    @foreach ($jurusan as $jr)
                                                        <optgroup label="{{ $jr['nama'] }}">
                                                            @foreach ($jr['kelas'] as $kelas)
                                                        <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                            @foreach ($kelas['rombel'] as $rmb)
                                                                <option
                                                                    value="{{ (new \App\Helpers\Help())->encode($rmb['id']) }}">
                                                                    {{ $rmb['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                    </optgroup>
                                                    @endforeach
                                                </select>
                                                <label for="l38">Rombel</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <select class="form-control" id="tahun_ajaran">
                                                    <option>Pilih Tahun Ajaran</option>
                                                    @foreach ($tahun as $th)
                                                        <option value="{{ substr($th['tahun_ajaran'], 0, 4) }}">
                                                            {{ $th['tahun_ajaran'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <label for="l38">Tahun</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" class="btn btn-info" id="btnCari"><i
                                                    class="fas fa-search"></i>
                                                Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @if (!empty($pelanggaran))
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Profil</th>
                                        <th>Kelas</th>
                                        <th>Point</th>
                                        <th class="text-center">Jumlah Pelanggaran</th>
                                        <th>Telepon Ortu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($pelanggaran as $plg)
                                        <tr>
                                            <td class="vertical-middle">{{ $no++ }}</td>
                                            <td>
                                                <b>{{ $plg['nama'] }}</b>
                                                <p class="m-0">NIS/NISN. {{ $plg['nis'] ?? '-' }} /
                                                    {{ $plg['nisn'] ?? '-' }}</p>
                                            </td>
                                            <td><b>{{ $plg['rombel'] }}</b>
                                                <p class="m-0">{{ $plg['jurusan'] }}</p>
                                            </td>
                                            <td class="vertical-middle">{{ $plg['total_point'] }}</td>
                                            <td class="vertical-middle text-center">{{ $plg['jumlah_pelanggaran'] }}</td>
                                            <td class="vertical-middle">{{ $plg['telp_ortu'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <script>
        var id_rombel, tahun;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                id_rombel = $('#id_rombel').val();
                tahun = $('#tahun_ajaran').val();
                if (id_rombel && tahun) {
                    $("#btnCari").html(
                        '<i class="fa fa-spin fa-sync-alt"></i> Mencari..');
                    $("#btnCari").attr("disabled", true);
                    reloadPage();
                } else {
                    alert('Rombel dan tahun ajaran tidak boleh kosong');
                }
            });

        })

        function reloadPage() {
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?rombel=" + id_rombel + "&tahun=" + tahun;
            document.location = url;
        }
    </script>
@endsection
