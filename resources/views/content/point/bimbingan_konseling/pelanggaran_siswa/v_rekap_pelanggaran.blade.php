@extends('template/template_default/app')
@section('content')
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-holder">
                        <div class="widget-body clearfix">
                            <h5 class="box-title">REKAPAN PELANGGARAN</h5>
                            <hr>
                            <div class="row mb-2">
                                <div class="col-sm-9 col-md-9"></div>
                                <div class="col-sm-3 col-md-3">
                                    <form class="navbar-form" role="search">
                                        <div class="input-group">
                                            @php
                                                $serc = str_replace('-', ' ', $search);
                                            @endphp
                                            <input type="text" value="{{ $serc }}" id="search" name="search"
                                                class="form-control" placeholder="Search">
                                            <div class="input-group-btn">
                                                <a href="javascript:void(0)" id="fil"
                                                    onclick="filter('{{ $routes }}')" class="btn btn-info"><i
                                                        class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover widget-status-table">
                                            <thead>
                                                <tr class="bg-info">
                                                    <th rowspan="2">No</th>
                                                    <th rowspan="2">Tanggal</th>
                                                    <th rowspan="2">Nama Siswa</th>
                                                    <th rowspan="2">NIS</th>
                                                    <th rowspan="2">Rombel</th>
                                                    <th class="text-center" colspan="2">Pelanggaran</th>
                                                    <th class="text-center" rowspan="2">Wali Kelas</th>
                                                    @if (session('role') == 'admin-kesiswaan')
                                                        <th rowspan="2">Aksi</th>
                                                    @endif
                                                </tr>
                                                <tr class="bg-info">
                                                    <th class="text-center">Nama Pelanggaran</th>
                                                    <th class="text-center">Point</th>
                                                </tr>
                                            </thead>

                                            @if (!empty($pelanggaran))
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($pelanggaran as $pl)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($pl['tgl_pelanggaran']) }}
                                                        </td>
                                                        <td>{{ ucwords($pl['nama_siswa']) }}</td>
                                                        <td>{{ $pl['nis'] }}</td>
                                                        <td>{{ $pl['rombel'] }}</td>
                                                        <td>{{ $pl['pelanggaran'] }}</td>
                                                        <td>{{ $pl['point'] }}</td>
                                                        <td>{{ $pl['wali_kelas'] }}</td>
                                                        @if (session('role') == 'admin-kesiswaan')
                                                            <td class="text-center">
                                                                <a href="javascript:void(0)" class="delete"
                                                                    data-id="{{ $pl['id'] }}"><i
                                                                        class="material-icons list-icon md-18 text-danger">delete</i></a>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9" class="text-center">Data saat ini tidak tersedia</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                    {!! $pagination !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
