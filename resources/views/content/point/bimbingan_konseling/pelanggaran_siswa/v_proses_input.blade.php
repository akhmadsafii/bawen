@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .alert.alert-warning {
            text-align: center;
        }

        @media (max-width: 960px) {
            #cari {
                margin-top: 0px !important;
            }
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #605ca8;">
                                <div class="card-header" role="tab" id="heading4" style="background: #605ca8;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            Siswa yang melanggar
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <form name="next_input" action="javascript:void(0)" id="next_input">
                                            @csrf
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>NISN</th>
                                                        <th>Nama</th>
                                                        <th>Rombel</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="checkbox" name="" id=""></td>
                                                        <td>1</td>
                                                        <td>2</td>
                                                        <td>23</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    </script>


@endsection
