@extends('template/template_default/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"> {{ Session::get('title') }}</h5>
                    <hr>
                    <div style="width: 100%;">
                        <div class="row mb-2">
                            <div class="col-sm-9 col-md-9"></div>
                            <div class="col-sm-3 col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Siswa</th>
                                        <th>Kelas</th>
                                        <th>Pelanggaran</th>
                                        <th>Point</th>
                                        <th>Penanganan</th>
                                        <th rowspan="2">Aksi</th>
                                    </tr>
                                </thead>

                                <tbody id="data-siswa">
                                    @if (!empty($pelanggaran))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($pelanggaran as $pl)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td class="vertical-middle">
                                                    {{ (new \App\Helpers\Help())->getTanggal($pl['tgl_pelanggaran']) }}
                                                </td>
                                                <td class="vertical-middle"> <b>{{ ucwords($pl['nama']) }}</b>
                                                    <p class="m-0">NIS/NISN.
                                                        {{ $pl['nis'] . ' / ' . $pl['nisn'] }}
                                                    </p>
                                                </td>
                                                <td class="vertical-middle">
                                                    <p class="m-0">{{ $pl['rombel'] }}</p>
                                                    <small>{{ $pl['jurusan'] }}</small>
                                                </td>
                                                <td class="vertical-middle">{{ $pl['pelanggaran'] }}</td>
                                                <td class="vertical-middle text-center">{{ $pl['point'] }}</td>
                                                <td class="widget-status-table vertical-middle"><span
                                                        class="badge badge-{{ $pl['penanganan'] == 0 ? 'danger' : 'success' }} text-inverse">{{ $pl['penanganan'] == 0 ? 'Belum' : 'Sudah' }}</span>
                                                </td>
                                                <td class="vertical-middle">
                                                    <a href="javascript:void(0)" data-id="{{ $pl['id'] }}"
                                                        class="edit btn btn-info btn-sm"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $pl['id'] }}"
                                                        class="btn btn-danger btn-sm delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" class="text-center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                            {!! $pagination !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPelanggaran" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Edit Sanksi Pelanggaran</h5>
                </div>
                <form id="formPelanggaran" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-4">
                                <header class="text-center">
                                    <figure class="inline-block thumb-lg">
                                        <img src="{{ asset('asset/img/default-avatar.jpg') }}" class="img-thumbnail"
                                            alt="">
                                    </figure>
                                    <h4 class="mt-1" id="nama_siswa">EMMA WATSON</h4>
                                    <div class="contact-info-address text-bold">NIS/NISN : <span id="nisn">324734672 /
                                            375934579</span>
                                    </div>
                                </header>
                                <input type="hidden" name="id_pelanggaran" id="id_pelanggaran">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Jurusan</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="jurusan">Teknik Informatika</p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Rombel</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="rombel">VII MOA</p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Wali Kelas</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="wali_kelas">Titik widyastuti</p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Guru Penginput</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="guru">Waskito</p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Tanggal</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="tgl_pelanggaran">20 Mei 2021</p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Pelanggaran</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="pelanggaran">Terbukti merokok di depan sekolah
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label">Point</label>
                                    <div class="col-md-9">
                                        <p class="form-control-plaintext" id="point">50</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3 class="box-title">Pilih aksi untuk mengupdate</h3>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Sanksi Pelanggaran</label>
                                    <div class="col-md-9">
                                        <select name="sanksi" id="sanksi" class="form-control">
                                            <option value="">Pilih sanksi pelanggaran..</option>
                                            @foreach ($sanksi as $ss)
                                                <option value="{{ $ss['id'] }}">
                                                    {{ $ss['sanksi'] . ' - ' . $ss['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Reset Point</label>
                                    <div class="col-md-9">
                                        <label class="switch">
                                            <input type="checkbox" class="reset_point" id="reset_point"
                                                name="reset_point">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Sudah ditangani?</label>
                                    <div class="col-md-9">
                                        <label class="switch">
                                            <input type="checkbox" class="tangani" id="tangani" name="ditangani">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kesiswaan_histori-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-sync-alt"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>'
                                );
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_pelanggaran_siswa-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Pelanggaran");
                        $('#id_pelanggaran').val(data.id);
                        $('#nama_siswa').html(data.nama.toUpperCase());
                        $('#nisn').html(data.nis + " / " + data.nisn);
                        $('#jurusan').html(data.jurusan);
                        $('#rombel').html(data.rombel);
                        $('#wali_kelas').html(data.wali_kelas);
                        $('#guru').html(data.guru);
                        $('#tgl_pelanggaran').html(convertDate(data.tgl_pelanggaran));
                        $('#pelanggaran').html(data.pelanggaran);
                        $('#point').html(data.point);
                        $('#sanksi').val(data.id_sanksi_pelanggaran).trigger('change');
                        if (data.penanganan === 1) {
                            $("#tangani").prop("checked", true);
                        } else {
                            $("#tangani").prop("checked", false);
                        }
                        $("#reset_point").prop("checked", false);
                        $('#modalPelanggaran').modal('show');
                    }
                });
            });

            $('#formPelanggaran').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pelanggaran_siswa-update_sanksi') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalSanksi').modal('hide');
                            $('#formSanksi').trigger("reset");
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });


        })

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>


@endsection
