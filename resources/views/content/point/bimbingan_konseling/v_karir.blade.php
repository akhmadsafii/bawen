@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-6 col-12">
                <div class="pull-right">
                    <button class="btn btn-outline-info mt-1" id="addData"><i class="fas fa-plus-circle"></i>
                        Tambah</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped table-bordered widget-status-table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>NISN</th>
                            <th>Rombel</th>
                            <th>Tahun Ajaran</th>
                            <th>Universitas</th>
                            <th>Status Diterima</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="data_karir">
                        @if (empty($karir))
                            <tr>
                                <td class="text-center" colspan="8">Data saat ini tidak tersedia</td>
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($karir as $kr)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $kr['nama'] }}</td>
                                    <td>{{ $kr['nisn'] }}</td>
                                    <td>{{ $kr['rombel'] }}</td>
                                    <td>{{ $kr['tahun_ajaran'] }}</td>
                                    <td>{{ $kr['universitas'] }}</td>
                                    @php
                                        $badge_color = '';
                                        if ($kr['diterima'] == 'ya') {
                                            $badge_color = 'badge-success';
                                        } elseif ($kr['diterima'] == 'tidak') {
                                            $badge_color = 'badge-danger';
                                        } else {
                                            $badge_color = 'badge-info';
                                        }
                                    @endphp
                                    <td><a href="javascript:void(0)" data-id="{{ $kr['id'] }}"
                                            data-value="{{ $kr['diterima'] }}" class="edit_diterima"><span
                                                class="badge {{ $badge_color }} text-inverse">{{ $kr['diterima'] }}</span></a>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="edit" data-id="{{ $kr['id'] }}">
                                            <i class="material-icons list-icon md-18 text-info">edit</i>
                                        </a>
                                        <a href="javascript:void(0)" class="delete" data-id="{{ $kr['id'] }}">
                                            <i class="material-icons list-icon md-18 text-danger">delete</i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalKarir" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="formSearchSiswa" method="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Jurusan</label>
                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                        <option value="">Pilih Jurusan</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">{{ $jr['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Kelas</label>
                                    <select name="id_kelas" id="id_kelas" disabled class="form-control">
                                        <option value="">Pilih Kelas..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Rombel</label>
                                    <select name="id_rombel" id="id_rombel" disabled class="form-control">
                                        <option value="">Pilih Rombel..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">&nbsp;</label>
                                    <input type="submit" class="btn btn-success form-control" value="Proses cari">
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="formKarir" method="post">
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>NISN</th>
                                        <th>Rombel</th>
                                        <th>Jurusan</th>
                                        <th style="width: 150px" class="text-center">
                                            <input class="form-check-input ml-0" type="checkbox" id="checkAll">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Pilih Semua
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="data-siswa">
                                    <tr>
                                        <td colspan="7" class="text-center">Harap pilih rombel dahulu</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title"><i class="fas fa-trash-restore"></i> Data Trash</h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="height: 400px; overflow-y: auto;">
                        <p></p>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Keterangan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_trash">
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark btn-rounded ripple text-left" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEditTerima" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit Status Terima</h5>
                </div>
                <form id="formEditTerima" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="status_id_karir">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status Diterima</label>
                                    <div class="col-sm-12">
                                        <select name="diterima" id="status_terima" class="form-control">
                                            <option value="ya">Iya</option>
                                            <option value="tidak">Tidak</option>
                                            <option value="proses">Proses</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"
                            id="btnStatusTerima">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit Karir</h5>
                </div>
                <form id="formEdit" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="edit_id_karir">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Rombel</label>
                                    <div class="col-sm-12">
                                        <select name="rombel" id="rombel" class="form-control">
                                            @foreach ($rombel as $rb)
                                                <option value="{{ $rb['id'] }}">{{ $rb['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Siswa</label>
                                    <div class="col-sm-12">
                                        <select name="id_siswa" id="edit_siswa" class="form-control" disabled>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="tahun_ajaran" id="edit_tahun" class="form-control">
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['tahun_ajaran'] }}">{{ $th['tahun_ajaran'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Universitas</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="universitas" id="edit_universitas" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="keterangan" id="edit_keterangan" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnEdit">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>





    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // const actualBtn = document.getElementById('actual-btn');
            // const fileChosen = document.getElementById('file-chosen');
            // actualBtn.addEventListener('change', function() {
            //     fileChosen.textContent = this.files[0].name
            // });

            $('#addData').click(function() {
                $('#formData').trigger("reset");
                $('#modelHeading').html("Tambah Data");
                $('#modalKarir').modal('show');
                $('#action').val('Add');
            });

            // $('#import').click(function() {
            //     $('#importModal').modal('show');
            //     $('#file-chosen').html('No file choosen');
            //     $('#HeadingImport').html('Import Jenis Beasiswa');
            // });

            $('select[name="id_jurusan"]').on('change', function() {
                let id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Load Kelas</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                            $("#id_rombel").attr("disabled", true);
                            $("#id_rombel").html('<option value="">Pilih Rombel..</option>');
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Load Rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih Rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#rombel').on('change', function() {
                let id_rombel = $(this).val();
                if (id_rombel) {
                    $('#edit_siswa').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('rombel-kelas_siswa') }}",
                        type: "POST",
                        data: {
                            id_rombel
                        },
                        beforeSend: function() {
                            $('#edit_siswa').html(
                                '<option value="">Memproses data Siswa...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Siswa--</option>';
                            // data = JSON.parse(data);
                            data.forEach(function(val) {
                                // console.log(val);
                                s += '<option value="' + val.id_siswa + '">' + val
                                    .nama + ' NISN ' + val.nisn +
                                    '</option>';
                            })
                            $('#edit_siswa').removeAttr('disabled')
                            $('#edit_siswa').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('point_karir-data_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formKarir').on('submit', function(event) {
                event.preventDefault();
                $('#btnSimpanBeasiswa').html('Proses Menyimpan...');
                $("#btnSimpanBeasiswa").attr("disabled", true);
                $.ajax({
                    url: "{{ route('point_karir-simpan') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#modalKarir').modal('hide');
                            $('#formKarir').trigger("reset");
                            $('#formSearchSiswa').trigger("reset");
                        }
                        $('#data_karir').html(data.html);
                        noti(data.icon, data.message);
                        $('#btnSimpanBeasiswa').html('Simpan');
                        $("#btnSimpanBeasiswa").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('point_karir-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#modelHeading').html("Edit Data");
                        $('#edit_id_karir').val(data.id);
                        $('#rombel').val(data.id_rombel).trigger('change');
                        $('#edit_tahun').val(data.tahun_ajaran).trigger('change');
                        $('#edit_keterangan').val(data.keterangan);
                        $('#edit_universitas').val(data.universitas);
                        load_kelas_siswa(data.id_rombel, data.id_kelas_siswa);
                        $('#modalEdit').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            // $(document).on('click', '#btnTrash', function() {
            //     $.ajax({
            //         type: 'GET',
            //         url: "{{ route('point_jenis_beasiswa-data_trash') }}",
            //         beforeSend: function() {
            //             $('#data_trash').html(
            //                 '<tr><td colspan="5" class="text-center">sedang memproses data trash...</td></tr>'
            //             );
            //         },
            //         success: function(data) {
            //             $('#data_trash').html(data);
            //             $('#modalTrash').modal('show');
            //         }
            //     });
            // });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#btnEdit").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnEdit").attr("disabled", true);
                $.ajax({
                    url: "{{ route('point_karir-update') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalEdit').modal('hide');
                            $('#formEdit').trigger("reset");
                        }
                        $('#data_karir').html(data.html);
                        noti(data.icon, data.message);
                        $('#btnEdit').html('Simpan');
                        $("#btnEdit").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_karir-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_karir').html(data.html);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.edit_diterima', function() {
                let id = $(this).data('id');
                let terima = $(this).data('value');
                $('#status_id_karir').val(id);
                $('#status_terima').val(terima).trigger('change');
                $('#modalEditTerima').modal('show');
            });

            $('#formEditTerima').on('submit', function(event) {
                event.preventDefault();
                $("#btnStatusTerima").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnStatusTerima").attr("disabled", true);
                $.ajax({
                    url: "{{ route('point_karir-update_status') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalEditTerima').modal('hide');
                            $('#formEditTerima').trigger("reset");
                        }
                        $('#data_karir').html(data.html);
                        noti(data.icon, data.message);
                        $('#btnStatusTerima').html('Simpan');
                        $("#btnStatusTerima").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            // $(document).on('click', '.restore', function() {
            //     let id = $(this).data('id');
            //     let loader = $(this);
            //     let konfirmasi = confirm("Apa kamu yakin ingin mengembalikan data ini?");
            //     if (konfirmasi == true) {
            //         $.ajax({
            //             url: "{{ route('point_jenis_beasiswa-restore') }}",
            //             type: "POST",
            //             data: {
            //                 id
            //             },
            //             beforeSend: function() {
            //                 $(loader).html(
            //                     '<i class="fa fa-spin fa-spinner"></i>');
            //             },
            //             success: function(data) {
            //                 $('#data_trash').html(data.trash);
            //                 $('#data_jenis').html(data.jenis);
            //                 swa(data.status + "!", data.message, data.icon);
            //             }
            //         })
            //     } else {
            //         swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
            //     }
            // });

            // $(document).on('click', '.hardDelete ', function() {
            //     let id = $(this).data('id');
            //     let loader = $(this);
            //     swal({
            //         title: "Apa kamu yakin?",
            //         text: "ingin menghapus data ini!",
            //         type: "warning",
            //         showCancelButton: true,
            //         confirmButtonColor: '#3085d6',
            //         cancelButtonColor: '#d33',
            //         confirmButtonText: 'Yes, delete it!',
            //         cancelButtonText: 'No, cancel!',
            //         confirmButtonClass: 'btn btn-success',
            //         cancelButtonClass: 'btn btn-danger',
            //         buttonsStyling: false
            //     }).then(function() {
            //         $.ajax({
            //             url: "{{ route('point_jenis_beasiswa-hard_delete') }}",
            //             type: "POST",
            //             data: {
            //                 id
            //             },
            //             beforeSend: function() {
            //                 $(loader).html(
            //                     '<i class="fa fa-spin fa-spinner"></i>');
            //             },
            //             success: function(data) {
            //                 $('#data_trash').html(data.trash);
            //                 swa(data.status + "!", data.message, data.icon);
            //             }
            //         })
            //     }, function(dismiss) {
            //         if (dismiss === 'cancel') {
            //             swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
            //         }
            //     })
            // });

            // $('body').on('submit', '#formImport', function(e) {
            //     e.preventDefault();
            //     let actionType = $('#btn-save').val();
            //     $('#importBtn').html('Sending..');
            //     var formDatas = new FormData(document.getElementById("formImport"));
            //     // let formData = new FormData($('#formImport'));
            //     $.ajax({
            //         type: "POST",
            //         url: "{{ route('point_jenis_beasiswa-import') }}",
            //         data: formDatas,
            //         cache: false,
            //         contentType: false,
            //         processData: false,
            //         success: (data) => {
            //             console.log(data);
            //             if (data.status == 'berhasil') {
            //                 $('#formImport').trigger("reset");
            //                 $('#importModal').modal('hide');
            //             }
            //             $('#importBtn').html('Simpan');
            //             $('#data_jenis').html(data.jenis);
            //             noti(data.icon, data.message);
            //         },
            //         error: function(data) {
            //             console.log('Error:', data);
            //             $('#importBtn').html('Simpan');
            //         }
            //     });
            // });
        });

        function load_kelas_siswa(id_rombel, id_kelas_siswa) {
            $.ajax({
                url: "{{ route('load_kelas_siswa-by_rombel') }}",
                type: "POST",
                data: {
                    id_rombel,
                    id_kelas_siswa
                },
                beforeSend: function() {
                    $('#edit_siswa').append('<option value="">memproses data siswa...</option>');
                },
                success: function(fb) {
                    $("#edit_siswa").attr("disabled", false);
                    $('#edit_siswa').html('');
                    $('#edit_siswa').html(fb);
                }
            });
            return false;
        }
    </script>


@endsection
