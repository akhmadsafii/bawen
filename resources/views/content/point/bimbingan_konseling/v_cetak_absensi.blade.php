<!DOCTYPE html>
<html>

<head>
    <title>Cetak Raport</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            padding: 30px 10px
        }

        table .absensi {
            border-spacing: 0px;
            border-collapse: separate;
            border: 1px solid black;
        }
        table .absensi th, table .absensi td {
            border: 1px solid black;
        }



    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <table style="width: 100%">
        <tr>
            <td colspan="2" style="text-align: center">
                <h3>Daftar Absensi Siswa</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width: 100%">
                    <tr>
                        <td>Nama Siswa</td>
                        <td>:</td>
                        <td>{{ $siswa['nama'] != null ? $siswa['nama'] : '-' }}</td>
                        <td></td>
                        <td>NIS</td>
                        <td>:</td>
                        <td>{{ $siswa['nis'] != null ? $siswa['nis'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>NISN</td>
                        <td>:</td>
                        <td>{{ $siswa['nisn'] != null ? $siswa['nisn'] : '-' }}</td>
                        <td></td>
                        <td>Rombel</td>
                        <td>:</td>
                        <td>{{ $siswa['rombel'] }}
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>:</td>
                        <td>{{ $siswa['kelas'] }}</td>
                        <td></td>
                        <td>Jurusan</td>
                        <td>:</td>
                        <td>{{ $siswa['jurusan'] != null ? $siswa['jurusan'] : '-' }}</td>
                    </tr>
                </table>

            </td>
        </tr>

        <tr>
            <td colspan="2">
                <table class="absensi" style="width: 100%;  border-collapse: collapse;">
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama Mapel</th>
                        <th>Status Absensi</th>
                        <th>Pertemuan</th>
                        <th>Masuk</th>
                    </tr>
                    @if (!empty($absensi))
                        @foreach ($absensi as $abs)
                            <tr>
                                <td style="text-align: center">
                                    {{ (new \App\Helpers\Help())->getTanggal($abs['created_at']) }}</td>
                                <td style="text-align: center">{{ $abs['mapel'] }}</td>
                                <td style="text-align: center">{{ $abs['absensi'] }}</td>
                                <td style="text-align: center">{{ $abs['pertemuan'] }}</td>
                                <td style="text-align: center">{{ $abs['masuk'] != null ? $abs['masuk'] : '-' }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" style="text-align: center">Siswa belum pernah mengikuti pelajaran</td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
