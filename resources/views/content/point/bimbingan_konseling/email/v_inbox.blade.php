@extends('content.point.bimbingan_konseling.email.v_main_mail')
@section('content_pesan')
    <div class="widget-heading clearfix m-0">
        <h5>Pesan Masuk</h5>
    </div>
    <div class="table-responsive" style="padding: 15px;">
        <table class="table table-striped" id="data-tabel" style="width: 100%">
            <thead>
                <tr>
                    <th><input type="checkbox" class='checkall' id='checkall'></th>
                    <th>Nama</th>
                    <th></th>
                    <th>Subject</th>
                    <th>Pesan</th>
                    <th>Waktu</th>
                </tr>
            </thead>
        </table>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('click', '#data_trash', function() {
                var deleteids_arr = [];
                $("input:checkbox[class=delete_check]:checked").each(function() {
                    deleteids_arr.push($(this).val());
                });

                if (deleteids_arr.length > 0) {
                    var confirmdelete = confirm("Apa kamu yakin ingin memindahkan data ke trash?");
                    var aksi = "soft_del";
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('point_pesan_email-delete') }}",
                            type: 'post',
                            data: {
                                aksi: aksi,
                                deleteids_arr: deleteids_arr
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#saveBtn').html('Simpan');
                                    var oTable = $('#data-tabel').dataTable();
                                    oTable.fnDraw(false);
                                    noti(data.success, data.message);
                                    $("#checkall").removeAttr('checked');
                                } else {
                                    noti(data.success, data.message);
                                    $('#saveBtn').html('Simpan');
                                }
                            }
                        });
                    }
                }
            });
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-12"p>>',
                buttons: [{
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-trash"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                ],
                pagingType: "numbers",
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('point_pesan_email-datatable') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.output = 'inbox';
                    }
                },
                "fnCreatedRow": function(nRow, aData, iDataIndex) {
                    $(nRow).attr('id', aData[0]);
                },

                columns: [{
                        data: 'select',
                        name: 'select',
                        orderable: false,
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'penerima',
                        name: 'penerima'
                    },
                    {
                        data: 'subject',
                        name: 'subject'
                    },
                    {
                        data: 'pesan',
                        name: 'pesan'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },
                ]
            });
        });
    </script>
@endsection
