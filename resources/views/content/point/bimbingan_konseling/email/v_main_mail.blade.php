@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {

        .btn-primary {
            border-color: #03a9f3;
        }

        .btn-primary:active,
        .btn-primary.active,
        .show>.btn-primary.dropdown-toggle {
            background-color: #03a9f3;
            color: #fff !important;
            background-image: none;
            border-color: #03a9f3;
            -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
            box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        }

        #radioBtn .notActive {
            color: #3276b1;
            background-color: #fff;
        }

        .ripple i.list-icon.material-icons,
        i.material-icons.list-icon.float-left.mr-2 {
            color: #5b6673 !important;
        }

        @media (min-width: 1200px) {
            .modal-xl {
                max-width: 1140px;
            }
        }

        button.dt-button {
            background: #fff !important;
            color: #584545 !important;
        }

        thead {
            display: none !important;
        }

    </style>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <div class="row no-gutters" id="loadSend">
        <div class="col-md-12 widget-holder widget-no-padding">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row no-gutters">
                        <div class="col-lg-2 col-md-4 d-none d-md-block mail-sidebar">
                            @include('content.point.bimbingan_konseling.email.v_menu_mail')
                        </div>
                        <div class="col-lg-10 col-md-8 col-12 mail-inbox">
                            @yield('content_pesan')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        $(function() {
            $('.createEmail').click(function() {
                $('.selectpicker').selectpicker('refresh');
                $('#mail_penerima').val('');
                $('#create_message').modal('show');
            });

            $('body').on('submit', '#form_send_message', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: "{{ route('point_pesan_email-save') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            location.reload(true);
                            $('#create_message').modal('hide');
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('<i class="fa fa-envelope"></i> Send Message');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });



        function create_email() {
            $('#create_message').modal('show');
            $('input[name=email_penerima]').val(email_dari);
            $('.selectpicker').selectpicker('refresh');
            $('#detail_message').modal('hide');

        }

        tinymce.init({
            selector: 'textarea#default',
            height: 300,
            menubar: false,
            forced_root_block: "",
            force_br_newlines: true,
            force_p_newlines: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>


@endsection
