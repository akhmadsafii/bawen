@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h5 class="box-title">{{ session('title') }}</h5>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-info addKategori"><i class="fas fa-plus-circle"></i> Tambah
                                        Kategori</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive mt-2">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">No</th>
                                    <th>Kategori</th>
                                    <th>Status</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_kategori">
                                @if (empty($kategori))
                                    <tr>
                                        <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($kategori as $kt)
                                        <tr>
                                            <td class="vertical-middle text-center">{{ $no++ }}</td>
                                            <td>{{ ucwords($kt['nama']) }}</td>
                                            <td class="vertical-middle">
                                                <label class="switch">
                                                    <input type="checkbox" {{ $kt['status'] == 1 ? 'checked' : '' }}
                                                        class="kategori_check" data-id="{{ $kt['id'] }}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td class="vertical-middle text-center">
                                                <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                                                    data-id="{{ $kt['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-danger btn-sm delete"
                                                    data-id="{{ $kt['id'] }}"><i class="fas fa-trash"></i></a>
                                                <a href="javascript:void(0)" data-toggle="collapse"
                                                    data-target="#pelanggaran{{ $kt['id'] }}"
                                                    class="btn btn-purple btn-sm" data-id="{{ $kt['id'] }}"><i
                                                        class="fas fa-angle-double-right"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="hiddenRow">
                                                <div class="accordian-body collapse" id="pelanggaran{{ $kt['id'] }}">
                                                    <button class="btn btn-info btn-sm my-3 pull-right"
                                                        onclick="tambahPelanggaran({{ $kt['id'] }})"><i
                                                            class="fas fa-plus-circle"></i>
                                                        Tambah Pelanggaran</button>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama</th>
                                                                <th>Bobot Point</th>
                                                                <th>Status</th>
                                                                <th class="text-center">Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="dataPelanggaran{{ $kt['id'] }}">
                                                            @if (!empty($kt['pelanggaran']))
                                                                @php
                                                                    $nomer = 1;
                                                                @endphp
                                                                @foreach ($kt['pelanggaran'] as $plg)
                                                                    <tr>
                                                                        <td>{{ $nomer++ }}</td>
                                                                        <td>{{ $plg['nama'] }}</td>
                                                                        <td>{{ $plg['point'] }}</td>
                                                                        <td>
                                                                            <label class="switch">
                                                                                <input type="checkbox"
                                                                                    {{ $plg['status'] == 1 ? 'checked' : '' }}
                                                                                    class="pelanggaran_check"
                                                                                    data-id="{{ $plg['id'] }}">
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-info btn-sm ePlg"
                                                                                data-id="{{ $plg['id'] }}"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-danger btn-sm dPlg"
                                                                                data-id="{{ $plg['id'] }}"
                                                                                data-kategori="{{ $plg['id_kategori_pelanggaran'] }}"><i
                                                                                    class="fas fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="5" class="text-center">Data saat ini
                                                                        tidak tersedia</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalKategori" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formKategori">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_kategori">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Kategori</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" required>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalPelanggaran" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titlePelanggaran"></h5>
                </div>
                <form id="formPelanggaran">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_pelanggaran">
                                <input type="hidden" name="id_kategori" id="id_kat_pelanggaran">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l2">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group"><span class="input-group-addon"><i
                                                    class="fas fa-question-circle"></i>
                                            </span>
                                            <input class="form-control" id="nama_pelanggaran" name="nama"
                                                placeholder="Pelanggaran" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l2">Bobot</label>
                                    <div class="col-md-9">
                                        <div class="input-group"><span class="input-group-addon"><i
                                                    class="fas fa-weight-hanging"></i>
                                            </span>
                                            <input class="form-control" id="bobot" name="bobot" placeholder="Bobot"
                                                type="number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionPelanggaran" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"
                            id="btnPelanggaran">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.kategori_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_kategori_pelanggaran-update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $('.addKategori').click(function() {
                $('#formKategori').trigger("reset");
                $('#modelHeading').html("Tambah Kategori Pelanggaran");
                $('.fomAddBaris').html('');
                $('.tambahBaris').show();
                $('#modalKategori').modal('show');
                $('#action').val('Add');
            });

            $('#formKategori').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                let action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kesiswaan_kategori_pelanggaran-simpan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kesiswaan_kategori_pelanggaran-update') }}";
                    method_url = "PUT";
                }
                let pelanggaran = $('#id_kat_pelanggaran').val();

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#modalKategori').modal('hide');
                            $('#formkategori').trigger("reset");
                            $('#data_kategori').html(data.kategori);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_kategori_pelanggaran-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Kategori Pelanggaran");
                        $('#id_kategori').val(data.id);
                        $('#nama').val(data.nama);
                        $('#action').val('Edit');
                        $('#modalKategori').modal('show');
                    }
                });
            });

            $(document).on('click', '.ePlg', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('point_pelanggaran-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePelanggaran').html("Edit Kategori Pelanggaran");
                        $('#id_pelanggaran').val(data.id);
                        $('#id_kat_pelanggaran').val(data.id_kategori_pelanggaran);
                        $('#nama_pelanggaran').val(data.nama);
                        $('#bobot').val(data.point);
                        $('#actionPelanggaran').val('Edit');
                        $('#modalPelanggaran').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kesiswaan_kategori_pelanggaran-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_kategori').html(data.kategori);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.dPlg', function() {
                let id = $(this).data('id');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_pelanggaran-delete') }}",
                        type: "POST",
                        data: {
                            id,
                            id_kategori
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataPelanggaran' + id_kategori).html(data
                                    .pelanggaran);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('#formPelanggaran').on('submit', function(event) {
                $('#btnPelanggaran').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#actionPelanggaran').val() == 'Add') {
                    action_url = "{{ route('point_pelanggaran-simpan') }}";
                    method_url = "POST";
                }

                if ($('#actionPelanggaran').val() == 'Edit') {
                    action_url = "{{ route('point_pelanggaran-update') }}";
                    method_url = "PUT";
                }
                let kategori = $('#id_kat_pelanggaran').val();
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#modalPelanggaran').modal('hide');
                            $('#formPelanggaran').trigger("reset");
                            $('#dataPelanggaran' + kategori).html(data.pelanggaran);
                        }
                        noti(data.icon, data.message);
                        $('#btnPelanggaran').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPelanggaran').html('Simpan');
                    }
                });
            });
        });

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama Kategori</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        function tambahPelanggaran(id) {
            $('#formPelanggaran').trigger("reset");
            $('#id_kat_pelanggaran').val(id)
            $('#titlePelanggaran').html("Tambah Pelanggaran");
            $('#modalPelanggaran').modal('show');
            $('#actionPelanggaran').val('Add');
        }
    </script>
@endsection
