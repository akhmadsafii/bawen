@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-lg-12 widget-bg widget-holder">
            <div class="card" style="margin-bottom: 12px;">
                <div class="card-header">
                    <h2 class="box-title">{{ Session::get('title') }}</h2>
                </div>
                <div class="card-body">
                    <form class="form-inline" id="search-form">
                        <div class="form-group mb-2">
                            <select name="rombel" id="rombel" class="form-control">
                                <option value="">Pilih Rombel..</option>
                                <option value="all">All</option>
                                @foreach ($jurusan as $jr)
                                    <optgroup label="{{ $jr['nama'] }}">
                                        @foreach ($jr['kelas'] as $kelas)
                                    <optgroup label="{{ $kelas['nama_romawi'] }}">
                                        @foreach ($kelas['rombel'] as $rombel)
                                            <option value="{{ $rombel['id'] }}">
                                                {{ $rombel['nama'] }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <select name="id_siswa" id="id_siswa" class="form-control" disabled>
                                <option value="">Pilih Siswa..</option>
                            </select>
                        </div>
                        <button class="btn btn-info mb-2" type="submit">Submit</button>
                    </form>
                </div>

            </div>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Profil Siswa</th>
                                <th>Kelas</th>
                                <th>Point</th>
                                <th>Jumlah Pelanggaran</th>
                                <th>Telepon Orang Tua</th>
                                <th>Detail Pelanggaran</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleSiswa">Detail Pelanggaran Siswa</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped widget-status-table" id="data-detail">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal Pelanggaran</th>
                                    <th>Kategori</th>
                                    <th>Pelanggaran</th>
                                    <th>Point</th>
                                    <th>Penanganan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            fill_datatable();
            fill_detail(0);

            function fill_datatable(siswa = '', rombel = '') {
                var table = $('#data-tabel').DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-clipboard"></i>',
                            exportOptions: {
                                columns: [0, ':visible']
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fas fa-file-excel"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },

                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fas fa-file-pdf"></i>',
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fas fa-sync"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: {
                        url: "{{ route('kesiswaan_pelanggaran_siswa-datatable') }}",
                        data: {
                            id_kelas_siswa: siswa,
                            id_rombel: rombel
                        }
                    },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            className: 'vertical-middle'
                        },
                        {
                            data: 'siswa',
                            name: 'siswa',
                            className: 'vertical-middle'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel',
                            className: 'vertical-middle'
                        },
                        {
                            data: 'total_point',
                            name: 'total_point',
                            className: 'vertical-middle text-center'
                        },
                        {
                            data: 'jumlah_pelanggaran',
                            name: 'jumlah_pelanggaran',
                            className: 'vertical-middle text-center'
                        },
                        {
                            data: 'whatsapp',
                            name: 'whatsapp',
                            className: 'text-center vertical-middle'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            className: 'vertical-middle text-center',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });
            }



            $('#search-form').on('submit', function(e) {
                e.preventDefault();
                var siswa = $('#id_siswa').val();
                var rombel = $('#rombel').val();

                if (rombel) {
                    $('#data-tabel').DataTable().destroy();
                    fill_datatable(siswa, rombel);
                } else {
                    alert('Harap pilih rombel terlebih dulu');
                }
            });

            $('select[name="rombel"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('rombel-kelas_siswa') }}",
                        type: "POST",
                        data: {
                            id_rombel: id
                        },
                        success: function(data) {
                            console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="id_siswa"]').html(
                                    '<option value="">-- Tidak ada siswa ditemukan --</option>'
                                );
                                $('select[name="id_siswa"]').prop('disabled', true);
                            } else {
                                var s = '<option value="all">Semua Siswa..</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama.toUpperCase() +
                                        ' - NISN ' + row.nisn + '</option>';

                                })
                                $('select[name="id_siswa"]').prop('disabled', false);
                            }
                            $('select[name="id_siswa"]').html(s)
                        }
                    });
                }
            })

            $(document).on('click', '.detail', function() {
                var id = $(this).data('id');
                var nama = $(this).data('nama');
                let loader = $(this);
                if (id) {
                    $('#data-detail').DataTable().destroy();
                    fill_detail(id);
                    $('#titleSiswa').html('Detail Pelanggaran ' + nama.toUpperCase());
                    $('#modalDetail').modal('show');
                }
            });

        });

        function fill_detail(kelas_siswa = '') {
            var table = $('#data-detail').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('pelanggaran_siswa-detail_datatable') }}",
                    data: {
                        kelas_siswa
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'kategori',
                        name: 'kategori',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'pelanggaran',
                        name: 'pelanggaran',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'point',
                        name: 'point',
                        className: 'vertical-middle'
                    },
                    {
                        data: 'penanganan',
                        name: 'penanganan',
                        className: 'vertical-middle'
                    },
                ]
            });
        }
    </script>
@endsection
