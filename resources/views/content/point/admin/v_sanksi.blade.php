@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="float-right">
                                @if (session('role') != 'supervisor')
                                    <button class="btn btn-outline-info mt-1" id="addData"><i class="fas fa-plus-circle"></i>
                                        Tambah</button>
                                @endif
                                {{-- <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                                    Import</button>
                                <button id="btnTrash" class="btn btn-outline-danger mt-1"><i
                                        class="fas fa-trash-restore"></i>
                                    Trash</button> --}}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center" rowspan="2">#</th>
                                        <th class="text-center" rowspan="2">Kriteria Pelanggaran</th>
                                        <th class="text-center" colspan="2">Bobot</th>
                                        <th class="text-center" rowspan="2">Sanksi</th>
                                        @if (session('role') != 'supervisor')
                                            <th class="text-center" rowspan="2">Aksi</th>
                                        @endif
                                    </tr>
                                    <tr class="bg-info">
                                        <th class="text-center">Mulai</th>
                                        <th class="text-center">Sampai</th>
                                    </tr>
                                </thead>
                                <tbody id="data_sanksi">
                                    @if (!empty($sanksi))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($sanksi as $ss)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $ss['nama'] }}</td>
                                                <td>{{ $ss['bobot_dari'] }}</td>
                                                <td>{{ $ss['bobot_sampai'] }}</td>
                                                <td>{{ $ss['sanksi'] }}</td>
                                                @if (session('role') != 'supervisor')
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" data-id="{{ $ss['id'] }}"
                                                            class="edit"><i
                                                                class="material-icons list-icon md-18 text-info">edit</i></a>
                                                        <a href="javascript:void(0)" data-id="{{ $ss['id'] }}"
                                                            class="delete"><i
                                                                class="material-icons list-icon md-18 text-danger">delete</i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">Data saat ini belum tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_sanksi">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kriteria Pelanggaran</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label" for="l1">Bobot</label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="bobot_dari" id="bobot_dari"
                                                    placeholder="bobot dari">
                                                <span class="input-group-addon bg-info text-inverse">sampai</span>
                                                <input type="text" class="form-control" name="bobot_sampai"
                                                    id="bobot_sampai" placeholder="bobot sampai">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Sanksi & Pembinaan</label>
                                    <div class="col-sm-12">
                                        <textarea name="sanksi" id="sanksi" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            $('#addData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Sanksi Pelanggaran");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kesiswaan_sanksi-simpan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kesiswaan_sanksi-update') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#ajaxModel').modal('hide');
                            $('#CustomerForm').trigger("reset");
                            $('#data_sanksi').html(data.sanksi);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_sanksi-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#modelHeading').html("Edit Sanksi Pelanggaran");
                        $('#id_sanksi').val(data.id);
                        $('#bobot_dari').val(data.bobot_dari);
                        $('#bobot_sampai').val(data.bobot_sampai);
                        $('#sanksi').val(data.sanksi);
                        $('#nama').val(data.nama)
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kesiswaan_sanksi-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_sanksi').html(data.sanksi);
                            }
                            $(loader).html(
                                '<i class="material-icons list-icon md-18 text-danger">delete</i>'
                            );
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });



        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('program/point/sanksi/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('program/point/sanksi/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
