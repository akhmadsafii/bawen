@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        h6.media-heading.mr-b-5.text-uppercase {
            color: #fff !important;
        }

        span.user-type.fs-12.roles.text-info {
            color: #fff !important;
        }

        main.main-wrapper.clearfix {
            padding-top: 0px !important;
        }

        .clock {
            width: 200px;
            height: 200px;
            position: relative;
            /* padding: 2rem; */
            border: 7px solid #1f2578;
            box-shadow: -4px -4px 10px rgba(67, 67, 67, 0.5),
                inset 4px 4px 10px rgba(0, 0, 0, 0.5),
                inset -4px -4px 10px rgba(67, 67, 67, 0.5),
                4px 4px 10px rgba(0, 0, 0, 0.3);
            border-radius: 50%;
            margin: auto;

        }

        .outer-clock-face {
            position: relative;
            background: #1f2578;
            overflow: hidden;
            width: 100%;
            height: 100%;
            border-radius: 100%;
        }

        .outer-clock-face::after {
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .outer-clock-face::after,
        .outer-clock-face::before,
        .outer-clock-face .marking {
            content: '';
            position: absolute;
            width: 5px;
            height: 100%;
            background: #1df52f;
            z-index: 0;
            left: 49%;
        }

        .outer-clock-face .marking {
            background: #bdbdcb;
            width: 3px;
        }

        .outer-clock-face .marking.marking-one {
            -webkit-transform: rotate(30deg);
            -moz-transform: rotate(30deg);
            transform: rotate(30deg)
        }

        .outer-clock-face .marking.marking-two {
            -webkit-transform: rotate(60deg);
            -moz-transform: rotate(60deg);
            transform: rotate(60deg)
        }

        .outer-clock-face .marking.marking-three {
            -webkit-transform: rotate(120deg);
            -moz-transform: rotate(120deg);
            transform: rotate(120deg)
        }

        .outer-clock-face .marking.marking-four {
            -webkit-transform: rotate(150deg);
            -moz-transform: rotate(150deg);
            transform: rotate(150deg)
        }

        .inner-clock-face {
            position: absolute;
            top: 10%;
            left: 10%;
            width: 80%;
            height: 80%;
            background: #1f2578;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            z-index: 1;
        }

        .inner-clock-face::before {
            content: '';
            position: absolute;
            top: 50%;
            left: 50%;
            width: 16px;
            height: 16px;
            border-radius: 18px;
            margin-left: -9px;
            margin-top: -6px;
            background: #4d4b63;
            z-index: 11;
        }

        .hand {
            width: 50%;
            right: 50%;
            height: 6px;
            background: #61afff;
            position: absolute;
            top: 50%;
            border-radius: 6px;
            transform-origin: 100%;
            transform: rotate(90deg);
            transition-timing-function: cubic-bezier(0.1, 2.7, 0.58, 1);
        }

        .hand.hour-hand {
            width: 30%;
            z-index: 3;
        }

        .hand.min-hand {
            height: 3px;
            z-index: 10;
            width: 40%;
        }

        .hand.second-hand {
            background: #ee791a;
            width: 45%;
            height: 2px;
        }

    </style>
    <div class="row">
        <div class="col-xl-12 col-xxl-12">
            @php
                date_default_timezone_set('Asia/Jakarta');
                $jam = date('H:i');

                if ($jam > '05:30' && $jam < '10:00') {
                    $salam = 'Pagi';
                } elseif ($jam >= '10:00' && $jam < '15:00') {
                    $salam = 'Siang';
                } elseif ($jam < '18:00') {
                    $salam = 'Sore';
                } else {
                    $salam = 'Malam';
                }
            @endphp
            <div class="alert alert-info alert-dismissible fade show">
                <i class="fas fa-smile"></i>
                <strong>Hallo Selamat {{ $salam }} {{ ucwords($profile['nama']) }}</strong> Tidak ada libur hari
                ini ! semangat belajarnya dan
                jangan lupa absen ya! <button type="button" class="close h-100" data-dismiss="alert"
                    aria-label="Close"><span><i class="fas fa-close"></i></span>
                </button>
            </div>
            <div class="row d-flex align-items-center">
                <div class="col-md-5 my-2">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h5 class="card-title">Hari Ini</h5>
                            </center>
                            <div class="clock">
                                <div class="outer-clock-face">
                                    <div class="marking marking-one"></div>
                                    <div class="marking marking-two"></div>
                                    <div class="marking marking-three"></div>
                                    <div class="marking marking-four"></div>
                                    <div class="inner-clock-face">
                                        <div class="hand hour-hand"></div>
                                        <div class="hand min-hand"></div>
                                        <div class="hand second-hand"></div>
                                    </div>
                                </div>
                            </div>
                            <center>
                                <h5 id="time" class="box-title mr-b-0"></h5>
                                <p id="datenow">
                                    {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h3>
                                </p>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 my-2">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 my-2">
                            <div class="widget-stat card bg-info">
                                <div class="card-body p-4">
                                    <div class="media">
                                        <span class="mr-3">
                                            <i class="la la-graduation-cap"></i>
                                        </span>
                                        <div class="media-body text-white">
                                            <p class="mb-1">Jumlah Kelas</p>
                                            <h3 class="text-white">4</h3>
                                            <div class="progress mb-2 bg-primary">
                                                <div class="progress-bar progress-animated bg-light" style="width: 76%">
                                                </div>
                                            </div>
                                            <small>dari 3 Jurusan</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 my-2">
                            <div class="widget-stat card bg-purple">
                                <div class="card-body p-4">
                                    <div class="media">
                                        <span class="mr-3">
                                            <i class="la la-graduation-cap"></i>
                                        </span>
                                        <div class="media-body text-white">
                                            <p class="mb-1">Jumlah Kelas</p>
                                            <h3 class="text-white">4</h3>
                                            <div class="progress mb-2 bg-primary">
                                                <div class="progress-bar progress-animated bg-light" style="width: 76%">
                                                </div>
                                            </div>
                                            <small>dari 3 Jurusan</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 my-2">
                            <div class="widget-stat card bg-warning">
                                <div class="card-body p-4">
                                    <div class="media">
                                        <span class="mr-3">
                                            <i class="la la-graduation-cap"></i>
                                        </span>
                                        <div class="media-body text-white">
                                            <p class="mb-1">Jumlah Kelas</p>
                                            <h3 class="text-white">4</h3>
                                            <div class="progress mb-2 bg-primary">
                                                <div class="progress-bar progress-animated bg-light" style="width: 76%">
                                                </div>
                                            </div>
                                            <small>dari 3 Jurusan</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 my-2">
                            <div class="widget-stat card bg-primary">
                                <div class="card-body p-4">
                                    <div class="media">
                                        <span class="mr-3">
                                            <i class="la la-graduation-cap"></i>
                                        </span>
                                        <div class="media-body text-white">
                                            <p class="mb-1">Jumlah Kelas</p>
                                            <h3 class="text-white">4</h3>
                                            <div class="progress mb-2 bg-primary">
                                                <div class="progress-bar progress-animated bg-light" style="width: 76%">
                                                </div>
                                            </div>
                                            <small>dari 3 Jurusan</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 my-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between bg-info">
                                    <h2 class="box-title mt-1 text-white"><strong> Identitas Diri</strong></h2>
                                    <div class="pamit">
                                        @if ($informasi['status'] == true)
                                            <button class="btn btn-danger my-1" id="izinMasuk"><i
                                                    class="fas fa-paste"></i>
                                                IZIN</button>
                                        @else
                                            <button class="btn btn-dark my-1"><i class="fas fa-smile-beam"></i>
                                                LIBUR</button>
                                        @endif
                                        <button class="btn btn-success my-1" id="qr_code"><i class="fas fa-qrcode"></i> QR
                                            CODE</button>

                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="w-100">
                                        <tr>
                                            <td rowspan="8" class="text-center vertical-middle"><img
                                                    src="{{ $profile['file'] }}" alt="" height="200px"></td>
                                            <th>Nama Lengkap</th>
                                            <td>{{ ucwords($profile['nama']) }}</td>
                                        </tr>
                                        <tr>
                                            <th>NIS/NISN</th>
                                            <td>{{ !empty($profile['nis']) ? $profile['nis'] : '-' }} /
                                                {{ !empty($profile['nisn']) ? $profile['nisn'] : '-' }}</td>
                                        </tr>

                                        <tr>
                                            <th>Rombel</th>
                                            <td>{{ $profile['rombel'] }}</td>
                                        </tr>

                                        <tr>
                                            <th>Jurusan</th>
                                            <td>{{ $profile['jurusan'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $profile['email'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Telepon</th>
                                            <td>{{ $profile['telepon'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Wali Kelas</th>
                                            <td>{{ $profile['wali_kelas'] }}</td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="card-footer text-muted d-flex justify-content-between">
                                    <p class="m-0">NISN SISWA : {{ $profile['nisn'] }}</p>
                                    <p class="m-0">Dibuat :
                                        {{ (new \App\Helpers\Help())->getTanggal($profile['created_at']) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 my-3">
                    @if ($informasi['status'] == true)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h2 class="box-title m-0 text-white">Status Absen Hari ini
                                            {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h2>
                                    </div>
                                    <div class="card-body">
                                        <table class="table widget-status-table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Masuk</th>
                                                    <th class="text-center">Keluar</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="data_hadir">
                                                @if (!empty($detail))
                                                    <tr data-toggle="collapse" data-target="#detail_hadir"
                                                        style="cursor: pointer">
                                                        <td class="text-center">
                                                            <b>{{ $detail['jam_masuk'] != null ? (new \App\Helpers\Help())->getTime($detail['jam_masuk']) : '-' }}</b>
                                                        </td>
                                                        <td class="text-center">
                                                            <b>{{ $detail['jam_keluar'] != null ? (new \App\Helpers\Help())->getTime($detail['jam_keluar']) : '-' }}</b>
                                                        </td>
                                                        <td class="text-center"><span
                                                                class="badge badge-{{ $detail['status_kehadiran'] == 'hadir' ? 'success' : 'danger' }} text-inverse">{{ $detail['status_kehadiran'] }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="hiddenRow">
                                                            <div class="accordian-body collapse" id="detail_hadir">
                                                                <table class="table table-bordered">
                                                                    <tr>
                                                                        <th class="vertical-middle">Tanggal</th>
                                                                        <td class="text-center">
                                                                            {{ (new \App\Helpers\Help())->getTanggal($detail['tgl_kehadiran']) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Bukti Masuk</th>
                                                                        <td class="text-center"> <img
                                                                                src="{{ $detail['bukti_masuk'] }}" alt=""
                                                                                height="150"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Bukti Keluar</th>
                                                                        <td class="text-center"> <img
                                                                                src="{{ $detail['bukti_keluar'] }}"
                                                                                alt="" height="150"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Keterangan</th>
                                                                        <td class="text-center">
                                                                            <p>{{ $detail['keterangan'] }}</p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="3" class="text-center">Belum ada Absensi untuk hari
                                                            ini
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 my-3">
                                <div class="card">
                                    <div class="card-header bg-danger">
                                        <h2 class="box-title m-0 text-white">Status Izin Hari ini
                                            {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h2>
                                    </div>
                                    <div class="card-body">
                                        <table class="table widget-status-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"></th>
                                                    <th class="text-center">Dibuat</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="data_izin">
                                                @if (!empty($riwayat))
                                                    <tr>
                                                        <td class="text-center"><span
                                                                class="badge badge-danger text-inverse">Izin</span></td>
                                                        <td class="text-center">
                                                            {{ (new \App\Helpers\Help())->getTanggalLengkap($riwayat['created_at']) }}
                                                        </td>
                                                        <td class="text-center"><span
                                                                class="text-{{ $riwayat['konfirmasi'] == 1 ? 'success' : 'danger' }}"><i
                                                                    class="fas fa-{{ $riwayat['konfirmasi'] == 1 ? 'check-circle' : 'exclamation-circle' }} "></i>
                                                                {{ $riwayat['konfirmasi'] == 1 ? 'Terverifikasi' : ' Belum Diverifikasi' }}</span>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <td class="text-center" colspan="3"><span
                                                            class="badge badge-success text-inverse">Belum ada pengajuan
                                                            Izin
                                                            kamu hari ini</span></td>
                                                @endif
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <img src="{{ asset('images/weekend.png') }}" alt="">
                                </center>
                            </div>
                        </div>

                    @endif

                </div>
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header bg-info">
                            <h2 class="box-title mt-1 text-white text-center"><strong><i class="fas fa-user-clock"></i>
                                    Absensi</strong></h2>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        @if ($informasi['status'] == true)
                                                            <div class="col-md-7">
                                                                <div id="preview" class="w-100"></div>
                                                            </div>
                                                            <div class="col-md-5 d-flex align-items-center">
                                                                <button class="btn btn-info" onclick="take_snapshot()"><i
                                                                        class="fas fa-camera fa-3x"></i> <br>Take a
                                                                    Pitcure</button>
                                                            </div>
                                                        @else
                                                            <div class="col-md-12">

                                                                <img src="{{ asset('images/camera.jpg') }}" alt="">
                                                            </div>
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12 my-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div id="map" style="width: 100%; height:250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id="date-and-clock" class="alert-success p-3 my-2">
                                                <center>
                                                    <i class="fas fa-user-clock fa-5x"></i>
                                                    <h5 id="time" class="box-title mr-b-0"></h5>
                                                    <p id="datenow">
                                                        {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h3>
                                                    </p>
                                                </center>

                                            </div>
                                            <div class="alert alert-{{ $informasi['status'] == false ? 'danger' : 'info' }} border-{{ $informasi['status'] == false ? 'danger' : 'info' }} my-2 text-center"
                                                role="alert">
                                                <p class="m-0"><strong>{{ $informasi['message'] }}</strong>
                                                </p>
                                            </div>
                                            <form id="formAbsensi">
                                                <div class="row">
                                                    @if ($informasi['status'] == true)
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="l30">Bukti Gambar</label>
                                                                <div id="results" class="d-flex justify-content-center">
                                                                    <img src="{{ asset('images/profile.png') }}" id="gambar_bukti" alt=""
                                                                        width="200">
                                                                </div>
                                                                <input type="hidden" name="image" id="image">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="l32">Longtitude</label>
                                                                        <div class="form-input-icon"><i
                                                                                class="material-icons list-icon">place</i>
                                                                            <input class="form-control"
                                                                                placeholder="Longtitude" name="long"
                                                                                type="text" id="long" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="l32">Lattitude</label>
                                                                        <div class="form-input-icon"><i
                                                                                class="material-icons list-icon">place</i>
                                                                            <input class="form-control"
                                                                                placeholder="Latitude" type="text"
                                                                                name="lat" id="lat" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-actions btn-list">
                                                                        <button class="btn btn-info" type="submit"
                                                                            id="btnAbsensi">Absensi</button>
                                                                        <button class="btn btn-outline-default"
                                                                            type="button">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="col-lg-12 d-flex justify-content-center">
                                                            <div class="closed text-center text-danger">
                                                                <i class="fas fa-times-circle fa-7x"></i>
                                                                <p>Saat ini absen tidak tersedia</p>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- <hr>


                            <form id="formAbsensi">
                                @if (!empty($riwayat))
                                    <div class="alert alert-{{ $riwayat['konfirmasi'] == 1 ? 'primary' : 'danger' }} border-{{ $riwayat['konfirmasi'] == 1 ? 'primary' : 'danger' }} alert-dismissible text-center fade show"
                                        role="alert"><strong>Status Kehadiran : </strong> <i
                                            class="material-icons list-icon text-{{ $riwayat['konfirmasi'] == 1 ? 'primary' : 'danger' }}">{{ $riwayat['konfirmasi'] == 1 ? 'check_circle' : 'error_outline' }}</i>
                                        <strong>{{ $riwayat['konfirmasi'] == 1 ? 'Izin Tidak Masuk' : 'Izin anda menunggu konfirmasi!' }}</strong>
                                    </div>
                                @else

                                @endif

                                <div class="form-group">
                                    <select name="" id="" class="form-control">
                                        <option value="sekolah">Absen dari Sekolah</option>
                                        <option value="rumah">Kelas Daring/dari Rumah</option>
                                    </select>

                                    <div id="myDetail" class="d-flex justify-content-around my-3">
                                        <p><b>Waktu Datang : </b> <span>06:00</span></p>
                                        <p><b>Waktu Pulang : </b> <span>06:00</span></p>
                                    </div>

                                    <div class="saveAbsen text-center">
                                        <button type="submit" class="btn btn-success" id="btnAbsensi"><i
                                                class="fas fa-hand-point-up"></i>
                                            Absen</button>
                                    </div>
                                </div>

                            </form> --}}
                        </div>
                        {{-- <div class="card-footer text-muted d-flex justify-content-between">
                            <p class="m-0">Jam Buka :
                                {{ (new \App\Helpers\Help())->getTanggalLengkap($informasi['open']) }}</p>
                            <p class="m-0">Jam Tutup :
                                {{ (new \App\Helpers\Help())->getTanggalLengkap($informasi['close']) }}</p>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalIzin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">FORM PERMINTAAN TIDAK MASUK</h5>
                </div>
                <form id="formIzin" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_izin">
                        <input type="hidden" name="latitude" id="izin_latitude">
                        <input type="hidden" name="longtitude" id="izin_longtitude">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Alasan Tidak Masuk</label>
                                    <div class="col-sm-12">
                                        <select name="alasan" id="alasan" class="form-control">
                                            <option value="" disabled>Pilih Alasan..</option>
                                            <option value="sakit">Sakit</option>
                                            <option value="izin">Izin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal</label>
                                    <div class="col-sm-12">
                                        <input type="date" name="tanggal" id="tanggal" class="form-control"
                                            value="{{ date('Y-m-d') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Bukti</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <div class="col-sm-8 px-0">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" class="form-control" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnIzin">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalBarcode" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">QR Code Siswa</h5>
                </div>
                <div class="modal-body">
                    <div class="downl pull-right">
                        <a href="{{ route('absensi-download_pdf') }}" class="text-dark">
                            <i class="fas fa-download fa-2x"></i>
                        </a>
                    </div>
                    <center>

                        {!! DNS2D::getBarcodeHTML($profile['akun_absen'], 'QRCODE') !!}
                        <b class="my-2">Kode Siswa : {{ $profile['akun_absen'] }}</b>

                        <table class="table table-bordered my-2">
                            <tr>
                                <th>Nama Lengkap</th>
                                <td>:</td>
                                <td>{{ ucwords($profile['nama']) }}</td>
                            </tr>
                            <tr>
                                <th>NIS / NISN</th>
                                <td>:</td>
                                <td>{{ $profile['nis'] }} / {{ $profile['nisn'] }}</td>
                            </tr>
                        </table>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/webcamjs@1.0.26/webcam.min.js" defer></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const secondHand = document.querySelector('.second-hand');
            const minsHand = document.querySelector('.min-hand');
            const hourHand = document.querySelector('.hour-hand');

            function setDate() {
                const now = new Date();

                const seconds = now.getSeconds();
                const secondsDegrees = ((seconds / 60) * 360) + 90;
                secondHand.style.transform = `rotate(${secondsDegrees}deg)`;

                const mins = now.getMinutes();
                const minsDegrees = ((mins / 60) * 360) + ((seconds / 60) * 6) + 90;
                minsHand.style.transform = `rotate(${minsDegrees}deg)`;

                const hour = now.getHours();
                const hourDegrees = ((hour / 12) * 360) + ((mins / 60) * 30) + 90;
                hourHand.style.transform = `rotate(${hourDegrees}deg)`;
            }

            setInterval(setDate, 1000);

            setDate();

            $('#izinMasuk').click(function() {
                $('#formIzin').trigger("reset");
                let lati = $('#lat').val();
                let longti = $('#long').val();
                $('#izin_latitude').val(lati);
                $('#izin_longtitude').val(longti);
                $('#modalIzin').modal('show');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });
            $('#qr_code').click(function() {
                $('#modalBarcode').modal('show');
            });

            var map = L.map('map', {
                zoomControl: false
            }).fitWorld();

            L.tileLayer(
                'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token={{ $key_maps['token'] }}', {
                    maxZoom: 20
                }).addTo(map);

            map.locate({
                setView: true,
                watch: true,
                maxZoom: 17
            });
            var marker;
            var MIcon = L.icon({
                iconSize: [25, 41],
                iconAnchor: [10, 41],
                popupAnchor: [2, -40],
                iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
                shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
            });

            map.on('locationfound', function(ev) {
                // console.log(ev);
                if (!marker) {
                    $('#lat').val(ev.latitude);
                    $('#long').val(ev.longitude);
                    marker = L.marker([ev.latitude, ev.longitude, 12], {
                        icon: MIcon
                    }).bindPopup('Lokasi Anda').addTo(map);
                } else {
                    marker.setLatLng(ev.latlng);
                }
            }).on("locationerror", error => {
                if (marker) {
                    map.removeLayer(marker);
                    marker = undefined;
                }
            });

            $('body').on('submit', '#formIzin', function(e) {
                e.preventDefault();
                $("#btnIzin").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnIzin").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('absensi-izin_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('absensi-izin_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formIzin').trigger("reset");
                            $('#modalIzin').modal('hide');
                            $('#data_izin').html(data.riwayat_siswa);
                        }
                        noti(data.icon, data.message);
                        $('#btnIzin').html('Simpan');
                        $("#btnIzin").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#formAbsensi', function(e) {
                e.preventDefault();
                $("#btnAbsensi").html(
                    '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                $("#btnAbsensi").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    url: "{{ route('absensi-hadir_create') }}",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {;
                            $('#gambar_bukti').attr('src', '{{ asset("images/profile.png") }}');
                            $('#data_hadir').html(data.hadir);

                        }
                        noti(data.icon, data.message);
                        $('#btnAbsensi').html('Absen');
                        $("#btnAbsensi").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
            let informasi = "{{ $informasi['status'] }}";
            if (informasi == true) {
                Webcam.set({
                    width: 320,
                    height: 240,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach('#preview');

            }

        })

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function() {
                startTime()
            }, 500);
        }
        startTime();

        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap(function(data_uri) {
                console.log(data_uri);
                // display results in page
                document.getElementById('results').innerHTML =
                    '<img src="' + data_uri + '" id="gambar_bukti"/>';
                $('#image').val(data_uri);
            });
        }
    </script>
@endsection
