@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .clock {
            width: 200px;
            height: 200px;
            position: relative;
            /* padding: 2rem; */
            border: 7px solid #1f2578;
            box-shadow: -4px -4px 10px rgba(67, 67, 67, 0.5),
                inset 4px 4px 10px rgba(0, 0, 0, 0.5),
                inset -4px -4px 10px rgba(67, 67, 67, 0.5),
                4px 4px 10px rgba(0, 0, 0, 0.3);
            border-radius: 50%;
            margin: auto;

        }

        .outer-clock-face {
            position: relative;
            background: #1f2578;
            overflow: hidden;
            width: 100%;
            height: 100%;
            border-radius: 100%;
        }

        .outer-clock-face::after {
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .outer-clock-face::after,
        .outer-clock-face::before,
        .outer-clock-face .marking {
            content: '';
            position: absolute;
            width: 5px;
            height: 100%;
            background: #1df52f;
            z-index: 0;
            left: 49%;
        }

        .outer-clock-face .marking {
            background: #bdbdcb;
            width: 3px;
        }

        .outer-clock-face .marking.marking-one {
            -webkit-transform: rotate(30deg);
            -moz-transform: rotate(30deg);
            transform: rotate(30deg)
        }

        .outer-clock-face .marking.marking-two {
            -webkit-transform: rotate(60deg);
            -moz-transform: rotate(60deg);
            transform: rotate(60deg)
        }

        .outer-clock-face .marking.marking-three {
            -webkit-transform: rotate(120deg);
            -moz-transform: rotate(120deg);
            transform: rotate(120deg)
        }

        .outer-clock-face .marking.marking-four {
            -webkit-transform: rotate(150deg);
            -moz-transform: rotate(150deg);
            transform: rotate(150deg)
        }

        .inner-clock-face {
            position: absolute;
            top: 10%;
            left: 10%;
            width: 80%;
            height: 80%;
            background: #1f2578;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            z-index: 1;
        }

        .inner-clock-face::before {
            content: '';
            position: absolute;
            top: 50%;
            left: 50%;
            width: 16px;
            height: 16px;
            border-radius: 18px;
            margin-left: -9px;
            margin-top: -6px;
            background: #4d4b63;
            z-index: 11;
        }

        .hand {
            width: 50%;
            right: 50%;
            height: 6px;
            background: #61afff;
            position: absolute;
            top: 50%;
            border-radius: 6px;
            transform-origin: 100%;
            transform: rotate(90deg);
            transition-timing-function: cubic-bezier(0.1, 2.7, 0.58, 1);
        }

        .hand.hour-hand {
            width: 30%;
            z-index: 3;
        }

        .hand.min-hand {
            height: 3px;
            z-index: 10;
            width: 40%;
        }

        .hand.second-hand {
            background: #ee791a;
            width: 45%;
            height: 2px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-md-4 my-2">
            <div class="card">
                <div class="card-body">
                    <center>
                        <h5 class="card-title">Hari Ini</h5>
                    </center>
                    <div class="clock">
                        <div class="outer-clock-face">
                            <div class="marking marking-one"></div>
                            <div class="marking marking-two"></div>
                            <div class="marking marking-three"></div>
                            <div class="marking marking-four"></div>
                            <div class="inner-clock-face">
                                <div class="hand hour-hand"></div>
                                <div class="hand min-hand"></div>
                                <div class="hand second-hand"></div>
                            </div>
                        </div>
                    </div>
                    <center>
                        <h5 id="time" class="box-title mr-b-0"></h5>
                        <p id="datenow">
                            {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h3>
                        </p>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-md-8 my-2">
            <div class="row">
                <div class="col-lg-6 col-sm-6 my-2">
                    <div class="widget-stat card bg-info">
                        <div class="card-body p-4">
                            <div class="media">
                                <div class="media-body text-white">
                                    <p class="mb-1"><i class="fas fa-graduation-cap"></i> Jumlah Akun yang
                                        terdaftar</p>
                                    <h3 class="text-white">{{ $dashboard['akun'] }}</h3>
                                    <div class="progress mb-2 bg-primary">
                                        <div class="progress-bar progress-animated bg-light" style="width: 76%"></div>
                                    </div>
                                    <small class="text-white">Gabungan Siswa dan pengajar</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 my-2">
                    <div class="widget-stat card bg-purple">
                        <div class="card-body p-4">
                            <div class="media">
                                <span class="mr-3">
                                    <i class="la la-graduation-cap"></i>
                                </span>
                                <div class="media-body text-white">
                                    <p class="mb-1">Izin di Semester saat ini</p>
                                    <h3 class="text-white">{{ $dashboard['izin'] }}</h3>
                                    <div class="progress mb-2 bg-primary">
                                        <div class="progress-bar progress-animated bg-light" style="width: 76%"></div>
                                    </div>
                                    <small class="text-white">Gabungan Siswa dan pengajar</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 my-2">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h5 class="card-title">Settingan Libur Bulan ini</h5>
                            </center>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Perayaan/Memperingati</th>
                                        <th>Status Libur</th>
                                    </tr>
                                    <tbody>
                                        @if (!empty($libur))
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($libur as $lb)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ (new \App\Helpers\Help())->getDay($lb['tgl_libur']) }}</td>
                                                    <td>{{ $lb['nama'] }}</td>
                                                    <td> <label class="switch">
                                                            <input type="checkbox"
                                                                {{ $lb['status'] == 1 ? 'checked' : '' }}
                                                                class="libur_check" data-id="{{ $lb['id'] }}">
                                                            <span class="slider round"></span>
                                                        </label></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4" class="text-center">Tidak ada libur di bulan ini</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12 my-2">
            <div class="alert alert-danger border-danger" role="alert">
                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                        aria-hidden="true">×</span>
                </button>
                <p class="my-1"><strong>Peringatan:</strong>
                </p>
                <p class="m-0">Perizinan yang sudah di konfirmasi / di tolak tidak bisa untuk di batalkan. jadi
                    harap cermati sebelum melakukan aksi</p>
            </div>
        </div>
        <div class="col-md-12 my-2">
            <div class="card">
                <div class="card-body">
                    <center>
                        <h5 class="card-title">Pengajuan Izin Siswa Bulan ini</h5>
                    </center>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Profil</th>
                                <th>Faktor</th>
                                <th>Kelas</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            <tbody id="data_izin_siswa">
                                @if (!empty($siswa))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($siswa as $sw)
                                        <tr>
                                            <td class="vertical-middle">{{ $no++ }}</td>
                                            <td class="vertical-middle">
                                                {{ (new \App\Helpers\Help())->getDay($sw['tgl_kehadiran']) }}</td>
                                            <td>
                                                <b>{{ ucwords($sw['nama']) }}</b>
                                                <p class="m-0">NIS/NISN : {{ $sw['nis'] . '/' . $sw['nisn'] }}
                                                </p>
                                            </td>
                                            <td class="vertical-middle">{{ $sw['status_kehadiran'] }}</td>
                                            <td><b>{{ $sw['rombel'] }}</b><br><small>{{ $sw['jurusan'] }}</small></td>
                                            <td class="vertical-middle">
                                                <span
                                                    class="badge badge-{{ $sw['konfirmasi'] == 1 ? 'success' : 'danger' }} text-inverse p-1 rounded">
                                                    @if ($sw['konfirmasi'] == 0)
                                                        Belum terverifikasi
                                                    @elseif($sw['konfirmasi'] == 1)
                                                        Sudah diverifikasi
                                                    @else
                                                        Ditolak
                                                    @endif
                                                </span>
                                            </td>
                                            <td class="vertical-middle">
                                                @if ($sw['konfirmasi'] == 0)
                                                    <a href="javascript:void(0)" data-id="{{ $sw['id'] }}"
                                                        data-base="siswa" class="btn btn-sm btn-success konfirmasi"><i
                                                            class="fas fa-clipboard-check"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $sw['id'] }}"
                                                        data-base="siswa" class="btn btn-sm btn-danger reject"><i
                                                            class="fas fa-times-circle"></i></a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-secondary"><i
                                                            class="fas fa-stop-circle"></i></a>

                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">Tidak ada libur di bulan ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 my-2">
            <div class="card">
                <div class="card-body">
                    <center>
                        <h5 class="card-title">Pengajuan Izin Guru Bulan ini</h5>

                    </center>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Profil</th>
                                <th>Faktor</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            <tbody id="data_izin_guru">
                                @if (!empty($guru))
                                    @php
                                        $nomer = 1;
                                    @endphp
                                    @foreach ($guru as $gr)
                                        <tr>
                                            <td class="vertical-middle">{{ $nomer++ }}</td>
                                            <td class="vertical-middle">
                                                {{ (new \App\Helpers\Help())->getDay($gr['tgl_kehadiran']) }}</td>
                                            <td>
                                                <b>{{ ucwords($gr['nama']) }}</b>
                                                <p class="m-0">NIP : {{ $gr['nip'] }}
                                                </p>
                                            </td>
                                            <td class="vertical-middle">{{ $gr['status_kehadiran'] }}</td>
                                            <td class="vertical-middle">
                                                <span
                                                    class="badge badge-{{ $gr['konfirmasi'] == 1 ? 'success' : 'danger' }} text-inverse p-1 rounded">
                                                    @if ($gr['konfirmasi'] == 0)
                                                        Belum terverifikasi
                                                    @elseif($gr['konfirmasi'] == 1)
                                                        Sudah diverifikasi
                                                    @else
                                                        Ditolak
                                                    @endif
                                                </span>
                                            </td>
                                            <td>
                                                @if ($gr['konfirmasi'] == 0)
                                                    <a href="javascript:void(0)" data-id="{{ $gr['id'] }}"
                                                        data-base="guru" class="btn btn-sm btn-success konfirmasi"><i
                                                            class="fas fa-clipboard-check"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $gr['id'] }}"
                                                        data-base="guru" class="btn btn-sm btn-danger reject"><i
                                                            class="fas fa-times-circle"></i></a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-secondary"><i
                                                            class="fas fa-stop-circle"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Tidak ada yang terdaftar bulan ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const secondHand = document.querySelector('.second-hand');
            const minsHand = document.querySelector('.min-hand');
            const hourHand = document.querySelector('.hour-hand');

            function setDate() {
                const now = new Date();

                const seconds = now.getSeconds();
                const secondsDegrees = ((seconds / 60) * 360) + 90;
                secondHand.style.transform = `rotate(${secondsDegrees}deg)`;

                const mins = now.getMinutes();
                const minsDegrees = ((mins / 60) * 360) + ((seconds / 60) * 6) + 90;
                minsHand.style.transform = `rotate(${minsDegrees}deg)`;

                const hour = now.getHours();
                const hourDegrees = ((hour / 12) * 360) + ((mins / 60) * 30) + 90;
                hourHand.style.transform = `rotate(${hourDegrees}deg)`;
            }

            setInterval(setDate, 1000);

            setDate();

            $(document).on('click', '.libur_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-libur_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.konfirmasi', function() {
                let id = $(this).data('id');
                let based = $(this).data('base');
                let loader = $(this);
                if (confirm('Apa kamu yakin ingin megaktivasi, proses perizinan ini?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-izin_update') }}",
                        data: {
                            id,
                            based,
                            value: 1
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (based == 'guru') {
                                console.log("data guru berhasil diupdate");
                                $('#data_izin_guru').html(data.izin);
                            } else {
                                console.log("data siswa berhasil diupdate");
                                $('#data_izin_siswa').html(data.izin);
                            }
                        }
                    });
                }

            });
            $(document).on('click', '.cancel', function() {
                let id = $(this).data('id');
                let based = $(this).data('base');
                let loader = $(this);
                if (confirm('Apa kamu yakin ingin membatalkan, proses perizinan ini?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-izin_update') }}",
                        data: {
                            id,
                            based,
                            value: 0
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (based == 'guru') {
                                console.log("data guru berhasil diupdate");
                                $('#data_izin_guru').html(data.izin);
                            } else {
                                console.log("data siswa berhasil diupdate");
                                $('#data_izin_siswa').html(data.izin);
                            }
                        }
                    });
                }

            });

            $(document).on('click', '.reject', function() {
                let id = $(this).data('id');
                let value = 2;
                let based = $(this).data('base');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menolak pengajuan izin ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-izin_update') }}",
                        type: "POST",
                        data: {
                            id,
                            value,
                            based,
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (based == 'guru') {
                                console.log("data guru berhasil diupdate");
                                $('#data_izin_guru').html(data.izin);
                            } else {
                                console.log("data siswa berhasil diupdate");
                                $('#data_izin_siswa').html(data.izin);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function() {
                startTime()
            }, 500);
        }
        startTime();
    </script>
@endsection
