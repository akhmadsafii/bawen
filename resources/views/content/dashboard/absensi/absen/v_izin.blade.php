@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="row">
        <div class="col-xl-12 col-xxl-12">
            <div class="col-md-12 my-3">
                <div class="card">
                    <div class="card-header bg-danger">
                        <h2 class="box-title m-0 text-white">Status Izin Hari ini
                            {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h2>
                    </div>
                    <div class="card-body">
                        <table class="table widget-status-table">
                            <thead>
                                <tr>
                                    <th class="text-center"></th>
                                    <th class="text-center">Dibuat</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="data_izin">
                                @if (!empty($riwayat))
                                    <tr>
                                        <td class="text-center"><span class="badge badge-danger text-inverse">Izin</span>
                                        </td>
                                        <td class="text-center">
                                            {{ (new \App\Helpers\Help())->getTanggalLengkap($riwayat['created_at']) }}
                                        </td>
                                        <td class="text-center"><span
                                                class="text-{{ $riwayat['konfirmasi'] == 1 ? 'success' : 'danger' }}"><i
                                                    class="fas fa-{{ $riwayat['konfirmasi'] == 1 ? 'check-circle' : 'exclamation-circle' }} "></i>
                                                {{ $riwayat['konfirmasi'] == 1 ? 'Terverifikasi' : ' Belum Diverifikasi' }}</span>
                                        </td>
                                    </tr>
                                @else
                                    <td class="text-center" colspan="3"><span
                                            class="badge badge-success text-inverse">Belum ada pengajuan
                                            Izin
                                            kamu hari ini</span></td>
                                @endif
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-xxl-12">
            <div class="row">
                <div class="col-md-6 my-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between bg-info">
                                    <h2 class="box-title mt-1 text-white"><strong> Identitas Diri</strong></h2>
                                    <div class="pamit">
                                        @if ($informasi['status'] == true)
                                            <button class="btn btn-danger my-1" id="izinMasuk"><i
                                                    class="fas fa-paste"></i>
                                                IZIN</button>
                                        @else
                                            <button class="btn btn-dark my-1"><i class="fas fa-smile-beam"></i>
                                                LIBUR</button>
                                        @endif
                                        <button class="btn btn-success my-1" id="qr_code"><i class="fas fa-qrcode"></i> QR
                                            CODE</button>

                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="w-100">
                                        <tr>
                                            <td rowspan="8" class="text-center vertical-middle"><img
                                                    src="{{ $profile['file'] }}" alt="" height="200px"></td>
                                            <th>Nama Lengkap</th>
                                            <td>{{ ucwords($profile['nama']) }}</td>
                                        </tr>
                                        @if (session('role') == 'siswa')
                                            <tr>
                                                <th>NIS/NISN</th>
                                                <td>{{ !empty($profile['nis']) ? $profile['nis'] : '-' }} /
                                                    {{ !empty($profile['nisn']) ? $profile['nisn'] : '-' }}</td>
                                            </tr>

                                            <tr>
                                                <th>Rombel</th>
                                                <td>{{ $profile['rombel'] }}</td>
                                            </tr>

                                            <tr>
                                                <th>Jurusan</th>
                                                <td>{{ $profile['jurusan'] }}</td>
                                            </tr>
                                            <tr>
                                                <th>Wali Kelas</th>
                                                <td>{{ $profile['wali_kelas'] }}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <th>NIP/NIK</th>
                                                <td>{{ !empty($profile['nip']) ? $profile['nip'] : '-' }} /
                                                    {{ !empty($profile['nik']) ? $profile['nik'] : '-' }}</td>
                                            </tr>

                                            <tr>
                                                <th>NUPTK</th>
                                                <td>{{ !empty($profile['nuptk']) ? $profile['nuptk'] : '-' }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $profile['email'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Telepon</th>
                                            <td>{{ $profile['telepon'] }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="card-footer text-muted d-flex justify-content-between">
                                    @if (session('role') == 'siswa')
                                        <p class="m-0">NISN SISWA : {{ $profile['nisn'] }}</p>
                                    @else
                                        <p class="m-0">NIP : {{ $profile['nip'] }}</p>
                                    @endif
                                    <p class="m-0">Dibuat :
                                        {{ (new \App\Helpers\Help())->getTanggal($profile['created_at']) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 my-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between bg-info">
                                    <h2 class="box-title mt-1 text-white"><strong> Koordinat Map</strong></h2>

                                </div>
                                <div class="card-body">
                                    <div id="map" style="width: 100%; height:250px;"></div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalIzin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">FORM PERMINTAAN TIDAK MASUK</h5>
                </div>
                <form id="formIzin" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_izin">
                        <input type="text" name="latitude" id="izin_latitude">
                        <input type="text" name="longtitude" id="izin_longtitude">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Alasan Tidak Masuk</label>
                                    <div class="col-sm-12">
                                        <select name="alasan" id="alasan" class="form-control">
                                            <option value="" disabled>Pilih Alasan..</option>
                                            <option value="sakit">Sakit</option>
                                            <option value="izin">Izin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal</label>
                                    <div class="col-sm-12">
                                        <input type="date" name="tanggal" id="tanggal" class="form-control"
                                            value="{{ date('Y-m-d') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Bukti</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <div class="col-sm-8 px-0">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" class="form-control" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnIzin">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalBarcode" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">QR Code Siswa</h5>
                </div>
                <div class="modal-body">
                    <div class="downl pull-right">
                        <a href="{{ route('absensi-download_pdf') }}" class="text-dark">
                            <i class="fas fa-download fa-2x"></i>
                        </a>
                    </div>
                    <center>

                        {!! DNS2D::getBarcodeHTML($profile['akun_absen'], 'QRCODE') !!}
                        <b class="my-2">Kode Siswa : {{ $profile['akun_absen'] }}</b>

                        <table class="table table-bordered my-2">
                            <tr>
                                <th>Nama Lengkap</th>
                                <td>:</td>
                                <td>{{ ucwords($profile['nama']) }}</td>
                            </tr>
                            @if (session('role') == 'siswa')
                                <tr>
                                    <th>NIS / NISN</th>
                                    <td>:</td>
                                    <td>{{ $profile['nis'] }} / {{ $profile['nisn'] }}</td>
                                </tr>
                            @else
                                <tr>
                                    <th>NIP / NUPTK</th>
                                    <td>:</td>
                                    <td>{{ $profile['nip'] }} / {{ $profile['nuptk'] }}</td>
                                </tr>
                            @endif

                        </table>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var lat, long;

            $('#izinMasuk').click(function() {
                $('#formIzin').trigger("reset");
                $('#izin_latitude').val(lat);
                $('#izin_longtitude').val(long);
                $('#modalIzin').modal('show');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });
            $('#qr_code').click(function() {
                $('#modalBarcode').modal('show');
            });
            var map = L.map('map', {
                zoomControl: false
            }).fitWorld();

            var lokasi = "{{ $lokasi['token'] }}";
            if (lokasi) {
                L.tileLayer(
                    'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=' +
                    lokasi, {
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                        maxZoom: 18
                    }).addTo(map);
            } else {
                alert('anda belum set configurasi maps di settingan admin');
            }

            map.locate({
                setView: true,
                watch: true,
                maxZoom: 17
            });
            var marker;
            var MIcon = L.icon({
                iconSize: [25, 41],
                iconAnchor: [10, 41],
                popupAnchor: [2, -40],
                iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
                shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
            });

            map.on('locationfound', function(ev) {
                // console.log(ev);
                if (!marker) {
                    lat = ev.latitude;
                    long = ev.longitude;

                    marker = L.marker([ev.latitude, ev.longitude, 12], {
                        icon: MIcon
                    }).bindPopup('Lokasi Anda').addTo(map);
                } else {
                    marker.setLatLng(ev.latlng);
                }
            }).on("locationerror", error => {
                if (marker) {
                    map.removeLayer(marker);
                    marker = undefined;
                }
            });

            $('body').on('submit', '#formIzin', function(e) {
                e.preventDefault();
                $("#btnIzin").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnIzin").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('absensi-izin_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('absensi-izin_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formIzin').trigger("reset");
                            $('#modalIzin').modal('hide');
                            $('#data_izin').html(data.riwayat_siswa);
                        }
                        noti(data.icon, data.message);
                        $('#btnIzin').html('Simpan');
                        $("#btnIzin").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        })
    </script>
@endsection
