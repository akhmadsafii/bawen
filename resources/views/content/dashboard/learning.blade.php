@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-top: 1.14286em;
            padding: 0;
            border: 0.0625rem solid #eee;
        }

        .col-md-6 {
            margin-top: 12px;
        }

        .dataTables_wrapper {
            width: 100%;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: #51d2b7;
            border: 0;
            border-radius: 0;
            color: #fff !important;
        }

        .dataTables_wrapper table.no-footer {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

        .color-info,
        .text-info {
            color: #343030 !important;
        }

    </style>

    <div class="row">
        <div class="col-sm-8 col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Jadwal Hari Ini</h5>
                    <div class="table-responsive">
                        <table class="table table-striped widget-status-table mr-b-0 hover" id="tabel-jadwal-hari-ini">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Jam</th>
                                    <th>Rombel</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th class="pd-r-20">Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Semua Jadwal</h5>
                    <div class="table-responsive">
                        <table class="table table-striped widget-status-table mr-b-0 hover" id="tabel-jadwal">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Hari</th>
                                    <th>Jam</th>
                                    <th>Rombel</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th class="pd-r-20">Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            var guru = {{ session('id') }};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const konfigUmum = {
                responsive: true,
                serverSide: true,
                processing: true,
                ordering: false,
            };

            $('#tabel-jadwal-hari-ini').DataTable({
                ...konfigUmum,
                paging: false,
                searching: false,
                dom: `rt`,
                ajax: {
                    "url": "{{ route('dashboard-get_jadwal_guru_hari_ini') }}",
                    "method": "POST",
                    "data": function(data) {
                        data.id_guru = guru;
                    },
                },
                columns: [{
                        data: 'jam',
                        name: 'jam'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'ruang',
                        name: 'ruang'
                    }
                ],
            });

            $('#tabel-jadwal').DataTable({
                ...konfigUmum,
                buttons: [{
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="far fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="far fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                dom: `<"pd-l-20 float-left"B><"pd-r-20 float-right"f>rt<"row pd-l-20 pd-r-20"<"col-sm-4"l><"col-sm-8"p>>`,
                ajax: {
                    "url": "{{ route('dashboard-get_jadwal_guru') }}",
                    "method": "POST",
                    "data": function(data) {
                        data.id_guru = guru;
                    },
                },
                columns: [{
                        data: 'hari',
                        name: 'hari'
                    },
                    {
                        data: 'jam',
                        name: 'jam'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'ruang',
                        name: 'ruang'
                    }
                ]
            });
        })
    </script>
@endsection
