@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-top: 1.14286em;
            padding: 0;
            border: 0.0625rem solid #eee;
        }

        .col-md-6 {
            margin-top: 12px;
        }

        .dataTables_wrapper {
            width: 100%;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: #51d2b7;
            border: 0;
            border-radius: 0;
            color: #fff !important;
        }

        .dataTables_wrapper table.no-footer {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

    </style>

    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Jumlah Siswa</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered widget-status-table mr-b-0 hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center" rowspan="2">Nomer</th>
                                    <th class="text-center" rowspan="2">Rombel</th>
                                    <th class="text-center" rowspan="2">Kelas</th>
                                    <th class="text-center" rowspan="2">Jurusan</th>
                                    <th class="text-center" colspan="3">Jumlah Siswa</th>
                                </tr>
                                <tr class="bg-info">
                                    <th class="text-center">L</th>
                                    <th class="text-center">P</th>
                                    <th class="text-center">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $nomer = 1;
                                    $l = 0;
                                    $p = 0;
                                    $jm = 0;
                                @endphp
                                @foreach ($dashboard as $ds)
                                    <tr>
                                        <td class="text-center">{{ $nomer++ }}</td>
                                        <td class="text-center">{{ $ds['nama'] }}</td>
                                        <td class="text-center">{{ $ds['kelas'] }}</td>
                                        <td class="text-center">{{ $ds['jurusan'] }}</td>
                                        <td class="text-center">{{ $ds['l'] }}</td>
                                        <td class="text-center">{{ $ds['p'] }}</td>
                                        <td class="text-center">{{ $ds['jumlah'] }}</td>
                                    </tr>
                                    @php
                                        $l += $ds['l'];
                                        $p += $ds['p'];
                                        $jm += $ds['jumlah'];
                                    @endphp
                                @endforeach
                                <tr class="bg-info">
                                    <td class="text-center font-weight-bold" colspan="4">Total</td>
                                    <td class="text-center font-weight-bold">{{ $l }}</td>
                                    <td class="text-center font-weight-bold">{{ $p }}</td>
                                    <td class="text-center font-weight-bold">{{ $jm }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        // const labels = [
        //     'January',
        //     'February',
        //     'March',
        //     'April',
        //     'May',
        //     'June',
        // ];
        // const data = {
        //     labels: labels,
        //     datasets: [{
        //         label: 'My First dataset',
        //         backgroundColor: 'rgb(255, 99, 132)',
        //         borderColor: 'rgb(255, 99, 132)',
        //         data: [0, 10, 5, 2, 20, 30, 45],
        //     }]
        // };

        // const config = {
        //     type: 'line',
        //     data,
        //     options: {}
        // };

        // var myChart = new Chart(
        //     document.getElementById('myChart'),
        //     config
        // );

        // $('#tabel-dashboard').DataTable({
        //     processing: true,
        //     serverSide: true,
        //     responsive: true,
        //     ajax: {
        //         "url": "{{ route('pkl_industri-beranda') }}",
        //         "method": "GET",
        //     },
        //     columns: [{
        //             data: 'DT_RowIndex',
        //             name: 'DT_RowIndex'
        //         },
        //         {
        //             data: 'nama',
        //             name: 'nama'
        //         },
        //         {
        //             data: 'alamat',
        //             name: 'alamat'
        //         },
        //         {
        //             data: 'pimpinan',
        //             name: 'pimpinan'
        //         },
        //     ]
        // });
    </script>

@endsection
