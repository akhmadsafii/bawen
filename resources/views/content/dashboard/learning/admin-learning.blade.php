@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="clearfix">
        <div class="widget-list">
            <div class="row my-2">
                <div class="col-md-8">
                    <div class="card card-danger my-shadow">
                        <div class="card-header bg-info my-auto">
                            <h5 class="box-title my-0 text-white">Room Yang Aktif</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Mapel</th>
                                            <th>Guru</th>
                                            <th>Rombel</th>
                                            <th>Status Aktif</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'guru',
                        name: 'guru'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '.room_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 2;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin_room-update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                    }
                });
            });
        })
    </script>
@endsection
