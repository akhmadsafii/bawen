@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .canvasjs-chart-credit {
            display: none !important;
        }

    </style>
    <div class="row">
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-info">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Total Keseluruhan</h4>
                        <div class="media d-flex">
                            <div class="align-self-center">
                                <i class="fas fa-users fa-6x float-left"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="text-white">{{ $top['total_alumni'] }}</h3>
                                <span>Jumlah Alumni</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-success">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Berdasarkan Gender</h4>
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="success text-white">{{ $top['gender']['l'] }}</h3>
                                <i class="fas fa-male"></i><span> Pria</span>
                            </div>
                            <div class="media-body text-center">
                                <h3 class="success text-white">{{ $top['gender']['p'] }}</h3>
                                <i class="fas fa-female"></i><span> Wanita</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-primary">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Jumlah Survey</h4>
                        <div class="media d-flex">
                            <div class="align-self-center">
                                <i class="fas fa-poll fa-6x float-left"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="text-white">{{ $top['survei'] }}</h3>
                                <span>Tracking</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-purple">
                        <h4 class="text-center mt-0 font-weight-bold text-white">Partisipasi Survey</h4>
                        <div class="media d-flex">

                            <div class="media-body">
                                <h3 class="text-white">{{ $top['partisipant'] }}</h3>
                                <span class="text-white">Tracking</span>
                            </div>
                            <div class="align-self-center">
                                <h1 class="font-weight-bold text-white">{{ $top['partisipant_percent'] }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 my-2">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"><i class="fas fa-chart-pie"></i> Diagram Pekerjaan</h5>
                    <hr>
                    <center>
                        <div class="padded-reverse" id="chart" style="height: 370px; width: 100%;">
                            {{-- {!! $alumniChart->container() !!} --}}
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-md-6 my-2">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"><i class="fas fa-user-tie"></i> Tabel Pekerjaan</h5>
                    <hr>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Pekerjaan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pekerjaan as $pk)
                                <tr>
                                    <td>{{ $pk['nama'] }}</td>
                                    <td>{{ $pk['jumlah'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 my-2">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"><i class="fas fa-graduation-cap"></i> Angkatan</h5>
                    <hr>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Angkatan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tahun_lulus as $tl)
                                <tr>
                                    <td>{{ $tl['lulusan'] }}</td>
                                    <td>{{ $tl['total'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 my-2">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"><i class="fas fa-chart-line"></i> Aktifitas Alumni</h5>
                    <hr>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Aktifitas</th>
                                <th width="150">Jumlah Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stats_data as $sd => $val)
                                <tr>
                                    <td>{{ $sd }}</td>
                                    <td>{{ $val }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 my-2">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"><i class="fas fa-poll"></i> Survey</h5>
                    <hr>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nama Survey</th>
                                <th>Partisipan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($survey as $sr)
                                <tr>
                                    <td>{{ $sr['nama'] }}</td>
                                    <td>{{ $sr['partisipant'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>


    <script>
        var dataPoints = [];
        var chart = new CanvasJS.Chart("chart", {
            animationEnabled: true,
            exportEnabled: true,
            title: {
                text: "Pekerjaan Alumni"
            },
            // subtitles: [{
            //     text: "Currency Used: Thai Baht (฿)"
            // }],
            data: [{
                type: "pie",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - #percent%",
                yValueFormatString: "##0",
                dataPoints: dataPoints
            }]
        });
        chart.render();

        $.getJSON("{{ route('admin_alumni-statistic') }}", addData);

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y
                });
            }
            chart.render();
        }

        // })
    </script>
@endsection
