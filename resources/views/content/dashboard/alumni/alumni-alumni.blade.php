@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .btn-primary {
            background-color: #42A5F5 !important;
            border-color: #42A5F5 !important
        }

        .cursor-pointer {
            cursor: pointer;
            color: #42A5F5
        }

        .pic {
            margin-top: 30px;
            margin-bottom: 20px
        }

        .card-block {
            width: 200px;
            border: 1px solid lightgrey;
            border-radius: 5px !important;
            background-color: #FAFAFA;
            margin-bottom: 30px
        }

        .card-body.show {
            display: block
        }

        .card {
            padding-bottom: 20px;
            box-shadow: 2px 2px 6px 0px rgb(200, 167, 216)
        }

        .radio {
            display: inline-block;
            border-radius: 0;
            box-sizing: border-box;
            cursor: pointer;
            color: #000;
            font-weight: 500;
            -webkit-filter: grayscale(100%);
            -moz-filter: grayscale(100%);
            -o-filter: grayscale(100%);
            -ms-filter: grayscale(100%);
            filter: grayscale(100%)
        }

        .radio:hover {
            box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1)
        }

        .radio.selected {
            box-shadow: 0px 8px 16px 0px #EEEEEE;
            -webkit-filter: grayscale(0%);
            -moz-filter: grayscale(0%);
            -o-filter: grayscale(0%);
            -ms-filter: grayscale(0%);
            filter: grayscale(0%)
        }

        .selected {
            background-color: #E0F2F1
        }

        .a {
            justify-content: center !important
        }

        .btn {
            border-radius: 0px
        }

        .btn,
        .btn:focus,
        .btn:active {
            outline: none !important;
            box-shadow: none !important
        }

    </style>

    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card text-center justify-content-center shaodw-lg card-1 border-0 bg-white px-sm-2">
                <div class="card-body show ">
                    <div class="row">
                        <div class="col">
                            <h5><b>Ikuti Survey</b></h5>
                            <p> What are you reporting ? <span class=" ml-1 cursor-pointer"> Learn more</span> </p>
                        </div>
                    </div>
                    <div class="radio-group row justify-content-between px-3 text-center a">

                        @foreach ($kategori as $kt)
                            @if ($kt['status_terisi'] != false)
                                <div class="col-auto mr-sm-2 mx-1 card-block py-0 text-center radio selected "
                                    style="position: relative">
                                @else
                                    <div class="col-auto ml-sm-2 mx-1 card-block py-0 text-center radio "
                                        data-id="{{ $kt['id'] }}" style="position: relative">
                            @endif
                            <button class="btn btn-sm btn-info infoKategori" data-total="{{ $kt['total_pertanyaan'] }}"
                                data-dijawab="{{ $kt['pertanyaan_dijawab'] }}"
                                style="position: absolute; right: 2px; top: 2px; border-radius: 11px"><i
                                    class="fas fa-info-circle"></i></button>
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic">
                                        @if ($kt['status_terisi'] != false)
                                            <img class="irc_mut img-fluid" src="https://i.imgur.com/6r0XCez.png" width="100"
                                                height="100">
                                        @else
                                            <img class="irc_mut img-fluid" src="https://i.imgur.com/bGCqROr.png" width="100"
                                                height="100">
                                        @endif

                                    </div>
                                    <p>{{ $kt['nama'] }}</p>
                                    <p>{{ $kt['keterangan'] }}</p>
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
                <div class="row justify-content-center">
                    <div class="col">
                        <p class="text-muted">A "Single transcript" is something you would have retrived in this way
                        </p>
                    </div>
                </div>
                <input type="hidden" id="id_kategori">
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <button type="button" class="btn btn-outline-secondary">
                            <span class="mr-2"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                            Back
                        </button>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-primary btnStart" disabled onclick="select_kategori()">Continue
                            <span class="ml-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.radio-group .radio').click(function() {
                $('.selected .fa').removeClass('fa-check');
                $('.radio').removeClass('selected');
                $(this).addClass('selected');
                $(".btnStart").attr("disabled", false);
                $("#id_kategori").val($(this).data('id'));
            });

            $(document).on('click', '.infoKategori', function() {
                let total = $(this).attr('data-total');
                let dijawab = $(this).attr('data-dijawab');
                let newLine = "\r\n";
                let msg = "Total Pertanyaan : " + total + " Pertanyaan";
                msg += newLine;
                msg += "Dijawab : " +dijawab+" Pertanyaan";
                alert(msg);
            });
        });

        function select_kategori() {
            if ($("#id_kategori").val() == null || $("#id_kategori").val() == "") {
                alert("Survey sampun nate diwangsuli");
                return false;
            } else {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-select_kategori') }}",
                    data: {
                        id: $("#id_kategori").val()
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.success == true) {
                            window.location = data.url;
                        }
                    }
                });
            }
        }
    </script>
@endsection
