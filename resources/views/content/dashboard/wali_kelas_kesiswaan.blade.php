@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template' . $ext . '/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                {{-- <div class="col-md-6">
                    <div class="card">
                        <div class="card-header bg-info">
                            <h5 class="box-title text-white m-0 text-center">
                                Grafik Data Pegawai
                            </h5>
                        </div>
                        <div class="card-body">
                            <center>
                                <div class="padded-reverse" id="chart" style="height: 370px; width: 100%;">
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body bg-info pb-0">
                                <div class="media d-flex">
                                    <div class="align-self-center">
                                        <i class="fas fa-users fa-6x float-left"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <h3 class="text-white">User Pegawai</h3>
                                        <span>Pegawai</span>
                                    </div>
                                </div>
                                <a href="{{ route('kepegawaian-pegawai') }}" class="text-center text-white">Lihat data
                                    pegawai <i class="fa-solid fa-circle-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body bg-purple pb-0">
                                <div class="media d-flex">
                                    <div class="align-self-center">
                                        <i class="fa-solid fa-users-gear fa-6x float-left text-white"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <h3 class="text-white">User Admin</h3>
                                        <span class="text-white">Pengguna</span>
                                    </div>
                                </div>
                                <a href="{{ route('kepegawaian-admin') }}" class="text-center text-white">Lihat data
                                    Pengguna <i class="fa-solid fa-circle-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div> --}}

            </div>
        </div>
    </main>

    <script>

    </script>
@endsection
