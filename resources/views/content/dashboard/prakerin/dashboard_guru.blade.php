@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Data Siswa</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Nama</th>
                                    <th class="text-center" rowspan="2">NIS</th>
                                    <th class="text-center" rowspan="2">NISN</th>
                                    <th class="text-center" rowspan="2">Email</th>
                                    <th class="text-center" rowspan="2">Telepon</th>
                                    <th class="text-center" colspan="3">Perusahaan</th>
                                </tr>
                                <tr class="bg-info">
                                    <th class="text-center">Industri</th>
                                    <th class="text-center">Pembimbing</th>
                                    <th class="text-center">Alamat</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($peserta))
                                    <tr>
                                        <td colspan="8" class="text-center">Data untuk saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($peserta as $ps)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($ps['nama']) }}</td>
                                            <td>{{ $ps['nis'] }}</td>
                                            <td>{{ $ps['nisn'] }}</td>
                                            <td>{{ $ps['email'] }}</td>
                                            <td>{{ $ps['telepon'] }}</td>
                                            <td>{{ $ps['industri'] }}</td>
                                            <td>{{ $ps['pemb_industri'] }}</td>
                                            <td>{{ $ps['alamat_industri'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                <tr class="bg-info">
                                    <td class="text-center font-weight-bold" colspan="8">Jumlah Siswa</td>
                                    <td class="text-center font-weight-bold">{{ count($peserta) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })
    </script>
@endsection
