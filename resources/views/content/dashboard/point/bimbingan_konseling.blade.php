@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title">Data Point Siswa</h5>
                            <hr>
                            <div class="tabs">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link btn-purple m-1 active" href="javascript:void(0)"
                                            onclick="return data_siswa('terbanyak')" data-toggle="tab">Point Terbanyak</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link btn-purple m-1" href="javascript:void(0)"
                                            data-toggle="tab" onclick="return data_siswa('berat')">Pelanggaran Berat</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link btn-purple m-1" href="javascript:void(0)"
                                            data-toggle="tab" onclick="return data_siswa('100')">Mendekati 100</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home-tab2">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="data-tabel">
                                                <thead>
                                                    <th style="width: 50%">Profil</th>
                                                    <th width="200">Rombel</th>
                                                    <th>Statistik Point</th>
                                                    <th>Aksi</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title">REKAPAN ABSENSI SISWA</h5>
                            <hr>
                            <form class="form-inline float-right my-2" id="formSearchSiswa">
                                <div class="form-group">
                                    <label for="inputPassword6">Rombel</label>
                                    <select name="id_rombel" id="id_rombel" class="form-control mx-sm-3">
                                        <option value="all" selected>Pilih Rombel..</option>
                                        @foreach ($all_jurusan as $aj)
                                            <optgroup label="{{ $aj['nama'] }}">
                                                @foreach ($aj['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rmb)
                                                    <option value="{{ $rmb['id'] }}">
                                                        {{ $rmb['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword6">Mapel</label>
                                    <select id="id_mapel" name="id_mapel" class="form-control mx-sm-3" disabled>
                                        <option value="">--Pilih Mapel--</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Nama</th>
                                            <th>NIS</th>
                                            <th>NISN</th>
                                        </tr>
                                    </thead>

                                    <tbody id="data-siswa">
                                        <tr>
                                            <td colspan="4" class="text-center">Silahkan masukan filter diatas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title">Mutasi Siswa</h5>
                                </div>
                                <div class="col-md-6">
                                    @if (session('role') != 'supervisor')
                                        <div class="float-right">
                                            <button class="btn btn-info mutasi_in"><i class="fa fa-plus-circle"></i> Mutasi
                                                Masuk</button>
                                            <a href="javascript:void(0)" class="btn btn-danger mutasi_out"><i
                                                    class="fa fa-times-circle"></i> Mutasi
                                                Keluar</a>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table widget-status-table table-bordered hover">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th class="text-center" rowspan="2">No</th>
                                                    <th class="text-center" rowspan="2">Nama Siswa</th>
                                                    <th class="text-center" rowspan="2">Kelas</th>
                                                    <th class="text-center" colspan="3" class="text-center">Keterangan
                                                    </th>
                                                    <th class="text-center" rowspan="2">Aksi</th>
                                                </tr>
                                                <tr class="bg-success">
                                                    <th class="text-center">Tanggal Keluar</th>
                                                    <th class="text-center">Tanggal Masuk</th>
                                                    <th class="text-center">Alasan</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tabel_mutasi">
                                                @if (!empty($mutasi))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($mutasi as $mt)
                                                        <tr>
                                                            <td class="text-center">{{ $no++ }}</td>
                                                            <td class="text-center">{{ ucwords($mt['nama']) }}</td>
                                                            <td class="text-center">{{ $mt['rombel'] }}</td>
                                                            <td class="text-center">
                                                                {{ $mt['jenis'] == 'keluar' ? $mt['tgl_mutasi'] : '-' }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ $mt['jenis'] == 'masuk' ? $mt['tgl_mutasi'] : '-' }}
                                                            </td>
                                                            <td class="text-center">{{ $mt['keterangan'] }}</td>
                                                            <td class="text-center">
                                                                <a href="#"><i
                                                                        class="material-icons list-icon md-18 text-success">info</i></a>
                                                                <a href="javascript:void(0)"
                                                                    onclick="editMutasi({{ $mt['id'] }})"><i
                                                                        class="material-icons list-icon md-18 text-info">edit</i></a>
                                                                <a href="javascript:void(0)"
                                                                    onclick="deleteMutasi({{ $mt['id'] }})"><i
                                                                        class="material-icons list-icon md-18 text-danger">backspace</i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="7" class="text-center">Maaf data saat ini kosong
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Statistik Siswa</h5>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2">Rombel</th>
                                    <th colspan="3" class="text-center">Jumlah Siswa</th>
                                </tr>
                                <tr>
                                    <th>Pria</th>
                                    <th>Perempuan</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $l = 0;
                                    $p = 0;
                                    $jm = 0;
                                @endphp
                                @foreach ($dashboard as $ds)
                                    <tr>
                                        <td>
                                            <b>{{ $ds['nama'] }}</b>
                                            <p class="m-0">{{ $ds['jurusan'] }}</p>
                                        </td>
                                        <td class="text-center">{{ $ds['l'] }}</td>
                                        <td class="text-center">{{ $ds['p'] }}</td>
                                        <td class="text-center">{{ $ds['jumlah'] }}</td>
                                    </tr>
                                    @php
                                        $l += $ds['l'];
                                        $p += $ds['p'];
                                        $jm += $ds['jumlah'];
                                    @endphp
                                @endforeach
                                <tr>
                                    <td class="text-center"><b>Total</b></td>
                                    <td class="text-center">{{ $l }}</td>
                                    <td class="text-center">{{ $p }}</td>
                                    <td class="text-center">{{ $jm }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Detail Pelanggaran</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-detail" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Kelas</th>
                                    <th>Tanggal Pelanggaran</th>
                                    <th>Pelanggaran</th>
                                    <th>Point</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxSiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingMutasiIn"></h5>
                </div>
                <form id="formSiswa" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <input type="hidden" name="id" id="id_jurusan">
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">NIK</label>
                                <input type="text" name="nik" class="form-control" id="nik">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">NIS</label>
                                <input type="text" name="nis" class="form-control" id="nis">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">NISN</label>
                                <input type="text" name="nisn" class="form-control" id="nisn">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Jenis Kelamin</label>
                                <select name="jenkel" class="form-control" id="jenkel">
                                    <option value="l">Laki - laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Agama</label>
                                <select name="agama" id="agama" class="form-control">
                                    <option value="kristen">Kristen</option>
                                    <option value="hindu">hindu</option>
                                    <option value="budha">budha</option>
                                    <option value="islam">islam</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Telepon</label>
                                <input type="text" name="telepon" class="form-control" id="telepon">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Email</label>
                                <input type="text" name="email" class="form-control" id="email">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Tanggal Lahir</label>
                                <input type="date" name="tgl_lahir" class="form-control" id="tgl_lahir">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Tahun Ajaran</label>
                                <input type="text" name="tahun_ajaran" class="form-control"
                                    value="{{ session('tahun') }}">
                            </div>
                            <div class="col-md-12 mb-2">
                                <label for="name" class=" control-label">Alamat</label>
                                <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Anak Ke</label>
                                <input type="text" name="anak_ke" class="form-control" id="anak_ke">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Status Keluarga</label>
                                <input type="text" name="status_keluarga" class="form-control" id="status_keluarga">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Tahun Angkatan</label>
                                <input type="text" name="tahun_angkatan" class="form-control" id="tahun_angkatan">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Kelas Diterima</label>
                                <select name="kelas_diterima" class="form-control" id="kelas_diterima">
                                    @foreach ($rombel as $rm)
                                        <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Tanggal Diterima</label>
                                <input type="date" name="tanggal_diterima" class="form-control" id="tanggal_diterima">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Nomor Ijazah</label>
                                <input type="text" name="no_ijazah" class="form-control" id="no_ijazah">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Nomor SKHUN</label>
                                <input type="text" name="no_skhun" class="form-control" id="no_skhun">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Tahun SKHUN</label>
                                <input type="text" name="tahun_skhun" class="form-control" id="tahun_skhun">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Nama Ayah</label>
                                <input type="text" name="nama_ayah" class="form-control" id="nama_ayah">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Pekerjaan Ayah</label>
                                <input type="text" name="pekerjaan_ayah" class="form-control" id="pekerjaan_ayah">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Nama Ibu</label>
                                <input type="text" name="nama_ibu" class="form-control" id="nama_ibu">
                            </div>
                            <div class="col-md-6 mb-2">
                                <label for="name" class=" control-label">Pekerjaan Ibu</label>
                                <input type="text" name="pekerjaan_ibu" class="form-control" id="pekerjaan_ibu">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Nama Wali</label>
                                <input type="text" name="nama_wali" class="form-control" id="nama_wali">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Telepon Wali</label>
                                <input type="text" name="telepon_wali" class="form-control" id="telepon_wali">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="name" class=" control-label">Pekerjaan Wali</label>
                                <input type="text" name="pekerjaan_wali" class="form-control" id="pekerjaan_wali">
                            </div>
                            <div class="col-md-12 mb-2">
                                <label for="name" class=" control-label">Alamat Wali</label>
                                <textarea name="alamat_wali" id="alamat_wali" rows="3" class="form-control"></textarea>
                            </div>
                            <div class="col-md-3 mb-2">
                                <label for="name" class=" control-label">Gambar Siswa</label>
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group" width="100%" style="margin-top: 10px">
                                <div id="delete_foto" style="text-align: center"></div>
                            </div>
                            <div class="col-md-12">
                                <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionMutasi" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalMutasiOut" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleMutasiOut"></h5>
                </div>
                <div style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="javascript:void(0)" method="post" id="formSearchMutasi">
                                <div class="row mt-3">
                                    <div class="col-lg-4 mb-1">
                                        <div class="m-input-icon m-input-icon--left">
                                            <label class="filter-col" style="margin-right:0;"
                                                for="pref-search">Jurusan:</label>
                                            <select name="id_jurusan" class="form-control" id="id_jurusan">
                                                <option value="">Semua Jurusan</option>
                                                @foreach ($jurusan as $jr)
                                                    <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-1">
                                        <label class="filter-col" style="margin-right:0;"
                                            for="pref-search">Kelas:</label>
                                        <select id="id_kelas" name="id_kelas" class="form-control" disabled></select>
                                    </div>
                                    <div class="col-lg-3 mb-1">
                                        <label class="filter-col" style="margin-right:0;"
                                            for="pref-orderby">Rombel:</label>
                                        <select id="id_rombel_search" name="id_rombel" class="form-control"
                                            disabled></select>
                                    </div>
                                    <div class="col-lg-2 mb-1">
                                        <label class="filter-col" style="margin-right:0;"
                                            for="pref-search">&nbsp;</label>
                                        <button type="submit" class="btn btn-outline-info btn-block"><i
                                                class="fab fa-searchengin"></i>
                                            Display</button>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <small class="text-danger text-center">*Harap pilih rombel dahulu untuk menampilkan
                                            daftar siswa</small>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NISN</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Rombel</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="data_siswa">
                                <tr>
                                    <td colspan="6" class="text-center">Harap pilih rombel terlebih dahulu untuk
                                        menampilkan daftar siswa</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEditMutasi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingEditMutasi"></h5>
                </div>
                <form id="formEditMutasi" name="formEditMutasi" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_mutasi">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jenis Mutasi</label>
                                    <div class="col-sm-12">
                                        <select name="jenis_mutasi" class="form-control" id="jenis_mutasi">
                                            <option value="">Pilih Jenis Mutasi</option>
                                            <option value="masuk">Masuk</option>
                                            <option value="keluar">Keluar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Mutasi</label>
                                    <div class="col-sm-12">
                                        <input type="date" name="tgl_mutasi" id="tgl_mutasi" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Rombel</label>
                                    <div class="col-sm-12">
                                        <select name="id_rombel" id="edit_id_rombel" class="form-control">
                                            <option value="">--Pilih Rombel--</option>
                                            @foreach ($rombel as $rm)
                                                <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tahun_ajaran" id="tahun_ajaran" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnUpdateMutasi"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var data_filter = 'terbanyak';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.nav-link').on('click', function() {
                console.log("remove classs");
                $('a.active').removeClass('active');
                $(this).addClass('active');
            });

            $('select[name="id_rombel"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('kesiswaan_absensi-mapel') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $("#id_mapel").html(
                                '<option value="">Loading...</option>');
                            $("#id_mapel").attr("disabled", true);
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_mapel"]').html(
                                    '<option value="">--- No Mapel Found ---</option>');
                            } else {
                                var s = '';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_mapel + '">' + row
                                        .mapel + '</option>';
                                    $("#id_mapel").attr("disabled", true);

                                })
                                $('select[name="id_mapel"]').removeAttr('disabled');
                            }
                            $('select[name="id_mapel"]').html(s)
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();

                $.ajax({
                    url: "{{ route('kesiswaan_absensi-get_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $("#data-siswa").html(
                            '<tr><td colspan="4" class="text-center">Sedang memproses data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        if (data.status) {
                            noti(data.icon, data.message);
                            $('#data-siswa').html(
                                '<tr><td colspan="4" class="text-center">Filter yang anda masukan bermasalah</td></tr>'
                            );
                        } else {
                            $('#data-siswa').html(data);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('.mutasi_in').click(function() {
                $('#ajaxSiswa').modal('show');
                $('#actionMutasi').val('Add');
                $('#headingMutasiIn').html('Mutasi Masuk');
            });

            $('.mutasi_out').click(function() {
                $('#modalMutasiOut').modal('show');
                $('#titleMutasiOut').html('Mutasi keluar');
            });


            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                if (id_jurusan) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id_jurusan
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">--Pilih Kelas--</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('#id_rombel_search').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel_search').html(
                                '<option value="">--Load data Rombel--</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel_search').removeAttr('disabled')
                            $('#id_rombel_search').html(s);
                        }
                    });
                }
            })

            $('body').on('submit', '#formSiswa', function(e) {
                e.preventDefault();
                var action_url = '';

                if ($('#actionMutasi').val() == 'Add') {
                    action_url = "{{ route('master_mutasi-tambah_masuk') }}";
                    method_url = "POST";
                }

                if ($('#actionMutasi').val() == 'Edit') {
                    action_url = "{{ route('master_mutasi-data_siswa') }}";
                    method_url = "POST";
                }
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#saveBtn').html('Memprosess..');
                        $("#saveBtn").attr("disabled", true);
                    },
                    success: function(data) {
                        // $('#data_siswa').html(data);
                        if (data.status == 'berhasil') {
                            $('#formSiswa').trigger("reset");
                            $('#ajaxSiswa').modal('hide');
                        }
                        $('.tabel_mutasi').html(data.mutasi);
                        $('#saveBtn').html('Simpan')
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                        // $('#saveBtn').html('Simpan');
                        // $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                })
            });



            $('#formSearchMutasi').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('master_mutasi-data_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data_siswa').html(
                            '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data_siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formEditMutasi').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('master_mutasi-update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#btnUpdateMutasi').html('Mengupdate...');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formEditMutasi').trigger("reset");
                            $('#modalEditMutasi').modal('hide');
                        }
                        $('.tabel_mutasi').html(data.mutasi);
                        noti(data.icon, data.message);
                        $('#btnUpdateMutasi').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpdateMutasi').html('Simpan');
                    }
                });
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('pelanggaran_siswa-datatable_siswa') }}",
                    data: function(d) {
                        d.filter_siswa = data_filter;
                    }
                },
                columns: [{
                        data: 'profile',
                        name: 'profile'
                    },

                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'statistik',
                        name: 'statistik'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },

                ]
            });

        });

        function data_siswa(filter_siswa) {
            console.log(filter_siswa);
            data_filter = filter_siswa;
            var oTable = $('#data-tabel').dataTable();
            oTable.fnDraw(false);
        }

        function tambahForm(obj) {
            $.ajax({
                url: "{{ route('master_mutasi-tambah') }}",
                method: "POST",
                data: $("#" + obj.id).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $(".tabel_mutasi").html(
                        '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                    );
                    $("#data_siswa").html(
                        '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                    );
                },
                success: function(data) {
                    console.log(data);
                    $('.tabel_mutasi').html(data.mutasi);
                    $('#data_siswa').html(data.search);
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }

        function editMutasi(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('master_mutasi-detail') }}",
                data: {
                    id
                },
                success: function(data) {
                    $('#HeadingEditMutasi').html('Edit Mutasi');
                    $('#id_mutasi').val(data.id);
                    $('#jenis_mutasi').val(data.jenis).trigger('change');
                    $('#tgl_mutasi').val(data.tgl_mutasi);
                    $('#edit_id_rombel').val(data.id_rombel).trigger('change');
                    $('#tahun_ajaran').val(data.tahun_ajaran);
                    $('#keterangan').val(data.keterangan);
                    $('#modalEditMutasi').modal('show');
                }
            });
        }

        function deleteMutasi(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('master_mutasi-soft_delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".tabel_mutasi").html(
                            '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        // if (data.status == 'berhasil') {
                        // }
                        $('.tabel_mutasi').html(data.mutasi);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }



        function detail(id) {
            if (id != '') {
                $('#data-detail').DataTable().destroy();
                fill_detail(id);
                $('#modalDetail').modal('show');
            }
        }

        function fill_detail(kelas_siswa = '') {
            var table = $('#data-detail').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('pelanggaran_siswa-detail_datatable') }}",
                    data: {
                        kelas_siswa
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nis',
                        name: 'nis'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'tgl_pelanggaran',
                        name: 'tgl_pelanggaran'
                    },
                    {
                        data: 'pelanggaran',
                        name: 'pelanggaran'
                    },
                    {
                        data: 'point',
                        name: 'point'
                    },
                ]
            });
        }
    </script>

@endsection
