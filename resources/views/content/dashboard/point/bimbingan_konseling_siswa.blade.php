@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif

@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">
                        {{ session('role') == 'siswa' ? 'Pelanggaran Kamu' : 'Pelanggaran dari Anak Anda' }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center" rowspan="2">No</th>
                                    <th class="text-center" colspan="3">Pelanggaran</th>
                                </tr>
                                <tr class="bg-info">
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Tanggal Pelanggaran</th>
                                    <th class="text-center">Point</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($pelanggaran))
                                @php
                                    $no = 1;
                                    $total = 0;
                                @endphp
                                    @foreach ($pelanggaran as $pg)
                                        <tr>
                                            <td class="text-center">{{ $no++ }}</td>
                                            <td class="text-center">{{ $pg['pelanggaran'] }}</td>
                                            <td class="text-center"> {{ (new \App\Helpers\Help())->getTanggal($pg['tgl_pelanggaran']) }}</td>
                                            <td class="text-center">{{ $pg['point'] }}</td>
                                        </tr>
                                        @php
                                             $total += $pg['point'];
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-center"><b>Total Point</b></td>
                                        <td class="text-center"><b>{{ $total }}</b></td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">Selamat, siswa belum pernah melakukan
                                            pelanggaran</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>

@endsection
