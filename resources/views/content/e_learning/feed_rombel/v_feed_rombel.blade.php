@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <style>
        .pace {
            display: none;
        }

        #loading {
            /* text-align: center; */
            background: url("{{ asset('asset/img/loading.gif') }}") no-repeat center;
            height: 290px;
            width: 290px;
            display: block;
            margin: auto;
            position: absolute;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            text-align: center;
            top: 48px;
        }

        .img-sm {
            width: 46px;
            height: 46px;
        }

        .panel {
            box-shadow: 0 2px 0 rgba(0, 0, 0, 0.075);
            border-radius: 0;
            border: 0;
            margin-bottom: 15px;
            padding-bottom: 10px;
        }

        .panel .panel-footer,
        .panel>:last-child {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .panel .panel-heading,
        .panel>:first-child {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .panel-body {
            padding: 25px 20px;
        }


        .media-block .media-left {
            display: block;
            float: left
        }

        .media-block .media-right {
            float: right
        }

        .media-block .media-body {
            display: block;
            overflow: hidden;
            width: auto
        }

        .middle .media-left,
        .middle .media-right,
        .middle .media-body {
            vertical-align: middle
        }

        .thumbnail {
            border-radius: 0;
            border-color: #e9e9e9
        }

        .tag.tag-sm,
        .btn-group-sm>.tag {
            padding: 5px 10px;
        }

        .tag:not(.label) {
            background-color: #fff;
            padding: 6px 12px;
            border-radius: 2px;
            border: 1px solid #cdd6e1;
            font-size: 12px;
            line-height: 1.42857;
            vertical-align: middle;
            -webkit-transition: all .15s;
            transition: all .15s;
        }

        .text-muted,
        a.text-muted:hover,
        a.text-muted:focus {
            color: #acacac;
        }

        .text-sm {
            font-size: 0.9em;
        }

        .text-5x,
        .text-4x,
        .text-5x,
        .text-2x,
        .text-lg,
        .text-sm,
        .text-xs {
            line-height: 1.25;
        }

        .btn-trans {
            background-color: transparent;
            border-color: transparent;
            color: #929292;
        }

        .btn-icon {
            padding-left: 9px;
            padding-right: 9px;
        }

        .btn-sm,
        .btn-group-sm>.btn,
        .btn-icon.btn-sm {
            padding: 5px 10px !important;
        }

        .comment {
            background: #e4e4e4;
        }

        .mar-top {
            margin-top: 15px;
        }

        .panel-body {
            background: #fff
        }

        .media-left,
        .media>.pull-left {
            padding-right: 10px;
        }


        @media (max-width:481px) {
            .pl-5, .px-5 {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

    </style>
    <div class="container bootdey">
        <div class="col-md-12 d-flex flex-column align-items-center bg-white">
            <h2 class="card-title mt-5" >Selamat Berdiskusi</h2>
            <p class="card-title">Tanyakan dan jawab berbagai pertanyaan</p>
        </div>
        <div class="col-md-12 bootstrap snippets bg-white">
            <div class="panel">
                <form action="javascript:void(0)" id="formMessage">
                    <div class="panel-body {{ session('media_template') == 'mobile' ? 'p-0' : 'px-5' }} ">
                        <textarea class="form-control" name="berita" rows="5" placeholder="What are you thinking?">
                            
                        </textarea>
                        <div class="mar-top clearfix">
                            <button class="btn btn-sm btn-success pull-right" type="submit" id="publish">
                                <i class="fa fa-pencil fa-fw"></i>
                                Kirim
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel">
                <div class="panel-body {{ session('media_template') == 'mobile' ? 'p-0' : 'px-5' }}" id="feeds">
                    @foreach ($feed as $fd)
                        <div class="media-block">
                            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture"
                                    src="{{ $fd['user_profil'] }}"></a>
                            <div class="media-body">
                                <div class="mar-btm">
                                    <a href="#"
                                        class="btn-link text-semibold media-heading box-inline">{{ ucwords($fd['pengirim']) }}.</a>
                                    <p class="text-muted text-sm"><i class="fa fa-user-circle-o fa-lg"></i>
                                        {{ $fd['role'] }} -
                                        {{ $fd['terbit'] }}
                                    </p>
                                </div>
                                <p>{{ $fd['berita'] }}.</p>
                                <div class="pad-ver d-flex">
                                    <!-- <a class="btn btn-sm btn-success btn-hover-primary mt-1" href="javascript:void(0)"
                                            onclick="addComment({{ $fd['id'] }})"><i class="fa fa-comment"></i> Comment
                                        </a> -->

                                    <button class="ml-2 btn btn-sm btn-transparent"
                                        data-target="#collapseExample_{{ $fd['id'] }}" data-toggle="collapse"
                                        href="#collapseExample_{{ $fd['id'] }}" role="button" aria-expanded="false"
                                        aria-controls="collapseExample_{{ $fd['id'] }}">
                                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                                        {{ count($fd['komentar']) }}
                                    </button>
                                </div>
                                <hr>
                                <div class="collapse" id="collapseExample_{{ $fd['id'] }}">

                                    <div id="dataKomentar_{{ $fd['id'] }}">
                                        @if (!empty($fd['komentar']))
                                            @foreach ($fd['komentar'] as $km)
                                                <div class="media-block">
                                                    <a class="media-left" href="#"><img class="img-circle img-sm"
                                                            alt="Profile Picture" src="{{ $km['user_profil'] }}"></a>
                                                    <div class="media-body">
                                                        <div class="mar-btm">
                                                            <a href="#"
                                                                class="btn-link text-semibold media-heading box-inline">{{ ucwords($km['nama']) }}</a>
                                                            <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i>
                                                                -
                                                                From
                                                                Mobile -
                                                                {{ $km['terbit'] }}o</p>
                                                        </div>
                                                        <p>{{ $km['komentar'] }}.</p>
                                                        <hr>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="ml-3 mb-5">
                                            <form class="p-3 comment">
                                                <div class="form-group mb-2">
                                                    <label for="exampleInputEmail1">Komentar</label>
                                                    <input type="text" class="form-control"
                                                        id="InputKomentar_{{ $fd['id'] }}"
                                                        placeholder="Tulis komentar anda">
                                                </div>
                                                <a onclick="submitComment({{ $fd['id'] }})" id="submitComment"
                                                    class="btn btn-sm btn-success text-white">Balas</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-between mb-3 mr-2 px-3">
                    {{ $feed->links() }}
                    <form method="GET">
                        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                        <select name="paginate" id="paginate" onchange="this.form.submit()">
                            <option value="">-item-</option>
                            <option value="4">4</option>
                            <option value="8">8</option>
                            <option value="10">10</option>
                        </select>
                    </form>
                </div>
            </div>





        </div>
    </div>
    <div class="modal modal-color-scheme bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="addComment" class="form-horizontal">
                    @csrf
                    <div class="modal-body" id="isiFeed" style="min-height: 337px;">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#paginate').change(function() {
                console.log("gg");
            })

            $('#formMessage').on('submit', function(event) {
                event.preventDefault();
                var actionType = $('#btn-save').val();
                $('#publish').html('Sending..');

                $.ajax({
                    url: "{{ route('feed_rombel-store') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);
                        $('#feeds').prepend(data).show('slow');
                        $('#publish').html('<i class="fa fa-pencil fa-fw"></i> Share');
                        $('#formMessage').trigger("reset");
                        noti('success', "Postingan berhasil ditambahkan");
                    },
                    error: function(data){
                        console.log(data);
                    },
                });
            });

        });

        function addComment(id) {
            $('#isiFeed').html('<div id="loading" style="" ></div>');
            $('#ajaxModel').modal('show');
            $.ajax({
                type: 'POST',
                url: "{{ route('feed_rombel-detail') }}",
                data: {
                    id
                },
                success: function(data) {
                    $('#isiFeed').html(data);
                }
            });
        }

        $('#addComment').on('submit', function(event) {
            event.preventDefault();
            var actionType = $('#btn-save').val();
            $('#tambahKomentar').html('Sending..');
            console.log($(this).serialize());
            $.ajax({
                url: "{{ route('feed_rombel-store_comment') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {

                        // $('#here').load('#here'));
                        // $( "#here" ).load(window.location.href + " #here" );
                        $('#dataKomentar_' + data.id_feed).load(location.href + ' #dataKomentar_' + data
                            .id_feed);
                        $('#ajaxModel').modal('hide');
                        $('#addComment').trigger("reset");
                    }
                    $('#tambahKomentar').html('Post comment');
                    noti(data.icon, data.success);
                    // console.log(data);
                    // $('#feeds').prepend(data).show('slow');

                    // $('#formMessage').trigger("reset");
                    // noti('success', "Postingan berhasil ditambahkan");
                },
            });
        });

        function submitComment(id) {
            event.preventDefault();
            $('#submitComment').html('Sending..');
            $.ajax({
                url: "{{ route('feed_rombel-store_comment') }}",
                method: "POST",
                data: {
                    'id_feed': id,
                    //blm dinamis
                    'comment': $('#InputKomentar_' + id).val(),
                    'Authorization': `Bearer {{ Session::get('token') }}`
                },
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        console.log("berhasil");
                        $('#dataKomentar_' + data.id_feed).load(location.href + ' #dataKomentar_' + data
                            .id_feed);
                        $('#tambahKomentar').html('Post comment');
                        $('#inputKomentar').trigger("reset");
                        $('#submitComment').html('Kirim');
                        console.log(data);
                    }

                    noti(data.icon, data.success);
                },
            });
        };
    </script>
@endsection
