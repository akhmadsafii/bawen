<table class="table table-bordered">
    <tbody>
        <tr>
            <td>
                <table class="table table-hover" border="1" width="100%">
                    <tbody>
                        <tr>
                            <td colspan="4" style="text-align: center">
                                <b>DATA ABSENSI</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center">
                                <b>MAPEL {{ strtoupper($profil['mapel']) }}</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px"></td>
                            <td style="width: 100px"></td>
                            <td style="width: 100px"></td>
                            <td style="width: 100px"></td>
                        </tr>
                        <tr>
                            <td><b>NAMA SISWA</b></td>
                            <td colspan="3" style="text-align: left"><b>: {{ strtoupper($profil['nama']) }}</b></td>
                        </tr>
                        <tr>
                            <td><b>NIS</b></td>
                            <td colspan="3" style="text-align: left"><b>: {{ $profil['nis'] }}</b></td>
                        </tr>
                        <tr>
                            <td><b>ROMBEL</b></td>
                            <td colspan="3" style="text-align: left"><b>: {{ strtoupper($profil['rombel']) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border: 2px solid black">
                    <tr>
                        <th style="text-align: center; width: 100px"><b>Pertemuan</b></th>
                        <th style="text-align: center; width: 100px"><b>Absensi</b></th>
                        <th style="text-align: center; width: 100px"><b>Masuk</b></th>
                        <th style="text-align: center; width: 100px"><b>Status</b></th>
                    </tr>
                    @if (!empty($absensi))
                        @foreach ($absensi as $ab)
                            <tr>
                                <td style="text-align: center">{{ $ab['pertemuan'] }}</td>
                                <td style="text-align: center">{{ $ab['absensi'] }}</td>
                                <td style="text-align: center">{{ $ab['masuk'] }}</td>
                                <td style="text-align: center">{{ $ab['status'] }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center">Saat ini belum tersedia</td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>

    </tbody>
</table>
