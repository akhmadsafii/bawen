@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="row">
        <!-- Events List -->
        <div class="col-lg-3 d-none d-lg-flex widget-holder widget-full-height widget-no-padding">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="fullcalendar-events" data-toggle="fullcalendar-events" data-target="#fullcalendar-1">
                        <div class="m-3">
                            <button class="btn btn-lg btn-danger btn-block ripple fc-add-event text-uppercase fs-16"><span>Add
                                    New Event</span>
                            </button>
                        </div>
                        <h5 class="box-title pr-4 pl-4 mt-3">Month Planner</h5>
                        <div class="fc-events">
                            <div class="fc-event bg-info"><span class="fc-event-text">My Event 1</span> <i
                                    class="material-icons color-info">bookmark</i>
                            </div>
                            <div class="fc-event bg-warning"><span class="fc-event-text">My Event 2</span> <i
                                    class="material-icons color-warning">bookmark</i>
                            </div>
                            <div class="fc-event bg-danger"><span class="fc-event-text">My Event 3</span> <i
                                    class="material-icons color-danger">bookmark</i>
                            </div>
                            <div class="fc-event bg-color-scheme"><span class="fc-event-text">My Event 4</span> <i
                                    class="material-icons color-color-scheme">bookmark</i>
                            </div>
                        </div>
                        <div class="checkbox checkbox-primary pr-4 pl-4">
                            <label>
                                <input type="checkbox" id="drop-remove"> <span class="label-text">remove after drop</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 widget-holder widget-no-padding">
            <div class="widget-bg-transparent">
                <div class="widget-body clearfix">
                    <div class="fullcalendar" id="fullcalendar-1" data-toggle="fullcalendar"
                        data-plugin-options='{ "events" : "assets/js/events-sample.json"}'></div>
                </div>
            </div>
        </div>
    </div>

@endsection
