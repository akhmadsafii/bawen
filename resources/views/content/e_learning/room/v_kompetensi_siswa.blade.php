@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        button.btn.btn-success {
            margin-left: 3px;
            background-color: #51d2b7;
        }

        input#l8 {
            width: -webkit-fill-available;
        }

        button.delete.btn.btn-danger.btn-sm {
            margin-top: 4px;
        }

        a.edit.btn.btn-info.btn-sm.edit {
            margin-top: 4px;
        }

        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .cart {
            padding: 30px;
            background-color: transparent
        }

        a.btn.btn-xs.btn-success {
            margin-right: 3px;
        }

    </style>
    <div class="row" style="padding: 12px;">

        {{-- <div class="col-md-12">
            <div class="alert alert-warning" style="color: #000">
                <b>Petunjuk : </b><br>
                <ul>
                    <li>Menu ini digunakan untuk menginput nilai pengetahuan pada mata pelajaran <b><i>Bahasa Indonesia,
                                kelas IX a.</i></b> </li>
                    <li>Jika kompetensi dasar belum ada, silakan klik tombol <b><i>Tambah KD</i></b>. Untuk mengubah atau
                        menghapus nama KD, silakan klik tombol "<i class="fa fa-pencil"></i>" atau "<i
                            class="fa fa-remove"></i>". </li>
                    <li>Untuk mengisikan nilai pengetahuan pada masing-masing KD, silakan klik nama KD, dan akan muncul
                        daftar siswa serta isian nilai. Nilai dalam <b><i>skala 1-100</i></b>. Jangan lupa klik tombol
                        <b><i>Simpan</i></b> di sebelah bawah.</li>
                </ul>
            </div>
        </div> --}}
        {{-- <div class="col-md-12" style="padding: 13px;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="http://raport.mysch.web.id/view_mapel" class="btn btn-info"><i class="fa fa-arrow-left"></i>
                        Kembali</a>
                    <a href="http://raport.mysch.web.id/n_pengetahuan/cetak/10" class="btn btn-warning" target="_blank"><i
                            class="fa fa-print"></i> Cetak</a>
                    <a href="http://raport.mysch.web.id/n_pengetahuan/import/6-7" class="btn btn-danger"><i
                            class="fa fa-download"></i> Download File Excel</a>
                    <a href="http://raport.mysch.web.id/n_pengetahuan/upload/6-7" class="btn btn-success"><i
                            class="fa fa-upload"></i> Upload File Excel</a>
                </div>
            </div>
        </div> --}}
        <div class="col-md-6">
            @php
                $num = 1;
            @endphp
            @if (empty($pengetahuan))
                <div class="card-body cart">
                    <div class="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png"
                            width="130" height="130" class="img-fluid mb-4 mr-3">
                        <h3><strong>Kompetensi Pengetahuan is Empty</strong></h3>
                        <h4>Add something to make me happy :)</h4>
                    </div>
                </div>
            @endif
            @foreach ($pengetahuan as $peng)
                <div class="card">
                    <div class="header">
                        <h5 class="title">{{ $num++ }}. Nilai {{ $peng['jenis_kompetensi'] }} &raquo;
                            {{ $peng['pelajaran'] . ' - ' . $peng['kelas'] }}</h5>
                    </div>
                    <div class="content">
                        <ul class="list-group" id="list_kd">
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Kompetensi Inti :</p>
                                <p class="mb-0">{{ $peng['kompetensi_inti'] }}</p>
                            </li>
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Indikator :</p>
                                <p class="mb-0">{{ $peng['indikator'] }}</p>
                            </li>
                        </ul>
                        <p style="margin-top: 1.42857em;">
                        <h5> <b>Kompetensi Dasar</b> </h5>
                        </p>
                        <ul class="list-group" id="list_kd">
                            <div id="list_kd_{{ $peng['id'] }}" style="margin-bottom: 10px">
                                @foreach ($peng['kompetensi_dasar'] as $dasar_peng)
                                    <li class="list-group-item">
                                        {{ $dasar_peng['nama'] }}
                                    </li>
                                @endforeach
                            </div>

                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-6">
            @php
                $no = 1;
            @endphp
            @if (empty($keterampilan))
                <div class="card-body cart">
                    <div class="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png"
                            width="130" height="130" class="img-fluid mb-4 mr-3">
                        <h3><strong>Kompetensi Ketrampilan is Empty</strong></h3>
                        <h4>Add something to make me happy :)</h4>
                    </div>
                </div>
            @endif
            @foreach ($keterampilan as $ktrmpln)
                <div class="card">
                    <div class="header">
                        <h5 class="title">{{ $no++ }}. Nilai {{ $ktrmpln['jenis_kompetensi'] }} &raquo;
                            {{ $ktrmpln['pelajaran'] . ' - ' . $ktrmpln['kelas'] }}</h5>
                    </div>
                    <div class="content">
                        <ul class="list-group" id="list_kd">
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Kompetensi Inti :</p>
                                <p class="mb-0">{{ $ktrmpln['kompetensi_inti'] }}</p>
                            </li>
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Indikator :</p>
                                <p class="mb-0">{{ $ktrmpln['indikator'] }}</p>
                            </li>
                        </ul>
                        <p style="margin-top: 1.42857em;">

                        <h5> <b>Kompetensi Dasar</b> </h5>
                        </p>
                        <ul class="list-group" id="list_kd">
                            <div id="list_kd_{{ $ktrmpln['id'] }}" style="margin-bottom: 10px">
                                @foreach ($ktrmpln['kompetensi_dasar'] as $dasar_ktrm)
                                    <li class="list-group-item">
                                        {{ $dasar_ktrm['nama'] }}
                                    </li>
                                @endforeach
                            </div>

                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
