@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn {
            margin-top: 6px;
        }

        #label {
            background-color: #11af49;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            border-radius: 0.3rem;
            cursor: pointer;
            margin-top: 1rem;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

        @media (max-width:481px) {
            .col-lg-9 {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table id="data-tabel" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr class="bg-info">
                        <th>NO</th>
                        <th>Nama</th>
                        {{-- <th>KD</th> --}}
                        <th>Keterangan</th>
                        <th>Batas Pengumpulan</th>
                        <th>File</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div id="peringatan">

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id_tugas_ketrampilan" id="id_tugas_ketrampilan">
                                <input required type="file" id="actual-btn" class="margin" type="file" name="image"
                                    accept="*" hidden />
                                <label for="actual-btn" id="label">Pilih File</label>
                                <span id="file-chosen">No file chosen</span>
                                <div class="form-group">
                                    <textarea name="keterangan" id="keterangan" cols="30" rows="5" placeholder="Keterangan"
                                        class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Hasil Kerja Saya</h5>
                </div>
                <div class="modal-body">
                    <span id="trd"></span>
                    <table class="table table-stripped" id="tabel-hasil" style="width: 100%">
                        <thead>
                            <tr class="bg-success">
                                <th>#</th>
                                <th>Waktu Pengumpulan</th>
                                <th>Keterangan</th>
                                <th>Status Tugas</th>
                                <th>File & Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalInstruksi" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Detail Insftruksi</h5>
                </div>
                <div class="modal-body">
                    <div id="hasil_instruksi">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        id_tugas = 0;
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({

                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },

                    {
                        data: 'status_kumpul',
                        name: 'status_kumpul'
                    },
                ]
            });

            var tabel_jawaban = $('#tabel-hasil').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "jawaban_ketrampilan/tugas_siswa",
                    "method": "POST",
                    "data": function(d) {
                        d.id_tugas = id_tugas;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'pengumpulans',
                        name: 'pengumpulans'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'status_kumpul',
                        name: 'status_kumpul'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    }
                ]
            });

            const actualBtn = document.getElementById('actual-btn');
            const fileChosen = document.getElementById('file-chosen');
            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            })

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $('#file-chosen').html('No file chosen');
                $.ajax({
                    type: 'POST',
                    url: "tugas_ketrampilan/edit",
                    data: {
                        id
                    },
                    success: function(data) {
                        $('#modelHeading').html("Upload Tugas");
                        $('#id_tugas_ketrampilan').val(data.id);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#saveBtn').val("edit-user");
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $(document).on('click', '.hasil', function() {
                id_tugas = $(this).data('id');
                tabel_jawaban.ajax.reload().draw();
                $('#hasilModel').modal('show');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var formData = new FormData(this);
                $.ajax({
                    url: "jawaban_ketrampilan/store",
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.detailInstruksi ', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "tugas_ketrampilan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-eye"></i>');
                        $('#hasil_instruksi').html(data.keterangan);
                        $('#modalInstruksi').modal('show');

                    }
                });
            });

            $(document).on('click', '.upload_tugas', function() {
                let id = $(this).data('id');
                let status_tugas = $(this).data('status');
                let loader = $(this);
                $('#form_result').html('');
                $('#file-chosen').html('No file chosen');
                $.ajax({
                    type: 'POST',
                    url: "tugas_ketrampilan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fa fa-upload"></i>');
                        $('#modelHeading').html("Upload Tugas");
                        $('#id_tugas_ketrampilan').val(data.id);
                        var today = new Date();

                        var date = today.getFullYear() + '-' + ("0" + (today.getMonth() + 1)).slice(-2) + '-' +
                            today.getDate();

                        var time = today.getHours() + ":" + today.getMinutes() + ":" + today
                            .getSeconds();

                        var dateTime = date + ' ' + time;
                        console.log(dateTime + ' pengumpulan ' + data.maks_pengumpulan);
                        let status = 'info';
                        let informasi =
                            'Anda sudah mengumpulan jawaban, apa anda yakin ingin menambahkan jawaban lagi?'
                        if (status_tugas == 0) {
                            status = 'danger';
                            informasi = 'Anda Belum Mengumpulkan Jawaban Sama sekali'
                        }
                        if (dateTime > data.maks_pengumpulan) {
                            let peringatan = confirm(
                                'saat ini batas pengumpulan telah berakhir, apa anda yakin ingin melanjutkan proses?'
                            );
                            if (peringatan) {
                                $('#ajaxModel').modal('show');
                                $('#peringatan').html(
                                    '<div class="alert alert-danger border-danger" role="alert"><p class="mb-1"><strong>Informasi Tugas:</strong></p><ul class="mr-t-10"><li class="text-' +
                                    status + '"><strong>Status : </strong>' + informasi +
                                    '</li><li>Pengumpulan jawaban sudah berakhir pada ' +
                                    data.format_pengumpulan + '</li></ul></div>');
                            }
                        } else {
                            $('#ajaxModel').modal('show');
                            $('#peringatan').html(
                                '<div class="alert alert-success border-success" role="alert"><p class="mb-1"><strong>Informasi Tugas:</strong></p><ul class="mr-t-10"><li class="text-' +
                                status + '"><strong>Status : </strong>' + informasi +
                                '</li><li>Pengumpulan jawaban akan berakhir pada ' + data
                                .format_pengumpulan +
                                '</li><li>Harap Kumpulkan sebelum tanggal tersebut ^_^</li></ul></div>'
                            );
                        }

                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "jawaban_ketrampilan/trash",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $("#tabel-hasil").dataTable().fnDraw()
                                $("#data-tabel").dataTable().fnDraw()
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

        });
        $(".wysihtml5-sandbox").hide()
    </script>
@endsection
