@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <h2>Tes</h2>

    <div class='table-responsive'>
        <table class='table table-bordered text-center table-striped' width='100%'>
            <thead class='bg-primary text-white'>
                <tr class=''>
                    <th rowspan='2'>KD</th>
                    <th colspan='3'>ASPEK YANG DIANALISIS (BOBOT)</th>
                    <th rowspan='2'>KKM</th>
                </tr>
                <tr>
                    <th>KOMPLEKSITAS</th>
                    <th>DAYA DUKUNG</th>
                    <th>INTAKE PESERTA DIDIK</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>KD.1</td>
                    <td>
                        <select class='form-control'>
                            <option>1 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>3 = Rendah</option>
                        </select>
                    </td>
                    <td>
                        <select class='form-control'>
                            <option>3 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>1 = Rendah</option>
                        </select>
                    </td>
                    <td>
                        <select class='form-control'>
                            <option>3 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>1 = Rendah</option>
                        </select>
                    </td>
                    <td>78</td>
                </tr>
                <tr>
                    <td>KD.2</td>
                    <td>
                        <select class='form-control'>
                            <option>1 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>3 = Rendah</option>
                        </select>
                    </td>
                    <td>
                        <select class='form-control'>
                            <option>3 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>1 = Rendah</option>
                        </select>
                    </td>
                    <td>
                        <select class='form-control'>
                            <option>3 = Tinggi</option>
                            <option>2 = Sedang</option>
                            <option>1 = Rendah</option>
                        </select>
                    </td>
                    <td>78</td>
                </tr>
                <tr>
                    <td colspan='4' class='text-left'>KRITERIA KETUNTASAN MINIMAL (KKM) MATA PELAJARAN</td>
                    <td>78</td>
                </tr>
            </tbody>
        </table>

        <table width='100%' class='table table-striped text-center'>
            <thead class='bg-primary text-white'>
                <th>INTERVAL PREDIKAT</th>
                <th>PREDIKAT</th>
            </thead>
            <tbody>
                <tr>
                    <td>93-100</td>
                    <td>A</td>
                </tr>
                <tr>
                    <td>86-92</td>
                    <td>B</td>
                </tr>
                <tr>
                    <td>79-85</td>
                    <td>C</td>
                </tr>
                <tr>
                    <td>
                        <78< /td>
                    <td>D
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
