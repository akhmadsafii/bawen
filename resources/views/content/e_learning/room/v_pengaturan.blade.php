@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <h2>Pengaturan Ruang Kelas</h2>

    <form>
        <div class='form-group'>
            <label>Kelas</label>
            <input type='text' class='form-control'>
        </div>
        <div class='form-group'>
            <label>Nama Room</label>
            <input type='text' class='form-control'>
        </div>
        <div class='form-group'>
            <label>Deskripsi</label>
            <textarea class='form-control'></textarea>
        </div>
        <div class='form-group'>
            <label>Mata Pelajaran</label>
            <input type='text' class='form-control'>
        </div>
        <div class='form-group'>
            <label>Agenda Pertemuan</label>
            <input type='text' class='form-control'>
        </div>
        <input type='submit' value='Simpan' class='button button-rounded'>
        <button class='button button-red'>Reset Setting</button>
    </form>
@endsection
