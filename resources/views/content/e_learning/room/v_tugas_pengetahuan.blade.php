@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn {
            margin-top: 6px;
        }

        #label {
            background-color: #11af49;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            border-radius: 0.3rem;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table id="data-tabel" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr class="bg-info">
                        <th>No</th>
                        <th>Pertemuan</th>
                        {{-- <th>KD</th> --}}
                        <th>Instruksi</th>
                        <th>Batas Pengumpulan</th>
                        <th>File</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_tugas_pengetahuan">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pertemuan ke</label>
                                    <div class="col-md-9">
                                        <select name="pertemuan" id="pertemuan" class="form-control">
                                            @for ($i = 1; $i <= 100; $i++)
                                                <option value="{{ $i }}">Pertemuan ke {{ $i }}
                                                </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Kompetensi Inti</label>
                                    <div class="col-md-9">
                                        <select name="kompetensi_inti" id="kompetensi_inti" class="form-control">
                                            <option value="">-- Pilih Kompetensi Inti --</option>
                                            @foreach ($kompetensi as $item)
                                                <option value="{{ $item['id'] }}">{{ $item['kompetensi_inti'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Kompetensi Dasar</label>
                                    <div class="col-md-9">
                                        <div id="kd_select">
                                            <small style="color: red">*Harap pilih Kompetensi Inti terlebih dahulu</small>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Instruksi</label>
                                    <div class="col-md-9">
                                        <textarea name="instruksi" id="instruksi" class="form-control editor"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Batas Pengumpulan</label>
                                    <div class="col-md-5">
                                        <input type="date" name="tgl_maks_pengumpulan" id="tgl_maks_pengumpulan"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="time" name="waktu_maks_pengumpulan" id="waktu_maks_pengumpulan"
                                            class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">File Tugas</label>
                                    <div class="col-md-9">
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModal" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Hasil Kerja Siswa</h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <table class="table table-bordered">
                        <style>
                            .table-bordered thead tr th,
                            #tbd tr td {
                                text-align: center;
                            }

                        </style>
                        <thead>
                            <tr>
                                <th>Siswa</th>
                                <th>Kelas</th>
                                <th>Rombel</th>
                                <th>File</th>
                                <th>Keterangan</th>
                                <th>Tanggal Pengumpulan</th>
                            </tr>
                        </thead>
                        <tbody id="tbd">

                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Hasil Kerja Siswa Tugas Pengetahuan</h5>
                </div>
                <div class="modal-body">
                    <span id="trd"></span>
                    <table class="table table-stripped table-bordered" id="tabel-hasil" style="width: 100%">
                        <thead>
                            <tr class="bg-success">
                                <th>#</th>
                                <th>NISN</th>
                                <th>Nama Siswa</th>
                                <th>Waktu Pengumpulan</th>
                                <th>Keterlambatan</th>
                                <th>File & Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalInstruksi" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Detail Insftruksi</h5>
                </div>
                <div class="modal-body">
                    <div id="hasil_instruksi">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
        integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
    <script type="text/javascript">
        id_tugas = 0;
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            editor();

            $('#table-hasil').DataTable()

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis'
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    // {
                    //     data: 'kd',
                    //     name: 'kd'
                    // },
                    {
                        data: 'instruksi',
                        name: 'instruksi'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var tabel_jawaban = $('#tabel-hasil').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: {
                    "url": "jawaban_pengetahuan/get_by_tugas",
                    "method": "POST",
                    "data": function(d) {
                        d.id_tugas = id_tugas;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'siswa',
                        name: 'siswa'
                    },
                    {
                        data: 'dikumpul',
                        name: 'dikumpul'
                    },
                    {
                        data: 'terlambat',
                        name: 'terlambat'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    }
                ]
            });

            const actualBtn = document.getElementById('actual-btn');

            const fileChosen = document.getElementById('file-chosen');

            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            })

            $(document).on('click', '.hasil', function() {
                id_tugas = $(this).data('id');
                tabel_jawaban.ajax.reload().draw();
                $('#hasilModel').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#file-chosen').html('');
                $('#Customer_id').val('');
                $('.select2').val(null).trigger('change');
                $('#kd_select').html(
                    '<small style="color: red">*Harap pilih Kompetensi Inti terlebih dahulu</small>'
                );
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Tugas Pengetahuan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });



            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "tugas_pengetahuan/store";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "tugas_pengetahuan/update";
                    method_url = "POST";
                }
                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');

                $.ajax({
                    type: 'POST',
                    url: "tugas_pengetahuan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Tugas Pengetahuan");
                        $('#id_tugas_pengetahuan').val(data.id);
                        $('#pertemuan').val(data.pertemuan).trigger("change");
                        $('#tgl_maks_pengumpulan').val(data.tgl_pengumpulan);
                        $('#waktu_maks_pengumpulan').val(data.waktu_pengumpulan);
                        var s2 = $('#skema_penilaian');
                        var vals = data.skema;
                        vals.forEach(function(key) {
                            if (!s2.find('option:contains(' + key + ')').length)
                                s2.append($('<option>').text(key));
                        });
                        s2.val(vals).trigger("change");
                        tinyMCE.activeEditor.setContent(data.instruksi);
                        $('#file-chosen').html(data.nama_file);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#saveBtn').val("edit-user");
                        $('#ajaxModel').modal('show');

                    }
                });
            });
            $(document).on('click', '.tampil_jawaban', function() {
                var id = $(this).data('id');
                $('#form_result').html('');

                $.ajax({
                    type: 'POST',
                    url: "tugas_pengetahuan/hasil",
                    data: {
                        id_pengetahuan: id
                    },
                    success: function(data) {
                        $('#modelHeading').html("Edit Data Penilaian");
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#saveBtn').val("edit-user");
                        $("#tbd").html(data);
                        $('#hasilModal').modal('show');
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "tugas_pengetahuan/trash",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $("#data-tabel").dataTable().fnDraw()
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.detailInstruksi ', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "tugas_pengetahuan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-eye"></i>');
                        $('#hasil_instruksi').html(data.detail_instruksi);
                        $('#modalInstruksi').modal('show');

                    }
                });
            });

        });

        $('select[name="kompetensi_inti"]').on('change', function() {
            var id_kompetensi = $(this).val();
            if (id_kompetensi) {
                $.ajax({
                    url: "standar_kompetensi/load",
                    type: "POST",
                    data: {
                        id_kompetensi_inti: id_kompetensi
                    },
                    beforeSend: function() {
                        $("#kd_select").html('<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        var s = '';
                        $.each(data, function(i, v) {
                            s += '<input type="checkbox" name="kompetensi_dasar[]" id="kompetensi_dasar" value="' +
                                v.id + '">' + v.nama + '<br>';
                        });
                        $('#kd_select').html(s)
                    }
                });
            }
        })


        function edit_dasar(id_kompetensi_inti, kompetensi_dasar) {
            $.ajax({
                url: "standar_kompetensi/get_inti",
                type: "POST",
                data: {
                    id_kompetensi_inti: id_kompetensi_inti,
                    kompetensi_dasar: kompetensi_dasar
                },
                beforeSend: function() {
                    $("#kd_select").html('<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(fb) {
                    console.log(fb);
                    $('#kd_select').html('');
                    $('#kd_select').html(fb);
                }
            });
            return false;
        }

        function input_nilai(id, params) {
            let text;
            let person = prompt("Silahkan masukan nilai:", params);
            if (person) {
                $.ajax({
                    url: "jawaban_pengetahuan/save_nilai",
                    type: "POST",
                    data: {
                        id,
                        nilai: person
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            var oTable = $('#tabel-hasil').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }

        function editor() {
            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "200",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');
                    input.onchange = function() {

                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                        tinymce.triggerSave();
                    };

                    input.click();
                }
            });
        }
    </script>
@endsection
