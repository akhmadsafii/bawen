@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        #calendar {
            margin-bottom: 12px;
        }

        .pace {
            display: none;
        }

        .fc-content {
            background: black !important;
            border-color: black !important;
        }

        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            .fc-scroller.fc-day-grid-container {
                height: max-content !important;
            }

            .widget-heading.clearfix {
                padding-left: 23px;
            }

            h5 {
                font-size: 21px !important;
                font-weight: 600 !important;
            }
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
        type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <div class="widget-heading clearfix">
        <h5>{{ Session::get('title') }}</h5>
    </div>
    <hr>
    <div class="row p-4">
        <div id="calendar"></div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div style="width: 100%;">
                        <div class="table-responsive" style="margin-top: 14px;">
                            <table class="table table-striped" id="data-trash" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Judul</th>
                                        <th>Pembuat</th>
                                        <th>Role</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="kalendarModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleKalendar"></h5>
                </div>
                <form action="javascript:void(0)" id="formUpdateKalender">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 my-2">
                                <label for="" class="form-label">Agenda/Kegiatan</label>
                                <input type="text" name="title" id="title" class="form-control" required>
                            </div>
                            <div class="col-md-12 my-2">
                                <label for="" class="form-label">Link URL</label>
                                <input type="url" name="link" id="link" class="form-control"
                                    placeholder="https://drive.google.com/1NOFVZym1j">
                                <small class="text-info">*Kosongi jika tidak diperlukan</small>
                            </div>
                            <input type="hidden" id="action" value="add">
                            <input type="hidden" name="id" id="id_kalender">
                            <input type="hidden" name="start" id="start">
                            <input type="hidden" name="end" id="end">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-update"><i class="fa fa-plus-circle"></i>
                            Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "",
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['start'],
                                    end: value['end'],
                                    title: value['title'],
                                    link: value['link'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.link != null) {
                        element.children().last().append(
                            "<br><a href='" + event.link +
                            "' target='_blank'><span class='link_url'>Link URL : " +
                            event
                            .link +
                            "</span></a><br><a href='javascript:void(0)' class='text-info edit' data-id='" +
                            event.id +
                            "'><i class='fas fa-pencil-alt'></i></a>&nbsp;&nbsp;<a href='javascript:void(0)' data-id='" +
                            event.id +
                            "' class='delete text-danger'><i class='fas fa-trash'></i></a>");
                    } else {
                        element.children().last().append(
                            "<br><a href='javascript:void(0)' class='text-info edit' data-id='" +
                            event.id +
                            "'><i class='fas fa-pencil-alt'></i></a>&nbsp;&nbsp;<a href='javascript:void(0)' data-id='" +
                            event.id +
                            "' class='delete text-danger'><i class='fas fa-trash'></i></a>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#action').val('add');
                    $('#titleKalendar').html('Tambah Agenda');
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#kalendarModal').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'kalendar_kelas/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id,
                        type: "PUT",
                        success: function(data) {
                            noti(data.icon, data.message)
                        }
                    });
                },
            });

            $('#formUpdateKalender').on('submit', function(event) {
                event.preventDefault();
                $(".btn-update").html(
                    '<i class="fa fa-spin fa-spinner"></i>');
                $(".btn-update").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'add') {
                    action_url = "kalendar_kelas/store";
                    method_url = "POST";
                }

                if ($('#action').val() == 'edit') {
                    action_url = "kalendar_kelas/update";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formUpdateKalender').trigger("reset");
                            $('#kalendarModal').modal('hide');
                            $('#calendar').fullCalendar('refetchEvents');
                        }
                        noti(data.icon, data.message);
                        $('.btn-update').html('<i class="fa fa-plus-circle"></i> Tambah');
                        $(".btn-update").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "kalendar_kelas/trash",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#calendar').fullCalendar('refetchEvents');
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-backspace"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "kalendar_kelas/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleKalendar').html("Edit Agenda");
                        $('#id_kalender').val(data.id);
                        $('#start').val(data.start);
                        $('#end').val(data.end);
                        $('#title').val(data.title);
                        $('#link').val(data.link);
                        $('#kalendarModal').modal('show');
                        $('#action').val('edit')
                    }
                });
            });

        });
    </script>
@endsection
