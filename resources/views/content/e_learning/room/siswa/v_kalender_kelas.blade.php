@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        #calendar {
            margin-bottom: 12px;
        }

        .pace {
            display: none;
        }

        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            .fc-scroller.fc-day-grid-container {
                height: max-content !important;
            }

            .widget-heading.clearfix {
                padding-left: 23px;
            }

            h5 {
                font-size: 21px !important;
                font-weight: 600 !important;
            }
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
        type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <div class="widget-heading clearfix">
        <h5>{{ Session::get('title') }}</h5>
    </div>
    <hr>
    <div class="row p-4">
        <div id="calendar"></div>
    </div>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "",
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['start'],
                                    end: value['end'],
                                    title: value['title'],
                                    link: value['link'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: false,
                eventRender: function(event, element, view) {
                    if (event.link != null) {
                        element.children().last().append(
                            "<br><a href='" + event.link +
                            "' target='_blank'><span class='link_url'>Link URL : " +
                            event
                            .link +
                            "</span></a>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },

            });




        });
    </script>
@endsection
