@if (empty($jawaban))
    <tr>
        <td colspan="6" style="text-align: center;">Data kosong</td>
    </tr>
@endif
@foreach ($jawaban as $item)
    <tr>
        <td>{{ $item['nama'] }}</td>
        <td>{{ $item['kelas'] }}</td>
        <td>{{ $item['rombel'] }}</td>
        <td>{{ $item['nama'] }}</td>
        <td>{{ $item['keterangan'] }}</td>
        <td>{{ date('d-m-Y', strtotime($item['pengumpulan'])) }}</td>
    </tr>
@endforeach
