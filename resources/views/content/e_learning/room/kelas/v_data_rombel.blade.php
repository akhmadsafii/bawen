@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif

@if (session('role') == 'admin' || session('role') == 'learning-admin')
    @php
        $ext = '_default';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            /* margin-top: 1.14286em; */
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

    </style>
    <div class="row">
        @if (session('role') == 'learning-admin' || session('role') == 'admin' )
            <div class="col-lg-12">
                @include('includes.program-e_learning.room.menu-room_admin')
            </div>
            <div class="col-lg-12">
                @yield('content_rombel')
            </div>
        @else
            <div class="sidebar col-md-3 col-12 col-sm-4">
                @include('includes.program-e_learning.room.menu-rombel')
            </div>

            <div class="col-md-9 col-12 col-sm-8">
                @yield('content_rombel')
            </div>
        @endif

    </div>

@endsection
