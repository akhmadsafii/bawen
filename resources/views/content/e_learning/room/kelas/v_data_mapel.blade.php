@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <style>
        h3.display-3,
        h6 {
            color: #fff;
        }

        .card-guru {
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
        }

        .card-guru:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        button.text-success {
            background: transparent;
            width: 100%;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
        }

        .left {
            display: none !important;
        }

    </style>
    @if (!empty($mapel))
        @if (session('media_template') == 'mobile')
            <div class="section mt-3 mb-3">
                <div class="row justify-content-center">
                    @foreach ($mapel as $item)
                        <div class="col-9 col-md-3 mb-2">
                            <div class="card">
                                <img src="{{ asset('asset/mobile/img/sample/photo/wide4.jpg') }}" class="card-img-top"
                                    alt="image">
                                <div class="card-body">
                                    @php
                                        $klas = '';
                                    @endphp
                                    @if ($item['room_status'] == 1)
                                        @php
                                            $klas = 'primary';
                                        @endphp
                                        <h6 class="card-subtitle">Kelas Dibuka</h6>
                                    @else
                                        <h6 class="card-subtitle">Kelas Berakhir</h6>
                                        @php
                                            $klas = 'danger';
                                        @endphp
                                    @endif
                                    <h5 class="card-title">{{ $item['mapel'] }}</h5>
                                    <a href="{{ url('program/e_learning/room', $item['id_room_encode']) }}"
                                        class="btn btn-{{ $klas }}">
                                        Masuk
                                        <ion-icon name="arrow-forward-outline"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="row">
                @foreach ($mapel as $item)
                    <div class="col-md-3 mt-2">
                        <div class="card card-guru">
                            <div
                                class="card-header bg-{{ $item['room_status'] == 1 ? 'success' : 'primary' }} text-white">
                                <div class="row align-items-center">
                                    <div class="col col-12" style="">
                                        <i class="fas fa-chalkboard-teacher fa-4x pull-right"></i>
                                    </div>
                                    <div class="col col-12">
                                        <h5 class="text-light text-center mt4">{{ $item['mapel'] }}</h5>
                                        <p class="text-light text-center">Kelas
                                            {{ $item['room_status'] == 1 ? 'Dibuka' : 'Ditutup' }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="{{ url('program/e_learning/room', $item['id_room_encode']) }}"
                                    data-id="{{ $item['id_room_encode'] }}"
                                    class="masuk btn btn-outline-{{ $item['room_status'] == 1 ? 'success' : 'primary' }} btn-block">Masuk
                                    ke
                                    kelas</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            </div>
        @endif
    @else
        <div class="card-body cart">
            <div class="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png" width="130"
                    height="130" class="img-fluid mb-4 mr-3">
                <h3><strong>Your Data is Empty</strong></h3>
                <h4>Add something to make me happy :)</h4>
            </div>
        </div>
    @endif
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Mapel</th>
                                        <th>Pertemuan</th>
                                        <th>Absensi</th>
                                        <th>Waktu Masuk</th>
                                        <th>AKSI</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var id_rooms = 0;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        action: function(e, dt, button, config) {
                            window.location = 'rombel-trash';
                        },
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('get_pertemuan_by_room') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.id_room = id_rooms;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'absensi',
                        name: 'absensi'
                    },
                    {
                        data: 'masuk',
                        name: 'masuk'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '.profil', function() {
                id_rooms = $(this).data('id');
                table.ajax.reload().draw();
                $('#ajaxModel').modal('show');
            });
        </script>

    @endsection
