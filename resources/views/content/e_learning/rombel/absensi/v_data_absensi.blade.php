@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header row mx-0 bg-success">
                <div class="col-md-7">
                    <h2 class="box-title text-white">{{ Session::get('title') }}</h2>

                </div>
                <div class="col-md-5">
                    <form class="navbar-form" id="searchMapel" onsubmit="searchTugas()">
                        <div class="input-group">
                            <select name="id_mapel" {{ empty($mapel) ? 'disabled' : '' }} class="form-control"
                                id="id_mapel">
                                @if (empty($mapel))
                                    <option value="">Kelas anda Belum set guru pelajaran</option>
                                @else
                                    <option value="">Pilih Pelajaran...</option>
                                    @foreach ($mapel as $mpl)
                                        <option value="{{ $mpl['id_mapel'] }}">
                                            {{ $mpl['guru'] . ' | ' . $mpl['mapel'] }}</option>
                                    @endforeach
                                @endif

                            </select>
                            <div class="input-group-btn">
                                <button type="submit" id="fil" class="btn btn-info"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="informasi">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Siswa</th>
                                            <th>NIS</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Detail Absensi Kehadiran</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mapel</th>
                                    <th>Pertemuan</th>
                                    <th>Absensi</th>
                                    <th>Masuk</th>
                                    {{-- <th>Aksi</th> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var mapel = 0;
        var id_kelas_siswa;
        var id_mapel;
        var id_rombel;
        var id_ta;

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "absensi/datatable",
                    "method": "POST",
                    "data": function(d) {
                        d.id_mapel = mapel;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nis',
                        name: 'nis'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>',
                buttons: [{
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('wali_kelas-absensi-detail_absensi') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.id_mapel = id_mapel;
                        d.id_kelas_siswa = id_kelas_siswa;
                        d.id_rombel = id_rombel;
                        d.id_ta = id_ta;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'masuk',
                        name: 'masuk'
                    },
                    {
                        data: 'absensi',
                        name: 'absensi'
                    },
                    // {
                    //     data: 'action',
                    //     name: 'action',
                    //     orderable: false,
                    //     searchable: false
                    // },
                ]
            });

            $(document).on('click', '.edit', function() {
                id_kelas_siswa = $(this).data('id_kelas_siswa');
                id_mapel = $(this).data('id_mapel');
                id_rombel = $(this).data('id_rombel');
                id_ta = $(this).data('id_ta');
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#searchMapel').on('submit', function(event) {
                event.preventDefault();
                mapel = $('#id_mapel').val();
                $('#data-tabel').DataTable().ajax.reload(null, false);
            });
        })
    </script>
@endsection
