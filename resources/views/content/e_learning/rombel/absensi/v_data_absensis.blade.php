@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        button.delete {
            background: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Mapel</th>
                        <th>Nama Guru</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="detail_table">
                        <tr>
                            <th>No</th>
                            <th>Pertemuan</th>
                            <th>Hadir</th>
                            <th>Tidak Hadir</th>
                            <th>Aksi</th>
                        </tr>
                        <tbody id="bodytable">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'Copy'
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Print'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel'
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF',
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'colvis'
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'mapel_kelas',
                        name: 'mapel_kelas'
                    },
                    {
                        data: 'guru',
                        name: 'guru'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });



            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Siswa");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-siswa_wali_kelas') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-siswa_wali_kelas') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        $.toast({
                            icon: 'success',
                            text: data.success,
                            hideAfter: 5000,
                            showConfirmButton: false,
                            position: 'bottom-right',
                        });

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.tampil', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'absensi/detail/' + id, function(data) {
                    $("#bodytable").empty();
                    $('#modelHeading').html("Detail Absensi");
                    $('#saveBtn').val("edit-user");
                    $('#detail_table tr').not(':first').not(':last').remove();
                    var html = '';
                    for (var i = 0; i < data.length; i++) {
                        html += '<tr>' +
                            // '<td>' + a + '</td>' +
                            '<td>' + data[i].pertemuan + '</td>' +
                            '<td>' + data[i].pertemuan + '</td>' +
                            '<td>' + data[i].hadir + '</td>' +
                            '<td>' + data[i].tidak_hadir + '</td>' +
                            '<td><a href="javascript:void(0)" data-id="' + data[i].pertemuan +
                            '" class="detail_pertemuan"><i class="material-icons list-icon md-18 text-muted">remove_red_eye</i></a></td>' +
                            '</tr>';
                    }
                    // }
                    $('#bodytable').html(html);
                    $('#ajaxModel').modal('show');
                });
            });

            $(document).on('click', '.detail_pertemuan', function() {
                var id = $(this).data('id');
                // console.log(id);
                $('#form_result').html('');
                $.get("" + 'absensi/detail_pertemuan/' + id, function(data) {
                    var url = "" + 'absensi/detail_pertemuan/' + id;
                    // window.open(url, '_blank');
                    window.open(url, "_blank",
                        "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=700,height=600"
                        );
                    // $("#bodyPertemuan").empty();
                    // $('#modelHeading').html("Detail Absensi");
                    // $('#ajaxModel').modal('hide');
                    // $('#detail_table tr').not(':first').not(':last').remove();
                    // var html = '';
                    // for(var i = 0; i < data.length; i++){
                    //     html += '<tr>'+
                    //         '<td>' + data[i].id + '</td>' +
                    //         '<td>' + data[i].nama + '</td>' +
                    //         '<td>' + data[i].absensi + '</td>' +
                    //         '<td>' + data[i].masuk + '</td>' +
                    //         // '<td><a href="javascript:void(0)" data-id="' + data[i].pertemuan + '" class="detail_pertemuan"><i class="material-icons list-icon md-18 text-muted">remove_red_eye</i></a></td>' +
                    //     '</tr>';
                    // }   
                    // $('#bodyPertemuan').html(html);
                    // $('#ajaxPertemuan').modal('show');
                });
            });
        });

        function deleteData(id) {
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ url('program/e_learning/room/jurnal_guru/trash/') }}" + '/' + id,
                            type: "POST",
                            data: {
                                '_method': 'DELETE'
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    'Proses Penghapusan Gagal :)',
                                    'error'
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }

    </script>
@endsection
