@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header row mx-0 bg-success">
                <div class="col-md-7">
                    <h2 class="box-title text-white">{{ Session::get('title') }}</h2>

                </div>
                <div class="col-md-5">
                    <form class="navbar-form" id="searchMapel" onsubmit="searchTugas()">
                        <div class="input-group">
                            <select name="id_mapel" {{ empty($mapel) ? 'disabled' : '' }} class="form-control"
                                id="id_mapel">
                                @if (empty($mapel))
                                    <option value="">Kelas anda Belum set guru pelajaran</option>
                                @else
                                    <option value="">Pilih Pelajaran...</option>
                                    @foreach ($mapel as $mpl)
                                        <option value="{{ $mpl['id_mapel'] }}">
                                            {{ $mpl['guru'] . ' | ' . $mpl['mapel'] }}</option>
                                    @endforeach
                                @endif

                            </select>
                            <div class="input-group-btn">
                                <button type="submit" id="fil" class="btn btn-info"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- <div class="col-md-4" style="margin-bottom: 15px">
                        <div class="card">
                            <ul class="list-group list-group-flush" id="list_kd">
                                @foreach ($mapel as $mpl)
                                    <li class="list-group-item" onclick="return view_kd({{ $mpl['id_mapel'] }});"> <i
                                            class="fa fa-chevron-right"></i> {{ $mpl['guru'] . ' | ' . $mpl['mapel'] }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-md-12" id="informasi">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Keterangan</th>
                                            <th>File Tugas</th>
                                            <th>Hasil</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Hasil Kerja Siswa Tugas Ketrampilan</h5>
                </div>
                <div class="modal-body">
                    <span id="trd"></span>
                    <table class="table table-stripped" id="tabel-hasil" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NISN</th>
                                <th>Nama Siswa</th>
                                <th>File</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var mapel = 0;
        id_tugas = 0;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $('#list_kd li').on('click', function() {
            //     $('li.active').removeClass('active');
            //     $(this).addClass('active');
            // });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fas fa-sync"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        action: function(e, dt, button, config) {
                            window.location = 'rombel-trash';
                        },
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "ket_datatable",
                    "method": "POST",
                    "data": function(d) {
                        d.id_mapel = mapel;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },
                    {
                        data: 'jawaban',
                        name: 'jawaban',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var tabel_jawaban = $('#tabel-hasil').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: {
                    "url": "get_by_tugas_ketrampilan",
                    "method": "POST",
                    "data": function(d) {
                        d.id_tugas = id_tugas;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'siswa',
                        name: 'siswa'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    }
                ]
            });

            $(document).on('click', '.hasil', function() {
                id_tugas = $(this).data('id');
                tabel_jawaban.ajax.reload().draw();
                $('#hasilModel').modal('show');
            });

            $('#searchMapel').on('submit', function(event) {
                event.preventDefault();
                mapel = $('#id_mapel').val();
                $('#data-tabel').DataTable().ajax.reload(null, false);
            });
        })

    </script>

@endsection
