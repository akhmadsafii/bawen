@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
<style>
    .col-lg-9 {
        background: #f2f4f8 !important;
    }
</style>
<div class="row" style="background: #fff; margin-bottom: 12px; padding:10px">
    <h3 class="box-title">{{Session::get('title')}}</h3>
</div>
<hr>
<div class="row">
    <div class="col-7" style="background: #fff">
        <div class="row" style="padding: 10px;">
            <div class="col-5">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="contact-info">
                            <header class="text-center">
                                <figure class="inline-block user--online thumb-lg">
                                    <img src="assets/demo/user-cards/6.jpg" class="rounded-circle img-thumbnail" alt="">
                                </figure>
                                <h4 class="mt-1"><a href="#">Emmy Wilson</a></h4>
                                <div class="contact-info-address"><i class="material-icons list-icon">location_on</i>
                                    <p>Charlotte, NC</p>
                                </div>
                            </header>
                            <footer class="clearfix"><a href="#" class="btn btn-success btn-rounded"><i class="material-icons list-icon">done</i>	Perbarui Profil</a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-7">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td style="font-weight: 700; padding-bottom: 10px;"><h3 style="margin:0px">{{ $guru['nama'] }}</h3></td>
                            <td>&nbsp;</td>
                            <td style="text-align: right; font-weight: 700; padding-bottom: 10px;"></td>
                        </tr>
                        <tr>
                            <td style="font-weight: 700; padding-bottom: 10px;"><div id="show_kelas"></div></td>
                            <td>&nbsp;</td>
                            <td style="text-align: center; font-weight: 700; padding-bottom: 10px;">{{ $guru['status'] }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left; padding: 10px 10px 10px 0px; border-top: 3px solid #eee;" colspan="2">
                            <b>NIP</b>
                            </td>
                            <td style="width: 20%; text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 3px solid #eee;">{{ $guru['nip'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="2">
                            <b>Jenis Kelamin</b>
                            </td>
                            <td style="text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;">{{ $guru['jenkel'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="2">
                            <b>Tempat lahir</b>
                            </td>
                            <td style="text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;">{{ $guru['tempat_lahir'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="2">
                            <b>Agama</b>
                            </td>
                            <td style="text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;"> {{ $guru['agama'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="3">
                            <b>Alamat</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px" colspan="3">{{ $guru['alamat'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="2">
                            <b>Tahun Masuk</b> 
                            </td>
                            <td style="text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;">{{ $guru['tahun_masuk'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;" colspan="2">
                            <b>Telepon</b>
                            </td>
                            <td style="text-align: center; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;">{{ $guru['telepon'] }}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 10px 0px; border-top: 1px solid #eee;">
                            <b>Informasi lain</b> 
                            </td>
                            <td style="text-align: center; padding: 10px 10px; border-top: 1px solid #eee;"></td>
                            <td style="text-align: right; padding: 10px 0px 10px 10px; white-space: nowrap; border-top: 1px solid #eee;"></td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px" id="show_informasi" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px" colspan="3">{{ $guru['alamat'] }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-1"></div>
    <div class="col-4">
        <div class="widget-bg" style="background: #fff; padding-top:0.42857em">
            <div class="widget-body clearfix" style="padding:0.42857em">
                
                <div class="padded-reverse" style="float: left;">
                    <h5 class="box-title">Jadwal Mengajar</h5>                
                </div>
                <div class="padded-reverse" style="float: right">
                    <a href="javascript:void(0)" class="btn btn-sm btn-facebook" id="createNewCustomer"><i class="fa fa-plus"></i> Tambah Data</a>
                </div>
            </div>
        </div>
        <div class="widget-bg" style="background: #fff; padding-top:0.42857em; margin-top:23px">
            <div class="widget-body clearfix" style="padding:0.42857em">
                
                <div class="padded-reverse" style="float: left;">
                    <h5 class="box-title">Akun</h5>                
                </div>
                <div class="padded-reverse" style="float: right">
                    <a href="javascript:void(0)" class="btn btn-sm btn-facebook" id="createNewCustomer"><i class="fa fa-plus"></i> Perbarui</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection