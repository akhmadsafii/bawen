@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-bg">
        <h3 class="box-title">{{ Session::get('title') }}</h3>
        <hr>
        <div id='calendar'></div>
    </div>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: "kalender_rombel",
                displayEventTime: false,
                eventColor: '#21bd9d',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    if (title) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                        var event = {
                            title: title,
                            start: start,
                            end: end
                        };
                        $.ajax({
                            url: "kalender_rombel/store",
                            data: event,
                            type: "POST",
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    noti(data.icon, data.success)
                                    $('#calendar').fullCalendar('refetchEvents');
                                } else {
                                    noti(data.icon, data.success)
                                }
                            }
                        });
                    }
                },
                eventDrop: function(event, delta) {
                    console.log(event);
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'kalender_rombel/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id,
                        type: "PUT",
                        success: function(response) {
                            if (data.status == 'berhasil') {
                                noti(data.icon, data.success)
                            } else {
                                noti(data.icon, data.success)
                            }
                        }
                    });
                },
                eventClick: function(event) {
                    var deleteMsg = confirm("Do you really want to delete?");
                    if (deleteMsg) {
                        $.ajax({
                            type: "DELETE",
                            url: 'kalender_rombel/trash',
                            data: "&id=" + event.id,
                            success: function(response) {
                                if (response['status'] == "berhasil") {
                                    // console.log("tes");
                                    $('#calendar').fullCalendar('removeEvents', event.id);
                                    noti(response.icon, response.message)
                                } else {
                                    noti(response.icon, response.message)
                                }
                            }
                        });
                    }
                }
            });
        });
    </script>
@endsection
