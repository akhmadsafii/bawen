@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        button.delete {
            background: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Mapel</th>
                        <th>Nama Mapel</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_room">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width:50%">
                                            <select name="status" id="status" class="form-control">
                                                <option value="1">Aktif</option>
                                                <option value="2">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Nama Mapel</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="id_mapel_kelas" id="id_mapel_kelas">
                                        <div class="input-group" style="width:50%">
                                            <select name="mapel" id="mapel" class="form-control">
                                                <option value="">---Pilih Pelajaran---</option>
                                                @foreach ($mapel_kelas as $item)
                                                    <option value="{{ $item['id_mapel'] }}">{{ $item['mapel'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama Guru</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width:50%">
                                            <select name="id_guru" id="id_guru" class="form-control"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'Copy'
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Print'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel'
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF',
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'colvis'
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'kode_pelajaran',
                        name: 'kode_pelajaran'
                    },
                    {
                        data: 'mapel',
                        name: 'mapel'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Siswa");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('walikelas_store-guru_mapel') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('walikelas_update-guru_mapel') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'guru_mapel/edit/' + id, function(data) {
                    console.log(data);
                    $('#modelHeading').html("Edit Data Siswa");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#status').val(data.status_kode).trigger("change");
                    $('#mapel').val(data.id_mapel).trigger("change");
                    $('#id_guru').val(data.id_guru).trigger("change");
                    $('#id_room').val(data.id_room);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                });
            });
        });

        function deleteData(id) {
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ url('program/e_learning/wali_kelas/guru_mapel/trash/') }}" + '/' + id,
                            type: "POST",
                            data: {
                                '_method': 'DELETE'
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    data.message,
                                    data.success
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    data.message,
                                    data.success
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }


        $('select[name="mapel"]').on('change', function() {
            var id_mapel = $(this).val();
            if (id_mapel) {
                $('select[name="id_guru"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ url('program/e_learning/wali_kelas/guru_mapel/mapel/') }}" + '/' + id_mapel,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        var s = '<option value="">---select---</option>';
                        data.forEach(function(row) {
                            console.log(row)
                            s += '<option value="' + row.id_guru + '">' + row.guru +
                            '</option>';
                            $('#id_mapel_kelas').val(row.id_mapel_kelas)

                        })
                        $('select[name="id_guru"]').removeAttr('disabled')
                        $('select[name="id_guru"]').html(s)
                    }
                });
            }
        })

    </script>
@endsection
