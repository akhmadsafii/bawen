@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header row mx-0 bg-success">
                <div class="col-md-7">
                    <h2 class="box-title text-white">{{ Session::get('title') }}</h2>

                </div>
                <div class="col-md-5">
                    <form class="navbar-form" id="searchMapel" onsubmit="searchTugas()">
                        <div class="input-group">
                            <select name="id_mapel" {{ empty($mapel) ? 'disabled' : '' }} class="form-control"
                                id="id_mapel">
                                @if (empty($mapel))
                                    <option value="">Kelas anda Belum set guru pelajaran</option>
                                @else
                                    <option value="">Pilih Pelajaran...</option>
                                    @foreach ($mapel as $mpl)
                                        <option value="{{ $mpl['id_mapel'] }}">
                                            {{ $mpl['mapel'] . ' | ' . $mpl['guru'] }}</option>
                                    @endforeach
                                @endif

                            </select>
                            <div class="input-group-btn">
                                <button type="submit" id="fil" class="btn btn-info"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- <div class="col-md-3">
                        <table class="table table-bordered filter" style="background-color: #ffbba2;">
                            <tr>
                                <th colspan="2">
                                    Pilih Mata Peljaran
                                </th>
                            </tr>
                            <tr>
                                <th>Mapel</th>
                                <th>
                                    <select name="id_mapel" id="id_mapel" class="form-control">
                                        @foreach ($pelajaran as $pel)
                                            <option value="{{ $pel['id_mapel'] }}">{{ $pel['mapel'] }}</option>
                                        @endforeach
                                    </select>
                                </th>
                            </tr>
                        </table>
                    </div> --}}
                    <div class="col-md-12">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Pertemuan</th>
                                            <th>Judul</th>
                                            <th>Guru</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var mapel = $('#id_mapel').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fas fa-sync"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        action: function(e, dt, button, config) {
                            window.location = 'rombel-trash';
                        },
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: {
                    "url": "bahan_ajar/get_datatable",
                    "method": "POST",
                    "data": function(d) {
                        d.id_mapel = mapel;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal_indo',
                        name: 'tanggal_indo'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'judul',
                        name: 'judul'
                    },
                    {
                        data: 'guru',
                        name: 'guru'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },
                ]
            });

            $('#searchMapel').on('submit', function(event) {
                event.preventDefault();
                mapel = $('#id_mapel').val();
                table.ajax.reload().draw();
            });
        })
    </script>


@endsection
