@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-12 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NISN</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($siswa))
                            <tr>
                                <td colspan="5" class="text-center">Maaf data anda saat ini kosong</td>
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($siswa as $sw)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $sw['nisn'] }}</td>
                                    <td>{{ ucwords($sw['nama']) }}</td>
                                    <td>{{ $sw['jenkel'] }}</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm accordion-toggle"
                                            data-toggle="collapse" data-target="#demo{{ $sw['id'] }}"><i
                                                class="fas fa-info-circle"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="hiddenRow">
                                        <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                            <table class="table">
                                                <tr class="bg-info">
                                                    <th colspan="5"><b><i class="fas fa-user-alt"></i> Profile Siswa</b>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>Nama</th>
                                                    <td>{{ ucwords($sw['nama']) }}</td>
                                                    <td></td>
                                                    <th>Email</th>
                                                    <td>{{ $sw['email'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>No Induk</th>
                                                    <td>{{ $sw['nik'] }}</td>
                                                    <td></td>
                                                    <th>Jurusan</th>
                                                    <td>{{ $sw['jurusan'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Telepon</th>
                                                    <td>{{ $sw['telepon'] }}</td>
                                                    <td></td>
                                                    <th>Jenis Kelamin</th>
                                                    <td>{{ $sw['jenkel'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Agama</th>
                                                    <td>{{ $sw['agama'] }}</td>
                                                    <td></td>
                                                    <th>Nomor Ijazah</th>
                                                    <td>{{ $sw['no_ijazah'] }}
                                                </tr>
                                                <tr>
                                                    <th>Tempat Lahir</th>
                                                    <td>{{ $sw['tempat_lahir'] }}
                                                    </td>
                                                    <td></td>
                                                    <th>Tanggal Lahir</th>
                                                    <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_lahir']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Terakhir Login</th>
                                                    <td>{{ (new \App\Helpers\Help())->getTanggalLengkap($sw['last_login']) }}
                                                    </td>
                                    </td>
                                    <td></td>
                                    <th>Tahun Angkatan</th>
                                    <td>{{ $sw['tahun_angkatan'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td colspan="4">{{ $sw['alamat'] }}
                                    </td>
                                </tr>
                </table>
            </div>
            </td>
            </tr>
            @endforeach
            @endif
            </tbody>

            </table>
        </div>
    </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
@endsection
