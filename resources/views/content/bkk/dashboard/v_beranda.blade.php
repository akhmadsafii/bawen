@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet"
        type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-5 widget-holder">
                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel"
                            data-slick='{"slidesToShow":1, "autoplay": true, "infinite": true, "speed": 2000, "fade": true, "dots": true, "infinite": true }'>
                            <div>
                                <img src="{{ asset('asset/demo/carousel/carousel-10.jpeg') }}" alt="">
                                <div class="carousel-caption single-item-caption d-none d-sm-block">
                                    <div class="single-item-caption-inside">
                                        <h6>Inspiration and Design</h6>
                                        <h3>A Piece of Green</h3>
                                        <p class="text-muted">Posted: May 14, 2017</p><a href="#"
                                            class="btn btn-outline-info btn-rounded">View Post</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src="{{ asset('asset/demo/carousel/carousel-11.jpeg') }}" alt="">
                                <div class="carousel-caption single-item-caption d-none d-sm-block">
                                    <div class="single-item-caption-inside">
                                        <h6>LifeStyle</h6>
                                        <h3>Balanced</h3>
                                        <p class="text-muted">Posted: May 14, 2017</p><a href="#"
                                            class="btn btn-outline-info btn-rounded">View Post</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src="{{ asset('asset/demo/carousel/carousel-12.jpeg') }}" alt="">
                                <div class="carousel-caption single-item-caption d-none d-sm-block">
                                    <div class="single-item-caption-inside">
                                        <h6>Travel</h6>
                                        <h3>A Trip to Unknown</h3>
                                        <p class="text-muted">Posted: May 14, 2017</p><a href="#"
                                            class="btn btn-outline-info btn-rounded">View Post</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src="{{ asset('asset/demo/carousel/carousel-13.jpeg') }}" alt="">
                                <div class="carousel-caption single-item-caption d-none d-sm-block">
                                    <div class="single-item-caption-inside">
                                        <h6>Inspiration and Design</h6>
                                        <h3>A Piece of Green</h3>
                                        <p class="text-muted">Posted: May 14, 2017</p><a href="#"
                                            class="btn btn-outline-info btn-rounded">View Post</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src="{{ asset('asset/demo/carousel/carousel-14.jpeg') }}" alt="">
                                <div class="carousel-caption single-item-caption d-none d-sm-block">
                                    <div class="single-item-caption-inside">
                                        <h6>Lifestyle</h6>
                                        <h3>Whatever it takes.</h3>
                                        <p class="text-muted">Posted: May 14, 2017</p><a href="#"
                                            class="btn btn-outline-info btn-rounded">View Post</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 widget-holder">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body" style="padding: 12px">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h4 class="text-warning">20</h4>
                                        <h6 class="text-muted m-b-0">All Earnings</h6>
                                    </div>
                                    <div class="col-4 text-end" style="padding-right: 0; padding-left: 0">
                                        <i class="fa fa-pie-chart" style="font-size: 52px;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-warning py-3">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white" style="margin-bottom: 0">33% change</p>
                                    </div>
                                    <div class="col-3 text-end">
                                        <i class="fa fa-line-chart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body" style="padding: 12px">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h4 class="text-success">20</h4>
                                        <h6 class="text-muted m-b-0">Lowongan</h6>
                                    </div>
                                    <div class="col-4 text-end" style="padding-right: 0; padding-left: 0">
                                        <i class="fa fa-pie-chart" style="font-size: 52px;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-success py-3">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white" style="margin-bottom: 0">33% change</p>
                                    </div>
                                    <div class="col-3 text-end">
                                        <i class="fa fa-line-chart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body" style="padding: 12px">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h4 class="text-danger">20</h4>
                                        <h6 class="text-muted m-b-0">Perusahaan</h6>
                                    </div>
                                    <div class="col-4 text-end" style="padding-right: 0; padding-left: 0">
                                        <i class="fa fa-pie-chart" style="font-size: 52px;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-danger py-3">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white" style="margin-bottom: 0">33% change</p>
                                    </div>
                                    <div class="col-3 text-end">
                                        <i class="fa fa-line-chart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body" style="padding: 12px">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h4 class="text-info">20</h4>
                                        <h6 class="text-muted m-b-0">Lulusan</h6>
                                    </div>
                                    <div class="col-4 text-end" style="padding-right: 0; padding-left: 0">
                                        <i class="fa fa-pie-chart" style="font-size: 52px;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-info py-3">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white" style="margin-bottom: 0">33% change</p>
                                    </div>
                                    <div class="col-3 text-end">
                                        <i class="fa fa-line-chart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12" style="margin-top: 12px;">
                        <div class="card">
                            <div class="card-header bg-info">
                                Informasi
                            </div>
                            <div class="card-body">
                                <div class="alert alert-dismissable alert-info">
                                    <h3>Ketentuan Pendaftaran</h3>

                                    <p><i class="fa fa-square"></i> Bagi lulusan SMK yang siap bekerja silahkan menghubungi
                                        BKK SMK saudara untuk pembuatan akun BKK Online/ Daftar langsung melalui menu
                                        pendaftaran.</p>
                                    <br>
                                    <p><a class="btn btn-info" href="https://bkkonline.sumbarprov.go.id/signup/daftar"><i
                                                class="fa fa-plus-square"></i> Pendaftaran</a></p>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12" style="margin-top: 12px;">
                <div class="card">
                    <div class="card-header bg-info">
                        <i class="fa fa-list"></i> Lowongan Pekerjaan
                    </div>
                    <div class="card-footer text-muted">
                        <form>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l13">Kategori Perusahaan</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="l13">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                        <option>Option 5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l13">Perusahaan</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="l13">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                        <option>Option 5</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Perusaan</th>
                                            <th>Lowongan</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
