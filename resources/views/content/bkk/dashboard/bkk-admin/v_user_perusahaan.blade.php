@extends('template/template_default/app')
@section('content')
    <style>
        div#table_riwayat_length {
            padding-top: .755em;
            margin-top: 1.42857em;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60 widget-bg">
                    <div class="col-md-12">
                        <div class="card card-outline-info">
                            <div class="card-header p-1">
                                <h5 class="panel-title m-2"><i class="fa fa-database" aria-hidden="true"></i> Data User
                                    {{ Session::get('title') }}</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><strong>Status :</strong></label>
                                            <select id="status_from" class="form-control">
                                                <option value="">--Select status--</option>
                                                <option value="1" selected>Active</option>
                                                <option value="0">Non Aktif / Pending</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="data-tabel" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Gambar</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                <th>Status</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="addForm" name="addForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_user">
                                <input type="hidden" name="role" value="industri">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="old_image" id="old_image">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group" width="100%" style="margin-top: 10px">
                                        <div id="delete_foto" style="text-align: center"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pendidikan Akhir</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="pendidikan_akhir" id="pendidikan_akhir" class="form-control">
                                                <option value="smp">SMP sederajat</option>
                                                <option value="sma" selected>SMA sederajat</option>
                                                <option value="d3">D3</option>
                                                <option value="s1">S1</option>
                                                <option value="s2">S2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="status" id="status" class="form-control">
                                                <option value="0">Tidak Aktif</option>
                                                <option value="1" selected>Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="riwayatModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div id="riwayat" style="width: 100%;">

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')



    <script>
        var url_import = "{{ route('import-user_bkk') }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        datables(1, "perusahaan")
        function datables(ids, data_aksi) {
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',

                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createData'
                        }
                    },
                    {
                        text: '<i class="fa fa-upload"></i>',
                        className: 'btn btn-sm',
                        attr: {
                            title: 'Import Data',
                            id: 'import'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('user_bkk-get_datatable') }}",
                    "type": "POST",
                    "data": function(d) {
                        d.status = ids;
                        d.aksi = data_aksi;
                    },
                },
                destroy : true,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }

        $('#status_from').on('change', function() {
            ids = $(this).val();
            datables(ids, "perusahaan");
        })

        $('#import').click(function() {
            $('#importModal').modal('show');
            $('#file-chosen').html('No file choosen');
            $('#HeadingImport').html('Import User Perusahaan');
        });

        $('#createData').click(function() {
            $('#Customer_id').val('');
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah User Pelamar");
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        });

        $(document).on('click', '.edit', function() {
            var id = $(this).data('id');
            var loader = $(this);
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "edit",
                data: {
                    id
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(loader).html('<i class="fa fa-edit"></i>');
                    $('#modelHeading').html("Edit Data Pelamar");
                    $('#id_user').val(data.id);
                    $('#nama').val(data.nama);
                    $('#email').val(data.email);
                    $('#telepon').val(data.telepon);
                    $('#agama').val(data.agama).trigger('change');
                    $('#jenkel').val(data.jk).trigger('change');
                    $('#status').val(data.status).trigger('change');
                    $('#pendidikan_akhir').val(data.pendidikan_akhir).trigger('change');
                    $('#alamat').val(data.alamat);
                    $('#tempat_lahir').val(data.tempat_lahir);
                    $('#tanggal_lahir').val(data.tgl_lhr);
                    $('#delete_foto').html('');
                    if (data.file) {
                        $('#modal-preview').attr('src', data.file);
                        if (data.file_check != "default.png") {
                            $('#delete_foto').append(
                                '<input type="checkbox" name="remove_photo" value="' + data
                                .file + '"/> Remove photo when saving');
                        }
                    }
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                    $('#ajaxModel').modal('show');
                }
            });
        });

        $(document).on('click', '.riwayat', function() {
            var id = $(this).data('id');
            $('#riwayat').html(
                '<div class="table-responsive" style="margin-top: 14px;"><table class="table table-striped" id="table_riwayat" style="width: 100%"><thead><tr><th>#</th><th>Nama</th><th>Posisi</th><th>Industri</th><th>Status</th><th>Tanggal Apply</th></tr></thead></table></div>'
            );
            $('#riwayatModal').modal('show');
            riwayatData(id);
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "soft_delete/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },

                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');

                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        $(".delete-" + id).html(
                            '<i class="fa fa-trash-o"></i>');
                        swa(data.status, data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function riwayatData(id_user) {
            var table_trash = $('#table_riwayat').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('bkk_pelamar-admin_riwayat') }}",
                    "type": "POST",
                    "data": function(d) {
                        d.id_user = id_user;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'loker',
                        name: 'loker'
                    },
                    {
                        data: 'industri',
                        name: 'industri'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },

                ]
            });
        }

        $('body').on('submit', '#addForm', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $("#saveBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('user_bkk-create') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('user_bkk-update') }}";
                method_url = "PUT";
            }

            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: action_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#addForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                    }
                    noti(data.icon, data.success);
                    $('#saveBtn').html('Simpan');
                    $("#saveBtn").attr("disabled", false);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function template() {
            window.location.href = "{{ $url }}";
        }

    </script>

@endsection
