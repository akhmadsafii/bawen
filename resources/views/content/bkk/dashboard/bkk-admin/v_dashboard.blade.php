@extends('template/template_default/app')
@section('content')
    <style>
        #radioBtn .notActive {
            color: #3276b1;
            background-color: #fff;
        }

        .title-question,
        .isi-question {
            width: 650px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            padding: 5px;
            margin: 5px;
        }

        .serv-section-2 {
            position: relative;
            border: 1px solid #eee;
            background: #fff;
            box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.16);
            border-radius: 5px;
            overflow: hidden;
            padding: 30px;
        }

        .serv-section-2:before {
            position: absolute;
            top: 0;
            right: 0px;
            z-index: 0;
            content: " ";
            width: 120px;
            height: 120px;
            background: #f5f5f5;
            border-bottom-left-radius: 136px;
            transition: all 0.4s ease-in-out;
            -webkit-transition: all 0.4s ease-in-out;
        }

        .serv-section-2-icon {
            position: absolute;
            top: 18px;
            right: 22px;
            max-width: 100px;
            z-index: 1;
            text-align: center;
        }

        .serv-section-2-icon i {
            color: #5f27cd;
            font-size: 48px;
            line-height: 65px;
            transition: all 0.4s ease-in-out;
            -webkit-transition: all 0.4s ease-in-out;
        }

        .serv-section-desc {
            position: relative;
        }

        .serv-section-2 h4 {
            color: #333;
            font-size: 20px;
            font-weight: 500;
            line-height: 1.5;
        }

        .serv-section-2 h5 {
            color: #333;
            font-size: 17px;
            font-weight: 400;
            line-height: 1;
            margin-top: 5px;
        }

        .section-heading-line-left {
            content: '';
            display: block;
            width: 100px;
            height: 3px;
            background: #5f27cd;
            border-radius: 25%;
            margin-top: 15px;
            margin-bottom: 5px;
        }

        .serv-section-2 p {
            margin-top: 25px;
            padding-right: 50px;
        }

        .serv-section-2:hover .serv-section-2-icon i {
            color: #fff;
        }

        .serv-section-2:hover:before {
            background: #5f27cd;
        }

        div#content-berita {
            border: 1px solid #6c757d;
            margin-bottom: 5px;
            padding: 18px;
            background: #d0e9ff
        }

        .row.support-content-comment {
            border: 1px solid #6c757d;
            margin-bottom: 5px;
            padding: 18px;
            background: #fff
        }

        p {
            margin: 0;
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60">
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="serv-section-2">
                            <div class="serv-section-2-icon"> <i class="fa fa-industry"></i> </div>
                            <div class="serv-section-desc">
                                <h4>{{ $jumlah['industri'] }}</h4>
                                <h5>Jumlah Industri</h5>
                            </div>
                            <div class="section-heading-line-left"></div>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="serv-section-2">
                            <div class="serv-section-2-icon"> <i class="fa fa-tasks"></i> </div>
                            <div class="serv-section-desc">
                                <h4>{{ $jumlah['loker'] }}</h4>
                                <h5>Lowongan terbuka</h5>
                            </div>
                            <div class="section-heading-line-left"></div>

                        </div>
                    </div>
                </div>
                <div class="row mt-60">
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="card card-default">
                            <div class="card-header" style="background: #5f27cd;">
                                <h5 class="card-title mt-0 mb-0" style="color: #fff">Pertanyaan yang ditujukan ke anda</h5>
                            </div>
                            <div class="card-body p-2" style="background: #f7f7f7;">
                                @if (!empty($feed))
                                    @foreach ($feed as $fd)
                                        <div class="well well-sm widget-bg p-3"
                                            style="border-bottom: 2px solid black !important;">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-8">
                                                    <h4 class="title-question"><b>{{ $fd['nama'] }}</b> -
                                                        {{ $fd['title'] }}</h4>
                                                    <div class="isi-question">{{ $fd['berita'] }}
                                                    </div>
                                                    <a href="javascript:void(0)"
                                                        onclick="detailQuestion({{ $fd['id'] }})">
                                                        <i class="fa fa-reply"></i> Jawab</a>
                                                    &nbsp;&nbsp;
                                                    <a href="javascript:void(0)" onclick="deleteData({{ $fd['id'] }})">
                                                        <i class="fa fa-trash"></i> Hapus</a>
                                                </div>
                                                <div class="col-xs-12 col-md-4 text-center">
                                                    <div style="color: #000000">
                                                        Tayangkan di website?
                                                    </div>
                                                    <div class="input-group" style="display: block">
                                                        <div id="radioBtn" class="btn-group">
                                                            @if ($fd['status'] == 0)
                                                                <a class="btn btn-primary btn-sm notActive"
                                                                    onclick="changeStatus(1,{{ $fd['id'] }})"
                                                                    data-toggle="pilihan_check{{ $fd['id'] }}"
                                                                    data-title="Y">ON</a>
                                                                <a class="btn btn-primary btn-sm active"
                                                                    onclick="changeStatus(0,{{ $fd['id'] }})"
                                                                    data-toggle="pilihan_check{{ $fd['id'] }}"
                                                                    data-title="N">OFF</a>
                                                            @else
                                                                <a class="btn btn-primary btn-sm active"
                                                                    onclick="changeStatus(1,{{ $fd['id'] }})"
                                                                    data-toggle="pilihan_check{{ $fd['id'] }}"
                                                                    data-title="Y">ON</a>
                                                                <a class="btn btn-primary btn-sm notActive"
                                                                    onclick="changeStatus(0,{{ $fd['id'] }})"
                                                                    data-toggle="pilihan_check{{ $fd['id'] }}"
                                                                    data-title="N">OFF</a>
                                                            @endif

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="well well-sm widget-bg p-3 mt-2">
                                        <div class="row">
                                            <center>
                                                Belum ada pertanyaan yang masuk untuk saat ini
                                            </center>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detailIssue" tabindex="-1" role="dialog" aria-labelledby="issue" aria-hidden="true">
        <div class="modal-wrapper">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="background: #eaf3ff;">
                    <div class="modal-header bg-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="modelTitle" style="color: #fff"><i class="fa fa-cog"></i>
                            Detail Pertanyaan</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="content-berita">
                            <div class="col-md-12">
                                <p id="detailFeed"></p>
                                <p id="berita_feed"></p>
                                <a href="#" data-toggle="collapse" data-target="#target1"><span class="fa fa-reply"></span>
                                    &nbsp;Post a reply</a>
                            </div>
                        </div>
                        <div id="target1" class="collapse">
                            <div class="form4 top">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 p-0">
                                            <div class="form-bg mb-5">
                                                <form action="javascript:void(0)" id="formComment">
                                                    <input type="hidden" name="id_feed" id="id_feed">
                                                    <div class="form-group">
                                                        <label class="sr-only">Pesan</label>
                                                        <textarea class="form-control" required="" rows="7" id="messageNine"
                                                            name="pesan" placeholder="Write message"></textarea>
                                                    </div>
                                                    <button type="submit" class="btn text-center btn-blue"
                                                        id="send-comment">Kirim Komentar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="com">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function detailQuestion(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('bkk_loker-detail_feed') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".editData-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    console.log(data);
                    $('#detailFeed').html('Issue <strong>#' + data.id +
                        '</strong> opened by <a href="#">' + data.nama + '</a> ' + data.terbit + '');
                    $('#berita_feed').html(data.berita);
                    $('#id_feed').val(data.id);
                    let komentar = '';
                    $.each(data.komentar, function(index, value) {
                        console.log(value);
                        komentar +=
                            '<div class="row support-content-comment"><div class="col-md-12"><p>Posted by ' +
                            value.nama + ' on ' + value.ditulis + ' </p><p>' + value.komentar +
                            '.</p></div></div>';
                    });
                    $('#com').html(komentar);
                    $('#detailIssue').modal('show');
                    $('#target1').collapse('hide');
                }
            });
        }

        $('#formComment').on('submit', function(event) {
            event.preventDefault();
            $("#send-comment").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#send-comment").attr("disabled", true);
            $.ajax({
                url: "{{ route('bkk_loker-comment_feed_simpan') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#com').prepend(data.html);
                        $('#target1').collapse('hide');
                        $('#formComment').trigger("reset");
                    }
                    noti(data.icon, data.success);
                    $('#send-comment').html('Kirim Komentar');
                    $("#send-comment").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#send-comment').html('Simpan');
                }
            });
        });

        function changeStatus(val, id) {
            if (val == 1) {
                $('a[data-toggle="pilihan_check' + id + '"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id);
            } else {
                $('a[data-toggle="pilihan_check' + id + '"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check' + id + '"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                updateStatus(val, id);
            }
        }

        function updateStatus(value, id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('bkk_loker-update_status_feed') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.icon, data.success);
                }
            });
        }

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('bkk_loker-delete_feed') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload();
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>

@endsection
