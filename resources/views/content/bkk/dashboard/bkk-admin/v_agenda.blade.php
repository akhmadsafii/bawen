@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        #calendar {
            margin-bottom: 12px;
        }

        .pace {
            display: none;
        }

        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            .fc-scroller.fc-day-grid-container {
                height: max-content !important;
            }

            .widget-heading.clearfix {
                padding-left: 23px;
            }

            h5 {
                font-size: 21px !important;
                font-weight: 600 !important;
            }
        }

        .section {
            font-family: 'Roboto Condensed', sans-serif;
            position: absolute;
            top: 10%;
            left: 50%;
            transform: translate(-85%, 0%);
        }

        .typeahead {
            padding: 15px 200px;
            width: 100% !important;
            background-color: #fff !important;
            padding-left: 40px !important;
            font-size: 18px;
        }

        .empty-message {
            color: red;
            text-align: center;
            padding: 10px 0px;
        }

        .tt-menu {
            display: block;
            width: 152%;
            background-color: #fff;
            border: unset !important;
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
            cursor: pointer;
        }

        .man-section {
            position: relative;
            width: 94%;
            border-bottom: 1px solid #d2d2d2;
            ;
            font-family: 'Roboto Condensed', sans-serif;
        }

        .image-section {
            width: 10%;
            float: left;
            display: table;
        }

        .image-section img {
            width: 70px;
            height: 70px;
            border: 1px solid #000;
            display: table-cell;
            vertical-align: middle;
            margin: 6px 0px 5px -9px;
        }

        .description-section {
            float: left;
            width: 80%;
        }

        .description-section h1 {
            margin: 0px;
            font-weight: bold;
            padding: 0px 7px;
            font-size: 16px;
            color: #000;
            margin-top: 4px;
            text-transform: uppercase;
        }

        .description-section p {
            margin: 0px;
            padding: 0px 1px 0px 8px;
            font-size: 14px;
            color: #7d7f80;
            line-height: 15px;
        }

        .description-section span {
            padding: 7px;
            font-size: 13px;
            color: #a09999;
        }

        .more-section {
            position: absolute;
            bottom: 7px;
            right: 16px;
        }

        .more-section a {
            text-decoration: none;
        }

        .more-section button {
            border: unset;
            color: #fff;
            border-radius: 5px;
            padding: 5px;
            background-color: #5D4C46 !important;
        }

        .ui-timepicker-container.ui-timepicker-standard {
            z-index: 1600 !important;
            /* has to be larger than 1050 */
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
        type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <div class="widget-bg">
        <div class="widget-heading clearfix">
            <h5>{{ Session::get('title') }}</h5>
        </div>
        <hr>
        <div class="row p-4">
            <div id="calendar"></div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form action="javascript:void(0)" id="formAgenda">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id_agenda" id="id_agenda">
                            <label for="form-label">Pilih Loker</label>
                            <select name="id_loker" id="id_loker" class="form-control">
                                <option value="">Pilih Loker</option>
                                @foreach ($loker as $lk)
                                    <option value="{{ $lk['id'] }}">{{ $lk['judul'] . ' - ' . $lk['industri'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="form-label">Nama</label>
                            <input type="text" name="nama" id="nama" class="form-control">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="form-label">Tanggal Mulai</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control datepicker"
                                    data-plugin-options="{'autoclose': true}">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="waktu_mulai" id="waktu_mulai"
                                    class="form-control timepicker timepicker-with-dropdown text-center">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="form-label">Tanggal Selesai</label>
                            </div>
                            <input type="hidden" name="tanggal" id="tanggal">
                            <div class="col-md-8">
                                <input type="text" name="tanggal_selesai" id="tanggal_selesai"
                                    class="form-control datepicker" data-plugin-options="{'autoclose': true}">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="waktu_selesai" id="waktu_selesai"
                                    class="form-control timepicker timepicker-with-dropdown text-center">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form-label">Lokasi</label>
                            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</button>
                        <a href="javascript:void(0)" class="deleteAgenda btn btn-danger btn-rounded ripple text-left"
                            onclick="deleteAgenda()">Hapus</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "{{ route('bkk_agenda-beranda') }}",
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    id_loker: value['id_loker'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    loker: value['loker'],
                                    alamat: value['alamat'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: true,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    element.children().last().append(
                        "<br><span class='loker' style='background-color: #fff; color: #2471d2'>LOWONGAN KERJA : " +
                        event
                        .loker + "</span>");
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    $('#action').val('add');
                    $('#modelHeading').html('Tambah Agenda');
                    $('#formAgenda').trigger("reset");
                    $('#tanggal_mulai').val($.fullCalendar.formatDate(start, "DD-MM-Y"));
                    $('#waktu_mulai').val($.fullCalendar.formatDate(start, "HH:mm:ss"));
                    $('#tanggal_selesai').val($.fullCalendar.formatDate(end, "DD-MM-Y"));
                    $('#waktu_selesai').val($.fullCalendar.formatDate(end, "HH:mm:ss"));
                    $('#tanggal').val($.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss"));
                    $('#ajaxModel').modal('show');
                },
                eventClick: function(event, jsEvent, view) {
                    $('#modelHeading').html('Update Agenda');
                    $('#action').val('edit');
                    $('#saveBtn').html('Update');
                    $('#deleteAgenda').show();
                    $('#id_loker').val(event.id_loker).trigger('change');
                    $('#nama').val(event.title);
                    $('#alamat').val(event.alamat);
                    $('#id_agenda').val(event.id);
                    $('#tanggal_mulai').val($.fullCalendar.formatDate(event.start,
                        "DD-MM-Y"));
                    $('#waktu_mulai').val($.fullCalendar.formatDate(event.start,
                        "HH:mm:ss"));
                    $('#tanggal_selesai').val($.fullCalendar.formatDate(event.end,
                        "DD-MM-Y"));
                    $('#waktu_selesai').val($.fullCalendar.formatDate(event.end,
                        "HH:mm:ss"));
                    $('#ajaxModel').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'kalendar/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id,
                        type: "PUT",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                noti(data.icon, data.success)
                            } else {
                                noti(data.icon, data.success)
                            }
                        }
                    });
                },
            });
        });

        $('#formAgenda').on('submit', function(event) {
            event.preventDefault();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".btn-update").attr("disabled", true);
            var action_url = '';

            if ($('#action').val() == 'add') {
                action_url = "{{ route('bkk_agenda-simpan') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'edit') {
                action_url = "{{ route('bkk_agenda-update') }}";
                method_url = "PUT";
            }
            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formAgenda').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                    noti(data.success, data.message);
                    $('.btn-update').html('Update');
                    $(".btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function deleteAgenda() {
            var id = $('#id_agenda').val();
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('bkk_agenda-soft_delete') }}",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $("#deleteKalender").html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#ajaxModel').modal('hide');
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                            swa(data.status + "!", data.message, data.icon);
                            $("#deleteKalender").html('Delete');
                        }
                    });
                },
                function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
        }

        $('.timepicker').timepicker({
            timeFormat: 'HH:mm:ss',
            interval: 60,
            minTime: '0',
            maxTime: '11:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: true,
            dropdown: true,
            scrollbar: true
        });
    </script>
@endsection
