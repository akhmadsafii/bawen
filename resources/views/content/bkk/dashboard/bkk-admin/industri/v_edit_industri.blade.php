@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 300px;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT INDUSTRI</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('bkk_industri-beranda') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i>
                Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Industri</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $industri['file'] }});">
                                </div>
                                <h4 class="my-2">{{ ucwords($industri['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id="{{ $industri['id'] }}"><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $industri['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#profile"
                                        data-toggle="tab">Profile</a>
                                </li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#pemilik"
                                        data-toggle="tab">Pemilik</a></li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#tab_lokasi"
                                        data-toggle="tab">Lokasi</a></li>
                            </ul>
                            <div class="aksi">
                                <button type="reset" class="btn btn-danger"><i class="fas fa-sync-alt"></i> Reset</button>
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Simpan</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="profile">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kategori</label>
                                        <div class="col-md-9">
                                            <select name="id_kategori" id="id_kategori" class="form-control">
                                                <option value="">Pilih Kategori Industri..</option>
                                                @foreach ($kategori as $kt)
                                                    <option value="{{ $kt['id'] }}"
                                                        {{ $industri['id_kategori'] == $kt['id'] ? 'selected' : '' }}>
                                                        {{ $kt['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $industri['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Industri</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Industri"
                                                type="text" value="{{ $industri['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Telepon</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="telepon" name="telepon"
                                                placeholder="No. Telepon" type="text" value="{{ $industri['telepon'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Deskripsi</label>
                                        <div class="col-md-9">
                                            <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control">{{ $industri['deskripsi'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Website</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="website" name="website"
                                                placeholder="website Industri" type="text"
                                                value="{{ $industri['website'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pemilik">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="keyword" name="keyword" class="form-control"
                                                    placeholder="Masukan Keyword Pencarian">
                                                <div class="input-group-btn">
                                                    <a href="javascript:void(0)" onclick="mulaiCari()"
                                                        class="btn btn-info btnCari"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="data_user">
                                        @if (!empty($user))
                                            @foreach ($user as $us)
                                                <div class="col-md-6 my-2">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <table>
                                                                <tr>
                                                                    <td width="20"><input type="radio" name="id_user"
                                                                            value="{{ $us['id'] }}"
                                                                            {{ $us['id'] == $industri['id_user'] ? 'checked' : '' }}>
                                                                    </td>
                                                                    <td><img src="{{ $us['file'] }}"
                                                                            class="mr-2" width="70"></td>
                                                                    <td>
                                                                        <b>{{ ucwords($us['nama']) }}</b>
                                                                        <p class="m-0">{{ $us['email'] }}</p>
                                                                        <small><i class="fas fa-phone"></i>
                                                                            {{ $us['telepon'] }}</small>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <table class="w-100">
                                                            <tr>
                                                                <td class="text-center">
                                                                    <h3>Belum ada user industri yang tersedia</h3>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_lokasi">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pilih Provinsi</label>
                                                <select name="provinsi" id="provinsi" class="form-control">
                                                    <option value="">PILIH PROVINSI..</option>
                                                    @foreach ($provinsi as $pr)
                                                        <option value="{{ $pr['id'] }}"
                                                            {{ !empty($industri) && $industri['id_provinsi'] == $pr['id'] ? 'selected' : '' }}>
                                                            {{ ucwords($pr['name']) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Kabupaten</label>
                                                <input type="text" name="kabupaten" id="kabupaten" class="form-control"
                                                    value="{{ !empty($industri) ? $industri['kabupaten'] : '' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i
                                                            class="material-icons">location_on</i>
                                                    </div>
                                                    <input class="form-control" type="text" id="lokasi" name="lokasi"
                                                        placeholder="Masukan pencarian lokasi">
                                                    <div class="input-group-addon">
                                                        <a href="javascript:void(0)" onclick="addr_search();"><i
                                                                class="material-icons list-icon search text-dark">search</i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div id="results"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Koordinat Latitude</label>
                                                <input class="form-control" type="lat" name="lat" id="lat"
                                                    placeholder="lat"
                                                    value="{{ !empty($industri) && $industri['lat'] != null ? $industri['lat'] : '-7.35768980' }}"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Koordinat Longitude</label>
                                                <input class="form-control" type="long" name="long" id="lon"
                                                    placeholder="Long"
                                                    value="{{ !empty($industri) && $industri['long'] != null ? $industri['long'] : '110.28245190' }}"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" id="map"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <textarea name="alamat" id="alamat" rows="3"
                                                    class="form-control">{{ !empty($industri) ? $industri['alamat'] : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $industri['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var options;
            var map;

            var startlat = $('#lat').val();
            var startlon = $('#lon').val();

            options = {
                center: [startlat, startlon],
                zoom: 9
            }
            map = L.map('map', options);

            var nzoom = 12;

            L.tileLayer(
                'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token={{ $leaflet['token'] }}'
            ).addTo(map);

            var myMarker = L.marker([startlat, startlon], {
                title: "Coordinates",
                alt: "Coordinates",
                draggable: true
            }).addTo(map).on('dragend', function() {
                var lat = myMarker.getLatLng().lat.toFixed(8);
                var lon = myMarker.getLatLng().lng.toFixed(8);
                var czoom = map.getZoom();
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat, lon);
            });

            window.getAddressMaker = function(latx, lonx) {
                $.ajax({
                    url: "https://nominatim.openstreetmap.org/reverse",
                    data: {
                        lat: latx,
                        lon: lonx,
                        format: "json"
                    },
                    beforeSend: function(xhr) {},
                    dataType: "json",
                    type: "GET",
                    async: true,
                    crossDomain: true
                }).done(function(res) {
                    // console.log(res.address);
                    var rows = JSON.parse(JSON.stringify(res.address));
                    if (rows.village != 'underfined' && rows.village != null) {
                        var alamatx = rows.village + ',' + ',' + rows
                            .state + ',' + rows.country;
                    } else if (rows.postcode != 'underfined' && rows.postcode != null) {
                        var alamatx = rows
                            .state + ',' + rows.postcode + ',' + rows.country;
                    } else if (rows.country != 'underfined' && rows.country != null) {
                        var alamatx = rows
                            .state + ',' + rows.country;
                    } else {
                        var alamatx = rows
                            .state + ',' + rows.country;
                    }
                    $('input[name="alamat"]').val(alamatx);
                    myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                        alamatx).openPopup();
                }).fail(function(error) {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                });
            }

            window.chooseAddr = function chooseAddr(lat1, lng1) {
                myMarker.closePopup();
                map.setView([lat1, lng1], 18);
                myMarker.setLatLng([lat1, lng1]);
                lat = lat1.toFixed(8);
                lon = lng1.toFixed(8);
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat1, lng1);
            }

            $('body').on('cllick', '.address', function() {
                $('input[name="alamat"]').val($(this).data('alamat'));
            });

            window.myFunction = function myFunction(arr) {
                var out = "<br />";
                var i;
                if (arr.length > 0) {
                    for (i = 0; i < arr.length; i++) {
                        out +=
                            "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                            arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                            .display_name + "'><a href='javascript:void(0)'>" + arr[i].display_name +
                            "</a></div>";
                    }
                    document.getElementById('results').innerHTML = out;
                } else {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                }
            }

            window.addr_search = function addr_search() {
                var inp = document.getElementById("lokasi");
                console.log(inp);
                var xmlhttp = new XMLHttpRequest();
                var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                    .value;
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var myArr = JSON.parse(this.responseText);
                        myFunction(myArr);
                    }
                };
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            }

            var homeTab = document.getElementById('tab_lokasi');
            var observer1 = new MutationObserver(function() {
                if (homeTab.style.display != 'none') {
                    map.invalidateSize();
                }
            });
            observer1.observe(homeTab, {
                attributes: true
            });


            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('bkk_industi-update') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status.toUpperCase() + "!", data.message, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });


            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('upload_profile-bkk_industri') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $(".profile-img").css("background-image", "url(" + data.image +
                                ")");
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete_profile-bkk_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".profile-img").css("background-image", "url(" + data
                                    .image + ")");
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });



        })



        function mulaiCari() {
            let keyword = $('#keyword').val();
            if (keyword.length) {
                // console.log(keyword);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('bkk_user_filter_name') }}",
                    data: {
                        keyword
                    },
                    beforeSend: function() {
                        $('.btnCari').html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        console.log(data);
                        $('#data_user').html(data);
                        $('.btnCari').html(
                            '<i class="fa fa-search"></i>');
                    }
                });
            } else {
                alert('kata kunci pencarian harus diisi');
            }
        }
    </script>
@endsection
