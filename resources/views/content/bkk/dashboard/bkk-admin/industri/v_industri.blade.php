@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    @if (session('role') == 'bkk-admin')
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info addIndustri"><i class="fas fa-plus-circle"></i> Tambah
                                    Industri</button>
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form pull-right">


                                {{-- <form class="navbar-form pull-right" role="search"> --}}
                                <div class="form-row align-items-center">
                                    <div class="col-auto">
                                        <select name="status" id="status" class="form-control">
                                            <option value="all" {{ $status == 'all' ? 'selected' : '' }}>Semua..</option>
                                            <option value="active" {{ $status == 'active' ? 'selected' : '' }}>Aktif
                                            </option>
                                            <option value="deactive" {{ $status == 'deactive' ? 'selected' : '' }}>Tidak
                                                Aktif/Pending</option>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <div class="input-group">
                                            @php
                                                $serc = str_replace('-', ' ', $search);
                                            @endphp
                                            <input type="text" value="{{ $serc }}" id="search" name="search"
                                                class="form-control" placeholder="Search">
                                            <div class="input-group-btn">
                                                <a type="submit" id="fil" onclick="filter('{{ $routes }}')"
                                                    class="btn btn-info"><i class="fa fa-search text-white"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Industri</th>
                                    <th>Pemilik</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($industri))
                                    <tr>
                                        <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($industri as $ind)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                <b>{{ $ind['nama'] }}</b>
                                                <p class="m-0">Telepon.
                                                    {{ $ind['telepon'] != null ? $ind['telepon'] : '-' }}</p>
                                                <small><b>{{ $ind['kategori'] }}</b></small>
                                            </td>
                                            <td>
                                                @if ($ind['id_user'] == null)
                                                    -
                                                @else
                                                    Pemilik<b> {{ ucwords($ind['pemilik']) }}</b>
                                                    <p class="m-0">Email. {{ $ind['email'] }}</p>
                                                @endif
                                            </td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" {{ $ind['status'] == 1 ? 'checked' : '' }}
                                                        class="industri_check" data-id="{{ $ind['id'] }}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('bkk_industi-change', ['code' => (new \App\Helpers\Help())->encode($ind['id']),'k' => str_slug($ind['nama'])]) }}"
                                                    class="btn btn-sm btn-info"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                    data-id="{{ $ind['id'] }}"><i class="fas fa-trash"></i></a>
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $pagination !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalIndustri" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formIndustri">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Kategori</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-layer-group"></i>
                                    </span>
                                    <select name="kategori_industri" id="kategori_industri" class="form-control">
                                        <option value="">Pilih Kategori Industri..</option>
                                        @foreach ($kategori as $kt)
                                            <option value="{{ $kt['id'] }}">{{ $kt['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama Industri</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-industry"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Industri"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Telepon</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-phone"></i>
                                    </span>
                                    <input class="form-control" id="telepon" name="telepon" placeholder="Telepon"
                                        type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Deskripsi</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('click', '.addIndustri', function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Industri");
                $('#modalIndustri').modal('show');
                $('#action').val('Add');
            });

            $('#formIndustri').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('bkk_industi-save') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $('body').on('click', '.delete', function() {
                let id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('user_bkk-sof_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash-alt"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function filter(routes) {
            // console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-');
            // alert(search);
            var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?status=" + status + "&search=" + search;
            // alert(url);
            document.location = url;
        }
    </script>
@endsection
