@extends('template/template_default/app')
@section('content')

    <div class="row">
        <div class="content" style="width: 100%; padding: 0 31px;">
            <div class="container-fluid">
                <div class="row mt-60 widget-bg">
                    <div class="col-md-12">
                        <h4 class="dark-grey m-0">Edit Web Setting</h4>
                        <hr>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="l10">Color Scheme</label>
                            <div class="col-md-7">
                                <input type="color" id="favcolor" name="favcolor" value="#ff0000" style="height: 100%;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="l10">Background Logo</label>
                            <div class="col-md-5">
                                <Select class="form-control">
                                    <option value="">Dark</option>
                                    <option value="">Green</option>
                                </Select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="l10">Sidebar Color</label>
                            <div class="col-md-3">
                                <Select class="form-control">
                                    <option value="">Dark</option>
                                    <option value="">Green</option>
                                </Select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="l10">Font Family</label>
                            <div class="col-md-4">
                                <Select class="form-control">
                                    <option value="">Dark</option>
                                    <option value="">Green</option>
                                </Select>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>
    </div>



    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@endsection
