@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="tab-pane active" id="home-tab-v1">
        <h5 class="box-title"><i class="fa fa-graduation-cap"></i> Pendidikan
        </h5>
        <div>
            <table style="width: 100%">
                <tr>
                    <td>Tambahkan 2 jenjang pendidikan yang paling tinggi</td>
                </tr>
            </table>
            <hr>
            <table style="width: 100%">

                @if (empty($pendidikan))
                    <tr>
                        <td colspan="3">
                            <form id="formPendidikan" name="formPendidikan" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Institusi/ Sekolah</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nama" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Tanggal Wisuda</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="bulan_dari" class="form-control select3">
                                                    <option value="">Bulan</option>
                                                    <option value="01">Januari</option>
                                                    <option value="02">Februari</option>
                                                    <option value="03">Maret</option>
                                                    <option value="04">April</option>
                                                    <option value="05">Mei</option>
                                                    <option value="06">Juni</option>
                                                    <option value="07">Juli</option>
                                                    <option value="08">Agustus</option>
                                                    <option value="09">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="tahun_dari" class="form-control select3">
                                                    <option value="">Tahun</option>
                                                    @php
                                                        $firstYear = (int) date('Y');
                                                        $lastYear = $firstYear - 20;
                                                        for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                            echo '<option value=' . $year . '>' . $year . '</option>';
                                                        }
                                                    @endphp
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Kualifikasi</label>
                                    <div class="col-md-9">
                                        <select name="kualifikasi" class="form-control select3">
                                            <option value="">-Pilih kualifikasi-</option>
                                            <option value="s1">S1 (Sarjana)</option>
                                            <option value="d3">d3 (Diploma)</option>
                                            <option value="sma" selected>SMU/SMA/SMK</option>
                                            <option value="smp">SMP/MTS/MA</option>
                                            <option value="sd">SD/MI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Jurusan</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="jurusan" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Nilai Akhir</label>
                                    <div class="col-md-9">
                                        <select name="nilai_akhir" id="nilai_akhirs" class="form-control">
                                            <option value="ipk">IPK</option>
                                            <option value="tidak_selesai">Tidak Selesai</option>
                                            <option value="masih_belajar" selected>Masih belajar</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row" id="skors" style="display: none">
                                    <label class="col-md-3 col-form-label" for="l10">Skor</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input class="form-control" name="nilai_skor" type="text">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="col-md-3 col-form-label" for="l10">dari</label>
                                            </div>
                                            <div class="col-md-5">
                                                <input class="form-control" name="dari" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15">Informasi Tambahan</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="informasi" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15"></label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="action" id="action" value="Add" />
                                        <button type="submit" class="btn btn-outline-info btn-sm" id="saveBtn"
                                            value="create">Simpan</button>
                                        <a class="btn btn-outline-danger btn-sm" id="btnCancel"
                                            style="color: #e6614f">Batalkan</a>
                                    </div>
                                </div>
        </div>
        </form>
        </td>
        </tr>
    @else
        <tr>
            <td style="width: 20%"></td>
            <td></td>
            <td></td>
            <td style="text-align: right;">
                <a href="javascript:void(0)" id="addPendidikan" class="btn btn-sm btn-outline-info">
                    <i class="fa fa-plus-circle"></i> Tambah
                </a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tbody id="outputPengalaman">

            @foreach ($pendidikan as $pnd)
                <tr class="mt-2">
                    <td style="vertical-align: top">
                        {{ (new \App\Helpers\Help())->getMonthYear($pnd['tahun_lulus']) }}
                    </td>
                    <td style="vertical-align: top">
                        <h5 class="mt-0"><b>{{ ucwords($pnd['nama']) }}</b></h5>
                        <h5 class="mt-0">{{ strtoupper($pnd['jenjang']) }}</h5>
                        <table style="width: 100%">
                            <tr>
                                <td>jurusan</td>
                                <td>{{ ucwords($pnd['jurusan']) }}</td>
                            </tr>
                            @if ($pnd['nilai_akhir'] == 'ipk')
                                <tr>
                                    @php
                                        $nilai = explode('#', $pnd['skor_nilai_akhir']);
                                    @endphp
                                    <td>IPK</td>
                                    <td>{{ $nilai[0] . ' / ' . $nilai[1] }}</td>
                                </tr>
                            @endif

                        </table>

                    <td>
                    <td style="text-align: right; vertical-align: top">
                        <a href="javascript:void(0)" data-id="{{ $pnd['id'] }}" class="editPendidikan"><i
                                class="fa fa-edit"></i></a> &nbsp; &nbsp;
                        <a href="javascript:void(0)" onclick="deletePendidikan({{ $pnd['id'] }})"><i
                                class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        @endif
        </table>
    </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="pendidikanModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formPendidikan" action="javascript:void(0)" name="formPendidikan" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Institusi/ Sekolah</label>
                            <div class="col-md-9">
                                <input name="id" id="id_pendidikan" type="hidden">
                                <input name="id_user" id="id_user" type="hidden">
                                <input class="form-control" name="nama" id="nama" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Tanggal Wisuda</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="bulan_dari" id="bulan" class="form-control select3">
                                            <option value="">Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_dari" class="form-control select3" id="tahun">
                                            <option value="">Tahun</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Kualifikasi</label>
                            <div class="col-md-9">
                                <select name="kualifikasi" id="kualifikasi" class="form-control select3">
                                    <option value="">-Pilih kualifikasi-</option>
                                    <option value="s1">S1 (Sarjana)</option>
                                    <option value="d3">d3 (Diploma)</option>
                                    <option value="sma" selected>SMU/SMA/SMK</option>
                                    <option value="smp">SMP/MTS/MA</option>
                                    <option value="sd">SD/MI</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Jurusan</label>
                            <div class="col-md-9">
                                <input class="form-control" name="jurusan" id="jurusan" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Nilai Akhir</label>
                            <div class="col-md-9">
                                <select name="nilai_akhir" id="nilai_akhir" class="form-control">
                                    <option value="ipk">IPK</option>
                                    <option value="tidak_selesai">Tidak Selesai</option>
                                    <option value="masih_belajar" selected>Masih belajar</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row" id="skor" style="display: none">
                            <label class="col-md-3 col-form-label" for="l10">Skor</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <input class="form-control" name="nilai_skor" id="skor_nilai" type="text">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="col-md-3 col-form-label" for="l10">dari</label>
                                    </div>
                                    <div class="col-md-5">
                                        <input class="form-control" name="dari" id="skor_dari" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Informasi Tambahan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="informasi" id="informasi" rows="3"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#nilai_akhir').on('change', function() {
            var nilai = $(this).val();
            if (nilai == "ipk") {
                $('#skor').show();
            } else {
                $('#skor').hide();
            }
            console.log(nilai);
        });

        $('#nilai_akhirs').on('change', function() {
            var nilai = $(this).val();
            if (nilai == "ipk") {
                $('#skors').show();
            } else {
                $('#skors').hide();
            }
            console.log(nilai);
        });

        $('#addPendidikan').click(function() {
            // console.log("tes");
            $('#pendidikanModal').modal('show');
            $('#formPengalaman').trigger("reset");
            $('#modelHeading').html("Tambah Data Pendidikan");
        })

        $('#formPendidikan').on('submit', function(event) {
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('bkk_pendidikan-create') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('bkk_pendidikan-update') }}";
                method_url = "PUT";
            }

            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#formPendidikan').trigger("reset");
                        $('#pendidikanModal').modal('hide');
                        location.reload(true);
                    }
                    $('#saveBtn').html('Simpan');
                    noti(data.success, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });

        });

        $(document).on('click', '.editPendidikan', function() {
            var id = $(this).data('id');
            $('#formPendidikan').trigger("reset");
            var animasi = $(this);
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "pendidikan/edit",
                data: {
                    id
                },
                beforeSend: function() {
                    $(animasi).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    // console.log(data)
                    $(animasi).html('<i class="fa fa-edit"></i>');
                    $('#modelHeading').html("Edit Data Pendidikan");
                    $('#id_pendidikan').val(data.id);
                    $('#id_user').val(data.id_user);
                    $("#nama").val(data.nama);
                    $("#bulan").val(data.bulan).trigger('change');
                    $("#tahun").val(data.tahun).trigger('change');
                    $("#kualifikasi").val(data.jenjang).trigger('change');
                    $("#jurusan").val(data.jurusan);
                    $("#nilai_akhir").val(data.nilai_akhir).trigger('change');
                    $("#skor_nilai").val(data.skor);
                    $("#skor_dari").val(data.skor_dari);
                    $("#informasi").val(data.informasi);
                    $('#action').val('Edit');
                    $('#pendidikanModal').modal('show');
                }
            });
        });

        function deletePendidikan(id) {
            if (id) {
                var confirmdelete = confirm("Do you really want remove data?");
                var animasi = $(this);
                if (confirmdelete == true) {
                    $.ajax({
                        type: 'POST',
                        url: "pendidikan/soft_delete",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(animasi).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload(true);
                            }
                            swa(data.status + "!", data.message, data.success);

                        }
                    });
                }

            }
        }
    </script>
@endsection
