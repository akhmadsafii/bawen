@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <style>
        #avatar {
            background-image: url('{{ $data['file'] }}');
            background-size: 100%;
            background-position: center center;
            background-repeat: no-repeat;
            height: 200px;
            width: 200px;
            border: 3px solid #0af;
            border-radius: 50%;
            transition: background ease-out 200ms;
        }

        @media (max-width: 960px) {
            #avatar{
                height: 120px;
                width: 120px;
            }
    }

        #preview {
            position: relative;
        }

        input[type="file"] {
            display: none;
        }

        #upload-button {
            padding: 18px;
            width: 58px;
            height: 58px;
            border-radius: 50%;
            border: none;
            cursor: pointer;
            background-color: #08f;
            box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
                0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
            transition: background-color ease-out 120ms;
            position: absolute;
            right: 9%;
            bottom: 0%;
        }

        #upload-button:hover {
            background-color: #45a;
        }

    </style>
    <div class="row widget-holder">
        <div class="col-md-12">
            <small class="text-center float-right">Last Updated: <br>
                {{ (new \App\Helpers\Help())->getTanggal($data['updated_at']) }}</small>
        </div>
        <div class="col-md-3 my-auto">
            <form id="formProfil" action="javascript:void(0)" enctype="multipart/form-data">
                @csrf
                <center>
                    <main>
                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                        <input type="file" name="image" id="image" accept="image/*" />
                        <div id="preview">
                            <div id="avatar"></div>
                            <a href="javascript:void(0)" id="upload-button" aria-labelledby="image"
                                aria-describedby="image">
                                <i class="fa fa-upload" style="color: #fff"></i>
                            </a>
                        </div>
                    </main>
                    @php
                        $gambars = explode('/', $data['file']);
                        $gambar = end($gambars);
                    @endphp
                    @if ($gambar != 'default.png')
                        <input type="checkbox" name="remove_photo" value="delete_check"> Remove Photo <br>
                    @endif

                    <button type="submit" class="btn btn-success btn-sm btn-image mt-2" style="text-align: center"><i
                            class="fa fa-refresh"></i> Update</button>
                </center>
            </form>
        </div>
        <div class="col-md-9 my-auto">
            <form>
                <div class="form-group row mb-0">
                    <label class="col-md-2 col-3 col-form-label">Nama</label>
                    <div class="col-md-10 col-9">
                        <p class="form-control-plaintext">: {{ ucwords($data['nama']) }}</p>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <label class="col-md-2 col-3 col-form-label">Role</label>
                    <div class="col-md-10 col-9">
                        <p class="form-control-plaintext">: {{ ucwords($data['role']) }}</p>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <label class="col-md-2 col-3 col-form-label">Telepon</label>
                    <div class="col-md-10 col-9">
                        <p class="form-control-plaintext">: {{ $data['telepon'] }}</p>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <label class="col-md-2 col-3 col-form-label">Email</label>
                    <div class="col-md-10 col-9">
                        <p class="form-control-plaintext">: {{ $data['email'] }}</p>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <label class="col-md-2 col-3 col-form-label">Alamat</label>
                    <div class="col-md-10 col-9">
                        <p class="form-control-plaintext">: {{ $data['alamat'] }}</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="row widget-holder">
        <div class="col-md-12">
            <h3 class="box-title">PENDIDIKAN</h3>
            @if (!empty($data['pendidikan']))
                <ul class="list-group list-group-flush">
                    @foreach ($data['pendidikan'] as $pd)
                        <li class="list-group-item border-0"><b>{{ strtoupper($pd['jenjang']) }}
                                ({{ $pd['jurusan'] }})
                            </b> -
                            <span class="text-info"> <i class="fas fa-graduation-cap"></i>
                                {{ Str::upper($pd['nama']) }}</span> - <span class="text-purple"><i
                                    class="fas fa-calendar-check"></i>
                                {{ (new \App\Helpers\Help())->getMonthYear($pd['tahun_lulus']) }}</span>
                        </li>
                    @endforeach
                </ul>
            @else
                <p class="text-danger">*Data pendidikan saat ini tidak tersedia</p>
            @endif
        </div>
    </div>
    <hr>
    <div class="row widget-holder">
        <div class="col-md-12">
            <h3 class="box-title">INFORMASI TAMBAHAN</h3>
            <div class="form-group row mb-0">
                <label class="col-md-2 col-12 col-form-label">Ketrampilan</label>
                <div class="col-md-10 col-12">
                    <ul class="list-group list-group-flush">
                        @if (!empty($data['keterampilan']))
                            @foreach ($data['keterampilan'] as $kt)
                                <li class="list-group-item border-0 p-1"><i class="fas fa-angle-double-right"></i>
                                    {{ ucwords($kt['nama']) }} -- <span class="text-success"><i
                                            class="fas fa-sort-numeric-up"></i> {{ $kt['tingkat'] }}</span> </li>
                            @endforeach
                        @else
                            <p class="form-control-plaintext text-danger">Anda belum menambahkan ketrampilan</p>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label class="col-md-2 col-12 col-form-label">Pengalaman</label>
                <div class="col-md-10 col-12">
                    <ul class="list-group list-group-flush">
                        @if (!empty($data['pengalaman']))
                            @foreach ($data['pengalaman'] as $pl)
                                <li class="list-group-item border-0">>> {{ ucwords($pl['nama']) }}</li>
                            @endforeach
                        @else
                            <p class="form-control-plaintext text-danger">Anda belum menambahkan panglaman</p>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="resumeModal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background-color: #03a9f3">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="modelHeadings">Import Data Jurusan</h5>
            </div>
            <form id="importFile" name="importFile" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <center>
                                <div class="centr">
                                    <i class="material-icons"
                                        style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                    <b style="color: #03a9f3">Choose the file to be imported</b>
                                    <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                    <p>Maximum upload file size is 5 MB.</p>

                                    <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                        accept="*" hidden />
                                    <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                        Upload File</label>
                                    <span id="file-chosen">No file chosen</span>
                                    <br>
                                    <u>
                                        <a href="javascript:void(0)" onclick="return template()"
                                            style="color:#03a9f3">Download sample template
                                            for
                                            import
                                        </a>
                                    </u>
                                </div>
                            </center>
                        </div>
                        <div class="col-md-3"></div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                        value="create">Simpan</a>
                </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            const UPLOAD_BUTTON = document.getElementById("upload-button");
            const FILE_INPUT = document.querySelector("input[type=file]");
            const AVATAR = document.getElementById("avatar");

            UPLOAD_BUTTON.addEventListener("click", () => FILE_INPUT.click());

            FILE_INPUT.addEventListener("change", event => {
                const file = event.target.files[0];

                const reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onloadend = () => {
                    AVATAR.setAttribute("aria-label", file.name);
                    AVATAR.style.background = `url(${reader.result}) center center/cover`;
                };
            });


            $('body').on('submit', '#formProfil', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "/program/bursa_kerja/profile/update_image",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(".btn-image").attr("disabled", true);
                        $(".btn-image").html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            // noti(data.icon, data.success);
                            window.location.reload(true);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $(".btn-image").html('<i class="fa fa-refresh"></i> Update');
                        $(".btn-image").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $(".btn-image").html('<i class="fa fa-refresh"></i> Update');
                        $(".btn-image").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
