<li class="list-group-item p-2 {{ Request::segment(4) === 'edit' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/profile/edit') }}">Profil saya</a>
</li>
<li class="list-group-item p-2 {{ Request::segment(3) === 'pengalaman' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/pengalaman') }}">Pengalaman</a>
</li>
<li class="list-group-item p-2 {{ Request::segment(3) === 'pendidikan' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/pendidikan') }}">Pendidikan</a>
</li>
<li class="list-group-item p-2 {{ Request::segment(3) === 'keterampilan' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/keterampilan') }}">Ketrampilan</a>
</li>
<li class="list-group-item p-2 {{ Request::segment(4) === 'resume' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/dokumen/resume') }}">Resume diunggah</a>
</li>
