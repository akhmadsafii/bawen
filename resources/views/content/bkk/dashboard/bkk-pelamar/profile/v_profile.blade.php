@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        #avatar {
            background-size: 100%;
            background-position: center center;
            background-repeat: no-repeat;
            height: 200px;
            width: 200px;
            border: 3px solid #0af;
            border-radius: 50%;
            transition: background ease-out 200ms;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h2 class="box-title m-3">Profle pelamar</h2>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Bursa Kerja</li>
                <li class="breadcrumb-item active">Info Profile</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3  widget-holder widget-bg">
                    <center>
                        <div id="avatar" style="background-image: url('{{ $profile['file'] }}')"></div>
                        <h3 class="box-title mb-0">{{ $profile['nama'] }}</h3>
                        <p><a href="{{ url('program/bursa_kerja/profile/edit') }}">Lihat Profile Saya >></a></p>

                    </center>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="{{ url('program/bursa_kerja/profile/update/pelamar') }}">
                                Edit Profil</a></li>
                        <li class="list-group-item"><a href="{{ url('program/bursa_kerja/pengalaman') }}">Pengalaman</a>
                        </li>
                        <li class="list-group-item"><a href="{{ url('program/bursa_kerja/pendidikan') }}">Pendidikan</a>
                        </li>
                        <li class="list-group-item"><a
                                href="{{ url('program/bursa_kerja/keterampilan') }}">Ketrampilan</a></li>
                        <li class="list-group-item"><a href="{{ url('program/bursa_kerja/dokumen/resume') }}">Resume
                                diunggah</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="widget-bg">
                        @yield('content_profile_pelamar')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

    </script>
@endsection
