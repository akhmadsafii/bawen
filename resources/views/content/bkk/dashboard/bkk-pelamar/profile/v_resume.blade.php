@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="tab-pane active" id="home-tab-v1">
        <h5 class="box-title"><i class="fa fa-paperclip"></i> Resume yang diunggah
        </h5>
        @if ($resume == null)
            <div>
                <p>Anda belum unggah resume.</p>
                <button class="btn btn-info" id="uploadResume">Unggah sekarang</button>
            </div>
        @else
            <div>
                <table style="width: 50%">
                    <tr>
                        <td>Nama File</td>
                        <td>{{ $resume['jenis'] }}.pdf &nbsp;&nbsp;&nbsp; <a href="javascript:void"
                                onclick="deleteDokumen('{{ $resume['id_code'] }}')" class="delete_dokumen"><i
                                    class="fa fa-trash"></i></a>

                        <td>
                    </tr>
                    <tr>
                        <td>Tanggal diperbarui</td>
                        <td>{{ (new \App\Helpers\Help())->getHoursMinute($resume['updated_at']) }}
                        <td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            @php
                                $file_explode = explode('/', $resume['file']);
                                $files = end($file_explode);
                            @endphp
                            <a href="{{ url('program/bursa_kerja/dokumen/download', $resume['id_code']) }}"
                                class="btn btn-info btn-success btn-sm"><i class="fa fa-eye"></i> Lihat</a>
                            <a href="javascript:void(0)" data-id="{{ $resume['id'] }}" data-files="{{ $files }}"
                                class="btn btn-info btn-sm ganti"><i class="fa fa-exchange"></i>
                                Ganti</a>
                        </td>
                    </tr>
                </table>
            </div>
        @endif


        <br>
        <small>Catatan: Versi terbaru dari resume yang diunggah dapat diakses oleh semua perusahaan yang telah dilamar.
            Pelajari Lebih Lanjut</small>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="resumeModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeadings">Upload Resume</h5>
                </div>
                <form id="formUpload" action="javascript:void(0)" name="formUpload" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <center>
                                    <div class="centr">
                                        <p class="mb-0">Your file must be in PDF (.pdf) format</p>
                                        <p>File size must not exceed 1MB.</p>
                                        <p>Click Browse to locate the file on your computer. The click Upload to begin the
                                            uploading process.</p>
                                        <div class="form-group row justify-content-center">
                                            <label class="col-md-5 col-form-label" for="l0">Nama Resume</label>
                                            <div class="col-md-7">
                                                <input type="hidden" name="id" id="id_dokumen">
                                                <input class="form-control" id="jenis" name="jenis"
                                                    placeholdser="masukan nama" type="text">
                                            </div>
                                            <div class="col-md-10">
                                                <input type="file" id="actual-btn" class="margin" type="file"
                                                    name="image" accept=".pdf" />
                                            </div>
                                        </div>
                                        <input type="hidden" name="action" id="action_upload" value="Add" />
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn btn-info btn-sm" id="importBtn" value="create"><i
                                                class="fa fa-paper-plane"></i> Upload</button>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm" data-dismiss="modal"
                                            aria-hidden="true"><i class="fa fa-close"></i> Batalkan</a>
                                    </div>
                                </center>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#uploadResume').click(function() {
            $('#resumeModal').modal('show');
            $('#file-chosen').html('No file choosen');
        });

        $('.ganti').click(function() {
            var id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: "{{ url('program/bursa_kerja/dokumen/edit') }}",
                data: {
                    id_dokumen: id
                },
                // beforeSend: function() {
                //     $(".editData-" + id).html(
                //         '<i class="fa fa-spin fa-spinner"></i> Loading');
                // },
                success: function(data) {
                    console.log(data);
                    $('#file-chosen').html('No file choosen');
                    $('#jenis').val(data.jenis);
                    $('#id_dokumen').val(data.id);
                    $('#action_upload').val('Edit');
                    $('#resumeModal').modal('show');
                }
            });
        });

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })

        $('body').on('submit', '#formUpload', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $('#importBtn').html('Sending..');
            var formDatas = new FormData(document.getElementById("formUpload"));
            var action_url = '';

            if ($('#action_upload').val() == 'Add') {
                action_url = "{{ route('bkk_dokumen-save') }}";
            }

            if ($('#action_upload').val() == 'Edit') {
                action_url = "{{ route('bkk_dokumen-update') }}";
            }
            $.ajax({
                type: "POST",
                url: action_url,
                data: formDatas,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    if (data.status == 'berhasil') {
                        $('#formUpload').trigger("reset");
                        $('#resumeModal').modal('hide');
                        window.location.href = "{{ session('URL_PWD') }}";
                    }
                    noti(data.icon, data.success);
                    $('#importBtn').html('Simpan');
                    // location.reload(true);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#importBtn').html('Simpan');
                }
            });
        });

        function deleteDokumen(id) {
            if (id) {
                var confirmdelete = confirm("Do you really want remove data?");
                if (confirmdelete == true) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('program/bursa_kerja/dokumen/hard_delete') }}",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('.delete_dokumen').html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload(true);
                            }
                            swa(data.status + "!", data.message, data.success);

                        }
                    });
                }

            }
        }
    </script>
@endsection
