@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="tab-pane active" id="home-tab-v1">
        <h5 class="box-title"><i class="fa fa-user"></i> Profil Saya &nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:void(0)"
                onclick="tampilEdit()" id="aksi_hide"><i class="fa fa-edit"></i></a>
        </h5>
        <table id="data-profile" style="display: block">
            <tr>
                <td width="200px">Nama</td>
                <td>: {{ $data['nama'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>: {{ $data['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Nomor Telepon</td>
                <td>: {{ $data['telepon'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>: {{ $data['email'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>: {{ $data['alamat'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Tempat, Tanggal lahir</td>
                <td>: {{ $data['tempat_lahir'] . ', ' . (new \App\Helpers\Help())->getTanggal($data['tgl_lahir']) }}</td>
            </tr>
        </table>
        <div id="page_edit" style="display: none">
            <form action="javascript:void(0)" id="formProfile">
                @csrf
                <div class="row mr-b-50">
                    <div class="col-md-12 mb-3">
                        <label for="validationServer01">Nama</label>
                        <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{ $data['nama'] }}"
                            required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer01">Tempat Lahir</label>
                        <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir"
                            value="{{ $data['tempat_lahir'] }}" required>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="validationServer03">Tanggal Lahir</label>
                        <select name="tanggal" class="form-control select3" id="">
                            @php
                                $day_selected = date('d', strtotime($data['tgl_lahir']));
                            @endphp
                            <option value="">Tanggal</option>
                            <option value="01" {{ $day_selected == '01' ? 'selected' : '' }}>01</option>
                            <option value="02" {{ $day_selected == '02' ? 'selected' : '' }}>02</option>
                            <option value="03" {{ $day_selected == '03' ? 'selected' : '' }}>03</option>
                            <option value="04" {{ $day_selected == '04' ? 'selected' : '' }}>04</option>
                            <option value="05" {{ $day_selected == '05' ? 'selected' : '' }}>05</option>
                            <option value="06" {{ $day_selected == '06' ? 'selected' : '' }}>06</option>
                            <option value="07" {{ $day_selected == '07' ? 'selected' : '' }}>07</option>
                            <option value="08" {{ $day_selected == '08' ? 'selected' : '' }}>08</option>
                            <option value="09" {{ $day_selected == '09' ? 'selected' : '' }}>09</option>
                            @php
                                for ($day = 10; $day <= 31; $day++) {
                                    echo '<option value=' . $day . ' ' . $day_selected == $day ? 'selected' : '' . '>' . $day . '</option>';
                                }
                            @endphp

                        </select>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="validationServer03">&nbsp;</label>
                        @php
                            $month_selected = date('m', strtotime($data['tgl_lahir']));
                        @endphp
                        <select name="bulan" class="form-control select3" id="">
                            <option value="">Bulan</option>
                            <option value="01" {{ $month_selected == '01' ? 'selected' : '' }}>Januari</option>
                            <option value="02" {{ $month_selected == '02' ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ $month_selected == '03' ? 'selected' : '' }}>Maret</option>
                            <option value="04" {{ $month_selected == '04' ? 'selected' : '' }}>April</option>
                            <option value="05" {{ $month_selected == '05' ? 'selected' : '' }}>Mei</option>
                            <option value="06" {{ $month_selected == '06' ? 'selected' : '' }}>Juni</option>
                            <option value="07" {{ $month_selected == '07' ? 'selected' : '' }}>Juli</option>
                            <option value="08" {{ $month_selected == '08' ? 'selected' : '' }}>Agustus</option>
                            <option value="09" {{ $month_selected == '09' ? 'selected' : '' }}>September</option>
                            <option value="10" {{ $month_selected == '10' ? 'selected' : '' }}>Oktober</option>
                            <option value="11" {{ $month_selected == '11' ? 'selected' : '' }}>November</option>
                            <option value="12" {{ $month_selected == '12' ? 'selected' : '' }}>Desember</option>
                        </select>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="validationServer03">&nbsp;</label>
                        <select name="tahun" class="form-control select3" id="">
                            <option value="">Tahun</option>
                            {{ $year_selected = (int) date('Y', strtotime($data['tgl_lahir'])) }}
                            {{ $firstYear = (int) date('Y') }}
                            {{ $lastYear = $firstYear - 50 }}

                            @for ($year = $firstYear; $year >= $lastYear; $year--)
                                <option value="{{ $year }}" {{ $year_selected == $year ? 'selected' : '' }}>
                                    {{ $year }}</option>
                            @endfor

                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer04">Jenis Kelamin</label>
                        <select name="jenkel" class="form-control" id="">
                            <option value="l" {{ $data['jenkel'] == 'Laki-laki' ? 'selected' : '' }}>Laki - laki</option>
                            <option value="p" {{ $data['jenkel'] == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ $data['email'] }}"
                            placeholder="Email">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Telepon</label>
                        <input type="text" class="form-control" name="telepon" value="{{ $data['telepon'] }}"
                            placeholder="telepon">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Alamat sekarang</label>
                        <input type="text" class="form-control" name="alamat" value="{{ $data['alamat'] }}"
                            placeholder="telepon">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Kecamatan</label>
                        <input type="text" class="form-control" placeholder="telepon">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Kota</label>
                        <input type="text" class="form-control" placeholder="telepon">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationServer05">Kode Pos</label>
                        <input type="text" class="form-control" placeholder="telepon">
                    </div>
                    <div class="col-sm-12 btn-list">
                        <button type="submit" class="btn btn-success" id="btn-update"><i class="fa fa-refresh"></i> Update</button>
                        <a href="{{ url('program/bursa_kerja/profile/edit') }}" class="btn btn-info" id="btn-cancel">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        function tampilEdit() {
            $('#page_edit').show();
            $('#data-profile').hide();
            $('#aksi_hide').hide();
        }


        $('#formProfile').on('submit', function(event) {
            $('#btn-update').html('Sending..');
            event.preventDefault();
            $.ajax({
                url: "{{ url('program/bursa_kerja/profile/update') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $("#btn-update").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                        $("#btn-update").attr("disabled", true);
                },
                success: function(data) {
                    $('#btn-update').html('<i class="fa fa-refresh"></i> Update');
                    noti(data.icon, data.success);
                    $("#btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btn-update').html('Simpan');
                }
            });
        });

    </script>
@endsection
