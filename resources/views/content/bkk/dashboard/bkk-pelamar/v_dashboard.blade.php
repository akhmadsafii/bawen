@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        #loading {
            text-align: center;
            background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
        }

        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .error-actions .btn {
            margin-right: 10px;
        }

        .well {
            min-height: 20px;
            padding: 0px;
            margin-bottom: 20px;
            background-color: #D9D9D9;
            border: 1px solid #D9D9D9;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            padding-left: 15px;
            border: 0px;
        }

        .thumbnail .caption {
            padding: 9px;
            color: #333;
            /* padding-left: 0px;
                                    padding-right: 0px; */
        }

        .icon-style {
            margin-right: 15px;
            font-size: 18px;
            margin-top: 20px;
        }

        p {
            margin: 3px;
        }

        .well-add-card {
            margin-bottom: 10px;
        }

        .btn-add-card {
            margin-top: 20px;
        }

        .thumbnail {
            display: block;
            padding: 4px;
            margin-bottom: 20px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 6px solid #D9D9D9;
            border-radius: 15px;
            -webkit-transition: border .2s ease-in-out;
            -o-transition: border .2s ease-in-out;
            transition: border .2s ease-in-out;
            padding-left: 0px;
            padding-right: 0px;
        }

        .btn {
            border-radius: 0px;
        }

        .btn-update {
            margin-left: 15px;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Lowongan Pekerjaan</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Bursa Kerja</li>
                <li class="breadcrumb-item active">Lowongan pekerjaan</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="search">
                    <form id="search-form" method="POST" action="javascript:void(0)">
                        @csrf
                        <div class="row" id="search">
                            <div class="form-group col-md-9">
                                <input class="form-control" type="text" name="judul"
                                    placeholder="Masukan nama pekerjaan" />
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" class="btn btn-block btn-search"
                                    style="border-radius: 10px; background: rgb(201, 10, 89); color: #fff"><i
                                        class="fa fa-search"></i> Cari Pekerjaan</button>
                            </div>
                        </div>
                    </form>
                    <form>
                        <div class="row" id="filter">
                            <div class="form-group col-md-6 col-xs-6">
                                <select data-filter="make" name="provinsi" class="filter-make filter form-control select3">
                                    <option value="">Select Provinsi</option>
                                    @foreach ($provinsi as $pro)
                                        <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-xs-6">
                                <select data-filter="type" name="bidang" class="filter-type filter form-control">
                                    <option value="">Select Bidang</option>
                                    @foreach ($bidang as $bi)
                                        <option value="{{ $bi['id'] }}">{{ $bi['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>

                <section>
                    <h3 class="title-h1">Lowongan Kerja </h3>
                    <h5 class="countlist" style="" data-lowongan="Lowongan Kerja ">Tersedia
                        <span>{{ count($loker) }}</span> Lowongan
                        Kerja
                    </h5>
                </section>
                <div class="row" id="products">

                    {{-- @foreach ($loker as $lk)
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                            <a href="{{ route('bkk_loker-get_detail', $lk['id_encode']) }}">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <div class="row" style="height: 121px">
                                            <div class="col-lg-6" style="display: flex; justify-content: center; align-items: center;">
                                                <img src="{{ $lk['file_industri'] }}" alt="Image 1">
                                            </div>
                                            <div class="col-lg-6">
                                                <i class="fa fa-bookmark" style="float: right; font-size: 23px;"></i>
                                            </div>
                                        </div>
                                        <div class='col-lg-12 well well-add-card'>
                                            <h4 class="mt-0">{{ $lk['judul'] }}</h4>
                                        </div>
                                        <div class='col-lg-12'>
                                            <p>{{ $lk['industri'] }}</p>
                                            <p class"text-muted">Exp: 12-08</p>
                                        </div>
                                        <div class='col-lg-12'>
                                            <p>{{ $lk['diposting'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    @endforeach --}}
                </div>
            </div>
        </div>


    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // $(document).ready(function() {
        //     $('.customer-logos').slick({
        //         slidesToShow: 6,
        //         slidesToScroll: 1,
        //         autoplay: true,
        //         autoplaySpeed: 1500,
        //         arrows: false,
        //         dots: false,
        //         pauseOnHover: false,
        //         responsive: [{
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 4
        //             }
        //         }, {
        //             breakpoint: 520,
        //             settings: {
        //                 slidesToShow: 3
        //             }
        //         }]
        //     });
        // });
        call_ajax();

        $('select[name="bidang"]').on('change', function() {
            call_ajax();
        })

        $('select[name="provinsi"]').on('change', function() {
            call_ajax();
        })

        $('.btn-search').on('click', function() {
            call_ajax();
        })
        var id_provinsi = null;
        var id_bidang = null;
        var judul = null;

        function call_ajax(id = "", params = "") {
            id_provinsi = $('select[name="provinsi"]').val();
            id_bidang = $('select[name="bidang"]').val();
            judul = $('input[name="judul"]').val();
            $.ajax({
                url: "/program/bursa_kerja/lowongan/search",
                method: "POST",
                data: {
                    id: id,
                    id_provinsi,
                    id_bidang,
                    judul,
                },
                success: function(data) {
                    $('#load_more_button').remove();
                    if (data.count != 0) {
                        if (params) {
                            $('#products').append(data.html);
                        } else {
                            $('#products').html(data.html);
                            $('.countlist').html('Tersedia <span>' + data.count + '</span> Lowongan Kerja');
                        }

                    } else {
                        $('#products').html(
                            '<div class="col-md-12"><div class="error-template"><i class="fas fa-exclamation-circle fa-4x text-danger"></i><h2 class="text-danger">Kesalahan Dalam Pencarian!</h2><div class="error-details">Maaf, data yang anda cari tidak tersedia. silahkan cari lagi dengan keywoard yang lain</div></div></div>'
                        );
                        $('.countlist').html('Tersedia <span>' + data.count + '</span> Lowongan Kerja');
                    }
                }
            })
        }

        $(document).on('click', '#load_more_button', function() {
            var id = $(this).data('id');
            $('#load_more_button').html('<b>Loading...</b>');
            call_ajax(id, "app");
        });
    </script>
@endsection
