@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-9">
                        <img src="{{ $loker['file_industri'] }}" alt="Image 1" height="90">
                        <h4 class="mt-0">{{ $loker['judul'] }}</h4>
                        <p class="mb-0"><a href="#" class="text-info">{{ $loker['industri'] }}</a> </p>
                        <p class="mb-0">{{ $loker['kabupaten'] != null ? ucwords($loker['kabupaten']) : '-' }}
                        </p>
                        <p class="mb-0">Diposting {{ $loker['diposting'] }}</p>
                        <small>{{ $loker['pelamar'] }} Pelamar yang mendaftar</small>
                    </div>
                    <div class="col-md-3" style="position: relative;">
                        <div class="vertical-center"
                            style="width: 80%; position: absolute; top: 50%; -ms-transform: translateY(-50%); transform: translateY(-50%);">
                            @if ($loker['sudah_melamar'] == 'sudah')
                                <a href="javascript:void(0)" class="btn btn-info btn-block"
                                    style="cursor: not-allowed; background-color: #ff5a39; border-color: #ff5a39">Anda Sudah
                                    Melamar</a>
                            @else
                                <a href="{{ url('program/bursa_kerja/pelamar/create', $loker['id_code']) }}"
                                    class="btn btn-info btn-block">Aplly Now</a>
                            @endif
                            <button class="btn btn-default btn-block" style="border: 1px solid #ff5a39; color: #ff5a39;"><i
                                    class="fa fa-bookmark"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mt-0">Deskripsi Lowongan</h4>
                        <h6><b>Deskripsi : </b></h6>
                        <p class="text-justify">{{ $loker['deskripsi'] != null ? $loker['deskripsi'] : '-' }}</p>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h6><b>Persyaratan : </b></h6>
                        <p class="text-justify">{{ $loker['syarat'] != null ? $loker['syarat'] : '-' }}</p>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h4 class="mt-0">Informasi Tambahan</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><b>Kisaran Gaji : </b></h6>
                                <p>{{ number_format($loker['gaji_awal']) }} -
                                    {{ number_format($loker['gaji_sampai']) }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Tanggal Lowongan : </b></h6>
                                <span>Tanggal Buka {{ (new \App\Helpers\Help())->getTanggal($loker['tgl_buka']) }} -
                                    {{ (new \App\Helpers\Help())->getTanggal($loker['tgl_tutup']) }}</span>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h4 class="mt-0">Informasi Perusahaan</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><b>Perusahaan : </b></h6>
                                <p>{{ $loker['industri'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Alamat : </b></h6>
                                <p>{{ $loker['alamat'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Deskripsi Perusahaan : </b></h6>
                                <p>{{ $loker['deskripsi_perusahaan'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Kabupaten : </b></h6>
                                <p>{{ $loker['kabupaten'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($loker['file'] != null)
            <div class="col-md-3">
                <div class="card">
                    <div class="image-container">
                        <div class="first">
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="wishlist"><i class="fa fa-heart-o"></i></span>
                            </div>
                        </div> <img src="{{ $loker['file'] }}" class="img-fluid rounded thumbnail-image">
                    </div>
                    <div class="product-detail-container p-2">
                        <center>
                            <div style="text-align: center" class="d-flex justify-content-between align-items-center">
                                <h5 class="dress-name m-0">Flyer dari lowongan terkait</h5>
                                <a href="{{ $loker['file'] }}" target="_blank" download><span><i
                                            class="fa fa-download"></i></span></a>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
