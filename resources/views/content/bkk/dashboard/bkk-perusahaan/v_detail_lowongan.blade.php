@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .footer {
            position: fixed !important;
            left: 0;
            bottom: 0;
            width: 100%;
        }

        #loading {
            text-align: center;
            background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
        }

        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .error-actions .btn {
            margin-right: 10px;
        }

        .profile-header {
            position: relative;
            overflow: hidden
        }

        .profile-header .profile-header-cover {
            background-color: #00b5ec;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0
        }

        .profile-header .profile-header-cover:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0, rgba(0, 0, 0, .75) 100%)
        }

        .profile-header .profile-header-content {
            color: #fff;
            padding: 25px
        }

        .profile-header-img {
            float: left;
            width: 120px;
            height: 120px;
            overflow: hidden;
            position: relative;
            z-index: 10;
            margin: 0 0 -20px;
            padding: 3px;
            border-radius: 4px;
            background: #fff
        }

        .profile-header-img img {
            max-width: 100%
        }

        .profile-header-info h4 {
            font-weight: 500;
            color: #fff
        }

        .profile-header-img+.profile-header-info {
            margin-left: 140px
        }

        .profile-header .profile-header-content,
        .profile-header .profile-header-tab {
            position: relative
        }

        section.row.custom-scroll-content.scrollbar-enabled {
            overflow-y: scroll;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 style="margin: 4px 0;">{{ ucwords($loker['judul']) }}</h4>
                                </div>
                                <div class="col-md-8">
                                    <ul class="nav nav-tabs" style="float: right">
                                        <li class="nav-item active"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataPelamar('belum_proses')" data-toggle="tab"
                                                aria-expanded="true">Belum
                                                diproses ({{ $loker['belum_diproses'] }})</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataPelamar('terpilih')" data-toggle="tab"
                                                aria-expanded="true">Terpilih ({{ $loker['terpilih'] }})</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataPelamar('wawancara')" data-toggle="tab"
                                                aria-expanded="true">Wawancara ({{ $loker['wawancara'] }})</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataPelamar('tidak_sesuai')" data-toggle="tab"
                                                aria-expanded="true">Tidak
                                                Sesuai ({{ $loker['ditolak'] }})</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataPelamar('diterima')" data-toggle="tab"
                                                aria-expanded="true">Diterima ({{ $loker['diterima'] }})</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 widget-holder">
                <div class="">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <div class="tab-content p-0">
                                <div class="tab-pane active" id="home-tab-bordered-1">
                                    <div class="row">

                                        <div class="col-xs-12 col-md-12 mt-3">
                                            <div class="row">
                                                <div class="col-md-6 pl-0">
                                                    <div class="well well-sm pl-3 pb-0">
                                                        <div class="form-check">
                                                            <input class="form-check-input ml-0" type="checkbox" value=""
                                                                id="defaultCheck1">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                Pilih Semua
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="well well-sm pl-3 pb-0 float-right">
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                Urut Berdasarkan
                                                            </label>
                                                            <select name="" id="">
                                                                <option value="">Paling Relevan</option>
                                                                <option value="">Paling Sering dicari</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="tampilanPelamar">
                                                @foreach ($pelamar as $pel)
                                                    <div class="col-xs-12 col-md-6 mt-3 pr-0">
                                                        <div class="well well-sm widget-bg p-3">
                                                            <div class="row m-0">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 10%; vertical-align: top;">
                                                                            <input class="form-check-input ml-0"
                                                                                type="checkbox" value="" id="defaultCheck1">
                                                                        </td>
                                                                        <td>
                                                                            <h5 class="card-title mb-1">
                                                                                {{ ucwords($pel['nama']) }}</h5>
                                                                            <a href="javascript:void(0)"
                                                                                data-id="{{ $pel['id'] }}"
                                                                                id="detail">lihat detail</a>
                                                                            @if (!empty($pel['pengalaman']))
                                                                                @foreach ($pel['pengalaman'] as $pengalaman)
                                                                                    <p class="mb-0">
                                                                                        {{ ucwords($pengalaman['nama']) }}
                                                                                        ({{ (new \App\Helpers\Help())->getMonthYear($pengalaman['tgl_mulai']) .' - ' .(new \App\Helpers\Help())->getMonthYear($pengalaman['tgl_akhir']) }})
                                                                                    </p>
                                                                                    <p>{{ ucwords($pengalaman['industri']) }}
                                                                                    </p>
                                                                                @endforeach
                                                                            @else
                                                                                <div style="min-height: 100px;">
                                                                                    <p>Belum punya penglaman (Fresh
                                                                                        Graduate)</p>
                                                                                </div>
                                                                            @endif
                                                                            <i class="fa fa-phone"></i>
                                                                            {{ $pel['telepon'] }} |
                                                                            <i class="fa fa-envelope"></i>
                                                                            {{ $pel['email'] }} |
                                                                            <i class="fa fa-dollar"></i> IDR 2,500,000
                                                                            <div class="nextAksi mt-2">
                                                                                <a href="javascript:void(0)"
                                                                                    onclick="movePilih('{{ $pel['id_code'] }},terpilih')"
                                                                                    id="terpilih">Terpilih</a>
                                                                                &nbsp;&nbsp;<a href="javascript:void(0)"
                                                                                    onclick="movePilih('{{ $pel['id_code'] }},wawancara')"
                                                                                    id="wawancara">Wawancara</a>
                                                                                &nbsp;&nbsp;
                                                                                <a href="javascript:void(0)"
                                                                                    onclick="movePilih('{{ $pel['id_code'] }},tidak_sesuai')"
                                                                                    id="tidak_sesuai">Tidak
                                                                                    sesuai</a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="detailModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12" id="profilDetail">
                        <div id="content" class="content content-full-width">
                            <div class="profile">
                                <div class="profile-header">
                                    <div class="profile-header-cover"></div>
                                    <div class="profile-header-content">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <div class="profile-header-img m-0">
                                                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png"
                                                            alt="">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="profile-header-info p-2">
                                                        <h4 class="mt-0 m-b-5">John Doe</h4>
                                                        <p class="mb-0">Web And Frontend Developer</p>
                                                        <p class="m-b-10">PT Trisula Indonesia</p>
                                                        <p class="mb-0"><i class="fa fa-phone"></i> (+62)
                                                            89626040205 | <i class="fa fa-envelope"></i>
                                                            akhmadsafii96@gmail.com | <i class="fa fa-dollar"></i> IDR
                                                            2,500,000</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="profile-aksi-info">
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <button type="button"
                                                                class="btn btn-info btn-sm">Wawancara</button>
                                                            <button type="button" class="btn btn-danger btn-sm">Tidak
                                                                sesuai</button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-content">
                                <div class="tab-content p-0">
                                    <section class="row custom-scroll-content scrollbar-enabled" style="height: 470px;">
                                        <div class="tab-pane fade in active show" id="profile-about"
                                            style="width: 100%; padding: 15px;">
                                            <h5 class="box-title"><i class="fa fa-shopping-bag"></i> Pengalaman
                                            </h5>
                                            <div>
                                                <div class="table-responsive">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 20%">Tingkat Pengalaman</td>
                                                            <td>Saya telah bekerja sejak Februari 2019
                                                            <td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                    <table style="width: 100%">
                                                        <tbody id="outputPengalaman">
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: top">
                                                                    Januari 2021 - Maret 2021
                                                                    <br><small>6 months</small>
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <h5 class="mt-0">Front End Developer</h5>
                                                                    <b>Bataianet
                                                                        | DI YOGYAKARTA, INDONESIA</b>
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>Industri</td>
                                                                            <td>Travel / Pariwisata</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Spesialisasi</td>
                                                                            <td>IT/Komputer - Perangkat Keras</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Bidang pekerjaan</td>
                                                                            <td>Teknisi Perangkat Keras Komputer</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Jabatan</td>
                                                                            <td>Pegawai (non-manajemen & non-supervisor)
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gaji bulanan</td>
                                                                            <td>IDR 1,600,000</td>
                                                                        </tr>
                                                                    </table>
                                                                    <small>Job desc : saya sebagai</small>
                                                                <td>
                                                            </tr>
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: top">
                                                                    Januari 2020 - Februari 2021
                                                                    <br><small>6 months</small>
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <h5 class="mt-0">It Support</h5>
                                                                    <b>Carmilla Java
                                                                        | JAWA TENGAH, INDONESIA</b>
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>Industri</td>
                                                                            <td>Travel / Pariwisata</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Spesialisasi</td>
                                                                            <td>IT/Komputer - Perangkat Keras</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Bidang pekerjaan</td>
                                                                            <td>Teknisi Perangkat Keras Komputer</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Jabatan</td>
                                                                            <td>Pegawai (non-manajemen & non-supervisor)
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gaji bulanan</td>
                                                                            <td>IDR 1,600,000</td>
                                                                        </tr>
                                                                    </table>
                                                                    <small>Job desc : saya sebagi it</small>
                                                                <td>
                                                            </tr>
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: top">
                                                                    Januari 2019 - Juli 2021
                                                                    <br><small>6 months</small>
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <h5 class="mt-0">Web Dveloper</h5>
                                                                    <b>Trisula Indonesia
                                                                        | DI YOGYAKARTA, INDONESIA</b>
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>Industri</td>
                                                                            <td>Travel / Pariwisata</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Spesialisasi</td>
                                                                            <td>IT/Komputer - Perangkat Keras</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Bidang pekerjaan</td>
                                                                            <td>Teknisi Perangkat Keras Komputer</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Jabatan</td>
                                                                            <td>Pegawai (non-manajemen & non-supervisor)
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gaji bulanan</td>
                                                                            <td>IDR 1,600,000</td>
                                                                        </tr>
                                                                    </table>
                                                                    <small>Job desc : saya sebagai web</small>
                                                                <td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <h5 class="box-title"><i class="fa fa-shopping-bag"></i> Resume/CV
                                            </h5>
                                            <div>
                                                <div class="table-responsive">
                                                    <table style="width: 100%">
                                                        <tbody id="outputPengalaman">
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: middle; width: 20%">
                                                                    portfolio.pdf
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <a href="http://localhost:8000/program/bursa_kerja/dokumen/download/jR"
                                                                        class="btn btn-info btn-success btn-sm"><i
                                                                            class="fa fa-eye"></i> Lihat</a>
                                                                <td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.confirmation', function() {
                return confirm('Apa kamu yakin ingin menerima pelamar tersebut?');
            });

            $(document).on('click', '#detail', function() {
                var id = $(this).data('id');
                var efek_load = $(this);
                // $('#detailModal').modal('show');
                console.log(id);
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/bursa_kerja/pelamar/detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(efek_load).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $('#profilDetail').html(data);
                        $(efek_load).html(
                            'Lihat Detail');
                        $('#detailModal').modal('show');
                    }
                });
            });
            dataPelamar('belum_proses');
        })


        function dataPelamar(params) {
            // console.log(params);
            $('#tampilanPelamar').html('<div id="loading" style="" ></div>');
            $.ajax({
                url: "{{ route('bkk_pelamar-status_by_perusahaan') }}",
                type: "POST",
                data: {
                    params,
                    id_loker: "{{ $loker['id_code'] }}",
                },
                success: function(data) {
                    if (data.count != 0) {
                        $('#tampilanPelamar').html(data.html);
                    } else {
                        $('#tampilanPelamar').html(
                            '<div class="col-md-12"><div class="error-template"><h1>Oops!</h1><h2>404 Data Not Found</h2><div class="error-details">Sorry, an error has occured, Requested page not found!</div><div class="error-actions"><a href="http://www.jquery2dotnet.com" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a><a href="http://www.jquery2dotnet.com" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a></div></div></div>'
                        );
                    }
                }
            });

        }

        function movePilih(params) {
            if (params) {
                var res = params.split(",");
                var id_code_pelamar = res[0];
                var aksi = res[1];
                var tombol = $(this);
                console.log(tombol);
                var confirmUpdate = confirm("Apa kamu yakin ingin melanjutkan proses data?");
                if (confirmUpdate == true) {
                    $.ajax({
                        url: "{{ route('bkk_pelamar-update_status') }}",
                        type: "POST",
                        data: {
                            id_code_pelamar,
                            aksi
                        },
                        beforeSend: function() {
                            $(tombol).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload(true);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data.success);

                        }
                    });
                }
            }
        }
    </script>
@endsection
