@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">Hi. Kamu mempunyai beberapa agenda</h5>
                        <hr>
                        <div class="row" style="margin-top: 15px">
                            <div class="col-sm-12 col-md-6 col-lg-4 my-auto">
                                <img src="{{ asset('images/agenda.png') }}" alt="">
                                <br>
                                <p class="m-0 text-center">*Klik Agenda untuk melihat detail</p>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-8">
                                <div class="widget-bg-transparent">
                                    <div class="widget-body clearfix">
                                        <div id='calendar'> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <center>
                        <img src="{{ asset('images/agenda.png') }}" alt="" width="200">
                        <h3 class="box-title" id="title">Kumpulan RT</h3>
                    </center>
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="form-group row mb-0">
                                <label class="col-md-5 col-form-label">Loker</label>
                                <div class="col-md-7">
                                    <p class="form-control-plaintext" id="loker">email@example.com</p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-5 col-form-label">Lokasi</label>
                                <div class="col-md-7">
                                    <p class="form-control-plaintext" id="lokasi">email@example.com</p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-5 col-form-label">Tgl Mulai</label>
                                <div class="col-md-7">
                                    <p class="form-control-plaintext" id="mulai">email@example.com</p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-5 col-form-label">Tgl Selesai</label>
                                <div class="col-md-7">
                                    <p class="form-control-plaintext" id="selesai">email@example.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                var calendar = $('#calendar').fullCalendar({
                    editable: true,
                    events: function(start, end, timezone, callback) {
                        $.ajax({
                            url: "",
                            type: "GET",
                            success: function(obj) {
                                var events = [];
                                $.each(obj, function(index, value) {
                                    events.push({
                                        id: value['id'],
                                        loker: value['judul'],
                                        start: value['tgl_mulai'],
                                        mulai: value['tgl_mulai'],
                                        end: value['tgl_selesai'],
                                        selesai: value['tgl_selesai'],
                                        title: value['nama'],
                                        lokasi: value['alamat']
                                    });
                                });
                                callback(events);
                            }

                        });
                    },
                    displayEventTime: false,
                    eventColor: '#2471d2',
                    eventTextColor: '#FFF',
                    editable: false,
                    eventRender: function(event, element, view) {
                        element.children().last().append(
                            "<br><span class='loker'>Lokasi : " +
                            event
                            .lokasi + "</span>");
                        if (event.allDay === 'true') {
                            event.allDay = true;
                        } else {
                            event.allDay = false;
                        }
                    },
                    eventClick: function(event, jsEvent, view) {
                        // console.log(event);
                        $('#modelHeading').html('Detail Agenda');
                        $('#loker').html(event.loker);
                        $('#title').html(event.title);
                        $('#lokasi').html(event.lokasi);
                        $('#mulai').html(event.mulai);
                        $('#selesai').html(event.selesai);
                        $('#title').html(event.title);
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
