@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .pace {
            display: none;
        }

        .iconic {
            padding: 8px;
            background: #ffffff;
            margin-right: 4px;
        }

        .icon-btn {
            padding: 1px 15px 3px 2px !important;
            border-radius: 50px !important;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="alert alert-info alert-dismissible fade show mt-2" role="alert">
        <center>
            <strong><i class="fas fa-info-circle"></i> Lengkapi detail perusahaanmu!</strong>
            <br>
            pastikan anda melengkapi detail perusahaan, agar meyakinkan pelamar untuk melamar di perusahaan anda. Untuk
            melengkapinya anda bisa klik <a href="{{ route('bkk_industi-detail') }}" class="text-danger">disini</a>.
        </center>
    </div>
    <div id="wrapper" class="row wrapper multi-step-signup" style="display: block !important">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <ul class="nav nav-tabs justify-content-center">
                    <li>
                        <a class="nav-link btn-purple m-1 icon-btn active" style="pointer-events:none" href="#lowongan"
                            data-toggle="tab">
                            <i class="fas fa-pencil-alt iconic text-dark rounded-circle"></i></span>
                            Tulis Lowongan
                        </a>
                    </li>
                    <li>
                        <a class="nav-link btn-purple m-1 icon-btn" href="#perusahaan" style="pointer-events:none"
                            data-toggle="tab">
                            <i class="fas fa-industry iconic text-info rounded-circle"></i></span>
                            Deskripsi Perusahaan
                        </a>
                    </li>
                    <li>
                        <a class="nav-link btn-purple m-1 icon-btn" style="pointer-events:none" href="#finish"
                            data-toggle="tab">
                            <i class="fas fa-check-circle iconic text-success rounded-circle"></i></span>
                            Selesai
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="widget-list">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="widget-bg">
                    <form id="formLoker">
                        <div class="tab-content pt-0">
                            <div class="tab-pane active" id="lowongan">
                                <h5 class="box-title mr-b-0">Mohon lengkapi detail pekerjaanmu</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l30">Judul Pekerjaan?</label>
                                            <input class="form-control" id="judul_loker" name="judul" type="text">
                                        </div>
                                        <div class="form-group">
                                            <p class="mb-0">
                                                <small>Apakah ingin memposting dari situs lain?.</small>
                                            </p>
                                            <label for="l30">Link Pekerjaan</label>
                                            <input class="form-control" placeholder="https://sample.com" id="l30"
                                                name="link" type="text">

                                        </div>
                                        <div class="form-group">
                                            <label for="l30">Deskripsi pekerjaan</label>
                                            <p class="mb-0">Tulis atau copy paste deskripsi dari lowongan
                                                pekerjaan
                                                anda, agar
                                                pelamar memahaminya.</p>
                                            @if (Request::segment(4) == 'edit')
                                                <textarea class="form-control" name="deskripsi" id="l38" rows="3">{{ $loker['deskripsi'] }}</textarea>
                                            @else
                                                <textarea class="form-control" name="deskripsi" id="l38" rows="3"></textarea>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Tanggal Buka Loker</label>
                                            <div class="input-daterange input-group datepicker">
                                                @if (Request::segment(4) == 'edit')
                                                    <input type="text" class="form-control" name="tgl_buka"
                                                        value="{{ date('d-m-Y', strtotime($loker['tgl_buka'])) }}"> <span
                                                        class="input-group-addon bg-info text-inverse">s/d</span>
                                                    <input type="text" class="form-control" name="tgl_tutup"
                                                        value="{{ date('d-m-Y', strtotime($loker['tgl_tutup'])) }}">
                                                @else
                                                    <input type="text" class="form-control" name="tgl_buka"
                                                        value="{{ date('d-m-Y') }}"> <span
                                                        class="input-group-addon bg-info text-inverse">s/d</span>
                                                    <input type="text" class="form-control" name="tgl_tutup"
                                                        value="{{ date('d-m-Y') }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l31">Kisaran Gaji</label>
                                            <p class="mb-0">Kandidat akan menyukai jika anda menambahkan kisaran
                                                gaji
                                                di
                                                lowongan pekerjaan ini.</p>
                                            <label for="l31">Berapa besaran kisaran gaji untuk pekerjaan ini?</label>
                                            <div class="input-group">
                                                @if (Request::segment(4) == 'edit')
                                                    <input type="text" class="form-control ribuan" name="gaji_awal"
                                                        value="{{ number_format($loker['gaji_awal']) }}"> <span
                                                        class="input-group-addon bg-info text-inverse">s/d</span>
                                                    <input type="text" class="form-control ribuans" name="gaji_sampai"
                                                        value="{{ number_format($loker['gaji_sampai']) }}">
                                                @else
                                                    <input type="text" class="form-control ribuan" name="gaji_awal"> <span
                                                        class="input-group-addon bg-info text-inverse">s/d</span>
                                                    <input type="text" class="form-control ribuans" name="gaji_sampai">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l30">Apa bidang dari lowongan pekerjaan</label> <br>
                                            <small>Ceritakan kepada pelamar tentang bidang dari lowongan pekerjaan
                                                anda.</small>
                                            <select name="id_bidang_loker[]" class="form-control select2" multiple>
                                                <option value="">-- Pilih Bidang Pekerjaan --</option>
                                                @foreach ($bidang as $bd)
                                                    @if (Request::segment(4) == 'edit')
                                                        <option value="{{ $bd['id'] }}"
                                                            {{ $bd['id'] == $loker['id_bidang'] ? 'selected' : '' }}>
                                                            {{ $bd['nama'] }}</option>
                                                    @else
                                                        <option value="{{ $bd['id'] }}">{{ $bd['nama'] }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="l30">Syarat</label>
                                            <p class="mb-0">Tulislah persyaratan persyaratan untuk menemukan
                                                kandidat
                                                pelamar yg
                                                sesuai</p>
                                            @if (Request::segment(4) == 'edit')
                                                <textarea class="form-control" id="l38" name="syarat" rows="3">{{ $loker['syarat'] }}</textarea>
                                            @else
                                                <textarea class="form-control" id="l38" name="syarat" rows="3"></textarea>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="l30">Kontak Recruitment?</label>
                                            <p class="mb-0">bisa diisi dengan nomor telepon, email, dll.</p>
                                            <label for="l31">Pastikan kontak yang anda inputkan bisa dihungi</label>
                                            @if (Request::segment(4) == 'edit')
                                                <input type="hidden" name="id" value="{{ $loker['id'] }}">
                                                <input class="form-control" id="l30" name="recruitment" type="text"
                                                    value="{{ $loker['recruitment'] }}">
                                            @else
                                                <input class="form-control" id="l30" name="recruitment" type="text">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="l30">Kouta</label>
                                            <p class="mb-0">Kuota karyawan yang dibutuhkan untuk posisi ini.</p>
                                            @if (Request::segment(4) == 'edit')
                                                <input class="form-control" id="l30" name="quota" type="number"
                                                    value="{{ $loker['quota'] }}">
                                            @else
                                                <input class="form-control" id="l30" name="quota" type="number">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="l30">File Flyer?</label>
                                            <p class="mb-0">bisa diisi dengan gambar brosur, flyer atau yang
                                                lainnya.
                                            </p>
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <img id="modal-preview" src="https://via.placeholder.com/200x100" alt="Preview"
                                                class="form-group" width="200" style="margin-top: 10px">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <a class="btn btn-success float-right" href="javascript:void(0)"
                                            onclick="activaTab('perusahaan')">Selanjutnya <i
                                                class="fas fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="perusahaan">
                                <div class="form-group">
                                    <label for="l30">informasi singkat perusahaanmu</label>
                                    <p class="">Jelaskan secara singkat perusahaanmu tentang lowongan ini.</p>
                                    <textarea class="form-control" name="deskripsi_perusahaan" rows="3"></textarea>
                                </div>
                                <a class="btn btn-danger" href="javascript:void(0)" onclick="activaTab('lowongan')"><i
                                        class="fas fa-angle-double-left"></i> Kembali</a>
                                <button type="submit" class="btn btn-success float-right" id="btnSelesai"> <i
                                        class="fas fa-check-double"></i>
                                    Selesai</button>
                            </div>
                            <div class="tab-pane" id="finish">
                                <div class="selesai">
                                    <center>
                                        <i class="far fa-check-circle fa-9x text-success"></i>
                                        <h3 class="m-0 text-success">SELAMAT !</h3>
                                        <p class="text-success"><b> Lowongan dengan judul <a class="text-danger"
                                                    id="template_judul">Cireng
                                                    Maju
                                                    Makmur</a> berhasil tersimpan</b></p>
                                        <a class="btn btn-info" href="javascript:void(0)"
                                            onclick="activaTab('lowongan')"><i class="fas fa-pencil-alt"></i> Buat Postingan
                                            Lagi</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            activaTab('lowongan');

            $('body').on('submit', '#formLoker', function(e) {
                $('#btnSelesai').html('<i class="fa fa-spin fa-sync-alt"></i> Memproses..');
                $("#btnSelesai").attr("disabled", true);
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('lowongan-save_with_notif') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            activaTab('finish');
                            $('#template_judul').html($('#judul_loker').val());
                            $('#formLoker').trigger("reset");
                            $('#modal-preview').attr('src',
                                'https://via.placeholder.com/200x100');
                        }
                        $('#btnSelesai').html('<i class="fas fa-check-double"></i> Selesai');
                        $("#btnSelesai").attr("disabled", false);
                        swa(data.status + "!", data.message, data.icon);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSelesai').html('<i class="fas fa-check-double"></i> Selesai');
                        $("#btnSelesai").attr("disabled", false);
                    }
                });
            });
        })

        function activaTab(tab) {
            $('.nav-tabs li a[href="#' + tab + '"]').trigger('click');
        };
    </script>
@endsection
