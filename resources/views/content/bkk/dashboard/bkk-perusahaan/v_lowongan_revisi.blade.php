@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <style>
        section.row.custom-scroll-content.scrollbar-enabled {
            overflow-y: scroll;
        }

        div#social-links {
            margin: 0 auto;
        }

        div#social-links ul li {
            display: inline-block;
        }

        div#social-links ul li a {
            padding: 20px;
            margin: 1px;
            font-size: 52px;
            color: #222;
        }

        .footer {
            position: fixed !important;
            left: 0;
            bottom: 0;
            width: 100%;
        }

        #loading {
            text-align: center;
            background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
        }

        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .error-actions .btn {
            margin-right: 10px;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 style="margin: 4px 0;">Lowongan Saya</h4>
                                </div>
                                <div class="col-md-8">
                                    <ul class="nav nav-tabs" style="float: right">
                                        <li class="nav-item active"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataLoker(1)" data-toggle="tab" aria-expanded="true">Postingan
                                                Lowongan Saya</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"
                                                onclick="dataLoker(0)" data-toggle="tab" aria-expanded="true">Save
                                                Drafts</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('lowongan-create') }}" class="btn btn-primary"
                                                style="background-color: #E60278; color: #fff">
                                                <b>Create Job Ad</b> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 widget-holder tampilanLoker">

            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="agendaModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0" style="border: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12 p-4" id="profilDetail">
                    </div>
                    <div class="col-md-12 detail_agenda">
                        <div class="detail-agenda pull-right">
                            <p>Detail Agenda</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg-color-scheme" id="tanggalTayang" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0" style="border: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="javascript:void(0)" id="formTanggal" name="formTanggal" method="post">
                            <div class="form-group">
                                <label for="l31">Tanggal Buka Loker</label>
                                <input type="hidden" name="id" id="id_loker_tanggal">
                                <input type="hidden" name="aksi_update" value="tanggal_tayang">
                                <div class="input-daterange input-group datepicker">
                                    <input type="text" class="form-control" name="tgl_buka" value="{{ date('d-m-Y') }}">
                                    <span class="input-group-addon bg-info text-inverse">to</span>
                                    <input type="text" class="form-control" name="tgl_tutup"
                                        value="{{ date('d-m-Y') }}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info" id="updateTayang"><i
                                        class="fa fa-refresh "></i>
                                    Perbarui</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        dataLoker(1)

        function dataLoker(id) {
            $('.tampilanLoker').html('<div id="loading" style="" ></div>');
            $.ajax({
                url: "{{ route('bkk_loker-data_loker_status') }}",
                type: "POST",
                data: {
                    id
                },
                success: function(data) {
                    if (data.count != 0) {
                        $('.tampilanLoker').html(data.html);
                    } else {
                        $('.tampilanLoker').html('<div class="error-template"><h1>Mohon maaf!</h1><h2>Data saat ini belum tersedia</h2><div class="error-details">Silahkan tambahkan lowongan untuk menampilkan data tersebut!</div><div class="error-actions"><a href="lowongan/create" class="btn btn-purple btn-lg"><i class="fas fa-clipboard"></i> Posting Lowongan</a></div></div>');
                    }
                }
            });
        }

        function deleteLoker(id) {
            if (id) {
                var confirmdelete = confirm("Do you really want remove data?");
                if (confirmdelete == true) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('program/bursa_kerja/lowongan/soft_delete') }}",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('.delete_dokumen').html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload(true);
                            }
                            swa(data.status + "!", data.message, data.success);

                        }
                    });
                }
            }
        }

        function shareLoker(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('lowongan-share') }}",
                data: {
                    id
                },
                success: function(data) {
                    $("#profilDetail").html(data);
                    $('#agendaModal').modal('show');
                    $('.detail_agenda').hide();
                }
            });
        }

        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $(".message").text("link copied");
        }

        function showAgenda(id) {
            $('.detail_agenda').show();
            $("#profilDetail").html('<div id="calendar_' + id + '"></div>');
            kalender(id);
        }

        function tayangKembali(id) {
            // kalender(id);
            $('#id_loker_tanggal').val(id);
            $('#tanggalTayang').modal('show');
        }

        $('#formTanggal').on('submit', function(event) {
            event.preventDefault();
            $("#updateTayang").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#updateTayang").attr("disabled", true);
            $.ajax({
                url: "{{ route('bkk_loker-update') }}",
                method: "PUT",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#formTanggal').trigger("reset");
                        $('#tanggalTayang').modal('hide');
                        dataLoker(1)
                    }
                    noti(data.icon, data.success);
                    $('#updateTayang').html('<i class="fa fa-spin fa-refresh"></i> Perbarui');
                    $("#updateTayang").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function kalender(id) {
            var calendar = $('#calendar_' + id).fullCalendar({
                editable: true,
                events: "{{ url('program/bursa_kerja/agenda/by_loker') }}" + '/' + id,
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    element.children().last().append("<br>Lokasi " + event.alamat);
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    var alamat = prompt('Event Location:');
                    if (title && alamat) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                        var event = {
                            alamat: alamat,
                            title: title,
                            start: start,
                            end: end,
                            id_loker: id
                        };
                        $.ajax({
                            url: "{{ route('bkk_agenda-simpan') }}",
                            data: event,
                            type: "POST",
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    $('#calendar_' + id).fullCalendar('refetchEvents');
                                }
                                noti(data.success, data.message)

                            }
                        });
                    }
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: "{{ route('bkk_agenda-update') }}",
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id + '&id_loker=' + event.id_loker + '&alamat=' + event
                            .alamat,
                        type: "PUT",
                        success: function(data) {
                            noti(data.success, data.message)
                        }
                    });
                },
                eventClick: function(event) {
                    var deleteMsg = confirm("Do you really want to delete?");
                    if (deleteMsg) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('bkk_agenda-soft_delete') }}",
                            data: "&id=" + event.id,
                            success: function(response) {
                                if (response['status'] == "berhasil") {
                                    $('#calendar_' + id).fullCalendar('removeEvents', event.id);
                                    noti(response.icon, response.message)
                                } else {
                                    noti(response.icon, response.message)
                                }
                            }
                        });
                    }
                }
            });
            $('#agendaModal').modal('show');
            $('#agendaModal').on('shown.bs.modal', function() {
                $("#calendar_" + id).fullCalendar('render');
            });
        }

        function detailLoker(id) {
            $.ajax({
                url: "/program/bursa_kerja/lowongan/detail_by_pelamar",
                type: "POST",
                data: {
                    id
                },
                success: function(data) {
                    $('#profilDetail').html(data);
                    $('.detail_agenda').hide();

                    $('#agendaModal').modal('show');
                }
            });
        }
    </script>
@endsection
