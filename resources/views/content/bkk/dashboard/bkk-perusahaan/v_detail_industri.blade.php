@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .pace {
            display: none;
        }

        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 300px;
        }

    </style>
    <div class="widget-list">
        <div class="row justify-content-center">
            <div class="col-md-8 widget-holder">
                <form id="msform">
                    <div class="widget-bg mt-3">
                        <div class="widget-body clearfix">
                            <div class="row">
                                <div class="col-md-8 col-9 my-auto">
                                    <h5 class="m-0">PROFILE PERUSAHAAN</h5>
                                </div>
                                <div class="col-md-4 col-3 my-auto">
                                    <button class="btn btn-info float-right" id="btnSave"><i class="fas fa-sync-alt"></i>
                                        <span class="d-none d-sm-inline-block ml-1">Perbarui</span>
                                    </button>
                                </div>
                            </div>
                            <hr>
                            <p class="text-muted mb-0">Untuk mempermudah pelamar menemukan perusahaan anda</p>
                            <h5 class="box-title mt-0 text-danger">Harap lengkapi semua isi form</h5>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Nama Perusahaan</label>
                                        <input class="form-control" id="l30" name="perusahaan" type="text"
                                            value="{{ !empty($industri) ? $industri['nama'] : '' }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="l30">Telepon</label>
                                        <input class="form-control" id="l30" name="telepon"
                                            value="{{ !empty($industri) ? $industri['telepon'] : '' }}" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="l30">Website</label>
                                        <input class="form-control" id="l30" name="website"
                                            value="{{ !empty($industri) ? $industri['website'] : '' }}" type="text">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Deskripsi</label>
                                        <textarea name="deskripsi" class="form-control" id="deskripsi"
                                            rows="3">{{ !empty($industri) ? $industri['deskripsi'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kabupaten/Kota</label>
                                        <input class="form-control" id="l30" name="kabupaten"
                                            value="{{ !empty($industri) ? $industri['kabupaten'] : '' }}" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="l30">Gambar</label>
                                                <input id="image" type="file" class="form-control p-0" name="image"
                                                    accept="image/*" onchange="readURL(this);"
                                                    style="border: none; box-shadow: none;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <img id="modal-preview"
                                                src="{{ !empty($industri) ? $industri['file'] : asset('images/industry.png') }}"
                                                alt="Preview" class="form-group" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="l30">Alamat</label>
                                        <textarea name="alamat" class="form-control" id="alamat"
                                            rows="3">{{ !empty($industri) ? $industri['alamat'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="material-icons">location_on</i>
                                            </div>
                                            <input class="form-control" type="text" id="lokasi" name="lokasi"
                                                placeholder="Masukan pencarian lokasi">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)" onclick="addr_search();"><i
                                                        class="material-icons list-icon search text-dark">search</i> </a>
                                            </div>
                                        </div>
                                        <div id="results"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Koordinat Latitude</label>
                                        <input class="form-control" type="lat" name="lat" id="lat" placeholder="lat"
                                            value="{{ !empty($industri) ? $industri['lat'] : '-6.991576' }}"
                                            readonly="readonly">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Koordinat Longitude</label>
                                        <input class="form-control" type="long" name="long" id="lon" placeholder="Long"
                                            value="{{ !empty($industri) ? $industri['long'] : '109.122923' }}"
                                            readonly="readonly">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="map"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var options;
            var map;

            var startlat = $('#lat').val();
            var startlon = $('#lon').val();

            options = {
                center: [startlat, startlon],
                zoom: 9
            }
            map = L.map('map', options);

            window.getLocation = function geolocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert('Geolocation is not supported by this browser.');
                }
            }

            window.showPosition = function getData(x) {

                var latxg = x.coords.latitude;
                var longx = x.coords.longitude;
                setMaplocation(latxg, longx);
            }

            window.setMaplocation = function setLokasiMap(startlat, startlon) {

                var container = L.DomUtil.get('map');
                if (container != null) {
                    container._leaflet_id = null;
                }
                map.invalidateSize();
                optionsx = {
                    center: [startlat, startlon],
                    zoom: 9
                }
                map = L.map('map', optionsx);
                document.getElementById('lat').value = startlat;
                document.getElementById('lon').value = startlon;
                getAddressMaker(startlat, startlon);
            }

            window.getLocation();

            var nzoom = 12;

            L.tileLayer(
                'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token={{ $key_maps['token'] }}'
            ).addTo(map);

            var myMarker = L.marker([startlat, startlon], {
                title: "Coordinates",
                alt: "Coordinates",
                draggable: true
            }).addTo(map).on('dragend', function() {
                var lat = myMarker.getLatLng().lat.toFixed(8);
                var lon = myMarker.getLatLng().lng.toFixed(8);
                var czoom = map.getZoom();
                if (czoom < 18) {
                    nzoom = czoom + 2;
                }
                if (nzoom > 18) {
                    nzoom = 18;
                }
                if (czoom != 18) {
                    map.setView([lat, lon], nzoom);
                } else {
                    map.setView([lat, lon]);
                }
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat, lon);
            });

            window.getAddressMaker = function(latx, lonx) {
                $.ajax({
                    url: "https://nominatim.openstreetmap.org/reverse",
                    data: {
                        lat: latx,
                        lon: lonx,
                        format: "json"
                    },
                    beforeSend: function(xhr) {},
                    dataType: "json",
                    type: "GET",
                    async: true,
                    crossDomain: true
                }).done(function(res) {
                    // console.log(res.address);
                    var rows = JSON.parse(JSON.stringify(res.address));
                    if (rows.village != 'underfined' && rows.village != null) {
                        var alamatx = rows.village + ',' + ',' + rows
                            .state + ',' + rows.country;
                    } else if (rows.postcode != 'underfined' && rows.postcode != null) {
                        var alamatx = rows
                            .state + ',' + rows.postcode + ',' + rows.country;
                    } else if (rows.country != 'underfined' && rows.country != null) {
                        var alamatx = rows
                            .state + ',' + rows.country;
                    } else {
                        var alamatx = rows
                            .state + ',' + rows.country;
                    }
                    $('input[name="alamat"]').val(alamatx);
                    myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                        alamatx).openPopup();
                }).fail(function(error) {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                });
            }

            window.chooseAddr = function chooseAddr(lat1, lng1) {
                myMarker.closePopup();
                map.setView([lat1, lng1], 18);
                myMarker.setLatLng([lat1, lng1]);
                lat = lat1.toFixed(8);
                lon = lng1.toFixed(8);
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat1, lng1);
            }

            $('body').on('cllick', '.address', function() {
                $('input[name="alamat"]').val($(this).data('alamat'));
            });

            window.myFunction = function myFunction(arr) {
                var out = "<br />";
                var i;
                if (arr.length > 0) {
                    for (i = 0; i < arr.length; i++) {
                        out +=
                            "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                            arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                            .display_name + "'><a href='javascript:void(0)'>" + arr[i].display_name +
                            "</a></div>";
                    }
                    document.getElementById('results').innerHTML = out;
                } else {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                }
            }

            window.addr_search = function addr_search() {
                var inp = document.getElementById("lokasi");
                var xmlhttp = new XMLHttpRequest();
                var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                    .value;
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var myArr = JSON.parse(this.responseText);
                        myFunction(myArr);
                    }
                };
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            }

            $('body').on('submit', '#msform', function(e) {
                e.preventDefault();
                $('#btnSave').html('<i class="fa fa-spin fa-sync-alt"></i> Memperbarui..');
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('bkk_industri-update_or_create') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        $('#btnSave').html('<i class="fas fa-sync-alt"></i> Perbarui');
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSave').html('<i class="fas fa-sync-alt"></i> Perbarui');
                    }
                });
            });
        })
    </script>
@endsection
