@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .footer {
            position: fixed !important;
            left: 0;
            bottom: 0;
            width: 100%;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Lowongan Saya</h4>
                                </div>
                                <div class="col-md-8">
                                    <ul class="nav nav-tabs" style="float: right">
                                        <li class="nav-item active"><a class="nav-link" href="#home-tab-bordered-1"
                                                data-toggle="tab" aria-expanded="true">Postingan Lowongan Saya</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="#profile-tab-bordered-1"
                                                data-toggle="tab" aria-expanded="true">Save Drafts</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('lowongan-create') }}" class="btn btn-primary"
                                                style="background-color: #E60278; color: #fff">
                                                <b>Create Job Ad</b> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active" id="home-tab-bordered-1">
                                    <div class="row">
                                        <style>
                                            .nav-tabs .nav-link.active,
                                            .nav-tabs .nav-item.show .nav-link {
                                                border-color: #ddd #ddd #fff;
                                                background: #fff;
                                            }

                                            button#createNewCustomer {
                                                background-color: #4fd6ba;
                                                color: #fff
                                            }

                                            .card-margin {
                                                margin-bottom: 1.875rem;
                                            }

                                            .card.card-block {
                                                padding: 12px;
                                                margin-top: 12px;
                                            }

                                            .thead-default th {
                                                color: #ffffff;
                                                background-color: #03a9f3;
                                            }

                                            .card {
                                                border: 0;
                                                box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
                                                -webkit-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
                                                -moz-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
                                                -ms-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
                                            }

                                            .card {
                                                position: relative;
                                                display: flex;
                                                flex-direction: column;
                                                min-width: 0;
                                                word-wrap: break-word;
                                                background-color: #ffffff;
                                                background-clip: border-box;
                                                border: 1px solid #e6e4e9;
                                                border-radius: 8px;
                                            }

                                            .card .card-header.no-border {
                                                border: 0;
                                            }

                                            .card .card-header {
                                                background: none;
                                                padding: 0 0.9375rem;
                                                font-weight: 500;
                                                display: flex;
                                                align-items: center;
                                                min-height: 50px;
                                            }

                                            .card-header:first-child {
                                                border-radius: calc(8px - 1px) calc(8px - 1px) 0 0;
                                            }

                                            .widget-49 .widget-49-title-wrapper {
                                                display: flex;
                                                align-items: center;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-primary {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #edf1fc;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-primary .widget-49-date-day {
                                                color: #4e73e5;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-primary .widget-49-date-month {
                                                color: #4e73e5;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-secondary {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #fcfcfd;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-secondary .widget-49-date-day {
                                                color: #dde1e9;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-secondary .widget-49-date-month {
                                                color: #dde1e9;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-success {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #e8faf8;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-success .widget-49-date-day {
                                                color: #17d1bd;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-success .widget-49-date-month {
                                                color: #17d1bd;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-info {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #ebf7ff;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-info .widget-49-date-day {
                                                color: #36afff;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-info .widget-49-date-month {
                                                color: #36afff;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-warning {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: floralwhite;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-warning .widget-49-date-day {
                                                color: #FFC868;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-warning .widget-49-date-month {
                                                color: #FFC868;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-danger {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #feeeef;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-danger .widget-49-date-day {
                                                color: #F95062;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-danger .widget-49-date-month {
                                                color: #F95062;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-light {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #fefeff;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-light .widget-49-date-day {
                                                color: #f7f9fa;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-light .widget-49-date-month {
                                                color: #f7f9fa;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-dark {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #ebedee;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-dark .widget-49-date-day {
                                                color: #394856;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-dark .widget-49-date-month {
                                                color: #394856;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-base {
                                                display: flex;
                                                align-items: center;
                                                justify-content: center;
                                                flex-direction: column;
                                                background-color: #f0fafb;
                                                width: 4rem;
                                                height: 4rem;
                                                border-radius: 50%;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-base .widget-49-date-day {
                                                color: #68CBD7;
                                                font-weight: 500;
                                                font-size: 1.5rem;
                                                line-height: 1;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-date-base .widget-49-date-month {
                                                color: #68CBD7;
                                                line-height: 1;
                                                font-size: 1rem;
                                                text-transform: uppercase;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-meeting-info {
                                                display: flex;
                                                flex-direction: column;
                                                margin-left: 1rem;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-meeting-info .widget-49-pro-title {
                                                color: #3c4142;
                                                font-size: 14px;
                                            }

                                            .widget-49 .widget-49-title-wrapper .widget-49-meeting-info .widget-49-meeting-time {
                                                color: #B1BAC5;
                                                font-size: 13px;
                                            }

                                            .widget-49 .widget-49-meeting-points {
                                                font-weight: 400;
                                                font-size: 13px;
                                                margin-top: .5rem;
                                            }

                                            .widget-49 .widget-49-meeting-points .widget-49-meeting-item {
                                                display: list-item;
                                                color: #727686;
                                            }

                                            .widget-49 .widget-49-meeting-points .widget-49-meeting-item span {
                                                margin-left: .5rem;
                                            }

                                            .widget-49 .widget-49-meeting-action {
                                                text-align: right;
                                            }

                                            .widget-49 .widget-49-meeting-action a {
                                                text-transform: uppercase;
                                            }

                                            h6.card-title {
                                                margin-bottom: 0;
                                                margin-top: 10px;
                                            }

                                        </style>
                                        @foreach ($loker as $lk)
                                            <div class="col-lg-3">
                                                <div class="card card-margin">
                                                    <div class="card-header no-border">
                                                        <h5 class="card-title">{{ $lk['bidang'] }}</h5>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="widget-49">
                                                            <div class="widget-49-title-wrapper">
                                                                <div class="widget-49-date-primary">
                                                                    <span
                                                                        class="widget-49-date-day">{{ date('d', strtotime($lk['tanggal_post'])) }}</span>
                                                                    <span
                                                                        class="widget-49-date-month">{{ date('F', strtotime($lk['tanggal_post'])) }}</span>
                                                                </div>
                                                                <div class="widget-49-meeting-info">
                                                                    <span
                                                                        class="widget-49-pro-title">{{ $lk['judul'] }}</span>
                                                                    @php
                                                                        $pecah = explode(';', $lk['gaji']);
                                                                    @endphp
                                                                    @if ($lk['gaji'] != null)
                                                                        <small
                                                                            style="color: green">{{ number_format((int) $pecah[0], 0, '', '.') . ' - ' . number_format((int) $pecah[1], 0, '', '.') }}</small>
                                                                    @endif
                                                                    <span
                                                                        class="widget-49-meeting-time">{{ date('d-m-Y', strtotime($lk['tgl_buka'])) }}
                                                                        to
                                                                        {{ date('d-m-Y', strtotime($lk['tgl_tutup'])) }}</span>
                                                                </div>
                                                            </div>
                                                            <h6 class="card-title">Syarat</h6>
                                                            <p style="min-height: 66px">
                                                                <span
                                                                    style="font-size: 13px; color: #727686;">{{ substr($lk['syarat'], 0, 100) }}</span>
                                                            </p>
                                                            <div class="widget-49-meeting-action">
                                                                <a href="javascript:void(0)" data-id="{{ $lk['id'] }}"
                                                                    class="btn btn-sm btn-flash-border-primary detail">View
                                                                    Detail</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-tab-bordered-1">
                                    <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
                                        rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                                        pretium. Integer tincidunt.Cras dapibus.
                                        Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
                                        porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                        dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                        nascetur ridiculus mus. Donec quam felis,
                                        ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Detail Lowongan</h5>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <header class="header">
                        <div class="container">
                            <div class="row" style="margin-top:20px;">
                                <div class="col-md-12">
                                    <h5 style="color:#3AAA64" style="margin-bottom: 0" id="d_lowongan"></h5>
                                    <p id="d_bidang" style="margin-bottom: 0">.</p>
                                    <small id="d_industri"></small>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block text-xs-left">
                                    <h5 class="card-title" style="color:#03a9f3"><i class="fa fa-user fa-fw"></i>Deskripsi
                                    </h5>
                                    <div style="height: 15px"></div>
                                    <p id="d_deskripsi"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block">
                                    <h5 class="card-title" style="color:#03a9f3"><i class="fa fa-rocket fa-fw"></i>Syarat
                                    </h5>
                                    <div style="height: 15px"></div>
                                    <p id="d_syarat"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block">
                                    <h5 class="card-title" style="color:#03a9f3"><i class="fa fa-trophy fa-fw"></i>User yang
                                        melamar</h5>
                                    <div style="height: 15px"></div>
                                    <div id="tabel-pelamar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true"><i
                            class="fa fa-times "></i> Close</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme bs-modal-lg-color-scheme" id="userModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background-color: #cafbc6;">
                <div class="modal-header text-inverse" style="background: #4fd6ba; margin-bottom: 10px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Detail Pelamar</h5>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-img">
                                <img id="modal-preview"
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52y5aInsxSm31CvHOFHWujqUx_wWTS9iM6s7BAm21oEN_RiGoog"
                                    alt="" style="height: 146px;" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="profile-head">
                                <h5 id="u_nama">
                                    Kshiti Ghelani
                                </h5>
                                <h6 id="u_tanggal" style="margin-top: 0">
                                    20 maret 2021
                                </h6>
                                <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                            aria-controls="home" aria-selected="true">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                            aria-controls="profile" aria-selected="false">Dokumen</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="agenda-tab" data-toggle="tab" href="#agenda" role="tab"
                                            aria-controls="agenda" aria-selected="false">Agenda</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-work">
                                <p
                                    style="margin-bottom: 2px;background-color: #4fd6ba; padding: 5px; color: #fff; font-weight: bold;">
                                    SKILLS</p>
                                <a href="">Web Designer</a><br />
                                <a href="">Web Developer</a><br />
                                <a href="">WordPress</a><br />
                                <a href="">WooCommerce</a><br />
                                <a href="">PHP, .Net</a><br />
                            </div>
                        </div>
                        <div class="col-md-8" style="background-color: #fff;">
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>User Id</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Kshiti123</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Name</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p id="u_nama1">Kshiti Ghelani</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Email</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p id="u_email">kshitighelani@gmail.com</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Phone</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p id="u_telepon">123 456 7890</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Profession</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Web Developer and Designer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Experience</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Expert</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Hourly Rate</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>10$/hr</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Total Projects</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>230</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>English Level</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Expert</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Availability</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>6 months</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Your Bio</label><br />
                                            <p>Your detail description</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="agenda" role="tabpanel" aria-labelledby="agenda-tab">
                                    <div class="data_agenda">
                                        <div style="width: 100%;">
                                            <div class="table-responsive">
                                                <table class="table table-striped" id="data-agenda" style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Agenda</th>
                                                            <th>Tempat</th>
                                                            <th>Tanggal Agenda</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="addAgenda" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingAgenda"></h5>
                </div>
                <form id="agendaForm" name="agendaForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_agenda">

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Agenda</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="judul" name="judul" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Deskripsi</label>
                                    <div class="col-sm-12">
                                        <textarea name="deskripsi" id="deskripsi" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="id_lowongan" id="id_lowongan">
                                <input type="hidden" name="id_pelamar" id="id_pelamar">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">tempat</label>
                                    <div class="col-sm-12">
                                        <textarea name="alamat" id="alamat" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal</label>
                                    <div class="col-sm-12">
                                        <div class="input-daterange input-group datepicker">
                                            <input type="text" class="form-control" name="tgl_mulai" id="tgl_mulai"
                                                style="margin-bottom: 0">
                                            <span class="input-group-addon bg-info text-inverse">sampai</span>
                                            <input type="text" class="form-control" name="tgl_selesai" id="tgl_selesai"
                                                style="margin-bottom: 0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id_pelamar = 0;
        var id_loker = 0;

        var table = $('#data-agenda').DataTable({
            dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            buttons: [{
                    text: 'Tambah Data',
                    className: 'btn btn-sm btn-facebook',
                    attr: {
                        title: 'Tambah Data',
                        id: 'createNewCustomer'
                    }
                },

            ],
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                "url": "agenda/get_datatable",
                "method": "POST",
                "data": function(d) {
                    d.id_loker = id_loker;
                    d.id_pelamar = id_pelamar;
                },
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'judul',
                    name: 'judul'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        $('#createNewCustomer').click(function() {
            $('#id_agenda').val('');
            $('#agendaForm').trigger("reset");
            $('#HeadingAgenda').html("Tambah Data Agenda");
            $('#addAgenda').modal('show');
            $('#action').val('Add');
        });

        $(document).on('click', '.detail', function() {
            var id = $(this).data('id');
            var self = $(this);
            id_loker = id
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "lowongan/edit",
                data: {
                    id_loker: id
                },
                beforeSend: function() {
                    $(self).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $('#d_lowongan').html(data.loker.judul);
                    $('#d_industri').html(data.loker.industri);
                    $('#d_bidang').html(data.loker.bidang);
                    $('#d_deskripsi').html(data.loker.deskripsi);
                    $('#d_syarat').html(data.loker.syarat);
                    html =
                        '<table class="table table-bordered table-hover" style="width:100%"><thead class="thead-default"><tr><th width="5%">No</th><th width="30%">Nama</th><th width="15%">Melamar</th><th width="30%">Telepon</th><th width="20%">Opsi</th></tr></thead><tbody>';
                    if (!$.trim(data.pelamar)) {
                        html +=
                            '<tr><td colspan="5" style="text-align: center">Mohon maaf Belum ada pelamar yang masuk</td></tr>';
                    } else {
                        var i = 1;
                        $.each(data.pelamar, function(k, v) {
                            html += '<tr><td>' + i + '</td><td>' + v.nama + '</td><td>' + v
                                .waktu + '</td><td>' + v
                                .telepon +
                                '</td><td><a class="btn btn-sm btn-info" onclick="detailProfil(' +
                                v.id +
                                ')" style="color:#fff"><i class="fa fa-info-circle"><i> Detail info</a></td></tr>';
                            i++;
                        });
                    }

                    html += '</tbody></table>';
                    $("#tabel-pelamar").html(html);
                    $(self).html('View Detail')
                    $('#ajaxModel').modal('show');
                }
            });
        });

        function detailProfil(id_user) {
            $('#userModal').modal('show');
            var detail_user = $(this);
            id_pelamar = id_user
            $.ajax({
                type: 'POST',
                url: "pelamar/edit",
                data: {
                    id_pelamar: id_user
                },
                beforeSend: function() {
                    $(detail_user).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    table.ajax.reload();
                    $('#id_pelamar').val(id_pelamar);
                    $('#id_lowongan').val(id_loker);
                    $('#u_nama').html(data.nama);
                    $('#u_tanggal').html(data.waktu);
                    $('#u_nama1').html(data.nama);
                    $('#u_email').html(data.email);
                    $('#u_telepon').html(data.telepon);
                    if (data.file) {
                        $('#modal-preview').attr('src', data.file);
                    }
                    html = '';
                    if (!$.trim(data.documents)) {
                        html += '<h5>Mohon maaf data dokumen kosong</h5>';
                    } else {
                        var i = 1;
                        $.each(data.documents, function(k, v) {
                            html +=
                                '<div class="row" style="margin-bottom: 10px"><div class="col-md-6"><label>' +
                                v.jenis +
                                '</label></div><div class="col-md-6"><a href="/program/bursa_kerja/dokumen/view/' +
                                v.id_code +
                                '" target="_blank" class="btn btn-sm btn-info">lihat dokumen</a></div></div>';
                            i++;
                        });
                    }

                    $("#profile").html(html);
                    $('#userModal').modal('show');
                }
            });
        }

        $('#agendaForm').on('submit', function(event) {
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('bkk_agenda-simpan') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('bkk_agenda-update') }}";
                method_url = "PUT";
            }

            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#agendaForm').trigger("reset");
                        $('#addAgenda').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-agenda').dataTable();
                        oTable.fnDraw(false);
                        noti(data.success, data.message);
                    } else {
                        noti(data.success, data.message);
                        $('#saveBtn').html('Simpan');
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $(document).on('click', '.editAgenda', function() {
            var id = $(this).data('id');
            var agenda = $(this);
            // $('#agendaForm').reset();
            $.ajax({
                type: 'POST',
                url: "agenda/edit",
                data: {
                    id_agenda: id
                },
                beforeSend: function() {
                    $(agenda).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(agenda).html('<i class="fa fa-pencil-square-o"></i> Edit');
                    $('#HeadingAgenda').html("Edit Data Agenda");
                    $('#saveBtn').val("edit-user");
                    $('#id_agenda').val(data.id);
                    $('#judul').val(data.judul);
                    $('#deskripsi').val(data.deskripsi);
                    $('#alamat').val(data.alamat);
                    $('#tgl_mulai').val(data.tanggal_mulai);
                    $('#tgl_selesai').val(data.tanggal_selesai);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                    $('#addAgenda').modal('show');
                    // $('#userModal').modal('hide');
                }
            });
        });

    </script>
@endsection
