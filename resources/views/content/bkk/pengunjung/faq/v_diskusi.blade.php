@extends('content.bkk.pengunjung.faq.main')
@section('content_feed')
    <style>
        .form4 {
            background-color: #fff;
            background-size: cover
        }

        main.main-wrapper.clearfix {
           min-height: 100vh;
        }

        .top {
            padding: 5px;
        }

        .form-bg {
            background: rgb(238 238 238);
            padding: 20px;
            border: 1px solid #ccc;
        }

        .form {
            background-color: white !important;
            padding: 20px
        }

        .form-group {
            margin-bottom: 20px
        }

        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            padding: 0;
            margin: -1px;
            overflow: hidden;
            clip: rect(0, 0, 0, 0);
            border: 0
        }

        .form-control {
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
            border: 1px solid #dedede;
            background: white;
            -webkit-transition: all 0.3s ease 0s;
            -moz-transition: all 0.3s ease 0s;
            -o-transition: all 0.3s ease 0s;
            transition: all 0.3s ease 0s
        }

        .btn-blue {
            background-color: #4285f4;
            color: #fff;
            border: 1px solid #4285f4
        }

        .modal-body {
            padding: 20px 10px 0 !important;
        }

        .modal-content {
            border-radius: 0.81429em !important;
        }

        .komen {
            margin-bottom: 5px !important;
        }

        .post-tittle {
            font-size: 18px;
            font-weight: bold;
        }

        #disqus_thread {
            position: relative;
        }

        #disqus_thread:after {
            content: "";
            display: block;
            height: 55px;
            width: 100%;
            position: absolute;
            bottom: 0;
            background: #f2f4f8;
        }

        #disqus_thread:before {
            content: "";
            display: block;
            height: 32px;
            width: 70%;
            position: absolute;
            top: 0;
            left: 100px;
            background: #f2f4f8;
        }

    </style>

    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['success'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    <div id="disqus_thread"></div>
    @if ($forum['token'] != null)
        <script>
            (function() {
                var d = document,
                    s = d.createElement('script');
                s.src = "{{ $forum['token'] }}";
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
    @else
        <script>
            alert('Anda belum mensetting Aktivasi Forum Disquss');
        </script>
    @endif


    <script type="text/javascript">
        var id_chat = "{{ $chat['token'] }}";

        function add_chatinline() {
            if (id_chat) {
                var hccid = id_chat;
                var nt = document.createElement("script");
                nt.async = true;
                nt.src = "https://www.mylivechat.com/chatinline.aspx?hccid=" + hccid;
                var ct = document.getElementsByTagName("script")[0];
                ct.parentNode.insertBefore(nt, ct);
            } else {
                alert('Anda belum mensetting Aktivasi LiveChat');
            }
        }
        add_chatinline();
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function detailModal(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('bkk_loker-detail_feed') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $("#editData-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    console.log(data);
                    $('#detailFeed').html('Issue <strong>#' + data.id +
                        '</strong> opened by <a href="#">' + data.nama + '</a> ' + data.terbit + '');
                    $('#berita_feed').html(data.berita);
                    $('#id_feed').val(data.id);
                    let komentar = '';
                    $.each(data.komentar, function(index, value) {
                        console.log(value);
                        komentar +=
                            '<div class="row support-content-comment"><div class="col-md-12"><p class="komen">Posted by ' +
                            value.nama + ' on ' + value.ditulis + ' </p><p class="komen">' + value
                            .komentar +
                            '.</p></div></div>';
                    });
                    $('#com').html(komentar);
                    $('#detailIssue').modal('show');
                    $('#target1').collapse('hide');
                    $("#editData-" + data.id).html('#' + data.id);
                }
            });
        }

        $('#formComment').on('submit', function(event) {
            event.preventDefault();
            $("#send-comment").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#send-comment").attr("disabled", true);
            $.ajax({
                url: "{{ route('bkk_loker-comment_feed_simpan') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#com').prepend(data.html);
                        $('#target1').collapse('hide');
                        $('#formComment').trigger("reset");
                    }
                    noti(data.icon, data.success);
                    $('#send-comment').html('Kirim Komentar');
                    $("#send-comment").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#send-comment').html('Simpan');
                }
            });
        });
    </script>
@endsection
