<div class="grid support">
    <div class="grid-body p-0">
        <h5 style="
            padding: 20px;
            margin: 0;
            background: #d4d8dc;
        ">Pilihan Diskusi</h5>


        <ul>
            <li class="{{ Request::segment(3) === 'diskusi' ? 'active' : '' }}"><a href="{{ route('bkk_loker-diskusi') }}">Diskusi umum<span class="pull-right">{{count($feed)}}</span></a></li>
            <li class="{{ Request::segment(3) === 'question' ? 'active' : '' }}"><a href="{{ route('bkk_loker-question') }}">Pesan untuk Admin<span class="pull-right"></span></a></li>
        </ul>
    </div>
</div>
