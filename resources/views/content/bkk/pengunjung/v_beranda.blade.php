@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <style>
        .wrapper,
        section {
            min-height: 100vh !important;
            margin: 0px auto !important;
            overflow: hidden !important;
        }

        .has-search .form-control-feedback {
            right: initial;
            left: 0;
            color: #ccc;
        }

        .has-search .form-control {
            padding-right: 12px;
            padding-left: 34px;
        }

        .card-loker:hover {
            border: 1px solid rgb(51 122 190);
        }

    </style>

    <div class="head bg-info">
        <form action="javascript:void(0)" id="filterLoker" name="filterLoker">
            <div class="row justify-content-center">
                <div class="col-md-7 m-4 ">
                    <div class="input-group">
                        @php
                            $serc = str_replace('-', ' ', $title);
                        @endphp
                        <input type="text" class="form-control" value="{{ $serc }}" id="keyword"
                            placeholder="Posisi atau perusahaan" style="height: 45px !important">
                        <span class="input-group-btn">
                            <button class="btn btn-default btnFilter" type="button" style="height: 45px !important"><i
                                    class="fas fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <main class="main-wrapper clearfix pt-0 bg-white">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-3 widget-holder">
                    <div class="card-header">
                        <a href="" class="btn btn-danger btn-block">Reset</a>
                    </div>
                    <div class="card-body">
                        <div class="accordion accordion-minimal" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <h5
                                    class="accordion-header my-2 ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all">
                                    {{-- <span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span> --}}
                                    <a class="collapsed" href="javascript:void(0)">Bidang</a>
                                </h5>
                                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                                    style="display: {{ isset($_GET['bidang']) && $_GET['bidang'] != '' ? 'block' : 'none' }}">
                                    @if (!empty($bidang))
                                        @foreach ($bidang as $bd)
                                            @php
                                                $check = '';
                                                if (isset($_GET['bidang'])) {
                                                    $p_bdg = explode('-', $_GET['bidang']);
                                                    // dd($p_bdg);
                                                    foreach ($p_bdg as $bdg) {
                                                        // dd($bdg);
                                                        if ($bd['id'] == $bdg) {
                                                            $check = 'checked';
                                                        }
                                                    }
                                                }
                                            @endphp
                                            <div class="form-group mb-3">
                                                <div class="checkbox checkbox-primary my-0">
                                                    <label class="">
                                                        <input type="checkbox" class="id_bidang"
                                                            id="{{ $bd['id'] }}" value="{{ $bd['id'] }}"
                                                            {{ $check }}> <span
                                                            class="label-text">{{ $bd['nama'] }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="card">
                                <h5
                                    class="accordion-header my-2 ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all">
                                    <a class="collapsed" href="javascript:void(0)">Lokasi</a>
                                </h5>
                                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                                    style="display: {{ isset($_GET['provinsi']) && $_GET['provinsi'] != '' ? 'block' : 'none' }}">
                                    @foreach ($provinsi as $pro)
                                        @php
                                            $check_p = '';
                                            if (isset($_GET['provinsi'])) {
                                                $p_prov = explode('-', $_GET['provinsi']);
                                                foreach ($p_prov as $provin) {
                                                    if ($pro['id'] == $provin) {
                                                        $check_p = 'checked';
                                                    }
                                                }
                                            }
                                        @endphp
                                        <div class="form-group mb-3">
                                            <div class="checkbox checkbox-primary my-0">
                                                <label class="">
                                                    <input type="checkbox" value="{{ $pro['id'] }}" id="sample2checkbox"
                                                        class="id_provinsi" {{ $check_p }}>
                                                    <span class="label-text">{{ $pro['name'] }}</span>
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <!-- /.card -->
                            <div class="card">
                                <h5
                                    class="accordion-header my-2 ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all">
                                    <a class="collapsed" href="javascript:void(0)">Gaji</a>
                                </h5>
                                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                                    style="display: {{ (isset($_GET['gaji-min']) && $_GET['gaji-min'] != '') || (isset($_GET['gaji-max']) && $_GET['gaji-max'] != '') ? 'block' : 'none' }}">
                                    <div class="input-group">
                                        @php
                                            $min = '';
                                            if (isset($_GET['gaji-min']) && $_GET['gaji-min'] != '') {
                                                $min = number_format($_GET['gaji-min'], 0, ',', '.');
                                            }
                                        @endphp
                                        <span class="input-group-addon" id="basic-addon1">IDR</span>
                                        <input type="text" class="form-control ribuan" placeholder="IDR" id="gaji-min"
                                            value="{{ $min }}">
                                    </div>
                                    <p class="text-muted my-2">Sampai dengan</p>
                                    <div class="input-group">
                                        @php
                                            $max = '';
                                            if (isset($_GET['gaji-max']) && $_GET['gaji-max'] != '') {
                                                $max = number_format($_GET['gaji-max'], 0, ',', '.');
                                            }
                                        @endphp
                                        <span class="input-group-addon" id="basic-addon1">IDR</span>
                                        <input type="text" class="form-control ribuan gaji" placeholder="IDR"
                                            value="{{ $max }}" id="gaji-max">
                                    </div>
                                </div>
                            </div>

                            <!-- /.panel -->
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-info float-right btn-block" id="btnFilter"><i class="fas fa-filter"></i>
                            Filter</button>
                    </div>
                </div>
                <!-- /.widget-holder -->

                <div class="col-md-9 widget-holder">
                    <div class="widget-bg">
                        @if (!empty($loker))
                            @foreach ($loker as $lk)
                                <div class="card card-loker widget-holder">
                                    <div class="card-body d-flex">
                                        <div class="logo-perusahaan p-2">
                                            <img src="{{ $lk['file_industri'] }}" alt="" width="100">
                                        </div>
                                        <div class="info-perusahaan p-2" style="min-width: 450px">
                                            <h3 class="mt-1"><a
                                                    href="{{ route('bursa_kerja-detail_lowongan', ['code' => (new \App\Helpers\Help())->encode($lk['id']), 'title' => str_slug($lk['judul'])]) }}">{{ $lk['judul'] }}</a>
                                            </h3>
                                            <h5>Dibutuhkan Segera</h5>
                                            <p class="font-weight-bold">{{ $lk['industri'] }}</p>
                                            <p class="text-muted">
                                                {{ (new \App\Helpers\Help())->getTanggal($lk['tgl_tutup']) }}</p>
                                            <div class="d-flex justify-content-between">
                                                @php
                                                    $bidang = [];
                                                @endphp
                                                @foreach ($lk['bidang'] as $bd)
                                                    @php
                                                        $bidang[] = $bd['nama'];
                                                    @endphp
                                                @endforeach
                                                <span><i class="fas fa-building"></i> {{ implode(', ', $bidang) }}</span>
                                                <span><i class="fas fa-map-marker-alt"></i> {{ $lk['kabupaten'] }}</span>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                @php
                                                    $gaji = 'Gaji Kompetitif';
                                                    if ($lk['gaji_awal'] != null) {
                                                        $gaji = 'IDR ' . $lk['gaji_awal'] . ' - ' . $lk['gaji_sampai'];
                                                    }
                                                @endphp
                                                <span class="text-success"><i class="fas fa-money-bill"></i>
                                                    {{ $gaji }}</span> <span><i class="fas fa-mail-bulk"></i>
                                                    {{ $lk['pelamar'] }}</span>
                                            </div>

                                        </div>
                                        <div class="aksi-perusahaan ml-auto p-2 my-auto" style="width: 200px">
                                            <a href="{{ route('bkk_pelamar-create', (new \App\Helpers\Help())->encode($lk['id'])) }}"
                                                class="btn btn-info btn-block">Lamar</a>
                                            <a href="#" class="btn  btn-block"><i class="fas fa-bookmark"></i> Simpan</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <center>
                                <h3 class="box-title my-0">oops pencarian anda tidak ditemukan</h3>
                                <img src="{{ asset('images/empty.png') }}" alt="" width="500">
                            </center>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        var bidang = [];
        var provinsi = [];
        var gaji_min, gaji_max;

        var headers = $('#accordion .accordion-header');
        // var contentAreas = $('#accordion .ui-accordion-content ').hide();
        var expandLink = $('.accordion-expand-all');

        // add the accordion functionality
        headers.click(function() {
            var panel = $(this).next();
            var isOpen = panel.is(':visible');

            // open or close as necessary
            panel[isOpen ? 'slideUp' : 'slideDown']()
                // trigger the correct custom event
                .trigger(isOpen ? 'hide' : 'show');

            // stop the link from causing a pagescroll
            return false;
        });

        // hook up the expand/collapse all
        expandLink.click(function() {
            $(".ui-accordion-content").show();
        });



        $(function() {

            $('#btnFilter, .btnFilter').click(function() {
                filter_coloumn();
            })

        })

        function filter_coloumn() {
            $('.id_bidang:checked').each(function(i) {
                bidang[i] = $(this).val();
            });
            $('.id_provinsi:checked').each(function(i) {
                provinsi[i] = $(this).val();
            });
            if (bidang) {
                bidang = bidang.join('-');
            }
            if (provinsi) {
                provinsi = provinsi.join('-');
            }
            gaji_min = $('#gaji-min').val();
            gaji_min = gaji_min.replace(/\./g, '');
            gaji_max = $('#gaji-max').val();
            gaji_max = gaji_max.replace(/\./g, '');
            filter();
        }

        function filter(routes = null) {
            var searchs = (document.getElementById("keyword") != null) ? document.getElementById("keyword").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?q=" + search + "&bidang=" + bidang + "&provinsi=" + provinsi + "&gaji-min=" +
                gaji_min + "&gaji-max=" + gaji_max;
            document.location = url;
        }
    </script>
@endsection
