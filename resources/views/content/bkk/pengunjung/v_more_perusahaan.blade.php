@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <style>
        section {
            text-align: center;
            margin: 0px auto 0px auto;
        }

    </style>
    <section class="mt-5" style="width: 70%;">
        <div class="justify-content-center h-100">
            <div class="row">
                @php
                    $count = 0;
                @endphp
                @foreach ($industri as $ind)
                    <div class="col-lg-3 col-xs-12 text-center widget-bg m-3">
                        <div class="box">
                            <div class="box-title">
                                <img src="{{ $ind['file'] }}" alt="" style="height: 126px;">
                            </div>
                            <div class="box-text">
                                <p class="mb-1">{{ ucwords($ind['nama']) }}</p>
                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                    class="fa fa-star"></i><i class="fa fa-star"></i>
                            </div>
                            <div class="box-btn">
                                <small>1094 Rating</small>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row mt-3 mb-3">
                <div class="col-md-12 ta-l">
                    <center>
                        <a href="{{ url('bursa_kerja/more-perusahaan') }}" class="btn btn-info">Show More Companies</a>
                    </center>
                </div>
            </div>


        </div>
    </section>


@endsection
