@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <style>
        .carousel-item {
            height: 75vh;
            /* min-height: 350px; */
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .carousel-caption {
            position: absolute;
            top: 25%;
            left: 0;
            right: 0;
            bottom: 0;
            transform: translateY(-32%);
            text-align: center;
            max-height: 30vh;
        }

        ol.carousel-indicators {
            display: none !important;
        }

        @media (min-width: 768px) {
            .form-inline #input-keyword {
                width: 426px;
            }

        }

    </style>
    <div class="head bg-info">
        <form class="form-inline d-flex justify-content-center p-4">

            <label class="col-form-label" for="l4">Cari Lowongan Kerja</label>
            <div class="form-group">
                <input type="text" id="input-keyword" class="form-control mx-sm-3" style="width: 346px"
                    placeholder="Cari berdasarkan Jabatan, Nama Perusahaan atau kata kunci">
            </div>

            <div class="input-group">
                <select name="" class="form-control" id="lokasi" style="height: 34px">
                    <option value="">SEMUA LOKASI</option>
                    @foreach ($provinsi as $pro)
                        <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                    @endforeach

                </select>
                <span class="input-group-btn">
                    <button class="btn btn-info" id="cari-loker" type="button"><i class="fas fa-search"></i> Cari
                        Lowongan</button>
                </span>
            </div>
        </form>


        {{-- <form action="javascript:void(0)" id="filterLoker" name="filterLoker">
            @csrf
            <div class="row p-3 mr-0">
                <div class="form-group col-md-3 col-xs-6 my-1">
                    <input type="text" name="judul" id="judul" placeholder="Masukan Kata kunci pencarian"
                        class="filter-type filter form-control">
                </div>
                <div class="form-group col-md-4 col-xs-6 my-1">
                    <select data-filter="type" name="provinsi" class="filter-type filter form-control select3"
                        style="height: auto">
                        <option value="">Select Provinsi</option>
                        @foreach ($provinsi as $pro)
                            <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group col-md-4 col-xs-6 my-1">
                    <select data-filter="type" name="bidang" class="filter-type filter form-control" style="height: auto">
                        <option value="">Select Bidang</option>
                        @foreach ($bidang as $bd)
                            <option value="{{ $bd['id'] }}">{{ $bd['nama'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-1 col-xs-6 my-1">
                    <button type="submit" class="btn btn-primary btn-block btn-search">Cari</button>
                </div>
            </div>
        </form> --}}
    </div>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active"
                    style="background-image: url('https://hrdspot.com/wp-content/uploads/job-description.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                        <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <p class="lead">
                            {{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">
                            {{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item"
                    style="background-image: url('https://2e8ram2s1li74atce18qz5y1-wpengine.netdna-ssl.com/wp-content/uploads/2019/01/cropped-shutterstock_669226057-1-1024x576.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                        <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <p class="lead">
                            {{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">
                            {{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item"
                    style="background-image: url('https://image.freepik.com/free-photo/business-job-interview-concept_1421-77.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                        <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <p class="lead">
                            {{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">
                            {{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
    <main class="main-wrapper clearfix pt-0 bg-white">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h3 class="box-title">Lowongan Kerja Terpopuler</h3>
                            <hr>
                            <div class="row">
                                @if (!empty($loker))
                                    @foreach ($loker as $lk)
                                        <div class="col-md-3 widget-holder">
                                            <div class="card">
                                                <div class="card-body">
                                                    <img src="{{ $lk['file_industri'] }}" alt="" height="35">
                                                    <h4 class="h3 fw-300 m-1 sub-heading-font-family blog-post-title my-4">
                                                        <a href="#">{{ $lk['judul'] }}</a>
                                                    </h4>
                                                    <p class="card-text mx-1 font-weight-bold text-uppercase">
                                                        {{ $lk['industri'] }}</p>
                                                    <p class="card-text mx-1"><i class="fas fa-map-marker-alt"></i>
                                                        {{ $lk['kabupaten'] != null ? $lk['kabupaten'] : '-' }}</p>
                                                    @php
                                                        $gaji = 'Gaji Kompetitif';
                                                        if ($lk['gaji_awal'] != null) {
                                                            $gaji = 'IDR ' . $lk['gaji_awal'] . ' - ' . $lk['gaji_sampai'];
                                                        }
                                                    @endphp
                                                    <small
                                                        class="mx-1 text-{{ $lk['gaji_awal'] != null ? 'success' : 'info' }}">{{ $gaji }}</small>
                                                    <hr>
                                                    <small>{{ $lk['diposting'] }}</small>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.widget-holder -->

                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h3 class="box-title">Perusahaan Rekomendasi</h3>
                            <hr>
                            <div class="row">
                                @if (!empty($industri))
                                    @foreach ($industri as $ind)
                                        <div class="col-md-2 widget-holder">
                                            <div class="card">
                                                <div class="card-body">
                                                    <center>
                                                        <img src="{{ $ind['file'] }}" alt="" height="70">
                                                        <h3 class="box-title my-3">{{ $ind['nama'] }}</h3>
                                                    </center>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.widget-holder -->
                <div class="col-md-12 bg-info">
                    <div class="widget-body clearfix">
                        <div class="row p-5">
                            <div class="col-md-5">
                                <img class="logo-expand" src="{{ asset('images/agenda.png') }}" width="400">

                            </div>
                            <div class="col-md-7 my-auto">
                                <h1 class="card-title">Kejar Suksesmu dengan jadi Member</h1>
                                <p>Berkarir dengan Perusahaan terbaik se Indonesia</p>
                                <button class="btn btn-light text-info">Daftar Sekarang</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $(document).on('click', '#cari-loker', function() {
                var keyword = (($('#input-keyword') != null) ? $('#input-keyword').val() : '');
                var lokasi = (($('#lokasi') != null) ? $('#lokasi').val() : '');
                keyword = keyword.replace(/ /g, '-')
                var url_asli = window.location.href;
                var explode_url = url_asli.split("?")[0];
                var url = "bursa_kerja/pekerjaan?q=" + keyword + "&provinsi=" + lokasi;
                document.location = url;

            });
        })
    </script>
@endsection
