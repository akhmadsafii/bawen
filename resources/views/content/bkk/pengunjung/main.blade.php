<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        @include('content.bkk.pengunjung.menu_bkk')
        @yield('content_bkk')
        <footer class="footer text-center clearfix">{{ $config != null ? $config['footer'] : "2021 © Tim Dev MySCH.id" }}</footer>
    </div>
    @include('includes.foot')
</body>

</html>
