@extends('content/bkk/pengunjung/main')
@section('content_bkk')

    <style>
        .wrapper {
            min-height:100vh !important;
            margin: 0px auto !important;
            overflow: hidden !important;
        }

        body{
            height: 100%;
        }

        .list-ind {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
        }

        .list-ind:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        .featurette {
            display: table;
            width: 100%;
        }

        .featurette-inner {
            display: table-cell;
            vertical-align: middle;
        }

        .featurette .search {
            padding: 3%;
            max-width: 980px;
            /*max width of search*/
            margin: 0 auto;
        }

        .no-margin-top {
            margin-top: 0
        }


        /* ------ form-control-lg */
        .form-control-lg {
            min-height: 46px;
            border-radius: 6px;
            font-size: 18px;
            line-height: normal;
        }


        /* ------ disjointed form group custom */
        .featurette .search.has-button .btn {
            margin-top: 5px;
            width: 100%;
        }

        @media (min-width:480px) {
            .featurette .search.has-button .form-group {
                position: relative;
                padding-right: 99px;
                /*space for button*/
            }

            .featurette .search.has-button .btn {
                position: absolute;
                top: 0;
                right: 0;
                width: auto;
                margin: 0;
            }
        }

    </style>
    <div class="content-wrapper">
        <main class="main-wrapper clearfix" style="padding-bottom: 70px">
            <section class="mx-auto" style="width:100%;">
                <div class="featurette">
                    <div class="featurette-inner text-center">
                        <form method="GET" action="{{ route('bkk_industri-search_industri') }}" class="search has-button">
                            <h3 class="no-margin-top h1 text-info">Cari Perusahaan?</h3>
                            <div class="form-group">
                                <input type="search" placeholder="search" value="{{ old('cari') }}" name="cari"
                                    class="form-control form-control-lg">
                                <button class="btn btn-lg btn-info" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>

            <section class="mt-5 rounded mx-auto">
                <div class="justify-content-center h-100">
                    <div class="row mx-0">
                        @foreach ($industri as $ind)
                            <div class="col-md-4 my-3">
                                <div class="row mx-0 widget-bg list-ind bg-info" style="border-radius: 20px">
                                    <div class="col-md-12">
                                        <h2 class="box-title text-center">{{ $ind['nama'] }}
                                        </h2>
                                    </div>
                                    <div class="col-md-4 d-flex align-items-center">
                                        <div class="heads">
                                            <table class="mt-3">
                                                <tr>
                                                    <td rowspan="3" style="vertical-align: bottom">
                                                        <img src="{{ $ind['file'] }}" alt="" style="height: 80%">
                                                    </td>

                                                    <td class="ta-l">
                                                        <h5 style="text-shadow: 0px 1px #000000;"><b class="text-white">
                                                        </h5>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ta-l text-white"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-8 d-flex align-items-center">
                                        <table style="width: 100%" style="border: 1px solid rgb(0, 0, 0)">
                                            <tr>
                                                <td colspan="2" class="ta-l">
                                                    {{ $ind['alamat'] != null ? $ind['alamat'] : 'Alamat belum di set' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="ta-l">
                                                    <hr>
                                                    <p class="mb-0"><b>Profil Perusahaan</b></p>
                                                    <p style="text-align: justify">
                                                        {{ $ind['deskripsi'] != null ? $ind['deskripsi'] : 'deskripsi perusahaan belum diset' }}
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <p
                                                        class="text-{{ $ind['jumlah_loker'] != 0 ? 'warning' : 'danger' }}">
                                                        {{ $ind['jumlah_loker'] != 0 ? $ind['jumlah_loker'] . ' Jobs Available' : 'Belum ada loker yang tersedia' }}
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ route('bursa_kerja-detail_perusahaan', ['code' => (new \App\Helpers\Help())->encode($ind['id']), 'industry' => str_slug($ind['nama'])]) }}"
                                                        class="btn btn-sm btn-success"><i class="fa fa-info-circle"></i>
                                                        Lihat
                                                        Detail</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </section>

        </main>
    </div>


@endsection
