@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        button.btn.active {
            color: #fff;
            background-color: #03a9f3;
            border-color: #03a9f3;
            -webkit-box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
            box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
        }

        .my-shadow {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(58 59 69 / 15%) !important;
        }

        .bg-lime {
            background-color: #01ff70 !important;
        }

    </style>
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"><b>CETAK BERITA ACARA</b></h5>
        </div>
        <div class="page-title-right">
            <a class="btn btn-danger" href="{{ route('cbt_cetak') }}"><i class="fas fa-arrow-alt-circle-left"></i>
                Kembali</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card my-shadow">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title mr-b-0">Setting Kop</h5>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="card-tools btn bg-success text-white float-right"
                                onclick="submitKop()" id="btnSave">
                                <i class="fas fa-save mr-1"></i> Simpan
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="set-kop">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 1</label>
                                    <textarea id="header-1" class="form-control" name="header_1" rows="2" placeholder="Header baris 1"
                                        required>{{ !empty($berita) ? $berita['header1'] : '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 2</label>
                                    <textarea id="header-2" class="form-control" name="header_2" rows="2" placeholder="Header baris 2"
                                        required>{{ !empty($berita) ? $berita['header2'] : '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 3</label>
                                    <textarea id="header-3" class="form-control" name="header_3" rows="2" placeholder="Header baris 3"
                                        required>{{ !empty($berita) ? $berita['header3'] : '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 4</label>
                                    <textarea id="header-4" class="form-control" name="header_4" rows="2" placeholder="Header baris 4"
                                        required>{{ !empty($berita) ? $berita['header4'] : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-4">
                            <form id="set-logo-kiri">
                                <div class="form-group">
                                    <label for="l39">Logo Kiri</label>
                                    <br>
                                    <input type="file" name="image" id="logo-kiri" accept="image/*">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form id="set-logo-kanan">
                                <div class="form-group">
                                    <label for="l39">Logo Kanan</label>
                                    <br>
                                    <input type="file" name="image" id="logo-kanan" accept="image/*">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title mr-b-0">Cetak</h5>
                        </div>
                        <div class="col-md-4">
                            <div class="btn-group float-right" id="selector" role="group" aria-label="Basic example">
                                <button type="button" class="btn active btn-outline-info">By Ruang</button>
                                <button type="button" class="btn btn-outline-info">By Kelas</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 col-md-3 mb-3 d-none" id="by-kelas">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label" for="l0">Rombel</label>
                                <div class="col-md-8">
                                    <select name="rombel" id="rombel" class="form-control">
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ $rombel['id'] }}">
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 mb-3" id="by-ruang">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label" for="l0">Ruang</label>
                                <div class="col-md-8">
                                    <select name="ruang" id="ruang" class="form-control">
                                        @foreach ($ruang as $rg)
                                            <option value="{{ $rg['id'] }}">{{ $rg['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 mb-3">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Sesi</label>
                                <div class="col-md-9">
                                    <select name="sesi" id="sesi" class="form-control">
                                        @foreach ($sesi as $ss)
                                            <option value="{{ $ss['id'] }}">{{ $ss['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-9 col-md-4 mb-3">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Jadwal</label>
                                <div class="col-md-9">
                                    <select name="jadwal" id="jadwal" class="form-control">
                                        @foreach ($jadwal as $jdw)
                                            <option value="{{ $jdw['id'] }}">{{ $jdw['kode_bank'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 col-md-2 mb-3">
                            <button class="btn bg-success text-white" id="btn-print">
                                <i class="fa fa-print"></i><span class="ml-1">Cetak</span>
                            </button>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <div id="print-preview" class="p-4">
                        <div style="display: flex; justify-content: center; align-items: center;">
                            <div style="width: 21cm; height: 30cm; padding: 1cm" class="border my-shadow">
                                <table id="table-header-print" style="width: 100%; border: 0;">
                                    <tr>
                                        <td style="width:15%;">
                                            <img alt="logo kiri" id="prev-logo-kiri-print"
                                                src="{{ !empty($berita) ? $berita['file1'] : '' }}"
                                                style="width:85px; height:85px; margin: 6px;">
                                        </td>
                                        <td style="width:70%; text-align: center;">
                                            <div id="prev-header-1"
                                                style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 14pt">
                                                {{ !empty($berita) ? $berita['header1'] : '' }}
                                            </div>
                                            <div style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 16pt">
                                                <b id="prev-header-2">{{ !empty($berita) ? $berita['header2'] : '' }}</b>
                                            </div>
                                            <div id="prev-header-3"
                                                style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 13pt">
                                                {{ !empty($berita) ? $berita['header3'] : '' }}
                                            </div>
                                            <div id="prev-header-4"
                                                style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 12pt">
                                                {{ !empty($berita) ? $berita['header4'] : '' }}
                                            </div>
                                        </td>
                                        <td style="width:15%;">
                                            <img alt="logo kanan" id="prev-logo-kanan-print"
                                                src="{{ !empty($berita) ? $berita['file2'] : '' }}"
                                                style="width:85px; height:85px; margin: 6px; border-style: none">
                                        </td>
                                    </tr>
                                </table>
                                <hr style="border: 1px solid; margin-bottom: 6px">
                                <br>
                                <br>
                                <div style="text-align: justify; font-family: 'Times New Roman'">
                                    Pada hari ini <span class="editable bg-lime" id="edit-hari"
                                        style="display: inline-block;min-width: 20px">{{ (new \App\Helpers\Help())->getHariOnly(now()) }}</span>
                                    tanggal <span class="editable bg-lime" id="edit-tanggal"
                                        style="display: inline-block;min-width: 20px">{{ date('d') }}</span>
                                    bulan <span class="editable bg-lime" id="edit-bulan"
                                        style="display: inline-block;min-width: 20px">{{ (new \App\Helpers\Help())->getNumberMonthIndo(date('m')) }}</span>
                                    tahun <span class="editable bg-lime" id="edit-tahun"
                                        style="display: inline-block;min-width: 20px">{{ date('Y') }}</span>
                                    telah diselenggarakan <span class="editable bg-lime" id="edit-jenis-ujian"
                                        style="display: inline-block;min-width: 20px">............................................</span>
                                    untuk Mata Pelajaran <span class="editable bg-lime" id="edit-mapel"
                                        style="display: inline-block;min-width: 20px">.....................................</span>
                                    dari pukul <span class="editable bg-lime" id="edit-waktu-mulai"
                                        style="display: inline-block;min-width: 20px">.............</span>
                                    sampai dengan pukul <span class="editable bg-lime" id="edit-waktu-akhir"
                                        style="display: inline-block;min-width: 20px">...........</span>
                                </div>
                                <br>
                                <table style="width: 100%;font-family: 'Times New Roman';">
                                    <tr>
                                        <td style="width: 30px;">1. </td>
                                        <td style="width: 30%;">
                                            Pada Sekolah
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-nama_sekolah"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td id="title-ruang">
                                            Ruang
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-ruang">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Sesi</td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-sesi">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            Jumlah Peserta Seharusnya
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-jml-peserta">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            Jumlah Peserta Hadir
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-hadir">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            Jumlah Peserta Tidak Hadir
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-tidak-hadir">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            Nomor Peserta Tidak Hadir
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime" id="edit-username">
                                            .................................................................</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 12px">2.</td>
                                        <td style="padding-top: 12px" colspan="3">
                                            Catatan selama <span class="editable bg-lime" id="edit-nama-ujian"
                                                style="display: inline-block;min-width: 20px">.......</span> berlangsung :
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="3" style="height: 100px; border: 1px solid black; padding: 12px"
                                            class="editable bg-lime" id="edit-catatan"></td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                                <br>
                                <br>
                                <table style="width:90%; font-family: 'Times New Roman';">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="text-align: center">TTD</th>
                                    </tr>
                                    <tr>
                                        <td style="width: 30px;">1.</td>
                                        <td>Proktor</td>
                                        <td>:</td>
                                        <td class="editable bg-lime"></td>
                                        <td style="padding-left: 20px" rowspan="2">1. _________________________</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            NIP
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime">_________________________</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 12px">2.</td>
                                        <td style="padding-top: 12px">
                                            Pengawas
                                        </td>
                                        <td style="padding-top: 12px">:</td>
                                        <td style="padding-top: 12px" class="editable bg-lime" id="edit-pengawas"></td>
                                        <td style="padding-left: 20px" rowspan="2">2. _________________________</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            NIP
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime">_________________________</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 12px">3.</td>
                                        <td style="padding-top: 12px">
                                            Kepala Sekolah
                                        </td>
                                        <td style="padding-top: 12px">:</td>
                                        <td style="padding-top: 12px" class="editable bg-lime"></td>
                                        <td style="padding-left: 20px" rowspan="2">3. _________________________</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            NIP
                                        </td>
                                        <td>:</td>
                                        <td class="editable bg-lime">_________________________</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btn-group > .btn").click(function() {
                $(this).addClass("active").siblings().removeClass("active");
            });
        });

        var berita = {!! json_encode($berita) !!};

        var oldVal1 = berita != null ? berita['header1'] : '';
        var oldVal2 = berita != null ? berita['header2'] : '';
        var oldVal3 = berita != null ? berita['header3'] : '';
        var oldVal4 = berita != null ? berita['header4'] : '';

        var logoKanan = berita != null ? berita['file2'] : '';
        var logoKiri = berita != null ? berita['file1'] : '';

        var printBy = 1;
        var infoData = {};
        var infoSiswa = [];
        var allInfo = '';
        var oldInfo = '';

        var hari = ['Minngu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
        var bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'];

        var d = new Date();
        var curr_day = d.getDay();
        var curr_date = d.getDate();

        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();

        function buatTanggal() {
            return hari[curr_day] + ", " + curr_date + "  " + bulan[curr_month] + " " + curr_year;
        }

        function submitKop() {
            $('#set-kop').submit();
        }

        $(document).ready(function() {
            var opsiJadwal = $("#jadwal");
            var opsiRuang = $("#ruang");
            var opsiSesi = $("#sesi");
            var opsiKelas = $("#rombel");

            $('.editable').attr('contentEditable', true);

            function loadSiswaRuang(ruang, sesi, jadwal) {
                var notempty = ruang && sesi && jadwal;
                if (notempty) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('cbt_print-hadir_ruang') }}",
                        data: {
                            ruang,
                            sesi,
                            jadwal
                        },
                        success: function(response) {
                            if (response.status == 'berhasil') {
                                $('#edit-jml-peserta').html('<b>' + response.siswa.length + '</b>');

                                $('#edit-jenis-ujian').html('<b>' + response.jenis_ujian +
                                    '</b>');
                                $('#edit-nama-ujian').html('<b>' + response.jenis_ujian + '<b>');
                                $('#edit-waktu-mulai').html('<b>' + response.jam_mulai
                                    .substring(0, 5) + '</b>');
                                $('#edit-waktu-akhir').html('<b>' + response.jam_selesai
                                    .substring(0, 5) + '</b>');
                                $('#edit-mapel').html('<b>' + response.mapel + '</b>');
                                $('#edit-pengawas').text(response.pengawas[0].nama);
                            } else {
                                alert('GAGAL!', 'Terdapat gangguan pemrosesan data');
                            }

                        },
                        error: function(data) {
                            console.log('Error:', data);
                            alert('GAGAL!', 'Terdapat error saat proses data');
                        }
                    });
                }
            }

            $("#logo-kanan").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kanan-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kanan')[0]);
                    uploadAttach("{{ route('cbt_upload-berita_acara_logo_kanan') }}", form);
                }
            });
            $("#logo-kiri").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kiri-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kiri')[0]);
                    uploadAttach("{{ route('cbt_upload-berita_acara_logo_kiri') }}", form);
                }
            });

            function loadSiswaKelas(kelas, sesi, jadwal) {
                var notempty = kelas && sesi && jadwal;
                if (notempty) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('cbt_print-hadir_kelas') }}",
                        data: {
                            kelas,
                            sesi,
                            jadwal
                        },
                        success: function(response) {
                            if (response.status == 'berhasil') {
                                $('#edit-jml-peserta').html('<b>' + response.hadir.peserta.length +
                                    '</b>');
                                $('#edit-jenis-ujian').html('<b>' + response.hadir.jenis_ujian +
                                    '</b>');
                                $('#edit-nama-ujian').html('<b>' + response.hadir.jenis_ujian + '<b>');
                                $('#edit-waktu-mulai').html('<b>' + response.hadir.jam_mulai
                                    .substring(0, 5) + '</b>');
                                $('#edit-ruang').html('<b>' + response.hadir.rombel + '</b>');
                                $('#edit-waktu-akhir').html('<b>' + response.hadir.jam_selesai
                                    .substring(0, 5) + '</b>');
                                $('#edit-mapel').html('<b>' + response.hadir.mapel + '</b>');
                                $('#edit-pengawas').text(response.hadir.pengawas[0].nama);
                            } else {
                                alert('GAGAL!', 'Terdapat gangguan pemrosesan data');
                            }

                        },
                        error: function(data) {
                            console.log('Error:', data);
                            alert('GAGAL!', 'Terdapat error saat proses data');
                        }
                    });
                }
            }

            opsiJadwal.prepend("<option value='' selected='selected'>Pilih Jadwal</option>");
            opsiRuang.prepend("<option value='' selected='selected'>Pilih Ruang</option>");
            opsiSesi.prepend("<option value='' selected='selected'>Pilih Sesi</option>");
            opsiKelas.prepend("<option value='' selected='selected'>Pilih Kelas</option>");


            opsiKelas.change(function() {
                $('#edit-ruang').text($("#rombel option:selected").text());
                loadSiswaKelas($(this).val(), opsiSesi.val(), opsiJadwal.val())
            });

            opsiRuang.change(function() {
                $('#edit-ruang').text($("#ruang option:selected").text());
                loadSiswaRuang($(this).val(), opsiSesi.val(), opsiJadwal.val())
            });

            opsiSesi.change(function() {
                $('#edit-sesi').text($("#sesi option:selected").text());
                if (printBy === 1) {
                    loadSiswaRuang(opsiRuang.val(), $(this).val(), opsiJadwal.val())
                } else {
                    loadSiswaKelas(opsiKelas.val(), $(this).val(), opsiJadwal.val())
                }
            });

            opsiJadwal.change(function() {
                if (printBy === 1) {
                    loadSiswaRuang(opsiRuang.val(), opsiSesi.val(), $(this).val())
                } else {
                    loadSiswaKelas(opsiKelas.val(), opsiSesi.val(), $(this).val())
                }
            });

            $("#btn-print").click(function() {
                var kosong = printBy === 2 ? ($('#rombel').val() === '' || ($('#sesi').val() === '') || ($(
                    '#jadwal').val() === '')) : ($('#ruang').val() === '' || ($('#sesi').val() ===
                    '') || ($('#jadwal').val() === ''));
                if (kosong) {
                    swa("ERROR!", "Isi semua pilihan terlebih dulu", "error");
                } else {
                    // console.log("tes");
                    $('#print-preview').printArea();
                }
            });

            $("#header-1").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal1) {
                    return;
                }

                oldVal1 = currentVal;
                $('#prev-header-1').html(currentVal);
                //alert("changed!");
            });
            $("#header-2").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal2) {
                    return;
                }

                oldVal2 = currentVal;
                $('#prev-header-2').text(currentVal);
                //alert("changed!");
            });
            $("#header-3").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal3) {
                    return;
                }

                oldVal3 = currentVal;
                $('#prev-header-3').text(currentVal);
                //alert("changed!");
            });
            $("#header-4").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal4) {
                    return;
                }
                oldVal4 = currentVal;
                $('#prev-header-4').text(currentVal);
            });

            $('#set-kop').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                $.ajax({
                    url: "{{ route('cbt_save-berita_acara') }}",
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        $('#btnSave').html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    },
                    success: function(response) {
                        if (response.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#btnSave').html(
                                '<i class="fas fa-save mr-1"></i> Simpan');
                        }
                        //history.back();
                        swa(response.status.toUpperCase() + "!", response.message, response
                            .icon);
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $('#selector button').click(function() {
                $(this).addClass('active').siblings().addClass('btn-outline-primary').removeClass(
                    'active btn-primary');

                if (!$('#by-kelas').is(':hidden')) {
                    $('#by-kelas').addClass('d-none');
                    $('#by-ruang').removeClass('d-none');
                    printBy = 1;
                    $('#title-ruang').text('Ruang');
                } else {
                    $('#by-kelas').removeClass('d-none');
                    $('#by-ruang').addClass('d-none');
                    $('#title-ruang').text('Rombel');
                    printBy = 2;
                }
            });

            function uploadAttach(action, data) {
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        if (data.src.includes('kanan')) {
                            logoKanan = data.src;
                        } else if (data.src.includes('kiri')) {
                            logoKiri = data.src;
                            //console.log('kiri', data.src);
                        } else if (data.src.includes('tanda')) {
                            tandatangan = data.src;
                            //console.log('tandatangan', data.src);
                        }
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $.toast({
                            heading: "ERROR!!",
                            text: "file tidak terbaca",
                            icon: 'error',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 5000,
                            position: 'top-right'
                        });
                    }
                });
            }

        })
    </script>
@endsection
