@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 font-weight-bold">CETAK KARTU PESERTA</h5>
        </div>
        <div class="page-title-right">
            <a class="btn btn-danger" href="{{ route('cbt_cetak') }}"><i class="fas fa-arrow-alt-circle-left"></i>
                Kembali</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title mr-b-0">Setting Kartu</h5>
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <button class="card-tools btn bg-info text-white" onclick="submitKartu()">
                                    <i class="fas fa-save mr-1"></i> Simpan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="set-kartu">
                        <div class="form-group">
                            <label>Header 1</label>
                            <textarea id="header-1" class="form-control" name="header_1" rows="2"
                                placeholder="Header baris 1"
                                required>{{ !empty($kartu) ? $kartu['header1'] : '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Header 2</label>
                            <textarea id="header-2" class="form-control" name="header_2" rows="2"
                                placeholder="Header baris 2"
                                required>{{ !empty($kartu) ? $kartu['header2'] : '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Header 3</label>
                            <textarea id="header-3" class="form-control" name="header_3" rows="2"
                                placeholder="Header baris 3"
                                required>{{ !empty($kartu) ? $kartu['header3'] : '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Header 4</label>
                            <textarea id="header-4" class="form-control" name="header_4" rows="2"
                                placeholder="Header baris 4"
                                required>{{ !empty($kartu) ? $kartu['header4'] : '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input id="input-tanggal" class="form-control" name="tanggal" placeholder="Titimangsa"
                                value="{{ !empty($kartu) ? $kartu['tanggal'] : '' }}" required>
                        </div>
                        <div class="form-group">
                            <label>Nama Kepala Sekolah</label>
                            <input id="input-kepsek" class="form-control" name="kepsek" placeholder="Nama Kepala Sekolah"
                                value="{{ !empty($kartu) ? $kartu['kepsek'] : '' }}" required>
                        </div>
                        <div class="form-group">
                            <label>Kabupaten</label>
                            <input id="kabupaten" class="form-control" name="kabupaten" placeholder="kabupaten Sekolah"
                                value="{{ !empty($kartu) ? $kartu['kabupaten'] : '' }}" required>
                        </div>
                    </form>
                    <form id="set-logo-kiri">
                        <div class="form-group">
                            <label>Logo Kiri</label>
                            <input type="file" class="form-control-file" name="image" id="logo-kiri" accept="image/*">
                        </div>
                    </form>
                    <form id="set-logo-kanan">
                        <div class="form-group">
                            <label>Logo Kanan</label>
                            <input type="file" class="form-control-file" name="image" id="logo-kanan" accept="image/*">
                        </div>
                    </form>
                    <form id="set-gambar_stempel">
                        <div class="form-group">
                            <label>Stempel</label>
                            <input type="file" class="form-control-file" name="image" id="file-stempel" accept="image/*">
                        </div>
                    </form>
                    <form id="set-gambar_ttd">
                        <div class="form-group">
                            <label>Ttd Kepala Sekolah</label>
                            <input type="file" class="form-control-file" name="image" id="file-ttd" accept="image/*">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="box-title mr-b-0">Preview</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body pb-4">
                    <div style="display: flex; justify-content: center; align-items: center;">
                        <div style="width: 10cm">
                            <table id="table-header"
                                style="width: 100%; border-top: 1px solid black; border-bottom: 0;border-left: 1px solid black; border-right: 1px solid black">
                                <tr>
                                    <td style="width:20%;">
                                        <img alt="Logo kiri" id="prev-logo-kiri"
                                            src="{{ !empty($kartu) ? $kartu['file1'] : '' }}"
                                            style="width:55px; height:55px; margin: 6px;">
                                    </td>
                                    <td style="width:60%;">
                                        <div id="prev-header-1" class="text-center"
                                            style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 9pt">
                                            {{ !empty($kartu) ? $kartu['header1'] : '' }}
                                        </div>
                                        <div id="prev-header-2" class="text-center"
                                            style="line-height: 1.1; font-family: 'Times New Roman'; font-size: 10pt">
                                            <b>{{ !empty($kartu) ? $kartu['header2'] : '' }}</b>
                                        </div>
                                        <div id="prev-header-3" class="text-center"
                                            style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 8pt">
                                            {{ !empty($kartu) ? $kartu['header3'] : '' }}</div>
                                        <div id="prev-header-4" class="text-center"
                                            style="line-height: 1.2; font-family: 'Times New Roman'; font-size: 8pt">
                                            {{ !empty($kartu) ? $kartu['header4'] : '' }}</div>
                                    </td>
                                    <td style="width:20%;">
                                        <img alt="Logo kanan" id="prev-logo-kanan"
                                            src="{{ !empty($kartu) ? $kartu['file2'] : '' }}"
                                            style="width:55px; height:55px; margin: 6px; border-style: none">
                                    </td>
                                </tr>
                            </table>
                            <table id="table-body" style="width:100%;border: 1px solid black">
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-top:8px;padding-left:22px;width: 35%">Nomor Peserta</td>
                                    <td style="padding-top:8px;">:</td>
                                    <td style="padding-top:8px;width: 60%">0000.00.000</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px">Nama</td>
                                    <td>:</td>
                                    <td>Nama Siswa</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px;width: 35%">NIS/NISN</td>
                                    <td>:</td>
                                    <td>012334455</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px;width: 35%">Kelas</td>
                                    <td>:</td>
                                    <td>IXA</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px;width: 35%">Ruang/Sesi</td>
                                    <td>:</td>
                                    <td>1/2</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px;width: 35%">Username</td>
                                    <td>:</td>
                                    <td>umbk001</td>
                                </tr>
                                <tr style="line-height: 1; font-family: 'Times New Roman'; font-size: 9pt">
                                    <td style="padding-left:22px;width: 35%">Password</td>
                                    <td>:</td>
                                    <td>umbk001</td>
                                </tr>
                                <tr>
                                    <td colspan="2"
                                        style="padding-top: 6px; padding-bottom: 6px; padding-left:22px;width: 35%">
                                        <div
                                            style="width: 60px; height: 70px; background: url('{{ asset('images/siswa.png') }}') no-repeat center; background-size: cover; outline: 1px solid;">
                                        </div>
                                    </td>
                                    <td style="text-align: center;">
                                        <div id="prev-tandatangan"
                                            style="font-family: 'Times New Roman'; font-size: 9pt; line-height: 1; background: url('{{ !empty($kartu) ? $kartu['stempel'] : '' }}') no-repeat center; background-size: 100px 60px">
                                            <span id="prev-kota">{{ !empty($kartu) ? $kartu['kabupaten'] : '' }}</span>,
                                            <span id="prev-tanggal">{{ !empty($kartu) ? $kartu['tanggal'] : '' }}</span>
                                            <br>
                                            Kepala Sekolah
                                            <br>
                                            <img alt="Logo TTD" id="prev-logo-ttd"
                                                src="{{ !empty($kartu) ? $kartu['ttd'] : '' }}"
                                                style="width:55px; height:48px; margin: 6px; border-style: none">
                                            <br>
                                            <span id="prev-kepsek">{{ !empty($kartu) ? $kartu['kepsek'] : '' }}</span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="card my-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="box-title mr-b-0">Cetak</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body pb-4">
                    <div class="row">
                        <div class="col-8">
                            <div class="input-group">
                                <select name="rombel" id="rombel" class="form-control">
                                    @foreach ($rombel as $rm)
                                        <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <button class="btn bg-success text-white" id="btn-print">
                                <i class="fa fa-print"></i><span class="ml-1">Cetak</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="box-title mr-b-0">Preview</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body pb-4">
                    <div class="d-flex justify-content-center bg-gray-light" style="min-height: 300mm">
                        <div id="print-preview" class="m-2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        var kartu = {!! json_encode($kartu) !!};
        // console.log(kartu);
        var oldVal1 = kartu != null ? kartu['header1'] : '';
        var oldVal2 = kartu != null ? kartu['header2'] : '';
        var oldVal3 = kartu != null ? kartu['header3'] : '';
        var oldVal4 = kartu != null ? kartu['header4'] : '';
        var oldTgl = kartu != null ? kartu['tanggal'] : '';
        var oldKota = kartu != null ? kartu['kabupaten'] : '';
        var kepsek = kartu != null ? kartu['kepsek'] : '';
        var logoKanan = kartu != null ? kartu['file2'] : '';
        var logoKiri = kartu != null ? kartu['file1'] : '';
        var tandatangan = kartu != null ? kartu['ttd'] : '';
        var stempel = kartu != null ? kartu['stempel'] : '';
        var printBy = 1;

        function submitKartu() {
            $('#set-kartu').submit();
        }

        function createPrintPreview(data) {
            console.log(data);
            var bagi2 = Math.round(data.length / 2);
            var pages = Math.round(bagi2 / 4);
            var konten = '';
            if (Object.keys(data).length) {
                for (let a = 0; a < pages; a++) {
                    var card = '<div class="border my-shadow mb-3 pt-4 bg-white"><div class="pt-4" ' +
                        'style="display: flex;-webkit-justify-content: center;justify-content: center;background: white;width: 210mm; height: 297mm;padding: 1mm">';

                    var tds = [];
                    var kelas = printBy === 1 ? 'Kelas/Sesi' : 'Ruang/Sesi';

                    let t = a * 8;
                    let end = (a + 1) < pages ? t + 8 : data.length;
                    //console.log('t', t);
                    //console.log('end', end);

                    for (let i = t; i < end; i++) {
                        var setSiswa = data[i].set_siswa === '1';
                        var ruang = setSiswa ? data[i].ruang_kelas : data[i].kode_ruang;
                        var sesi = setSiswa ? data[i].sesi_kelas : data[i].kode_sesi;
                        //var kelasVal = printBy === 1 ? data[i].nama_kelas : ruang;

                        //var foto = data[i].foto == null || data[i].foto === '' ? 'siswa.png' : data[i].foto;
                        //var foto = getFoto(data[i].foto);
                        var td = '<div style="display: flex; justify-content: center; align-items: center;">' +
                            '<div style="width: 10cm">' +
                            '<table id="table-header-print" style="width: 100%; border-top: 1px solid black; border-bottom: 0;border-left: 1px solid black; border-right: 1px solid black">' +
                            '<tr>' +
                            '<td style="width:20%;">' +
                            '<img id="prev-logo-kiri-print" src="' + logoKiri +
                            '" style="width:55px; height:55px; margin-left: 6px; margin-right: 6px; margin-top:4px;">' +
                            '</td>' +
                            '<td style="width:60%; text-align: center;">' +
                            '<div style="line-height: 1.1; font-family: \'Times New Roman\'; font-size: 9pt">' + oldVal1 +
                            '</div>' +
                            '<div class="text-center" style="line-height: 1.1; font-family: \'Times New Roman\'; font-size: 10pt"><b>' +
                            oldVal2 + '</b></div>' +
                            '<div class="text-center" style="line-height: 1.2; font-family: \'Times New Roman\'; font-size: 8pt">' +
                            oldVal3 + '</div>' +
                            '<div class="text-center" style="line-height: 1.2; font-family: \'Times New Roman\'; font-size: 8pt">' +
                            oldVal4 + '</div>' +
                            '</td>' +
                            '<td style="width:20%;">' +
                            '<img id="prev-logo-kanan-print" src="' + logoKanan +
                            '" style="width:55px; height:55px; margin-left: 6px; margin-right: 6px; margin-top:4px; border-style: none">' +
                            '</td>' +
                            '</tr>' +
                            '</table>' +
                            '<table id="table-body-print" style="width:100%;border: 1px solid black">' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 10pt">' +
                            '<td style="padding-top:4px;padding-left:22px;width: 30%">No. Peserta</td>' +
                            '<td style="padding-top:4px;">:</td>' +
                            '<td style="padding-top:4px;width: 65%">' + data[i].nomor_peserta + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">Nama</td>' +
                            '<td>:</td>' +
                            '<td>' + data[i].nama + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">NIS / NISN</td>' +
                            '<td>:</td>' +
                            '<td>' + data[i].nis + '/' + data[i].nisn + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">Kelas</td>' +
                            '<td>:</td>' +
                            '<td>' + data[i].rombel + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">Ruang/Sesi</td>' +
                            '<td>:</td>' +
                            '<td>' + ruang + '/' + sesi + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">Username</td>' +
                            '<td>:</td>' +
                            '<td>' + data[i].username + '</td>' +
                            '</tr>' +
                            '<tr style="line-height: 1; font-family: \'Times New Roman\'; font-size: 9pt">' +
                            '<td style="padding-left:22px;width: 30%">Password</td>' +
                            '<td>:</td>' +
                            '<td>' + data[i].password + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td colspan="2" style="padding-top: 6px; padding-bottom: 6px; padding-left:22px;width: 35%">' +
                            '<img class="avatar" style="width: 60px; height: 70px; object-fit: cover;object-position: center; ' +
                            'outline: 1px solid;" ' +
                            'src= "' + data[i].file + '"' +
                            '/>' +
                            '</td>' +
                            '<td style="text-align: center;">' +
                            '<div id="prev-tandatangan-print" style="font-family: \'Times New Roman\'; font-size: 9pt; line-height: 1; background: url(' +
                            stempel + ') no-repeat center; background-size: 100px 60px"' +
                            '<span>' + oldKota + '</span>, <span>' + oldTgl + '</span>' +
                            '<br>Kepala Sekolah' +
                            '<br>' +
                            '<img alt="Logo kanan" id="prev-logo-ttd"' +
                            'src="' + tandatangan + '" style="width:55px; height:48px; margin: 6px; border-style: none">' +
                            '<br>' +
                            '<span>' + kepsek + '</span>' +
                            '</div>' +
                            '</td>' +
                            '</tr>' +
                            '</table>' +
                            '</div>' +
                            '</div>';

                        tds.push(td);
                    }
                    var table = '<table>';
                    for (let j = 0; j < tds.length; j++) {
                        if ((j + 1) % 2 === 0) {
                            table += '<td style="padding: 5px;">' + tds[j] + '</td></tr>';
                        } else {
                            table += '<tr><td style="padding: 5px;">' + tds[j] + '</td>';
                        }
                    }
                    table += '</table>';
                    card += table + '</div></div>';
                    konten += card + '<div style="page-break-after: always"></div>';
                }
            } else {
                alert('data siswa dengan rombel tersebut saat ini belum tersedia');
            }
            $("#print-preview").html(konten);
            $(`.avatar`).each(function() {
                $(this).on("error", function() {
                    $(this).attr("src", '{{ asset('images/siswa.png') }}');
                });
            });

        }

        $(document).ready(function() {
            $("#logo-kanan").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kanan').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kanan')[0]);
                    uploadAttach("{{ route('cbt_save_kartu-logo_kanan') }}", form);
                }
            });

            $("#file-ttd").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-ttd').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-gambar_ttd')[0]);
                    uploadAttach("{{ route('cbt_save_kartu-ttd') }}", form);
                }
            });

            $("#logo-kiri").change(function() {
                var input = $(this)[0];
                var data = new FormData(document.getElementById("set-logo-kiri"));

                // console.log(new FormData(this));
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kiri').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kiri')[0]);
                    uploadAttach("{{ route('cbt_save_kartu-logo_kiri') }}", form);
                }
            });

            $("#file-stempel").change(function() {
                var input = $(this)[0];
                var data = new FormData(document.getElementById("set-gambar_stempel"));

                // console.log(new FormData(this));
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $("#prev-tandatangan").css("background-image", "url(" + e.target.result + ")");
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-gambar_stempel')[0]);
                    uploadAttach("{{ route('cbt_save_kartu-stempel') }}", form);
                }
            });

            $("#header-1").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal1) {
                    return;
                }

                oldVal1 = currentVal;
                $('#prev-header-1').html(currentVal);
                //alert("changed!");
            });
            $("#header-2").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal2) {
                    return;
                }

                oldVal2 = currentVal;
                $('#prev-header-2').text(currentVal);
                //alert("changed!");
            });
            $("#header-3").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal3) {
                    return;
                }

                oldVal3 = currentVal;
                $('#prev-header-3').text(currentVal);
                //alert("changed!");
            });
            $("#header-4").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal4) {
                    return;
                }
                oldVal4 = currentVal;
                $('#prev-header-4').text(currentVal);
            });

            $('#input-tanggal').on('input', function(e) {
                var tgl = $(this).val();
                $('#prev-tanggal').text(tgl);
                oldTgl = tgl;
            });
            $('#input-kepsek').on('input', function(e) {
                var input_kepsek = $(this).val();
                $('#prev-kepsek').text(input_kepsek);
                kepsek = input_kepsek;
            });
            $('#kabupaten').on('input', function(e) {
                var input_kab = $(this).val();
                $('#prev-kota').text(input_kab);
                olKota = input_kab;
            });

            $('#set-kartu').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                $.ajax({
                    url: "{{ route('cbt_save-kartu_peserta') }}",
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        $(".card-tools").html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    },
                    success: function(response) {
                        window.location.reload();
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                        $(".card-tools").html(
                            '<i class="fas fa-save mr-1"></i> Simpan');
                    }
                });
            });

            function uploadAttach(action, data) {
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        if (data.src.includes('kanan')) {
                            logoKanan = data.src;
                        } else if (data.src.includes('kiri')) {
                            logoKiri = data.src;
                            //console.log('kiri', data.src);
                        } else if (data.src.includes('tanda')) {
                            tandatangan = data.src;
                            //console.log('tandatangan', data.src);
                        }
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $.toast({
                            heading: "ERROR!!",
                            text: "file tidak terbaca",
                            icon: 'error',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 5000,
                            position: 'top-right'
                        });
                    }
                });
            }

            // function deleteImage(src) {
            //     $.ajax({
            //         data: {
            //             src: src
            //         },
            //         type: "POST",
            //         url: base_url + "cbtcetak/deletefile",
            //         cache: false,
            //         success: function(response) {
            //             console.log(response);
            //         }
            //     });
            // }

            function loadSiswaKelas(kelas) {
                $.ajax({
                    type: "GET",
                    url: "kartu/cetak?rombel=" + kelas,
                    success: function(response) {
                        createPrintPreview(response);
                    }
                });
            }

            function loadSiswaRuang(ruang) {
                $.ajax({
                    type: "GET",
                    url: base_url + "cbtcetak/getsiswaruang?ruang=" + ruang,
                    success: function(response) {
                        createPrintPreview(response.siswa);
                    }
                });
            }

            //loadSiswaKelas($('#kelas').val());
            $("#rombel").prepend("<option value='' selected='selected'>Pilih Rombel</option>");
            $("#rombel").change(function() {
                loadSiswaKelas($(this).val());
            });

            $("#btn-print").click(function() {
                if ($('#rombel').val() === '') {
                    swa("GAGAL!", "Pilih rombel dulu", "error");
                } else {
                    $('#print-preview').printArea();
                }
            });

            $('#selector button').click(function() {
                $(this).addClass('active').siblings().addClass('btn-outline-primary').removeClass(
                    'active btn-primary');

                if (!$('#kelas').is(':hidden')) {
                    $('#kelas').addClass('d-none');
                    $('#ruang').removeClass('d-none');
                    printBy = 2;
                } else {
                    $('#kelas').removeClass('d-none');
                    $('#ruang').addClass('d-none');
                    printBy = 1;
                }
            });
        })
    </script>
@endsection
