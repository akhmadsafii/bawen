@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        button.btn.active {
            color: #fff;
            background-color: #03a9f3;
            border-color: #03a9f3;
            -webkit-box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
            box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
        }

    </style>
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5 font-weight-bold">CETAK FORMAT ABSENSI</h5>
        </div>
        <div class="page-title-right">
            <a class="btn btn-danger" href="{{ route('cbt_cetak') }}"><i class="fas fa-arrow-alt-circle-left"></i>
                Kembali</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="card my-shadow">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title mr-b-0">Setting Kop Daftar Kehadiran</h5>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn-update btn bg-success text-white float-right"
                                onclick="submitKop()">
                                <i class="fas fa-save mr-1"></i> Simpan
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="set-kop">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 1</label>
                                    <input class="form-control" name="header_1" placeholder="Header 1"
                                        value="{{ $hadir != null ? $hadir['header1'] : '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 2</label>
                                    <input class="form-control" name="header_2" placeholder="Header 2"
                                        value="{{ $hadir != null ? $hadir['header2'] : '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 3</label>
                                    <input class="form-control" name="header_3" placeholder="Header 3"
                                        value="{{ $hadir != null ? $hadir['header3'] : '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Header 4</label>
                                    <input class="form-control" name="header_4" placeholder="Header 4"
                                        value="{{ $hadir != null ? $hadir['header4'] : '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama Proktor</label>
                                    <input id="input-proktor" class="form-control" name="proktor" placeholder="Proktor"
                                        value="{{ $hadir != null ? $hadir['proktor'] : '' }}">
                                </div>
                            </div>
                    </form>
                    <div class="col-md-4">
                        <form id="form_logo_kiri">
                            <div class="form-group">
                                <label>Logo Kiri</label>
                                <input type="file" class="form-control-file" name="image" id="file-logo_kiri"
                                    accept="image/*">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form id="form_logo_kanan">
                            <div class="form-group">
                                <label>Logo Kanan</label>
                                <input type="file" class="form-control-file" name="image" id="file-logo_kanan"
                                    accept="image/*">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form id="form_ttd_proktor">
                            <div class="form-group">
                                <label>TTD Proktor</label>
                                <input type="file" class="form-control-file" name="image" id="file-proktor"
                                    accept="image/*">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form id="form_ttd_pengawas1">
                            <div class="form-group">
                                <label>TTD Pengawas 1</label>
                                <input type="file" class="form-control-file" name="image" id="file-ttd_pengawas1"
                                    accept="image/*">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form id="form_ttd_pengawas2">
                            <div class="form-group">
                                <label>TTD Pengawas 2</label>
                                <input type="file" class="form-control-file" name="image" id="file-ttd_pengawas2"
                                    accept="image/*">
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <h5 class="box-title text-center">PREVIEW</h5>
                <div class="row justify-content-md-center">
                    <div class="col col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <label>Logo Kiri</label>
                                <img id="prev-logo-kiri"
                                    src="{{ !empty($hadir) && $hadir['file1'] != null ? $hadir['file1'] : 'https://via.placeholder.com/150' }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <label>Logo Kanan</label>
                                <img id="prev-logo-kanan"
                                    src="{{ !empty($hadir) && $hadir['file2'] != null ? $hadir['file2'] : 'https://via.placeholder.com/150' }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <label>TTD Proktor</label>
                                <img id="prev-ttd-proktor"
                                    src="{{ !empty($hadir) && $hadir['ttd_proktor'] != null ? $hadir['ttd_proktor'] : 'https://via.placeholder.com/150' }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <label>Pengawas 1</label>
                                <img id="prev-ttd-pengawas1"
                                    src="{{ !empty($hadir) && $hadir['ttd_peng1'] != null ? $hadir['ttd_peng1'] : 'https://via.placeholder.com/150' }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <label>Pengawas 2</label>
                                <img id="prev-ttd-pengawas2"
                                    src="{{ !empty($hadir) && $hadir['ttd_peng2'] != null ? $hadir['ttd_peng2'] : 'https://via.placeholder.com/150' }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card my-2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="box-title mr-b-0">Cetak</h5>
                    </div>
                    <div class="col-md-4">
                        <div class="btn-group float-right" id="selector" role="group" aria-label="Basic example">
                            <button type="button" class="btn active btn-outline-info">By Ruang</button>
                            <button type="button" class="btn btn-outline-info">By Kelas</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6 col-md-3 mb-3 d-none" id="by-kelas">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Rombel</label>
                            <div class="col-md-8">
                                <select name="rombel" id="rombel" class="form-control">
                                    @foreach ($rombel as $rmb)
                                        <option value="{{ $rmb['id'] }}">{{ $rmb['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 mb-3" id="by-ruang">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l0">Ruang</label>
                            <div class="col-md-8">
                                <select name="ruang" id="ruang" class="form-control">
                                    @foreach ($ruang as $rg)
                                        <option value="{{ $rg['id'] }}">{{ $rg['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 mb-3">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Sesi</label>
                            <div class="col-md-9">
                                <select name="sesi" id="sesi" class="form-control">
                                    @foreach ($sesi as $ss)
                                        <option value="{{ $ss['id'] }}">{{ $ss['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 col-md-4 mb-3">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Jadwal</label>
                            <div class="col-md-9">
                                <select name="jadwal" id="jadwal" class="form-control">
                                    @foreach ($jadwal as $jdw)
                                        <option value="{{ $jdw['id'] }}">{{ $jdw['kode_bank'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 col-md-2 mb-3">
                        <button class="btn bg-success text-white" id="btn-print">
                            <i class="fa fa-print"></i><span class="ml-1">Cetak</span>
                        </button>
                    </div>
                </div>
                <hr>
                <br>
                <div class="d-flex justify-content-center bg-gray-light" style="min-height: 300mm">
                    <div id="print-preview" class="m-4">
                    </div>
                </div>
            </div>
            {{-- <div class="overlay d-none" id="loading">
                    <div class="spinner-grow"></div>
                </div> --}}
        </div>
    </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btn-group > .btn").click(function() {
                $(this).addClass("active").siblings().removeClass("active");
            });
        });

        var hadir = {!! json_encode($hadir) !!};
        var oldVal1 = hadir != null ? hadir['header1'] : '';
        var oldVal2 = hadir != null ? hadir['header2'] : '';
        var oldVal3 = hadir != null ? hadir['header3'] : '';
        var oldVal4 = hadir != null ? hadir['header4'] : '';
        var logoKanan = hadir != null ? hadir['file2'] : '';
        var logoKiri = hadir != null ? hadir['file1'] : '';
        var ttdProktor = hadir != null ? hadir['ttd_proktor'] : '';
        var proktor = hadir != null ? hadir['proktor'] : '';
        var ttdpengawas1 = hadir != null ? hadir['ttd_peng1'] : '';
        var ttdpengawas2 = hadir != null ? hadir['ttd_peng2'] : '';
        var printBy = 1;


        function submitKop() {
            $('#set-kop').submit();
        }

        function buatTanggal() {
            var hari = ['Minngu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
            var bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'];

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();

            return hari[curr_day] + ", " + curr_date + "  " + bulan[curr_month] + " " + curr_year;
        }

        function createPrintPreview(data) {
            // console.log(data);
            var bagi = 23;
            var pages = Math.ceil(data.peserta.length / bagi);
            // console.log('page', pages);
            var kelasTitle = printBy === 2 ? 'Kelas' : 'Ruang';
            var kelas = printBy === 2 ? 'Kelas/Sesi' : 'Ruang/Sesi';
            var sesi = $("#sesi option:selected").text();
            //var kelasVal = printBy === 2 ? $("#kelas option:selected").text() + '/' + sesiSelected : $("#ruang option:selected").text() + '/' + sesiSelected;
            //var sesi = data.info.sesi.nama_sesi;
            var kelasVal = printBy === 2 ? data.rombel : data.ruang + ' (' + data.kode_ruang + ')';

            var pengawas1 = '';
            var pengawas2 = '';
            for (let i = 0; i < data.pengawas.length; i++) {
                pengawas1 = data.pengawas[0].nama;
                if (i === 1) {
                    pengawas2 = data.pengawas[1].nama;
                }
            }
            var card = '';
            for (let a = 0; a < pages; a++) {
                let t = a * bagi;
                let end = (a + 1) < pages ? t + bagi : data.peserta.length;

                card += '<div class="border my-shadow mb-3 p-4 bg-white">' +
                    '<div style="-webkit-justify-content: center;justify-content: center;width: 200mm; height: 290mm;padding: 15px">';

                card += '<table style="width: 100%; border: 0;">' +
                    '<tr>' +
                    '<td style="width:15%;">' +
                    '<img src="' + logoKiri + '" style="width:85px; height:85px; margin: 6px;">' +
                    '</td>' +
                    '<td style="width:70%; text-align: center;">' +
                    '<div style="line-height: 1.1; font-family: \'Times New Roman\'; font-size: 14pt">' + oldVal1 +
                    '</div>' +
                    '<div style="line-height: 1.1; font-family: \'Times New Roman\'; font-size: 16pt"><b>' + oldVal2 +
                    '</b></div>' +
                    '<div style="line-height: 1.2; font-family: \'Times New Roman\'; font-size: 13pt">' + oldVal3 +
                    '</div>' +
                    '<div style="line-height: 1.2; font-family: \'Times New Roman\'; font-size: 12pt">' + oldVal4 +
                    '</div>' +
                    '</td>' +
                    '<td style="width:15%;">' +
                    '<img src="' + logoKanan + '" style="width:85px; height:85px; margin: 6px; border-style: none">' +
                    '</td>' +
                    '</tr>' +
                    '</table>' +
                    '<hr style="border: 1px solid; margin-bottom: 6px">' +
                    '<table style="width: 100%; border: 0;">' +
                    '<tr style="line-height: 1.1;font-family: \'Times New Roman\';">' +
                    '<td style="width:15%;">' + kelasTitle + '</td>' +
                    '<td>:</td>' +
                    '<td style="width:35%;">' + kelasVal + '</td>' +
                    '<td style="width:15%;">Hari/Tanggal</td>' +
                    '<td>:</td>' +
                    '<td>' + buatTanggal() + '</td>' +
                    '</tr>' +
                    '<tr style="line-height: 1.1;font-family: \'Times New Roman\';">' +
                    '<td style="width:15%;">Sesi</td>' + '<td>:</td>' + '<td style="width:35%;">' + sesi + '</td>' +
                    '<td>Mata Pelajaran</td>' + '<td>:</td>' + '<td>' + data.mapel + '</td>' +
                    '</tr>' +
                    '<tr style="line-height: 1.1;font-family: \'Times New Roman\';">' +
                    '<td>Waktu</td>' + '<td>:</td>' + '<td>' + data.jam_mulai + ' s/d ' + data.jam_selesai + '</td>' +
                    '</tr>' +
                    '</table>' +
                    '<br>' +
                    '<table style="width: 100%; border-collapse: collapse">' +
                    '<tr style="font-family: \'Times New Roman\';">' +
                    '<th style="border: 1px solid black; width: 40px; height: 40px; text-align: center;">No.</th>' +
                    '<th style="border: 1px solid black; text-align: center;">No Peserta</th>' +
                    '<th style="border: 1px solid black; text-align: center;">Nama</th>' +
                    '<th style="border: 1px solid black; text-align: center;">Kelas</th>' +
                    '<th colspan="2" style="border: 1px solid black; text-align: center;">Tanda Tangan</th>' +
                    '<th style="border: 1px solid black; text-align: center;width: 80px;">Ket.</th>' +
                    '</tr>';

                for (let i = t; i < end; i++) {
                    var genap = (i + 1) % 2 === 0;
                    var forGenap = genap ? (i + 1) + '.' : '';
                    var forGanjil = genap ? '' : (i + 1) + '.';
                    card += '<tr style="font-family: \'Times New Roman\';">' +
                        '<td class="ts" style="line-height: 1.1;border: 1px solid black; text-align: center">' + (i + 1) +
                        '</td>' +
                        '<td class="ts" style="line-height: 1.1;border: 1px solid black; text-align: center; padding-left: 5px">' +
                        data.peserta[i].nomor_peserta + '</td>' +
                        '<td class="ts" style="line-height: 1.1;border: 1px solid black; padding-left: 5px">' + data
                        .peserta[
                            i].nama + '</td>' +
                        '<td class="ts" style="line-height: 1.1;border: 1px solid black;text-align: center;">' + data
                        .peserta[
                            i].rombel + '</td>' +
                        '<td class="ts" style="line-height: 1.1;border-bottom: 1px solid black;width:50px;padding-left: 5px">' +
                        forGanjil + '</td>' +
                        '<td class="ts" style="line-height: 1.1;border-bottom: 1px solid black;width:100px;padding-left: 5px">' +
                        forGenap + '</td>' +
                        '<td class="ts" style="line-height: 1.1;border: 1px solid black;"> </td>' +
                        '</tr>';
                }
                card += '</table>';
                if (a === (pages - 1)) {
                    card += '</table>' +
                        '<br>' +
                        '<br>' +
                        '<table style="width: 100%">' +
                        '<tr style="line-height: 1.1;font-family: \'Times New Roman\';">' +
                        '<td style="width: 45%; text-align: left">' +
                        '<table style="border: 1px solid black; border-collapse: collapse">' +
                        '<tr>' +
                        '<td style="padding-left: 5px;">Jumlah peserta yang seharusnya hadir</td>' +
                        '<td>:</td>' +
                        '<td style="padding-right: 5px"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> orang</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border-bottom: 1px solid black;padding-left: 5px;">Jumlah peserta yang tidak hadir</td>' +
                        '<td style="border-bottom: 1px solid black;">:</td>' +
                        '<td style="border-bottom: 1px solid black; padding-right: 5px"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> orang</td>' +
                        '</tr>' +
                        '<tr >' +
                        '<td style="padding-left: 5px;">Jumlah peserta hadir</td>' +
                        '<td>:</td>' +
                        '<td style="padding-right: 5px"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> orang</td>' +
                        '</tr>' +
                        '</table>' +
                        '</td>' +
                        '<td style="width: 27%; padding-left: 10px">' +
                        'Proktor' +
                        '<br>' +
                        '<img alt="Logo kanan" id="prev-logo-ttd"' +
                        'src="' + ttdProktor + '" style="width:55px; height:48px; margin: 6px; border-style: none">' +
                        '<br>' +
                        '<u>' + proktor + '</u>' +
                        '<br>' +
                        'Nip:' +
                        '</td>' +
                        '<td style="width: 27%">' +
                        'Pengawas' +
                        '<br>' +
                        '<img alt="Logo kanan" id="prev-logo-ttd"' +
                        'src="' + ttdpengawas1 + '" style="width:55px; height:48px; margin: 6px; border-style: none">' +
                        '<br>' +
                        '<u>' + pengawas1 + '</u>' +
                        '<br>' +
                        'Nip:' +
                        '</td>' +
                        '</tr>' +
                        '</table>' +
                        '</div>' +
                        '</div>';
                }
                card += '</div></div>';
                card += '<div style="page-break-after: always"></div>';
            }


            $('#print-preview').html(card);
            // $('#loading').addClass('d-none');

            if (pages > 1) {
                $('.ts').height('28px');
                //$('.ts').css('background-color', 'black');
            } else {
                $('.ts').height('25px');
            }
        }

        $(document).ready(function() {
            var opsiJadwal = $("#jadwal");
            var opsiRuang = $("#ruang");
            var opsiSesi = $("#sesi");
            var opsiKelas = $("#rombel");

            $("#file-logo_kiri").change(function() {
                // console.log($('#form_logo_kiri'));
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kiri').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#form_logo_kiri')[0]);
                    uploadAttach("{{ route('cbt_hadir-upload_logo_kiri') }}", form);
                }
            });

            $("#file-logo_kanan").change(function() {
                console.log($('#form_logo_kanan'));
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kanan').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#form_logo_kanan')[0]);
                    uploadAttach("{{ route('cbt_hadir-upload_logo_kanan') }}", form);
                }
            });

            $("#file-proktor").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-ttd-proktor').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#form_ttd_proktor')[0]);
                    uploadAttach("{{ route('cbt_hadir-upload_ttd_proktor') }}", form);
                }
            });

            $("#file-ttd_pengawas1").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-ttd-pengawas1').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#form_ttd_pengawas1')[0]);
                    uploadAttach("{{ route('cbt_hadir-upload_ttd_pengawas1') }}", form);
                }
            });

            $("#file-ttd_pengawas2").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-ttd-pengawas2').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#form_ttd_pengawas2')[0]);
                    uploadAttach("{{ route('cbt_hadir-upload_ttd_pengawas2') }}", form);
                }
            });

            function uploadAttach(action, data) {
                // console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        if (data.src.includes('kanan')) {
                            logoKanan = data.src;
                        } else if (data.src.includes('kiri')) {
                            logoKiri = data.src;
                            //console.log('kiri', data.src);
                        } else if (data.src.includes('tanda')) {
                            tandatangan = data.src;
                            //console.log('tandatangan', data.src);
                        }
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $.toast({
                            heading: "ERROR!!",
                            text: "file tidak terbaca",
                            icon: 'error',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 5000,
                            position: 'top-right'
                        });
                    }
                });
            }

            function loadSiswaRuang(ruang, sesi, jadwal) {
                var notempty = ruang && sesi && jadwal;
                console.log('ruang', ruang);

                if (notempty) {
                    // $('#print-preview').addClass('d-none');
                    // $('#loading').removeClass('d-none');

                    setTimeout(function() {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('cbt_print-hadir_ruang') }}",
                            data: {
                                ruang,
                                sesi,
                                jadwal
                            },
                            success: function(response) {
                                if (response.status == 'berhasil') {
                                    createPrintPreview(response.hadir);
                                } else {
                                    swa("GAGAL!", 'Ada kesalahan saat memroses data', 'error');
                                }
                            }
                        });
                    }, 500);
                }
            }

            function loadSiswaKelas(kelas, sesi, jadwal) {

                var notempty = kelas && sesi && jadwal;
                if (notempty) {
                    setTimeout(function() {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('cbt_print-hadir_kelas') }}",
                            data: {
                                kelas,
                                sesi,
                                jadwal
                            },
                            success: function(response) {
                                if (response.status == 'berhasil') {
                                    createPrintPreview(response.hadir);
                                } else {
                                    swa("GAGAL!", 'Ada kesalahan saat memroses data', 'error');
                                }

                            },
                        });
                    }, 500);
                }
            }



            opsiJadwal.prepend("<option value='' selected='selected'>Pilih Jadwal</option>");
            opsiRuang.prepend("<option value='' selected='selected'>Pilih Ruang</option>");
            opsiSesi.prepend("<option value='' selected='selected'>Pilih Sesi</option>");
            opsiKelas.prepend("<option value='' selected='selected'>Pilih Kelas</option>");

            opsiKelas.change(function() {
                loadSiswaKelas($(this).val(), opsiSesi.val(), opsiJadwal.val())
            });

            opsiRuang.change(function() {
                loadSiswaRuang($(this).val(), opsiSesi.val(), opsiJadwal.val())
            });

            opsiSesi.change(function() {
                if (printBy === 1) {
                    loadSiswaRuang(opsiRuang.val(), $(this).val(), opsiJadwal.val())
                } else {
                    loadSiswaKelas(opsiKelas.val(), $(this).val(), opsiJadwal.val())
                }
            });

            opsiJadwal.change(function() {
                if (printBy === 1) {
                    loadSiswaRuang(opsiRuang.val(), opsiSesi.val(), $(this).val())
                } else {
                    loadSiswaKelas(opsiKelas.val(), opsiSesi.val(), $(this).val())
                }
            });

            $("#btn-print").click(function() {
                var kosong = printBy === 2 ? ($('#rombel').val() === '' || ($('#sesi').val() === '') || ($(
                    '#jadwal').val() === '')) : ($('#ruang').val() === '' || ($('#sesi').val() ===
                    '') || ($('#jadwal').val() === ''));
                if (kosong) {
                    Swal.fire({
                        title: "ERROR",
                        text: "Isi semua pilihan terlebih dulu",
                        icon: "error"
                    })
                } else {
                    $('#print-preview').printArea();
                }
            });


            $("#header-1").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal1) {
                    return;
                }
                oldVal1 = currentVal;
            });
            $("#header-2").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal2) {
                    return;
                }
                oldVal2 = currentVal;
            });
            $("#header-3").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal3) {
                    return;
                }
                oldVal3 = currentVal;
            });
            $("#header-4").on("change keyup paste", function() {
                var currentVal = $(this).val();
                if (currentVal === oldVal4) {
                    return;
                }
                oldVal4 = currentVal;
            });

            $('#set-kop').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                console.log($(this).serialize());
                $.ajax({
                    url: "{{ route('cbt_save-hadir') }}",
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        $('.btn-update').html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('.btn-update').html(
                                '<i class="fas fa-save mr-1"></i>Simpan');
                        }
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $('#selector button').click(function() {
                $(this).addClass('active').siblings().addClass('btn-outline-primary').removeClass(
                    'active btn-primary');

                if (!$('#by-kelas').is(':hidden')) {
                    $('#by-kelas').addClass('d-none');
                    $('#by-ruang').removeClass('d-none');
                    printBy = 1;
                } else {
                    $('#by-kelas').removeClass('d-none');
                    $('#by-ruang').addClass('d-none');
                    printBy = 2;
                }
            });

        })
    </script>
@endsection
