@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="card my-shadow">
        <div class="card-header">
            <div class="card-title">
                <h6>Cetak</h6>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-3 mr-b-20 text-center">
                    <div class="border">
                        <a href="{{ route('cbt_cetak-kartu_peserta') }}">
                            <h6 class="h5 mr-b-0 mr-t-10"><i class="fa fa-id-card fa-3x"></i></h6>
                            <small>Kartu Peserta</small>
                        </a>
                    </div>
                </div>
                <div class="col-3 mr-b-20 text-center">
                    <div class="border">
                        <a href="{{ route('cbt_cetak-daftar_hadir') }}">
                            <h6 class="h5 mr-b-0 mr-t-10"><i class="fa fa-list-ul fa-3x"></i></h6>
                            <small>Daftar Hadir</small>
                        </a>
                    </div>
                </div>
                <div class="col-3 mr-b-20 text-center">
                    <div class="border">
                        <a href="{{ route('cbt_cetak-berita_acara') }}">
                            <h6 class="h5 mr-b-0 mr-t-10"><i class="fa fa-newspaper fa-3x"></i></h6>
                            <small>Berita Acara</small>
                        </a>
                    </div>
                </div>
                <div class="col-3 mr-b-20 text-center">
                    <div class="border">
                        <a href="{{ route('cbt_cetak-peserta_ujian') }}">
                            <h6 class="h5 mr-b-0 mr-t-10"><i class="fa fa-user-graduate fa-3x"></i></h6>
                            <small>Peserta ujian</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
@endsection
