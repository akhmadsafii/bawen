@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        button.btn.active {
            color: #fff;
            background-color: #03a9f3;
            border-color: #03a9f3;
            -webkit-box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
            box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
        }

        .my-shadow {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(58 59 69 / 15%) !important;
        }

        .bg-lime {
            background-color: #01ff70 !important;
        }

    </style>
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"><b>CETAK DAFTAR PESERTA</b></h5>
        </div>
        <div class="page-title-right">
            <a class="btn btn-danger" href="{{ route('cbt_cetak') }}"><i class="fas fa-arrow-alt-circle-left"></i>
                Kembali</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card my-shadow">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title mr-b-0">Cetak</h5>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" id="btn-print"
                                class="card-tools btn bg-success mx-2 text-white pull-right">
                                <i class="fas fa-print mr-1"></i> Cetak
                            </button>
                            <div class="btn-group pull-right" id="selector" role="group" aria-label="Basic example">
                                <a type="button" href="{{ route('cbt_cetak-peserta_ujian', ['m' => 'ruang']) }}"
                                    class="btn {{ empty($_GET) || $_GET['m'] != 'rombel' ? 'active' : '' }} btn-outline-info">By
                                    Ruang</a>
                                <a type="button" href="{{ route('cbt_cetak-peserta_ujian', ['m' => 'rombel']) }}"
                                    class="btn {{ !empty($_GET) && $_GET['m'] == 'rombel' ? 'active' : '' }} btn-outline-info">By
                                    Kelas</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 col-md-4 mb-4">
                            <div class="input-group">
                                <div class="input-group-addon"> <span class="input-group-text">Jenis Ujian</span>
                                </div>
                                <select name="jenis" id="jenis" class="form-control">
                                    @foreach ($jenis as $jns)
                                        <option value="{{ $jns['id'] }}">{{ $jns['nama'] . ' (' . $jns['kode'] . ')' }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mb-4">
                            <form id="set-logo-kiri">
                                <div class="input-group">
                                    <div class="input-group-addon"> <span class="input-group-text">Logo Kiri</span>
                                    </div>
                                    <input type="file" class="form-control-file mt-2 mx-2" name="image" id="logo-kiri"
                                        accept="image/*">
                                </div>
                            </form>
                        </div>
                        <div class="col-12 col-md-4 mb-4">
                            <form id="set-logo-kanan">
                                <div class="input-group">
                                    <div class="input-group-addon"> <span class="input-group-text">Logo Kanan</span>
                                    </div>
                                    <input type="file" class="form-control-file mt-2 mx-2" name="image" id="logo-kanan"
                                        accept="image/*">
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center bg-gray-light" style="min-height: 300mm">
                        <div id="print-preview" class="m-2">
                            @foreach ($sesi_siswa as $ss)
                                @if (!empty($ss['peserta']))
                                    <div class="border my-shadow mb-3 p-4 bg-white">
                                        <div class="pt-4"
                                            style="-webkit-justify-content: center;justify-content: center;background: white;width: 210mm; height: 297mm;padding: 1mm">
                                            <div style="display: flex; justify-content: center; align-items: center;">
                                                <table style="width: 100%; border: 0;">
                                                    <tr>
                                                        <td style="width:15%;">
                                                            <img src="{{ !empty($peserta) ? $peserta['file1'] : '' }}"
                                                                id="prev-logo-kiri-print"
                                                                style="width:85px; height:85px; margin: 6px;">
                                                        </td>
                                                        <td style="width:70%; text-align: center;">
                                                            <div style="line-height: 1.1;font-size: 13pt"></div>
                                                            <div style="line-height: 1.1;font-size: 16pt"><b>DAFTAR
                                                                    PESERTA</b>
                                                            </div>
                                                            <div style="line-height: 1.1;font-size: 14pt"
                                                                class="jenis-ujian">
                                                                Jenis
                                                                Ujian
                                                            </div>
                                                            <div style="line-height: 1.1;font-size: 12pt">Tahun
                                                                Pelajaran: {{ $tahun['tahun_ajaran'] }}
                                                        </td>
                                                        <td style="width:15%;">
                                                            <img src="{{ !empty($peserta) ? $peserta['file2'] : '' }}"
                                                                id="prev-logo-kanan-print"
                                                                style="width:85px; height:85px; margin: 6px; border-style: none">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <hr class="m-0">
                                            <table>
                                                <tr>
                                                    <td style="width: 100px">
                                                        {{ !empty($_GET) && $_GET['m'] == 'rombel' ? 'Rombel' : 'Ruang' }}
                                                    </td>
                                                    <td> : </td>
                                                    <td> {{ $ss['nama'] }}</td>
                                                </tr>
                                                @foreach ($sesi as $s)
                                                    <tr>
                                                        <td>{{ $s['nama'] }}</td>
                                                        <td> : </td>
                                                        <td>
                                                            {{ date('H:i', strtotime($s['jam_mulai'])) }} s/d
                                                            {{ date('H:i', strtotime($s['jam_selesai'])) }}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                            <table
                                                style="width: 100%; border: 1px solid black;border-collapse: collapse; font-size: 10pt">
                                                <thead>
                                                    <tr>
                                                        <th
                                                            style="border: 1px solid black; width: 40px; height: 40px; text-align: center;">
                                                            NO
                                                        </th>
                                                        <th style="border: 1px solid black; text-align: center;">NO.
                                                            PESERTA
                                                        </th>
                                                        <th style="border: 1px solid black; text-align: center;">NAMA
                                                            PESERTA
                                                        </th>
                                                        <th style="border: 1px solid black; text-align: center;">KELAS
                                                        </th>
                                                        <th style="border: 1px solid black; text-align: center;">RUANG
                                                        </th>
                                                        <th style="border: 1px solid black; text-align: center;">SESI
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($ss['peserta'] as $ps)
                                                        <tr>
                                                            <td style="border: 1px solid black; text-align: center;">
                                                                {{ $no++ }}</td>
                                                            <td style="border: 1px solid black; text-align: center;">
                                                                {{ $ps['nomor_peserta'] }}
                                                            </td>
                                                            <td style="border: 1px solid black; padding: 1px 0 1px 6px">
                                                                {{ ucwords($ps['nama_siswa']) }}
                                                            </td>
                                                            <td style="border: 1px solid black; text-align: center;">
                                                                {{ $ps['nama_rombel'] }}</td>
                                                            <td style="border: 1px solid black; text-align: center;">
                                                                {{ $ps['nama_ruang'] }}
                                                            </td>
                                                            <td style="border: 1px solid black; text-align: center;">
                                                                {{ $ps['nama_sesi'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="page-break-after: always"></div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btn-group > .btn").click(function() {
                $(this).addClass("active").siblings().removeClass("active");
            });
        });

        var logoKanan = '';
        var logoKiri = '';
        var sklh = '';

        $(document).ready(function() {
            var opsiJenis = $("#jenis");

            opsiJenis.prepend("<option value='' selected='selected'>Pilih Jenis Ujian</option>");

            opsiJenis.change(function() {
                $('.jenis-ujian').text($("#jenis option:selected").text().toUpperCase());
            });

            $("#btn-print").click(function() {
                if (opsiJenis.val() === '') {
                    swa("ERROR!", "Isi semua pilihan terlebih dulu", "error");
                } else {
                    $('#print-preview').printArea();
                }
            });

            $("#logo-kanan").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kanan-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kanan')[0]);
                    uploadAttach("{{ route('cbt_save-peserta_logo_kanan') }}", form);
                }
            });
            $("#logo-kiri").change(function() {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-logo-kiri-print').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);

                    var form = new FormData($('#set-logo-kiri')[0]);
                    uploadAttach("{{ route('cbt_save-peserta_logo_kiri') }}", form);
                }
            });

            function uploadAttach(action, data) {
                // console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        // if (data.src.includes('kanan')) {
                        //     logoKanan = data.src;
                        // } else if (data.src.includes('kiri')) {
                        //     logoKiri = data.src;
                        //     //console.log('kiri', data.src);
                        // } else if (data.src.includes('tanda')) {
                        //     tandatangan = data.src;
                        //     //console.log('tandatangan', data.src);
                        // }
                    },
                    error: function(e) {
                        console.log("error", e.responseText);
                        $.toast({
                            heading: "ERROR!!",
                            text: "file tidak terbaca",
                            icon: 'error',
                            showHideTransition: 'fade',
                            allowToastClose: true,
                            hideAfter: 5000,
                            position: 'top-right'
                        });
                    }
                });
            }
        })
    </script>
@endsection
