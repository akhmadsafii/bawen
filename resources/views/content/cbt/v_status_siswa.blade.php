@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 col-md-3 mb-3" id="by-ruang">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label" for="l0">Jadwal</label>
                                <div class="col-md-8">
                                    <select name="jadwal" id="jadwal" class="form-control">
                                        @foreach ($jadwal as $jd)
                                            <option value="{{ $jd['id'] }}">{{ $jd['kode_bank'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3" id="by-kelas">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label" for="l0">Rombel</label>
                                <div class="col-md-8">
                                    <select name="rombel" id="rombel" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 d-none" id="by-ruang">
                            <div class="input-group">
                                <div class="input-group-prepend w-30">
                                    <span class="input-group-text">Ruang</span>
                                </div>
                                <select name="ruang" id="ruang" class="form-control">
                                    <option value="1">Ruang 1</option>
                                    <option value="2">Ruang 2</option>
                                    <option value="3">Ruang 3</option>
                                    <option value="7">Ruang 4</option>
                                    <option value="8">Ruang 5</option>
                                    <option value="9">Ruang PAT 1</option>
                                    <option value="10">Ruang PAT 2</option>
                                    <option value="11">Ruang PAT 3</option>
                                    <option value="12">Ruang PAT 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-3 d-none">
                            <div class="input-group">
                                <div class="input-group-prepend w-30">
                                    <span class="input-group-text">Sesi</span>
                                </div>
                                <select name="sesi" id="sesi" class="form-control">
                                    <option value="1">Sesi 1</option>
                                    <option value="2">Sesi 2</option>
                                    <option value="3">Sesi 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row d-none" id="info">
                        <div class="col-md-4">
                            <div class="alert alert-success border-success">
                                <h6><i class="icon fas fa-check"></i> Info Ujian</h6>
                                <div id="info-ujian"></div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="alert alert-info border-info">
                                <div id="info-penggunaan">
                                    <ul>
                                        <li>
                                            Gunakan tombol <span class="badge badge-success pt-1 pb-1"><i
                                                    class="fa fa-sync ml-1 mr-1"></i> Refresh</span>
                                            untuk merefresh halaman
                                        </li>
                                        <li>
                                            Aksi <b>RESET</b> untuk mengizinkkan siswa mengerjakan ujian di beberapa
                                            komputer.
                                        </li>
                                        <li>
                                            Aksi <b>SELESAIKAN</b> untuk memaksa siswa menyelesaikan ujian.
                                        </li>
                                        <li>
                                            Aksi <b>ULANGI</b> untuk mengulang ujian siswa dari awal.
                                        </li>
                                        <li>
                                            <span class="badge badge-success"><i class="fa fa-check ml-1 mr-1"></i> Terapkan
                                                Aksi</span>
                                            untuk menerapkan aksi terpilih ke setiap siswa yang dipilih
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="button" id="btnRefresh" class="btn btn-success align-bottom mb-2"
                                onclick="refreshStatus()" data-toggle="tooltip" title="Refresh">
                                <i class="fa fa-sync ml-1 mr-1"></i> Refresh
                            </button>
                            <button type="button" class="btn btn-success align-bottom mb-2 float-right"
                                onclick="terapkanAksi()" data-toggle="tooltip" title="Terapkan Aksi pada siswa terpilih">
                                <i class="fa fa-check ml-1 mr-1"></i> Terapkan Aksi
                            </button>
                        </div>
                    </div>
                    <div>
                        <table class="table table-bordered table-sm" id="table-status">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <form id="reset">
        <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-xs" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="resetLabel">Reset Waktu Siswa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="icheck-success">
                                <input class="radio" type="radio" name="reset" id="reset1" value="1">
                                <label for="reset1">Reset Waktu dari awal</label>
                            </div>
                            <div class="icheck-success">
                                <input class="radio" type="radio" name="reset" id="reset2" value="2">
                                <label for="reset2">Lanjutkan sisa waktu</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">OK</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">BATAL</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js"></script> --}}
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        var printBy = 1;
        var url = '';

        var kelas;
        var jadwal;

        function terapkanAksi() {
            const $rows = $('#table-status').find('tr'),
                headers = $rows.splice(0, 2);
            let item = {};
            item["reset"] = [];
            //item ["id_logs"] = [];
            item["force"] = [];
            item["log"] = [];
            item["ulang"] = [];
            item["hapus"] = [];
            $rows.each((i, row) => {
                var siswa_id = $(row).attr("data-id");
                const $colReset = $(row).find('.input-reset');
                const $colForce = $(row).find('.input-force');
                const $colUlang = $(row).find('.input-ulang');
                if ($colReset.prop("checked") === true) {
                    item["reset"].push(siswa_id + '###' + jadwal);
                }
                if ($colForce.prop("checked") === true) {
                    item["force"].push(siswa_id + '###' + jadwal);
                    item["log"].push(siswa_id);
                }
                if ($colUlang.prop("checked") === true) {
                    item["ulang"].push(siswa_id + '###' + jadwal);
                    item["hapus"].push(siswa_id + '###' + jadwal);
                }
            });

            var dataSiswa = $('#reset').serialize() + '&jadwal=' + jadwal + "&aksi=" + JSON.stringify(item);
            var jmlReset = item.reset.length === 0 ? '' : '<b>' + item.reset.length + '</b> siswa akan direset<br>';
            var jmlForce = item.force.length === 0 ? '' : '<b>' + item.force.length + '</b> siswa akan dipaksa selesai<br>';
            var jmlUlang = item.ulang.length === 0 ? '' : '<b>' + item.ulang.length + '</b> siswa akan mengulang ujian';

            if (item.reset.length === 0 && item.force.length === 0 && item.ulang.length === 0) {
                noti("error", "Silahkan pilih Aksi");
                return;
            }
            swal({
                title: "Apa kamu yakin?",
                html: jmlReset + jmlForce + jmlUlang,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Apply"
            }).then(function() {
                $.ajax({
                    url: "{{ route('cbt_status_siswa-save_siswa') }}",
                    type: 'POST',
                    data: dataSiswa,
                    success: function(data) {
                        // console.log(data);
                        url = "status_siswa/filter_data?kelas=" + kelas + '&jadwal=' +
                            jadwal;
                        refreshStatus();
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })

        }

        function refreshStatus() {
            setTimeout(function() {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function() {
                        $("#btnRefresh").html(
                            '<i class="fa fa-spin fa-sync-alt"></i> refreshing..');
                    },
                    success: function(response) {
                        // console.log(response);
                        $("#btnRefresh").html(
                            '<i class="fa fa-sync ml-1 mr-1"></i> Refresh');
                        createPreview(response)
                    }
                });
            }, 500);
        }

        function createPreview(data) {
            var tbody = '<thead>' +
                '<tr>' +
                '<th rowspan="2" class="text-center align-middle" width="40">No.</th>' +
                '<th rowspan="2" class="text-center align-middle" width="100">No. Peserta</th>' +
                '<th rowspan="2" class="text-center align-middle">Nama</th>' +
                '<th rowspan="2" class="text-center align-middle">Ruang</th>' +
                '<th rowspan="2" class="text-center align-middle">Sesi</th>' +
                '<th colspan="2" class="text-center align-middle">Status</th>' +
                '<th colspan="4" class="text-center align-middle">Aksi</th>' +
                '</tr>' +
                '<tr>' +
                '<th class="text-center align-middle p-1">Mulai</th>' +
                '<th class="text-center align-middle">Durasi</th>' +
                '<th class="text-center align-middle">Reset</th>' +
                '<th class="text-center align-middle">Selesaikan</th>' +
                '<th class="text-center align-middle">Ulangi</th>' +
                '</tr></thead><tbody>';

            for (let i = 0; i < data.siswa.length; i++) {
                var idSiswa = data.siswa[i].id_kelas_siswa;
                var durasi = data.siswa[i].durasi != null ? data.siswa[i].durasi : ' - -';

                // var logging = data.durasi[idSiswa].log;
                var mulai = !data.siswa[i].mulai ? '- -  :  - -' : new Date(data.siswa[i].mulai).getHours() + ":" +
                    new Date(data.siswa[i].mulai).getMinutes() +
                    ":" + new Date(data.siswa[i].mulai).getSeconds();
                // var mulai = data.siswa[i].mulai.getHours() + ":" + data.siswa[i].mulai.getMinutes();
                var selesai = '- -  :  - -';
                var reset = data.siswa[i].reset;
                // for (let k = 0; k < logging.length; k++) {
                //     if (logging[k].log_type === '1') {
                //         if (logging[k] != null) {
                //             reset = logging[k].reset;
                //             var t = logging[k].log_time.split(/[- :]/);
                //             mulai = t[3] + ':' + t[4];
                //         }
                //     } else {
                //         if (logging[k] != null) {
                //             var ti = logging[k].log_time.split(/[- :]/);
                //             selesai = ti[3] + ':' + ti[4];
                //         }
                //     }
                // }

                //var reset = data.durasi[idSiswa].dur != null ? data.durasi[idSiswa].dur.reset : '0';
                var belumUjian = data.siswa[i].durasi == null;
                var sudahSelesai = !belumUjian && data.siswa[i].selesai != null;
                var loading = belumUjian ? '' : (sudahSelesai ? "" : '<i class="fa fa-spinner fa-spin mr-2"></i>');
                // console.log(sudahSelesai + " , " + reset);
                var disabledReset = !sudahSelesai && reset != null && reset == '0' ? '' : 'disabled';
                // console.log(disabledReset);
                var disabledSelesai = !sudahSelesai && !belumUjian ? '' : 'disabled';
                var disabledUlang = belumUjian ? 'disabled' : (sudahSelesai ? '' : 'disabled');

                var sesi = data.siswa[i].kode_sesi;
                var ruang = data.siswa[i].kode_ruang;

                tbody += '<tr data-id="' + idSiswa + '">' +
                    '<td class="text-center align-middle">' + (i + 1) + '</td>' +
                    '<td class="text-center align-middle">' + data.siswa[i].nomor_peserta + '</td>' +
                    '<td class="align-middle">' + data.siswa[i].nama_siswa + '</td>' +
                    '<td class="text-center align-middle">' + ruang + '</td>' +
                    '<td class="text-center align-middle">' + sesi + '</td>' +
                    '<td class="text-center align-middle">' + mulai + '</td>' +
                    '<td class="text-center align-middle">' + loading + durasi + '</td>' +
                    '<td class="text-center text-success align-middle">' +
                    '<input class="check input-reset" type="checkbox" ' + disabledReset + '>' +
                    '</td>' +
                    '<td class="text-center text-danger align-middle">' +
                    '<input class="check input-force" type="checkbox" ' + disabledSelesai + '>' +
                    '</td>' +
                    '<td class="text-center text-danger align-middle">' +
                    '<input class="check input-ulang" type="checkbox" ' + disabledUlang + '>' +
                    '</td>' +
                    '</tr>';
            }

            tbody += '</tbody>';
            $('#table-status').html(tbody);
            $('#info').removeClass('d-none');
            var gurus = [];
            $.each(data.detail.gurus, function(idx2, val2) {
                var str = val2.nama;
                gurus.push(str);
            })

            var infoJadwal = '<div class="row">' +
                '<div class="col-4">Mapel</div>' +
                '<div class="col-8">' +
                '<b>' + data.detail.mapel + '</b>' +
                '</div>' +
                '<div class="col-4">Guru</div>' +
                '<div class="col-8">' +
                '<b>' + gurus.join(", ") + '</b>' +
                '</div>' +
                '<div class="col-4">Jml. Soal</div>' +
                '<div class="col-8">' +
                '<b>' + data.detail.jumlah_soal + '</b>' +
                '</div>' +
                '</div>';

            $('#info-ujian').html(infoJadwal);
            $('#loading').addClass('d-none');
        }

        function getDetailJadwal(idJadwal) {
            $.ajax({
                type: "POST",
                url: "{{ route('cbt_status_siswa-by_rombel') }}",
                data: {
                    id_jadwal: idJadwal
                },
                // cache: false,
                success: function(response) {
                    // console.log(response);
                    var selKelas = $('#rombel');
                    selKelas.html('');
                    selKelas.append('<option value="">Pilih Rombel</option>');
                    $.each(response, function(k, v) {
                        // console.log(k, v);
                        if (v != null) {
                            selKelas.append('<option value="' + v.id + '">' + v.rombel + '</option>');
                        }
                    });
                }
            });
        }

        $(document).ready(function() {

            var opsiHari = $("#hari");
            var opsiJadwal = $("#jadwal");
            var opsiSesi = $("#sesi");
            var opsiKelas = $("#rombel");

            opsiJadwal.prepend("<option value='' selected='selected'>Pilih Jadwal</option>");
            opsiSesi.prepend("<option value='' selected='selected'>Pilih Sesi</option>");
            opsiKelas.prepend("<option value='' selected='selected'>Pilih Kelas</option>");

            function loadSiswaKelas(kelas, jadwal) {
                var empty = kelas === '' || jadwal === '';
                if (!empty) {
                    url = "status_siswa/filter_data?kelas=" + kelas + '&jadwal=' +
                        jadwal;
                    refreshStatus();
                } else {
                    console.log('empty')
                }
            }

            opsiKelas.change(function() {
                kelas = $(this).val();
                jadwal = opsiJadwal.val();
                loadSiswaKelas(kelas, jadwal)
            });
            opsiJadwal.change(function() {
                getDetailJadwal($(this).val());
                /*
                			if (printBy === 1) {
                				loadSiswaKelas(opsiKelas.val(), opsiSesi.val(), $(this).val())
                			} else {
                				loadSiswaRuang(opsiRuang.val(), opsiSesi.val(), $(this).val())
                			}*/
            });

            $('#selector button').click(function() {
                $(this).addClass('active').siblings().addClass('btn-outline-primary').removeClass(
                    'active btn-primary');

                if (!$('#by-kelas').is(':hidden')) {
                    $('#by-kelas').addClass('d-none');
                    $('#by-ruang').removeClass('d-none');
                    printBy = 2;
                } else {
                    $('#by-kelas').removeClass('d-none');
                    $('#by-ruang').addClass('d-none');
                    printBy = 1;
                }
            });

            var idSiswa = '';
            var idJadwal = '';
            $('#resetModal').on('show.bs.modal', function(e) {
                idSiswa = $(e.relatedTarget).data('siswa');
                idJadwal = $(e.relatedTarget).data('jadwal');

                console.log('siswa:' + idSiswa, 'jadwal:' + idJadwal);
                //$(e.currentTarget).find('input[id="namaEdit"]').val(nama);
                //$(e.currentTarget).find('input[id="kodeEdit"]').val(kode);
            });

            $('#reset').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                $('#resetModal').modal('hide').data('bs.modal', null);
                $('#resetModal').on('hidden', function() {
                    $(this).data('modal', null);
                });

                $.ajax({
                    url: base_url + "siswa/resettimer",
                    type: 'POST',
                    data: $(this).serialize() + '&id_durasi=' + idSiswa + '' + idJadwal,
                    success: function(data) {
                        console.log(data.status);
                    },
                    error: function(xhr, status, error) {
                        console.log('error');
                    }
                });

            });
        })
    </script>
@endsection
