@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h5 class="box-title mr-b-0">Hasil Ujian</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3" id="by-kelas">
                            <div class="input-group my-1">
                                <div class="input-group-addon"> <span class="input-group-text">Rombel</span>
                                </div>
                                <select name="rombel" id="rombel" class="form-control">
                                    @foreach ($jurusan as $jr)
                                        <optgroup label="{{ $jr['nama'] }}">
                                            @foreach ($jr['kelas'] as $kelas)
                                        <optgroup label="{{ $kelas['nama_romawi'] }}">
                                            @foreach ($kelas['rombel'] as $rombel)
                                                <option value="{{ $rombel['id'] }}">
                                                    {{ $rombel['nama'] }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="input-group my-1">
                                <div class="input-group-addon"> <span class="input-group-text">Jadwal</span>
                                </div>
                                <select name="jadwal" id="jadwal" class="form-control">
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="float-right class=" d-none"" id="group-export">
                        {{-- <button type="button" class="btn btn-danger align-text-bottom" data-toggle="modal"
                            data-target="#perbaikanModal">
                            <i class="fa fa-balance-scale-right ml-1 mr-1"></i> Katrol Nilai
                        </button> --}}
                        <button type="button" id="download-excel" class="btn btn-success align-text-bottom"
                            data-toggle="tooltip" title="Download Excel">
                            <i class="fa fa-file-excel ml-1 mr-1"></i> Ekspor ke Excel
                        </button>
                    </div>
                    {{-- <div class="" id="info">
                        <div id="info-ujian"></div>
                    </div> --}}
                    @if ($hasil != 0)
                        <div class="table-responsive">
                            <table class="table table-sm" id="table-status"
                                data-cols-width="5,15,35,15,10,10,10,4,10,10,10">
                                <tr>
                                    <td colspan="2">Mata Pelajaran</td>
                                    <td colspan="5">{{ $nilai_jadwal != 0 ? ucwords($jadwal['mapel']) : '' }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Kelas</td>
                                    <td colspan="5">{{ $nilai_jadwal != 0 ? $jadwal['rombel'] : '' }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Guru</td>
                                    @php
                                        $guru = '';
                                        if ($nilai_jadwal != 0) {
                                            foreach ($jadwal['guru_bank'] as $gr) {
                                                $guru = implode(',', (array) $gr['nama']);
                                            }
                                        }
                                    @endphp
                                    <td colspan="5">{{ $guru }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width: 120px">Soal</td>
                                    <td colspan="5">{{ $nilai_jadwal != 0 ? $jadwal['kode_bank'] : '' }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="60" class="align-top">Jumlah Soal</td>
                                    <td colspan="5" class="align-top">
                                        {{ $nilai_jadwal != 0 ? $jadwal['jumlah_soal'] : '' }}</td>
                                </tr>
                                <tr></tr>
                                <tr class="bg-blue">
                                    <th rowspan="2" class="text-center text-white align-middle" width="40"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        No.
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white" width="100"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        No. Peserta
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Nama
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true">Ruang
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true">Sesi
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true">Mulai
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true">Durasi
                                    </th>
                                    <th colspan="1" class="text-center align-middle text-white d-none"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Nomor Soal PG
                                    </th>
                                    <th colspan="2" class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        PG
                                    </th>

                                    <th colspan="4" rowspan="1" class="text-center align-middle text-white"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Skor
                                    </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Nilai
                                    </th>
                                    <th rowspan="2" class="text-center align-middle text-white" data-exclude="true"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Aksi
                                    </th>
                                </tr>
                                <tr class="bg-blue">
                                    <th class="text-center align-middle text-white p-1 d-none" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        1 </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        B
                                    </th>
                                    <th class="text-center align-middle text-white"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;"
                                        data-fill-color="b8daff" data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                        data-f-bold="true">S
                                    </th>
                                    <th class="text-center align-middle text-white p-1" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        PG
                                    </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        PK
                                    </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        IS
                                    </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        ES
                                    </th>
                                    <th class="text-center align-middle text-white" data-fill-color="b8daff"
                                        data-a-v="middle" data-a-h="center" data-b-a-s="thin" data-f-bold="true"
                                        style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                        Asli
                                    </th>
                                </tr>
                                @if (!empty($hasil))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($hasil as $hs)
                                        <tr>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $no++ }}
                                            </td>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['nomor_peserta'] }}
                                            </td>
                                            <td class="align-middle" data-a-v="middle" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse;">
                                                {{ strtoupper($hs['nama_siswa']) }} </td>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['kode_ruang'] }}
                                            </td>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['kode_sesi'] }}
                                            </td>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['mulai'] != null ? (new \App\Helpers\Help())->getTime($hs['mulai']) : '- - : - -' }}
                                            </td>
                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['durasi'] }}
                                            </td>
                                            <td class="d-none" data-fill-color="FF7043" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                            </td>
                                            <td data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['pilgan_benar'] }}
                                            </td>
                                            <td data-a-v="middle" data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{ $hs['pilgan_benar'] != null ? $hs['jml_pilgan'] - $hs['pilgan_benar'] : '' }}
                                            </td>
                                            <td class="text-center text-success align-middle" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b>{{ $hs['pg_nilai'] }}</b>
                                            </td>
                                            <td class="text-center text-success align-middle" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b>{{ $hs['kompleks_nilai'] }}</b>
                                            </td>
                                            <td class="text-center text-success align-middle" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b>{{ $hs['isian_nilai'] }}</b>
                                            </td>
                                            <td class="text-center text-success align-middle" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b>{{ $hs['essai_nilai'] }}</b>
                                            </td>

                                            <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b>{{ $hs['mulai'] != null? round($hs['pg_nilai'] + $hs['kompleks_nilai'] + $hs['isian_nilai'] + $hs['essai_nilai'], 1): '' }}</b>
                                            </td>
                                            {{-- <td class="text-center align-middle" data-a-v="middle" data-a-h="center"
                                                data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                <b> </b>
                                            </td> --}}
                                            <td class="text-center align-middle" data-exclude="true" data-a-v="middle"
                                                data-a-h="center" data-b-a-s="thin"
                                                style="border: 1px solid black;border-collapse: collapse; text-align: center;">
                                                {{-- {{ $hs['pg_nilai'] }} --}}
                                                <button type="button"
                                                    class="btn btn-xs bg-success mb-1 {{ $hs['status_koreksi'] == 0 ? 'disabled' : '' }}"
                                                    onclick="lihatJawaban({{ $hs['id_kelas_siswa'] }}, {{ $hs['id_jadwal'] }})"
                                                    data-toggle="tooltip" title="Detail Jawaban Siswa">Koreksi
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                @endif

                            </table>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <form action="http://garuda.mysch.web.id/update" id="perbaikan-nilai" method="post" accept-charset="utf-8">
        <input type="hidden" name="csrf_token" value="498b080ac227924b922eef7d5b178a22" />
        <div class="modal fade" id="perbaikanModal" tabindex="-1" role="dialog" aria-labelledby="perbaikanModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="perbaikanModalLabel">Perbaikan Nilai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="nama_sesi" class="col-md-4 col-form-label">Nilai Tertinggi</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="ya" value="100"
                                    placeholder="Nilai tertinggi yang diinginkan" required>
                                <small>diisi nilai puluhan maksimal 100, misal 80 sampai 100</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kode_sesi" class="col-md-4 col-form-label">Nilai Terrendah</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="yb" value="60"
                                    placeholder="Nilai terrendah yang diinginkan" required>
                                <small>diisi nilai puluhan dibawah nilai tertinggi, misal 60</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="convert">Katrol <i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="https://cdn.jsdelivr.net/npm/@linways/table-to-excel@1.0.4/dist/tableToExcel.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var idJadwal = '';
        var isSelected = 0;
        var namaMapel = '';
        var jenisUjian = '';

        var nilai_max = 0; //nilai siswa terbesar
        var nilai_min = 20; //nilai siswa terkecil
        var hasil_max = 100; //batas nilai terbesar
        var hasil_min = 60; //batas nilai terkecil

        function lihatJawaban(idSiswa, id_jadwal) {
            window.location.href = 'hasil_ujian/nilai?siswa=' + idSiswa + '&jadwal=' + id_jadwal;
        }

        function getDetailJadwal(idJadwal) {
            $.ajax({
                type: "GET",
                url: base_url + "cbtstatus/getjadwalujianbyjadwal?id_jadwal=" + idJadwal,
                cache: false,
                success: function(response) {
                    console.log(response);
                    var selKelas = $('#kelas');
                    selKelas.html('');
                    selKelas.append('<option value="">Pilih Kelas</option>');
                    $.each(response, function(k, v) {
                        if (v != null) {
                            selKelas.append('<option value="' + k + '">' + v + '</option>');
                        }
                    });
                }
            });
        }

        function getKelasJadwal(id) {
            $.ajax({
                type: "POST",
                url: "{{ route('cbt-get_jadwal_by_rombel') }}",
                data: {
                    id
                },
                success: function(response) {
                    console.log(response);

                    var selJadwal = $('#jadwal');
                    selJadwal.html('');
                    selJadwal.append('<option value="">Pilih Jadwal</option>');
                    $.each(response, function(k, v) {
                        if (v != null) {
                            selJadwal.append('<option value="' + v.id + '">' + v.kode_bank +
                                '</option>');
                        }
                    });

                }
            });
        }



        $(document).ready(function() {
            var opsiJadwal = $("#jadwal");
            var opsiKelas = $("#rombel");

            var selected = isSelected === 0 ? "selected='selected'" : "";
            opsiJadwal.prepend("<option value='' " + selected + " disabled>Pilih Jadwal</option>");
            opsiKelas.prepend("<option value='' " + selected + " disabled>Pilih Rombel</option>");

            function loadSiswaKelas(kelas, jadwal) {
                var empty = jadwal === null;
                //console.log(jadwal, kelas)
                if (!empty) {
                    // $('#loading').removeClass('d-none');
                    window.location.href = 'hasil_ujian?kelas=' + kelas + '&jadwal=' + jadwal;
                } else {
                    console.log('empty')
                }
            }

            $('#rollback').on('click', function(e) {
                loadSiswaKelas(opsiKelas.val(), opsiJadwal.val())
            });

            opsiKelas.change(function() {
                getKelasJadwal($(this).val());
            });

            opsiJadwal.change(function() {
                idJadwal = $(this).val();
                loadSiswaKelas(opsiKelas.val(), $(this).val())
            });

            $("#download-excel").click(function(event) {
                var table = document.querySelector("#table-status");
                if (table != null) {
                    //TableToExcel.convert(table);
                    TableToExcel.convert(table, {
                        name: `Nilai ${jenisUjian} ${$("#kelas option:selected").text()} ${namaMapel}.xlsx`,
                        sheet: {
                            name: "Sheet 1"
                        }
                    });
                } else {
                    Swal.fire({
                        title: "ERROR",
                        text: "Isi JADWAL dan KELAS terlebih dulu",
                        icon: "error"
                    })
                }
            });

            $('#loading').addClass('d-none');

            $('#perbaikan-nilai').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                var table = document.querySelector("#table-status");
                if (table != null) {
                    var $inputs = $('#perbaikan-nilai :input');
                    var values = {};
                    $inputs.each(function() {
                        values[this.name] = $(this).val();
                    });
                    hasil_max = values['ya'];
                    hasil_min = values['yb'];
                    //console.log(hasil_max, hasil_min);
                    //console.log(nilai_max, nilai_min);

                    $('#perbaikanModal').modal('hide').data('bs.modal', null);
                    $('#perbaikanModal').on('hidden', function() {
                        $(this).data('modal', null); // destroys modal
                    });
                    $('#loading').removeClass('d-none');
                    window.location.href = base_url + 'cbtnilai?kelas=' + opsiKelas.val() + '&jadwal=' +
                        opsiJadwal.val() +
                        '&xa=' + nilai_max + '&xb=' + nilai_min + '&ya=' + hasil_max + '&yb=' + hasil_min;
                } else {
                    $('#perbaikanModal').modal('hide').data('bs.modal', null);
                    $('#perbaikanModal').on('hidden', function() {
                        $(this).data('modal', null); // destroys modal
                    });


                    Swal.fire({
                        title: "ERROR",
                        text: "Isi JADWAL dan KELAS terlebih dulu",
                        icon: "error"
                    })
                }
            });
        })
    </script>
@endsection
