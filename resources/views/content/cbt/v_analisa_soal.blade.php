@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default my-shadow mb-4">
                <div class="card-header">
                    <h5 class="box-title mr-b-0">Analisa Soal Ujian</h5>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-5">
                            <div class="input-group">
                                <div class="input-group-addon"> <span class="input-group-text">Tahun Ajaran</span>
                                </div>
                                <select name="tahun" id="tahun" class="form-control">
                                    @foreach ($tahun as $th)
                                        <option value="{{ $th['id'] }}">
                                            {{ $th['tahun_ajaran'] . ' (' . $th['semester'] . ')' }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="input-group">
                                <div class="input-group-addon"> <span class="input-group-text">Jadwal</span>
                                </div>
                                <select name="jadwal" id="jadwal" class="form-control">
                                    @foreach ($jadwal as $jd)
                                        <option value="{{ $jd['id'] }}"> {{ $jd['kode_bank'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-3">
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="overlay d-none" id="loading">
                    <div class="spinner-grow"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var idJadwal = '';
        var isSelected = 0;

        function getDetailJadwal(idJadwal) {
            $('#loading').removeClass('d-none');
            $.ajax({
                type: "GET",
                url: base_url + "cbtstatus/getjadwalujianbyjadwal?id_jadwal=" + idJadwal,
                cache: false,
                success: function(response) {
                    $('#loading').addClass('d-none');
                    console.log(response);
                    var selKelas = $('#kelas');
                    selKelas.html('');
                    selKelas.append('<option value="">Pilih Kelas</option>');
                    $.each(response, function(k, v) {
                        if (v != null) {
                            selKelas.append('<option value="' + k + '">' + v + '</option>');
                        }
                    });
                }
            });
        }

        $(document).ready(function() {
            var opsiJadwal = $("#jadwal");
            var opsiThn = $("#tahun");

            var selected = isSelected === 0 ? "selected='selected'" : "";
            opsiJadwal.prepend("<option value='' " + selected + ">Pilih Jadwal</option>");

            //opsiKelas.prepend("<option value='' "+selected+">Pilih Kelas</option>");

            function loadSoal(jadwal, thn, smt) {
                var empty = jadwal === '';
                if (!empty) {
                    $('#loading').removeClass('d-none');
                    window.location.href = base_url + 'cbtanalisis?jadwal=' + jadwal + '&thn=' + thn + '&smt=' +
                        smt;
                } else {
                    console.log('empty')
                }
            }

            function loadJadwalTahun(thn, smt) {
                $('#loading').removeClass('d-none');
                window.location.href = base_url + "cbtanalisis?&thn=" + thn + "&smt=" + smt;
            }

            opsiSmt.change(function() {
                loadJadwalTahun(opsiThn.val(), $(this).val());
            });

            opsiThn.change(function() {
                loadJadwalTahun($(this).val(), opsiSmt.val());
            });

            opsiJadwal.change(function() {
                console.log($(this).val());
                loadSoal($(this).val(), opsiThn.val(), opsiSmt.val())
                //getDetailJadwal($(this).val(), opsiThn.val(), opsiSmt.val());
            });

            $('#kalkulasi').click(function() {
                console.log('test', base_url + "cbtanalisis/kalkulasi?jadwal=" + opsiJadwal.val());
                $.ajax({
                    url: base_url + "cbtanalisis/kalkulasi?jadwal=" + opsiJadwal.val(),
                    type: "GET",
                    success: function(data) {
                        window.location.reload();
                    },
                    error: function(xhr, status, error) {
                        console.log("error", xhr.responseText);
                    }
                });
            });
        });
    </script>
@endsection
