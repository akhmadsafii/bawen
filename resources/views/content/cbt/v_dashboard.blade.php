@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row m-0">
                <div class="col-md-2 bg-teal text-white p-2">
                    <center>
                        <i class="fas fa-user-shield fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Admin CBT</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['user']['Admin Cbt'] }}</h4>
                        @if (session('role') != 'guru')
                            <a href="{{ route('cbt-admin') }}" class="my-2 btn btn-info btn-xs">
                                Detail <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        @endif
                    </center>
                </div>
                <div class="col-md-2 bg-facebook text-white p-2">
                    <center>
                        <i class="fas fa-users fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Siswa</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['user']['Siswa'] }}</h4>
                        @if (session('role') != 'guru')
                            <a href="{{ route('user-siswa') }}" class="my-2 btn btn-info btn-xs">
                                Detail <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        @endif
                    </center>
                </div>
                <div class="col-md-2 bg-cyan text-white p-2">
                    <center>
                        <i class="fas fa-user-tie fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Guru</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['user']['Guru'] }}</h4>
                        @if (session('role') != 'guru')
                            <a href="{{ route('user-guru') }}" class="my-2 btn btn-info btn-xs">
                                Detail <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        @endif
                    </center>
                </div>
                <div class="col-md-2 bg-purple text-white p-2">
                    <center>
                        <i class="fas fa-book fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Mapel</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['master']['Mata pelajaran'] }}</h4>
                        @if (session('role') != 'guru')
                            <a href="{{ route('cbt-mapel') }}" class="my-2 btn btn-info btn-xs">
                                Detail <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        @endif
                    </center>
                </div>
                <div class="col-md-2 bg-success text-white p-2">
                    <center>
                        <i class="fas fa-chalkboard-teacher fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Rombel</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['master']['Rombongan Belajar'] }}</h4>
                    </center>
                </div>
                <div class="col-md-2 bg-yellow text-white p-2">
                    <center>
                        <i class="fas fa-tools fa-3x"></i>
                        <p class="mr-tb-10 opacity-05"><strong>Jurusan</strong></p>
                        <h4 class="my-0 opacity-08 text-white">{{ $dashboard['master']['Jurusan'] }}</h4>
                    </center>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="card-title mb-0">
                                        JADWAL HARI INI
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{ route('cbt-jadwal') }}" type="button" onclick=""
                                        class="btn btn-xs btn-success float-right my-auto">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="card border-0 shadow-none m-0">
                                <div class="card-body text-center">
                                    <div class="row">
                                        @foreach ($jadwal as $jd)
                                            @if (!empty($jd['jadwal']))
                                                @foreach ($jd['jadwal'] as $jdw)
                                                    <div class="col-md-6" style="min-height: 60px">
                                                        <a
                                                            href="{{ route('cbt-jadwal_edit', ['k' => (new \App\Helpers\Help())->encode($jdw['id'])]) }}">
                                                            <div class="info-box border p-1 bg-danger"
                                                                style="min-height: 60px;">
                                                                <div class="info-box-content p-1 text-danger">
                                                                    <span
                                                                        class="info-box-text text-white">{{ $jdw['kd_mapel'] != null ? $jdw['kd_mapel'] : '-' }}</span>
                                                                    <h5 class="info-box-number m-0 text-white">
                                                                        {{ $jdw['durasi_ujian'] }}'</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-danger my-shadow">
                        <div class="card-header bg-danger">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-title mb-0">
                                        PENILAIAN
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @if (session('role') != 'guru')
                                    <div class="col-md-3 col-4" style="min-height: 60px">
                                        <a href="{{ route('cbt-ruang') }}">
                                            <div class="info-box border p-1"
                                                style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                                <div class="info-box-content p-1 text-danger bg-info">
                                                    <span class="info-box-text text-white">Ruang Ujian</span>
                                                    <h5 class="info-box-number m-0 text-white">{{ $dash_cbt['ruang'] }}
                                                    </h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-4" style="min-height: 60px">
                                        <a href="{{ route('cbt-sesi') }}">
                                            <div class="info-box border p-1"
                                                style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                                <div class="info-box-content p-1 text-danger bg-success">
                                                    <span class="info-box-text text-white">Sesi</span>
                                                    <h5 class="info-box-number m-0 text-white">{{ $dash_cbt['sesi'] }}
                                                    </h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                                <div class="col-md-3 col-4" style="min-height: 60px">
                                    <a href="{{ route('cbt-bank_soal') }}">
                                        <div class="info-box border p-1"
                                            style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                            <div class="info-box-content p-1 text-danger bg-purple">
                                                <span class="info-box-text text-white">Bank Soal</span>
                                                <h5 class="info-box-number m-0 text-white">{{ $dash_cbt['bank'] }}</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-4" style="min-height: 60px">
                                    <a href="{{ route('cbt-jadwal') }}">
                                        <div class="info-box border p-1"
                                            style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                            <div class="info-box-content p-1 text-danger bg-facebook">
                                                <span class="info-box-text text-white">Jadwal</span>
                                                <h5 class="info-box-number m-0 text-white">{{ $dash_cbt['jadwal'] }}</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
