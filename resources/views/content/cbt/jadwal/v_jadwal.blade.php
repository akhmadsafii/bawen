@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-7 col-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-5 col-6">
                                    <div class="float-right">
                                        <button onClick="reloadL()" class="btn btn-danger btn-reload">
                                            <i class="fa fa-sync"></i>
                                            <span class="d-none d-sm-inline-block ml-1"> Reload</span>
                                        </button>
                                        <a href="{{ route('cbt-jadwal_create') }}" type="button"
                                            class="btn btn-info ml-1">
                                            <i class="fas fa-plus-circle"></i> <span class="d-none d-sm-inline-block ml-1">
                                                Tambah</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            Kode Background Jadwal:
                            <table class="w-100">
                                <tbody>
                                    <tr>
                                        <td class="p-1" style="width: 20px"><i
                                                class="fas fa-square text-danger"></i></td>
                                        <td class="p-1">Tidak aktif</td>
                                        <td class="p-1" style="width: 20px"><i
                                                class="fas fa-square text-success"></i></td>
                                        <td class="p-1">Sedang dilaksanakan</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1"><i class="fas fa-square text-secondary"></i></td>
                                        <td class="p-1">Belum dimulai</td>
                                        <td class="p-1"><i class="fas fa-square text-purple"></i></td>
                                        <td class="p-1">Selesai</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="alert alert-danger my-2" id="bulk-delete">
                        <input style="width: 28px; height: 28px" class="check-all m-1" id="checkAll" type="checkbox">
                        <label for="check-all" class="align-middle">Pilih Semua</label>
                        <button id="submit-hapus" type="button" class="btn btn-danger ml-4" style="display: none">
                            <i class="far fa-trash-alt"></i> Hapus Jadwal Terpilih
                        </button>
                    </div>

                    <div id="konten-jadwal">
                        @foreach ($jenis as $jns)
                            @if (!empty($jns['jadwal']))
                                <div class="card card-default my-shadow mb-4">
                                    <div class="card-header bg-info">
                                        <h5 class="box-title m-0 text-white"><b>{{ $jns['kode'] }}</b> 2021/2022 smt I
                                            (satu)
                                        </h5>
                                    </div>
                                    <div class="card-body" style="display: block;">
                                        <div><b>Kelas: 10</b></div>
                                        <div class="row mt-2">
                                            @foreach ($jns['jadwal'] as $jdwl)
                                                @php
                                                    if ($jdwl['status'] == 0) {
                                                        $warna = 'danger';
                                                    } elseif (date('Y-m-d') >= $jdwl['tgl_mulai'] && date('Y-m-d') <= $jdwl['tgl_selesai']) {
                                                        $warna = 'success';
                                                    } elseif (date('Y-m-d') < $jdwl['tgl_mulai']) {
                                                        $warna = 'secondary';
                                                    } elseif (date('Y-m-d') > $jdwl['tgl_mulai']) {
                                                        $warna = 'purple';
                                                    }
                                                @endphp
                                                <div class="col-lg-3 col-md-4 col-sm-6 col-12">

                                                    <div class="card bg-{{ $warna }}">
                                                        <h5 class="box-title text-center text-white">{{ $jdwl['mapel'] }}
                                                        </h5>
                                                        <p class="my-0 mx-1 text-white">Jumlah Soal
                                                            {{ $jdwl['jumlah_soal'] }}</p>
                                                        <p class="my-0 mx-1 text-white">Kelas <span class="float-right">
                                                                @php
                                                                    if (!empty($jdwl['rombel_bank'])) {
                                                                        $rombel = [];
                                                                        foreach ($jdwl['rombel_bank'] as $rmb) {
                                                                            $rombel[] = $rmb['nama'];
                                                                        }
                                                                    }
                                                                    echo implode(', ', $rombel);
                                                                @endphp
                                                            </span>
                                                        </p>
                                                        <ul class="list-group list-group-unbordered m-1">
                                                            <li class="list-group-item p-1 text-dark"> Jenis
                                                                <span class="float-right"><b>PAT</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Kode Soal
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['kode_bank'] }}</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Mulai
                                                                <span class="float-right">
                                                                    <b>{{ (new \App\Helpers\Help())->getDay($jdwl['tgl_mulai']) }}</b>
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Sampai
                                                                <span
                                                                    class="float-right"><b>{{ (new \App\Helpers\Help())->getDay($jdwl['tgl_selesai']) }}</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Durasi
                                                                <span class="float-right"><b>{{ $jdwl['durasi_ujian'] }}
                                                                        Menit</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Acak Soal
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['acak_soal'] == 1 ? 'Ya' : 'Tidak' }}</b></span>
                                                            </li>
                                                            {{-- <li class="list-group-item p-1"> Acak Jawaban
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['acak_opsi'] == 1 ? 'Ya' : 'Tidak' }}</b></span>
                                                            </li> --}}
                                                            <li class="list-group-item p-1 text-dark"> Hasil Tampil
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['hasil_tampil'] == 1 ? 'Ya' : 'Tidak' }}</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1 text-dark"> Gunakan Token
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['token'] == 1 ? 'Ya' : 'Tidak' }}</b></span>
                                                            </li>
                                                            {{-- <li class="list-group-item p-1"> Reset Login
                                                                <span class="float-right"><b>Tidak</b></span>
                                                            </li> --}}
                                                            <li class="list-group-item p-1 text-dark"> Status
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['status'] == 1 ? 'Aktif' : 'Non Aktif' }}</b></span>
                                                            </li>
                                                            {{-- <li class="list-group-item p-1"> Sudah Rekap
                                                                <span class="float-right"><b>Belum</b></span>
                                                            </li> --}}
                                                            {{-- <li class="list-group-item p-1"> Mengerjakan
                                                                <span class="float-right"><b>0</b></span>
                                                            </li> --}}
                                                        </ul>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <input name="checked[]" value="{{ $jdwl['id'] }}"
                                                                class="check-jadwal float-left" type="checkbox"
                                                                style="width: 28px;height: 28px">
                                                            <div class="aksi">
                                                                <a href="{{ route('cbt-jadwal_edit', ['k' => (new \App\Helpers\Help())->encode($jdwl['id'])]) }}"
                                                                    class="btn btn-info btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <button class="btn btn-danger btn-sm delete"
                                                                    data-id="{{ $jdwl['id'] }}"><i
                                                                        class="fas fa-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
                if (this.checked) {
                    $('#submit-hapus').show();
                } else {
                    $('#submit-hapus').hide();
                }
            });

            $('#submit-hapus').click(function() {
                var id_jadwal = [];
                $(".check-jadwal:checked").each(function() {
                    id_jadwal.push($(this).val());
                });
                if (id_jadwal.length > 0) {
                    var confirmdelete = confirm("Apa kamu yakin ingin menghapus data ini?");
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('cbt-jadwal_many_delete') }}",
                            type: 'post',
                            data: {
                                id_jadwal
                            },
                            beforeSend: function() {
                                $("#submit-hapus").html(
                                    '<i class="fa fa-spin fa-sync-alt"></i> Menghapus..');
                                $("#submit-hapus").attr("disabled", true);
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    location.reload();
                                    $(".check-jadwal, #checkAll").prop("checked", false);
                                } else {
                                    $('#submit-hapus').html(
                                        '<i class="far fa-trash-alt"></i> Hapus Jadwal Terpilih'
                                    );
                                    $("#submit-hapus").attr("disabled", false);
                                }
                                swa(data.status + "!", data.message, data.icon);

                            }
                        });
                    }
                }
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-jadwal_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function reloadL() {
            $(".btn-reload").html(
                '<i class="fa fa-spin fa-sync-alt"></i> Reload..');
            window.location.reload();
        }
    </script>
@endsection
