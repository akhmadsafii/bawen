@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="main-wrapper clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <form id="formSoal">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="l0">Mapel</label>
                                            <div class="col-md-10">
                                                <select name="id_mapel" id="id_mapel" class="form-control">
                                                    <option value="">Pilih Mapel..</option>
                                                    @foreach ($mapel as $mp)
                                                        <option value="{{ $mp['id'] }}">{{ $mp['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="l10">Guru</label>
                                            <div class="col-md-10">
                                                <select name="id_guru" id="id_guru" class="form-control" disabled>
                                                    <option value="">Pilih guru..</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="l10">Tipe Soal</label>
                                            <div class="col-md-10">
                                                <select name="tipe_soal" id="tipe_soal" class="form-control">
                                                    <option value="">Pilih Tipe Soal..</option>
                                                    <option value="pilihan" selected>Pilihan Ganda</option>
                                                    <option value="essay">Essay</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="l11">Teks Soal</label>
                                            <div class="col-md-3">
                                                <input type="file" name="gambar" id="gambar" class="form-control">
                                            </div>
                                            <div class="col-md-7">
                                                <textarea name="soal" id="soal" class="form-control editor"></textarea>
                                            </div>
                                        </div>
                                        <div class="pilihan">
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label" for="l11">Jawaban A</label>
                                                <div class="col-md-3">
                                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea name="soal" id="soal" class="form-control editor"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label" for="l11">Jawaban B</label>
                                                <div class="col-md-3">
                                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea name="soal" id="soal" class="form-control editor"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label" for="l11">Jawaban C</label>
                                                <div class="col-md-3">
                                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea name="soal" id="soal" class="form-control editor"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label" for="l11">Jawaban D</label>
                                                <div class="col-md-3">
                                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea name="soal" id="soal" class="form-control editor"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label pilihan" for="l11">Kunci Jawaban</label>
                                            <div class="col-md-3 pilihan">
                                                <input type="text" name="kunci_jawaban" id="id_kunci_jawaban"
                                                    class="form-control">
                                            </div>
                                            <label class="col-md-2 col-form-label" for="l11">Bobot Nilai Soal</label>
                                            <div class="col-md-5">
                                                <input type="text" name="kunci_jawaban" id="id_kunci_jawaban"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-actions btn-list">
                                            <input type="hidden" name="action" id="action" value="Add">
                                            <button class="btn btn-primary" type="submit" id="saveBtn">Simpan</button>
                                            <a class="btn btn-outline-default" href="{{ url()->previous() }}">Kembali</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    /*
                      Note: In modern browsers input[type="file"] is functional without
                      even adding it to the DOM, but that might not be the case in some older
                      or quirky browsers like IE, so you might want to add it to the DOM
                      just in case, and visually hide it. And do not forget do remove it
                      once you do not need it anymore.
                    */

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            /*
                              Note: Now we need to register the blob in TinyMCEs image blob
                              registry. In the next release this part hopefully won't be
                              necessary, as we are looking to handle it internally.
                            */
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
                //images_upload_handler:function(blobInfo, success, failure){}
            });

            $(document).on('change', '#id_mapel', function() {
                let id_mapel = $(this).val();
                if (id_mapel) {
                    $.ajax({
                        url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id_mapel
                        },
                        beforeSend: function() {
                            $('#id_guru').html(
                                '<option value="">Memproses guru..</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('#id_guru').html(
                                    '<option value="">Tidak ada guru yg terkait</option>');
                                $('#id_guru').removeAttr('enabled');
                            } else {
                                var s = '';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_guru + '">' + row
                                        .guru +
                                        '</option>';

                                })
                                $('#id_guru').removeAttr('disabled');
                            }
                            $('#id_guru').html(s)
                        }
                    });
                }
            });
            $(document).on('change', '#tipe_soal', function() {
                let soal = $(this).val();
                if (soal) {
                    if(soal == "essay"){
                        $('.pilihan').hide();
                    }else{
                        $('.pilihan').show();
                    }
                }
            });
            $(document).on('change', '#id_mapel', function() {
                let id_mapel = $(this).val();
                if (id_mapel) {
                    $.ajax({
                        url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id_mapel
                        },
                        beforeSend: function() {
                            $('#id_guru').html(
                                '<option value="">Memproses guru..</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('#id_guru').html(
                                    '<option value="">Tidak ada guru yg terkait</option>');
                                $('#id_guru').removeAttr('enabled');
                            } else {
                                var s = '';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_guru + '">' + row
                                        .guru +
                                        '</option>';

                                })
                                $('#id_guru').removeAttr('disabled');
                            }
                            $('#id_guru').html(s)
                        }
                    });
                }
            });

            $('body').on('submit', '#formSoal', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('cbt-soal_create') }}";
                    method_url = "POST";
                }

                // if ($('#action').val() == 'Edit') {
                //     action_url = "{{ route('update-guru') }}";
                //     method_url = "PUT";
                // }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        })
    </script>
@endsection
