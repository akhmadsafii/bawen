@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="main-wrapper clearfix pt-0">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol float-right">
                                        <a href="{{ route('cbt-soal_new') }}" class="btn btn-info"><i
                                                class="fas fa-plus-circle"></i> Tambah</a>
                                        <a href="javascript:void(0)" class="btn btn-purple"><i class="fas fa-download"></i>
                                            Download</a>
                                        <a href="javascript:void(0)" class="btn btn-primary"><i
                                                class="fas fa-file-export"></i> Export</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered mr-b-0 hover">
                                    <thead>
                                        <tr class="bg-info">
                                            <th class="text-center">No</th>
                                            <th class="text-center">Soal</th>
                                            <th class="text-center">Mapel / Guru</th>
                                            <th class="text-center">Analisa</th>
                                            <th class="text-center">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_mapel">
                                        @if (!empty($mapel))
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($mapel as $mp)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>
                                                        <b>{{ ucwords($mp['nama']) }} - Kode.
                                                            {{ $mp['kode_mapel'] }}</b>
                                                        <p class="m-0">Kelompok {{ $mp['kelompok'] }}</p>
                                                    </td>
                                                    <td>
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                {{ $mp['status'] == 1 ? 'checked' : '' }}
                                                                class="mapel_check" data-id="{{ $mp['id'] }}">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-info edit"
                                                            data-id="{{ $mp['id'] }}"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                            data-id="{{ $mp['id'] }}"><i class="fas fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5" class="text-center">Data saat ini belum tersedia</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalMapel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formMapel" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <input type="hidden" name="id" id="id_mapel">
                            <div class="col-lg-4">
                                <div class="form-group mb-1 mt-3">
                                    <label for="l30">Kode Mapel</label>
                                    <input type="text" name="kode_mapel[]" id="kode_mapel" autocomplete="off"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-1 mt-3">
                                    <label for="l31">Nama Mapel</label>
                                    <div class="input-group">
                                        <input type="text" name="nama[]" id="nama" autocomplete="off" class="form-control"
                                            required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group mb-1 mt-3">
                                    <label for="l31">Kelompok</label>
                                    <div class="input-group">
                                        <div class="input-group">
                                            <select name="kelompok[]" id="kelompok" class="form-control" required>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fomAddMapel">

                        </div>
                        <div class="form-group row tambahBaris">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" onclick="tambahData()" class="btn btn-success btn-sm"><i
                                        class="fa fa-plus"></i> tambah baris</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.mapel_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt-mapel_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '#addData', function() {
                $('.fomAddMapel').html('');
                $('.tambahBaris').show('');
                $('#formMapel').trigger("reset");
                $('#modelHeading').html("Tambah Pelajaran");
                $('#modalMapel').modal('show');
                $('#action').val('Add');
            });

            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-mapel') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Mapel");
                        $('#id_mapel').val(data.id);
                        $('#kode_mapel').val(data.kode_mapel);
                        $('#nama').val(data.nama);
                        $('.tambahBaris').hide();
                        $('.fomAddMapel').html('');
                        $('#kelompok').val(data.kelompok).trigger("change");
                        $('#modalMapel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $('#formMapel').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('cbt-mapel_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('cbt-mapel_update') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formMapel').trigger("reset");
                            $('#modalMapel').modal('hide');
                            $('#data_mapel').html(data.mapel);
                        }
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.message);
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-mapel_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_mapel').html(data.mapel);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

        })

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddMapel').append(
                '<div class="row mb-1" id="row' + i +
                '"><div class="col-lg-4 mb-1"><div class="form-group mb-1 mt-3"><label for="l30">Kode Mapel</label><input type="text" name="kode_mapel[]" id="kode_mapel" autocomplete="off" class="form-control" required></div></div><div class="col-lg-6 mb-1"><div class="form-group mb-1 mt-3"><label for="l31">Nama Mapel</label><div class="input-group"><input type="text" name="nama[]" autocomplete="off" id="nama" class="form-control" required></div></div></div><div class="col-lg-2 mb-1"><div class="form-group mb-1 mt-3"><label for="l31">Kelompok</label><div class="input-group"><div class="input-group"><select name="kelompok[]" id="kelompok" class="form-control" required><option value="A">A</option><option value="B">B</option></select></div></div></div></div><div class="col-md-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }
    </script>
@endsection
