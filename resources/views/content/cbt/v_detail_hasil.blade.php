@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
<style>
    img {
        max-width: 100%;
        height: auto !important;
    }

</style>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default my-shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <table class="table table-bordered table-sm">
                                <tr>
                                    <td style="width: 120px">Nama</td>
                                    <td>{{ $detail['nama'] }}</td>
                                </tr>
                                <tr>
                                    <td>N I S</td>
                                    <td>{{ $detail['nis'] }}</td>
                                </tr>
                                <tr>
                                    <td>Rombel</td>
                                    <td>{{ $detail['rombel'] }}</td>
                                </tr>
                                <tr>
                                    <td>No. Peserta</td>
                                    <td>{{ $detail['nomor_peserta'] }}</td>
                                </tr>
                                <tr>
                                    <td>Sesi</td>
                                    <td>{{ $detail['sesi'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-5">
                            <table class="table table-bordered table-sm" id="table-status">
                                <tr>
                                <tr>
                                    <td>Ruang</td>
                                    <td>{{ $detail['kode_ruang'] }}</td>
                                </tr>
                                <td>Mata Pelajaran</td>
                                <td>{{ $detail['mapel'] }}</td>
                                </tr>
                                <tr>
                                    <td>Guru</td>
                                    <td>{{ $detail['guru'] }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Ujian</td>
                                    <td>{{ $detail['kode_jenis_ujian'] }}</td>
                                </tr>
                                <tr>
                                    <td>Tahun Pelajaran</td>
                                    <td>{{ $tahun['tahun_ajaran'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table class="table table-bordered table-sm" id="table-status">
                                <tr>
                                    <td>PG</td>
                                    <td class="text-center">{{ $detail['pg_nilai'] }}</td>
                                    <td rowspan="5" style="width: 100px" class="text-center">
                                        <b>NILAI</b>
                                        <br>
                                        <span
                                            style="font-size: 40pt">{{ round($detail['pg_nilai'] + $detail['kompleks_nilai'] + $detail['isian_nilai'] + $detail['essai_nilai'], 1) }}</span>
                                        {{-- <span style="font-size: 40pt">{{ $nilai_ee + $nilai_es + $nilai_k + $nilai_np }}</span> --}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>PK</td>
                                    <td class="text-center">{{ $detail['kompleks_nilai'] }}</td>
                                </tr>
                                <tr>
                                    <td>IS</td>
                                    <td class="text-center">{{ $detail['isian_nilai'] }}</td>
                                </tr>
                                <tr>
                                    <td>ES</td>
                                    <td class="text-center">{{ $detail['essai_nilai'] }}</td>
                                </tr>
                            </table>
                            <div class="float-right">
                                <a href="{{ session('url_back') }}" class="btn btn-danger btn-sm"><i
                                        class="fas fa-angle-left"></i> Kembali</a>
                                <button
                                    class="btn-sm btn btn-{{ $nilai['koreksi'] == 1 ? 'success' : 'info' }} btn-marked"
                                    data-id="{{ $nilai['id'] }}" {{ $nilai['koreksi'] == 1 ? 'disabled' : '' }}><i
                                        class="fas fa-{{ $nilai['koreksi'] == 1 ? 'clipboard-check' : 'spell-check' }}"></i>
                                    {{ $nilai['koreksi'] == 1 ? 'Sudah di koreksi' : 'Tandai Sudah' }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="konten-soal">
                <div class="card my-3">
                    <div class="card-header" role="tab" id="heading13">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="my-2"><b>I. PILIHAN
                                        GANDA</b></h5>
                            </div>
                            <div class="col-md-2">
                                <a class="float-right btn btn-danger btn-sm" data-toggle="collapse" href="#pilgan"><i
                                        class="fas fa-minus"></i></a>
                            </div>
                        </div>

                    </div>
                    <div id="pilgan" class="card-collapse collapse show" role="tabpanel">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <ul class="m-1">
                                    <li>Bobot soal: <b>{{ $hasil['bobot_pilgan'] }}</b>. Jumlah soal:
                                        <b>{{ $hasil['jml_pilgan'] }}</b>. Max point persoal:
                                        <b>{{ round($hasil['bobot_pilgan'] / $hasil['jml_pilgan'], 1) }}</b>.
                                    </li>
                                    <li>Point soal PG tidak bisa diedit</li>
                                </ul>
                            </div>
                            <table id="table-pg" class="w-100 table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">No</th>
                                        <th class="text-center align-middle bg-blue text-white">Soal</th>
                                        <th class="text-center align-middle bg-blue text-white">Pilihan</th>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">Jawaban
                                            Benar</th>
                                        <th class="text-center align-middle bg-teal d-none" style="width: 50px">No. Acak
                                        </th>
                                        <th class="text-center align-middle bg-teal d-none">Pilihan Acak</th>
                                        <th class="text-center align-middle bg-teal d-none" style="width: 50px">Jawaban
                                            Siswa Acak
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Jawaban
                                            Siswa</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Analisa
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Point
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_pg = 1;
                                        $tot_np = 0;
                                    @endphp
                                    @foreach ($hasil['soal'] as $sp)
                                        @if ($sp['tipe'] == 1)
                                            <tr>
                                                <td class="text-center">{{ $no_pg++ }}</td>
                                                <td>{!! $sp['soal'] !!}</td>
                                                <td>
                                                    <ol type="A">
                                                        @for ($i = 1; $i <= $hasil['opsi']; $i++)
                                                            <li>
                                                                {!! $sp['opsi_' . strtolower((new \App\Helpers\Help())->alphabet($i))] !!}
                                                            </li>
                                                        @endfor
                                                    </ol>
                                                </td>
                                                <td class="text-center">{{ strtoupper($sp['jawaban']) }}</td>
                                                <td class="text-center d-none">2</td>
                                                <td class="d-none">
                                                    <ol type="A">
                                                        <li>
                                                            <span
                                                                style="color: rgb(0, 0, 0); font-family: &quot;PT Serif&quot;, Georgia, serif; font-size: 20px; white-space: pre-wrap;">Sumber
                                                                barang elektronik</span>
                                                        </li>
                                                        <li>
                                                            <span
                                                                style="color: rgb(0, 0, 0); font-family: &quot;PT Serif&quot;, Georgia, serif; font-size: 20px; white-space: pre-wrap;">Alat
                                                                untuk membuat tanaman</span>
                                                        </li>
                                                        <li>
                                                            <span
                                                                style="color: rgb(0, 0, 0); font-family: &quot;PT Serif&quot;, Georgia, serif; font-size: 20px; white-space: pre-wrap;">Sumber
                                                                minuman</span>
                                                        </li>
                                                        <li>
                                                            <span
                                                                style="color: rgb(0, 0, 0); font-family: &quot;PT Serif&quot;, Georgia, serif; font-size: 20px; white-space: pre-wrap;">Alat
                                                                untuk bahan bakar</span>
                                                        </li>
                                                    </ol>
                                                </td>
                                                <td class="text-center d-none">C</td>
                                                <td class="text-center">{{ strtoupper($sp['jawaban_siswa']) }}
                                                </td>
                                                <td class="text-center">
                                                    <i
                                                        class="fa {{ $sp['jawaban_siswa'] == $sp['jawaban'] ? 'fa-check-circle text-green' : 'fa-times-circle text-red' }}"></i>
                                                </td>
                                                <td class="text-center">
                                                    {{ $sp['jawaban_siswa'] == $sp['jawaban'] ? round($sp['point_soal'], 1) : 0 }}
                                                </td>
                                            </tr>
                                            @php
                                                if ($sp['jawaban_siswa'] == $sp['jawaban']) {
                                                    $tot_np += $sp['point_soal'];
                                                }

                                            @endphp
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="6" class="text-right"><b>TOTAL SCORE PILIHAN GANDA</b></td>
                                        <td class="text-center"><b>{{ round($tot_np, 1) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="overlay d-none" id="loading-pg">
                            <div class="spinner-grow"></div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="my-2"><b>II. PILIHAN GANDA KOMPLEKS</b></h5>
                            </div>
                            <div class="col-md-2">
                                <a class="float-right btn btn-danger btn-sm" data-toggle="collapse" href="#pilkom"><i
                                        class="fas fa-minus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="pilkom" class="card-collapse collapse show">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <ul>
                                    <li>Bobot soal: <b>{{ $hasil['bobot_kompleks'] }}</b>. Jumlah soal:
                                        <b>{{ $hasil['jml_kompleks'] }}</b>. Max point persoal:
                                        <b>{{ round($hasil['bobot_kompleks'] / $hasil['jml_kompleks'], 1) }}</b>.
                                    </li>
                                    <li>Point soal PG Kompleks bisa diedit</li>
                                    <li>Utamakan mengkoreksi hasil analisa yang berwarna kuning</li>
                                    <li>Klik <i class="fa fa-pencil"></i> untuk mengedit point. Klik <i
                                            class="fa fa-undo"></i> untuk mengembalikan ke point otomatis
                                    </li>
                                    <li>Klik tobol <b>SIMPAN</b> untuk menyimpan perubahan point</li>
                                </ul>
                            </div>
                            <table id="table-pg2" class="w-100 table table-sm table-striped table-bordered border-success">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">No.
                                        </th>
                                        <th class="text-center align-middle bg-blue text-white">Soal</th>
                                        <th class="text-center align-middle bg-blue text-white">Pilihan</th>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">Jawaban
                                            Benar</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Jawaban
                                            Siswa</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Analisa
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Point
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Aksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_k = 1;
                                        $nilai_k = 0;
                                    @endphp
                                    @foreach ($hasil['soal'] as $sk)
                                        @if ($sk['tipe'] == 2)
                                            <tr>
                                                <td class="text-center">{{ $no_k++ }}</td>
                                                <td style="word-wrap: break-word">{!! $sk['soal'] !!}</td>
                                                @php
                                                    $opsi_pk = json_decode($sk['opsi_a']);
                                                    $jb_pk = str_replace('###', ', ', $sk['jawaban']);
                                                    $js_pk = str_replace('###', ', ', $sk['jawaban_siswa']);
                                                @endphp
                                                <td>
                                                    <ol type="A">
                                                        @foreach ($opsi_pk as $jk)
                                                            <li>{!! $jk->isian !!}</li>
                                                        @endforeach
                                                    </ol>
                                                </td>
                                                <td class="text-center">{{ strtoupper($jb_pk) }}</td>
                                                <td class="text-center">{{ strtoupper($js_pk) }}</td>
                                                @php
                                                    if (strip_tags($sk['jawaban']) == strip_tags($sk['jawaban_siswa'])) {
                                                        if ($sk['nilai_koreksi'] == $sk['nilai_otomatis']) {
                                                            $icon_kompleks = 'fa-check-circle text-success';
                                                        } else {
                                                            $icon_kompleks = 'fa-spell-check text-info';
                                                        }
                                                    } else {
                                                        if ($sk['nilai_koreksi'] == $sk['nilai_otomatis']) {
                                                            $icon_kompleks = 'fa-times-circle text-warning';
                                                        } else {
                                                            $icon_kompleks = 'fa-spell-check text-info';
                                                        }
                                                    }
                                                @endphp
                                                <td class="text-center"><i class="fa {{ $icon_kompleks }}"></i>
                                                </td>
                                                @php
                                                    $nilai_k += $sk['nilai_koreksi'];
                                                @endphp
                                                <td class="text-center">
                                                    <input id="input{{ $sk['id'] }}" name="input{{ $sk['id'] }}"
                                                        value="{{ $sk['nilai_koreksi'] }}" type="number" min="0"
                                                        max="{{ round($hasil['bobot_kompleks'] / $hasil['jml_kompleks'], 1) }}"
                                                        step="0.1"
                                                        style="width: 100%; box-sizing: border-box; -webkit-box-sizing:border-box; -moz-box-sizing: border-box; display: none" />
                                                    <span class="pg2" data-idsoal="{{ $sk['id'] }}"
                                                        id="span{{ $sk['id'] }}">{{ $sk['nilai_koreksi'] }}</span>
                                                </td>
                                                <td>

                                                    <button id="edit{{ $sk['id'] }}" type="button"
                                                        class="btn btn-sm" onclick="edit({{ $sk['id'] }})"><i
                                                            class="fa fa-pencil"></i>
                                                    </button>
                                                    <button id="undo{{ $sk['id'] }}" type="button"
                                                        class="btn btn-sm"
                                                        onclick="undo({{ $sk['id'] }}, {{ $sk['nilai_otomatis'] }})"><i
                                                            class="fa fa-undo"></i></button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach

                                    <tr>
                                        <td colspan="6" class="text-right text-bold">TOTAL SCORE PILIHAN GANDA KOMPLEKS</td>
                                        <td class="text-center text-bold">{{ $nilai_k }}</td>
                                        <td>
                                            <button id="pg2"
                                                data-max="{{ round($hasil['bobot_kompleks'] / $hasil['jml_kompleks'], 1) }}"
                                                class="btn btn-sm btn-info" onclick="simpan(this)"><i
                                                    class="fas fa-save"></i> Simpan
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- <div class="card card-default my-shadow mb-4">
                    <div class="card-header">
                        <h6 class="card-title"><b>III. MENJODOHKAN</b></h6>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-default-success" role="alert">
                            <ul>
                                <li>Bobot soal: <b>25</b>. Jumlah soal:
                                    <b>2</b>. Max point persoal:
                                    <b>12.5</b>.
                                </li>
                                <li>Point soal menjodohkan bisa diedit</li>
                                <li>Utamakan mengkoreksi hasil analisa yang berwarna kuning</li>
                                <li>Klik <i class="fa fa-pencil"></i> untuk mengedit point. Klik <i
                                        class="fa fa-undo"></i> untuk mengembalikan ke point otomatis
                                </li>
                                <li>Klik tobol <b>SIMPAN</b> untuk menyimpan perubahan point</li>
                            </ul>
                        </div>
                        <table id="table-jodohkan"
                            class="w-100 table table-sm table-striped table-bordered border-success nowrap">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle bg-blue text-white" style="width: 50px">No. Soal
                                    </th>
                                    <th class="text-center align-middle bg-blue text-white">Soal</th>
                                    <th class="text-center align-middle bg-blue text-white">Jawaban</th>
                                    <th class="text-center align-middle bg-teal text-white">Jawaban Siswa</th>
                                    <th class="text-center align-middle bg-teal text-white" style="width: 50px">Analisa
                                    </th>
                                    <th class="text-center align-middle bg-teal text-white" style="width: 80px">Point</th>
                                    <th class="text-center align-middle bg-teal text-white" style="width: 80px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td>Jodohkan soal dan jawaban berikut!</td>
                                    <td>
                                        <span>Soal 1</span>
                                        <ul>
                                            <li>jawaban 1</li>
                                            <li>jawaban 2</li>
                                        </ul>
                                        <span>soal 2</span>
                                        <ul>
                                            <li>jawaban 2</li>
                                            <li>jawaban 3</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <span>Soal 1</span>
                                        <ul>
                                            <li>jawaban 2</li>
                                        </ul>
                                        <span>soal 2</span>
                                        <ul>
                                            <li>jawaban 1</li>
                                            <li>jawaban 3</li>
                                        </ul>
                                    </td>
                                    <td class="text-center"><i class="fa fa-times-circle text-yellow text-lg"></i></td>
                                    <td class="text-center">
                                        <input id="input1211" name="input1211" value="6.25" type="number" min="0" max="12.5"
                                            step="0.10"
                                            style="width: 100%; box-sizing: border-box; -webkit-box-sizing:border-box; -moz-box-sizing: border-box; display: none" />
                                        <span class="jodohkan" data-idsoal="1211" id="span1211">6.25</span>
                                    </td>
                                    <td>
                                        <button id="edit1211" type="button" class="btn btn-sm" onclick="edit(1211)"><i
                                                class="fa fa-pencil"></i>
                                        </button>
                                        <button id="undo1211" type="button" class="btn btn-sm"
                                            onclick="undo(1211, 6.25)"><i class="fa fa-undo"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td>Test lagi jawaban&nbsp;</td>
                                    <td>
                                        <span>Baris 1</span>
                                        <ul>
                                            <li>Kolom 1</li>
                                            <li>Kolom 2</li>
                                        </ul>
                                        <span>Baris 2</span>
                                        <ul>
                                            <li>Kolom 2</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <span>Baris 1</span>
                                        <ul>
                                            <li>Kolom 2</li>
                                        </ul>
                                        <span>Baris 2</span>
                                        <ul>
                                            <li>Kolom 1</li>
                                            <li>Kolom 2</li>
                                        </ul>
                                    </td>
                                    <td class="text-center"><i class="fa fa-times-circle text-yellow text-lg"></i></td>
                                    <td class="text-center">
                                        <input id="input1215" name="input1215" value="12" type="number" min="0" max="12.5"
                                            step="0.10"
                                            style="width: 100%; box-sizing: border-box; -webkit-box-sizing:border-box; -moz-box-sizing: border-box; display: none" />
                                        <span class="jodohkan" data-idsoal="1215" id="span1215">12</span>
                                    </td>
                                    <td>
                                        <button id="edit1215" type="button" class="btn btn-sm" onclick="edit(1215)"><i
                                                class="fa fa-pencil"></i>
                                        </button>
                                        <button id="undo1215" type="button" class="btn btn-sm"
                                            onclick="undo(1215, 9.38)"><i class="fa fa-undo"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right text-bold">TOTAL SCORE MENJODOHKAN</td>
                                    <td class="text-center text-bold">18.25</td>
                                    <td>
                                        <button id="jodohkan" data-max="12.5" class="btn btn-sm btn-primary"
                                            onclick="simpan(this)">Simpan
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="overlay d-none" id="loading-jodohkan">
                        <div class="spinner-grow"></div>
                    </div>
                </div> --}}
                <div class="card my-2">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="my-2"><b>III. ISIAN SINGKAT</b></h5>
                            </div>
                            <div class="col-md-2">
                                <a class="float-right btn btn-danger btn-sm" data-toggle="collapse" href="#issin"><i
                                        class="fas fa-minus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="issin" class="card-collapse collapse show">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <ul>
                                    <li>Bobot soal: <b>{{ $hasil['bobot_isian'] }}</b>. Jumlah soal:
                                        <b>{{ $hasil['jml_isian'] }}</b>. Max point persoal:
                                        <b>{{ round($hasil['bobot_isian'] / $hasil['jml_isian'], 1) }}</b>.
                                    </li>
                                    <li>Point soal isian singkat bisa diedit</li>
                                    <li>Utamakan mengkoreksi hasil analisa yang berwarna kuning</li>
                                    <li>Klik <i class="fa fa-pencil"></i> untuk mengedit point. Klik <i
                                            class="fa fa-undo"></i> untuk mengembalikan ke point otomatis
                                    </li>
                                    <li>Klik tobol <b>SIMPAN</b> untuk menyimpan perubahan point</li>
                                </ul>
                            </div>
                            <table id="table-isian" class="w-100 table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">No.
                                        </th>
                                        <th class="text-center align-middle bg-blue text-white">Soal</th>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">Jawaban
                                            Benar</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Jawaban
                                            Siswa</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Analisa
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Point
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Aksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_es = 1;
                                        $nilai_es = 0;
                                    @endphp
                                    @foreach ($hasil['soal'] as $es)
                                        @if ($es['tipe'] == 4)
                                            @php
                                                $nilai_es += $es['nilai_koreksi'];
                                            @endphp
                                            <tr>
                                                <td class="text-center">{{ $no_es++ }}</td>
                                                <td>{!! $es['soal'] !!}
                                                </td>
                                                <td class="text-center">{!! $es['jawaban'] !!}</td>
                                                <td class="text-center">{!! $es['jawaban_siswa'] !!}</td>
                                                @php
                                                    if (strip_tags($es['jawaban']) == strip_tags($es['jawaban_siswa'])) {
                                                        if ($es['nilai_koreksi'] == $es['nilai_otomatis']) {
                                                            $icon_es = 'fa-check-circle text-success';
                                                        } else {
                                                            $icon_es = 'fa-spell-check text-info';
                                                        }
                                                    } else {
                                                        if ($es['nilai_koreksi'] == $es['nilai_otomatis']) {
                                                            $icon_es = 'fa-times-circle text-warning';
                                                        } else {
                                                            $icon_es = 'fa-spell-check text-info';
                                                        }
                                                    }
                                                @endphp
                                                <td class="text-center"><i class="fa {{ $icon_es }}"></i>
                                                </td>
                                                <td class="text-center">
                                                    <input id="input{{ $es['id'] }}" name="input{{ $es['id'] }}"
                                                        value="{{ $es['nilai_koreksi'] }}" type="number" min="0"
                                                        max="{{ round($hasil['bobot_isian'] / $hasil['jml_isian'], 1) }}"
                                                        step="0.1"
                                                        style="width: 100%; box-sizing: border-box; -webkit-box-sizing:border-box; -moz-box-sizing: border-box; display: none" />
                                                    <span class="isian" data-idsoal="{{ $es['id'] }}"
                                                        id="span{{ $es['id'] }}">{{ $es['nilai_koreksi'] }}</span>
                                                </td>
                                                <td>
                                                    <button id="edit{{ $es['id'] }}" type="button"
                                                        class="btn btn-sm" onclick="edit({{ $es['id'] }})"><i
                                                            class="fa fa-pencil"></i>
                                                    </button>
                                                    <button id="undo{{ $es['id'] }}" type="button"
                                                        class="btn btn-sm"
                                                        onclick="undo({{ $es['id'] }}, {{ $es['nilai_otomatis'] }})"><i
                                                            class="fa fa-undo"></i></button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="5" class="text-right text-bold">TOTAL SCORE ISIAN SINGKAT</td>
                                        <td class="text-center text-bold">{{ $nilai_es }}</td>
                                        <td>
                                            <button id="isian"
                                                data-max="{{ round($hasil['bobot_isian'] / $hasil['jml_isian'], 1) }}"
                                                class="btn btn-sm btn-info" onclick="simpan(this)"><i
                                                    class="fas fa-save"></i> Simpan
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card my-2">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="my-2"><b>IV. URAIAN</b></h5>
                            </div>
                            <div class="col-md-2">
                                <a class="float-right btn btn-danger btn-sm" data-toggle="collapse" href="#ises"><i
                                        class="fas fa-minus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="ises" class="card-collapse collapse show">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <ul>
                                    <li>Bobot soal: <b>{{ $hasil['bobot_essay'] }}</b>. Jumlah soal:
                                        <b>{{ $hasil['jml_essay'] }}</b>. Max point persoal:
                                        <b>{{ round($hasil['bobot_essay'] / $hasil['jml_essay'], 1) }}</b>.
                                    </li>
                                    <li>Point soal uraian bisa diedit</li>
                                    <li>Utamakan mengkoreksi hasil analisa yang berwarna kuning</li>
                                    <li>Klik <i class="fa fa-pencil"></i> untuk mengedit point. Klik <i
                                            class="fa fa-undo"></i> untuk mengembalikan ke point otomatis
                                    </li>
                                    <li>Klik tobol <b>SIMPAN</b> untuk menyimpan perubahan point</li>
                                </ul>
                            </div>
                            <table id="table-essai" class="w-100 table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 50px">No.
                                        </th>
                                        <th class="text-center align-middle bg-blue text-white">Soal</th>
                                        <th class="text-center align-middle bg-blue text-white" style="width: 230px">
                                            Jawaban
                                            Benar</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 230px">
                                            Jawaban
                                            Siswa</th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 50px">Analisa
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Point
                                        </th>
                                        <th class="text-center align-middle bg-teal text-white" style="width: 80px">Aksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_ee = 1;
                                        $nilai_ee = 0;
                                    @endphp
                                    @foreach ($hasil['soal'] as $ee)
                                        @if ($ee['tipe'] == 5)
                                            @php
                                                $nilai_ee += $ee['nilai_koreksi'];
                                            @endphp
                                            <tr>
                                                <td class="text-center">{{ $no_ee++ }}</td>
                                                <td>{!! $ee['soal'] !!}</td>
                                                <td class="text-center">
                                                    {!! $ee['jawaban'] !!}
                                                </td>
                                                <td class="text-center">{!! $ee['jawaban_siswa'] !!}</td>
                                                @php
                                                    if (strip_tags($ee['jawaban']) == strip_tags($ee['jawaban_siswa'])) {
                                                        if ($ee['nilai_koreksi'] == $ee['nilai_otomatis']) {
                                                            $icon_ee = 'fa-check-circle text-success';
                                                        } else {
                                                            $icon_ee = 'fa-spell-check text-info';
                                                        }
                                                    } else {
                                                        if ($ee['nilai_koreksi'] == $ee['nilai_otomatis']) {
                                                            $icon_ee = 'fa-times-circle text-warning';
                                                        } else {
                                                            $icon_ee = 'fa-spell-check text-info';
                                                        }
                                                    }
                                                @endphp
                                                <td class="text-center"><i class="fa {{ $icon_ee }}"></i>
                                                </td>
                                                <td class="text-center">
                                                    <input id="input{{ $ee['id'] }}" name="input{{ $ee['id'] }}"
                                                        value="{{ $ee['nilai_koreksi'] }}" type="number" min="0"
                                                        max="{{ round($hasil['bobot_essay'] / $hasil['jml_essay'], 1) }}"
                                                        step="0.10"
                                                        style="width: 100%; box-sizing: border-box; -webkit-box-sizing:border-box; -moz-box-sizing: border-box; display: none" />
                                                    <span class="essai" data-idsoal="{{ $ee['id'] }}"
                                                        id="span{{ $ee['id'] }}">{{ $ee['nilai_koreksi'] }}</span>
                                                </td>
                                                <td>
                                                    <button id="edit{{ $ee['id'] }}" type="button"
                                                        class="btn btn-sm" onclick="edit({{ $ee['id'] }})"><i
                                                            class="fa fa-pencil"></i>
                                                    </button>
                                                    <button id="undo{{ $ee['id'] }}" type="button"
                                                        class="btn btn-sm"
                                                        onclick="undo({{ $ee['id'] }}, {{ $ee['nilai_otomatis'] }})">
                                                        <i class="fa fa-undo"></i></button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="5" class="text-right"><b>TOTAL SCORE URAIAN</b></td>
                                        <td class="text-center"><b>{{ $nilai_ee }}</b></td>
                                        <td>
                                            <button id="essai" data-id="btn_essay"
                                                data-max="{{ round($hasil['bobot_essay'] / $hasil['jml_essay'], 1) }}"
                                                class="btn btn-sm btn-info" onclick="simpan(this)"><i
                                                    class="fas fa-save"></i> Simpan
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="koreksi">
        <input type="hidden" name="siswa" value="{{ $_GET['siswa'] }}">
        <input type="hidden" name="jadwal" value="{{ $_GET['jadwal'] }}">
    </form>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var idNilai = "{{ $nilai['id'] }}";

        function edit(id) {
            var input = $(`#input${id}`);
            var span = $(`#span${id}`);
            var btnedit = $(`#edit${id}`);

            if (input.is(":visible")) {
                input.hide();
                span.text(input.val()).show();
                btnedit.html(`<i class="fa fa-pencil"></i>`);
            } else {
                span.hide();
                input.val(span.text()).show();
                btnedit.html(`<i class="fa fa-check"></i>`);
            }
        }

        function undo(id, nilai) {
            var input = $(`#input${id}`);
            var span = $(`#span${id}`);
            input.val(nilai);
            span.text(nilai);
        }

        function simpan(btn) {
            var id = $(btn).attr('id');
            var key;
            if (id === 'pg') {
                key = 'pg_nilai';
            } else if (id === 'pg2') {
                key = 'kompleks_nilai';
            } else if (id === 'isian') {
                key = 'isian_nilai';
            } else if (id === 'essai') {
                key = 'essai_nilai';
            }

            var max = $(btn).data('max');
            var $nilai = $(`#table-${id}`).find(`.${id}`);
            var json = [];
            $.each($nilai, function() {
                var n = $(this).text();
                if (n > max) {
                    noti('error', "Point persoal harus kurang dari " + max);
                    json = [];
                    return false;
                }
                if ($(this).is(":hidden")) {
                    noti('error', "Klik tombol &#10004; dulu");
                    json = [];
                    return false;
                }

                var item = {};
                item['id_soal'] = $(this).data('idsoal');
                item['koreksi'] = $(this).text();
                json.push(item);
            });

            var dataPost = $('#koreksi').serialize() + '&jenis=' + key +
                '&nilai=' + JSON.stringify(json) + '&id_nilai=' + idNilai;
            console.log(dataPost);
            if (json.length > 0) {
                $.ajax({
                    url: "{{ route('cbt_hasil_ujian-simpan_koreksi') }}",
                    type: "POST",
                    data: dataPost,
                    beforeSend: function() {
                        $(btn).html(
                            '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            noti('error', "Tidak ada nilai yang disimpan");
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log("error", xhr.responseText);
                        noti('error', "Error");
                    }
                });
            }
        }

        $(document).ready(function() {
            $(document).on('click', '.btn-marked', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt_hasil_ujian-update_status_koreksi') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $(loader).attr("disabled", true);
                            $(loader).addClass('btn-success');
                            $(loader).removeClass('btn-info');
                            $(loader).html(
                                '<i class="fas fa-clipboard-check"></i> Sudah Dikoreksi');
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    }
                });
            });
        })
    </script>
@endsection
