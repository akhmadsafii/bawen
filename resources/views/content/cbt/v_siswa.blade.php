@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol float-right">
                                        <a href="" class="btn btn-purple"><i class="fas fa-sync-alt"></i> Refresh Page</a>
                                        <a href="javascript:void(0)" data-id="1" class="btn btn-success allAktif"><i
                                                class="fas fa-lightbulb"></i> Aktifkan Semua</a>
                                        <a href="javascript:void(0)" data-id="0" class="btn btn-danger allAktif"><i
                                                class="fas fa-user-times"></i> NonAktifkan Semua</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <form action="javascript:void(0)" id="formFilter">
                                    <div class="form-row align-items-center">
                                        <div class="col-auto">
                                            <select name="id_jurusan" id="id_jurusan" class="form-control mb-2">
                                                <option value="">Pilih Jurusan..</option>
                                                <option value="0">Tidak Punya Jurusan</option>
                                                @foreach ($jurusan as $jr)
                                                    <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-auto">
                                            <select name="id_kelas" id="id_kelas" class="form-control mb-2">
                                                <option value="">Pilih Kelas..</option>
                                                @foreach ($kelas as $kl)
                                                    <option value="{{ $kl['id'] }}">{{ $kl['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-auto">
                                            <select name="rombel" id="rombel" class="form-control mb-2" disabled>
                                                <option value="">Pilih Rombel..</option>
                                            </select>
                                        </div>

                                        <div class="col-auto">
                                            <button type="submit" id="btnFilter" class="btn btn-info mb-2"><i
                                                    class="fas fa-search"></i> Pencarian</button>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped table-bordered mr-b-0 hover">
                                    <thead>
                                        <tr class="bg-info">
                                            <th class="text-center">#</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Rombel</th>
                                            <th class="text-center">Jurusan</th>
                                            <th class="text-center">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_siswa">
                                        <tr>
                                            <td colspan="6" class="text-center">Harap filter berdasarkan rombel</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                if (id_jurusan) {
                    $('select[name="id_kelas"]').attr("disabled", true);
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    var action_url = '';

                    if (id_jurusan != 0) {
                        action_url = "{{ route('get-jurusan') }}";
                        method_url = "POST";
                    }

                    if (id_jurusan == 0) {
                        action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                        method_url = "GET";
                    }
                    $.ajax({
                        url: action_url,
                        type: method_url,
                        data: {
                            id_jurusan
                        },
                        beforeSend: function() {
                            $('select[name="id_kelas"]').html(
                                '<option value="">Load kelas..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih kelas..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama +
                                    '</option>';

                            })
                            $('select[name="id_kelas"]').attr("disabled", false);
                            $('select[name="id_kelas"]').html(s)

                        }
                    });
                } else {
                    $('select[name="id_kelas"]').attr("disabled", true);
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas
                        },
                        beforeSend: function() {
                            $('#rombel').html('<option value="">Load rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#rombel').attr("disabled", false);
                            $('#rombel').html(s);
                        }
                    });
                } else {
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })

            $('#formFilter').on('submit', function(event) {
                event.preventDefault();
                $("#btnFilter").html(
                    '<i class="fa fa-spin fa-spinner"></i> Memfilter');
                $("#btnFilter").attr("disabled", true);
                $.ajax({
                    url: "{{ route('cbt-filter_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#data_siswa').html(data);
                        $('#btnFilter').html('Filter');
                        $("#btnFilter").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.allAktif', function() {
                var id_siswa = [];
                let status = $(this).data('id');
                let loader = $(this);
                $('input[name="id_siswa[]"]').each(function() {
                    id_siswa.push($(this).val());
                });
                // console.log(id_siswa);
                if (id_siswa.length > 0) {
                    var confirmNonaktif = confirm("Apa kamu yakin ingin menonaktifkan semua siswa?");
                    if (confirmNonaktif == true) {
                        $.ajax({
                            url: "{{ route('cbt-update_status_siswa') }}",
                            type: 'post',
                            data: {
                                id_siswa,
                                status
                            },
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Mengupadte..');
                            },
                            success: function(data) {
                                if (status == 1) {
                                    $(loader).html(
                                        '<i class="fas fa-lightbulb"></i> Aktifkan Semua'
                                    );
                                } else {
                                    $(loader).html(
                                        '<i class="fas fa-user-times"></i> NonAktifkan Semua'
                                    );
                                }

                                if (data.status == 'berhasil') {
                                    $('#data_siswa').html(data.siswa);
                                }
                                noti(data.icon, data.message);

                            }
                        });
                    }
                }
            });

        })
    </script>
@endsection
