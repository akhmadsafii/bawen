@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default my-shadow mb-4">
                <div class="card-header">
                    <h5 class="box-title mr-b-0">Rekap Penilaian</h5>
                </div>
                <div class="card-body">
                    <div class="row" id="konten">
                        <div class="alert alert-info align-content-center w-100" role="alert">
                            INFO TABEL PENILAIAN
                            <ul>
                                <li>
                                    Tabel ini berisi Jadwal Ujian dan Bank Soal yang belum dihapus
                                </li>
                                <li>
                                    Lakukan Aksi <b>REKAP NILAI</b> agar nilai hasil siswa bisa diekspor dan diolah
                                </li>
                                <li>
                                    <b>REKAP NILAI</b> berguna untuk membackup nilai siswa agar bisa dibuka kapan saja
                                </li>
                                <li>
                                    <b>REKAP NILAI</b> hanya untuk jadwal penilaian yang sudah dilaksanakan
                                </li>
                                <li>
                                    Jadwal Penilaian yang sudah direkap bisa dihapus di menu <b>Jadwal Ujian</b> atau
                                    <b>Bank Soal</b>
                                </li>
                            </ul>
                        </div>


                        <div class="col-12 mb-3">
                            <button id="hapusterpilih" onclick="bulk_delete()" type="button"
                                class="btn btn-outline-danger mr-1" data-toggle="tooltip" title="Hapus Terpilh"
                                disabled="disabled"><i class="far fa-trash-alt"></i> Hapus Terpilih</button>
                            <button id="rekapterpilih" onclick="bulk_rekap()" type="button"
                                class="btn btn-outline-success mr-1" data-toggle="tooltip" title="Rekap Terpilh"
                                disabled="disabled"><i class="fa fa-database"></i> Rekap Terpilih</button>
                            <a href="http://garuda.mysch.web.id/cbtrekap/export" type="button"
                                class="btn btn-success mr-1 float-right"><i class="fa fa-download"></i> <span
                                    class="d-none d-sm-inline-block ml-1">Ekspor Semua</a>
                        </div>

                        <table id="jadwal-bank" class="w-100 table table-sm table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="text-center">
                                            <input id="check-all" class="check-all" type="checkbox">
                                        </div>
                                    </th>
                                    <th class="text-center align-middle p-0">No.</th>
                                    <th>Bank Soal</th>
                                    <th>Jenis</th>
                                    <th>Mapel</th>
                                    <th>Kelas</th>
                                    <th>Pelaksanaan</th>
                                    <th class="text-center align-middle p-0"><span>Nilai</span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td class="align-middle">
                                        <div class="text-center">
                                            <input class="check" value="0" type="checkbox">
                                        </div>
                                    </td>
                                    <td class="text-center align-middle">1</td>
                                    <td class="align-middle">UB001</td>
                                    <td class="align-middle">PH</td>
                                    <td class="align-middle">EKN</td>
                                    <td class="align-middle">A</td>
                                    <td class="align-middle">17 Jan 2022 sd 17 Jan 2022</td>
                                    <td class="text-center">
                                        <button class="btn btn-primary btn-sm" onclick="backup(0)">REKAP NILAI</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="overlay d-none" id="loading-atas">
                    <div class="spinner-grow"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var idJadwal = '';
        var isSelected = 0;

        function getDetailJadwal(idJadwal) {
            $('#loading').removeClass('d-none');
            $.ajax({
                type: "GET",
                url: base_url + "cbtstatus/getjadwalujianbyjadwal?id_jadwal=" + idJadwal,
                cache: false,
                success: function(response) {
                    $('#loading').addClass('d-none');
                    console.log(response);
                    var selKelas = $('#kelas');
                    selKelas.html('');
                    selKelas.append('<option value="">Pilih Kelas</option>');
                    $.each(response, function(k, v) {
                        if (v != null) {
                            selKelas.append('<option value="' + k + '">' + v + '</option>');
                        }
                    });
                }
            });
        }

        $(document).ready(function() {
            var opsiJadwal = $("#jadwal");
            var opsiThn = $("#tahun");

            var selected = isSelected === 0 ? "selected='selected'" : "";
            opsiJadwal.prepend("<option value='' " + selected + ">Pilih Jadwal</option>");

            //opsiKelas.prepend("<option value='' "+selected+">Pilih Kelas</option>");

            function loadSoal(jadwal, thn, smt) {
                var empty = jadwal === '';
                if (!empty) {
                    $('#loading').removeClass('d-none');
                    window.location.href = base_url + 'cbtanalisis?jadwal=' + jadwal + '&thn=' + thn + '&smt=' +
                        smt;
                } else {
                    console.log('empty')
                }
            }

            function loadJadwalTahun(thn, smt) {
                $('#loading').removeClass('d-none');
                window.location.href = base_url + "cbtanalisis?&thn=" + thn + "&smt=" + smt;
            }

            opsiSmt.change(function() {
                loadJadwalTahun(opsiThn.val(), $(this).val());
            });

            opsiThn.change(function() {
                loadJadwalTahun($(this).val(), opsiSmt.val());
            });

            opsiJadwal.change(function() {
                console.log($(this).val());
                loadSoal($(this).val(), opsiThn.val(), opsiSmt.val())
                //getDetailJadwal($(this).val(), opsiThn.val(), opsiSmt.val());
            });

            $('#kalkulasi').click(function() {
                console.log('test', base_url + "cbtanalisis/kalkulasi?jadwal=" + opsiJadwal.val());
                $.ajax({
                    url: base_url + "cbtanalisis/kalkulasi?jadwal=" + opsiJadwal.val(),
                    type: "GET",
                    success: function(data) {
                        window.location.reload();
                    },
                    error: function(xhr, status, error) {
                        console.log("error", xhr.responseText);
                    }
                });
            });
        });
    </script>
@endsection
