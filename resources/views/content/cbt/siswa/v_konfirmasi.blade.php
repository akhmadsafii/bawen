@extends('content.cbt.siswa.page.main')
@section('content')
    <div class="row h-100 justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="card my-shadow">
                <div class="card-body">
                    <h3 class="text-center">KONFIRMASI</h3>
                    <h5 class="text-center">
                        <b>{{ $jadwal['kode_bank'] }} | {{ $tahun['tahun_ajaran'] }} | {{ $tahun['semester'] }}</b>
                    </h5>
                    <br>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item p-1"> Mata Pelajaran
                            <span class="float-right"><b>{{ $jadwal['mapel'] }}</b></span>
                        </li>
                        <li class="list-group-item p-1"> Guru
                            @php
                                $guru = [];
                                foreach ($jadwal['guru_bank'] as $gr) {
                                    $guru[] = $gr['nama'];
                                }
                            @endphp
                            <span class="float-right"><b>{{ ucwords(implode(', ', $guru)) }}</b></span>
                        </li>
                        <li class="list-group-item p-1"> Pengawas
                            @php
                                $pengawas = [];
                                foreach ($jadwal['pengawas_guru'] as $pg) {
                                    $pengawas[] = $pg['nama'];
                                }
                            @endphp
                            <span class="float-right"><b>{{ ucwords(implode(', ', $pengawas)) }}</b></span>
                        </li>
                        <li class="list-group-item p-1"> Kelas
                            <span class="float-right"><b>{{ $rombel['nama'] }}</b></span>
                        </li>
                        <li class="list-group-item p-1"> Durasi Waktu
                            <span class="float-right"><b>{{ $jadwal['durasi_ujian'] }} Menit</b></span>
                        </li>
                        <li class="list-group-item p-1"> Jumlah Soal
                            <span class="float-right">
                                <b>{{ $jadwal['jumlah_soal'] }}</b>
                            </span>
                        </li>
                        <li class="list-group-item p-1"> Acak Soal
                            <span class="float-right">
                                <b>{{ $jadwal['acak_soal'] == 1 ? 'Aktif' : 'Mati' }}</b>
                            </span>
                        </li>
                        <li class="list-group-item p-1"> Tampilkan Hasil
                            <span class="float-right">
                                <b>{{ $jadwal['hasil_tampil'] == 1 ? 'Ya' : 'Tidak' }}</b>
                            </span>
                        </li>
                        @if ($jadwal['token'] == 1)
                            <li class="list-group-item p-1"><span class="text-danger"><b>Token</b></span>
                                <div class="float-right">
                                    <input type="text" id="input-token" class="text-center" name="token"
                                        placeholder="Masukkan Token">
                                </div>
                            </li>
                        @endif
                    </ul>
                    <br>
                    <form id="proses_cbt">
                        <span class="float-right" data-toggle="tooltip" title="MULAI">
                            <button type="submit" id="mulaiUjian"
                                class="btn btn-{{ $jadwal['token'] == 1 ? 'danger' : 'success' }}"
                                {{ $jadwal['token'] == 1 ? 'disabled' : '' }}>KERJAKAN</button>
                            <input type="hidden" name="token" value="{{ $jadwal['token'] }}">
                            <input type="hidden" name="id_bank" value="{{ $jadwal['id_bank'] }}">
                            <input type="hidden" name="id_jadwal" value="{{ $jadwal['id'] }}">
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            var token = "{{ $jadwal['token'] }}";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if (token == 1) {
                $('#input-token').focus(function() {
                    $("#mulaiUjian").removeAttr('disabled');
                    $("#mulaiUjian").addClass('btn-success');
                    $("#mulaiUjian").removeClass('btn-danger');
                }).blur(function() {
                    if ($(this).val() == '') {
                        $("#mulaiUjian").attr('disabled', 'disabled');
                        $("#mulaiUjian").addClass('btn-danger');
                        $("#mulaiUjian").removeClass('btn-success');
                    }
                })
            }

            $('#proses_cbt').on('submit', function(event) {
                $("#mulaiUjian").attr("disabled", true);
                event.preventDefault();
                let value = $('#input-token').val();
                let status = "{{ $_GET['status'] }}";
                $.ajax({
                    url: "{{ route('cbt-token_konfirmasi') }}",
                    method: "POST",
                    data: $(this).serialize() + '&value=' + value + '&status=' + status,
                    dataType: "json",
                    beforeSend: function() {
                        $('#mulaiUjian').html(
                            '<i class="fa fa-spin fa-sync-alt"></i> Memproses..');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.href = data.url_red
                        } else {
                            $('#mulaiUjian').html('KERJAKAN');
                            $("#mulaiUjian").attr("disabled", false);
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    }
                });
            });

        });
    </script>
@endsection
