@extends('content.cbt.siswa.page.main')
@section('content')
    <div class="row">
        <div class="col-12 col-md-7">
            <div class="card card-success">
                <div class="card-header">
                    <div class="card-title text-white">
                        INFO/PENGUMUMAN
                    </div>
                </div>
                <div class="card-body">
                    <div class="konten-pengumuman">
                        <div id="pengumuman">
                        </div>
                        <p id="loading-post" class="text-center d-none">
                            <br /><i class="fa fa-spin fa-circle-o-notch"></i> Loading....
                        </p>
                        <div id="loadmore-post" onclick="getPosts()" class="text-center mt-4 loadmore d-none">
                            <div class="btn btn-default">Muat Timeline lainnya ...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-5">
            <div class="card card-red">
                <div class="card-header">
                    <div class="card-title text-white">
                        JADWAL HARI INI
                    </div>
                    <div class="card-tools">
                        <button type="button" onclick="loadJadwal()" class="btn btn-sm">
                            <i class="fa fa-sync text-white"></i> <span
                                class="d-none d-sm-inline-block ml-1 text-white">Reload</span>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div id='list-jadwal'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card card-purple">
                <div class="card-header">
                    <div class="card-title text-white">
                        NILAI HASIL ULANGAN/UJIAN
                    </div>
                </div>
                <div class="card-body">
                    <div id="list-cbt">
                        <table class="table table-sm table-hover w-100 ">
                            <tbody>
                                <tr>
                                    <th class="text-center align-middle">NO</th>
                                    <th>Jenis Penilaian</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Kode Penilaian</th>
                                    <th class="text-center">Nilai</th>
                                </tr>
                                @if (!empty($hasil))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($hasil as $hs)
                                        @php
                                            $nilai = '';
                                            if ($hs['nilai'] == 0 || $hs['nilai'] == null) {
                                                $nilai = '-';
                                            } elseif ($hs['koreksi'] == 0) {
                                                $nilai = '*';
                                            } elseif ($hs['nilai'] != 0 && $hs['hasil_tampil'] == 0) {
                                                $nilai = '**';
                                            } else {
                                                $nilai = $hs['nilai'];
                                            }

                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ $no++ }}</td>
                                            <td>Penilaian Harian</td>
                                            <td>{{ $hs['mapel'] }}</td>
                                            <td>{{ $hs['kode_bank'] }}</td>
                                            <td class="text-center">{{ $nilai }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada data saat ini yang tersedia</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <hr>
                        <span><i>Catatan:</i></span>
                        <br>
                        <small>
                            <b>(-)</b> Belum dikerjakan
                            <br><b>(*)</b> Menunggu hasil koreksi
                            <br><b>(**)</b> Hubungi Guru Pengampu jika ingin mengetahui nilai
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
