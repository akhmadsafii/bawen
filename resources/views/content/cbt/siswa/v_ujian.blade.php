@extends('content.cbt.siswa.page.main')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-shadow">
                <div class="card-header">
                    <div class="text-center">INFO ULANGAN/UJIAN</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card border">
                                <div class="card-header border-bottom-0">
                                    <h4 class="card-title mt-1 text-wrap">{{ ucwords($profile['nama']) }}</h4>
                                </div>
                                <div class="card-body pt-0">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item p-1">
                                            Ruang <span
                                                class="float-right"><b>{{ !empty($sesi_siswa) ? $sesi_siswa['ruang'] : '' }}</b></span>
                                        </li>
                                        <li class="list-group-item p-1">
                                            Sesi <span
                                                class="float-right"><b>{{ !empty($sesi_siswa) ? $sesi_siswa['sesi'] : '' }}</b></span>
                                        </li>
                                        <li class="list-group-item p-1">
                                            Dari <span
                                                class="float-right"><b>{{ !empty($sesi_siswa) ? $sesi_siswa['jam_mulai'] : '' }}</b></span>
                                        </li>
                                        <li class="list-group-item p-1">
                                            Sampai <span
                                                class="float-right"><b>{{ !empty($sesi_siswa) ? $sesi_siswa['jam_selesai'] : '' }}</b></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="alert alert-default-danger">
                                <div class="text-center"><b>** INFORMASI PENTING **</b></div>
                                Selama melaksanakan ULANGAN/UJIAN <b>Siswa DILARANG:</b>
                                <ul>
                                    <li>
                                        Meninggalkan ruang ujian tanpa izin pengawas
                                    </li>
                                    <li>
                                        Logout/Keluar dari aplikasi tanpa izin dari pengawas
                                    </li>
                                    <li>
                                        Saling memberitahukan jawaban sesama peserta
                                    </li>
                                    <li>
                                        Membawa makanan dan minuman
                                    </li>
                                    <li>
                                        Membawa handphone ke ruangan ujian
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card my-shadow">
                <div class="card-header">
                    <h5 class="text-center">
                        PENILAIAN HARI INI<br>{{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }} </h5>
                </div>
                <div class="card-body">
                    <div class="row" id="jadwal-content">

                        @if (empty($sesi_siswa))
                            <div class="col-12 alert alert-default-warning">
                                RUANG atau SESI belum terdaftar. <br> Tidak bisa mengerjakan ulangan/ujian. <br> Hubungi
                                Proktor/Admin
                            </div>
                        @else
                            @if (!empty($jadwal))
                                @foreach ($jadwal as $jw)
                                    @if (date('Y-m-d') >= $jw['tgl_mulai'] && date('Y-m-d') <= $jw['tgl_selesai'])
                                        {{-- @if ($jw['tgl_mulai'] >= date('Y-m-d') && $jw['tgl_selesai'] <= date('Y-m-d')) --}}
                                        <div class="jadwal-cbt col-md-6 col-lg-4">
                                            <div class="card border">
                                                <div class="card-header">
                                                    <div class="card-title">
                                                        <b><i class="fa fa-clock-o text-gray mr-1"></i>
                                                            {{ $jw['durasi_ujian'] }} mnt</b>
                                                    </div>
                                                    <div class="card-tools">
                                                        {{ date('H:i', strtotime($jw['sesi_dari'])) }} ~
                                                        {{ date('H:i', strtotime($jw['sesi_selesai'])) }} </div>
                                                </div>
                                                <div class="card-body p-0">

                                                    @php
                                                        $kelas = 'info';
                                                        $close_open = 'buka';
                                                        $ikon = 'arrow-circle-right';
                                                        if ($jw['status_kerjakan'] == 'MENUNGGU') {
                                                            $kelas = 'danger';
                                                            $ikon = 'pause-circle';
                                                        }

                                                        if ($jw['status_kerjakan'] == 'KERJAKAN' || $jw['status_kerjakan'] == 'LANJUTKAN') {
                                                            $kelas = 'success';
                                                            $ikon = 'play-circle';
                                                        }

                                                        if (!empty($sesi_siswa) && $sesi_siswa['jam_selesai'] < date('H:i:s')) {
                                                            $kelas = 'danger';
                                                            $close_open = 'tutup';
                                                            $ikon = 'exclamation-circle';
                                                        }

                                                        if (!empty($sesi_siswa) && $sesi_siswa['jam_mulai'] > date('H:i:s')) {
                                                            $kelas = 'danger';
                                                            $close_open = 'tutup';
                                                            $ikon = 'exclamation-circle';
                                                        }

                                                    @endphp
                                                    <div class="small-box bg-gradient-{{ $kelas }} mb-0">
                                                        <div class="inner">
                                                            <h4><b>{{ $jw['kode_bank'] }}</b></h4>
                                                            <h5>{{ $jw['jenis'] }}</h5>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fas fa-book-open"></i>
                                                        </div>
                                                        <hr class="my-0">
                                                        <a id="status" data-=""
                                                            href="{{ $jw['status_kerjakan'] == 'MENUNGGU' ||
                                                            $jw['status_kerjakan'] == 'SUDAH DIKERJAKAN' ||
                                                            $close_open == 'tutup'
                                                                ? 'javascript:void(0)'
                                                                : route('cbt_siswa-ujian_konfirmasi', [
                                                                    'key' => (new \App\Helpers\Help())->encode($jw['id']),
                                                                    'status' => str_slug($jw['status_kerjakan']),
                                                                ]) }}"
                                                            class="text-white small-box-footer p-2">
                                                            <b>{{ $close_open == 'tutup' ? 'SESI TERTUTUP' : $jw['status_kerjakan'] }}</b><i
                                                                class="fas fa-{{ $ikon }} ml-3"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        {{-- <div class="col-12 alert alert-default-warning">
                                            Tidak ada jadwal Ujian untuk hari ini
                                        </div> --}}
                                    @endif
                                @endforeach
                            @else
                                <div class="col-12 alert alert-default-warning">Tidak ada jadwal penilaian.</div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
