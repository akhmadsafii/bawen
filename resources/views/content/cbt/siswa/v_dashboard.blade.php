@extends('content.cbt.siswa.page.main')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-blue">
                <div class="card-header">
                    <div class="card-title text-white">
                        MENU UTAMA
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-2 col-sm-3 col-4 mb-3">
                            <a href="{{ route('cbt-siswa_ujian') }}">
                                <figure class="text-center">
                                    <img class="img-fluid" src="{{ asset('images/ujian.png') }}" width="80"
                                        height="80" />
                                    <figcaption>Ujian / Ulangan</figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-4 mb-3">
                            <a href="{{ route('cbt_siswa-hasil_ujian') }}">
                                <figure class="text-center">
                                    <img class="img-fluid" src="{{ asset('images/hasil.png') }}" width="80"
                                        height="80" />
                                    <figcaption>Nilai Hasil</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-7">
            <div class="card card-success">
                <div class="card-header">
                    <div class="card-title text-white">
                        INFO/PENGUMUMAN
                    </div>
                </div>
                <div class="card-body">
                    @if (!empty($pengumuman))
                        @foreach ($pengumuman as $pg)
                            <div class="alert alert-icon alert-success border-success alert-dismissible fade show"
                                role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <div class="media">
                                    @if ($pg['file'] != null)
                                        <img class="align-self-center mr-3" src="{{ $pg['file'] }}"
                                            alt="Generic placeholder image" width="150">
                                    @endif
                                    <div class="media-body align-self-center">
                                        <h5 class="mt-0">{{ $pg['judul'] }}</h5>
                                        <p>{{ $pg['isi'] }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-icon alert-warning border-warning alert-dismissible fade show" role="alert">
                            <strong>Maaf !</strong> Untuk saat ini belum ada pemberitahuan untuk anda.
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-md-5">
            <div class="card card-red">
                <div class="card-header">
                    <div class="card-title text-white">
                        JADWAL HARI INI
                    </div>
                    <div class="card-tools">
                        <button type="button" onclick="loadJadwal()" class="btn btn-sm">
                            <i class="fa fa-sync text-white"></i> <span
                                class="d-none d-sm-inline-block ml-1 text-white">Reload</span>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($jadwal as $jw)
                            <div class="col-md-6 col-6" style="min-height: 60px">
                                <a href="{{ route('cbt-siswa_ujian') }}">
                                    <div class="info-box border p-1"
                                        style="min-height: 60px; box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%)">
                                        <div class="info-box-content p-1 text-danger bg-danger">
                                            <center>
                                                <span
                                                    class="info-box-text text-white">{{ strtoupper($jw['kode_bank']) }}</span>
                                                <small>Selesai
                                                    {{ (new \App\Helpers\Help())->getTanggal($jw['tgl_selesai']) }},
                                                    {{ $jw['durasi_ujian'] }}'</small>

                                                <p class="m-0"><b>PENGAWAS</b></p>
                                                <small>
                                                    @if (!empty($jw['pengawas_guru']))
                                                        @php
                                                            $resultstr = [];
                                                        @endphp
                                                        @foreach ($jw['pengawas_guru'] as $pengawas)
                                                            @php
                                                                $resultstr[] = $pengawas['nama'];
                                                            @endphp
                                                        @endforeach
                                                        [{{ implode(', ', $resultstr) }}]
                                                    @else
                                                        [Belum diset]
                                                    @endif
                                                </small>
                                            </center>

                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
