<!DOCTYPE html>
<html>

<head>

    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Penilaian</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="http://localhost/cbt/assets/img/favicon.png" type="image/x-icon">

    <!-- Required CSS -->
    <!-- v3 -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/fontawesome-free/css/fontawesome.min.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/fontawesome-free/css/v4-shims.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/Ionicons/css/ionicons.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/pace-progress/themes/silver/pace-theme-center-circle.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- multi select -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/multiselect/css/multi-select.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/summernote/summernote-bs4.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/app/css/jquery.toast.min.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/toastr/toastr.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/dropify/css/dropify.min.css">

    <!-- Datatables Buttons -->
    <link rel="stylesheet"
        href="http://localhost/cbt//assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    <!-- textarea editor -->
    <!-- summernote -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/plugins/summernote/summernote-bs4.css">
    <!-- /texarea editor; -->

    <!-- fonts -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/adminlte/dist/css/montserrat.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/adminlte/dist/css/scheherazade.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/adminlte/dist/css/fonts.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/adminlte/dist/css/adminlte.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="http://localhost/cbt//assets/app/css/mystyle.css">
    <link rel="stylesheet" href="http://localhost/cbt//assets/app/css/show.toast.css">
    <link rel="stylesheet" type="text/css" href="http://localhost/cbt//assets/plugins/fields-linker/fieldsLinker.css">


    <!-- jQuery -->
    <script src="http://localhost/cbt//assets/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="http://localhost/cbt//assets/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="http://localhost/cbt//assets/plugins/sweetalert2/sweetalert2.min.js"></script>

</head>

<script type="text/javascript">
    let base_url = 'http://localhost/cbt/';
</script>


<body class="layout-top-nav layout-navbar-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-dark navbar-green border-bottom-0">
            <ul class="navbar-nav ml-2 ">
                <li class="nav-item">
                    <a href="http://localhost/cbt/dashboard" type="button" class="btn btn-success">
                        <i class="fas fa-arrow-left mr-2"></i><span class="d-none d-sm-inline-block ml-1">Beranda</span>
                    </a>
                </li>
            </ul>
            <div class="mx-auto text-white text-center" style="line-height: 1">
                <span class="text-lg p-0">cbt</span>
                <br>
                <small>Tahun Pelajaran: 2021/2022 Smt:Ganjil</small>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <button onclick="logout()" class="btn btn-danger btn-outline-light">
                        <span class="d-none d-sm-inline-block mr-2">Logout</span><i class="fas fa-sign-out-alt"></i>
                    </button>
                </li>
            </ul>
        </nav>

        <div class="content-wrapper" style="margin-top: -1px;">
            <div class="sticky">
            </div>
            <section class="content overlap pt-4">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-6">
                            <div class="info-box bg-transparent shadow-none">
                                <img src="http://localhost/cbt//assets/img/garuda_circle.png" width="60" height="60">
                                <div class="info-box-content">
                                    <span class="text-white"
                                        style="font-size: 24pt; line-height: 0.7;"><b>GarudaCBT</b></span>
                                    <span class="text-white">C B T A p p l i c a t i o n</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="float-right mt-2 d-none d-md-inline-block">
                                <div class="float-right ml-4">
                                    <img src="http://localhost/cbt//assets/app/img/ic_graduate.png" width="60"
                                        height="60">
                                </div>
                                <div class="float-left" style="line-height: 1.2">
                                    <span class="text-white"><b>akhmad</b></span>
                                    <br>
                                    <span class="text-white">3243435345</span>
                                    <br>
                                    <span class="text-white">X IPA A</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="card my-shadow">
                                <div class="card-header p-4">
                                    <div class="card-title">
                                        NOMOR:
                                        <div id="nomor-soal" class="btn bg-primary no-hover ml-2 text-lg"></div>
                                    </div>
                                    <div class="card-tools">
                                        <button class="btn btn-outline-danger btn-oval-sm no-hover">
                                            <span class='mr-4 d-none d-md-inline-block'><b>Sisa Waktu</b></span>
                                            <span id="timer"><b>00:00:00</b></span>
                                        </button>
                                        <button data-toggle="modal" data-target="#daftarModal"
                                            class="btn btn-primary btn-oval-sm">
                                            <span class="d-none d-md-inline-block mr-2"><b>Daftar Soal</b></span>
                                            <i class="fa fa-th"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body p-3">
                                    <div class="resize-text mb-3">
                                        <button class="btn btn-outline-primary btn-oval-sm no-hover" id="minus"><i
                                                class="fa fa-minus"></i></button>
                                        <button class="btn btn-outline-primary btn-oval-sm no-hover" id="plus"><i
                                                class="fa fa-plus"></i></button>
                                    </div>
                                    <div style="border: 1px solid; border-color: #D3D3D3">
                                        <div class="konten-soal-jawab">
                                            <div class="row p-2 mb-4 ml-1">
                                                <div id="konten-soal"></div>
                                            </div>
                                            <form action="http://localhost/cbt/jawab" id="jawab" method="post"
                                                accept-charset="utf-8">
                                                <input type="hidden" name="csrf_token"
                                                    value="010eabda10be47d3c3632eee84d1e306" />
                                                <div class="row p-3">
                                                    <div id="konten-jawaban" class="col-12">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-between bd-highlight">
                                        <div class="bd-highlight">
                                            <button class="btn btn-primary btn-oval-sm" id="prev" onclick="prevSoal()">
                                                <i class="fa fa-arrow-circle-left"></i>
                                                <span class="ml-2 d-none d-md-inline-block"><b>Soal
                                                        Sebelumnya</b></span>
                                            </button>
                                        </div>
                                        <div class="bd-highlight">
                                            <button class="btn btn-oval-sm" id="next" onclick="nextSoal()">
                                                <span id="text-next" class="mr-2 d-none d-md-inline-block"></span>
                                            </button>
                                            <!--
                                    <button class="btn btn-success btn-oval-sm d-none" id="finish" onclick="selesai()">
                                        <span class="mr-2 d-none d-md-inline-block"><b>Selesai</b></span>
                                        <i class="fa fa-check-circle"></i>
                                    </button>
                                    -->
                                        </div>
                                    </div>
                                </div>
                                <div class="overlay" id="loading">
                                    <div class="spinner-grow"></div>
                                    <div class="pl-3">MEMUAT SOAL</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog" aria-labelledby="daftarLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="daftarLabel">Daftar Nomor Soal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid" id="konten-modal">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
                    </div>
                </div>
            </div>
        </div>

        <form action="http://localhost/cbt/siswa/penilaian/1" id="up" method="post" accept-charset="utf-8">
            <input type="hidden" name="csrf_token" value="010eabda10be47d3c3632eee84d1e306" />
            <input type="hidden" name="siswa" value="1">
            <input type="hidden" name="jadwal" value="1">
            <input type="hidden" name="bank" value="1">
        </form>
        <script src="http://localhost/cbt//assets/plugins/fields-linker/fieldsLinker.js"></script>
        <script src="http://localhost/cbt//assets/app/js/redirect.js"></script>
        <script>
            history.pushState(null, null, '/cbt/siswa/penilaian/1');
            window.addEventListener('popstate', function(event) {
                loadSoalNomor(1);
            });

            const infoJadwal = JSON.parse(JSON.stringify({
                "id_jadwal": "1",
                "id_tp": "2",
                "id_smt": "1",
                "id_bank": "1",
                "id_jenis": "2",
                "tgl_mulai": "2022-01-11",
                "tgl_selesai": "2022-01-14",
                "durasi_ujian": "200",
                "pengawas": "a:3:{i:0;a:1:{s:4:\"guru\";s:1:\"3\";}i:1;a:1:{s:4:\"guru\";s:1:\"4\";}i:2;a:1:{s:4:\"guru\";N;}}",
                "acak_soal": "1",
                "acak_opsi": "1",
                "hasil_tampil": "0",
                "token": "1",
                "status": "1",
                "ulang": "0",
                "reset_login": "0",
                "rekap": "0",
                "jam_ke": "0",
                "jarak": "0",
                "nama_jenis": "Penilaian Tengah Semester",
                "kode_jenis": "PTS",
                "bank_jenis_id": "0",
                "bank_kode": "soal 1",
                "bank_level": "10",
                "bank_kelas": "a:3:{i:0;a:1:{s:8:\"kelas_id\";s:1:\"1\";}i:1;a:1:{s:8:\"kelas_id\";s:1:\"2\";}i:2;a:1:{s:8:\"kelas_id\";N;}}",
                "bank_mapel_id": "21",
                "bank_jurusan_id": "0",
                "bank_guru_id": "1",
                "bank_nama": "",
                "kkm": "0",
                "jml_soal": "2",
                "tampil_pg": "2",
                "bobot_pg": "1",
                "jml_kompleks": "3",
                "tampil_kompleks": "3",
                "bobot_kompleks": "2",
                "jml_jodohkan": "0",
                "tampil_jodohkan": "0",
                "bobot_jodohkan": "0",
                "jml_isian": "3",
                "tampil_isian": "3",
                "bobot_isian": "1",
                "jml_esai": "3",
                "tampil_esai": "2",
                "bobot_esai": "3",
                "opsi": "4",
                "date": "2022-01-11 11:25:01",
                "soal_agama": "0",
                "deskripsi": null,
                "status_soal": "1",
                "id_mapel": "21",
                "nama_mapel": "Bahasa dan Sastra Inggris",
                "kode": "BSING",
                "kelompok": "C",
                "bobot_p": "0",
                "bobot_k": "0",
                "jenjang": "3",
                "urutan": "0",
                "deletable": "0",
                "urutan_tampil": null,
                "id_guru": "1",
                "nama_guru": "firda",
                "id_jurusan": "0",
                "nama_jurusan": "NON JURUSAN",
                "kode_jurusan": "NON",
                "tahun": "2021\/2022",
                "active": "1",
                "smt": "Ganjil",
                "nama_smt": "I (satu)",
                "total_soal": "10"
            }));
            let elapsed = '0';
            var timer = '0';
            let idDurasi;
            let h, m, s, th, tm, ts;
            var interval = null;

            let nomorSoal = 0;
            let idSoal, idSoalSiswa, jenisSoal, modelSoal, typeSoal;
            let jawabanSiswa, jawabanBaru = null,
                jsonJawaban;
            let nav = 0;
            let soalTerjawab = 0,
                soalTotal = 0;

            $(document).ready(function() {
                $('#jawab').on('submit', function(e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    var siswa = $('#up').find('input[name="siswa"]').val();
                    var bank = $('#up').find('input[name="bank"]').val();

                    $.ajax({
                        url: base_url + 'siswa/savejawaban',
                        method: 'POST',
                        data: $(this).serialize() + '&siswa=' + siswa + '&bank=' + bank + '&data=' +
                            JSON.stringify(jsonJawaban),
                        success: function(response) {
                            soalTerjawab = response.soal_terjawab;
                            loadSoalNomor(nav)
                            //console.log(response);
                        },
                        error: function(xhr, error, status) {
                            console.log(xhr.responseText);
                        }
                    });
                });

                $("#plus").click(function() {
                    $(".konten-soal-jawab").find('*').each(function() {
                        var size = parseInt($(this).css("font-size"));
                        if (size < 30) {
                            size = (size + 1) + "px";
                            $(this).css({
                                'font-size': size
                            });
                        }
                    });
                });

                $("#minus").click(function() {
                    $(".konten-soal-jawab").find('*').each(function() {
                        var size = parseInt($(this).css("font-size"));
                        if (size > 12) {
                            size = (size - 1) + "px";
                            $(this).css({
                                'font-size': size
                            });
                        }
                    });
                });

                loadSoalNomor(1)
            });

            function loadSoal(datas) {
                $('#daftarModal').modal('hide').data('bs.modal', null);
                $('#daftarModal').on('hidden', function() {
                    $(this).data('modal', null);
                });

                nav = $(datas).data('nomorsoal');
                var jwb1 = jawabanSiswa;
                var jwb2 = jawabanBaru;
                if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                    jwb1 = JSON.stringify(jwb1)
                }
                if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                    jwb2 = JSON.stringify(jwb2)
                }

                if (jawabanBaru != null && jwb1 !== jwb2) {
                    $('#jawab').submit();
                } else {
                    loadSoalNomor(nav);
                }
            }

            function loadSoalNomor(nomor) {
                if (nomor == nomorSoal) {
                    return;
                }

                if (soalTotal === 0 || nomor <= parseInt(soalTotal)) {
                    if (interval != null) clearInterval(interval);
                    //$('#loading').removeClass('d-none');
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'siswa/loadsoal',
                        data: $('#up').serialize() + '&nomor=' + nomor + '&elapsed=' + elapsed + '&timer=' + timer,
                        success: function(data) {
                            //console.log(data);
                            $('#loading').addClass('d-none');
                            setKonten(data);
                        },
                        error: function(xhr, error, status) {
                            showDangerToast('ERROR!');
                            console.log(xhr.responseText);
                        }
                    });
                } else {
                    selesai();
                }

            }

            function setKonten(data) {
                idSoal = data.soal_id;
                idSoalSiswa = data.soal_siswa_id;
                nomorSoal = parseInt(data.soal_nomor);
                jenisSoal = data.soal_jenis;
                soalTerjawab = data.soal_terjawab;
                soalTotal = data.soal_total;

                idDurasi = data.id_durasi;
                if (data.elapsed == null) {
                    window.location.href = base_url + 'siswa/cbt';
                    return;
                }
                elapsed = data.elapsed == '0' ? "00:00:00" : data.elapsed;

                jsonJawaban = {};
                jawabanBaru = null;
                jawabanSiswa = data.soal_jawaban_siswa != null ? data.soal_jawaban_siswa : '';
                if ($.isArray(jawabanSiswa)) jawabanSiswa.sort();

                if (nomorSoal === 1) {
                    $('#prev').attr('disabled', 'disabled');
                } else {
                    $('#prev').removeAttr('disabled');
                }

                $('#nomor-soal').html(nomorSoal);
                $('#konten-soal').html(data.soal_soal);
                var jenis = data.soal_jenis;
                var html = '';
                if (jenis == "1") {
                    $.each(data.soal_opsi, function(key, opsis) {
                        if (opsis.valAlias != "") {
                            html += '<label class="container-jawaban font-weight-normal">' + opsis.opsi +
                                '<input type="radio"' +
                                ' name="jawaban"' +
                                ' value="' + opsis.value.toUpperCase() + '"' +
                                ' data-jawabansiswa="' + opsis.value.toUpperCase() + '"' +
                                ' data-jawabanalias="' + opsis.valAlias.toUpperCase() + '"' +
                                //' data-nomor="'+nomorSoal+'"' +
                                //' data-jenis="'+data.soal_jenis+'"' +
                                ' onclick="submitJawaban(this)" ' + opsis.checked + '>' +
                                '<span class="checkmark shadow text-center align-middle">' + opsis.valAlias
                                .toUpperCase() + '</span>' +
                                '</label>';
                        }
                    });
                    $('#konten-jawaban').html(html);
                } else if (jenis == "2") {
                    $.each(data.soal_opsi, function(key, opsis) {
                        //console.log(key, opsis);
                        html += '<label class="container-jawaban font-weight-normal">' + opsis.opsi +
                            '<input type="checkbox"' +
                            ' name="jawaban"' +
                            ' value="' + opsis.value.toUpperCase() + '"' +
                            ' data-jawabansiswa="' + opsis.value.toUpperCase() + '"' +
                            ' onclick="submitJawaban(this)" ' + opsis.checked + '>' +
                            //'<i class="fa fa-2x icon-checkbox"></i>' +
                            '<span class="boxmark"></span>' +
                            '</label>';
                    });
                    $('#konten-jawaban').html(html);
                } else if (jenis == "3") {
                    modelSoal = data.soal_opsi.model;
                    typeSoal = data.soal_opsi.type;
                    if (data.soal_opsi.model == '1') {
                        var datalist = convertTableToList(data.soal_opsi);
                        html = '<div class="bonds" id="original" style="display:block;"></div>';
                        $('#konten-jawaban').html(html);
                        var mode = datalist.type == '2' ? "oneToOne" : "manyToMany";
                        var inputs = {
                            "localization": {},
                            "options": {
                                "associationMode": mode, // oneToOne,manyToMany
                                "lineStyle": "square-ends",
                                "buttonErase": false, //"Batalkan",
                                "displayMode": "original",
                                "whiteSpace": 'normal', //normal,nowrap,pre,pre-wrap,pre-line,break-spaces default => nowrap
                                "mobileClickIt": true
                            },
                            "Lists": [{
                                    "name": "baris-kiri" + data.soal_nomor_asli,
                                    "list": datalist.jawaban[0]
                                },
                                {
                                    "name": "baris-kanan" + data.soal_nomor_asli,
                                    "list": datalist.jawaban[1],
                                    //"mandatories": ["last_name", "email_adress"]
                                }
                            ],
                            "existingLinks": datalist.linked
                        };
                        //console.log('no-soal', nomor_soal);

                        fieldLinks = $("#original").fieldsLinker("init", inputs);

                        $(`ul[data-col="baris-kanan${data.soal_nomor_asli}"] li`).click(function(e) {
                            submitJawaban(null);
                        });
                    } else {
                        html += '<table id="table-jodohkan" class="table table-sm table-bordered" data-type="' + data.soal_opsi
                            .type + '">';
                        html += '<tr class="text-center">';
                        $.each(data.soal_opsi.thead, function(key, val) {
                            if (key === 0) {
                                html += '<th class="text-white">' + val + '</th>';
                            } else {
                                html += '<th class="text-center">' + val + '</th>';
                            }
                        });
                        html += '</tr>';
                        $.each(data.soal_opsi.tbody, function(k, v) {
                            html += '<tr class="text-center">';
                            $.each(v, function(t, i) {
                                if (t === 0) {
                                    html += '<td class="baris text-bold">' + i + '</td>';
                                } else {
                                    const checked = i == '1' ? ' checked' : '';
                                    const type = data.soal_opsi.type != '2' ? 'checkbox' : 'radio';
                                    html += '<td>' +
                                        '<input class="check" type="' + type + '" name="check' + k +
                                        '" style="height: 20px; width: 20px"' + checked + '>' +
                                        '</td>';
                                }
                            });
                            html += '</tr>';
                        });
                        html += '</table>';
                        $('#konten-jawaban').html(html);
                    }
                } else if (jenis == "4") {
                    html += '<div class="pr-4">' +
                        '<span class="">JAWABAN:</span><br>' +
                        '<input id="jawaban-essai" class="pl-1" type="text"' +
                        ' name="jawaban" value="' + jawabanSiswa + '"' +
                        ' placeholder="Tulis jawaban disini"/><br>' +
                        '</div>';
                    $('#konten-jawaban').html(html);
                } else {
                    html += '<div class="pr-4">' +
                        '<label>JAWABAN:</label><br>' +
                        '<textarea id="jawaban-essai" class="w-100 pl-1" type="text"' +
                        ' name="jawaban" rows="4"' +
                        ' placeholder="Tulis jawaban disini">' + jawabanSiswa + '</textarea><br>' +
                        '</div>';
                    $('#konten-jawaban').html(html);
                }

                $('#konten-modal').html(data.soal_modal);

                var $imgs = $('.konten-soal-jawab').find('img');
                $.each($imgs, function() {
                    $(this).addClass('img-zoom');
                    var curSrc = $(this).attr('src');
                    if (curSrc.indexOf("http") === -1 && curSrc.indexOf("data:image") === -1) {
                        $(this).attr('src', base_url + curSrc);
                    } else if (curSrc.indexOf(base_url) === -1) {
                        var pathUpload = 'uploads';
                        var forReplace = curSrc.split(pathUpload);
                        $(this).attr('src', base_url + pathUpload + forReplace[1]);
                    }
                    //$(this).wrap('<span style="display:inline-block"></span>').css('display', 'block').parent().zoom();
                });


                var next = $('#next');
                next.removeAttr('disabled');
                var txtnext = $('#text-next');
                $('#ic-btn').remove();
                if (soalTotal === nomorSoal) {
                    next.removeClass('btn-primary');
                    next.addClass('btn-success');
                    next.append('<i id="ic-btn" class="fa fa-check-circle"></i>');
                    txtnext.html('<b>Selesai</b>');
                    //.$('#finish').removeClass('d-none');
                } else {
                    next.removeClass('btn-success');
                    next.addClass('btn-primary');
                    next.append('<i id="ic-btn" class="fa fa-arrow-circle-right"></i>');
                    txtnext.html('<b>Soal Berikutnya</b>')
                    //$('#next').removeClass('d-none');
                    //$('#finish').addClass('d-none');
                }

                $('.check').change(function() {
                    submitJawaban(null);
                });

                $("#jawaban-essai").on('change keyup paste', function() {
                    submitJawaban(null);
                });

                var ss = elapsed.split(":");
                h = parseInt(ss[0]);
                m = parseInt(ss[1]);
                s = parseInt(ss[2]);

                timer = data.timer;
                var tt = timer.split(":");
                th = parseInt(tt[0]);
                tm = parseInt(tt[1]);
                ts = parseInt(tt[2]);

                if (!countdown()) {
                    interval = setInterval(countdown, 1000);
                }
            }

            function nextSoal() {
                $('#next').attr('disabled', 'disabled');
                $('#loading').removeClass('d-none');
                nav = (nomorSoal + 1);
                var jwb1 = jawabanSiswa;
                var jwb2 = jawabanBaru;
                if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                    jwb1 = JSON.stringify(jwb1)
                }
                if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                    jwb2 = JSON.stringify(jwb2)
                }

                if (jawabanBaru != null && jwb1 !== jwb2) {
                    $('#jawab').submit();
                } else {
                    loadSoalNomor(nav);
                }
            }

            function prevSoal() {
                $('#prev').attr('disabled', 'disabled');
                $('#loading').removeClass('d-none');
                nav = (nomorSoal - 1);
                var jwb1 = jawabanSiswa;
                var jwb2 = jawabanBaru;
                if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                    jwb1 = JSON.stringify(jwb1)
                }
                if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                    jwb2 = JSON.stringify(jwb2)
                }

                if (jawabanBaru != null && jwb1 !== jwb2) {
                    $('#jawab').submit();
                } else {
                    loadSoalNomor(nav);
                }
            }

            function updateModal(jwb) {
                var badges = $('#konten-modal').find(`#badge${nomorSoal}`);
                var btn = $('#konten-modal').find(`#btn${nomorSoal}`);
                btn.removeClass("btn-outline-secondary");
                btn.addClass("btn-primary");
                if (jenisSoal == 1) {
                    if (badges.length) {
                        $(`#badge${nomorSoal}`).text(jwb)
                    } else {
                        var badge = '<div id="badge' + nomorSoal +
                            '" class="badge badge-pill badge-success border border-dark text-yellow"' +
                            ' style="font-size:12pt; width: 30px; height: 30px; margin-top: -60px; margin-left: 30px;">' +
                            jwb +
                            '</div>';
                        $(`#box${nomorSoal}`).append(badge);
                    }
                } else {
                    if (!badges.length) {
                        var badge = '<div id="badge' + nomorSoal +
                            '" class="badge badge-pill badge-success border border-dark"' +
                            ' style="font-size:12pt; width: 30px; height: 30px; margin-top: -60px; margin-left: 30px;">' +
                            '&check;</div>';
                        $(`#box${nomorSoal}`).append(badge);
                    }
                }
            }

            function submitJawaban(opsi) {
                var jawaban_Siswa = '',
                    jawaban_Alias = '';
                if (jenisSoal == 1) {
                    jawaban_Siswa = $(opsi).data('jawabansiswa');
                    jawaban_Alias = $(opsi).data('jawabanalias');
                } else if (jenisSoal == 2) {
                    var selected = [];
                    $('#konten-jawaban input:checked').each(function() {
                        selected.push($(this).val());
                    });
                    jawaban_Siswa = selected;
                } else if (jenisSoal == 3) {
                    var jawaban_json = modelSoal == '1' ? convertListToTable() : getDataTable();
                    jawaban_Siswa = {};
                    jawaban_Siswa['jawaban'] = jawaban_json;
                    jawaban_Siswa['type'] = typeSoal;
                    jawaban_Siswa['model'] = modelSoal;
                } else {
                    jawaban_Siswa = $('#jawaban-essai').val();
                }

                jawabanBaru = jawaban_Siswa;
                if (jenisSoal == 2) {
                    if ($.isArray(jawabanBaru)) jawabanBaru.sort();
                }

                updateModal(jawaban_Alias);
                jsonJawaban = createJsonJawaban(jawaban_Alias, jawaban_Siswa);
            }

            function createJsonJawaban(jawab_Alias, jawab_Siswa) {
                var siswa = $('#up').find('input[name="siswa"]').val();
                var jadwal = $('#up').find('input[name="jadwal"]').val();
                var bank = $('#up').find('input[name="bank"]').val();

                var item = {};
                item["no_soal_alias"] = nomorSoal;
                item["jawaban_alias"] = jawab_Alias;
                item["jawaban_siswa"] = jawab_Siswa;
                item["jenis"] = jenisSoal;
                item["id_soal"] = idSoal;
                item["id_soal_siswa"] = idSoalSiswa;
                item["id_jadwal"] = jadwal;
                item["id_bank"] = bank;
                item["id_siswa"] = siswa;

                return item;
            }

            function getDataTable() {
                var tbl = $('#table-jodohkan tr').get().map(function(row) {
                    var $tables = [];

                    $(row).find('th').get().map(function(cell) {
                        var klm = $(cell).text().trim();
                        $tables.push(klm == "" ? "#" : klm);
                    });

                    $(row).find('td').get().map(function(cell) {
                        if ($(cell).children('input').length > 0) {
                            $tables.push($(cell).find('input').prop("checked") === true ? "1" : "0");
                        } else {
                            $tables.push($(cell).text().trim())
                        }
                    });

                    return $tables;
                });
                return tbl;
            }

            function selesai() {
                if (soalTotal === soalTerjawab) {
                    swal.fire({
                        title: "Kamu yakin?",
                        text: "Kamu akan menyelesaikan ujian",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Selesaikan!"
                    }).then(result => {
                        if (result.value) {
                            $.ajax({
                                url: base_url + 'siswa/selesaiujian',
                                method: "POST",
                                data: $('#up').serialize(),
                                success: function(respon) {
                                    $('#next').removeAttr('disabled');
                                    $('#loading').addClass('d-none');
                                    //console.log(respon);
                                    if (respon.status) {
                                        window.location.href = base_url + 'siswa/cbt';
                                    } else {
                                        swal.fire({
                                            title: "Gagal",
                                            text: "Tidak bisa menyelesaikan ujian",
                                            icon: "error"
                                        });
                                    }
                                },
                                error: function(xhr, error, status) {
                                    console.log(xhr.responseText);
                                    swal.fire({
                                        title: "Gagal",
                                        text: "Tidak bisa menyelesaikan ujian",
                                        icon: "error"
                                    });
                                }
                            });
                        } else {
                            $('#next').removeAttr('disabled');
                            $('#loading').addClass('d-none');
                        }
                    });
                } else {
                    swal.fire({
                        title: "BELUM SELESAI!",
                        text: "Masih ada soal yang belum dikerjakan",
                        icon: "error",
                        confirmButtonColor: "#3085d6",
                    }).then(result => {
                        if (result.value) {
                            $('#next').removeAttr('disabled');
                            $('#loading').addClass('d-none');
                        }
                    });
                }
            }

            function countdown() {
                getElapsewdTimer();
                ts--;
                if (ts < 0) {
                    ts = 59;
                    tm--;
                }
                if (tm < 0) {
                    tm = 59;
                    th--;
                }

                if (th === 0 && tm === 0 && ts === 0) {
                    clearInterval(interval);
                    $("#timer").html("WAKTU SUDAH HABIS");
                    $('#prev').attr('disabled', 'disabled');
                    $('#next').attr('disabled', 'disabled');

                    var siswa = $('#up').find('input[name="siswa"]').val();
                    var bank = $('#up').find('input[name="bank"]').val();
                    var jadwal = $('#up').find('input[name="jadwal"]').val();

                    $.ajax({
                        url: base_url + 'siswa/savejawaban',
                        method: 'POST',
                        data: $('#jawab').serialize() + '&jadwal=' + jadwal + '&siswa=' + siswa + '&bank=' + bank +
                            '&waktu=habis' + '&data=' + JSON.stringify(jsonJawaban),
                        success: function(response) {
                            console.log('habis', response);
                            $('.konten-soal-jawab').html('');
                            dialogWaktu();
                        },
                        error: function(xhr, error, status) {
                            console.log(xhr.responseText);
                        }
                    });
                    return true;
                }

                var sh = th < 10 ? '0' + th : th;
                var sm = tm < 10 ? '0' + tm : tm;
                var ss = ts < 10 ? '0' + ts : ts;
                timer = sh + ":" + sm + ":" + ss;
                $("#timer").html("<b>" + timer + "</b>");

                return false;
            }

            function dialogWaktu() {
                swal.fire({
                    title: "Sudah Habis",
                    text: "Waktu Ujian sudah habis, hubungi proktor",
                    icon: "warning",
                    allowOutsideClick: false,
                    confirmButtonColor: "#3085d6",
                    confirmButtonText: "OK"
                }).then(result => {
                    if (result.value) {
                        window.location.href = base_url + 'siswa/cbt';
                    }
                });
            }

            function getElapsewdTimer() {
                s++;
                if (s > 59) {
                    s = 0;
                    m++;
                }
                if (m > 59) {
                    m = 0;
                    h++;
                }
                elapsed = (h < 10 ? '0' + h : h) + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s);
            }

            function secondsToHMS(s) {
                var h = Math.floor(s / 3600);
                s -= h * 3600;
                var m = Math.floor(s / 60);
                s -= m * 60;

                return (h < 10 ? '0' + h : h) + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s);
            }

            function convertTableToList(data) {
                var kanan = data.thead;
                console.log('kanan', kanan);
                var kiri = [];
                $.each(data.tbody, function(i, v) {
                    kiri.push(v.shift());
                });
                kanan.shift();

                var linked = [];
                $.each(data.tbody, function(n, arv) {
                    $.each(arv, function(t, v) {
                        if (v == '1') {
                            var it = {};
                            it['from'] = kiri[n];
                            it['to'] = kanan[t];
                            linked.push(it);
                        }
                    });
                });
                var item = {};
                item['type'] = data.type;
                item['jawaban'] = [kiri, kanan];
                item['linked'] = linked;
                console.log('test', item);
                return item;
            }

            function getListData() {
                var kolom = [];
                var baris = [];
                $(".FL-left li").each(function() {
                    baris.push($(this).text());
                });
                $(".FL-right li").each(function() {
                    kolom.push($(this).text());
                });
                return [kolom, baris];
            }

            function convertListToTable() {
                var results = fieldLinks.fieldsLinker("getLinks");
                var links = results.links;
                console.log('linked', links);

                var array = getListData();
                var kolom = array[0];
                console.log('kolom', kolom);
                var arrayres = [];
                $.each(array[1], function(ind, val) {
                    var vv = [];
                    for (let i = 0; i < kolom.length; i++) {
                        var sv = '0';
                        if (links.length > 0) {
                            $.each(links, function(p, isi) {
                                if (isi.from == val) {
                                    if (isi.to == kolom[i]) {
                                        sv = '1';
                                        //console.log('k', isi.from, val);
                                        //console.log('b', isi.to, kolom[i]);
                                    }
                                }
                            });
                        }
                        vv.push(sv);
                    }

                    vv.unshift(val);
                    arrayres.push(vv);
                });
                kolom.unshift('#');
                arrayres.unshift(kolom);
                console.log('aray', arrayres);
                return arrayres;

                //var item = {};
                //item['model'] = $('#model-opsi').val();
                //item['type'] = $('#type-opsi').val();
                //item['jawaban'] = arrayres;
                //return item;
            }
        </script>
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <script src="http://localhost/cbt//assets/app/js/jquery.marquee.min.js"></script>
    <div style="position: -webkit-sticky;
 position: sticky;
 bottom: 0;
 padding: 2px;
 background-color: darkgreen;
 font-size: 16pt;
 color: white;
    height: auto;">
        <div id="running-text-siswa" class="marquee" style="overflow: hidden;"></div>
    </div>

    <footer class="main-footer d-none">
        <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="d-none float-right d-none d-sm-inline-block">
            <b>Version</b> 3.0.5
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Required JS -->
    <!-- v3 -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);

        var runningText = JSON.parse(
            '[{"id_text":"1","text":"Selamat melaksanakan Penilaian Akhir Semester (PAS) tingkat MTs"},{"id_text":"2","text":"Madrasah Aliyah Al Hasan menerima pendaftaran peserta didik baru tahun pelajaran 2021\/2022, Informasi Pendaftaran: Telp\/WA 0123456767809"},{"id_text":"3","text":""},{"id_text":"4","text":""},{"id_text":"5","text":""},{"id_text":"9","text":"Selamat melaksanakan Try Out UMBK tingkat MTs"},{"id_text":"21","text":"Madrasah Aliyah Al Hasan menerima pendaftaran peserta didik baru tahun pelajaran 2021\/2022, Informasi Pendaftaran: Telp\/WA 0134356767879"}]'
            );
        console.log('runn', runningText);
        var teks = '';
        $.each(runningText, function(i, v) {
            teks += '<span class="ml-3 mr-3">' + v.text + '</span> &bull; '
        });

        $('#running-text-siswa').html(teks);

        $('.marquee').marquee({
            duration: 15000,
            //gap in pixels between the tickers
            gap: 20,
            delayBeforeStart: 1,
            direction: 'left',
            //true or false - should the marquee be duplicated to show an effect of continues flow
            duplicated: true
        });
    </script>
    <!-- DataTables -->
    <script src="http://localhost/cbt//assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="http://localhost/cbt//assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="http://localhost/cbt//assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="http://localhost/cbt//assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="http://localhost/cbt//assets/plugins/pace-progress/pace.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="http://localhost/cbt//assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Summernote -->
    <script src="http://localhost/cbt//assets/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="http://localhost/cbt//assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- Toastr -->
    <script src="http://localhost/cbt//assets/plugins/toastr/toastr.min.js"></script>
    <!-- Select2 -->
    <script src="http://localhost/cbt//assets/plugins/select2/js/select2.full.min.js"></script>
    <!-- multi select -->
    <script src="http://localhost/cbt//assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <!-- Bootstrap Switch -->
    <script src="http://localhost/cbt//assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script src="http://localhost/cbt//assets/plugins/dropify/js/dropify.min.js"></script>
    <script src="http://localhost/cbt//assets/app/js/jquery.toast.min.js"></script>

    <!-- TimeAgo -->
    <script src="http://localhost/cbt//assets/plugins/jquery-timeago/jquery.timeago.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="http://localhost/cbt//assets/adminlte/dist/js/adminlte.js"></script>

    <!-- App JS -->
    <script src="http://localhost/cbt//assets/app/js/show.toast.js"></script>
    <script src="http://localhost/cbt//assets/app/js/dashboard_guru.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        function ajaxcsrf() {
            var csrfname = 'csrf_token';
            var csrfhash = '010eabda10be47d3c3632eee84d1e306';
            var csrf = {};
            csrf[csrfname] = csrfhash;
            $.ajaxSetup({
                "data": csrf
            });
        }

        function reload_ajax() {
            table.ajax.reload();
        }

        var initDestroyTimeOutPace = function() {
            var counter = 0;

            var refreshIntervalId = setInterval(function() {
                var progress;

                if (typeof $('.pace-progress').attr('data-progress-text') !== 'undefined') {
                    progress = Number($('.pace-progress').attr('data-progress-text').replace("%", ''));
                }

                if (progress === 99) {
                    counter++;
                }

                if (counter > 50) {
                    clearInterval(refreshIntervalId);
                    Pace.stop();
                }
            }, 100);
        };
        initDestroyTimeOutPace();
    </script>

</body>

</html>
