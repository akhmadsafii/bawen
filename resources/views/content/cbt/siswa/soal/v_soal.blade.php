@extends('content.cbt.siswa.page.main_soal')
@section('content')
    <style>
        button.swal2-confirm.btn.btn-success {
            margin-right: 5px;
        }

    </style>
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="card my-shadow">
            </div>
        </div>
    </div>
    <div class="card my-shadow">
        <div class="card-header p-4">
            <div class="card-title">
                NOMOR:
                <div id="nomor-soal" class="btn bg-primary no-hover ml-2 text-lg"></div>
            </div>
            <div class="card-tools">
                <button class="btn btn-outline-danger btn-oval-sm no-hover">
                    <span class="mr-4 d-none d-md-inline-block"><b>Sisa Waktu</b></span>
                    <span id="timer"><b>00:00:00</b></span>
                </button>
                <button id="listSoal" class="btn btn-primary btn-oval-sm">
                    <span class="d-none d-md-inline-block mr-2"><b>Daftar Soal</b></span>
                    <i class="fa fa-th"></i>
                </button>
            </div>
        </div>
        <div class="card-body p-3">
            <div class="resize-text mb-3">
                <button class="btn btn-outline-primary btn-oval-sm no-hover" id="minus"><i
                        class="fa fa-minus"></i></button>
                <button class="btn btn-outline-primary btn-oval-sm no-hover" id="plus"><i
                        class="fa fa-plus"></i></button>
            </div>
            <div style="border: 1px solid; border-color: #D3D3D3">
                <div class="konten-soal-jawab">
                    <div class="row p-2 mb-4 ml-1">
                        <div id="konten-soal"></div>
                    </div>
                    <form action="javascript:void(0)" id="jawab">
                        <div class="row p-3">
                            <div id="konten-jawaban" class="col-12">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between bd-highlight">
                <div class="bd-highlight">
                    <button class="btn btn-primary btn-oval-sm" id="prev" onclick="prevSoal()">
                        <i class="fa fa-arrow-circle-left"></i>
                        <span class="ml-2 d-none d-md-inline-block"><b>Soal
                                Sebelumnya</b></span>
                    </button>
                </div>
                <div class="bd-highlight">
                    <button class="btn btn-oval-sm" id="next" onclick="nextSoal()">
                        <span id="text-next" class="mr-2 d-none d-md-inline-block"></span>
                    </button>
                </div>
            </div>
        </div>
        {{-- <div class="overlay" id="loading">
            <div class="spinner-grow"></div>
            <div class="pl-3">MEMUAT SOAL</div>
        </div> --}}
    </div>

    <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog" aria-labelledby="daftarLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="daftarLabel">Daftar Nomor Soal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid" id="konten-modal">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
                </div>
            </div>
        </div>
    </div>

    <form id="up" method="post" accept-charset="utf-8">
        <input type="hidden" name="siswa" value="{{ session('id_kelas_siswa') }}">
        <input type="hidden" name="jadwal" value="{{ $jadwal['id'] }}">
        <input type="hidden" name="bank" value="{{ $jadwal['id_bank'] }}">
    </form>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        let elapsed = '0';
        var timer = '0';
        let idDurasi;
        let h, m, s, th, tm, ts;
        var interval = null;

        let nomorSoal = 0;
        let idSoal, idSoalSiswa, jenisSoal, modelSoal, typeSoal;
        let jawabanSiswa, jawabanBaru = null,
            jsonJawaban;
        let nav = 0;
        let soalTerjawab = 0,
            soalTotal = 0;


        $(document).ready(function() {
            $('#listSoal').click(function() {
                $('#daftarModal').modal('show');
            });
            $('#jawab').on('submit', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                var siswa = $('#up').find('input[name="siswa"]').val();
                var bank = $('#up').find('input[name="bank"]').val();

                $.ajax({
                    url: "{{ route('cbt-soal_siswa_simpan') }}",
                    method: 'POST',
                    data: $(this).serialize() + '&siswa=' + siswa + '&bank=' + bank + '&data=' +
                        JSON.stringify(jsonJawaban),
                    success: function(response) {
                        // console.log(response);
                        soalTerjawab = response.soal_terjawab;
                        // console.log(soalTerjawab);
                        loadSoalNomor(nav)
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $("#plus").click(function() {
                $(".konten-soal-jawab").find('*').each(function() {
                    var size = parseInt($(this).css("font-size"));
                    if (size < 30) {
                        size = (size + 1) + "px";
                        $(this).css({
                            'font-size': size
                        });
                    }
                });
            });

            $("#minus").click(function() {
                $(".konten-soal-jawab").find('*').each(function() {
                    var size = parseInt($(this).css("font-size"));
                    if (size > 12) {
                        size = (size - 1) + "px";
                        $(this).css({
                            'font-size': size
                        });
                    }
                });
            });

            loadSoalNomor(1)
        });

        function loadSoal(datas) {
            $('#daftarModal').modal('hide').data('bs.modal', null);
            $('#daftarModal').on('hidden', function() {
                $(this).data('modal', null);
            });

            nav = $(datas).data('nomorsoal');
            var jwb1 = jawabanSiswa;
            var jwb2 = jawabanBaru;
            if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                jwb1 = JSON.stringify(jwb1)
            }
            if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                jwb2 = JSON.stringify(jwb2)
            }

            if (jawabanBaru != null && jwb1 !== jwb2) {
                $('#jawab').submit();
            } else {
                loadSoalNomor(nav);
            }
        }

        function loadSoalNomor(nomor) {
            if (nomor == nomorSoal) {
                return;
            }
            //  console.log("soal total = " + soalTotal + " , soal terjawab " + soalTerjawab);


            if (soalTotal === 0 || nomor <= parseInt(soalTotal)) {
                if (interval != null) clearInterval(interval);
                //$('#loading').removeClass('d-none');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt_jawaban-data') }}",
                    data: $('#up').serialize() + '&nomor=' + nomor + '&elapsed=' + elapsed + '&timer=' + timer,
                    success: function(data) {
                        // $('#loading').addClass('d-none');
                        setKonten(data);
                    },
                    error: function(xhr, error, status) {
                        // showDangerToast('ERROR!');
                        console.log(xhr.responseText);
                    }
                });
            } else {
                selesai();
            }

        }

        function setKonten(data) {
            // console.log(data);
            idSoal = data.id_soal;
            idSoalSiswa = data.id;
            nomorSoal = parseInt(data.no_soal_alias);
            jenisSoal = data.tipe;
            soalTerjawab = data.soal_terjawab;
            soalTotal = data.total_soal;
            // console.log(soalTerjawab);

            idDurasi = data.id_durasi;
            if (data.elapsed == null) {
                window.location.href = "{{ route('cbt-siswa_ujian') }}";
                return;
            }
            elapsed = "00:00:00";
            elapsed = data.elapsed == '0' ? "00:00:00" : data.elapsed;

            jsonJawaban = {};
            jawabanBaru = null;
            jawabanSiswa = data.jawaban_siswa != null ? data.jawaban_siswa : '';
            if ($.isArray(jawabanSiswa)) jawabanSiswa.sort();

            if (nomorSoal === 1) {
                $('#prev').attr('disabled', 'disabled');
            } else {
                $('#prev').removeAttr('disabled');
            }

            $('#nomor-soal').html(nomorSoal);
            $('#konten-soal').html(data.soal);
            var jenis = data.tipe;
            var html = '';
            if (jenis == "1") {
                $.each(data.soal_opsi, function(key, opsis) {
                    if (opsis.valAlias != "") {
                        html += '<label class="container-jawaban font-weight-normal">' + opsis.opsi +
                            '<input type="radio"' +
                            ' name="jawaban"' +
                            ' value="' + opsis.value.toUpperCase() + '"' +
                            ' data-jawabansiswa="' + opsis.value.toUpperCase() + '"' +
                            ' data-jawabanalias="' + opsis.valAlias.toUpperCase() + '"' +
                            //' data-nomor="'+nomorSoal+'"' +
                            //' data-jenis="'+data.soal_jenis+'"' +
                            ' onclick="submitJawaban(this)" ' + opsis.checked + '>' +
                            '<span class="checkmark shadow text-center align-middle">' + opsis.valAlias
                            .toUpperCase() + '</span>' +
                            '</label>';
                    }
                });
                $('#konten-jawaban').html(html);
            } else if (jenis == "2") {
                $.each(data.soal_opsi, function(key, opsis) {
                    html += '<label class="container-jawaban font-weight-normal">' + opsis.isian +
                        '<input type="checkbox"' +
                        ' name="jawaban"' +
                        ' value="' + opsis.jawaban.toUpperCase() + '"' +
                        ' data-jawabansiswa="' + opsis.jawaban.toUpperCase() + '"' +
                        ' onclick="submitJawaban(this)" ' + opsis.checked + '>' +
                        //'<i class="fa fa-2x icon-checkbox"></i>' +
                        '<span class="boxmark"></span>' +
                        '</label>';
                });
                $('#konten-jawaban').html(html);
            } else if (jenis == "3") {
                modelSoal = data.soal_opsi.model;
                typeSoal = data.soal_opsi.type;
                if (data.soal_opsi.model == '1') {
                    var datalist = convertTableToList(data.soal_opsi);
                    html = '<div class="bonds" id="original" style="display:block;"></div>';
                    $('#konten-jawaban').html(html);
                    var mode = datalist.type == '2' ? "oneToOne" : "manyToMany";
                    var inputs = {
                        "localization": {},
                        "options": {
                            "associationMode": mode, // oneToOne,manyToMany
                            "lineStyle": "square-ends",
                            "buttonErase": false, //"Batalkan",
                            "displayMode": "original",
                            "whiteSpace": 'normal', //normal,nowrap,pre,pre-wrap,pre-line,break-spaces default => nowrap
                            "mobileClickIt": true
                        },
                        "Lists": [{
                                "name": "baris-kiri" + data.soal_nomor_asli,
                                "list": datalist.jawaban[0]
                            },
                            {
                                "name": "baris-kanan" + data.soal_nomor_asli,
                                "list": datalist.jawaban[1],
                                //"mandatories": ["last_name", "email_adress"]
                            }
                        ],
                        "existingLinks": datalist.linked
                    };

                    fieldLinks = $("#original").fieldsLinker("init", inputs);

                    $(`ul[data-col="baris-kanan${data.soal_nomor_asli}"] li`).click(function(e) {
                        submitJawaban(null);
                    });
                } else {
                    html += '<table id="table-jodohkan" class="table table-sm table-bordered" data-type="' + data.soal_opsi
                        .type + '">';
                    html += '<tr class="text-center">';
                    $.each(data.soal_opsi.thead, function(key, val) {
                        if (key === 0) {
                            html += '<th class="text-white">' + val + '</th>';
                        } else {
                            html += '<th class="text-center">' + val + '</th>';
                        }
                    });
                    html += '</tr>';
                    $.each(data.soal_opsi.tbody, function(k, v) {
                        html += '<tr class="text-center">';
                        $.each(v, function(t, i) {
                            if (t === 0) {
                                html += '<td class="baris text-bold">' + i + '</td>';
                            } else {
                                const checked = i == '1' ? ' checked' : '';
                                const type = data.soal_opsi.type != '2' ? 'checkbox' : 'radio';
                                html += '<td>' +
                                    '<input class="check" type="' + type + '" name="check' + k +
                                    '" style="height: 20px; width: 20px"' + checked + '>' +
                                    '</td>';
                            }
                        });
                        html += '</tr>';
                    });
                    html += '</table>';
                    $('#konten-jawaban').html(html);
                }
            } else if (jenis == "4") {
                html += '<div class="pr-4">' +
                    '<span class="">JAWABAN:</span><br>' +
                    '<input id="jawaban-essai" class="pl-1" type="text"' +
                    ' name="jawaban" value="' + jawabanSiswa + '"' +
                    ' placeholder="Tulis jawaban disini"/><br>' +
                    '</div>';
                $('#konten-jawaban').html(html);
            } else {
                html += '<div class="pr-4">' +
                    '<label>JAWABAN:</label><br>' +
                    '<textarea id="jawaban-essai" class="w-100 pl-1" type="text"' +
                    ' name="jawaban" rows="4"' +
                    ' placeholder="Tulis jawaban disini">' + jawabanSiswa + '</textarea><br>' +
                    '</div>';
                $('#konten-jawaban').html(html);
            }

            $('#konten-modal').html(data.soal_modal);

            // var $imgs = $('.konten-soal-jawab').find('img');
            // $.each($imgs, function() {
            //     $(this).addClass('img-zoom');
            //     var curSrc = $(this).attr('src');
            //     if (curSrc.indexOf("http") === -1 && curSrc.indexOf("data:image") === -1) {
            //         $(this).attr('src', base_url + curSrc);
            //     } else if (curSrc.indexOf(base_url) === -1) {
            //         var pathUpload = 'uploads';
            //         var forReplace = curSrc.split(pathUpload);
            //         $(this).attr('src', base_url + pathUpload + forReplace[1]);
            //     }
            // });


            var next = $('#next');
            next.removeAttr('disabled');
            var txtnext = $('#text-next');
            $('#ic-btn').remove();
            if (soalTotal === nomorSoal) {

                next.removeClass('btn-primary');
                next.addClass('btn-success');
                next.append('<i id="ic-btn" class="fa fa-check-circle"></i>');
                txtnext.html('<b>Selesai</b>');
                //.$('#finish').removeClass('d-none');
            } else {
                next.removeClass('btn-success');
                next.addClass('btn-primary');
                next.append('<i id="ic-btn" class="fa fa-arrow-circle-right"></i>');
                txtnext.html('<b>Soal Berikutnya</b>')
                //$('#next').removeClass('d-none');
                //$('#finish').addClass('d-none');
            }

            $('.check').change(function() {
                submitJawaban(null);
            });

            $("#jawaban-essai").on('change keyup paste', function() {
                submitJawaban(null);
            });

            var ss = elapsed.split(":");
            // console.log(ss);
            h = parseInt(ss[0]);
            m = parseInt(ss[1]);
            s = parseInt(ss[2]);

            timer = data.timer;
            var tt = timer.split(":");
            // console.log(tt);
            th = parseInt(tt[0]);
            tm = parseInt(tt[1]);
            ts = parseInt(tt[2]);

            if (!countdown()) {
                interval = setInterval(countdown, 1000);
            }
        }

        function nextSoal() {
            $('#next').attr('disabled', 'disabled');
            // $('#loading').removeClass('d-none');
            nav = (nomorSoal + 1);
            var jwb1 = jawabanSiswa;
            var jwb2 = jawabanBaru;
            if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                jwb1 = JSON.stringify(jwb1)
            }
            if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                jwb2 = JSON.stringify(jwb2)
            }

            if (jawabanBaru != null && jwb1 !== jwb2) {
                $('#jawab').submit();
            } else {
                loadSoalNomor(nav);
            }
        }

        function prevSoal() {
            $('#prev').attr('disabled', 'disabled');
            $('#loading').removeClass('d-none');
            nav = (nomorSoal - 1);
            var jwb1 = jawabanSiswa;
            var jwb2 = jawabanBaru;
            if ($.isArray(jwb1) || jwb1 instanceof jQuery) {
                jwb1 = JSON.stringify(jwb1)
            }
            if (jwb2 != null && ($.isArray(jwb2) || jwb2 instanceof jQuery)) {
                jwb2 = JSON.stringify(jwb2)
            }

            if (jawabanBaru != null && jwb1 !== jwb2) {
                $('#jawab').submit();
            } else {
                loadSoalNomor(nav);
            }
        }

        function updateModal(jwb) {
            // console.log($(`#box${nomorSoal}`));
            var badges = $('#konten-modal').find(`#badge${nomorSoal}`);
            var btn = $('#konten-modal').find(`#btn${nomorSoal}`);
            btn.removeClass("btn-outline-secondary");
            btn.addClass("btn-primary");
            if (!badges.length) {
                // console.log("jalankan");

                var badge = '<div id="badge' + nomorSoal +
                    '" class="badge badge-pill badge-success border border-dark"' +
                    ' style="font-size:12pt; width: 30px; height: 30px; margin-top: -60px; margin-left: 30px;">' +
                    '&check;</div>';
                $(`#box${nomorSoal}`).html(badge);
            } else {
                console.log("berhenti");
            }
            // soalTerjawab = soalTerjawab + 1;
        }

        function submitJawaban(opsi) {
            var jawaban_Siswa = '',
                jawaban_Alias = '';
            if (jenisSoal == 1) {
                jawaban_Siswa = $(opsi).data('jawabansiswa');
                jawaban_Alias = $(opsi).data('jawabanalias');
            } else if (jenisSoal == 2) {
                var selected = [];
                $('#konten-jawaban input:checked').each(function() {
                    selected.push($(this).val());
                });
                jawaban_Siswa = selected;
            } else if (jenisSoal == 3) {
                var jawaban_json = modelSoal == '1' ? convertListToTable() : getDataTable();
                jawaban_Siswa = {};
                jawaban_Siswa['jawaban'] = jawaban_json;
                jawaban_Siswa['type'] = typeSoal;
                jawaban_Siswa['model'] = modelSoal;
            } else {
                jawaban_Siswa = $('#jawaban-essai').val();
            }

            jawabanBaru = jawaban_Siswa;
            if (jenisSoal == 2) {
                if ($.isArray(jawabanBaru)) jawabanBaru.sort();
            }

            updateModal(jawaban_Alias);
            jsonJawaban = createJsonJawaban(jawaban_Alias, jawaban_Siswa);
        }

        function createJsonJawaban(jawab_Alias, jawab_Siswa) {
            var siswa = $('#up').find('input[name="siswa"]').val();
            var jadwal = $('#up').find('input[name="jadwal"]').val();
            var bank = $('#up').find('input[name="bank"]').val();

            var item = {};
            item["no_soal_alias"] = nomorSoal;
            item["jawaban_alias"] = jawab_Alias;
            item["jawaban_siswa"] = jawab_Siswa;
            item["jenis"] = jenisSoal;
            item["id_soal"] = idSoal;
            item["id_soal_siswa"] = idSoalSiswa;
            item["id_jadwal"] = jadwal;
            item["id_bank"] = bank;
            item["id_siswa"] = siswa;

            return item;
        }

        // function getDataTable() {
        //     var tbl = $('#table-jodohkan tr').get().map(function(row) {
        //         var $tables = [];

        //         $(row).find('th').get().map(function(cell) {
        //             var klm = $(cell).text().trim();
        //             $tables.push(klm == "" ? "#" : klm);
        //         });

        //         $(row).find('td').get().map(function(cell) {
        //             if ($(cell).children('input').length > 0) {
        //                 $tables.push($(cell).find('input').prop("checked") === true ? "1" : "0");
        //             } else {
        //                 $tables.push($(cell).text().trim())
        //             }
        //         });

        //         return $tables;
        //     });
        //     return tbl;
        // }

        function selesai() {
            // console.log("soal total = " + soalTotal + " , soal terjawab " + soalTerjawab);
            if (soalTotal === soalTerjawab) {
                swal({
                    title: "Kamu yakin?",
                    text: "Kamu akan menyelesaikan ujian",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "Selesaikan!",
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(res) {
                    $.ajax({
                        url: "{{ route('cbt_jawaban-selesai_ujian') }}",
                        type: "POST",
                        data: $('#up').serialize(),
                        beforeSend: function() {
                            $('#next').html(
                                '<span id="text-next" class="mr-2 d-none d-md-inline-block"><b>Menyimpan..</b></span><i id="ic-btn" class="fa fa-spin fa-sync-alt"></i>'
                            );
                            $("#next").attr("disabled", true);
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.href = "{{ route('cbt-siswa_ujian') }}";
                            } else {
                                $('#next').html(
                                    '<span id="text-next" class="mr-2 d-none d-md-inline-block"><b>Selesai</b></span><i id="ic-btn" class="fa fa-check-circle"></i>'
                                );
                                $("#next").attr("disabled", true);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        $("#next").attr("disabled", false);
                    } else {
                        console.log('gagal');
                    }
                })
            } else {
                swa("BELUM SELESAI!", "Masih ada soal yang belum dikerjakan", "error");
                $("#next").attr("disabled", false);
            }
        }

        function countdown() {
            getElapsewdTimer();
            ts--;
            if (ts < 0) {
                ts = 59;
                tm--;
            }
            if (tm < 0) {
                tm = 59;
                th--;
            }

            if (th === 0 && tm === 0 && ts === 0) {
                clearInterval(interval);
                $("#timer").html("WAKTU SUDAH HABIS");
                $('#prev').attr('disabled', 'disabled');
                $('#next').attr('disabled', 'disabled');

                var siswa = $('#up').find('input[name="siswa"]').val();
                var bank = $('#up').find('input[name="bank"]').val();
                var jadwal = $('#up').find('input[name="jadwal"]').val();

                $.ajax({
                    url: "{{ route('cbt_jawaban-save') }}",
                    method: 'POST',
                    data: $('#jawab').serialize() + '&jadwal=' + jadwal + '&siswa=' + siswa + '&bank=' + bank +
                        '&waktu=habis' + '&data=' + JSON.stringify(jsonJawaban),
                    success: function(response) {
                        // console.log('habis', response);
                        $('.konten-soal-jawab').html('');
                        dialogWaktu();
                    },
                    error: function(xhr, error, status) {
                        console.log(xhr.responseText);
                    }
                });
                return true;
            }

            var sh = th < 10 ? '0' + th : th;
            var sm = tm < 10 ? '0' + tm : tm;
            var ss = ts < 10 ? '0' + ts : ts;
            timer = sh + ":" + sm + ":" + ss;
            $("#timer").html("<b>" + timer + "</b>");

            return false;
        }

        function dialogWaktu() {
            swal({
                title: "Sudah Habis",
                text: "Waktu Ujian sudah habis, hubungi proktor",
                type: "warning",
                allowOutsideClick: false,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "OK"
            }).then(function() {
                window.location.href = "{{ route('cbt-siswa_ujian') }}";
            })
        }

        function getElapsewdTimer() {
            s++;
            if (s > 59) {
                s = 0;
                m++;
            }
            if (m > 59) {
                m = 0;
                h++;
            }
            elapsed = (h < 10 ? '0' + h : h) + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s);
        }

        function secondsToHMS(s) {
            var h = Math.floor(s / 3600);
            s -= h * 3600;
            var m = Math.floor(s / 60);
            s -= m * 60;

            return (h < 10 ? '0' + h : h) + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s);
        }

        function convertTableToList(data) {
            var kanan = data.thead;
            // console.log('kanan', kanan);
            var kiri = [];
            $.each(data.tbody, function(i, v) {
                kiri.push(v.shift());
            });
            kanan.shift();

            var linked = [];
            $.each(data.tbody, function(n, arv) {
                $.each(arv, function(t, v) {
                    if (v == '1') {
                        var it = {};
                        it['from'] = kiri[n];
                        it['to'] = kanan[t];
                        linked.push(it);
                    }
                });
            });
            var item = {};
            item['type'] = data.type;
            item['jawaban'] = [kiri, kanan];
            item['linked'] = linked;
            // console.log('test', item);
            return item;
        }

        function getListData() {
            var kolom = [];
            var baris = [];
            $(".FL-left li").each(function() {
                baris.push($(this).text());
            });
            $(".FL-right li").each(function() {
                kolom.push($(this).text());
            });
            return [kolom, baris];
        }

        function convertListToTable() {
            var results = fieldLinks.fieldsLinker("getLinks");
            var links = results.links;
            // console.log('linked', links);

            var array = getListData();
            var kolom = array[0];
            // console.log('kolom', kolom);
            var arrayres = [];
            $.each(array[1], function(ind, val) {
                var vv = [];
                for (let i = 0; i < kolom.length; i++) {
                    var sv = '0';
                    if (links.length > 0) {
                        $.each(links, function(p, isi) {
                            if (isi.from == val) {
                                if (isi.to == kolom[i]) {
                                    sv = '1';
                                    //console.log('k', isi.from, val);
                                    //console.log('b', isi.to, kolom[i]);
                                }
                            }
                        });
                    }
                    vv.push(sv);
                }

                vv.unshift(val);
                arrayres.push(vv);
            });
            kolom.unshift('#');
            arrayres.unshift(kolom);
            // console.log('aray', arrayres);
            return arrayres;

            //var item = {};
            //item['model'] = $('#model-opsi').val();
            //item['type'] = $('#type-opsi').val();
            //item['jawaban'] = arrayres;
            //return item;
        }
    </script>
@endsection
