<!DOCTYPE html>
<html>

<head>
    @include('content.cbt.siswa.page.head')
</head>

<body class="layout-top-nav layout-navbar-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-dark navbar-green border-bottom-0">
            <ul class="navbar-nav ml-2">
                @if (request()->segment(4) == 'penilaian' || !empty(request()->segment(3)))
                    <li class="nav-item">
                        <a href="{{ url('program/cbt') }}" type="button" class="btn btn-success">
                            <i class="fas fa-arrow-left mr-2"></i><span class="d-sm-inline-block ml-1">Beranda</span>
                        </a>
                    </li>
                @endif
            </ul>
            <div class="mx-auto text-white text-center" style="line-height: 1">
                <span class="text-lg p-0">{{ strtoupper(session('nama_program')) }}</span>
                <br>
                <small>Tahun Pelajaran: {{ $tahun['tahun_ajaran'] }} Smt: {{ $tahun['semester'] }}</small>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('auth.logout') }}" class="btn btn-danger">
                        <span class="d-none d-sm-inline-block"><i class="fas fa-sign-out-alt"></i> Logout</span>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="margin-top: -1px;">
            <!-- Main content -->
            <div class="sticky">
            </div>
            <section class="content overlap p-4">
                <div class="container">
                    <div class="info-box bg-transparent shadow-none">
                        <img src="{{ $profile['file'] }}" width="120" height="120">
                        <div class="info-box-content">
                            <h5 class="info-box-text text-white text-wrap"><b>{{ ucwords($profile['nama']) }}</b></h5>
                            <span class="info-box-text text-white">{{ $profile['nisn'] }}</span>
                            <span class="info-box-text text-white mb-1">{{ $rmbl['nama'] }}</span>
                        </div>
                    </div>
                    @yield('content')
                </div>
            </section>
        </div>
    </div>
    @include('content.cbt.siswa.page.foot')
</body>

</html>
