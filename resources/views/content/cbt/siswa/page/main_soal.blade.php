<!DOCTYPE html>
<html>

<head>
    @include('content.cbt.siswa.page.head')
</head>

<body class="layout-top-nav layout-navbar-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-dark navbar-green border-bottom-0">
            <ul class="navbar-nav ml-2">
                @if (!empty(request()->segment(3)))
                    <li class="nav-item">
                        <a href="{{ url('program/cbt') }}" type="button" class="btn btn-success">
                            <i class="fas fa-arrow-left mr-2"></i><span class="d-sm-inline-block ml-1">Beranda</span>
                        </a>
                    </li>
                @endif
            </ul>
            <div class="mx-auto text-white text-center" style="line-height: 1">
                <span class="text-lg p-0">{{ strtoupper(session('nama_program')) }}</span>
                <br>
                <small>Tahun Pelajaran: {{ $tahun['tahun_ajaran'] }} Smt: {{ $tahun['semester'] }}</small>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('auth.logout') }}" class="btn btn-danger">
                        <span class="d-none d-sm-inline-block"><i class="fas fa-sign-out-alt"></i> Logout</span>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="margin-top: -1px;">
            <!-- Main content -->
            <div class="sticky">
            </div>
            <section class="content overlap pt-4">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-6">
                            <div class="info-box bg-transparent shadow-none">
                                <img src="{{ session('logo_sekolah') }}" width="60" height="60">
                                <div class="info-box-content">
                                    <span class="text-white"
                                        style="font-size: 24pt; line-height: 0.7;"><b>{{ session('nama_program') }}</b></span>
                                    <span class="text-white">{{ strtoupper(session('sekolah')) }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="float-right mt-2 d-none d-md-inline-block">
                                <div class="float-right ml-4">
                                    <img src="{{ $profile['file'] }}" width="60" height="60">
                                </div>
                                <div class="float-left" style="line-height: 1.2">
                                    <span class="text-white"><b>{{ ucwords($profile['nama']) }}</b></span>
                                    <br>
                                    <span class="text-white">{{ $profile['nis'] }}</span>
                                    <br>
                                    <span class="text-white">{{ $rombel['nama'] }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('content.cbt.siswa.page.foot')
</body>

</html>
