@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="main-wrapper clearfix pt-0">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Horizontal Form with right icons</h5>
                            <p class="text-muted">Use Bootstrap's predefined classes horizontal form</p>
                            <form id="formSetting" action="javascript:void(0)">
                                <div class="form-group row">
                                    <label for="sample4UserName" class="text-sm-right col-sm-2 col-form-label">Memperlihatkan Nilai</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <label class="switch">
                                                <input type="checkbox" name="lihat_nilai" class="fakultas_check" {{ $setting['lihat_nilai'] == 1 ? "checked" : '' }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample4Password" class="text-sm-right col-sm-2 col-form-label">Token</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group">
                                                <label class="switch">
                                                    <input type="checkbox" name="token" {{ $setting['token'] == 1 ? "checked" : '' }}>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample4Password" class="text-sm-right col-sm-2 col-form-label">Jumlah Opsi</label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="opsi_soal" id="opsi_soal" value="{{ $setting['jumlah_opsi'] }}">
                                        </div>
                                    </div>
                                    <label for="sample4Password" class="col-sm-2 col-form-label font-weight-normal">Soal</label>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="sample4Password" class="text-sm-right col-sm-2 col-form-label">Judul Login</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <textarea name="judul" id="judul" class="form-control" rows="3">{{ $setting['judul_login'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample4Password" class="text-sm-right col-sm-2 col-form-label">Nama Aplikasi</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control" value="{{ $setting['nama_aplikasi'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="text-sm-right col-sm-2 col-form-label">Tombol Selesai</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <label class="switch">
                                                <input type="checkbox" name="tombol_selesai" {{ $setting['tombol_selesai'] == 1 ? 'checked' : '' }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="form-group row">
                                        <div class="col-sm-10 ml-auto btn-list">
                                            <button type="submit" class="btn btn-info" id="saveBtn">Submit</button>
                                            <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
   
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formSetting').on('submit', function(event) {
                event.preventDefault();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('cbt-setting_create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        // if (data.status == 'berhasil') {
                        //     $('#formMapel').trigger("reset");
                        //     $('#modalMapel').modal('hide');
                        //     $('#data_mapel').html(data.mapel);
                        // }
                        // $('#saveBtn').html('Simpan');
                        // noti(data.icon, data.message);
                        // $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-mapel_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_mapel').html(data.mapel);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

        })

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddMapel').append(
                '<div class="row mb-1" id="row' + i +
                '"><div class="col-lg-4 mb-1"><div class="form-group mb-1 mt-3"><label for="l30">Kode Mapel</label><input type="text" name="kode_mapel[]" id="kode_mapel" autocomplete="off" class="form-control" required></div></div><div class="col-lg-6 mb-1"><div class="form-group mb-1 mt-3"><label for="l31">Nama Mapel</label><div class="input-group"><input type="text" name="nama[]" autocomplete="off" id="nama" class="form-control" required></div></div></div><div class="col-lg-2 mb-1"><div class="form-group mb-1 mt-3"><label for="l31">Kelompok</label><div class="input-group"><div class="input-group"><select name="kelompok[]" id="kelompok" class="form-control" required><option value="A">A</option><option value="B">B</option></select></div></div></div></div><div class="col-md-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }
    </script>
@endsection
