@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix pt-0">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">

                </div>
            </div>
        </div>
    </main>
    <div class="card card-default my-shadow mb-4" data-select2-id="6">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <form class="form-inline float-right">
                        <div class="form-group my-1">
                            <select name="id_rombel" id="select_rombel" class="form-control">
                                <option value="">Pilih Rombel..</option>
                                @foreach ($jurusan as $jr)
                                    <optgroup label="{{ $jr['nama'] }}">
                                        @foreach ($jr['kelas'] as $kelas)
                                    <optgroup label="{{ $kelas['nama_romawi'] }}">
                                        @foreach ($kelas['rombel'] as $rombel)
                                            <option value="{{ $rombel['id'] }}">{{ $rombel['nama'] }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <button type="button" class="btn btn-purple mx-2 my-1" id="import"><i
                                class="fas fa-file-import"></i>
                            <span class="d-none d-sm-inline-block ml-1">Import</span></button>
                    </form>
                </div>
            </div>

        </div>
        <div class="card-body" data-select2-id="5">
            <div class="row" data-select2-id="4">
                <div class="col-md-6 mb-3" data-select2-id="3">
                    <label>Pilih Kelas Penilaian</label>
                    <select name="kelas[]" id="rombel" class="form-control select2" multiple=""
                        data-placeholder="Pilih Rombel penilaian">
                        @foreach ($jurusan as $jr)
                            <optgroup label="{{ $jr['nama'] }}">
                                @foreach ($jr['kelas'] as $kelas)
                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                @foreach ($kelas['rombel'] as $rombel)
                                    <option value="{{ $rombel['id'] }}">{{ $rombel['nama'] }}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                    <small class="ml-2"><i>Pilih semua kelas yang akan melaksanakan penilaian</i></small>
                </div>
                <div class="col-md-6 d-none" id="generate">
                    <form id="setNomor">
                        <label>Aksi Kelas Terpilih</label>
                        <br>
                        <button type="button" class="btn btn-info" onclick="createNumber()"><i
                                class="fas fa-plus-circle"></i><span class="d-none d-sm-inline-block ml-1">Buat
                                otomatis</span>
                        </button>
                        <button type="button" class="btn btn-danger" onclick="resetNumber()"><i class="fas fa-trash"></i>
                            <span class="d-none d-sm-inline-block ml-1">Hapus</span>
                        </button>
                        <button type="submit" class="btn btn-success">
                            <i class="fas fa-save"></i><span class="d-none d-sm-inline-block ml-1">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
            <div>
                <table class="table table-bordered table-sm mt-4" id="table-nomor">
                    <thead class="alert-primary">
                        <tr>
                            <th class="text-center align-middle" width="40" height="50">No.</th>
                            <th class="text-center align-middle" width="100">USERNAME</th>
                            <th class="text-center align-middle">NAMA</th>
                            <th class="text-center align-middle">KELAS</th>
                            <th class="text-center align-middle">NO. PESERTA UJIAN</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form action="javascript:void(0)" id="importFile" name="importFile" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row  justify-content-center">
                            <div class="col-md-8">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons text-info" style="font-size: 4.5rem;">file_download</i><br>
                                        <b class="text-info">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" />
                                        <br>
                                        <u id="template_import">
                                            <a id="download_template" class="text-info">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        var url_import = "{{ route('cbt_nomor_peserta-import') }}"
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#import').click(function() {
                let rmb = $('#select_rombel').val();
                if (rmb) {
                    $('#importModal').modal('show');
                    $('#file-chosen').html('No file choosen');
                    $('#HeadingImport').html('Import Nomor Peserta');
                    var link = document.getElementById("download_template");
                    link.setAttribute('href', 'nomor_peserta/template?rombel=' + rmb);
                } else {
                    alert('Harap pilih rombel terlebih dulu ^_^');
                }
            });


        });
        var tahun = "{{ $tahun['tahun_ajaran'] }}";

        function sortByName(a, b) {
            var aName = a.name.toLowerCase();
            var bName = b.name.toLowerCase();
            return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        }

        function sortByNumber(a, b) {
            var aName = a.nomor;
            var bName = b.nomor;
            //return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
            return (aName === '') - (bName === '') || +(a > b) || -(a < b);
        }
        //array.sort(SortByName);

        function pad(str, max) {
            console.log(str);
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        var arrayNomor = [];

        function getNomorPeserta(arr) {
            // console.log(arr);
            if (arr.length > 0) {
                setTimeout(function() {
                    // console.log(arrStr);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('cbt_nomor_peserta-show_data') }}",
                        data: {
                            id_rombel: arr
                        },
                        success: function(response) {
                            var newArr = [];
                            $.map(response, function(value, index) {
                                // console.log(value);
                                var item = {};
                                item['id'] = value.id;
                                item['nis'] = value.nis;
                                item['nama'] = value.nama;
                                item['id_rombel'] = value.id_rombel;
                                item['rombel'] = value.rombel;
                                item['nomor'] = !value.no_peserta ? '-' : value.no_peserta;

                                newArr.push(item);
                            });
                            newArr.sort(sortByNumber);
                            arrayNomor = newArr;
                            createPreview();
                        }
                    })
                }, 500);
            } else {
                $('#generate').addClass('d-none');
            }
        }

        function resetNumber() {
            var val = $('#rombel').val();

            if (val.length > 0) {
                console.log(val);
                // $('#loading').removeClass('d-none');
                setTimeout(function() {
                    $.ajax({
                        url: "{{ route('cbt_nomor_peserta-reset_nomor') }}",
                        type: "POST",
                        data: {
                            rombel: val
                        },
                        //data: $(this).serialize() + "&siswa=" + JSON.stringify(arrayNomor),
                        success: function(data) {
                            console.log("response:", data);
                            if (data.status) {
                                getNomorPeserta(val)
                            } else {
                                swa("ERROR!", "Data Tidak Tersimpan", "error");
                            }
                        },
                        error: function(xhr, status, error) {
                            swa("ERROR!", "Data Tidak Tersimpan", "error");
                        }
                    });
                });
            }
        }

        function createNumber() {
            // $('#loading').removeClass('d-none');
            setTimeout(function() {
                if (arrayNomor.length > 0) {
                    //var t = new Date();
                    //var current = t.getFullYear();
                    //t.setFullYear(t.getFullYear() + 1);
                    var thn = tahun.split('/');
                    //console.log(current.toString().substr(-2), t.getFullYear().toString().substr(-2));

                    var newArrNomor = [];
                    var n = 1;
                    $.map(arrayNomor, function(value, index) {
                        console.log(value);
                        var item = {};
                        item['id'] = value.id;
                        item['nis'] = value.nis;
                        item['nama'] = value.nama;
                        item['id_rombel'] = ("0" + value.id_rombel).slice(-2);
                        item['rombel'] = value.rombel;
                        item['nomor'] = thn[0].substr(-2) +
                            thn[1].substr(-2) + '.' +
                            pad(item['id_rombel'], 2) + '.' +
                            pad(n, 3);

                        n++;
                        newArrNomor.push(item);
                    });
                    arrayNomor = newArrNomor;
                    createPreview();
                }
            }, 500);
        }

        function createPreview() {
            $('#generate').removeClass('d-none');
            // console.log(arrayNomor);
            $('#table-nomor').html('');
            var tbody = '<thead class="alert-primary">' +
                '<tr>' +
                '<th class="text-center align-middle" width="40" height="50">No.</th>' +
                '<th class="text-center align-middle" width="100">NIS</th>' +
                '<th class="text-center align-middle">NAMA</th>' +
                '<th class="text-center align-middle">KELAS</th>' +
                '<th class="text-center align-middle">NO. PESERTA UJIAN</th>' +
                '</tr></thead><tbody>';

            for (let i = 0; i < arrayNomor.length; i++) {
                //var nomorPes = data.nomor[data.siswa[i].id_siswa].nomor_peserta;
                tbody += '<tr>' +
                    '<td class="text-center">' + (i + 1) + '</td>' +
                    '<td class="text-center">' + arrayNomor[i].nis + '</td>' +
                    '<td>' + arrayNomor[i].nama.toUpperCase() + '</td>' +
                    '<td class="text-center">' + arrayNomor[i].rombel + '</td>' +
                    '<td class="text-center editable">' + arrayNomor[i].nomor + '</td>' +
                    '</tr>';
            }

            tbody += '</tbody>';
            $('#table-nomor').html(tbody);
            $('#loading').addClass('d-none');

            $('.editable').attr('contentEditable', true);
        }

        $(document).ready(function() {
            var opsiKelas = $("#rombel");
            rombel
            opsiKelas.select2();

            opsiKelas.change(function() {
                var val = $(this).val();
                getNomorPeserta(val);
            });

            $('#setNomor').submit('click', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                if (arrayNomor.length > 0) {
                    //console.log($(this).serialize() + "&siswa=" + JSON.stringify(arrayNomor));
                    $.ajax({
                        url: "{{ route('cbt_nomor_peserta-create') }}",
                        type: "POST",
                        dataType: "JSON",
                        data: $(this).serialize() + "&siswa=" + JSON.stringify(arrayNomor),
                        success: function(data) {
                            swa(data.status + "!", data.message, data.icon);
                        },
                        error: function(xhr, status, error) {
                            swal.fire({
                                title: "ERROR",
                                text: "Data Tidak Tersimpan",
                                icon: "error",
                                showCancelButton: false,
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
