@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row widget-bg">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="row">
                        <div class="col-md-5">
                            <h5 class="box-title">{{ session('title') }}
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun Ajaran</label>
                                    <select name="tahun_ajaran" id="tahun_ajaran" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun Ajaran</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ (new \App\Helpers\Help())->encode($th['id']) }}"
                                                {{ $th['id'] == (new \App\Helpers\Help())->decode($_GET['th']) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword6">Rombel</label>
                                    <select name="id_rombel" id="id_rombel" class="form-control mx-sm-3">
                                        <option value="all" {{ $_GET['rb'] == 'all' ? 'selected' : '' }}>All</option>
                                        @foreach ($jurusan as $jr)
                                            <optgroup label="{{ $jr['nama'] }}">
                                                @foreach ($jr['kelas'] as $kelas)
                                            <optgroup label="{{ $kelas['nama_romawi'] }}">
                                                @foreach ($kelas['rombel'] as $rombel)
                                                    <option value="{{ (new \App\Helpers\Help())->encode($rombel['id']) }}"
                                                        {{ $_GET['rb'] != 'all' && $rombel['id'] == (new \App\Helpers\Help())->decode($_GET['rb']) ? 'selected' : '' }}>
                                                        {{ $rombel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Judul</th>
                                        <th>Rombel</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formPengumuman">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_pengumuman">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="judul" id="judul" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pengumuman</label>
                                    <div class="col-sm-12">
                                        <textarea name="pengumuman" id="pengumuman" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Rombel</label>
                                    <div class="col-sm-12">
                                        <select name="id_rombel[]" id="rombel" class="select2" multiple>
                                            @foreach ($jurusan as $jurus)
                                                <optgroup label="{{ $jurus['nama'] }}">
                                                    @foreach ($jurus['kelas'] as $kls)
                                                <optgroup label="{{ $kls['nama_romawi'] }}">
                                                    @foreach ($kls['rombel'] as $rmb)
                                                        <option value="{{ $rmb['id'] }}"
                                                            {{ $_GET['rb'] != 'all' && $rmb['id'] == (new \App\Helpers\Help())->decode($_GET['rb']) ? 'selected' : '' }}>
                                                            {{ $rmb['nama'] }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">File</label>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group w-100">
                                            </div>
                                            <div class="col-md-9 px-0">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fas fa-sync-alt"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'judul',
                        name: 'judul'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });



            $('#createNewCustomer').click(function() {
                if ($('#tahun_ajaran').val()) {
                    $('#formPengumuman').trigger("reset");
                    $('#modelHeading').html("Tambah Pengumuman");
                    $('.select2').val(null).trigger('change');
                    $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                    $('#ajaxModel').modal('show');
                    $('#action').val('Add');
                } else {
                    alert('Harap Memilih Tahun Ajaran yang lebih spesifik')
                }
            });



            $('body').on('submit', '#formPengumuman', function(e) {
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                e.preventDefault();
                var actionType = $('#saveBtn').val();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('cbt-pengumuman_simpan') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('cbt-pengumuman_update') }}";
                }

                var formData = new FormData(document.getElementById("formPengumuman"));
                formData.append("th", "{{ $_GET['th'] }}");
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formPengumuman').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                if ($('#tahun_ajaran').val()) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('cbt-pengumuman-detail') }}",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $(loader).html('<i class="fas fa-pencil-alt"></i>');
                            $('#modelHeading').html("Edit Pengumuman");
                            $('#id_pengumuman').val(data.id);
                            $('#judul').val(data.judul);
                            $('#pengumuman').val(data.isi);
                            if (data.file) {
                                $('#modal-preview').attr('src', data.file);
                            }
                            $('#rombel').val(data.id_rombel).trigger("change");
                            $('#action').val('Edit');
                            $('#ajaxModel').modal('show');
                        }
                    });
                } else {
                    alert('Harap Memilih Tahun Ajaran yang lebih spesifik')
                }
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-pengumuman-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $("#tahun_ajaran").change(function() {
                loadPage($(this).val(), $("#id_rombel").val())
            });

            $("#id_rombel").change(function() {
                loadPage($("#tahun_ajaran").val(), $(this).val())
            });
        });

        function loadPage(tahun, rombel) {
            var notempty = tahun && rombel;
            if (notempty) {
                window.location.href = "pengumuman?th=" + tahun + "&rb=" + rombel;
            }
        }
    </script>
@endsection
