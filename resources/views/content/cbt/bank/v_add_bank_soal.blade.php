@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <style>
        span.select2-selection.select2-selection--multiple {
            height: 34px !important;
        }

    </style>
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <form id="formBankSoal">
                            <div class="card-header alert-info">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="aksiTombol float-right">
                                            <a href="{{ route('cbt-bank_soal') }}" class="btn btn-danger"><i
                                                    class="fas fa-arrow-circle-left"></i> Kembali</a>
                                            <button type="submit" id="btnSubmit" class="btn btn-info"><i
                                                    class="fas fa-plus-circle"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="validationServer04">Kode Bank Soal</label>
                                        <input type="hidden" name="id" value="{{ !empty($bank) ? $bank['id'] : '' }}">
                                        <input type="text" required name="kode" class="form-control" id="kode"
                                            value="{{ !empty($bank) ? $bank['kode'] : '' }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="validationServer04">Mata Pelajaran</label>
                                        <select required name="mapel" id="mapel" class="form-control">
                                            <option value="">Pilih mapel..</option>
                                            @foreach ($mapel as $mp)
                                                <option value="{{ $mp['id'] }}"
                                                    {{ !empty($bank) && $bank['id_mapel'] == $mp['id'] ? 'selected' : '' }}>
                                                    {{ $mp['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="validationServer04">Guru Pengampu</label>
                                        <select required name="id_guru[]" id="id_guru"
                                            {{ empty($bank) ? 'disabled' : '' }} class="form-control select2" multiple>
                                            <option value="">Pilih Guru..</option>
                                            @if (!empty($bank) && !empty($guru))
                                                @foreach ($guru as $gr)
                                                    <option value="{{ $gr['id_guru'] }}"
                                                        {{ !empty($bank) && $gr['id_guru'] == $bank['id_guru'] ? 'selected' : '' }}>
                                                        {{ $gr['guru'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <small class="text-muted">Pilih mapel terlebih dahulu agar form terbuka</small>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="validationServer04">Jurusan</label>
                                        <select required name="id_jurusan" class="form-control">
                                            <option value="">Pilih Jurusan..</option>
                                            <option value="">Non Jurusan</option>
                                            @foreach ($jurusan as $jr)
                                                <option value="{{ $jr['id'] }}"
                                                    {{ !empty($bank) && $jr['id'] == $bank['id_jurusan'] ? 'selected' : '' }}>
                                                    {{ $jr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="validationServer04">Level/Kelas</label>
                                        <select required name="id_kelas" class="form-control"
                                            {{ empty($bank) ? 'disabled' : '' }}>
                                            <option value="">Pilih Kelas..</option>
                                            @if (!empty($bank) && !empty($kelas))
                                                @foreach ($kelas as $kl)
                                                    <option value="{{ $kl['id'] }}"
                                                        {{ !empty($bank) && $kl['id'] == $bank['id_kelas'] ? 'selected' : '' }}>
                                                        {{ $kl['nama'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <small class="text-muted">Pilih Jurusan terlebih dahulu agar form
                                            terbuka</small>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="validationServer04">Pilih Rombel</label>
                                        <select required name="id_rombel[]" id="rombel"
                                            {{ empty($bank) ? 'disabled' : '' }} class="form-control select2"
                                            multiple="multiple">
                                            <option value="">Pilih Rombel..</option>
                                            @if (!empty($bank) && !empty($rombel))
                                                @foreach ($rombel as $rm)
                                                    @php
                                                        $select = '';
                                                        foreach ($bank['rombels'] as $rmb) {
                                                            if (!empty($bank) && $rm['id'] == $rmb['id']) {
                                                                $select = 'selected';
                                                            }
                                                        }
                                                    @endphp
                                                    <option value="{{ $rm['id'] }}" {{ $select }}>
                                                        {{ $rm['nama'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-7">
                                        <div class="alert alert-danger">
                                            <h5 class="box-title my-0">Soal Pilihan Ganda</h5>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="validationServer04">Jumlah Soal</label>
                                                    <input type="number" name="tampil_pg" class="jumlah form-control"
                                                        value="{{ !empty($bank) ? $bank['jml_pilgan'] : '' }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="validationServer04">Bobot %</label>
                                                    <input type="number" name="bobot_pg" class="bobot form-control"
                                                        value="{{ !empty($bank) ? $bank['bobot_pilgan'] : '' }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="validationServer04">Opsi Jawaban</label>
                                                    <select name="opsi" class="form-control" required>
                                                        <option value="">Pilih jml Opsi..</option>
                                                        <option value="3"
                                                            {{ !empty($bank) && $bank['opsi'] == 3 ? 'selected' : '' }}>3
                                                            (A, B, C)</option>
                                                        <option value="4"
                                                            {{ !empty($bank) && $bank['opsi'] == 4 ? 'selected' : '' }}>4
                                                            (A, B, C, D)</option>
                                                        <option value="5"
                                                            {{ !empty($bank) && $bank['opsi'] == 5 ? 'selected' : '' }}>5
                                                            (A, B, C, D, E)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="alert alert-warning">
                                            <h5 class="box-title my-0">Soal Ganda Kompleks</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Jumlah Soal</label>
                                                    <input type="number" name="tampil_komplek" class="jumlah form-control"
                                                        value="{{ !empty($bank) ? $bank['jml_kompleks'] : '' }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Bobot %</label>
                                                    <input type="number" name="bobot_komplek" class="bobot form-control"
                                                        value="{{ !empty($bank) ? $bank['bobot_kompleks'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="alert alert-success">
                                            <h5 class="box-title my-0">Soal Isian Singkat</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Jumlah Soal</label>
                                                    <input type="number" name="tampil_es" class="form-control jumlah"
                                                        value="{{ !empty($bank) ? $bank['jml_isian'] : '' }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Bobot %</label>
                                                    <input type="number" name="bobot_es" class="form-control bobot"
                                                        value="{{ !empty($bank) ? $bank['bobot_isian'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-secondary">
                                            <h5 class="box-title my-0">Soal Uraian/Essai</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Jumlah Soal</label>
                                                    <input type="number" name="tampil_uraian" class="form-control jumlah"
                                                        value="{{ !empty($bank) ? $bank['jml_essay'] : '' }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="validationServer04">Bobot %</label>
                                                    <input type="number" name="bobot_uraian" class="form-control bobot"
                                                        value="{{ !empty($bank) ? $bank['bobot_essay'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="w-100">
                                                    <tr>
                                                        <th class="text-center">Total Soal</th>
                                                        <th class="text-center">Total Bobot</th>
                                                    </tr>
                                                    <tr class="text-center font-weight-bold">
                                                        <td id="total-soal" class="h4">0</td>
                                                        <td id="total-bobot" class="h4">0</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group my-3">
                                            <label>Status Bank Soal</label>
                                            <select name="status" class="form-control form-control-sm" required="">
                                                <option value="">Pilih Status :</option>
                                                <option value="1" selected="selected">Aktif</option>
                                                <option value="0">Non Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var select2 = $(".select2").select2();
            select2.data('select2').$selection.css('height', '34px');
            // $('.select2').select2();

            $('select[name="mapel"]').on('change', function() {
                var id_mapel = $(this).val();
                if (id_mapel) {
                    $('#id_guru').html('<option value="">Pilih Guru..</option>');
                    $('#id_guru').attr("disabled", true);
                    $.ajax({
                        url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id_mapel
                        },
                        beforeSend: function() {
                            $('#id_guru').html('<option value="">Load guru..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="" disabled>Pilih Guru..</option>';
                            // data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id_guru + '">' + val.guru +
                                    '</option>';
                            })
                            $('#id_guru').attr("disabled", false);
                            $('#id_guru').html(s);
                        }
                    });
                } else {
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })

            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                if (id_jurusan) {
                    $('select[name="id_kelas"]').attr("disabled", true);
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    var action_url = '';

                    if (id_jurusan != 0) {
                        action_url = "{{ route('get-jurusan') }}";
                        method_url = "POST";
                    }

                    if (id_jurusan == 0) {
                        action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                        method_url = "GET";
                    }
                    $.ajax({
                        url: action_url,
                        type: method_url,
                        data: {
                            id_jurusan
                        },
                        beforeSend: function() {
                            $('select[name="id_kelas"]').html(
                                '<option value="">Load kelas..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih kelas..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama +
                                    '</option>';

                            })
                            $('select[name="id_kelas"]').attr("disabled", false);
                            $('select[name="id_kelas"]').html(s)

                        }
                    });
                } else {
                    $('select[name="id_kelas"]').attr("disabled", true);
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })



            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas
                        },
                        beforeSend: function() {
                            $('#rombel').html('<option value="">Load rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="" disabled>Pilih rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#rombel').attr("disabled", false);
                            $('#rombel').html(s);
                        }
                    });
                } else {
                    $('#rombel').html('<option value="">Pilih rombel..</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })

            calculateJumlah();

            $(".jumlah, .bobot").on("keydown keyup", function() {
                calculateJumlah();
            });

            $('#formBankSoal').on('submit', function(event) {
                event.preventDefault();
                let jml = $("#total-bobot").text();
                console.log(jml);
                if (jml != '100') {
                    alert('Jumlah total Bobot harus 100, tidak kurang dan tidak lebih');
                } else {

                }
                $("#btnSubmit").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
                $("#btnSubmit").attr("disabled", true);
                $.ajax({
                    url: "{{ route('cbt-store_bank') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.href = "{{ route('cbt-bank_soal') }}";
                        } else {
                            $('#btnSubmit').html('<i class="fas fa-plus-circle"></i> Simpan');
                            $("#btnSubmit").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSubmit').html('<i class="fas fa-plus-circle"></i> Simpan');
                        $("#btnSubmit").attr("disabled", false);
                    }
                });

            });
        })

        function calculateJumlah() {
            var sum = 0;
            var bobot = 0;
            $(".jumlah").each(function() {
                //tambahkan hanya inputan berupa nomer
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                    $(this).css("background-color", "#FEFFB0");
                } else if (this.value.length != 0) {
                    $(this).css("background-color", "red");
                }
            });
            $(".bobot").each(function() {
                //tambahkan hanya inputan berupa nomer
                if (!isNaN(this.value) && this.value.length != 0) {
                    bobot += parseFloat(this.value);
                    $(this).css("background-color", "#FEFFB0");
                } else if (this.value.length != 0) {
                    $(this).css("background-color", "red");
                }
            });

            $("#total-soal").html(sum);
            $("#total-bobot").html(bobot);
        }
    </script>
@endsection
