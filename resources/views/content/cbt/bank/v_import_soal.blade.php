@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row page-title clearfix border-bottom-0">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">IMPORT BANK SOAL</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('cbt-bank_soal') }}" class="btn btn-danger"><i class="fas fa-arrow-alt-circle-left"></i>
                Kembali</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show mb-0" role="alert">
                <strong>Catatan!</strong> untuk import data dari file excel/word, silahkan download templatenya
                terlebih dahulu.
            </div>
        </div>
    </div>
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card my-shadow mb-4">
                        <div class="card-header">
                            <h5 class="box-title mr-b-0">Upload Soal {{ $bank['mapel'] }} kelas {{ $bank['kelas'] }}
                            </h5>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <a href="{{ (new \App\ApiService\Cbt\BankSoalApi())->download_import($bank['id']) }}"
                                        target="_blank()" class="btn-success btn mb-1 w-100"
                                        download="Template Soal {{ $bank['mapel'] }}">
                                        <i class="fas fa-download"></i><span class="ml-2">Download Template</span>
                                    </a>
                                </div>
                                <div class="col-md-8 mb-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <form id="formImport">
                                                <div class="form-group">
                                                    <input type="file" name="image" accept="*">
                                                    <br><small class="text-muted">Technical information for user</small>
                                                </div>
                                                <input type="hidden" name="id_bank" value="{{ $bank['id'] }}">
                                            </form>
                                        </div>
                                        <div class="col-4">
                                            <button type="button" class="btn btn-info w-100" id="btnUpload"
                                                onclick="importSoal()">
                                                <i class="fas fa-cloud-upload-alt mr-2"></i>Upload
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="file-preview">
                                <div class="alert alert-icon alert-info border-info alert-dismissible fade show"
                                    role="alert"> Sebelum upload, pastikan anda telah mengisi format yang telah disediakan.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                $('#btnUpload').html('<i class="fa fa-spin fa-sync-alt"></i> Mengimport..');
                $.ajax({
                    type: "POST",
                    url: "{{ route('cbt-soal_import') }}",
                    data: new FormData($('#formImport')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#btnUpload').html(
                                '<i class="fas fa-cloud-upload-alt mr-2"></i>Upload');
                        }
                        swa(data.status.toUpperCase() + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html(
                            '<i class="fas fa-cloud-upload-alt mr-2"></i>Upload');
                    }
                });
            });
        })

        function importSoal() {
            $('#formImport').submit();
        }
    </script>
@endsection
