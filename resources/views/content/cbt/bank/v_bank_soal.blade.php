@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6 col-5 my-auto pr-0">
                                    <h5 class="box-title m-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6 col-7 pl-0">
                                    <div class="aksiTombol float-right">
                                        <a href="javascript:void(0)" onclick="reloadL()"
                                            class="btn btn-light text-dark btn-reload my-1"><i class="fas fa-sync-alt"></i>
                                            <span class="d-none d-sm-inline-block ml-1">Reload</span>
                                        </a>
                                        <a href="{{ route('cbt-add_bank') }}" class="btn btn-info my-1"><i
                                                class="fas fa-plus-circle"></i> <span
                                                class="d-none d-sm-inline-block ml-1">Tambah Bank</span></a>
                                        <a href="javascript:void(0)" id="copy" class="btn btn-purple my-1"><i
                                                class="fas fa-copy"></i> <span class="d-none d-sm-inline-block ml-1">Copy
                                                Soal</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <form class="form-inline">
                                            @if (session('role') != 'guru')
                                                <div class="form-group">
                                                    <label for="inputPassword6">Filter</label>
                                                    <select name="f" id="filter" class="form-control mx-sm-3 my-1">
                                                        <option value="0">Semua</option>
                                                        <option value="1">Guru</option>
                                                        <option value="2">Mapel</option>
                                                        <option value="3" selected="selected">Rombel</option>
                                                    </select>
                                                    <div id="select-guru" class="mx-sm-6 d-none my-1">
                                                        <select name="guru" id="guru" class="sel form-control">
                                                            <option value="0">Pilih Guru :</option>
                                                            @foreach ($guru as $gr)
                                                                <option value="{{ $gr['id'] }}">{{ $gr['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div id="select-mapel" class="mx-sm-6 d-none my-1">
                                                        <select name="mapel" id="mapel" class="sel form-control">
                                                            @foreach ($mapel as $mp)
                                                                <option value="{{ $mp['id'] }}">{{ $mp['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div id="select-level" class="mx-sm-6 d-none my-1">
                                                        <select name="level" id="level" class="sel form-control">
                                                            @foreach ($rombel as $rmb)
                                                                <option value="{{ $rmb['id'] }}">{{ $rmb['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <form id="hapus_semua" class="w-100">
                                            <div class="row">
                                                <div class="col-md-5 col-6 my-auto">
                                                    <div class="form-check m-0">
                                                        <input class="form-check-input ml-1" type="checkbox" id="check-all">
                                                        <label class="form-check-label" for="inlineFormCheck">
                                                            Select All
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 col-6">
                                                    <button type="submit" class="btn btn-danger float-right"><i
                                                            class="fas fa-trash"></i> <span
                                                            class="d-none d-sm-inline-block ml-1">Hapus Terpilih</span>
                                                    </button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive" id="konten">
                                        <table class="table table-striped table-bordered mr-b-0 hover">
                                            <thead>
                                                <tr class="bg-info">
                                                    <th class="text-center"></th>
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Kode</th>
                                                    <th class="text-center">Mapel</th>
                                                    <th class="text-center">Kelas</th>
                                                    <th class="text-center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (!empty($soal))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($soal as $s)
                                                        <tr>
                                                            <td>
                                                                <div class="form-check">
                                                                    <input class="form-check-input mx-0 check-bank"
                                                                        type="checkbox" value="{{ $s['id'] }}"
                                                                        name="checked[]">
                                                                </div>
                                                            </td>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $s['kode'] }}</td>
                                                            <td>{{ ucwords($s['mapel']) }}</td>
                                                            <td>
                                                                <b>Kelas : {{ $s['kelas'] }}</b>
                                                                @php
                                                                    $rombel = [];
                                                                    foreach ($s['rombels'] as $r) {
                                                                        $rombel[] = $r['nama'];
                                                                    }
                                                                @endphp
                                                                <p class="m-0">Rombel :
                                                                    {{ implode(', ', $rombel) }}</p>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('cbt-add_bank', ['code' => (new \App\Helpers\Help())->encode($s['id']),'key' => str_slug($s['kode'])]) }}"
                                                                    class="btn btn-info btn-sm my-1"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="{{ route('cbt-bank_import_soal', ['code' => (new \App\Helpers\Help())->encode($s['id']),'key' => str_slug($s['kode'])]) }}"
                                                                    class="btn btn-success btn-sm my-1"><i
                                                                        class="fas fa-file-import"></i>
                                                                    <span class="d-none d-sm-inline-block ml-1">Import
                                                                        Soal</span>
                                                                </a>
                                                                <a href="{{ route('cbt-detail_bank', ['code' => (new \App\Helpers\Help())->encode($s['id']),'key' => str_slug($s['kode'])]) }}"
                                                                    class="btn btn-purple btn-sm my-1"><i
                                                                        class="fas fa-info-circle"></i> <span
                                                                        class="d-none d-sm-inline-block ml-1">Detail</span></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">Bank Soal saat ini tidak
                                                            tersedia</td>
                                                    </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalCopy" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Semua Bank Sola</h5>
                </div>
                <div class="modal-body">
                    <p>2021/2022 SMT Ganjil</p>
                    <table id="tableEkstra"
                        class="w-100 table table-striped table-bordered table-hover table-head-fixed overflow-auto display nowrap"
                        style="height: 300px">
                        <thead>
                            <tr>
                                <th class="text-center align-middle p-0">No.</th>
                                <th>Kode</th>
                                <th>Mapel</th>
                                <th>Kelas</th>
                                <th>TP/SMT</th>
                                <th class="text-center align-middle p-0" style="width: 100px"><span>Aksi</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($soal))
                                @php
                                    $nomer = 1;
                                @endphp
                                @foreach ($soal as $sl)
                                    <tr>
                                        <td>{{ $nomer++ }}</td>
                                        <td>{{ $sl['kode'] }}</td>
                                        <td>{{ $sl['mapel'] }}</td>
                                        <td> <b>Kelas : {{ $sl['kelas'] }}</b>
                                            @php
                                                $rombl = [];
                                                foreach ($sl['rombels'] as $rm) {
                                                    $rombl[] = $rm['nama'];
                                                }
                                            @endphp
                                            <p class="m-0">Rombel :
                                                {{ implode(', ', $rombl) }}</p>
                                        </td>
                                        <td>{{ $sl['tahun_ajaran'] }} SMT {{ $sl['semester'] }}</td>
                                        <td>
                                            <button onclick="copy({{ $sl['id'] }})" type="button"
                                                class="btn btn-sm btn-success"><i class="fa fa-copy"></i> Copy
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Bank soal saat ini belum tersedia</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnJurusan">Simpan</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        var idFilter = '';
        var idMapel = '';
        var idLevel = '';
        var idGuru = '';
        var mode = '1';

        $(document).ready(function() {
            $('#copy').click(function() {
                $('#modalCopy').modal('show');
            });

            var count = $('#konten .check-bank').length;
            if (count == 0) {
                $('#row-filter').addClass('d-none');
            }

            var selectedF = idFilter == '' ? 'selected' : '';
            var selectedM = idMapel == '' ? 'selected' : '';
            var selectedL = idLevel == '' ? 'selected' : '';
            $('#filter').prepend("<option value='' " + selectedF +
                " disabled='disabled'>Filter berdasarkan:</option>");
            $('#mapel').prepend("<option value='' " + selectedM + " disabled='disabled'>Pilih mapel:</option>");
            $('#level').prepend("<option value='' " + selectedL + " disabled='disabled'>Pilih level:</option>");

            function onChangeFilter(type) {
                if (type == '1') {
                    $('#select-guru').removeClass('d-none');
                    $('#select-mapel').addClass('d-none');
                    $('#select-level').addClass('d-none');
                } else if (type == '2') {
                    $('#select-guru').addClass('d-none');
                    $('#select-mapel').removeClass('d-none');
                    $('#select-level').addClass('d-none');
                } else if (type == '3') {
                    $('#select-guru').addClass('d-none');
                    $('#select-mapel').addClass('d-none');
                    $('#select-level').removeClass('d-none');
                } else {
                    $('#select-guru').addClass('d-none');
                    $('#select-mapel').addClass('d-none');
                    $('#select-level').addClass('d-none');
                }
            }

            $('#filter').on('change', function() {
                var type = $(this).val();
                if (type == '0') {
                    window.location.href = 'bank_soal';
                } else {
                    onChangeFilter(type);
                }
            });

            $('.sel').on('change', function() {
                var id = $(this).val();
                window.location.href = 'bank_soal?id=' + id + '&type=' + $('#filter').val();
            });

            onChangeFilter($('#filter').val());

            var unchecked = [];
            var checked = [];

            function findUnchecked() {
                unchecked = [];
                checked = [];
                var count = $('#konten .check-bank').length;

                $("#konten .check-bank:not(:checked)").each(function() {
                    unchecked.push($(this).val());
                });
                $("#konten .check-bank:checked").each(function() {
                    checked.push($(this).val());
                });
                var countChecked = $("#konten .check-bank:checked").length;
                $("#check-all").prop("checked", countChecked == count);

                if (countChecked > 0) {
                    $("#submit-hapus").removeClass('d-none');
                } else {
                    $("#submit-hapus").addClass('d-none');
                }
            }

            $("#konten").on("change", ".check-bank", function() {
                findUnchecked();
            });

            $("#check-all").on("click", function() {
                // alert('check all');
                if (count > 0) {
                    if (this.checked) {
                        $(".check-bank").each(function() {
                            this.checked = true;
                            $("#check-all").prop("checked", true);
                        });
                    } else {
                        $(".check-bank").each(function() {
                            this.checked = false;
                            $("#check-all").prop("checked", false);
                        });
                    }
                    findUnchecked()
                }
            });

            $('#hapus_semua').on('submit', function(e) {
                e.stopPropagation();
                e.preventDefault();
                e.stopImmediatePropagation();

                var dataPost = $(this).serialize() + "&ids=" + JSON.stringify(checked);
                console.log(dataPost);
                if (checked.length == 0) {
                    swa("", "Pilih Bank Soal yang akan dihapus", "warning");
                    return;
                }

                swal({
                    title: "Apa kamu yakin?",
                    text: "Semua Bank Soal yang saat ini ditampilkan akan dihapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-bank_delete_many_soal') }}",
                        type: "POST",
                        data: dataPost,
                        success: function(respon) {
                            if (respon.status == 'berhasil') {
                                window.location.reload();
                            }
                            swa(respon.status.toUpperCase() + "!", respon.message,
                                respon
                                .icon);
                        },
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            })
        });

        function filterBy() {
            console.log('find', $('#filter-by').val());
            $('.card').show();
            var filter = $('#filter-by').val();
            $('#konten').find(".card-body:not(:contains(" + filter + "))").parent().parent().css('display', 'none');
        }

        function hapus(id) {
            swal.fire({
                title: "Anda yakin?",
                text: "Bank Soal akan dihapus!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Hapus!"
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'cbtbanksoal/deleteBank?id_bank=' + id,
                        //data: {id_bank: id},
                        type: "GET",
                        success: function(respon) {
                            if (respon.status) {
                                swal.fire({
                                    title: "Berhasil",
                                    text: "Bank soal berhasil dihapus",
                                    icon: "success"
                                }).then(result => {
                                    if (result.value) {
                                        window.location.reload();
                                        //window.location.href = base_url + 'cbtbanksoal?id=' + idGuru;
                                    }
                                });
                            } else {
                                swal.fire({
                                    title: "Gagal",
                                    text: "Tidak bisa menghapus, " + respon.message,
                                    icon: "error"
                                });
                            }
                        },
                        error: function() {
                            swal.fire({
                                title: "Gagal",
                                text: "Ada data yang sedang digunakan",
                                icon: "error"
                            });
                        }
                    });
                }
            });
        }

        function copy(id) {
            swal({
                title: "Copi Bank Soal?",
                text: "Bank Soal ini akan dicopy",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Copy it',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('cbt-bank_copy_soal') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    success: function(respon) {
                        if (respon.status == 'berhasil') {
                            window.location.reload();
                        }
                        swa(respon.status.toUpperCase() + "!", respon.message,
                            respon
                            .icon);
                    },
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function reloadL() {
            $(".btn-reload").html(
                '<i class="fa fa-spin fa-sync-alt"></i> Reload..');
            window.location.reload();
        }
    </script>
@endsection
