@extends('template.template_default.app')
@section('content')

<style>
    .close{
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .pace{
        display: none;
    }

</style>


<div class="container bg-white p-5">
<!-- header -->
                
                <span>
                    <button type="button" onclick="PublishDocs()" class="btn btn-primary btn-sm">publish</button>
                    <button type="button" onclick="UnpublishDocs()" class="btn btn-secondary btn-sm">unpublish</button>
                </span>

                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Dokumen</h1>
                        <div class="btn-toolbar mb-2 mb-md-0">
                            <div class="btn-group mr-2">
                                <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#buatDocs">Buat +</button>
                            </div>
                        </div>
                    </div>               
<!-- end header -->

        <div class="table-responsive">
            <table class="table table-striped widget-status-table mr-b-0 hover" id="data-kategori">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Sub Kategori</th>
                            <th>Kejuruan</th>
                            <th>File</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>  
                                
                    </tbody>
            </table>
        </div>

<!-- modal create-->
                    <div class="modal fade" id="buatDocs" tabindex="-1" role="dialog" aria-labelledby="buatDocs" aria-hidden="true">
                        <div class="modal-wrapper">
                            <div class="modal-dialog modal-lg d-flex justify-content-center">
                                <div class="modal-content px-2" style="width: 700px;">

                                    <div class="modal-header bg-white d-flex justify-content-center m-0">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h5 class="modal-title pb-0 text-weight-bold" id="modelTitle" style="color: #0073b7; background: #fff"><i class="fa fa-pencil"></i>
                                            Tambahkan Dokumen</h5>
                                    </div>
                                    
                                    <form action="{{ route('buat-Dokumen') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="inputNama" class="col-sm-5 col-form-label">Nama file : </label>
                                                <div id="inputNama" class="col-sm-12">
                                                    <input name="nama" type="text" class="form-control" placeholder="Masukan nama anda">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputNama" class="col-sm-5 col-form-label">Keterangan : </label>
                                                <div id="inputNama" class="col-sm-12">
                                                    <input name="keterangan" type="text" class="form-control" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputNama" class="col-sm-5 col-form-label">Upload file : </label>
                                                <div id="inputNama" class="col-sm-12">
                                                    <input type="file" class="file-uploader pull-left" id="images" name="image" accept="application/pdf">
                                                    
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inputCity">Tanggal buat</label>
                                                    <input type="date" name="tgl_buat" class="form-control" id="inputCity">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputState">Tanggal keluar</label>
                                                    <input type="date" name="tgl_keluar" class="form-control" id="inputCity">
                                                </div>
                                            </div>

                                            <div class="form-row m-2 pt-3" style="width: 100%">
                                                <div class="form-group col-md-4">
                                                    <label for="inputState">Kategori</label>
                                                    <select name="id_kategori" class="form-control">
                                                    @foreach($dataKategori as $Kategori)
                                                    <option value="{{$Kategori['id']}}">{{$Kategori['nama']}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputState">Sub Kategori</label>
                                                    <select name="id_subkategori" class="form-control">
                                                    @foreach($dataSubKategori as $SubKategori)
                                                    <option value="{{$SubKategori['id']}}">{{$SubKategori['nama']}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputState">Kejuruan</label>
                                                    <select name="id_kejuruan" class="form-control">
                                                    @foreach($dataKejuruan as $Kejuruan)
                                                    <option value="{{$Kejuruan['id']}}">{{$Kejuruan['nama']}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="modal-footer pt-0">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                                    class="fa fa-times"></i> Batal</button>
                                            <button type="submit" class="btn btn-info btn-sm pull-right"><i class="fa fa-pencil"></i>
                                                Kirim File
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

<!-- end modal -->


<!-- modal detail -->
      <div class="modal fade bd-example-modal-lg" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail Dokumen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="" id="detailDocs">
                    
                </div>
            </div>

          </div>
        </div>
      </div>
<!-- end modal detail -->

<!-- modal edit-->
        <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="buatDocs" aria-hidden="true">
            <div class="modal-wrapper">
                <div class="modal-dialog modal-lg d-flex justify-content-center">
                    <div class="modal-content px-2" style="width: 700px;">

                        <div class="modal-header bg-white d-flex justify-content-center m-0">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h5 class="modal-title pb-0 text-weight-bold" id="modelTitle" style="color: #0073b7; background: #fff"><i class="fa fa-pencil"></i>
                                            Edit Dokumen</h5>
                        </div>
                        
                        <div class="modal-body">       
                        <form action="{{ route('update-Dokumen') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div id="content-edit">
                                
                            </div>
                            
                                
                                

                        </div>

                            <div class="modal-footer pt-0">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i
                                                    class="fa fa-times"></i> Batal</button>
                                <button type="submit" class="btn btn-info btn-sm pull-right"><i class="fa fa-pencil"></i>
                                                Edit File
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<!-- end modal -->
</div>

<script>

$(function(){

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: true,
    };

    let table = $('#data-kategori').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('unpublish-dokumen') }}",
                "method": "GET"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "50%"
                },
                {
                    data: 'kategori',
                    name: 'kategori',
                    width: "10%"
                },
                {
                    data: 'subkategori',
                    name: 'subkategori'
                    
                },
                {
                    data: 'kejuruan',
                    name: 'kejuruan'
                },
                {
                    data: 'files',
                    name: 'files',
                    width: "20%"
                },
                {
                    data: 'status',
                    name: 'status',
                    width: "20%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ],
            columnDefs: [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": true
            },
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": true
            },
            {
                "targets": [ 4 ],
                "visible": false,
                "searchable": true
            }
            ]
    });

});

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    };

    function Detail(id){
        let data;
        console.log(id);
        $("#modal-detail").modal("show");
        $.ajax({
                type: "GET",
                url : "{{ route('detail-Dokumen') }}",
                data : {
                    id : id
                },
                dataType: 'json',
                success: function(data) {
                    data = data.body.data;
                    console.log(data);
                    $("#detailDocs").html(`
                        <div class="row container d-flex justify-content-center p-0">
                            <div class="col-sm-11">

                                <div class="card-block">
                                    <span class="d-flex justify-content-between align-items-center">
                                        <h4 class="m-b-20 p-b-5 b-b-default f-w-600">Informasi</h4>
                                        
                                            <span>
                                                <a onclick="UnPublish(${data.id})" class="btn btn-primary btn-sm">UnPublish</a>
                                            </span>
                                                                               
                                    </span>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold text-weight-bold">Nama</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.nama}</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Nama File</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.nama_file}</h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Keterangan</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.keterangan}</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Status</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.status}</h6>
                                    </div>
                                    
                                </div>

                                <div class="card-block">
                                    <h4 class="m-b-20 p-b-5 b-b-default f-w-600">Informasi Detail</h4>      
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Author</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.diupload}</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Tanggal Upload</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.tgl_upload}</h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Kategori</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.kategori}</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Sub Kategori</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.subkategori}</h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600 mb-0 text-weight-bold">Kejuruan</p>
                                        <h6 class="text-muted f-w-400 mt-0">${data.kejuruan}</h6>
                                    </div>
                                    
                                </div>




                            </div>
                        </div>
                    `);
                }
        });    
    };

    function Edit(id){
        let datas;
        $("#modal-detail").modal("hide");
        $("#modal-edit").modal("show");
        $.ajax({
                type: "GET",
                url : "{{ route('detail-Dokumen') }}",
                data : {
                    id : id
                },
                dataType: 'json',
                success: function(data) {
                    data = data.body.data;
                    console.log(data);
                    $("#content-edit").html(`
                                <div class="form-group">
                                    <label for="inputNama" class="col-sm-5 col-form-label">Nama file : </label>
                                    <div id="inputNama" class="col-sm-12">
                                        <input name="nama" type="text" class="form-control" placeholder="" value="${data.nama}">
                                        <input name="id" type="hidden" class="form-control" placeholder="" value="${data.id}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputNama" class="col-sm-5 col-form-label">Keterangan : </label>
                                    <div id="inputNama" class="col-sm-12">
                                        <input name="keterangan" type="text" class="form-control" placeholder="" value="${data.keterangan}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputNama" class="col-sm-5 col-form-label">Upload file : </label>
                                    <div id="inputNama" class="col-sm-12">
                                        <input type="file" class="file-uploader pull-left" id="images" name="image" accept="application/pdf" value="${data.file}">
                                                    
                                    </div>
                                </div>

                                <div class="form-row m-2 pt-3" style="width: 100%">
                                    <div class="form-group col-md-4">
                                        <label for="inputCity">Tanggal buat</label>
                                        <input type="date" name="tgl_buat" class="form-control" id="inputCity" value="${data.tgl_awal}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputState">Tanggal keluar</label>
                                        <input type="date" name="tgl_keluar" class="form-control" id="inputCity" value="${data.tgl_akhir}">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputState">Kategori</label>
                                        <select name="id_kategori" class="form-control" value="${data.id_kategori}">
                                        @foreach($dataKategori as $Kategori)
                                        <option value="{{$Kategori['id']}}">{{$Kategori['nama']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputState">Sub Kategori</label>
                                        <select name="id_subkategori" class="form-control" value="${data.id_sub_kategori}">
                                        @foreach($dataSubKategori as $SubKategori)
                                        <option value="{{$SubKategori['id']}}">{{$SubKategori['nama']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputState">Kejuruan</label>
                                        <select name="id_kejuruan" class="form-control" value="${data.id_kejuruan}">
                                        @foreach($dataKejuruan as $Kejuruan)
                                        <option value="{{$Kejuruan['id']}}">{{$Kejuruan['nama']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>

                    `);
                }
        });
    }

    function Publish(id){
        console.log("publish");
        $.ajax({
            type: "POST",
            url : "{{ route('publish-Dokumen') }}",
            data : {
                id : id
            },
            dataType: 'json',
            success : function(data){
                noti(data.icon, data.message);
                $('#buatDocs').modal('hide');
            },
            error : function(data){
                console.log(data);
            }
        })
    }

    function UnPublish(id){
        console.log("unpublish");
        $.ajax({
            type: "POST",
            url : "{{ route('unpublish-Dokumen') }}",
            data : {
                id : id
            },
            dataType: 'json',
            success : function(data){
                noti(data.icon, data.success);
                $('#modal-detail').modal('hide');
            },
            error : function(data){
                console.log(data);
            }
        })
    }

    function PublishDocs(){
        $.ajax({
            type: "GET",
            url : "{{ route('all-dokumen') }}",
            data : {
                id : 1
            },
            dataType: 'json',
            success : function(data){
                console.log(data.data);
                let datas = JSON.stringify(data.data);
                
                $('#data-kategori').DataTable({
                    "data": datas,
                    "deferRender": true,
                    "pageLength": 25,
                    "retrieve": true,
                });
                $('#data-kategori').dataTable().fnDraw(false);
                noti(data.icon, data.success);
                $('#modal-detail').modal('hide');
            },
            error : function(data){
                console.log(data);
            }
        })
    }

    function UnpublishDocs(){
        $.ajax({
            type: "GET",
            url : "{{ route('all-dokumen') }}",
            data : {
                id : 2
            },
            dataType: 'json',
            success : function(data){
                console.log(data.data);
                $('#data-kategori').dataTable().fnDraw(false);
                noti(data.icon, data.success);
                $('#modal-detail').modal('hide');
            },
            error : function(data){
                console.log(data);
            }
        })
    }





</script>

@endsection