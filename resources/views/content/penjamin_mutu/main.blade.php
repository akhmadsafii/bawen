@extends('template.template_default.app')
@section('content')

<style>
	.items{
		width: 29%;
		margin: 0 10px;
		background: rgb(119,177,251);
		background: linear-gradient(139deg, rgba(119,177,251,1) 0%, rgba(158,197,247,1) 49%, rgba(193,220,255,1) 100%);
		border-radius: 10px;
		padding: 10px;
	}

	.detail-content{
		font-family: 'Poppins', sans-serif;
        font-size: 24px;
        color: #103e79;
	}

	thead{
		background: #ffffff !important;
    	color: #1d1c1c;
	}
</style>

<div class="container">

	<div class="container mb-4">
		<h4 class="my-2 title-content ml-3"><i class="fa fa-home fa-lg mr-2" aria-hidden="true"></i>Dashboard</h4>
	</div>

	<div class="row justify-content-center">

		<div class="items">
			<div class="card-body">
			    <h5 class="card-title">Jumlah Dokumen</h5>
			    <h3 class="card-text mt-1"><strong>{{$dataDashboard['dokumen']}}</strong></h3>
			</div>
		</div>

		<div class="items">
			<div class="card-body">
			    <h5 class="card-title">Jumlah Kategori</h5>
			    <h3 class="card-text mt-1"><strong>{{$dataDashboard['kategori']}}</strong></h3>
			</div>
		</div>

		<div class="items">
			<div class="card-body">
			    <h5 class="card-title">Jumlah User</h5>
			    <h3 class="card-text mt-1"><strong>{{$dataDashboard['user']}}</strong></h3>
			</div>
		</div>
	</div>

	<div class="container mt-5 mb-3">
		<h4 class="my-2 detail-content ml-3"><i class="fa fa-file-text-o mr-1" aria-hidden="true"></i>Dokumen</h4>
	</div>

	<div class="container px-4">
		<div class="row px-4">
			<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
				  <th scope="col">Kategori Dokumen</th>
				  <th scope="col">Jumlah</th>
				</tr>
			  </thead>
			  <tbody>
				  @foreach ($dataKategoriDashboard as $data)
				<tr>
				  <th>{{$data['nama']}}</th>
				  <td>{{$data['total']}}</td>
				</tr>
				@endforeach
			  </tbody>
			</table>

		</div>
	</div>
</div>

@endsection