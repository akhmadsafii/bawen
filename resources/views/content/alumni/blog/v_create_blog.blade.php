@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <form action="{{ route('alumni-blog_create') }}"  onsubmit="return loader()" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kategori</label>
                                        <select name="id_kategori" id="id_kategori" class="form-control" required>
                                            <option value="">Pilih Kategori..</option>
                                            @foreach ($kategori as $kt)
                                                <option value="{{ (new \App\Helpers\Help())->encode($kt['id']) }}">
                                                    {{ $kt['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l31">Judul</label>
                                        <div class="input-group">
                                            <input type="text" id="judul" name="judul" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l38">Textarea</label>
                                        <textarea class="form-control editor" id="isi" name="isi" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group mb-1">
                                        <label for="l39">File input</label>
                                        <br>
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <br><small class="text-muted">Digunakan untuk thumbnail Blog</small>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-2 mb-3">
                                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                        class="form-group mb-1" width="100%" style="margin-top: 10px">
                                </div>
                            </div>
                            <div class="form-actions btn-list">
                                <button class="btn btn-info addBtn" type="submit"><i class="fas fa-plus"></i> Tambahkan</button>
                                <a class="btn btn-outline-default btnBack" href="{{ route('alumni-blog') }}" onclick="return kembali()"><i class="fas fa-ban"></i> Batal</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });
        })

        function loader(){
            $(".addBtn").attr("disabled", true);
            $(".addBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Menambah..');
        }
       
        function kembali(){
            $(".btnBack").attr("disabled", true);
            $(".btnBack").html(
                '<i class="fa fa-spin fa-spinner"></i> Kembali..');
        }
    </script>
@endsection
