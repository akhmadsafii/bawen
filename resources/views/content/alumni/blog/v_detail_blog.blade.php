@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .blog-listing {
            padding-top: 30px;
            padding-bottom: 30px;
        }

        .gray-bg {
            background-color: #f5f5f5;
        }

        /* Blog 
                                                                                                                            ---------------------*/
        .blog-grid {
            box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
            border-radius: 5px;
            overflow: hidden;
            background: #ffffff;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .blog-grid .blog-img {
            position: relative;
        }

        .blog-grid .blog-img .date {
            position: absolute;
            background: #fc5356;
            color: #ffffff;
            padding: 8px 15px;
            left: 10px;
            top: 10px;
            border-radius: 4px;
        }

        .blog-grid .blog-img .date span {
            font-size: 22px;
            display: block;
            line-height: 22px;
            font-weight: 700;
        }

        .blog-grid .blog-img .date label {
            font-size: 14px;
            margin: 0;
        }

        .blog-grid .blog-info {
            padding: 20px;
        }

        .blog-grid .blog-info h5 {
            font-size: 22px;
            font-weight: 700;
            margin: 0 0 10px;
        }

        .blog-grid .blog-info h5 a {
            color: #20247b;
        }

        .blog-grid .blog-info p {
            margin: 0;
        }

        .blog-grid .blog-info .btn-bar {
            margin-top: 20px;
        }


        /* Blog Sidebar
                                                                                                                            -------------------*/
        .blog-aside .widget {
            box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
            border-radius: 5px;
            overflow: hidden;
            background: #ffffff;
            margin-top: 15px;
            margin-bottom: 15px;
            width: 100%;
            display: inline-block;
            vertical-align: top;
        }

        .blog-aside .widget-body {
            padding: 15px;
        }

        .blog-aside .widget-title {
            padding: 15px;
            border-bottom: 1px solid #eee;
        }

        .blog-aside .widget-title h3 {
            font-size: 20px;
            font-weight: 700;
            color: #fc5356;
            margin: 0;
        }

        .blog-aside .widget-author .media {
            margin-bottom: 15px;
        }

        .blog-aside .widget-author p {
            font-size: 16px;
            margin: 0;
        }

        .blog-aside .widget-author .avatar {
            width: 70px;
            height: 70px;
            border-radius: 50%;
            overflow: hidden;
        }

        .blog-aside .widget-author h6 {
            font-weight: 600;
            color: #20247b;
            font-size: 22px;
            margin: 0;
            padding-left: 20px;
        }

        .blog-aside .post-aside {
            margin-bottom: 15px;
        }

        .blog-aside .post-aside .post-aside-title h5 {
            margin: 0;
        }

        .blog-aside .post-aside .post-aside-title a {
            font-size: 18px;
            color: #20247b;
            font-weight: 600;
        }

        .blog-aside .post-aside .post-aside-meta {
            padding-bottom: 10px;
        }

        .blog-aside .post-aside .post-aside-meta a {
            color: #6F8BA4;
            font-size: 12px;
            text-transform: uppercase;
            display: inline-block;
            margin-right: 10px;
        }

        .blog-aside .latest-post-aside+.latest-post-aside {
            border-top: 1px solid #eee;
            padding-top: 15px;
            margin-top: 15px;
        }

        .blog-aside .latest-post-aside .lpa-right {
            width: 90px;
        }

        .blog-aside .latest-post-aside .lpa-right img {
            border-radius: 3px;
        }

        .blog-aside .latest-post-aside .lpa-left {
            padding-right: 15px;
        }

        .blog-aside .latest-post-aside .lpa-title h5 {
            margin: 0;
            font-size: 15px;
        }

        .blog-aside .latest-post-aside .lpa-title a {
            color: #20247b;
            font-weight: 600;
        }

        .blog-aside .latest-post-aside .lpa-meta a {
            color: #6F8BA4;
            font-size: 12px;
            text-transform: uppercase;
            display: inline-block;
            margin-right: 10px;
        }

        .tag-cloud a {
            padding: 4px 15px;
            font-size: 13px;
            color: #ffffff;
            background: #20247b;
            border-radius: 3px;
            margin-right: 4px;
            margin-bottom: 4px;
        }

        .tag-cloud a:hover {
            background: #fc5356;
        }

        .blog-single {
            padding-top: 30px;
            padding-bottom: 30px;
        }

        .article {
            box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
            border-radius: 5px;
            overflow: hidden;
            background: #ffffff;
            padding: 15px;
            margin: 15px 0 30px;
        }

        .article .article-title {
            padding: 15px 0 20px;
        }

        .article .article-title h6 {
            font-size: 14px;
            font-weight: 700;
            margin-bottom: 20px;
        }

        .article .article-title h6 a {
            text-transform: uppercase;
            color: #fc5356;
            border-bottom: 1px solid #fc5356;
        }

        .article .article-title h2 {
            color: #20247b;
            font-weight: 600;
        }

        .article .article-title .media {
            padding-top: 15px;
            border-bottom: 1px dashed #ddd;
            padding-bottom: 20px;
        }

        .article .article-title .media .avatar {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            overflow: hidden;
        }

        .article .article-title .media .media-body {
            padding-left: 8px;
        }

        .article .article-title .media .media-body label {
            font-weight: 600;
            color: #fc5356;
            margin: 0;
        }

        .article .article-title .media .media-body span {
            display: block;
            font-size: 12px;
        }

        .article .article-content h1,
        .article .article-content h2,
        .article .article-content h3,
        .article .article-content h4,
        .article .article-content h5,
        .article .article-content h6 {
            color: #20247b;
            font-weight: 600;
            margin-bottom: 15px;
        }

        .article .article-content blockquote {
            max-width: 600px;
            padding: 15px 0 30px 0;
            margin: 0;
        }

        .article .article-content blockquote p {
            font-size: 20px;
            font-weight: 500;
            color: #fc5356;
            margin: 0;
        }

        .article .article-content blockquote .blockquote-footer {
            color: #20247b;
            font-size: 16px;
        }

        .article .article-content blockquote .blockquote-footer cite {
            font-weight: 600;
        }

        .article .tag-cloud {
            padding-top: 10px;
        }

        .article-comment {
            box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
            border-radius: 5px;
            overflow: hidden;
            background: #ffffff;
            padding: 20px;
        }

        .article-comment h4 {
            color: #20247b;
            font-weight: 700;
            margin-bottom: 25px;
            font-size: 22px;
        }

        img {
            max-width: 100%;
        }

        img {
            vertical-align: middle;
            border-style: none;
        }

        /* Contact Us
                                                                                                                            ---------------------*/
        .contact-name {
            margin-bottom: 30px;
        }

        .contact-name h5 {
            font-size: 22px;
            color: #20247b;
            margin-bottom: 5px;
            font-weight: 600;
        }

        .contact-name p {
            font-size: 18px;
            margin: 0;
        }

        .social-share a {
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 50%;
            color: #ffffff;
            text-align: center;
            margin-right: 10px;
        }

        .social-share .dribbble {
            box-shadow: 0 8px 30px -4px rgba(234, 76, 137, 0.5);
            background-color: #ea4c89;
        }

        .social-share .behance {
            box-shadow: 0 8px 30px -4px rgba(0, 103, 255, 0.5);
            background-color: #0067ff;
        }

        .social-share .linkedin {
            box-shadow: 0 8px 30px -4px rgba(1, 119, 172, 0.5);
            background-color: #0177ac;
        }

        .contact-form .form-control {
            border: none;
            border-bottom: 1px solid #20247b;
            background: transparent;
            border-radius: 0;
            padding-left: 0;
            box-shadow: none !important;
        }

        .contact-form .form-control:focus {
            border-bottom: 1px solid #fc5356;
        }

        .contact-form .form-control.invalid {
            border-bottom: 1px solid #ff0000;
        }

        .contact-form .send {
            margin-top: 20px;
        }

        @media (max-width: 767px) {
            .contact-form .send {
                margin-bottom: 20px;
            }
        }

        .section-title h2 {
            font-weight: 700;
            color: #20247b;
            font-size: 45px;
            margin: 0 0 15px;
            border-left: 5px solid #fc5356;
            padding-left: 15px;
        }

        .section-title {
            padding-bottom: 45px;
        }

        .contact-form .send {
            margin-top: 20px;
        }

        .px-btn {
            padding: 0 50px 0 20px;
            line-height: 60px;
            position: relative;
            display: inline-block;
            color: #20247b;
            background: none;
            border: none;
        }

        .px-btn:before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            border-radius: 30px;
            background: transparent;
            border: 1px solid rgba(252, 83, 86, 0.6);
            border-right: 1px solid transparent;
            -moz-transition: ease all 0.35s;
            -o-transition: ease all 0.35s;
            -webkit-transition: ease all 0.35s;
            transition: ease all 0.35s;
            width: 60px;
            height: 60px;
        }

        .px-btn .arrow {
            width: 13px;
            height: 2px;
            background: currentColor;
            display: inline-block;
            position: absolute;
            top: 0;
            bottom: 0;
            margin: auto;
            right: 25px;
        }

        .px-btn .arrow:after {
            width: 8px;
            height: 8px;
            border-right: 2px solid currentColor;
            border-top: 2px solid currentColor;
            content: "";
            position: absolute;
            top: -3px;
            right: 0;
            display: inline-block;
            -moz-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .btn-label {
            position: relative;
            left: -12px;
            display: inline-block;
            padding: 6px 12px;
            background: rgba(0, 0, 0, 0.15);
            border-radius: 3px 0 0 3px;
        }

        .btn-labeled {
            padding-top: 0;
            padding-bottom: 0;
        }

        #search-box {
            padding-left: 0;
            position: relative;
        }

        #search-box input {
            width: 100%;
            border: 0;
            height: 60px;
            border-radius: 25px;
            outline: none;
            padding-left: 60px;
            line-height: 61px;
            box-shadow: 12px 12px 30px 0 rgba(77, 77, 119, .10);
            font-weight: 500;
            color: #8ba2ad;
        }

        #search-box input::-webkit-input-placeholder {
            font-weight: 500;
            color: #c5d3de;
            font-size: 14px;
        }

        #search-box input::-moz-placeholder {
            font-weight: 500;
            color: #c5d3de;
            font-size: 14px;
        }

        #search-box input::-ms-input-placeholder {
            font-weight: 500;
            color: #c5d3de;
            font-size: 14px;
        }

        #search-box input::-moz-placeholder {
            font-weight: 500;
            color: #c5d3de;
            font-size: 14px;
        }

        #search-box .fa-blog {
            position: absolute;
            top: 19px;
            left: 22px;
            font-size: 23px;
            color: #dcdee0;
        }

        #search-box .inline-search {
            position: absolute;
            top: 10px;
            right: 25px;
        }

        #search-box .inline-search #search-btn {
            background-image: linear-gradient(to right, #20247b, #03a9f3);
            border: 0;
            transition: background-size .2s ease-in-out, .2s box-shadow ease-in-out, .2s filter, .3s opacity;
            color: #fff;
            height: 40px;
            width: 40px;
            font-size: 14px;
            border-radius: 50px;
            outline: none !important;
            line-height: 40px;
            cursor: pointer;
            box-shadow: 3px 4px 31px 0 rgb(183 204 214);
            margin-left: 2px;
            -webkit-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
        }

        #search-box .inline-search #search-btn:hover {
            background-size: 175% 100%;
            color: #fff;
            outline: none;
            transition: background-size .2s ease-in-out, .2s box-shadow ease-in-out, .2s filter, .3s opacity;
        }

        #search-box .inline-search #transfer-btn {
            background-image: linear-gradient(to right, #5c5cfd, #d65ffd);
            border: 0;
            transition: background-size .2s ease-in-out, .2s box-shadow ease-in-out, .2s filter, .3s opacity;
            color: #fff;
            height: 40px;
            width: 40px;
            font-size: 14px;
            border-radius: 50px;
            outline: none !important;
            line-height: 40px;
            cursor: pointer;
            box-shadow: 3px 4px 31px 0 rgba(149, 94, 253, 0.45);
            -webkit-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
        }

        #search-box .inline-search #transfer-btn:hover {
            background-size: 175% 100%;
            color: #fff;
            outline: none;
            transition: background-size .2s ease-in-out, .2s box-shadow ease-in-out, .2s filter, .3s opacity;
        }

        #disqus_thread {
            position: relative;
        }

        #disqus_thread:after {
            content: "";
            display: block;
            height: 55px;
            width: 100%;
            position: absolute;
            bottom: 0;
            background: #f2f4f8;
        }

        #disqus_thread:before {
            content: "";
            display: block;
            height: 32px;
            width: 70%;
            position: absolute;
            top: 0;
            left: 100px;
            background: #f2f4f8;
        }

    </style>

    <div class="blog-single gray-bg">
        <div class="container">
            <div class="row align-items-start">
                <div class="col-lg-8 m-15px-tb">
                    <a href="{{ url()->previous() }}" class="btn btn-labeled btn-info pt-1 pb-1">
                        <span class="btn-label"><i class="fas fa-chevron-circle-left"></i></span>kembali
                    </a>
                    <article class="article">
                        <div class="article-img">
                            @if ($blog['file_edit'] == 'default.png')
                                <img src="https://via.placeholder.com/800x350/87CEFA/000000" title="" alt="">
                            @else
                                <img src="{{ $blog['file'] }}" alt="">
                            @endif

                        </div>
                        <div class="article-title">
                            <h6><a href="#">{{ $blog['kategori'] }}</a></h6>
                            <h2>{{ $blog['judul'] }}</h2>
                            <div class="media">
                                <div class="avatar">
                                    <img src="{{ $blog['avatar'] }}" title="" alt="">
                                </div>
                                <div class="media-body">
                                    <label>{{ $blog['pembuat'] }}</label>
                                    <span>{{ (new \App\Helpers\Help())->getTanggal($blog['created_at']) }}</span>
                                    {{-- <span>26 FEB 2020</span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="article-content">
                            {!! $blog['isi'] !!}
                        </div>
                        <div class="like p-2 cursor" style="width: 100%">
                            <div class="pull-right">
                                <span>Dilihat</span>
                                <i class="fas fa-eye"></i><span class="ml-1">{{ $blog['dilihat'] }}x</span>
                            </div>
                        </div>
                        {{-- <div class="nav tag-cloud" style="width: 100%">
                            <a href="#">Design</a>
                            <a href="#">Development</a>
                            <a href="#">Travel</a>
                            <a href="#">Web Design</a>
                            <a href="#">Marketing</a>
                            <a href="#">Research</a>
                            <a href="#">Managment</a>
                        </div> --}}

                    </article>
                    {{-- <div class="contact-form article-comment mb-5">
                        <div class="card p-2" id="blog_komentar">
                            <div class="header">
                                <h2>{{ count($blog['komentar']) }} Comments</h2>
                            </div>
                            <div class="body">
                                <ul class="comment-reply list-unstyled">
                                    @if (empty($blog['komentar']))
                                        <li class="row clearfix m-0">
                                            <p>Data komentar kosong</p>
                                        </li>
                                    @else
                                        @foreach ($blog['komentar'] as $km)
                                            <li class="row clearfix m-0">
                                                <div class="icon-box col-md-2 col-4 pl-1 pr-1">
                                                    <img class="img-fluid img-thumbnail" src="{{ $km['avatar'] }}"
                                                        alt="Awesome Image">
                                                </div>
                                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                                    <h5 class="m-b-0">{{ $km['pembuat'] }}</h5>
                                                    <p>{{ $km['komentar'] }} </p>
                                                    <ul class="list-inline">
                                                        <li><a
                                                                href="javascript:void(0);">{{ (new \App\Helpers\Help())->getTanggal($km['created_at']) }}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div> --}}
                    <div id="disqus_thread"></div>
                    {{-- @if (session('sosial') != $blog['sosial'])
                        <div class="contact-form article-comment">
                            <h4>Leave a Reply</h4>
                            <form id="contact-form" action="javascript:void(0)" method="POST">
                                <div class="row">
                                    <input type="hidden" name="id_blog" value="{{ $blog['id'] }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="komentar" id="komentar" placeholder="Your message *" rows="4"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="send">
                                            <button class="px-btn theme" type="submit"><span>Submit</span> <i
                                                    class="arrow"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif --}}
                </div>
                <div class="col-lg-4 m-15px-tb blog-aside">
                    <!-- Author -->
                    <form action="{{ route('alumni-blog_search') }}" method="GET" id="search-box">
                        {{-- <form method="post" action="{{ route('alumni-blog_search', ['key' => 'rocket']) }}" id="search-box" class=""> --}}
                        <i class="fas fa-blog"></i>
                        <input type="text" placeholder="Type your keyword here..." name="keyword"
                            value="{{ old('keyword') }}">
                        <span class="inline-search">
                            <button id="search-btn" type="submit" value="Search"><i class="fa fa-search"></i></button>
                        </span>
                    </form>
                    <div class="widget widget-author">
                        <div class="widget-title">
                            <h3>Author</h3>
                        </div>
                        <div class="widget-body">
                            <div class="media align-items-center">
                                <div class="avatar">
                                    <img src="{{ $profile['file'] }}" title="" alt="">
                                </div>
                                <div class="media-body">
                                    <h6>Hallo, Saya<br> {{ $profile['nama'] }}</h6>
                                </div>
                            </div>
                            <p class="text-center">
                                {{ empty($profile['bio']) ? 'Pembuat tidak mempunyai Bio untuk ditampilkan' : $profile['bio'] }}
                            </p>
                        </div>
                    </div>

                    <div class="widget widget-latest-post">
                        <div class="widget-title">
                            <h3>Blog Saya Lainnya</h3>
                        </div>
                        <div class="widget-body">
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($myBlog as $mb)
                                <div class="latest-post-aside media">
                                    <div class="lpa-left media-body">
                                        <div class="lpa-title">
                                            <h5><a href="#">{{ $mb['judul'] }}</a></h5>
                                        </div>
                                        <div class="lpa-meta">
                                            <a class="name" href="#">
                                                {{ $mb['pembuat'] }}
                                            </a>
                                            <a class="date" href="#">
                                                26 FEB 2020
                                            </a>
                                        </div>
                                    </div>
                                    <div class="lpa-right">
                                        <a href="#">
                                            @if ($mb['file'] != null)
                                                <img src="{{ $mb['file'] }}" alt="">
                                            @else
                                                <img src="https://via.placeholder.com/400x200/FFB6C1/000000" title=""
                                                    alt="">
                                            @endif
                                        </a>
                                    </div>
                                </div>
                                @php
                                    $count++;
                                @endphp
                                @if ($count == 5)
                                    @php
                                        break;
                                    @endphp
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#contact-form').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-blog_komentar_create') }}";
                    method_url = "POST";
                }
                $.ajax({
                    url: "{{ route('alumni-blog_komentar_create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#blog_komentar').html(
                            '<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#contact-form').trigger("reset");
                        }
                        $("#blog_komentar").load(window.location.href +
                            " #blog_komentar");
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document,
                s = d.createElement('script');
            s.src = 'https://smart-shool-disqus-com.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>

@endsection
