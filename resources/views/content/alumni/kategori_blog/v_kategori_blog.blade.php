@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .dataTables_wrapper .dataTables_length {
            margin: 0 !important;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="widget-bg">
        <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
        <hr>
        <div class="m-portlet__body">
            <button class="btn btn-info mb-2" id="addData"><i class="fas fa-plus"></i> Tambah Data</button>
            <div class="table-responsive">
                <table class="table table-striped" id="data-tabel">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kategori">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Kategori Blog</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan[]" id="keterangan" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris mt-2">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-siswa') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $(document).on('click', '.check_kategori', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-kategori_blog_alumni') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $('#addData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah " + "{{ session('title') }}");
                $('.tambahBaris').show();
                $('.fomAddBaris').html('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });


            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-kat_blog_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-kat_blog_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-kategori_blog_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit {{ session('title') }}");
                        $('#id_kategori').val(data.id);
                        $('#nama').val(data.nama);
                        $('#keterangan').val(data.keterangan);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-kategori_blog_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            swa(data.status + "!", data.message, data.success);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });


        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div id="row' + i +
                '"><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Nama Kategori Blog</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div></div><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Keterangan</label><div class="col-sm-12"><textarea name="keterangan[]" id="keterangan" cols="30" rows="3" class="form-control"></textarea></div></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
