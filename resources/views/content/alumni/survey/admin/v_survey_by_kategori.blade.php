@extends('content.alumni.survey.admin.v_survey')
@section('content_admin_alumni')
    <style>
        .template_faq {
            background: #edf3fe none repeat scroll 0 0;
        }

        .panel-group {
            background: #fff none repeat scroll 0 0;
            border-radius: 3px;
            box-shadow: 0 5px 30px 0 rgba(0, 0, 0, 0.04);
            margin-bottom: 0;
            padding: 30px;
        }

        #accordion .panel {
            border: medium none;
            border-radius: 0;
            box-shadow: none;
            margin: 0 0 15px 10px;
        }

        #accordion .panel-heading {
            border-radius: 30px;
            padding: 0;
        }

        #accordion .panel-title a {
            background: #ffb900 none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 30px;
            color: #fff;
            display: block;
            font-size: 18px;
            font-weight: 600;
            padding: 12px 20px 12px 50px;
            position: relative;
            transition: all 0.3s ease 0s;
        }

        #accordion .panel-title a.collapsed {
            background: #fff none repeat scroll 0 0;
            border: 1px solid #ddd;
            color: #333;
        }

        #accordion .panel-title a::after,
        #accordion .panel-title a.collapsed::after {
            background: #ffb900 none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
            color: #fff;
            content: "";
            font-family: fontawesome;
            font-size: 25px;
            height: 55px;
            left: -20px;
            line-height: 55px;
            position: absolute;
            text-align: center;
            top: -5px;
            transition: all 0.3s ease 0s;
            width: 55px;
        }

        #accordion .panel-title a.collapsed::after {
            background: #fff none repeat scroll 0 0;
            border: 1px solid #ddd;
            box-shadow: none;
            color: #333;
            content: "";
        }

        #accordion .panel-body {
            background: transparent none repeat scroll 0 0;
            border-top: medium none;
            padding: 20px 25px 10px 9px;
            position: relative;
        }

        #accordion .panel-body p {
            border-left: 1px dashed #8c8c8c;
            padding-left: 25px;
        }

        i.fas.fa-pencil-alt.fa-2x {
            background: #002fff none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
            content: "";
            font-size: 25px;
            height: 55px;
            line-height: 55px;
            text-align: center;
            transition: all 0.3s ease 0s;
            width: 55px;
        }

        i.fas.fa-plus-circle.fa-2x {
            background: #00b92e none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
            content: "";
            font-size: 25px;
            height: 55px;
            line-height: 55px;
            text-align: center;
            transition: all 0.3s ease 0s;
            width: 55px;
        }

        i.fas.fa-trash-alt.fa-2x {
            background: #ff0000 none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
            content: "";
            font-size: 25px;
            height: 55px;
            line-height: 55px;
            text-align: center;
            transition: all 0.3s ease 0s;
            width: 55px;
        }
        i.fas.fa-chart-bar.fa-2x {
            background: #ff00d4d7 none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
            content: "";
            font-size: 25px;
            height: 55px;
            line-height: 55px;
            text-align: center;
            transition: all 0.3s ease 0s;
            width: 55px;
        }

    </style>
    <div class="col-lg-8">
        <div class="widget-bg" style="background: #f2f4f8 !important">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow zoomIn">
                        <h1>{{ $kategori['nama'] }}</h1>
                        <span></span>
                        <p>{{ $kategori['keterangan'] }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="action">
                        <button class="btn" style="border: none" id="addSurvey"><i class="fas fa-plus-circle fa-2x"
                                style="color:#fff;"></i></button>
                        <a class="btn" href="{{ route('alumni-statistik_survey', $id_kategori) }}" style="border: none"><i class="fas fa-chart-bar fa-2x"
                                style="color:#fff;"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="resultSurvey">
                    @if (empty($data))
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <p style="text-align: center">Maaf, Survey saat ini masih kosong. silahkan tambahkan beberapa
                                untuk mengisi</p>
                        </div>
                    @else
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach ($data as $qs)
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading{{ $qs['id'] }}"
                                        style="position: relative">
                                        <div class="action" style="position: absolute; right: 0">
                                            <a onclick="return confirm('Apa kamu yakin?')"
                                                href="{{ route('alumni-delete_survey', $qs['id']) }}"><i
                                                    class="fas fa-trash-alt fa-2x" style="color: #fff"></i></a>
                                            <a onclick="editSurvey({{ $qs['id'] }})" class="editSur{{ $qs['id'] }}"><i class="fas fa-pencil-alt fa-2x"
                                                    style="color:#fff"></i></a>
                                        </div>
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                                data-parent="#accordion" href="#collapse{{ $qs['id'] }}"
                                                aria-expanded="false" aria-controls="collapseOne"
                                                style="margin-right: 130px">
                                                {{ $qs['pertanyaan'] }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $qs['id'] }}" class="panel-collapse collapse in" role="tabpanel"
                                        aria-labelledby="heading{{ $qs['id'] }}">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6 class="text-uppercase">jenis</h6>
                                                    <p rel="field-nominal" class="mr-t-0">{{ $qs['jenis'] }}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6 class="text-uppercase">Kategori</h6>
                                                    <p rel="field-nominal" class="mr-t-0">{{ $qs['kategori'] }}
                                                    </p>
                                                </div>
                                            </div>
                                            Pilihan Jawaban :
                                            @if ($qs['jenis'] == 'pilihan')
                                            <ul>
                                                <li><label for="">Pilihan 1</label> >>> {{ $qs['pilihan1'] }}</li>
                                                <li><label for="">Pilihan 2</label> >>> {{ $qs['pilihan2'] }}</li>
                                                <li><label for="">Pilihan 3</label> >>> {{ $qs['pilihan3'] }}</li>
                                                <li><label for="">Pilihan 4</label> >>> {{ $qs['pilihan4'] }}</li>
                                                <li><label for="">Pilihan 5</label> >>> {{ $qs['pilihan5'] }}</li>
                                            </ul>
                                            @else
                                                <textarea name="" id="" rows="3" class="form-control" disabled></textarea>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalSurvey" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleSurvey"></h5>
                </div>
                <form id="formSurvey" name="formSurvey" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_survey">
                                <input type="hidden" name="id_kategori" value="{{ $id_kategori }}">
                                <div class="form-group mb-2">
                                    <label for="name" class="col-sm-12 control-label">Pertanyaan</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="pertanyaan" id="pertanyaan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mb-2">
                                    <label for="name" class="col-sm-12 control-label">Jenis Survey</label>
                                    <div class="col-sm-12">
                                        <select name="jenis" id="jenis_survey" class="form-control">
                                            <option value="">--Pilih Jenis--</option>
                                            <option value="pilihan">Pilihan</option>
                                            <option value="teks">Essay</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-2 panel-collapse collapse in" id="jawaban">
                                    <label for="name" class="col-sm-12 control-label">Pilihan Jawaban</label>
                                    <div class="row m-2 p-2" style="border: 1px solid #eeeeee;">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Pilihan 1</label>
                                                <input type="text" class="form-control" id="pilihan1" name="pilihan1"
                                                    value="Sangat Baik">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Pilihan 2</label>
                                                <input type="text" class="form-control" id="pilihan2" name="pilihan2"
                                                    value="Baik">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Pilihan 3</label>
                                                <input type="text" class="form-control" name="pilihan3" id="pilihan3"
                                                    value="Cukup Baik">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Pilihan 4</label>
                                                <input type="text" class="form-control" name="pilihan4" id="pilihan4"
                                                    value="Kurang Baik">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Pilihan 5</label>
                                                <input id="pilihan5" type="text" class="form-control" name="pilihan5"
                                                    value="Tidak Baik">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionSurvey" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script>
        (function($) {
            'use strict';

            jQuery(document).on('ready', function() {
                $('a.page-scroll').on('click', function(e) {
                    var anchor = $(this);
                    $('html, body').stop().animate({
                        scrollTop: $(anchor.attr('href')).offset().top - 50
                    }, 1500);
                    e.preventDefault();
                });

            });

        })(jQuery);

        $(function() {
            $('#addSurvey').click(function() {
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#titleSurvey').html("Tambah Survey");
                $('#modalSurvey').modal('show');
                $('#actionSurvey').val('Add');
            });

            $('#formSurvey').on('submit', function(event) {
                event.preventDefault();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#actionSurvey').val() == 'Add') {
                    action_url = "{{ route('alumni-simpan_soal_survey') }}";
                    method_url = "POST";
                }

                if ($('#actionSurvey').val() == 'Edit') {
                    action_url = "{{ route('alumni-update_soal_survey') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#resultSurvey').html('<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalSurvey').modal('hide');
                            location.reload();
                            $('#formSurvey').trigger("reset");
                        } else {
                            $(".table-responsive").load(window.location.href +
                                " #resultSurvey");
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        });

        $('#jawaban').collapse('hide');
        $('#jenis_survey').change(function() {
            if ($('#jenis_survey').val() == "pilihan") {
                $('#jawaban').collapse('show');
            } else {
                $('#jawaban').collapse('hide');
            }
        });

        function editSurvey(id) {
            $('#formSurvey').trigger("reset");
            $.ajax({
                type: 'POST',
                url: "{{ route('alumni-detail_survey') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".editSur" + id).html(
                        '<i class="fa fa-spin fa-spinner fa-2x" style="color: #002fff"></i>');
                },
                success: function(data) {
                    $(".editSur" + id).html('<i class="fas fa-pencil-alt fa-2x" style="color:#fff"></i>');
                    $('#titleSurvey').html("Edit Survey");
                    $('#id_survey').val(data.id);
                    $('#pertanyaan').val(data.pertanyaan);
                    $('#jenis_survey').val(data.jenis).trigger('change');
                    $('#pilihan1').val(data.pilihan1);
                    $('#pilihan2').val(data.pilihan2);
                    $('#pilihan3').val(data.pilihan3);
                    $('#pilihan4').val(data.pilihan4);
                    $('#pilihan5').val(data.pilihan5);
                    $('#actionSurvey').val('Edit');
                    $('#modalSurvey').modal('show');
                }
            });
        }
    </script>
@endsection
