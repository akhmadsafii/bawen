@extends('content.alumni.v_alumni')
@section('content_alumni')
    <style>
        .media.col-md-10.col-lg-8.col-xl-7.p-0.my-4.mx-auto {
            display: flex;
            align-items: center;
            justify-content: center;
        }


        .forgot {
            background-color: #fff;
            padding: 12px;
            border: 1px solid #dfdfdf
        }

        .padding-bottom-3x {
            padding-bottom: 72px !important
        }

        .card-footer {
            background-color: #fff
        }

        .btn {
            font-size: 13px
        }

        .form-control:focus {
            color: #495057;
            background-color: #fff;
            border-color: #76b7e9;
            outline: 0;
            box-shadow: 0 0 0 0px #28a745
        }

        .ui-w-100 {
            width: 100px !important;
            height: auto;
        }

    </style>
    <div class="col-lg-8">
        <div class="row">
            <div class="widget-bg">
                <div class="ui-bg-overlay bg-dark opacity-50"></div>
                <div class="container">
                    <div class="media col-md-10 col-lg-8 col-xl-7 p-0 my-4 mx-auto">
                        <img src="{{ $alumni['file'] }}" alt class="d-block ui-w-100 rounded-circle">
                        <div class="media-body ml-5">
                            <h4 class="font-weight-bold mb-4">{{ ucfirst($alumni['nama']) }}</h4>
                            <div class="opacity-75 mb-4">
                                Lorem ipsum dolor sit amet, nibh suavitate qualisque ut nam. Ad harum primis electram duo,
                                porro principes ei has.
                            </div>
                            <a href="#" class="d-inline-block text-white">
                                <strong>234</strong>
                                <span class="opacity-75">followers</span>
                            </a>
                            <a href="#" class="d-inline-block text-white ml-3">
                                <strong>111</strong>
                                <span class="opacity-75">following</span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="widget-body clearfix">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="nav-item active col-12"><a class="nav-link" href="#tab-edit" data-toggle="tab"
                                    aria-expanded="true">EDIT</a>
                            </li>
                            <li class="col-12 nav-item"><a class="nav-link" href="#tab-reset" data-toggle="tab"
                                    aria-expanded="true">RESET PASSWORD</a>
                            </li>
                        </ul>
                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-edit">
                                <form class="form" action="{{ route('alumni-alumni_update') }}" method="post"
                                    enctype="multipart/form-data" onsubmit="return loader()">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="text-center">
                                                <img src="{{ $alumni['file'] }}" class="avatar img-circle img-thumbnail"
                                                    alt="avatar">
                                                <h6>Upload a different photo...</h6>
                                                <input type="file" name="image" accept="*" onchange="readURL(this);"
                                                    class="text-center center-block file-upload">
                                            </div>
                                            <br>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="hidden" name="id" value="{{ $alumni['id'] }}">
                                                <div class="col-xs-6">
                                                    <label for="first_name">
                                                        Nomor Induk
                                                    </label>
                                                    <input type="text" class="form-control" name="no_induk" id="no_induk"
                                                        value="{{ $alumni['no_induk'] }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="first_name">
                                                        Nama
                                                    </label>
                                                    <input type="text" class="form-control" name="nama" id="nama"
                                                        value="{{ $alumni['nama'] }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="last_name">
                                                        Jurusan
                                                    </label>
                                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                                        <option value="">Pilih Jurusan</option>
                                                        @foreach ($jurusan as $jr)
                                                            <option value="{{ $jr['id'] }}"
                                                                {{ $alumni['id_jurusan'] == $jr['id'] ? 'selected' : '' }}>
                                                                {{ $jr['nama'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="phone">
                                                        Jenis Kelamin
                                                    </label>
                                                    <select name="jenkel" class="form-control" id="jenkel">
                                                        <option value="l"
                                                            {{ $alumni['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki
                                                        </option>
                                                        <option value="p"
                                                            {{ $alumni['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="mobile">
                                                        Agama
                                                    </label>
                                                    <select name="agama" id="agama" class="form-control">
                                                        <option value="islam"
                                                            {{ $alumni['agama'] == 'islam' ? 'selected' : '' }}>Islam
                                                        </option>
                                                        <option value="kristen"
                                                            {{ $alumni['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                                        </option>
                                                        <option value="hindu"
                                                            {{ $alumni['agama'] == 'hindu' ? 'selected' : '' }}>Hindu
                                                        </option>
                                                        <option value="budha"
                                                            {{ $alumni['agama'] == 'budha' ? 'selected' : '' }}>Budha
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="email">
                                                        Email
                                                    </label>
                                                    <input type="email" class="form-control" name="email" id="email"
                                                        value="{{ $alumni['email'] }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="email">
                                                        Telepon
                                                    </label>
                                                    <input type="text" class="form-control" name="telepon" id="telepon"
                                                        value="{{ $alumni['telepon'] }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="col-xs-6">
                                                            <label for="email">
                                                                Tempat Lahir
                                                            </label>
                                                            <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                                class="form-control"
                                                                value="{{ $alumni['tempat_lahir'] }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="col-xs-6">
                                                            <label for="email">
                                                                Tanggal Lahir
                                                            </label>
                                                            <input type="text" name="tgl_lahir" id="tgl_lahir"
                                                                class="form-control"
                                                                value="{{ $alumni['tgl_lahir'] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="email">
                                                        Tahun Lulus
                                                    </label>
                                                    <select name="tahun_lulus" id="tahun_lulus" class="form-control">
                                                        @php
                                                            $firstYear = (int) date('Y');
                                                            $lastYear = $firstYear - 20;
                                                        @endphp
                                                        @for ($i = $firstYear; $i >= $lastYear; $i--)
                                                            <option value="{{ $i }}"
                                                                {{ $alumni['tahun_lulus'] == $i ? 'selected' : '' }}>
                                                                {{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="email">
                                                        Nomor Ijazah
                                                    </label>
                                                    <input type="text" class="form-control" name="no_ijazah"
                                                        id="no_ijazah" value="{{ $alumni['no_ijazah'] }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="email">
                                                        Alamat
                                                    </label>
                                                    <textarea name="alamat" id="alamat" class="form-control" cols="30"
                                                        rows="3">{{ $alumni['alamat'] }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-success" type="submit" id="submit_edit"><i
                                                            class="fas fa-save"></i>
                                                        Save</button>
                                                    <button class="btn btn-danger" type="reset"><i
                                                            class="fas fa-undo-alt"></i>
                                                        Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab-reset">
                                <div class="container padding-bottom-3x mb-2 mt-5">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="forgot">
                                                <h2>Forgot your password?</h2>
                                                <p>Change your password in three easy steps. This will help you to secure
                                                    your password!</p>
                                                <ol class="list-unstyled">
                                                    <li><span class="text-primary text-medium">1. </span>Enter your email
                                                        address below.</li>
                                                    <li><span class="text-primary text-medium">2. </span>Our system will
                                                        send you a temporary link</li>
                                                    <li><span class="text-primary text-medium">3. </span>Use the link to
                                                        reset your password</li>
                                                </ol>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form class="card mt-4" id="reset_manual"
                                                        action="javascript:void(0)" onsubmit="resetPassword('manual')">
                                                        <label for="">PASSWORD MANUAL</label>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <label for="email-for-pass">Masukan
                                                                    Password yang baru</label>
                                                                <input class="form-control" type="text" id="first_pass"
                                                                    required name="first_password">
                                                                <small class="form-text text-muted">Harap masukan password
                                                                    baru anda, setelah dirasa cukup klik ganti password
                                                                    untuk mengganti</small>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <button class="btn btn-success btnPassMan" type="submit">Ganti
                                                                Password</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-6">
                                                    <form class="card mt-4" id="reset_random"
                                                        action="javascript:void(0)" onsubmit="resetPassword('random')">
                                                        <label for="">PASSWORD RANDOM</label>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <label for="email-for-pass">Password akan ditampilkan
                                                                    disini</label>
                                                                <input class="form-control" type="text" id="pass_random"
                                                                    readonly>
                                                                <small class="form-text text-muted">Klik dapatkan password,
                                                                    password akan otomatis terganti, metode tercepat untuk
                                                                    mengganti password dengan keamanan tinggi.</small>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <button class="btn btn-danger btnPassRan" type="submit">Dapatkan
                                                                Password</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function loader() {
            $("#submit_edit").html('<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#submit_edit").attr("disabled", true);
        }

        function resetPassword(params) {
            $.ajax({
                url: "{{ route('alumni-alumni_reset_password') }}",
                method: "POST",
                data: {
                    password: $('#first_pass').val(),
                    id: "{{ $alumni['id'] }}",
                    params
                },
                dataType: "json",
                beforeSend: function() {
                    if (params == "random") {
                        $(".btnPassRan").html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    } else {
                        $(".btnPassMan").html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    }
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        if (params == "random") {
                            $('#pass_random').val(data.data);
                            $('.btnPassRan').html("Dapatkan Password");
                        } else {
                            $('.btnPassMan').html("Ganti Password");
                            $('#pass_random').val("");
                        }
                    }
                    swa(data.status + "!", data.message, data.icon);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }

        // $('#CustomerForm').on('submit', function(event) {
        //     event.preventDefault();
        //     inputForm();
        // });

        // function inputForm(){
        //     $("#saveBtn").html(
        //         '<i class="fa fa-spin fa-spinner"></i> Loading');
        //     $("#saveBtn").attr("disabled", true);
        //     var action_url = '';

        //     if ($('#action').val() == 'Add') {
        //         action_url = "{{ route('store-jurusan') }}";
        //         method_url = "POST";
        //     }

        //     if ($('#action').val() == 'Edit') {
        //         action_url = "{{ route('update-jurusan') }}";
        //         method_url = "PUT";
        //     }
        //     $.ajax({
        //         url: "{{ route('update-jurusan') }}",
        //         method: "POST",
        //         data: $(this).serialize(),
        //         dataType: "json",
        //         success: function(data) {
        //             if (data.status == 'berhasil') {
        //                 $('#CustomerForm').trigger("reset");
        //                 $('#ajaxModel').modal('hide');
        //                 var oTable = $('#data-tabel').dataTable();
        //                 oTable.fnDraw(false);
        //             }
        //             noti(data.icon, data.success);
        //             $('#saveBtn').html('Simpan');
        //             $("#saveBtn").attr("disabled", false);
        //         },
        //         error: function(data) {
        //             console.log('Error:', data);
        //             $('#saveBtn').html('Simpan');
        //         }
        //     });
        // }
    </script>
@endsection
