@extends('content.alumni.detail.v_detail')
@section('content_detail')
    <style>
        .card-box {
            border: 1px solid #ddd;
            padding: 20px;
            box-shadow: 0px 0px 10px 0px #c5c5c5;
            margin-bottom: 30px;
            float: left;
            border-radius: 10px;
        }

        .card-box .card-thumbnail {
            max-height: 200px;
            overflow: hidden;
            border-radius: 10px;
            transition: 1s;
        }

        .card-box .card-thumbnail:hover {
            transform: scale(1.2);
        }

        .card-box h3 a {
            font-size: 20px;
            text-decoration: none;
        }

        .image-blog {
            width: 234px;
            height: 159px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>
    <h5 class="box-title mr-b-0">Artikel User</h5>
    <hr>
    <div class="row">
        @if (!empty($alumni['blog']))
            @foreach ($alumni['blog'] as $blog)
                <div class="col-md-6 col-lg-4">
                    <div class="card-box">
                        <div class="card-thumbnail image-blog" style="background-image: url({{ $blog['file'] }});">
                        </div>
                        <h3><a href="#" class="mt-2 text-info">{!! Str::limit($blog['judul'], 20, ' ...') !!}</a></h3>
                        <p class="text-secondary text-justify">{!! Str::limit($blog['isi'], 200, ' ...') !!}</p>
                        <a href="{{ route('alumni-blog_detail', ['id' => (new \App\Helpers\Help())->encode($blog['id']),'judul' => str_slug($blog['judul'])]) }}"
                            class="btn btn-sm btn-info float-right">Read more >></a>
                    </div>
                </div>
            @endforeach
        @else
            <center>Tidak ada blog dari user yang tersedia</center>
        @endif
    </div>
@endsection
