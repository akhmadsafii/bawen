@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <h5 class="box-title mr-b-0">Profile Saya</h5>
    <hr>
    <form id="formProfile">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="l30">Nama</label>
                    <input class="form-control" name="nama" value="{{ $alumni['nama'] }}" type="text">
                </div>
                <div class="form-group">
                    <label for="l30">No Induk</label>
                    <input class="form-control" name="no_induk" value="{{ $alumni['no_induk'] }}" type="text">
                </div>
                <div class="form-group">
                    <label for="l30">Email</label>
                    <input class="form-control" name="email" type="email" value="{{ $alumni['email'] }}">
                </div>
                <div class="form-group">
                    <label for="l30">Fakultas</label>
                    <select name="id_fakultas" class="form-control">
                        <option value="">Pilih Fakultas..</option>
                        @foreach ($fakultas as $fk)
                            <option value="{{ (new \App\Helpers\Help())->encode($fk['id']) }}"
                                {{ $alumni['id_fakultas'] == $fk['id'] ? 'selected' : '' }}>
                                {{ $fk['nama'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Jurusan</label>
                    <select name="id_jurusan" class="form-control" disabled>
                        <option value="">Pilih Jurusan..</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Telepon</label>
                    <input class="form-control" name="telepon" value="{{ $alumni['telepon'] }}" type="text">
                </div>
                <div class="form-group">
                    <label for="l30">Agama</label>
                    <select name="agama" class="form-control">
                        <option value="">Pilih Agama..</option>
                        <option value="islam" {{ $alumni['agama'] == 'islam' ? 'selected' : '' }}>Islam
                        </option>
                        <option value="kristen" {{ $alumni['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                        </option>
                        <option value="hindu" {{ $alumni['agama'] == 'hindu' ? 'selected' : '' }}>Hindu
                        </option>
                        <option value="budha" {{ $alumni['agama'] == 'budha' ? 'selected' : '' }}>Budha
                        </option>
                        <option value="katolik" {{ $alumni['agama'] == 'katolik' ? 'selected' : '' }}>Katolik
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Pendidikan Terakhir</label>
                    <select name="pendidikan_terakhir" class="form-control">
                        <option value="">Pilih Pendidikan Akhir..</option>
                        <option value="d3" {{ $alumni['pendidikan_terakhir'] == 'd3' ? 'selected' : '' }}>D3
                        </option>
                        <option value="d4" {{ $alumni['pendidikan_terakhir'] == 'd4' ? 'selected' : '' }}>D4
                        </option>
                        <option value="s1" {{ $alumni['pendidikan_terakhir'] == 's1' ? 'selected' : '' }}>S1
                        </option>
                        <option value="s2" {{ $alumni['pendidikan_terakhir'] == 's2' ? 'selected' : '' }}>S2
                        </option>
                        <option value="s3" {{ $alumni['pendidikan_terakhir'] == 's3' ? 'selected' : '' }}>S3
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Profile</label> <br>
                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview" class="form-group mb-1"
                        width="150">
                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="l30">Jenis Kelamin</label>
                    <select name="jenkel" id="jenkel" class="form-control">
                        <option value="">Pilih Jenis Kelamin..</option>
                        <option value="l" {{ $alumni['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki
                        </option>
                        <option value="p" {{ $alumni['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Tempat Lahir</label>
                    <input class="form-control" name="tempat_lahir" type="text" value="{{ $alumni['tempat_lahir'] }}">
                </div>
                <div class="form-group">
                    <label for="l30">Tanggal Lahir</label>
                    <input class="form-control datepicker" name="tanggal_lahir" value="{{ $alumni['tgl_lahir'] }}"
                        type="text">
                </div>
                <div class="form-group">
                    <label for="l30">Tahun Lulus</label>
                    <input type="number" name="tahun_lulus" value="{{ $alumni['tahun_lulus'] }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="l30">Tahun Angkatan</label>
                    <input type="number" name="tahun_masuk" value="{{ $alumni['tahun_angkatan'] }}"
                        class="form-control">
                </div>
                <div class="form-group">
                    <label for="l30">No Ijazah</label>
                    <input type="number" name="no_ijazah" value="{{ $alumni['no_ijazah'] }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="l30">Alamat</label>
                    <textarea class="form-control" name="alamat" rows="3">{{ $alumni['alamat'] }}</textarea>
                </div>
                <div class="form-group">
                    <label for="l30">Kota Sekarang</label>
                    <select name="id_kota" id="id_kota" class="select2 form-control">
                        @foreach ($kabupaten as $kb)
                            <option value="{{ $kb['id'] }}" {{ $alumni['id_kota'] == $kb['id'] ? 'selected' : '' }}>
                                {{ $kb['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="l30">Bio</label>
                    <textarea class="form-control" name="bio" rows="3">{{ $alumni['bio'] }}</textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-actions btn-list pull-right">
                    <button class="btn btn-danger" type="reset"><i class="fas fa-sync"></i> Reset</button>
                    <button class="btn btn-info" id="saveBtn" type="submit"><i class="fas fa-user-edit"></i>
                        Update</button>
                </div>
            </div>
        </div>
    </form>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_fakultas"]').on('change', function() {
                var id_fakultas = $(this).val();
                if (id_fakultas) {
                    $.ajax({
                        url: "{{ route('alumni-jurusan_by_fakultas') }}",
                        type: "POST",
                        data: {
                            id_fakultas
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_jurusan"]').html(
                                    '<option value="">Tidak ada jurusan yang ditemukan..</option>'
                                );
                            } else {
                                var s = '<option>Pilih Jurusan..</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.nama +
                                        '</option>';

                                })
                                $('select[name="id_jurusan"]').removeAttr('disabled');
                            }
                            $('select[name="id_jurusan"]').html(s)
                        }
                    });
                }
            })

            $('body').on('submit', '#formProfile', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('update-profile_tanpa_slug') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-user-edit"></i> Update');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-user-edit"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });
        })
    </script>
@endsection
