@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <style>
        .profile-kalendar {
            width: 52px;
            height: 52px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100%;
            border-radius: 10px;
        }

    </style>
    <h5 class="box-title mr-b-0">Agenda User</h5>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div id="calendar"></div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content bg-purple">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 post-slider">
                            <img src="https://www.markuptag.com/images/image-three.jpg" class="img-fluid"
                                id="file-agenda" alt="">
                        </div>
                        <div class="col-md-6 post-slider">
                            <h3 id="title-agenda"></h3>
                            <p class="font-italic text-white" id="keterangan-agenda"></p>
                            <ul>
                                <li><i class="fas fa-map-marker-alt"></i> <span class="text-white text-capitalize"
                                        id="lokasi-agenda"></span></li>
                                <li><i class="fas fa-calendar-week"></i> <span class="text-white"
                                        id="tanggal-agenda"></span></li>
                                <li><i class="fas fa-clock"></i> <span class="text-white" id="waktu-agenda"></span>
                                </li>
                            </ul>
                            <p class="text-white pull-right">Diposting <span class="text-white"
                                    id="dibuat-agenda"></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    avatar: value['avatar'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    file: value['file'],
                                    keterangan: value['keterangan'],
                                    tempat: value['tempat'],
                                    waktu: value['waktu'],
                                    dibuat: value['dibuat'],
                                    pembuat: value['pembuat']
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.title != null) {
                        element.children().last().html(
                            '<div class="icon-box icon-box-left py-0"><header class="p-1"><div class="profile-kalendar" style="background-image: url(' +
                            event.file +
                            ');"></div></header><section class="vertical-middle"><p class="m-0">' +
                            event.title + '</p></section></div>'
                        );
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                eventClick: function(event, jsEvent, view) {
                    $('#title-agenda').html(event.title);
                    $('#file-agenda').attr('src', event.file);
                    $('#keterangan-agenda').html(event.keterangan);
                    $('#lokasi-agenda').html(event.tempat);
                    $('#tanggal-agenda').html(convertDate(event.start) + " sampai dengan " +
                        convertDate(event.end));
                    $('#waktu-agenda').html(event.waktu);
                    $('#dibuat-agenda').html(event.dibuat + " oleh " + event.pembuat);
                    $('#ajaxModel').modal('show');
                }
            });
        });
    </script>
@endsection
