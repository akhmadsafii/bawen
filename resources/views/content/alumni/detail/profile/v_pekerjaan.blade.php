@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <h5 class="box-title mr-b-0">Riwayat Pekerjaan</h5>
    <hr>
    <button type="button" id="btnAdd" class="btn btn-success mb-3">
        <li class="fas fa-plus-circle"></li> Tambah
    </button>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-tabel">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Masa Kerja</th>
                    <th>Posisi</th>
                    <th>Industri</th>
                    <th>Keterangan</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalResult" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="resultForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_pekerjaan">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Posisi</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="posisi" name="posisi"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Industri</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="industri" name="industri"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="name" class="col-sm-12 control-label">Masa Bekerja</label>
                                        <div class="checkbox checkbox-info px-3">
                                            <label class="">
                                                <input type="checkbox" id="status_kerja" name="aktif" value="1"> <span
                                                    class="label-text">Saya masih kerja disini</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control datepicker" id="tgl_mulai"
                                                    name="tgl_mulai" autocomplete="off" placeholder="Tanggal Mulai" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control datepicker" id="tgl_selesai"
                                                    name="tgl_selesai" autocomplete="off" placeholder="Tanggal Selesai" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="1">Aktif</option>
                                            <option value="2">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'masa_kerja',
                        name: 'masa_kerja',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'posisi',
                        name: 'posisi',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'industri',
                        name: 'industri',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center vertical-middle',
                    },
                ]
            });

            $('#btnAdd').click(function() {
                $('#resultForm').trigger("reset");
                $('#modelHeading').html("Tambah Pekerjaan");
                $('#modalResult').modal('show');
                $('#action').val('Add');
            });

            $('#resultForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-pekerjaan_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-pekerjaan_update') }}";
                    method_url = "PUT";
                }
                var id_alumni = $('#id_alumni').val();
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalResult').modal('hide');
                            $('#resultForm').trigger("reset");
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-pekerjaan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Pekerjaan");
                        $('#id_pekerjaan').val(data.id);
                        $('#posisi').val(data.posisi);
                        $('#industri').val(data.industri);
                        $('#tgl_mulai').val(convertDate(data.tgl_mulai));
                        $('#tgl_selesai').val(convertDate(data.tgl_selesai));
                        $('#keterangan').val(data.keterangan);
                        if (data.aktif == 1) {
                            $("#status_kerja").prop("checked", true);
                        }else{
                            $("#status_kerja").prop("checked", false);
                        }
                        $('#status').val(data.status).trigger('change');
                        $('#modalResult').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pekerjaan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


        });
    </script>
@endsection
