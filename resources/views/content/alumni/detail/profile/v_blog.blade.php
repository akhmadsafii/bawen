@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <style>
        .card-box {
            border: 1px solid #ddd;
            padding: 20px;
            box-shadow: 0px 0px 10px 0px #c5c5c5;
            margin-bottom: 30px;
            float: left;
            border-radius: 10px;
        }

        .card-box .card-thumbnail {
            max-height: 200px;
            overflow: hidden;
            border-radius: 10px;
            transition: 1s;
        }

        .card-box .card-thumbnail:hover {
            transform: scale(1.2);
        }

        .card-box h3 a {
            font-size: 20px;
            text-decoration: none;
        }

        .image-blog {
            width: 234px;
            height: 159px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>
    <div class="row">
        <div class="col-md-8 col-6">
            <h5 class="box-title mr-b-0">Artikel User</h5>
        </div>
        <div class="col-md-4 col-6">
            <a href="{{ route('detail-profile_alumni', ['method' => 'blog', 'aksi' => 'create']) }}"
                class="btn btn-success pull-right"><i class="fas fa-plus"></i> Tambah Baru</a>
        </div>
    </div>
    <hr>
    <div class="row">
        @if (!empty($alumni['blog']))
            @foreach ($alumni['blog'] as $blog)
                <div class="col-md-6 col-lg-4">
                    <div class="card-box">
                        <div class="card-thumbnail image-blog" style="background-image: url({{ $blog['file'] }});">
                        </div>
                        <h3><a href="#" class="mt-2 text-info">{!! Str::limit($blog['judul'], 20, ' ...') !!}</a></h3>
                        <p class="text-secondary text-justify">{!! Str::limit($blog['isi'], 200, ' ...') !!}</p>
                        <div class="aksi pull-right">
                            <a href="javascript:void(0)" data-id="{{ $blog['id'] }}"
                                class="btn btn-sm btn-danger delete"><i class="fas fa-trash"></i></a>
                            <a href="{{ route('detail-profile_alumni', ['method' => 'blog','aksi' => 'edit','key' => (new \App\Helpers\Help())->encode($blog['id']),'judul' => str_slug($blog['judul'])]) }}"
                                class="btn btn-sm btn-purple"><i class="fas fa-pencil-alt"></i></a>
                            <a href="{{ route('alumni-blog_detail', ['id' => (new \App\Helpers\Help())->encode($blog['id']),'judul' => str_slug($blog['judul'])]) }}"
                                class="btn btn-sm btn-info">Read more >></a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <center>Tidak ada blog dari user yang tersedia</center>
        @endif
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-data_blog_soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

        })
    </script>
@endsection
