@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <div class="row">
        <div class="col-md-8">
            <h5 class="box-title mr-b-0">Pekerjaan Saya Saat ini</h5>
        </div>
        <div class="col-md-4">
            @if (!empty($pekerjaan))
                <button class="btn pull-right delete" data-id="{{ $pekerjaan['id'] }}"><i
                        class="fas fa-trash text-danger"></i></button>
            @endif
        </div>
    </div>
    @if (!empty($pekerjaan))
        <div class="alert alert-icon alert-info border-info alert-dismissible fade show my-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button> <i class="material-icons list-icon text-info">info</i> <strong>Informasi!</strong> Kamu saat ini
            mempunyai 1 pekerjaan aktif
        </div>
    @endif
    <hr>
    <form id="formPekerjaan">
        <div class="form-group row">
            <label class="col-md-2 col-form-label" for="l0">Jenis Pekerjaan</label>
            <div class="col-md-10">
                <select name="jenis_pekerjaan" id="jenis_pekerjaan" class="form-control">
                    <option value="">Pilih jenis Pekerjaan..</option>
                    @foreach ($jenis as $jn)
                        <option value="{{ $jn['id'] }}"
                            {{ !empty($pekerjaan) && $pekerjaan['id_jenis_pekerjaan'] == $jn['id'] ? 'selected' : '' }}>
                            {{ $jn['nama'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 col-form-label" for="l0">Posisi</label>
            <div class="col-md-10">
                <input class="form-control" id="posisi" name="posisi"
                    value="{{ !empty($pekerjaan) ? $pekerjaan['posisi'] : '' }}" type="text">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 col-form-label" for="l0">Industri</label>
            <div class="col-md-10">
                <input class="form-control" id="industri" name="industri" type="text"
                    value="{{ !empty($pekerjaan) ? $pekerjaan['industri'] : '' }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 col-form-label" for="l0">Keterangan</label>
            <div class="col-md-10">
                <textarea name="keterangan" id="keterangan" rows="3"
                    class="form-control">{{ !empty($pekerjaan) ? $pekerjaan['keterangan'] : '' }}</textarea>
            </div>
        </div>
        <div class="form-actions">
            <div class="form-group row">
                <div class="col-sm-10 ml-auto btn-list">
                    <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i> Update</button>
                    <button type="reset" class="btn btn-danger"><i class="fas fa-bomb"></i> Cancel</button>
                </div>
            </div>
        </div>
    </form>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formPekerjaan').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    url: "{{ route('alumni-pekerjaan_alumni_create') }}",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="fas fa-save"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pekerjaan_alumni_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })
    </script>
@endsection
