@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('detail-profile_alumni', ['method' => 'blog', 'aksi' => 'result']) }}"
                class="btn btn-danger btn-block"><i class="far fa-arrow-alt-circle-left"></i> Kembali</a>

        </div>
        <div class="col-md-10">
            <h5 class="box-title mr-b-0">{{ $_GET['aksi'] == 'create' ? 'BUAT' : 'EDIT' }} BLOG</h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <form id="formBlog">
                <input type="hidden" name="id" value="{{ $_GET['aksi'] == 'edit' ? $blog['id'] : '' }}">
                <div class="form-actions">
                    <div class="form-group row">
                        <div class="col-sm-10 ml-auto btn-list">
                            <img id="modal-preview"
                                src="{{ $_GET['aksi'] == 'edit' ? $blog['file'] : asset('asset/img/350.png') }}"
                                alt="Preview" class="form-group mb-1" width="350">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="l0">Sampul</label>
                    <div class="col-md-10">
                        <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="l0">Judul</label>
                    <div class="col-md-10">
                        <input class="form-control" id="judul"
                            value="{{ $_GET['aksi'] == 'edit' ? $blog['judul'] : '' }}" name="judul" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="l0">Kategori</label>
                    <div class="col-md-10">
                        <select name="id_kategori" id="id_kategori" class="form-control">
                            <option value="">PIlih Kategori Blog..</option>
                            @foreach ($kategori as $kt)
                                <option value="{{ $kt['id'] }}"
                                    {{ $_GET['aksi'] == 'edit' && $blog['id_kategori'] == $kt['id'] ? 'selected' : '' }}>
                                    {{ $kt['nama'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="l0">Isi</label>
                    <div class="col-md-10">
                        <textarea name="isi" id="isi" class="editor form-control" rows="3">
                                                            {{ $_GET['aksi'] == 'edit' ? $blog['isi'] : '' }}
                                                        </textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="form-group row">
                        <input type="hidden" name="action" id="action"
                            value="{{ $_GET['aksi'] == 'edit' ? 'Edit' : 'Add' }}" />
                        <div class="col-sm-10 ml-auto btn-list">
                            <button type="submit" id="saveBtn" class="btn btn-info"><i class="far fa-save"></i>
                                Update</button>
                            <button type="reset" class="btn btn-danger"><i class="fas fa-bomb"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');
                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });

            $('body').on('submit', '#formBlog', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';
                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-blog_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-blog_update') }}";
                }

                var formData = new FormData(this);
                console.log(action_url);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.href = 'my_profile?method=blog&aksi=result';
                        } else {
                            $('#saveBtn').html('<i class="far fa-save"></i> Update');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('<i class="far fa-save"></i> Update');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });



        })
    </script>
@endsection
