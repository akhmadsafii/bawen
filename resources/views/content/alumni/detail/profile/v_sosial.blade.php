@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <h5 class="box-title mr-b-0">Sosial Media</h5>
    <hr>
    <button type="button" id="btnAdd" class="btn btn-success mb-3">
        <li class="fas fa-plus-circle"></li> Tambah
    </button>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-tabel">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sosial Media</th>
                    <th>Link URL</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalResult" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="resultForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_sosial">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jenis Sosial Media</label>
                                    <div class="col-sm-12">
                                        <select name="media" id="media" class="form-control">
                                            <option value="">Pilih jenis..</option>
                                            <option value="facebook">Facebook</option>
                                            <option value="instagram">Instagram</option>
                                            <option value="linkedin">Linkdln</option>
                                            <option value="twitter">Twitter</option>
                                            <option value="website">Website</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Link URL</label>
                                    <div class="col-sm-12">
                                        <input type="url" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="1">Aktif</option>
                                            <option value="2">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'media',
                        name: 'media',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'url',
                        name: 'url',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'text-center vertical-middle',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center vertical-middle',
                    },
                ]
            });

            $('#btnAdd').click(function() {
                $('#resultForm').trigger("reset");
                $('#modelHeading').html("Tambah Sosial Media");
                $('#modalResult').modal('show');
                $('#action').val('Add');
            });

            $('#resultForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-sosial_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-sosial_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalResult').modal('hide');
                            $('#resultForm').trigger("reset");
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-sosial_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Sosial Media");
                        $('#id_sosial').val(data.id);
                        $('#media').val(data.media).trigger('change');
                        $('#nama').val(data.nama);
                        $('#status').val(data.status).trigger('change');
                        $('#modalResult').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-sosial_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


        });
    </script>
@endsection
