@extends('content.alumni.detail.v_my_detail')
@section('content_profile')
    <style>
        .galeri-image {
            width: 190px;
            height: 200px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>
    <h5 class="box-title mr-b-0">Galeri User</h5>
    <hr>
    <div class="row">
        <div class="col-6 col-md-3 mb-2">
            <a class="thumbnail" id="createGaleri" href="javasript:void(0)">
                <div class="img-thumbnail galeri-image d-flex align-items-center  justify-content-center">
                    <i class="fas fa-plus fa-5x"></i>
                </div>
            </a>
        </div>
        @foreach ($alumni['galeri'] as $galeri)
            <div class="col-6 col-md-3 mb-2">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal"
                    data-title="{{ $galeri['nama'] . ' - ' . $galeri['kategori'] }}"
                    data-keterangan="{{ $galeri['keterangan'] }}" data-image="{{ $galeri['file'] }}"
                    data-target="#image-gallery">
                    <div class="img-thumbnail galeri-image" style="background-image: url({{ $galeri['file'] }});">
                    </div>
                </a>
                <div class="caption m-1">
                    <div class="row">
                        <div class="col-md-6  d-flex align-items-center">
                            <p class="m-0"><b>{!! Str::limit($galeri['nama'], 25, ' ...') !!}</b></p>
                        </div>
                        <div class="col-md-6  d-flex align-items-center">
                            <div class="aksi pull-right">
                                <a class="btn btn-info btn-sm edit" data-id="{{ $galeri['id'] }}"
                                    href="javascript:void(0)"><i class="fas fa-pencil-alt"></i></a>
                                <a class="btn btn-danger btn-sm delete" data-id="{{ $galeri['id'] }}"
                                    href="javascript:void(0)"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    <div class="p-3 m-3" style="border: 1px solid #e9ecef; border-radius: 6px; margin-top: 17px;">
                        <label for="">Keterangan :</label>
                        <p id="image-gallery-keterangan">ini adalah keterangan semua deskripis</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i
                            class="fa fa-arrow-left"></i>
                    </button>

                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i
                            class="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalGaleri" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formGaleri">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_galeri">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kategori</label>
                                    <div class="col-sm-12">
                                        <select name="id_kategori" id="id_kategori" class="form-control">
                                            <option value="">Pilih Kategori..</option>
                                            @foreach ($kategori as $kt)
                                                <option value="{{ $kt['id'] }}">{{ $kt['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="nama" id="nama" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" class="form-control"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Profile</label>
                                    <div class="col-sm-12">
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group mb-1" width="150">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        let modalId = $('#image-gallery');
        $(document).ready(function() {
            loadGallery(true, 'a.thumbnail');
            //This function disables buttons when needed
            function disableButtons(counter_max, counter_current) {
                $('#show-previous-image, #show-next-image')
                    .show();
                if (counter_max === counter_current) {
                    $('#show-next-image')
                        .hide();
                } else if (counter_current === 1) {
                    $('#show-previous-image')
                        .hide();
                }
            }

            function loadGallery(setIDs, setClickAttr) {
                let current_image,
                    selector,
                    counter = 0;

                $('#show-next-image, #show-previous-image')
                    .click(function() {
                        if ($(this)
                            .attr('id') === 'show-previous-image') {
                            current_image--;
                        } else {
                            current_image++;
                        }

                        selector = $('[data-image-id="' + current_image + '"]');
                        updateGallery(selector);
                    });

                function updateGallery(selector) {
                    let $sel = selector;
                    current_image = $sel.data('image-id');
                    $('#image-gallery-title')
                        .text($sel.data('title'));
                    $('#image-gallery-keterangan')
                        .text($sel.data('keterangan'));
                    $('#image-gallery-image')
                        .attr('src', $sel.data('image'));
                    disableButtons(counter, $sel.data('image-id'));
                }

                if (setIDs == true) {
                    $('[data-image-id]')
                        .each(function() {
                            counter++;
                            $(this)
                                .attr('data-image-id', counter);
                        });
                }
                $(setClickAttr)
                    .on('click', function() {
                        updateGallery($(this));
                    });
            }

            $('#createGaleri').click(function() {
                $('#formGaleri').trigger("reset");
                $('#modelHeading').html("Tambah Galeri");
                $('#modalGaleri').modal('show');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                $('#action').val('Add');
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-galeri_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Galeri");
                        $('#id_galeri').val(data.id);
                        $('#id_kategori').val(data.id_kategori).trigger('change');
                        $('#nama').val(data.nama);
                        $('#keterangan').val(data.keterangan);
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#modalGaleri').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });


            $('body').on('submit', '#formGaleri', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-galeri_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-galeri_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-galeri_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload();
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data
                                .icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        // build key actions
        $(document).keydown(function(e) {
            switch (e.which) {
                case 37: // left
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                        $('#show-previous-image')
                            .click();
                    }
                    break;

                case 39: // right
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                        $('#show-next-image')
                            .click();
                    }
                    break;

                default:
                    return; // exit this handler for other keys
            }
            e.preventDefault(); // prevent the default action (scroll / move caret)
        });
    </script>
@endsection
