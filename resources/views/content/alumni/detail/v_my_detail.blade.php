@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .profile-center {
            width: 248px;
            height: 248px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100%;
        }

        .pace {
            display: none;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-md-4 my-2">
            <div class="card">
                <div class="card-header bg-info text-center">
                    <b class="">TENTANG USER</b>
                </div>
                <div class="card-header text-center">
                    <b class="">{{ $alumni['nama'] }}</b>
                </div>
                <div class="card-body">
                    <center>
                        <div class="profile-center m-auto" style="background-image: url({{ $alumni['file'] }});">
                        </div>
                    </center>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Nama</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['nama'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">No Induk</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['no_induk'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">No Ijazah</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['no_ijazah'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Email</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['email'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Telepon</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['telepon'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Jurusan</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['jurusan'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Agama</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['agama'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">TTL</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">
                                {{ $alumni['tempat_lahir'] . ', ' . (new \App\Helpers\Help())->getTanggal($alumni['tgl_lahir']) }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Alamat</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['alamat'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label pull-right">Angkatan</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">
                                {{ $alumni['tahun_angkatan'] . ' - ' . $alumni['tahun_lulus'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Pendidikan Akhir</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext text-uppercase">{{ $alumni['pendidikan_terakhir'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Bio</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['bio'] }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">Login Terakhir</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">
                                {{ (new \App\Helpers\Help())->getHoursMinute($alumni['last_login']) }}</p>
                        </div>
                    </div>
                    <div class="form-group row m-0">
                        <label class="col-md-5 col-form-label">IP Adress</label>
                        <div class="col-md-7">
                            <p class="form-control-plaintext">{{ $alumni['last_ip'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 my-2">
            <div class="card">
                <div class="card-header bg-info">
                    <a href="{{ route('detail-profile_alumni', ['method' => 'profil']) }}"
                        class="btn btn{{ $_GET['method'] != 'profil' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-user"></i>
                        Profile</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'pendidikan']) }}"
                        class="btn btn{{ $_GET['method'] != 'pendidikan' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-graduation-cap"></i>
                        Pendidikan</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'pekerjaan']) }}"
                        class="btn btn{{ $_GET['method'] != 'pekerjaan' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-user-tie"></i>
                        Riwayat Pekerjaan</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'organisasi']) }}"
                        class="btn btn{{ $_GET['method'] != 'organisasi' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-users"></i>
                        Organisasi</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'agenda']) }}"
                        class="btn btn{{ $_GET['method'] != 'agenda' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-calendar"></i>
                        Agenda</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'sosial']) }}"
                        class="btn btn{{ $_GET['method'] != 'sosial' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-hashtag"></i>
                        Sosial</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'galeri']) }}"
                        class="btn btn{{ $_GET['method'] != 'galeri' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-images"></i>
                        Galeri</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'blog', 'aksi' => 'result']) }}"
                        class="btn btn{{ $_GET['method'] != 'blog' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="far fa-newspaper"></i>
                        Blog</a>
                    <a href="{{ route('detail-profile_alumni', ['method' => 'myjob']) }}"
                        class="btn btn{{ $_GET['method'] != 'myjob' ? '-outline' : '' }}-dark mt-1 btn-sm"><i
                            class="fas fa-suitcase"></i>
                        Pekerjaan</a>
                </div>
                <div class="card-body">
                    @yield('content_profile')
                </div>
            </div>

        </div>
    </div>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script> --}}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //Proses Update Profile

            //Proses Pendidikan
            $(document).on('click', '.btnAddPendidikan', function() {
                $('#formPendidikan').trigger("reset");
                $('#titlePendidikan').html("Tambah Pendidikan");
                $('#actionPendidikan').val('Add')
                $('#addPendidikan').collapse('show');
            })

            $(document).on('click', '.btnCancel', function() {
                $('#formPendidikan').trigger("reset");
                $('#addPendidikan').collapse('hide');
            })

            $('body').on('submit', '#formPendidikan', function(e) {
                e.preventDefault();
                $("#btnPendidikan").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnPendidikan").attr("disabled", true);
                var action_url = '';

                if ($('#actionPendidikan').val() == 'Add') {
                    action_url = "{{ route('alumni-pendidikan_create') }}";
                    method_url = "POST";
                }

                if ($('#actionPendidikan').val() == 'Edit') {
                    action_url = "{{ route('alumni-pendidikan_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            console.log("tes");
                            $('#addPendidikan').collapse('hide');
                            $('#formPendidikan').trigger("reset");
                            $('#data_pendidikan').html(data.pendidikan);
                        }
                        noti(data.icon, data.message);
                        $('#btnPendidikan').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnPendidikan").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPendidikan').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editPendidikan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-pendidikan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePendidikan').html("Edit Pendidikan");
                        $('#id_pendidikan').val(data.id);
                        $('#nama_sekolah').val(data.nama);
                        $('#jurusan').val(data.jurusan);
                        $('#jenjang').val(data.jenjang).trigger('change');
                        $('#tahun_masuk').val(data.th_masuk);
                        $('#tahun_keluar').val(data.th_lulus);
                        $('#alamat_sekolah').val(data.alamat);
                        $('#status_pendidikan').val(data.status).trigger('change');
                        $('#addPendidikan').collapse('show');
                        $('#actionPendidikan').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deletePendidikan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pendidikan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pendidikan').html(data.pendidikan);
                                $('#addPendidikan').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            //Detail Pekerjaan
            $(document).on('click', '.btnAddPekerjaan', function() {
                $('#formPekerjaan').trigger("reset");
                $('#titlePekerjaan').html("Tambah Pekerjaan");
                $('#actionPekerjaan').val('Add')
                $('#addPekerjaan').collapse('show');
            })

            $(document).on('click', '.cancelPekerjaan', function() {
                $('#formPekerjaan').trigger("reset");
                $('#addPekerjaan').collapse('hide');
            })

            $('body').on('submit', '#formPekerjaan', function(e) {
                e.preventDefault();
                $("#btnPekerjaan").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnPekerjaan").attr("disabled", true);
                var action_url = '';

                if ($('#actionPekerjaan').val() == 'Add') {
                    action_url = "{{ route('alumni-pekerjaan_create') }}";
                    method_url = "POST";
                }

                if ($('#actionPekerjaan').val() == 'Edit') {
                    action_url = "{{ route('alumni-pekerjaan_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addPekerjaan').collapse('hide');
                            $('#formPekerjaan').trigger("reset");
                            $('#data_pekerjaan').html(data.pekerjaan);
                        }
                        noti(data.icon, data.message);
                        $('#btnPekerjaan').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnPekerjaan").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPekerjaan').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editPekerjaan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-pekerjaan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePekerjaan').html("Edit Pekerjaan");
                        $('#id_pekerjaan').val(data.id);
                        $('#posisi').val(data.posisi);
                        $('#pekerjaan_industri').val(data.industri);
                        if (data.aktif == 1) {
                            $("#checkPekerjaan").prop("checked", true);
                            $('#pekerjaan_tanggal_mulai_on').val(data.tgl_mulai);
                            $("#aktifPekerjaan").show(); // checked
                            $('#notPekerjaan').hide();
                        } else {
                            $("#checkPekerjaan").prop("checked", false);
                            $('#tgl_mulai').val(data.tgl_mulai);
                            $('#tgl_selesai').val(data.tgl_selesai);
                            $("#aktifPekerjaan").hide(); // checked
                            $('#notPekerjaan').show();
                        }
                        $('#pekerjaan_keterangan').val(data.keterangan);

                        $('#addPekerjaan').collapse('show');
                        $('#actionPekerjaan').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deletePekerjaan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pekerjaan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pekerjaan').html(data.pekerjaan);
                                $('#addPekerjaan').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Organisasi
            $(document).on('click', '.btnAddOrganisasi', function() {
                $('#formOrganisasi').trigger("reset");
                $('#titleOrganisasi').html("Tambah Organisasi");
                $('#actionOrganisasi').val('Add')
                $('#addOrganisasi').collapse('show');
            })

            $(document).on('click', '.cancelOrganisasi', function() {
                $('#formOrganisasi').trigger("reset");
                $('#addOrganisasi').collapse('hide');
            })

            $('body').on('submit', '#formOrganisasi', function(e) {
                e.preventDefault();
                $("#btnOrganisasi").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnOrganisasi").attr("disabled", true);
                var action_url = '';

                if ($('#actionOrganisasi').val() == 'Add') {
                    action_url = "{{ route('alumni-organisasi_create') }}";
                    method_url = "POST";
                }

                if ($('#actionOrganisasi').val() == 'Edit') {
                    action_url = "{{ route('alumni-organisasi_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addOrganisasi').collapse('hide');
                            $('#formOrganisasi').trigger("reset");
                            $('#data_organisasi').html(data.organisasi);
                        }
                        noti(data.icon, data.message);
                        $('#btnOrganisasi').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnOrganisasi").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnOrganisasi').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editOrganiasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-organisasi_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleOrganisasi').html("Edit Organisasi");
                        $('#id_organisasi').val(data.id);
                        $('#posisi_organisasi').val(data.posisi);
                        $('#nama_organisasi').val(data.organisasi);
                        if (data.aktif == 1) {
                            $("#checkOrganisasi").prop("checked", true);
                            $('#organisasi_tanggal_mulai_on').val(data.tgl_mulai);
                            $("#aktifOrganisasi").show(); // checked
                            $('#notOrganisasi').hide();
                        } else {
                            $("#checkOrganisasi").prop("checked", false);
                            $('#organisasi_tgl_mulai').val(data.tgl_mulai);
                            $('#organisasi_tgl_selesai').val(data.tgl_selesai);
                            $("#aktifOrganisasi").hide(); // checked
                            $('#notOrganisasi').show();
                        }
                        $('#organisasi_keterangan').val(data.keterangan);

                        $('#addOrganisasi').collapse('show');
                        $('#actionOrganisasi').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteOrganisasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-organisasi_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_organisasi').html(data.organisasi);
                                $('#addOrganisasi').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Sosisal Media
            $(document).on('click', '.btnAddSosmed', function() {
                $('#formSosmed').trigger("reset");
                $('#titleSosmed').html("Tambah Sosial Media");
                $('#actionSosmed').val('Add')
                $('#addSosmed').collapse('show');
            })

            $(document).on('click', '.cancelSosmed', function() {
                $('#formSosmed').trigger("reset");
                $('#addSosmed').collapse('hide');
            })

            $('body').on('submit', '#formSosmed', function(e) {
                e.preventDefault();
                $("#btnSosmed").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnSosmed").attr("disabled", true);
                var action_url = '';

                if ($('#actionSosmed').val() == 'Add') {
                    action_url = "{{ route('alumni-sosial_create') }}";
                    method_url = "POST";
                }

                if ($('#actionSosmed').val() == 'Edit') {
                    action_url = "{{ route('alumni-sosial_update') }}";
                    method_url = "PUT";
                }

                // var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addSosmed').collapse('hide');
                            $('#formSosmed').trigger("reset");
                            $('#data_sosmed').html(data.sosial);
                        }
                        noti(data.icon, data.message);
                        $('#btnSosmed').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnSosmed").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSosmed').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editSosmed', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-sosial_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleOrganisasi').html("Edit Organisasi");
                        $('#id_sosmed').val(data.id);
                        $('#media_sosmed').val(data.media).trigger('change');
                        $('#nama_sosmed').val(data.nama);
                        $('#addSosmed').collapse('show');
                        $('#actionSosmed').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteSosmed', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-sosial_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_sosmed').html(data.sosial);
                                $('#addSosmed').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Agenda

            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function(e) {


            })
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "{{ route('alumni-get_agenda') }}",
                        type: "POST",
                        data: {
                            params: "auth"
                        },
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    isi: value['keterangan'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value[
                                        'keterangan'],
                                    file: value['file'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.isi != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' +
                            event.waktu +
                            '</p></td></tr></table>');
                        // element.children().last().append(
                        //     "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Isi : " +
                        //     event
                        //     .isi + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: '/admin/master/kalender_rombel/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' +
                            end + '&id=' + event.id + '&isi=' + event.isi,
                        type: "PUT",
                        success: function(data) {
                            noti(data.icon, data.success)
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    // console.log(event);
                    $('#actionAgenda').val('Edit');
                    $('#judul_agenda').val(event.title);
                    $('#tempat_agenda').val(event.tempat);
                    $('#titleAgenda').html('Edit Agenda');
                    $('#id_agenda').val(event.id);
                    $('#delAgenda').show();
                    $('#waktu_agenda').val(event.waktu);
                    $('#keterangan_agenda').val(event.keterangan),
                        $('#start').val($.fullCalendar.formatDate(event.start,
                            "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    if (event.file) {
                        $('#modal-previews').attr('src', event.file);
                    }
                    $('#agendaModal').modal('show');
                }
            });
            // calendar.render();
            $('#calendar').fullCalendar('render');
            $('.tabs .nav li a').click(function() {
                $('#calendar').fullCalendar('render');
            });



            $('body').on('submit', '#formModalAgenda', function(e) {
                e.preventDefault();
                $("#btnAgenda").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnAgenda").attr("disabled", true);
                let action_url = '';
                if ($('#actionAgenda').val() == 'Add') {
                    action_url = "{{ route('alumni-agenda_create') }}";
                }

                if ($('#actionAgenda').val() == 'Edit') {
                    action_url = "{{ route('alumni-agenda_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formModalAgenda').trigger("reset");
                            $('#agendaModal').modal('hide');
                            $('#calendar').fullCalendar('refetchEvents');
                        }
                        noti(data.icon, data.message);
                        $('#btnAgenda').html('Simpan');
                        $("#btnAgenda").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            //Detail Artikel
            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });

            $(document).on('click', '.btnAddArtikel', function() {
                $('#formArtikel').trigger("reset");
                $('#titleArtikel').html("Tambah Artikel");
                $('#actionArtikel').val('Add')
                $('#addArtikel').collapse('show');
            })

            $(document).on('click', '.cancelArtikel', function() {
                $('#formArtikel').trigger("reset");
                $('#addArtikel').collapse('hide');
            })

            $('body').on('submit', '#formArtikel', function(e) {
                e.preventDefault();
                $("#btnArtikel").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnArtikel").attr("disabled", true);
                var action_url = '';

                if ($('#actionArtikel').val() == 'Add') {
                    action_url = "{{ route('alumni-blog_create') }}";
                }

                if ($('#actionArtikel').val() == 'Edit') {
                    action_url = "{{ route('alumni-blog_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addArtikel').collapse('hide');
                            $('#formArtikel').trigger("reset");
                            $('#data_artikel').html(data.blog);
                        }
                        noti(data.icon, data.message);
                        $('#btnArtikel').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnArtikel").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnArtikel').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editArtikel', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-blog_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleArtikel').html("Edit Artikel");
                        $('#id_artikel').val(data.id);
                        $('#id_kategori_blog').val(data.id_kategori);
                        $('#judul_blog').val(data.judul);
                        tinymce.activeEditor.setContent(data.isi);
                        $('#addArtikel').collapse('show');
                        $('#actionArtikel').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteArtikel', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-data_blog_soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_artikel').html(data.blog);
                                $('#addArtikel').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            //Detail Galeri
            $(document).on('click', '.btnAddGaleri', function() {
                $('#formGaleri').trigger("reset");
                $('#titleGaleri').html("Tambah Galeri");
                $('#actionGaleri').val('Add')
                $('#addGaleri').collapse('show');
            })

            $(document).on('click', '.cancelGaleri', function() {
                $('#formGaleri').trigger("reset");
                $('#addGaleri').collapse('hide');
            })

            $('body').on('submit', '#formGaleri', function(e) {
                e.preventDefault();
                $("#btnGaleri").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnGaleri").attr("disabled", true);
                var action_url = '';

                if ($('#actionArtikel').val() == 'Add') {
                    action_url = "{{ route('alumni-galeri_create') }}";
                }

                if ($('#actionArtikel').val() == 'Edit') {
                    action_url = "{{ route('alumni-galeri_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addGaleri').collapse('hide');
                            $('#formGaleri').trigger("reset");
                            $('#data_galeri').html(data.galeri);
                        }
                        noti(data.icon, data.message);
                        $('#btnGaleri').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnGaleri").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnGaleri').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editGaleri', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-organisasi_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleGaleri').html("Edit Organisasi");
                        $('#id_galeri').val(data.id);
                        $('#posisi_organisasi').val(data.posisi);
                        $('#nama_organisasi').val(data.organisasi);

                        $('#organisasi_keterangan').val(data.keterangan);

                        $('#addGaleri').collapse('show');
                        $('#actionGaleri').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteGaleri', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-galeri_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_galeri').html(data.galeri);
                                $('#addGaleri').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function displayPekerjaan() {
            if ($("#checkPekerjaan").is(':checked')) {
                $("#aktifPekerjaan").show();
                $('#notPekerjaan').hide();
            } else {
                $("#aktifPekerjaan").hide();
                $('#notPekerjaan').show();
            }
        }

        function displayOrganisasi() {
            if ($("#checkOrganisasi").is(':checked')) {
                $("#aktifOrganisasi").show();
                $('#notOrganisasi').hide();
            } else {
                $("#aktifOrganisasi").hide();
                $('#notOrganisasi').show();
            }
        }

        function loader() {
            $("#btnAddProfile").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btnAddProfile").attr("disabled", true);
        }
    </script>
@endsection
