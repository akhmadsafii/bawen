<table style="width: 100%">
    @if (empty($alumni['organisasi']))
        @if (session('role') == 'admin-alumni')
            <tr>
                <td colspan="3">
                    <form id="formPendidikan" name="formPendidikan" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Institusi/
                                Sekolah</label>
                            <div class="col-md-9">
                                <input class="form-control" name="nama" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Jurusan</label>
                            <div class="col-md-9">
                                <input type="text" name="jurusan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Jenjang</label>
                            <div class="col-md-9">
                                <select name="jenjang" class="form-control" id="jenjang">
                                    <option value="s1">S1</option>
                                    <option value="d4">D4</option>
                                    <option value="d3">D3</option>
                                    <option value="sma">SMA Sederajat</option>
                                    <option value="smp">SMP Sederajat</option>
                                    <option value="sd">SD Sederajat</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Angkatan</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="tahun_masuk" id="tahun_masuk" class="form-control select3">
                                            <option value="">Tahun Masuk</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_keluar" class="form-control select3" id="tahun_keluar">
                                            <option value="">Tahun Lulus</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15"></label>
                            <div class="col-md-9">
                                <div class="button pull-right">
                                    <input type="hidden" name="action" id="action" value="Add" />
                                    <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                        value="create">Simpan</button>
                                    <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        @else
            <tr>
                <td style="text-align: center">Data Organisasi masih kosong</td>
            </tr>
        @endif
    @else
        @if (session('role') == 'admin-alumni')
            <tr>
                <td style="width: 20%">
                    <a href="javascript:void(0)" id="addOrganisasi" class="btn btn-outline-info mb-3">
                        <i class="fa fa-plus-circle"></i> Tambah Organisasi
                    </a>
                </td>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endif
        <tbody id="outputPengalaman" class="mt-4">
            @foreach ($alumni['organisasi'] as $org)
                <tr class="mt-2" style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                    <td style="vertical-align: middle; text-align: center" class="pr-2 pl-2">
                        {{ (new \App\Helpers\Help())->getMonthYear($org['tgl_mulai']) }} <p class="mb-0">-
                        </p>
                        {{ $org['aktif'] == 1 ? 'saat ini' : (new \App\Helpers\Help())->getMonthYear($org['tgl_selesai']) }}
                    </td>
                    <td style="vertical-align: middle">
                        <h5 class="mt-0">
                            <b>{{ strtoupper($org['posisi']) }}</b>
                        </h5>
                        <h5 class="mt-0">{{ strtoupper($org['organisasi']) }}
                        </h5>
                        <table style="width: 100%">
                            <tr>
                                <td>Keterangan : {{ ucwords($org['keterangan']) }}</td>
                            </tr>
                        </table>
                    <td>
                        @if (session('role') == 'admin-alumni')
                    <td style="text-align: right; vertical-align: middle" class="pr-2 pl-2">
                        <ul class="social-links list-inline mb-0 mt-2">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-info btn-sm editOrg" data-original-title="Edit Organisasi"
                                    data-id="{{ $org['id'] }}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-danger btn-sm deleteOrg" data-original-title="Hapus Organisasi"
                                    data-id="{{ $org['id'] }}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </li>
                        </ul>
                        {{-- <a href="javascript:void(0)" data-id="{{ $pnd['id'] }}"
                            class="editPendidikan"><i class="fa fa-edit"></i></a>
                        &nbsp; &nbsp;
                        <a href="javascript:void(0)"
                            onclick="deletePendidikan({{ $pnd['id'] }})"><i
                                class="fa fa-trash"></i></a> --}}
                    </td>
            @endif

            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
    @endforeach
    </tbody>
    @endif
</table>
