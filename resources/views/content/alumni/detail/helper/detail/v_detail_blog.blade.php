<section>
    <div class="container">

        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="input-group">
                    <div class="input-group-btn search-panel">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <span id="search_concept">Filter by</span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pt-0 pb-0" role="menu">
                            <li class="p-1" style="border: 1px solid #0075a9; background: #038fcd;">
                                <a href="#contains" style="color: #fff">Semua Kategori</a>
                            </li>
                            <li class="p-1" style="border: 1px solid #0075a9; background: #038fcd;"><a
                                    href="#its_equal" style="color: #fff">It's
                                    equal</a></li>
                            <li class="p-1" style="border: 1px solid #0075a9; background: #038fcd;"><a
                                    href="#greather_than" style="color: #fff">Greather than </a></li>
                            <li class="p-1" style="border: 1px solid #0075a9; background: #038fcd;"><a
                                    href="#less_than" style="color: #fff">Less than </a>
                            </li>
                            <li class="divider"></li>
                            <li class="p-1" style="border: 1px solid #0075a9; background: #038fcd;"><a
                                    href="#all" style="color: #fff">Anything</a>
                            </li>
                        </ul>
                    </div>
                    <input type="hidden" name="search_param" value="all" id="search_param">
                    <input type="text" class="form-control" name="x" placeholder="Search term...">
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="button"><span class="fas fa-search"></span>
                        </button>
                    </span>
                </div>
            </div>

            @if (!empty($alumni['blog']))
                @php
                    $first_blog = $alumni['blog'][0];
                    $first_blog_array = $alumni['blog'];
                    unset($first_blog_array[0]);
                @endphp
                @if (!empty($first_blog_array))
                    <div class="col-lg-7 mb-4">
                        <div id="avatar" style="height: 243px; width: 100%; position: relative">
                            <img class="img-fluid rounded mb-3" src="{{ $first_blog['avatar'] }}"
                                alt="A guide to building your online presence"
                                style="position: absolute;  max-height: -webkit-fill-available; margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center;">
                        </div>
                        <a href="#" class="mt-4 h2 text-dark">{{ $first_blog['judul'] }}</a>
                        <p class="mt-4">{{ $first_blog['isi'] }}.</p>

                        <div class="d-flex text-small">
                            <a href="#">{{ $first_blog['kategori'] }}</a>
                            <span class="text-muted ml-1">
                                {{ (new \App\Helpers\Help())->getDayMonth($first_blog['created_at']) }}</span>
                        </div>

                    </div>
                    <div class="col-lg-5 ">
                        <ul class="list-unstyled">
                            @foreach ($first_blog_array as $bl)
                                <li class="row mb-4">
                                    <a href="#" class="col-3">
                                        <img src="{{ $bl['avatar'] }}" alt="Image" class="rounded img-fluid"
                                            style="width: 55px">
                                    </a>
                                    <div class="col-9">
                                        <a href="#">
                                            <h6 class="mb-3 h5 text-dark">{{ $bl['judul'] }}</h6>
                                        </a>
                                        <div class="d-flex text-small">
                                            <a href="#">{{ $bl['kategori'] }}</a>
                                            <span
                                                class="text-muted ml-1">{{ (new \App\Helpers\Help())->getDayMonth($first_blog['created_at']) }}</span>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                @else
                    <div class="col-lg-12 mb-4">
                        <div id="avatar" style="height: 243px; width: 100%; position: relative">
                            <img class="img-fluid rounded mb-3" src="{{ $first_blog['avatar'] }}"
                                alt="A guide to building your online presence"
                                style="position: absolute;  max-height: -webkit-fill-available; margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center;">
                        </div>
                        <a href="#" class="mt-4 h2 text-dark">{{ $first_blog['judul'] }}</a>
                        <p class="mt-4">{{ $first_blog['isi'] }}.</p>

                        <div class="d-flex text-small">
                            <a href="#">{{ $first_blog['kategori'] }}</a>
                            <span class="text-muted ml-1">
                                {{ (new \App\Helpers\Help())->getDayMonth($first_blog['created_at']) }}</span>
                        </div>
                    </div>
                @endif
            @else
                @if (session('role') == 'admin-alumni')
                    <div class="col-md-12">
                        <form id="formAddBlog" class="form-horizontal">
                            <div class="form-group row">
                                <input type="hidden" name="byDetail" value="true">
                                <label class="col-md-3 col-form-label" for="l0">Kategori Blog</label>
                                <div class="col-md-9">
                                    <select name="kategori" class="form-control">
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach ($kategori as $kt)
                                            <option value="{{ $kt['id'] }}">{{ $kt['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Judul</label>
                                <div class="col-md-9">
                                    <input type="text" name="judul" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Isi</label>
                                <div class="col-md-9">
                                    <textarea name="isi" class="form-control isi_blog" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-3 col-form-label" for="l1"></label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6" style="position: relative">
                                            <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <input type="hidden" name="hidden_image" id="hidden_image">
                                    </div>
                                    <input type="hidden" name="old_image" id="old_image">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l15"></label>
                                <div class="col-md-9">
                                    <div class="button pull-right">
                                        <input type="hidden" name="action" id="actionBlog" value="Add" />
                                        <button type="submit" class="btn btn-outline-info" id="addBlog"
                                            value="create">Simpan</button>
                                        <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @else
                    <div class="col-md-12">
                        <p style="text-align: center">
                            Maaf, user belum punya blog yang dipost
                        </p>
                    </div>

                @endif
            @endif

        </div>
    </div>
</section>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('submit', '#formAddBlog', function(e) {
        e.preventDefault();
        $("#addBlog").html(
            '<i class="fa fa-spin fa-spinner"></i> Loading');
        $("#addBlog").attr("disabled", true);
        var action_url = '';

        if ($('#actionBlog').val() == 'Add') {
            action_url = "{{ route('alumni-blog_create') }}";
            method_url = "POST";
        }

        if ($('#actionBlog').val() == 'Edit') {
            action_url = "{{ route('alumni-blog_create') }}";
            method_url = "PUT";
        }
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            url: action_url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('.blog_result').html(
                    '<div id="loading" style="" ></div>');
            },
            success: (data) => {
                if (data.status == 'berhasil') {
                    $('#formAddBlog').trigger("reset");
                }
                $(".blog_result").load(window.location.href +
                    " .blog_result");
                noti(data.icon, data.success);
                $('#addBlog').html('Simpan');
                $("#addBlog").attr("disabled", false);

            },
            error: function(data) {
                console.log('Error:', data);
                $('#addBlog').html('Simpan');
            }
        });
    });
</script>
