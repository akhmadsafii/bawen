<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalBlog" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="titleBlog"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalBlog" action="javascript:void(0)" name="formModalBlog" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_blog">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Kategori</label>
                            <div class="col-md-9">
                                <select name="id_kategori" id="id_kategori" class="form-control">
                                    @foreach ($kategori as $kt)
                                        <option value="{{ $kt['id'] }}">{{ $kt['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Judul</label>
                            <div class="col-md-9">
                                <input type="text" name="judul" class="form-control" id="judul">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Isi</label>
                            <div class="col-md-9">
                                <textarea name="isi" id="isi" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-3 col-form-label" for="l1"></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group mb-1" width="100%" style="margin-top: 10px">
                                    </div>
                                    <div class="col-md-6" style="position: relative">
                                        <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*"
                                        onchange="readURL(this);">
                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                </div>
                                <input type="hidden" name="old_image" id="old_image">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionBlog" value="Add" />
                        <button type="submit" class="btn btn-info" id="btnBlog" value="create">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
