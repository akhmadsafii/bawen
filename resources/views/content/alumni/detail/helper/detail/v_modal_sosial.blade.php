<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalSosial" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="titleSosial"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalSosial" action="javascript:void(0)" name="formModalSosial" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_sosial">
                            <label class="col-md-3 col-form-label" for="l0">Media</label>
                            <div class="col-md-9">
                                <input class="form-control" name="media" id="media" readonly type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Nama</label>
                            <div class="col-md-9">
                                <input type="text" name="nama" class="form-control" id="sosial_nama">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" id="btnSosial" value="create">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

