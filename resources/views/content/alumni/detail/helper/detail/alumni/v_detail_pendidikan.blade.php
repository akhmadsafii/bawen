<table style="width: 100%">
    @if (empty($alumni['pendidikan']))
        @if (session('id') == $alumni['id'])
            <tr>
                <td colspan="3">
                    <form id="formPendidikan" name="formPendidikan" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Institusi/
                                Sekolah</label>
                            <div class="col-md-9">
                                <input class="form-control" name="nama" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Jurusan</label>
                            <div class="col-md-9">
                                <input type="text" name="jurusan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Jenjang</label>
                            <div class="col-md-9">
                                <select name="jenjang" class="form-control" id="jenjang">
                                    <option value="s1">S1</option>
                                    <option value="d4">D4</option>
                                    <option value="d3">D3</option>
                                    <option value="sma">SMA Sederajat</option>
                                    <option value="smp">SMP Sederajat</option>
                                    <option value="sd">SD Sederajat</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Angkatan</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="tahun_masuk" id="tahun_masuk" class="form-control select3">
                                            <option value="">Tahun Masuk</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_keluar" class="form-control select3" id="tahun_keluar">
                                            <option value="">Tahun Lulus</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15"></label>
                            <div class="col-md-9">
                                <div class="button pull-right">
                                    <input type="hidden" name="action" id="action" value="Add" />
                                    <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                        value="create">Simpan</button>
                                    <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        @else
            <tr>
                <td style="text-align: center">Data Pendidikan masih kosong</td>
            </tr>

        @endif
    @else
        {{-- @if (session('role') == 'admin-alumni') --}}
        <tr>
            <td style="width: 20%">
                <a href="javascript:void(0)" id="addPendidikan" onclick="addPendidikan()"
                    class="btn btn-outline-info mb-3">
                    <i class="fa fa-plus-circle"></i> Tambah Pendidikan
                </a>
            </td>
            <td></td>
            <td></td>
            <td>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @endif
    <tbody id="outputPengalaman" class="mt-4">
        @foreach ($alumni['pendidikan'] as $pnd)
            <tr class="mt-2" style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                <td style="vertical-align: middle" class="pr-2 pl-2">
                    {{ (new \App\Helpers\Help())->getMonthYear($pnd['th_masuk']) }}
                </td>
                <td style="vertical-align: middle">
                    <h5 class="mt-0">
                        <b>{{ strtoupper($pnd['nama']) }}</b>
                    </h5>
                    <h5 class="mt-0">{{ strtoupper($pnd['jenjang']) }}
                    </h5>
                    <table style="width: 100%">
                        <tr>
                            <td>jurusan : {{ ucwords($pnd['jurusan']) }}</td>
                        </tr>
                    </table>
                <td>
                    {{-- @if (session('role') == 'admin-alumni') --}}
                <td style="text-align: right; vertical-align: middle" class="pr-2 pl-2">
                    <ul class="social-links list-inline mb-0 mt-2">
                        <li class="list-inline-item">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                class="btn btn-info btn-sm editPendidikan" data-original-title="Edit Pendidikan"
                                data-id="{{ $pnd['id'] }}" onclick="editP({{ $pnd['id'] }})">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                class="btn btn-danger btn-sm deletePendidikan" data-original-title="Hapus Pendidikan"
                                data-id="{{ $pnd['id'] }}">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </li>
                    </ul>
                </td>
                {{-- @endif --}}
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        @endforeach
    </tbody>
    {{-- @endif --}}
</table>
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // pendidikan();

        function pendidikan() {
            $.ajax({
                url: "{{ route('alumni-pendidikan_by_alumni') }}",
                type: "GET",
                beforeSend: function() {
                    $('.pendidikan').html(
                        '<div id="loading"></div>');
                },
                success: function(data) {
                    $('.pendidikan').html(data);
                }
            })
        }

        $('#formPendidikan, #formModalPendidikan').on('submit', function(event) {
            event.preventDefault();
            // console.log("insput form");
            $("#saveBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('alumni-pendidikan_create') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('alumni-pendidikan_update') }}";
                method_url = "PUT";
            }
            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.pendidikan').html('<div id="loading" style="" ></div>');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formPendidikan').trigger("reset");
                        $('#formModalPendidikan').trigger("reset");
                        $('#modalAjax').modal('hide');
                    }
                    $(".pendidikan").load(window.location.href +
                        " .pendidikan");
                    $(".modal_pendidikan").load(window.location.href +
                        " .modal_pendidikan");
                    noti(data.icon, data.success);
                    $('#saveBtn').html('Simpan');
                    $("#saveBtn").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $(document).on('click', '.editPendidikan', function() {
            let id = $(this).data('id');
            let loader = $(this);
            $('#form_result').html('');
            $('#tambahPendidikan').remove();
            $.ajax({
                type: 'POST',
                url: "{{ route('alumni-pendidikan_edit') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(data) {
                    $(loader).html('<i class="fas fa-pencil-alt"></i>');
                    $('#modelTitle').html("Edit Pendidikan");
                    $('#id_pendidikan').val(data.id);
                    $('#nama_pendidikan').val(data.nama);
                    $('#jurusan_pendidikan').val(data.jurusan);
                    $('#jenjang_pendidikan').val(data.jenjang).trigger('change');
                    $('#tahun_masuk_pendidikan').val(data.th_masuk).trigger('change');
                    $('#tahun_keluar_pendidikan').val(data.th_lulus).trigger('change');
                    $('#alamat_pendidikan').val(data.alamat);
                    $('#action').val('Edit');
                    $('#modalAjax').modal('show');
                }
            });
        });

        $(document).on('click', '.deletePendidikan', function() {
            let id = $(this).data('id');
            let loader = $(this);
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('alumni-pendidikan_delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                        $('.pendidikan').html(
                            '<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $(".pendidikan").load(window.location.href +
                                " .pendidikan");
                            $(".modal_pendidikan").load(window.location.href +
                                " .modal_pendidikan");
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        });

    });

    function addPendidikan() {
        $('#saveBtn').val("create-Customer");
        $('#modelTitle').html("Tambah Pendidikan");
        $('#modalAjax').modal('show');
        $('#tambahPendidikan').remove();
        $('#action').val('Add');
    }
</script>
