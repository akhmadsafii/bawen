@extends('content.alumni.v_alumni')
@section('content_alumni')
    <div class="col-lg-8" style="display: flex; align-items: center; justify-content: center;">
        <div class="row">
            <div class="col-md-12" style="text-align: center; color: #e6614f ">
                <i class="fas fa-exclamation fa-4x"></i>
            </div>
            <div class="col-md-12" style="text-align: center; color: #e6614f">
                Silahkan pilih terlebih dahulu kategori pada alumni yang ingin dicari
            </div>
        </div>
    </div>
@endsection
