@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .dataTables_wrapper .dataTables_length {
            margin: 0 !important;
        }

        .profile-center {
            width: 248px;
            height: 248px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100%;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row py-5 px-4 my-1">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header bg-info text-center">
                    <b class="">TENTANG USER</b>
                </div>
                <div class="card-header text-center">
                    <b class="">{{ ucwords($alumni['nama']) }}</b>
                </div>
                <div class="card-body">
                    <div class="profile-center" style="background-image: url({{ $alumni['file'] }});">
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-9 my-1">
            <div class="card">
                <div class="card-header bg-info">
                    <b class="">PROFILE USER</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-stripped">
                                <tr>
                                    <td><b>Nama</b></td>
                                    <td>{{ ucwords($alumni['nama']) }}</td>
                                </tr>
                                <tr>
                                    <td><b>Email</b></td>
                                    <td>{{ $alumni['email'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Jurusan</b></td>
                                    <td>{{ $alumni['jurusan'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-stripped">
                                <tr>
                                    <td><b>Ponsel</b></td>
                                    <td>{{ $alumni['telepon'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Angkatan</b></td>
                                    <td>{{ $alumni['tahun_angkatan'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tahun Lulus</b></td>
                                    <td>{{ $alumni['tahun_lulus'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'pendidikan','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'pendidikan' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-graduation-cap"></i>
                                Pendidikan</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'pekerjaan','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'pekerjaan' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-user-tie"></i>
                                Pekerjaan</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'organisasi','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'organisasi' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-users"></i>
                                Organisasi</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'agenda','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'agenda' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-calendar"></i>
                                Agenda</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'sosial','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'sosial' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-hashtag"></i>
                                Sosial</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'galeri','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'galeri' ? '-outline' : '' }}-info mt-1"><i
                                    class="fas fa-images"></i>
                                Galeri</a>
                            <a href="{{ route('detail-alumni_alumni', ['method' => 'blog','key' => (new \App\Helpers\Help())->encode($alumni['id']),'name' => str_slug($alumni['nama'])]) }}"
                                class="btn btn{{ $_GET['method'] != 'blog' ? '-outline' : '' }}-info mt-1"><i
                                    class="far fa-newspaper"></i>
                                Blog</a>
                            <div class="row">
                                <div class="col-md-12 my-3">
                                    @yield('content_detail')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {
            var table = $('.data-tabel').DataTable();


            load_nav('profil', 1)
            $('#createData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Alumni");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#alumniForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('alumni-alumni_create') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#resultAlumni').html('<div id="loading" style="" ></div>');
                    },
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#alumniForm').trigger("reset");
                        }
                        $("#resultAlumni").load(window.location.href +
                            " #resultAlumni");
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });




        })

        function editLoad(id) {
            $(".btnEdit" + id).html('<i class="fa fa-spin fa-spinner"></i> Loading');
        }

        function changeStatus(val, id) {
            if (id == 1) {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Tidak Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="Tidak Aktif"]')
                    .removeClass('active').addClass('notActive');
            }
            updateStatus(val, id);
        }

        function updateStatus(value, id) {

            $.ajax({
                type: "POST",
                url: "{{ route('update_status-user_alumni') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.success, data.message);
                }
            });
        }



        function load_nav(params, id) {

            let action_url = '';
            if (params == 'profil') {
                action_url = "{{ route('detail-data_alumni_profile') }}";
            }
            if (params == 'blog') {
                action_url = "{{ route('alumni-data_blog_detail') }}";
            }
            if (params == 'pendidikan') {
                action_url = "{{ route('alumni-pendidikan_by_alumni') }}";
            }

            if (params == 'organisasi') {
                action_url = "{{ route('alumni-organisasi_detail') }}";
            }

            if (params == 'pekerjaan') {
                action_url = "{{ route('alumni-pekerjaan_detail_by_alumni') }}";
            }

            if (params == 'sosial') {
                action_url = "{{ route('alumni-sosial_detail') }}";
            }

            $.ajax({
                type: "POST",
                url: action_url,
                data: {
                    params: "alumni",
                    id: "{{ $alumni['id'] }}"
                },
                beforeSend: function() {
                    $("#detail" + id).html('<div id="loading"></div>');
                },
                success: function(data) {
                    $("#detail" + id).html(data);
                }
            });
        }

        // Form Organisasi

        function pencarianBlog(obj) {

            $.ajax({
                url: "{{ route('alumni-search_blog') }}",
                method: "POST",
                data: $("#" + obj.id).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $("#hasil_pencarian_blog").html('<div id="loading"></div>');
                    $("#btn-search").html('<span class="fa fa-spin fa-spinner"></span>');
                },
                success: function(data) {
                    $("#hasil_pencarian_blog").html(data);
                    // if (data.status == 'berhasil') {
                    // } else {
                    //     $("#hasil_pencarian_blog").load(window.location.href +
                    //         " #hasil_pencarian_blog");
                    // }
                    $("#btn-search").html('<span class="fas fa-search"></span>');
                    // noti(data.icon, data.message);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }


        function btnAddBlog() {
            $('#titleBlog').html("Tambah Blog");
            $('#modalBlog').modal('show');
            $('#actionBlog').val('Add');
            $('#delete_foto').html('');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        }

        function formTambahBlog(obj) {
            $("#addBlog").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            var formData = new FormData(obj);
            $.ajax({
                url: "{{ route('alumni-blog_create') }}",
                method: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                beforeSend: function() {
                    $("#detail3").html('<div id="loading"></div>');
                },
                success: function(data) {
                    $("#detail3").html(data.html);
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }
        //Blog Artikel

        // Agenda

        $(document).on('shown.bs.tab', 'a[id="detailAgenda"]', function(e) {
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "{{ route('alumni-get_agenda') }}",
                        type: "POST",
                        data: {
                            params: "alumni",
                            id: "{{ $alumni['id'] }}"
                        },
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    isi: value['keterangan'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value['keterangan'],
                                    file: value['file'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.isi != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' + event.waktu +
                            '</p></td></tr></table>');
                        // element.children().last().append(
                        //     "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Isi : " +
                        //     event
                        //     .isi + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: '/admin/master/kalender_rombel/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' +
                            end + '&id=' + event.id + '&isi=' + event.isi,
                        type: "PUT",
                        success: function(data) {
                            noti(data.icon, data.success)
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    console.log(event);
                    $('#actionAgenda').val('Edit');
                    $('#judul_agenda').val(event.title);
                    $('#tempat_agenda').val(event.tempat);
                    $('#titleAgenda').html('Edit Agenda');
                    $('#id_agenda').val(event.id);
                    $('#delAgenda').show();
                    $('#waktu_agenda').val(event.waktu);
                    $('#keterangan_agenda').val(event.keterangan);
                    $('#start').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    if (event.file) {
                        $('#modal-previews').attr('src', event.file);
                    }
                    $('#agendaModal').modal('show');
                }
            });

        })
    </script>
@endsection
