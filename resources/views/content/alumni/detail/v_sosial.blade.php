@extends('content.alumni.detail.v_detail')
@section('content_detail')
    <h5 class="box-title mr-b-0">Sosial Media</h5>
    <hr>
    <div class="row m-0">
        @if (!empty($alumni['sosial']))
            @foreach ($alumni['sosial'] as $sosial)
                @php
                    if ($sosial['media'] == 'facebook') {
                        $icon = 'fab fa-facebook-f';
                        $color = 'facebook';
                    } elseif ($sosial['media'] == 'instagram') {
                        $icon = 'fab fa-instagram';
                        $color = 'dribbble';
                    } elseif ($sosial['media'] == 'linkedin') {
                        $icon = 'fab fa-linkedin-in';
                        $color = 'linkedin';
                    } elseif ($sosial['media'] == 'twitter') {
                        $icon = 'fab fa-twitter';
                        $color = 'twitter';
                    } elseif ($sosial['media'] == 'website') {
                        $icon = 'fas fa-wifi';
                        $color = 'info';
                    }

                @endphp
                <div class="col-sm-6 col-md-4">
                    <div class="icon-box icon-box-left">
                        <a href="{{ $sosial['nama'] }}" target="_blank">
                            <header>
                                <i class="{{ $icon }} border-color-{{ $color }} text-{{ $color }}"></i>
                            </header>
                            <section>
                                <h5 class="text-capitalize m-0 text-{{ $color }}">{{ $sosial['media'] }}</h5>
                                <p class="text-muted m-0 text-{{ $color }}">{{ $sosial['nama'] }}</p>
                            </section>
                        </a>
                    </div>
                </div>
            @endforeach
        @else
            <center>
                Belum ada sosial media yang diunggah
            </center>
        @endif
    </div>
@endsection
