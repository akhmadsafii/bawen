@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">

                    <div class="row mt-3">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h3 class="box-title">{{ Session::get('title') }}</h3>
                            @if (session('role') == 'admin' || session('role') == 'bk')
                                <div class="tombol">
                                    <button class="btn btn-info" id="addAdmin"><i class="fas fa-user-plus"></i>
                                        Tambah</button>
                                    <button id="importSiswa" class="btn btn-purple"><i class="fas fa-file-import"></i>
                                        Import</button>
                                    <button id="trashSiswa" class="btn btn-warning"><i class="fas fa-trash-restore"></i>
                                        Sampah</button>
                                </div>
                            @endif

                        </div>
                    </div>
                    <hr>
                    <div class="m-portlet__body">
                        <div class="row justify-content-end">
                            <div class="col-md-3">
                                <form class="navbar-form pull-right" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-info vertical-middle">
                                                <th>No</th>
                                                <th>Profile</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (empty($admin))
                                                <tr>
                                                    <td colspan="8" class="text-center">Data saat ini kosong</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($admin as $adm)
                                                    @php
                                                        $status = 'Aktif';

                                                        $color = 'badge badge-success text-inverse';
                                                        if ($adm['status'] == '0') {
                                                            $status = 'Tidak Aktif';
                                                            $color = 'badge badge-danger text-inverse';
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td class="vertical-middle">{{ $no++ }}</td>

                                                        <td class="vertical-middle"><b>{{ ucwords($adm['nama']) }}
                                                                <br><small>Username : {{ $adm['username'] }}</small></b>
                                                        </td>
                                                        <td class="vertical-middle">{{ ucwords($adm['email']) }}</td>
                                                        <td class="vertical-middle">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                    {{ $adm['status'] == 1 ? 'checked' : '' }}
                                                                    class="admin_check" data-id="{{ $adm['id'] }}">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </td>
                                                        <td class="text-center vertical-middle">
                                                            @if (session('role') == 'admin' || session('role') == 'bk' || session('role') == 'learning-admin')
                                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                                    class="btn btn-info btn-sm edit"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                                    class="btn btn-danger btn-sm delete"><i class="far fa-trash-alt"></i></a>
                                                            @endif
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                    {!! $pagination !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalAdmin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAdmin" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_admin">
                            <label class="col-md-3 col-form-label" for="l2">Nama Admin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Admin"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Username</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-address-card"></i></i>
                                    </span>
                                    <input class="form-control" id="username" name="username" placeholder="username"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-envelope-open-text"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" placeholder="Email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Gender</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-venus-mars"></i>
                                    </span>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="">-- Pilih Jenkel --</option>
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">File</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-image"></i></span>
                                    <input id="image" type="file" name="image" accept="image/*" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="form_pass">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password_add" name="password" placeholder="Password"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                                <small class="text-danger d-none" id="notifPass">Tidak usah diisi bila tidak ingin merubah
                                    password</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-siswa') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.generate', function() {
                $('#password_add').val(Math.floor(Math.random() * 90000) + 10000);
            });


            $('#addAdmin').click(function() {
                $('#modelHeading').html("Tambah {{ session('title') }}");
                $('#notifPass').addClass('d-none');
                $('#modalAdmin').modal('show');
                $('#action').val('Add');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });

            $('body').on('submit', '#formAdmin', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-admin_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-admin_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                $('#notifPass').removeClass('d-none');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-admin_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-user-edit"></i>');
                        $('#modelHeading').html("Edit Data {{ session('title') }}");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#username').val(data.username);
                        $('#jenkel').val(data.jenkel_kode).trigger('change');
                        $('#agama').val(data.agama).trigger('change');
                        $('#email').val(data.email);
                        $('#modalAdmin').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-admin_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload();
                            }
                            swa(data.status + "!", data.message, data.success);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function changeStatus(val, id) {
            if (id == 1) {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Tidak Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="Tidak Aktif"]')
                    .removeClass('active').addClass('notActive');
            }
            updateStatus(val, id);
        }

        function updateStatus(value, id) {
            $.ajax({
                type: "POST",
                url: "{{ route('update_status-admin_alumni') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.success, data.message);
                }
            });
        }

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
