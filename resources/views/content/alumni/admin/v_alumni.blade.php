@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-4 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-8 col-12">
                <div class="pull-right">
                    @if (session('role') == 'admin-alumni')
                        <button class="btn btn-outline-info mt-1" id="addAlumni"><i class="fas fa-plus-circle"></i>
                            Tambah Data</button>
                        <button id="data_trash" class="btn btn-outline-success mt-1"><i class="fas fa-clipboard-check"></i>
                            Approve <span class="badge badge-pill bg-primary">2</span></button>
                    @endif
                    <button id="reload_page" class="btn btn-outline-purple mt-1"><i class="fas fa-sync-alt"></i>
                        Refresh Page</button>
                </div>
            </div>

        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-sm-9 col-md-9"></div>
                <div class="col-sm-3 col-md-3">
                    <form class="navbar-form" action="javascript:void(0)" role="search">
                        <div class="input-group">
                            @php
                                $serc = str_replace('-', ' ', $search);
                            @endphp
                            <input type="text" value="{{ $serc }}" id="search" name="search" class="form-control"
                                placeholder="Search">
                            <div class="input-group-btn">
                                <button id="fil" onclick="filter('{{ $routes }}')" type="submit"
                                    class="btn btn-info"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped table-bordered table-hover" id="table-data">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nomor Hp</th>
                            <th>Email</th>
                            <th>Jurusan</th>
                            <th>Alamat</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($data))
                            <tr>
                                <td colspan="7">Maaf data anda saat ini kosong</td>
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($data as $dt)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $dt['nama'] }}</td>
                                    <td>{{ $dt['telepon'] }}</td>
                                    <td>{{ $dt['email'] }}</td>
                                    <td>{{ $dt['jurusan'] }}</td>
                                    <td>{{ $dt['alamat'] }}</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm accordion-toggle"
                                            data-toggle="collapse" data-target="#demo{{ $no }}"><i
                                                class="fas fa-info-circle"></i></a>
                                        @if (session('role') == 'admin-alumni')
                                            <a href="javascript:void(0)" data-id="{{ $dt['id'] }}"
                                                class="btn btn-success btn-sm edit"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="javascript:void(0)" data-id="{{ $dt['id'] }}"
                                                class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="hiddenRow">
                                        <div class="accordian-body collapse" id="demo{{ $no }}">
                                            <table class="table">
                                                <tr class="bg-info">
                                                    <th colspan="5"><b><i class="fas fa-user-alt"></i> Profile Alumni</b>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>Nama</th>
                                                    <td>{{ ucwords($dt['nama']) }}</td>
                                                    <td></td>
                                                    <th>Email</th>
                                                    <td>{{ $dt['email'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>No Induk</th>
                                                    <td>{{ $dt['no_induk'] }}</td>
                                                    <td></td>
                                                    <th>Jurusan</th>
                                                    <td>{{ $dt['jurusan'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Telepon</th>
                                                    <td>{{ $dt['telepon'] }}</td>
                                                    <td></td>
                                                    <th>Jenis Kelamin</th>
                                                    <td>{{ $dt['jenkel'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Agama</th>
                                                    <td>{{ $dt['agama'] }}</td>
                                                    <td></td>
                                                    <th>Tahun Lulus</th>
                                                    <td>{{ $dt['tahun_lulus'] }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Tempat Lahir</th>
                                                    <td>{{ $dt['tempat_lahir'] }}
                                                    </td>
                                                    <td></td>
                                                    <th>Tanggal Lahir</th>
                                                    <td>{{ (new \App\Helpers\Help())->getTanggal($dt['tgl_lahir']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Pendidikan Terakhir</th>
                                                    <td>{{ $dt['pendidikan_terakhir'] }}
                                                    </td>
                                                    <td></td>
                                                    <th>Tahun Angkatan</th>
                                                    <td>{{ $dt['tahun_angkatan'] }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Nomor Ijazah</th>
                                                    <td>{{ $dt['no_ijazah'] }}
                                                    </td>
                                                    <td></td>
                                                    <th>Terakhir Login</th>
                                                    <td>{{ (new \App\Helpers\Help())->getTanggalLengkap($dt['last_login']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>BIO</th>
                                                    <td>{{ $dt['bio'] }}
                                                    </td>
                                                    <td></td>
                                                    <th>Login IP Adress</th>
                                                    <td>{{ $dt['last_ip'] }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Alamat</th>
                                                    <td colspan="4">{{ $dt['alamat'] }}
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                        @endif
                    </tbody>

                </table>
            </div>
            {!! $pagination !!}
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAlumni" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAlumni" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id_alumni">
                                    <label for="name" class="col-sm-12 control-label">Nama
                                        Alumni</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nomer
                                        Induk</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="no_induk" name="no_induk"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Telepon</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="telepon" name="telepon">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Email</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="email" name="email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tempat,
                                        Tgl Lahir</label>
                                    <div class="row pr-3 pl-3">
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jurusan</label>
                                    <div class="col-sm-12">
                                        <select name="id_jurusan" id="id_jurusan" class="form-control">
                                            <option value="">-- Pilih Jurusan --</option>
                                            @foreach ($jurusan as $jr)
                                                <option value="{{ $jr['id'] }}">
                                                    {{ $jr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jenis
                                        Kelamin</label>
                                    <div class="col-sm-12">
                                        <select name="jenkel" id="jenkel" class="form-control">
                                            <option value="l">Laki - laki</option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Agama</label>
                                    <div class="col-sm-12">
                                        <select name="agama" id="agama" class="form-control">
                                            <option value="islam">Islam</option>
                                            <option value="kristen">Kristen</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun
                                        Lulus</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tahun_lulus" id="tahun_lulus" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nomor
                                        Ijazah</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="no_ijazah" id="no_ijazah" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pendidikan Terakahir</label>
                                    <div class="col-sm-12">
                                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control">
                                            <option value="d3">D3</option>
                                            <option value="d4">D4</option>
                                            <option value="s1">S1</option>
                                            <option value="s2">S2</option>
                                            <option value="s3">S3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Angkatan</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tahun_angkatan" id="tahun_angkatan"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row pr-3 pl-3">
                                    <label class="col-md-12 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group" width="100%" style="margin-top: 10px">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Alamat</label>
                                    <div class="col-sm-12">
                                        <textarea name="alamat" class="form-control" id="alamat" rows="4"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="action" value="Add">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '#reload_page', function() {
                $('#reload_page').html('<i class="fa fa-spin fa-spinner"></i> Sedang Merefresh..')
                location.reload();
            })

            $(document).on('click', '#addAlumni', function() {
                $('#formAlumni').trigger("reset");
                $('#modelHeading').html("Tambah Alumni");
                $('#modalAlumni').modal('show');
                $('#action').val('Add');
            })

            $('body').on('submit', '#formAlumni', function(e) {
                e.preventDefault();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-alumni_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-alumni_update') }}";
                }

                var formData = new FormData(this);
                formData.append('from', "tabel");
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formAlumni').trigger("reset");
                            $('#modalAlumni').modal('hide');
                            location.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-detail_profile') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data");
                        $('#id_alumni').val(data.id);
                        $('#no_induk').val(data.no_induk);
                        $('#telepon').val(data.telepon);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tgl_lahir').val(data.tgl_lahir);
                        $('#jenkel').val(data.jenkel).trigger('change');
                        $('#tahun_lulus').val(data.tahun_lulus);
                        $('#email').val(data.email);
                        $('#id_jurusan').val(data.id_jurusan).trigger('change');
                        $('#agama').val(data.agama).trigger('change');
                        $('#no_ijazah').val(data.no_ijazah);
                        $('#alamat').val(data.alamat);
                        $('#tahun_angkatan').val(data.tahun_angkatan);
                        $('#pendidikan_terakhir').val(data.pendidikan_terakhir).trigger(
                            'change');
                        $('#nama').val(data.nama);
                        $('#modal-preview').attr('src', data.file);
                        $('#modalAlumni').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Menghapus..');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            }
                            $(loader).html('<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })


        function filter(routes) {
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>

@endsection
