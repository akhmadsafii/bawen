@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .photoGallery {
            padding: 30px 0px;
        }

        .photoGallery h2 {
            font-size: 40px;
            color: #2196F3;
        }

        .photoGallery p {
            font-size: 18px;
            color: #9e9e9e;
        }

        .gallery-columns {
            margin-top: 30px;
        }

        .gallery-columns .thumbnail {
            max-height: 235px;
            overflow: hidden;
            display: block;
            border-radius: 50px 0px 50px 0px;
            box-shadow: 0px 0px 6px 0px #777777;
            border: 1px solid #fff;
        }

        .gallery-columns a img {
            transition: 1s;
            cursor: zoom-in;
        }

        .gallery-columns .thumbnail:hover img {
            transform: scale(1.07);
        }

        #baguetteBox-overlay .full-image img {
            border: 3px solid #fff;
            border-radius: 10px;
        }

        .image-galery {
            width: 415px;
            height: 233px;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100%;
        }

    </style>
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.css">
    <div class="widget-bg photoGallery">
        <h2 class="text-center">{{ $kategori['nama'] }}</h2>
        <p class="text-center">Click on gallery image and view images as slides</p>
        <hr>
        <div class="gallery-columns">
            <div class="row">
                @if (!empty($galeri))
                    @foreach ($galeri as $glr)
                        <div class="col-6 col-sm-4 mb-4">
                            <a href="{{ $glr['file'] }}" class="thumbnail">
                                <div class="image-galery" style="background-image: url({{ $glr['file'] }});"></div>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <center>Tidak ada foto yang tersedia</center>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            baguetteBox.run('.gallery-columns');
        })
    </script>
@endsection
