@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Kategori Galeri</a>
                </li>
                <li class="breadcrumb-item active">Bootstrap Tables</li>
            </ol>
            @if (session('role') == 'admin-alumni')
            <div class="d-none d-sm-inline-flex justify-center align-items-center"><a
                    href="{{ route('alumni-galeri_data_approve') }}"
                    class="btn btn-outline-info mr-l-20 btn-sm btn-rounded hidden-xs hidden-sm ripple"
                    target="_blank">Galeri Belum Diapprove <span
                        class="badge badge-pill bg-primary">{{ $total }}</span></a>
                        {{-- class="badge badge-pill bg-primary">{{ $approve }}</span></a> --}}
            </div>
        @endif
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <p class="text-muted">Gambar Galeri berada di dalam Kategori.</p>
                        <hr>
                        <div class="row my-2">
                            <div class="col-md-2">
                                <a href="javascript:void(0)" class="btn btn-outline-info addKategori"><i
                                        class="fas fa-plus"></i>
                                    Kategori Galeri</a>
                            </div>
                            <div class="col-md-7"></div>
                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="vertical-middle">Nama</th>
                                        <th class="vertical-middle">Status</th>
                                        <th class="vertical-middle">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_kategori">
                                    @if (!empty($kategori))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($kategori as $kt)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $kt['nama'] }}</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" {{ $kt['status'] == 1 ? 'checked' : '' }}
                                                            class="kategori_check" data-id="{{ $kt['id'] }}">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" data-toggle="collapse"
                                                        data-target="#galeri{{ $kt['id'] }}"
                                                        class="btn btn-sm btn-purple"><i class="fas fa-image"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $kt['id'] }}"
                                                        class="btn btn-sm btn-info edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $kt['id'] }}"
                                                        class="btn btn-sm btn-danger delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="galeri{{ $kt['id'] }}">
                                                        <button class="btn btn-purple btn-sm my-3 pull-right"
                                                            onclick="addGaleri({{ $kt['id'] }})"><i
                                                                class="fas fa-plus-circle"></i>
                                                            Tambah Galeri</button>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="bg-purple text-white">
                                                                    <th class="text-center">No</th>
                                                                    <th class="text-center">Gambar</th>
                                                                    <th class="text-center">Deskripsi</th>
                                                                    <th class="text-center">Status</th>
                                                                    <th class="text-center">Published</th>
                                                                    <th class="text-center">Opsi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="dataGaleri{{ $kt['id'] }}">
                                                                @if (!empty($kt['galeri']))
                                                                    @php
                                                                        $nomer = 1;
                                                                    @endphp
                                                                    @foreach ($kt['galeri'] as $glr)
                                                                        <tr>
                                                                            <td>{{ $nomer++ }}</td>
                                                                            <td><img src="{{ $glr['file'] }}" alt=""
                                                                                    height="50">
                                                                            </td>
                                                                            <td>
                                                                                <b
                                                                                    class="text-dark">{!! $glr['nama'] !!}</b>
                                                                                <span class="text-dark">by
                                                                                    {{ $glr['pembuat'] }}</span>
                                                                                <br>
                                                                                <small class="m-0 text-dark">
                                                                                    {!! Str::limit($glr['keterangan'], 100, ' ...') !!}
                                                                                </small>
                                                                            </td>
                                                                            <td>
                                                                                <label class="switch">
                                                                                    <input type="checkbox"
                                                                                        {{ $glr['status'] == 1 ? 'checked' : '' }}
                                                                                        class="galeri_check"
                                                                                        data-id="{{ $glr['id'] }}">
                                                                                    <span class="slider round"></span>
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                {{ (new \App\Helpers\Help())->getTanggalLengkap($glr['created_at']) }}
                                                                                <br>
                                                                                <small>{{ $glr['dibuat'] }}</small>
                                                                            </td>
                                                                            <td>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $glr['id'] }}"
                                                                                    class="editGaleri btn btn-info btn-sm"><i
                                                                                        class="fas fa-pencil-alt"></i></a>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $glr['id'] }}"
                                                                                    data-kategori="{{ $kt['id'] }}"
                                                                                    class="btn btn-danger btn-sm deleteGaleri"><i
                                                                                        class="fas fa-trash-alt"></i></a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @else
                                                                    <tr>
                                                                        <td colspan="6" class="text-center">Data saat ini
                                                                            tidak tersedia</td>
                                                                    </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Data saat ini belum tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="widget-bg">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('alumni-galeri_data_approve') }}" class="btn btn-success pull-right">
                    <i class="fas fa-bell"></i> Belum Diapprove
                    <span class="badge badge-pill bg-primary">{{ $total }}</span>
                </a>
            </div>
        </div>
        <div class="row" id="result_galeri">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <div class="js--image-preview"></div>
                <div class="upload-options">
                    <label>
                        <input class="image-upload" />
                    </label>
                </div>
            </div>
            @foreach ($galeri as $gl)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <div class="galeri" style="position: relative">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal"
                            data-title="{{ $gl['nama'] . ' - ' . $gl['kategori'] }}"
                            data-keterangan="{{ $gl['keterangan'] }}" data-image="{{ $gl['file'] }}"
                            data-target="#image-gallery">
                            <div class="gambar" style="background:url({{ $gl['file'] }}); background-repeat: no-repeat;  background-position: center; height: 243px; width: 100%"></div>
                        </a>

                        <div class="row mr-0 ml-0" style="position: absolute; bottom: 0; left: 0; background: #13121259;">
                            <div class="col-md-3" style="display: flex; align-items: center; justify-content: center">
                                <img src="{{ $gl['avatar'] }}" alt="">
                            </div>
                            <div class="col-md-9 mr-0">
                                <h5 class="m-0" style="color: #fff;"><b>{{ $gl['pembuat'] }}</b></h5>
                                <h6 class="m-0" style="color: #fff">{{ $gl['role'] }}</h6>
                                <small class="m-0" style="color: #fff">{{ $gl['dibuat'] }}</small>
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="delete" data-id="{{ $gl['id'] }}">
                            <i class="fas fa-trash-alt fa-2x"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                        <div class="p-3 m-3"
                            style="border: 1px solid #e9ecef; border-radius: 6px; margin-top: 17px;">
                            <label for="">Keterangan :</label>
                            <p id="image-gallery-keterangan">ini adalah keterangan semua deskripis</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i
                                class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i
                                class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalKategori" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleKategori"></h5>
                </div>
                <form id="formKategori" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kategori">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama_kategori" name="nama[]"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris mt-2">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status_kategori" class="form-control">
                                            <option value="0">Disable</option>
                                            <option value="1" selected>Enable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action_kategori" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnKategori">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalGaleri" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleGaleri"></h5>
                </div>
                <form id="formGaleri" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_galeri">
                        <input type="hidden" name="id_kategori" id="id_kategori_galeri">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" class="form-control"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">File</label>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Aksi dari kategori
            $(document).on('click', '.addKategori', function() {
                $('#formKategori').trigger("reset");
                $('.tambahBaris').show('');
                $('#titleKategori').html("Tambah Kategori");
                $('#modalKategori').modal('show');
                $('#action_kategori').val('Add');
            });

            $(document).on('click', '.kategori_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-kategori_galeri_alumni') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-kategori_galeri_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_kategori').html(data.kategori);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            })

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-kategori_galeri_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleKategori').html("Edit Kategori");
                        $('#id_kategori').val(data.id);
                        $('#nama_kategori').val(data.nama);
                        $('#status_kategori').val(data.status);
                        $('#modalKategori').modal('show');
                        $('#action_kategori').val('Edit')
                    }
                });
            })

            $('#formKategori').on('submit', function(event) {
                event.preventDefault();
                $("#btnKategori").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#savbtnKategoriebtnKategoriBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action_kategori').val() == 'Add') {
                    action_url = "{{ route('alumni-kat_galeri_create') }}";
                    method_url = "POST";
                }

                if ($('#action_kategori').val() == 'Edit') {
                    action_url = "{{ route('alumni-kat_galeri_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalKategori').modal('hide');
                            $('#formKategori').trigger("reset");
                            $('#data_kategori').html(data.kategori);
                        }
                        $('#btnKategori').html('Simpan');
                        $("#btnKategori").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            // Aksi dari Galeri
            $('body').on('submit', '#formGaleri', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-galeri_create') }}";
                }

                // if ($('#action').val() == 'Edit') {
                //     action_url = "{{ route('alumni-kat_galeri_update') }}";
                // }

                let id_kategori = $('#id_kategori_galeri').val();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalGaleri').modal('hide');
                            $('#formGaleri').trigger("reset");
                            $('#dataGaleri' + id_kategori).html(data.galeri);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.deleteGaleri', function() {
                let id = $(this).data('id');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-galeri_delete') }}",
                        type: "POST",
                        data: {
                            id,
                            id_kategori
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataGaleri' + id_kategori).html(data.galeri);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            })

            $(document).on('click', '.galeri_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-galeri_change_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

        })

        function addGaleri(id) {
            $('#formGaleri').trigger("reset");
            $('#titleGaleri').html("Tambah Galeri");
            $('#modalGaleri').modal('show');
            $('#id_kategori_galeri').val(id);
            $('#action').val('Add');
        }

        $('.image-upload').click(function() {
            $('#Customer_id').val('');
            $('#CustomerForm').trigger("reset");
            $('.tambahBaris').show('');
            $('#modelHeading').html("Tambah {{ session('title') }}");
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        });



        // $(document).on('click', '.delete', function() {
        //     let id = $(this).data('id');
        //     let loader = $(this);
        //     swal({
        //         title: "Apa kamu yakin?",
        //         text: "ingin menghapus data ini!",
        //         type: "warning",
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!',
        //         cancelButtonText: 'No, cancel!',
        //         confirmButtonClass: 'btn btn-success',
        //         cancelButtonClass: 'btn btn-danger',
        //         buttonsStyling: false
        //     }).then(function() {
        //         $.ajax({
        //             url: "{{ route('alumni-galeri_delete') }}",
        //             type: "POST",
        //             data: {
        //                 id
        //             },
        //             beforeSend: function() {
        //                 $(loader).html(
        //                     '<i class="fa fa-spin fa-spinner"></i>');
        //                 $('#result_galeri').html(
        //                     '<div id="loading" style="" ></div>');
        //             },
        //             success: function(data) {
        //                 if (data.status == 'berhasil') {
        //                     location.reload();
        //                 } else {
        //                     $("#result_galeri").load(window.location.href +
        //                         " #result_galeri");
        //                 }
        //                 swa(data.status + "!", data.message, data.icon);
        //             }
        //         })
        //     }, function(dismiss) {
        //         if (dismiss === 'cancel') {
        //             swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
        //         }
        //     })
        // });
















        let modalId = $('#image-gallery');

        $(document)
            .ready(function() {

                loadGallery(true, 'a.thumbnail');

                //This function disables buttons when needed
                function disableButtons(counter_max, counter_current) {
                    $('#show-previous-image, #show-next-image')
                        .show();
                    if (counter_max === counter_current) {
                        $('#show-next-image')
                            .hide();
                    } else if (counter_current === 1) {
                        $('#show-previous-image')
                            .hide();
                    }
                }

                function loadGallery(setIDs, setClickAttr) {
                    let current_image,
                        selector,
                        counter = 0;

                    $('#show-next-image, #show-previous-image')
                        .click(function() {
                            if ($(this)
                                .attr('id') === 'show-previous-image') {
                                current_image--;
                            } else {
                                current_image++;
                            }

                            selector = $('[data-image-id="' + current_image + '"]');
                            updateGallery(selector);
                        });

                    function updateGallery(selector) {
                        let $sel = selector;
                        current_image = $sel.data('image-id');
                        $('#image-gallery-title')
                            .text($sel.data('title'));
                        $('#image-gallery-keterangan')
                            .text($sel.data('keterangan'));
                        $('#image-gallery-image')
                            .attr('src', $sel.data('image'));
                        disableButtons(counter, $sel.data('image-id'));
                    }

                    if (setIDs == true) {
                        $('[data-image-id]')
                            .each(function() {
                                counter++;
                                $(this)
                                    .attr('data-image-id', counter);
                            });
                    }
                    $(setClickAttr)
                        .on('click', function() {
                            updateGallery($(this));
                        });
                }
            });

        // build key actions
        $(document)
            .keydown(function(e) {
                switch (e.which) {
                    case 37: // left
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                            $('#show-previous-image')
                                .click();
                        }
                        break;

                    case 39: // right
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                            $('#show-next-image')
                                .click();
                        }
                        break;

                    default:
                        return; // exit this handler for other keys
                }
                e.preventDefault(); // prevent the default action (scroll / move caret)
            });

        function tes() {
            alert("hallo tes");
        }

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }
    </script>
@endsection
