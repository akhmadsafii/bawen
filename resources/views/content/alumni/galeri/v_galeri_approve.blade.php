@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <div class="row page-title clearfix">
        <div class="page-title-left">

        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('alumni-kategori_blog') }}">Kategori</a>
                </li>
                <li class="breadcrumb-item active">Belum di Aprove</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <hr>
                        <div class="row my-2">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                <thead>
                                    <tr>
                                        @if (session('role') == 'admin-alumni')
                                            <th class="vertical-middle"></th>
                                        @endif
                                        <th class="vertical-middle">File</th>
                                        <th class="vertical-middle">Deskripsi</th>
                                        <th class="vertical-middle">Published</th>
                                        @if (session('role') == 'admin-alumni')
                                            <th></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($galeri))
                                        @foreach ($galeri as $glr)
                                            <tr>
                                                @if (session('role') == 'admin-alumni')
                                                    <td class="vertical-middle text-center">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-success approve"
                                                            data-id="{{ $glr['id'] }}"><i class="fas fa-check-circle"></i></a>
                                                    </td>
                                                @endif
                                                <td><a href="{{ $glr['file'] }}" target="_blank"><img src="{{ $glr['file'] }}" alt="" height="70"></a></td>
                                                <td class="w-50">
                                                    <b>{{ $glr['nama'] }}</b>
                                                    <p>Keterangan : {{ $glr['keterangan'] != null ? $glr['keterangan'] : '-' }}</p>
                                                </td>

                                                <td class="vertical-middle">
                                                    {{ (new \App\Helpers\Help())->getTanggalLengkap($glr['created_at']) }}
                                                </td>
                                                @if (session('role') == 'admin-alumni')
                                                    <td class="vertical-middle">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                            data-id="{{ $glr['id'] }}"><i
                                                                class="fas fa-trash"></i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">Data Saat ini tidak tersedia
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {!! $pagination !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.approve', function() {
                let id = $(this).data('id');
                let status = 1;
                let loader = $(this);
                if (confirm('Apa kamu yakin ingin meng aprove artikel ini?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('alumni-galeri_change_status') }}",
                        data: {
                            id,
                            status
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            }
                        }
                    });
                }
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-galeri_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            })



        })

        function filter(routes) {
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }

        function loader() {
            $(".addBlog").attr("disabled", true);
            $(".addBlog").html(
                '<i class="fa fa-spin fa-spinner"></i> Memproses..');
        }
    </script>
@endsection
