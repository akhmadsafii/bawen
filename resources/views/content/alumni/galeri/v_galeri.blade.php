@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        #photo-gallery .state-thumb {
            max-height: 200px;
            overflow: hidden;
            border-radius: 10px;
        }

        #photo-gallery .state-thumb img {
            width: 100%;
            transition: 0.5s;
        }

        #photo-gallery .photo-frame:hover img {
            transform: scale(1.3, 1.3);

        }

        #photo-gallery h4 {
            margin: 10px 0px 0px;
            text-align: right;
        }

        #photo-gallery h4 a {
            color: #9c27b0;
            font-size: 20px;
        }

        #photo-gallery .photo-frame {
            border: 1px solid #cecece;
            padding: 15px;
            margin-bottom: 30px;
            border-radius: 10px;
            transition: 0.3s;
            background-color: #fff;
        }

        #photo-gallery .photo-frame:hover {
            box-shadow: 0px 0px 15px 0px #d0d0d0;
            border-color: #9c27b0;
        }

    </style>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <div class="widget-bg" id="photo-gallery">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="text-center">Photo Gallery</h2>
                    <p class="text-center mb-5"><i>Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy text</i></p>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach ($kategori as $kt)
                <div class="col-md-6 col-lg-4">
                    <a
                        href="{{ route('alumni-galeri_detail_by-kategori', ['key' => (new \App\Helpers\Help())->encode($kt['id']),'kategori' => str_slug($kt['nama'])]) }}">
                        <div class="photo-frame">
                            <div class="state-thumb">
                                <img src="{{ $kt['galeri_sampul'] != null ? $kt['galeri_sampul']['file'] : asset('asset/img/foundit.png') }}"
                                    class="img-fluid">
                            </div>
                            <h4 class="text-capitalize">{{ $kt['nama'] }}({{ $kt['jumlah_galeri'] }})</h4>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
@endsection
