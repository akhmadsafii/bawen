@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == 'e_commerce')
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == 'real_state')
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == 'university')
    @php
        $ext = '_university';
    @endphp
@elseif($template == 'default')
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .dataTables_wrapper .dataTables_length {
            margin: 0 !important;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="widget-bg">
        <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
        <hr>
        <center>
            <div class="padded-reverse" id="chart" style="height: 400px; width: 100%;">
            </div>
        </center>

        <div class="aksi my-3">
            <b>FILTER BERDASARKAN KATEGORI : </b><br>
            <a href="{{ route('alumni-pekerjaan_alumni_tracking', ['by' => 'all', 'key' => 'null']) }}"
                class="btn btn{{ $_GET['by'] != 'all' ? '-outline' : '' }}-info mt-1 btn-sm"><i
                    class="fas fa-align-justify"></i>
                Semua</a>
            @foreach ($jenis as $jn)
                @if ($jn['jumlah_pekerja'] != 0)
                    <a href="{{ route('alumni-pekerjaan_alumni_tracking', ['by' => 'kategori','key' => (new \App\Helpers\Help())->encode($jn['id']),'kategori' => str_slug($jn['nama'])]) }}"
                        class="btn btn{{ $_GET['key'] != (new \App\Helpers\Help())->encode($jn['id']) ? '-outline' : '' }}-info mt-1 btn-sm"><i
                            class="fas fa-user-tie"></i>
                        {{ $jn['nama'] }} ({{ $jn['jumlah_pekerja'] }})</a>
                @endif
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12">
                @yield('content_tracking')
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var dataPoints = [];
            var chart = new CanvasJS.Chart("chart", {
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: "Pekerjaan Alumni"
                },
                // subtitles: [{
                //     text: "Currency Used: Thai Baht (฿)"
                // }],
                data: [{
                    type: "pie",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - #percent%",
                    yValueFormatString: "##0",
                    dataPoints: dataPoints
                }]
            });
            chart.render();

            $.getJSON("{{ route('admin_alumni-statistic') }}", addData);

            function addData(data) {
                for (var i = 0; i < data.length; i++) {
                    dataPoints.push({
                        label: data[i].label,
                        y: data[i].y
                    });
                }
                chart.render();
            }
        });
    </script>
@endsection
