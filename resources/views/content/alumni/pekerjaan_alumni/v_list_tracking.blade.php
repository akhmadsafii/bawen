@extends('content.alumni.pekerjaan_alumni.v_tracking')
@section('content_tracking')
    <div class="widget-bg">
        <div class="m-portlet__body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="data-tabel">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Alumni</th>
                            <th>Jenis</th>
                            <th>Posisi</th>
                            <th>Industri</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'alumni',
                        name: 'alumni'
                    },
                    {
                        data: 'jenis_pekerjaan',
                        name: 'jenis_pekerjaan'
                    },
                    {
                        data: 'posisi',
                        name: 'posisi'
                    },
                    {
                        data: 'industri',
                        name: 'industri'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                ]
            });
        })
    </script>
@endsection
