@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .pricing6 {
            font-family: "Montserrat", sans-serif;
            color: #8d97ad;
            font-weight: 300;
        }

        .pricing6 h1,
        .pricing6 h2,
        .pricing6 h3,
        .pricing6 h4,
        .pricing6 h5,
        .pricing6 h6 {
            color: #3e4555;
        }

        .pricing6 .font-weight-medium {
            font-weight: 500;
        }

        .pricing6 .bg-light {
            background-color: #f4f8fa !important;
        }

        .pricing6 h5 {
            line-height: 22px;
            font-size: 18px;
        }

        .pricing6 .subtitle {
            color: #8d97ad;
            line-height: 24px;
        }

        .pricing6 .card.card-shadow {
            -webkit-box-shadow: 0px 0px 30px rgba(115, 128, 157, 0.1);
            box-shadow: 0px 0px 30px rgba(115, 128, 157, 0.1);
        }

        .pricing6 .price-box sup {
            top: -20px;
            font-size: 16px;
        }

        .pricing6 .price-box .display-5 {
            line-height: 58px;
            font-size: 3rem;
        }

        .pricing6 .btn-info-gradiant {
            background: #188ef4;
            background: -webkit-linear-gradient(legacy-direction(to right), #188ef4 0%, #316ce8 100%);
            background: -webkit-gradient(linear, left top, right top, from(#188ef4), to(#316ce8));
            background: -webkit-linear-gradient(left, #188ef4 0%, #316ce8 100%);
            background: -o-linear-gradient(left, #188ef4 0%, #316ce8 100%);
            background: linear-gradient(to right, #188ef4 0%, #316ce8 100%);
        }

        .pricing6 .btn-info-gradiant:hover {
            background: #316ce8;
            background: -webkit-linear-gradient(legacy-direction(to right), #316ce8 0%, #188ef4 100%);
            background: -webkit-gradient(linear, left top, right top, from(#316ce8), to(#188ef4));
            background: -webkit-linear-gradient(left, #316ce8 0%, #188ef4 100%);
            background: -o-linear-gradient(left, #316ce8 0%, #188ef4 100%);
            background: linear-gradient(to right, #316ce8 0%, #188ef4 100%);
        }

        .pricing6 .btn-md {
            padding: 15px 45px;
            font-size: 16px;
        }

        .pricing6 .text-info {
            color: #188ef4 !important;
        }

        .pricing6 .badge-danger {
            background-color: #ff4d7e;
        }

        .pricing6 .font-14 {
            font-size: 14px;
        }

    </style>
    <div class="widget-bg">
        <h4>Kalender</h4>
        <div class="content">
            <div id="calendar">

            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="agendaModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleAgenda"></h5>
                </div>
                <div class="dataModal" style="width: 100%;">
                    <form id="formModalAgenda" action="javascript:void(0)" name="formModalAgenda" class="form-horizontal">
                        <div class="modal-body">
                            <span id="form_result"></span>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Judul</label>
                                <div class="col-md-9">
                                    <input type="text" name="judul_agenda" class="form-control" id="judul_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Tempat</label>
                                <div class="col-md-9">
                                    <input type="text" name="tempat_agenda" class="form-control" id="tempat_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Waktu</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="id" id="id_agenda">
                                    <input type="hidden" name="start" id="start">
                                    <input type="hidden" name="end" id="end">
                                    <input type="time" name="waktu_agenda" class="form-control" id="waktu_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                                <div class="col-md-9">
                                    <textarea name="keterangan_agenda" id="keterangan_agenda" class="form-control"
                                        rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-3 col-form-label" for="l1"></label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="modal-previews" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6" style="position: relative">
                                            <div id="delete_foto_agenda" style="position: absolute; bottom: 0"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURLS(this);">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="actionAgenda" id="actionAgenda" value="Add" />
                            <button type="submit" class="btn btn-info" id="btnAgenda" value="create">Simpan</button>
                            <a href="javascript:void(0)" class="btn btn-danger" id="delAgenda" onclick="deleteAgenda()"><i
                                    class="fas fa-exclamation"></i> Hapus Agenda</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="detailAgenda" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingAgenda"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing6 py-5 bg-light">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 text-center">
                                            <h3 class="mb-3" id="title_detail">Pricing to make your Work Effective
                                            </h3>
                                            <h6 class="subtitle font-weight-normal" id="keterangan_detail">We offer 100%
                                                satisafaction and Money
                                                back Guarantee</h6>
                                        </div>
                                    </div>
                                    <!-- row  -->
                                    <div class="row mt-4">
                                        <!-- column  -->
                                        <div class="col-md-6">
                                            <div class="card card-shadow border-0 mb-4">
                                                <div class="card-body p-4">
                                                    <img id="previews" src="https://via.placeholder.com/728x90.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card card-shadow border-0 mb-4">
                                                <div class="card-body p-4">
                                                    <div class="d-flex align-items-center">
                                                        <h5 class="font-medium m-b-0">Info Selengkapnya dari Agenda</h5>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-5 text-center">
                                                            <div class="price-box my-3"><span class="text-dark display-5"
                                                                    id="dateOnly">26</span>
                                                                <h6 class="font-weight-light" id="monthOnly">NOVEMBER</h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-7 align-self-center">
                                                            <ul
                                                                class="list-inline pl-3 font-14 font-weight-medium text-dark">
                                                                <li class="py-2">
                                                                    <i class="fas fa-clock"></i>
                                                                    <span id="waktu">24 : 00</span>
                                                                </li>
                                                                <li class="py-2">
                                                                    <i class="fas fa-map-signs"></i>
                                                                    <span id="tempat">Dimana</span>
                                                                </li>
                                                                <li class="py-2">
                                                                    <i class="fas fa-calendar-alt"></i>
                                                                    <span id="tgl_mulai">Diet Plan Included </span> -
                                                                    <span id="tgl_selesai">Diet Plan Included </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <table>
                                                <tr>
                                                    <td rowspan="2" style="width: 30%"><img id="avatars" style="width: 100px" src="https://via.placeholder.com/728x90.png" alt="" style="border-radius: 50%"></td>
                                                    <td><span id="pembuat">Paijo</span></td>
                                                </tr>
                                                <tr>
                                                    <td>diposting : <span id="diposting"></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!-- column  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "",
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value['keterangan'],
                                    file: value['file'],
                                    dayOnly: value['day_only'],
                                    monthOnly: value['month_only'],
                                    avatar: value['avatar'],
                                    dibuat: value['dibuat'],
                                    pembuat: value['pembuat'],
                                    mulaiIndonesia: value[
                                        'tgl_mulai_indonesia'],
                                    selesaiIndonesia: value[
                                        'tgl_selesai_indonesia'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.title != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' + event.waktu +
                            '</p></td></tr></table>');
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventClick: function(event, jsEvent, view) {
                    console.log(event);
                    $('#title_detail').html(event.title);
                    $('#keterangan_detail').html(event.keterangan);
                    $('#HeadingAgenda').html('Detail Agenda');
                    $('#waktu').html(event.waktu);
                    $('#tempat').html(event.tempat);
                    $('#dateOnly').html(event.dayOnly);
                    $('#monthOnly').html(event.monthOnly);
                    $('#tgl_mulai').html(event.mulaiIndonesia);
                    $('#tgl_selesai').html(event.selesaiIndonesia);
                    $('#avatars').attr('src', event.avatar);
                    $('#pembuat').html(event.pembuat);
                    $('#diposting').html(event.dibuat);
                    if (event.file != null) {
                        $('#previews').attr('src', event.file);
                    } else {
                        $('#previews').attr('src', 'https://via.placeholder.com/728x90.png');
                    }
                    $('#detailAgenda').modal('show');
                }
            });

            $('body').on('submit', '#formModalAgenda', function(e) {
                e.preventDefault();
                $("#btnAgenda").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnAgenda").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('alumni-agenda_create') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formModalAgenda').trigger("reset");
                            $('#agendaModal').modal('hide');
                            $('#calendar').fullCalendar('refetchEvents');
                            noti(data.icon, "Agenda anda berhasil di ajukan. Sedang menunggu persetujuan admin untuk ditampilkan");
                        }else{
                            noti(data.icon, data.message);
                        }
                        $('#btnAgenda').html('Simpan');
                        $("#btnAgenda").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        })
    </script>

@endsection
