<x-spp.layout>
    <div class="row">
        <div class="col-md-4 widget-holder">
            <div class="widget-bg bg-facebook"style="height: 310px">
                <div class="widget-body clearfix text-inverse" >
                    <h5>Konten Dashboard Program SPP user "{{ Session('role') }}" #1</h5>
                </div>
            </div>
        </div>

        <div class="col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    @if (Session('role') === 'tata-usaha')
                    <div class="col-6">
                        <h4>Daftar Jurusan</h4>

                            <div class="d-flex align-items-center form-group mx-sm-3 mb-2 input-has-value">
                                <label for="inputJurusan" class="sr-only">Input Jurusan</label>
                                <input type="text" name="nama" class="form-control" id="inputJurusan" placeholder="">
                                <input type="hidden" name="jurusan" class="form-control ml-1" id="inputID" placeholder="{{ Session('id_sekolah') }}" value="{{ Session('id_sekolah') }}">
                                &nbsp;
                                <a onclick="addJurusan()" class="btn btn-sm btn-success">Add </a>
                            </div>

                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <ul id="listJurusan" class="list-group list-group-flush">
                                @foreach($data_jurusan as $jurusan)
                                <!-- <div id="itemJurusan" class="d-flex justify-content-between align-items-center">
                                    <li id="itemNama" class="list-group-item text-secondary">{{$jurusan["nama"]}}</li>

                                    <a id="itemID" onclick="delJurusan( {{ $jurusan["id"] }} )" id="del-btn" class="btn btn-sm btn-warning">Hapus </a>


                                </div> -->
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    @elseif (Session('role') === 'siswa')

                    Tagihan Terakhir show data

                    @endif

                    <div class="col-6 bg-secondary"></div>
                </div>
            </div>
        </div>
    </div>
    @push('js_custom')

    <script>
        (function($, global, undefined) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}',
                    'Authorization': `Bearer {{Session::get('token')}}`

                }
            });

            $(document).ready(function () {
                renderJurusan();
            });


        })(jQuery, window);

        function renderJurusan(){
            $.ajax({
                type: "GET",
                url : "http://localhost/smart_school/api/data/master/jurusan/profile",
                success: function(dat) {
                    $('#listJurusan').empty();
                    var x;
                    let nama;
                    let ID;
                    let datas = dat.data;;
                    //console.log(datas[0]);
                    for(x = 0; x<datas.length ; x++){
                        console.log(datas[x].nama);
                        nama = datas[x].nama;
                        ID = datas[x].id;
                        $('#listJurusan').append(`
                                <div id="itemJurusan" class="d-flex justify-content-between align-items-center">
                                    <li id="itemNama" class="list-group-item text-secondary">${nama}</li>
                                    <a id="itemID" onclick="delJurusan(${ID})" id="del-btn" class="btn btn-sm btn-warning">Hapus </a>
                                </div>
                        `);
                    }
                }
            })
        }


        function addJurusan(){
            let formData = {
                "nama" : $('#inputJurusan').val(),
                "id_sekolah" : $('#inputID').val(),
                "status" : 1
            }
            $.ajax({
                type: "POST",
                url : "{{ route('store-jurusan') }}",
                data : formData,
                dataType: 'json',
                success: function(data) {
                        console.log("berhasil");
                        $('#inputJurusan').val("");
                        renderJurusan();
                },
                error: function(error) {
                    let ParseData = JSON.parse(JSON.stringify(error['responseJSON']));
                    let field_required = ParseData.errors['nama'];
                    let message  = ParseData.message;
                    console.log(field_required);

                    swal(
                        'Alert!',
                        message + '' + field_required,
                        'error'
                        );

                }
            })
        }

        function delJurusan(id){
                console.log(id);

                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete)=>{
                    if (willDelete) {
                     $.ajax({
                            url: "http://localhost/smart_school/api/data/master/jurusan/delete/" + id ,
                            type: "DELETE",
                            data: {
                                '_method': 'DELETE'
                            },

                            success: function(data) {
                                console.log("berhasil");
                                swal(
                                    'Deleted!',
                                    'Your data has been deleted.',
                                    'success'
                                )
                                renderJurusan();
                            },error:function(data){
                                swal(
                                    'Cancelled',
                                    'Proses Penghapusan Gagal :)',
                                    'error'
                                )
                            }
                        })
                    }else{
                        swal("Your data is safe!");
                    }

                });


        }

    </script>
    @endpush
</x-spp.layout>
