<x-spp.layout current-page="Transaksi">
@push('gaya_custom')
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
/* ini gaya custom */
</style>
@endpush

<div class="row">
    <div class="col-md-8 mx-auto widget-holder padded-reversed">
        <div class="widget-bg">
            <div class="widget-body">
                <h5 class="box-title">Input Transaksi Pembayaran</h5>
                <p>Pilih siswa untuk menginputkan transaksi manual.</p>

                <form id="form-search-siswa">
                    <div class="row mr-t-50 clearfix">
                        <div class="col-md-8">
                            <div class="form-input-icon d-flex">
                                <i class="list-icon material-icons fs-28">search</i>
                                <input
                                    type="search"
                                    id="pencarian"
                                    name="pencarian"
                                    class="form-control fs-22 fw-300 border-0"
                                    placeholder="Cari siswa..."
                                >
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <select id="jenis" name="jenis" class="form-control">
                                    <option value="nama" selected="selected">Nama</option>
                                    <option value="nis">NIS</option>
                                    <option value="id_kelas">Kelas</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <button type="submit" class="btn btn-outline-default btn-block">
                                <i class="list-icon material-icons">search</i>
                            </button>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table id="tabel-siswa" class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="pd-l-20" style="width: 240px">NIS</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th class="text-center" style="width: 120px"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.widget-body -->
        </div>
    </div>

</div>

<!-- Modal -->
<!-- Create Transaksi / Bayar Manual -->
<div
    id="modal-create-transaksi"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <i class="list-icon material-icons text-muted">add_circle</i>
                    Transaksi
                    <small class="text-muted">Tambahkan transaksi.</small>
                </h5>

                <form id="form-create-transaksi" class="form-material">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="id_kelas_siswa"
                                    id="field-kelas-siswa-create"
                                    class="form-control"
                                    data-placeholder="Pilih Siswa"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataSiswa as $siswa)
                                    <option value="{{ $siswa['id'] }}">{{ $siswa['nama'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-kelas-siswa-create">Siswa</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <input
                                    class="form-control"
                                    id="field-nis-create"
                                    name="nis"
                                    placeholder="Nomor induk siswa..."
                                    type="text"
                                >
                                <label for="field-nis-create">Nomor Induk Siswa</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="tahun_ajaran"
                                    id="field-tahun-ajaran-create"
                                    class="form-control"
                                    data-placeholder="Pilih Tahun Ajaran"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataTahunAjaran as $tahun)
                                    <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-tahun-ajaran-create">Tahun Ajaran</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input
                                    id="field-tgl-bayar-create"
                                    name="tgl_bayar"
                                    data-field="tgl-bayar"
                                    class="form-control"
                                    placeholder="Tanggal bayar..."
                                    type="text"
                                >
                                <label for="field-tgl-bayar-create">Tanggal Bayar</label>
                            </div>
                        </div>
                    </div>

                    <hr class="mr-tb-50" />
                    <h5 class="mr-b-20">Rincian Pembayaran</h5>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="list-details" class="table table-responsive">
                                <tbody class="list" rel="line-list"></tbody>
                            </table>

                            <div rel="line-action-editor">
                                <u>
                                    <a href="javascript:void(0)">
                                        Tambahkan item pembayaran
                                    </a>
                                </u>
                            </div>
                        </div>
                    </div>

                    <div rel="line-inline-editor" style="display: none;">
                        <div class="row mr-t-10">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="id_tagihan"
                                        id="field-line-id-tagihan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option selected disabled></option>
                                    </select>
                                    <label for="field-line-id-tagihan-create">Tagihan</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input
                                        id="field-line-nominal-create"
                                        name="nominal"
                                        class="form-control"
                                        placeholder=""
                                        type="text"
                                        data-values=""
                                        data-periode=""
                                    >
                                    <label for="field-line-nominal-create">Nominal</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="bulan"
                                        id="field-line-bulan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option disabled selected></option>
                                        <option value="1">Juli</option>
                                        <option value="2">Agustus</option>
                                        <option value="3">September</option>
                                        <option value="4">Oktober</option>
                                        <option value="5">November</option>
                                        <option value="6">Desember</option>
                                        <option value="7">Januari</option>
                                        <option value="8">Februari</option>
                                        <option value="9">Maret</option>
                                        <option value="10">April</option>
                                        <option value="11">Mei</option>
                                        <option value="12">Juni</option>
                                    </select>
                                    <label for="field-line-bulan-create" class="form-control-label">Untuk Bulan</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a
                                    class="btn btn-default"
                                    type="button"
                                    rel="line-action-tambah"
                                >+</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="form-actions btn-list ml-auto">
                    <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                    <button class="btn btn-color-scheme" type="submit">Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('js_custom')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
(function($, global) {
    "use-strict"

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const SettingTagihan = (function () {
        let $DataTable;
        let $DataTableSiswa;

        function init () {
            datatableSiswa();
            searchSiswa();
            modalCreateTransaksi();
        }

        function datatableSiswa() {
            $DataTableSiswa = $('#tabel-siswa').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                searching: false,
                ordering:  false,
                autoWidth: false,
                ajax: "{{ route('spp-transaksi_search_siswa') }}",
                columns: [
                    { data: 'nis', name: 'nis' },
                    { data: 'nama', name: 'nama' },
                    { data: 'kelas', name: 'kelas' },
                    {
                        data: 'id',
                        name: 'id',
                        className: "text-center",
                        render: function (data) {
                            return `
                                    <a
                                        class="btn btn-outline-default ripple"
                                        data-toggle="modal"
                                        data-target="#modal-create-transaksi"
                                        data-id-kelas-siswa="${data}"
                                    >
                                        <i class="list-icon material-icons">add</i>
                                        Transaksi
                                    </a>
                            `;
                        },
                    },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari siswa...",
                },
            });

            $('[data-komponen="ikon-search"]').prependTo('.form-input-icon label');
        }

        function searchSiswa() {
            const $form = $('#form-search-siswa');
            const $inputSearch = $form.find('[name="pencarian"]');
            const $selectJenisFilter = $form.find('[name="jenis"]');

            $form.submit(function (ev) {
                ev.preventDefault();
                if ( !($selectJenisFilter.val()) ) {
                    return $selectJenisFilter.focus();
                }
                if ( !($inputSearch.val()) ) {
                    $inputSearch.focus();
                    // Reset data tabel ke data semula kalau input searchnya kosong
                    $DataTableSiswa.ajax.url("{{ route('spp-transaksi_search_siswa') }}").load();
                    return;
                }
                const endpointSearch = "{{ route('spp-siswa_search', ['jenis' => 'filterJenis', 'katakunci' => 'kataKunci']) }}"
                    .replace("&amp;", "&")
                    .replace("filterJenis", $selectJenisFilter.val())
                    .replace("kataKunci", $inputSearch.val());
                $DataTableSiswa.ajax.url(endpointSearch).load();
            });
        }

        function modalCreateTransaksi() {
            const $modal = $('#modal-create-transaksi');
            const $form = $modal.find('form');

            const $lineItemTemplate = $modal.find('[rel="line-item"]').detach();
            const $lineActionEditor = $modal.find('[rel="line-action-editor"]');
            const $lineEditor = $modal.find('[rel="line-inline-editor"]');
            const $lineActionTambah = $modal.find('[rel="line-action-tambah"]');

            const $selectSiswa = $form.find('[name="id_kelas_siswa"]');
            const $inputNis =  $form.find('[name="nis"]');
            const $selectTahunAjaran = $form.find('[name="tahun_ajaran"]');
            const $inputTanggalBayar = $form.find('[name="tgl_bayar"]');
            const $inputTagihan = $lineEditor.find('[name="id_tagihan"]');
            const $inputNominal = $lineEditor.find('[name="nominal"]');
            const $inputBulan = $lineEditor.find('[name="bulan"]');

            const Lines = new List('list-details', {
                valueNames: [
                    { data: ['id_tagihan', 'bulan', 'key'] },
                    'nama_tagihan',
                    'nominal',
                    'nama_bulan',

                ],
                item:   `<tr>` +
                        `<td class="nama_tagihan"></td>` +
                        `<td class="nominal text-right"></td>` +
                        `<td class="nama_bulan"></td>` +
                        `<td style="width: 30px;">` +
                        `   <a href="javascript:void(0)" class="btn btn-sm" title="Hapus item ini">` +
                        `       <i class="material-icons list-icon fs-14 text-muted">delete</i>` +
                        `   </a>` +
                        `</td>` +
                        `</tr>`,
            });

            $('#tabel-siswa').on('click', 'a[data-target="#modal-create-transaksi"]', function () {
                const id_kelas_siswa = $(this).data('idKelasSiswa');
                if (id_kelas_siswa) {
                    $selectSiswa
                        .val(id_kelas_siswa)
                        .trigger('input')
                        .trigger('change')
                        .attr('disabled', true);
                    $inputNis.attr('disabled', true);
                }
            });

            $modal.on('shown.bs.modal', function () {
                $selectTahunAjaran.focus();
            });

            $modal.on('hidden.bs.modal', function () {
                // Reset form & list rincian bayar
                $form.find('input, select').val('').trigger('input');
                Lines.clear();
                _showLineEditor(false);
            });

            $selectSiswa.change(async function () {
                try {
                    const endpointSiswa = "{{ route('spp-siswa_detail', 'IDKELASSISWA') }}".replace("IDKELASSISWA", $selectSiswa.val());
                    const responSiswa = await $.ajax(endpointSiswa);
                    const valNis = responSiswa.body.data?.nis;
                    $inputNis.val(valNis).trigger('input');

                    $inputTagihan.html(function () {
                        return [
                            `<option disabled selected></option>`,
                            ...responSiswa.body.data.tagihan.map(tagihan => `<option value="${tagihan.id}">${tagihan.nama}</option>`)
                        ];
                    });

                    const nominalById = {};
                    const periodeById = {};
                    for (let tagihan of responSiswa.body.data.tagihan) {
                        nominalById[tagihan.id] = tagihan.nominal;
                        periodeById[tagihan.id] = tagihan.priode;
                    }
                    $inputNominal.data('values', nominalById);
                    $inputNominal.data('periode', periodeById);
                } catch (error) {
                    console.error(error);
                }
            });

            $lineActionEditor.on('click', 'a', function () {
                if ( !($selectTahunAjaran.val()) ) {
                    return $selectTahunAjaran.focus();
                }
                _showLineEditor(true);
            });

            $inputTagihan.change(function () {
                _renderInputNominal($(this).val());
            });

            $lineActionTambah.on('click', function (ev) {
                ev.preventDefault();

                if (!_validateEditor()) {
                    return;
                }

                const linesData = Lines.get();
                const keyNew = !linesData.length ? 1 : linesData[linesData.length - 1].values().key + 1;
                const data = {
                    key: keyNew,
                    id_tagihan: $inputTagihan.val(),
                    nama_tagihan: $inputTagihan.find('option:selected').text(),
                    nominal: $inputNominal.val(),
                    bulan: $inputBulan.val(),
                    nama_bulan: $inputBulan.find('option:selected').text(),
                }
                _renderNewLine(data);
                _showLineEditor(false);
            });

            $('[rel="line-list"]').on('click', 'a', function () {
                const key = $(this).closest('tr').data('key');
                Lines.remove('key', key);
            });

            // Submisi form diganti dengan event klik pada tombol submit itu sendiri,
            // tidak dengan event submit, untuk menghindari tidak sengaja pencet Enter ketika
            // belum selesai isi form.
            $modal.find('button[type="submit"]').on('click', async function (ev) {
                ev.preventDefault();

                _showLineEditor(false);

                // Validasi
                if ( !($selectSiswa.val()) ) {
                    return $selectSiswa.focus();
                }
                if ( !($inputNis.val()) ) {
                    return $inputNis.focus();
                }
                if ( !($selectTahunAjaran.val()) ) {
                    return $selectTahunAjaran.focus();
                }
                if ( !($inputTanggalBayar.val()) ) {
                    return $inputTanggalBayar.focus();
                }
                if ( !(Lines.get().length) ) {
                    return $lineActionEditor.find('a').focus();
                }

                try {
                    const responPost = await $.ajax("{{ route('spp-transaksi_create') }}", {
                        method: "POST",
                        data: {
                            id_kelas_siswa: $selectSiswa.val(),
                            nis: $form.find('[name="nis"]').val(),
                            tahun_ajaran: $form.find('[name="tahun_ajaran"]').val(),
                            tgl_bayar: $form.find('[name="tgl_bayar"]').val(),
                            details: Lines.get()?.map(line => line.values()),
                        },
                    });
                    $DataTable.ajax.reload();
                } catch (error) {
                    console.error(error);
                }

                $modal.modal('hide');
            });

            function _validateEditor($fields) {
                if (!$inputTagihan.val()) {
                    $inputTagihan.focus();
                    return false
                }
                if (!$inputBulan.val()) {
                    $inputBulan.focus();
                    return false
                }
                return true;
            }

            function _showLineEditor(show) {
                if (show) {
                    $lineActionEditor.hide();
                    $lineEditor.show();
                    $inputTagihan.focus();
                } else {
                    $lineActionEditor.show();
                    $lineEditor.hide();
                }
                $inputTagihan.val('');
                $inputNominal.val('');
                $inputBulan.val('');
            }

            function _renderNewLine(data) {
                Lines.add(data);
            }

            function _renderInputNominal(idTagihan) {
                const valNominalById = $inputNominal.data('values')[idTagihan];
                $inputNominal.val(valNominalById).trigger('input');

                // Beberapa jenis tagihan nominalnya bisa dibayar tidak sama dengan settingnya
                // ...jadi perlu editable. Sedangkan bulanan tidak bisa diedit, harus sama
                // dengan settingan.
                if ($inputNominal.data('periode')[idTagihan] === 'bulanan') {
                    $inputNominal.attr('disabled', true);
                } else {
                    $inputNominal.attr('disabled', false);
                }
            }
        }

        return {init};
    })();

    $(document).ready(function () {
        SettingTagihan.init();
    });
})(jQuery, window);
</script>
@endpush
</x-spp.layout>
