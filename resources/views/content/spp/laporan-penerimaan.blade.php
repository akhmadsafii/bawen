<x-spp.layout :current-page="$currentPage">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading">
                    <h1>Laporan Penerimaan SPP</h1>
                </div>

                <div class="widget-body clearfix">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 80px">No</th>
                                <th>Kolom laporan di sini.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-center">1</th>
                                <td>Konten laporan di sini.</td>
                            </tr>
                            <tr>
                                <th class="text-center">2</th>
                                <td>Konten laporan di sini.</td>
                            </tr>
                            <tr>
                                <th class="text-center">3</th>
                                <td>Konten laporan di sini.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>
</x-spp.layout>
