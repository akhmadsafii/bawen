<x-spp.layout current-page="siswa">
    <div class="row">
        <div class="col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="widget-body pd-lr-30 clearfix">
                    <div class="row">
                        <div class="col-sm-9">
                            <h4>SPP {{ $data['label'] }} &mdash; Tahun Ajaran 2021/2022</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Jumlah</h6>
                            <p class="mr-t-0">
                                {{-- TODO: format angka monetary di JavaScript? --}}
                                {{-- Rp {{ $data['besaran_spp'] }} --}}
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Status</h6>
                            <p>
                                {{-- {{ $data['status'] }} --}}
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Field 3</h6>
                            <p>
                                Nilai
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Field 4</h6>
                            <p>
                                Nilai
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>
</x-spp.layout>
