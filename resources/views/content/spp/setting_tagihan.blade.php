@php
// Helper untuk bikin string option kelas
function _buildTextKelas($data) {
    $teks = $data['nama'];
    $teks .= $data['jurusan'] ? ' - ' . $data['jurusan'] : null;
    return $teks;
}
@endphp
<x-spp.layout current-page="Pengaturan Tagihan">
@push('gaya_custom')
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
/* ini gaya custom */
</style>
@endpush

<div class="row">
    <div id="pos" class="col-sm-10 widget-holder mx-auto">
        <div class="widget-bg">
            <div class="widget-body clearfix">
                <div class="row mr-b-20 clearfix">
                    <div class="col-lg-8">
                        <h5 class="box-title mr-b-0">
                            Pengaturan Tagihan
                        </h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="btn-group float-right">
                            <a
                                class="btn btn-block btn-outline-default ripple"
                                data-komponen="tombol-create-setting"
                                data-toggle="modal"
                                data-target="#modal-create-setting"
                            >
                                <i class="list-icon material-icons">add</i>
                                Tambah Pengaturan
                            </a>
                        </div>
                    </div>
                </div>

                <i data-komponen="ikon-search" class="list-icon material-icons fs-28">search</i>
                <table class="table-responsive list-pos mt-20" id="tabel-setting">
                    <thead>
                        <tr>
                            <th>Tahun Ajaran</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Jurusan</th>
                            <th>Periode</th>
                            <th>Nominal</th>
                            <th style="width: 80px;"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- Create -->
<div
    id="modal-create-setting"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <i class="list-icon material-icons text-muted">add_circle</i> Pengaturan Tagihan
                    <small class="text-muted">Tambahkan pengaturan.</small>
                </h5>
                <form id="form-create-setting" class="form-material">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input
                                    id="field-nama-create"
                                    name="nama"
                                    rel="field-nama"
                                    class="form-control"
                                    placeholder="Contoh... Tagihan Biaya Praktikum"
                                    type="text"
                                    required
                                >
                                <label for="field-nama-create">Nama</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="tahun_ajaran"
                                    id="field-tahun-ajaran-create"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Tahun Ajaran"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataTahunAjaran as $tahun)
                                    <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-tahun-ajaran-create">Berlaku untuk Tahun Ajaran</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="id_pos_pemasukan"
                                    id="field-pos-pemasukan-create"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Pos Pemasukan"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                    disabled="disabled"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataPosPemasukan as $pos)
                                    <option
                                        value="{{ $pos['id'] }}"
                                        data-tahun-ajaran="{{ $pos['tahun_ajaran'] }}"
                                        style="display: none;"
                                    >
                                        {{ $pos['kode'] ? $pos['kode'] . ' - ' : null }}{{ $pos['nama_pos'] }}
                                    </option>
                                    @endforeach
                                </select>
                                <label for="field-pos-pemasukan-create">Pos Pemasukan</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select
                                    name="id_kelas"
                                    id="field-kelas-create"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Kelas"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataKelas as $kelas)
                                    <option value="{{ $kelas['id'] }}">{{ _buildTextKelas($kelas) }}</option>
                                    @endforeach
                                </select>
                                <label for="field-kelas-create">Kelas</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input
                                    name="nominal"
                                    rel="field-nominal"
                                    class="form-control"
                                    id="field-nominal-create"
                                    placeholder="Rp"
                                    type="text"
                                >
                                <label for="field-nominal-create">Nominal Tagihan</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="periode"
                                    id="field-periode-create"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Periode"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="sekali">Sekali</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="bebas">Bebas</option>
                                </select>
                                <label for="field-periode-create">Periode</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="bulan[]"
                                    id="field-bulan-create"
                                    class="form-control select2-hidden-accessible"
                                    multiple=""
                                    data-toggle="select2"
                                    data-plugin-options="{&quot;minimumResultsForSearch&quot;: -1}"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                                <label for="field-bulan-create" class="form-control-label">Untuk Bulan</label>
                                {{-- <span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 595.531px;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-selection__choice" title="Mustard"><span class="select2-selection__choice__remove" role="presentation">×</span>Mustard</li><li class="select2-selection__choice" title="Ketchup"><span class="select2-selection__choice__remove" role="presentation">×</span>Ketchup</li><li class="select2-selection__choice" title="Relish"><span class="select2-selection__choice__remove" role="presentation">×</span>Relish</li><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span> --}}
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <div class="form-actions btn-list ml-auto">
                    <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                    <button form="form-create-setting" class="btn btn-color-scheme" type="submit">Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Edit -->
<div
    id="modal-edit-setting"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <i class="list-icon material-icons text-muted">edit</i>
                    Pengaturan Tagihan
                    <small class="text-muted">Ubah pengaturan.</small>
                </h5>
                <form id="form-edit-setting" class="form-material">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input
                                    id="field-nama-edit"
                                    name="nama"
                                    rel="field-nama"
                                    class="form-control"
                                    placeholder="Contoh... Tagihan Biaya Praktikum"
                                    type="text"
                                >
                                <label for="field-nama-edit">Nama</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="tahun_ajaran"
                                    id="field-tahun-ajaran-edit"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Tahun Ajaran"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataTahunAjaran as $tahun)
                                    <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-tahun-ajaran-edit">Berlaku untuk Tahun Ajaran</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="id_pos_pemasukan"
                                    id="field-pos-pemasukan-edit"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Pos Pemasukan"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled"></option>
                                    @foreach ($dataPosPemasukan as $pos)
                                    <option
                                        value="{{ $pos['id'] }}"
                                        data-tahun-ajaran="{{ $pos['tahun_ajaran'] }}"
                                        style="display: none;"
                                    >
                                        {{ $pos['kode'] ? $pos['kode'] . ' - ' : null }}{{ $pos['nama_pos'] }}
                                    </option>
                                    @endforeach
                                </select>
                                <label for="field-pos-pemasukan-edit">Pos Pemasukan</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select
                                    name="id_kelas"
                                    id="field-kelas-edit"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Kelas"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataKelas as $kelas)
                                    <option value="{{ $kelas['id'] }}">{{ _buildTextKelas($kelas) }}</option>
                                    @endforeach
                                </select>
                                <label for="field-kelas-edit">Kelas</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input
                                    name="nominal"
                                    rel="field-nominal"
                                    class="form-control"
                                    id="field-nominal-edit"
                                    placeholder="Rp"
                                    type="text"
                                >
                                <label for="field-nominal-edit">Nominal Tagihan</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="periode"
                                    id="field-periode-edit"
                                    class="form-control select2-hidden-accessible"
                                    data-placeholder="Pilih Periode"
                                    data-toggle="select2"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="sekali">Sekali</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="bebas">Bebas</option>
                                </select>
                                <label for="field-periode-edit">Periode</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                    name="bulan[]"
                                    id="field-bulan-edit"
                                    class="form-control select2-hidden-accessible"
                                    multiple=""
                                    data-toggle="select2"
                                    data-plugin-options="{&quot;minimumResultsForSearch&quot;: -1}"
                                    tabindex="-1"
                                    aria-hidden="true"
                                >
                                    <option value="1">Juli</option>
                                    <option value="2">Agustus</option>
                                    <option value="3">September</option>
                                    <option value="4">Oktober</option>
                                    <option value="5">November</option>
                                    <option value="6">Desember</option>
                                    <option value="7">Januari</option>
                                    <option value="8">Februari</option>
                                    <option value="9">Maret</option>
                                    <option value="10">April</option>
                                    <option value="11">Mei</option>
                                    <option value="12">Juni</option>
                                </select>
                                <label for="field-bulan-edit" class="form-control-label">Untuk Bulan</label>
                                {{-- <span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 595.531px;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-selection__choice" title="Mustard"><span class="select2-selection__choice__remove" role="presentation">×</span>Mustard</li><li class="select2-selection__choice" title="Ketchup"><span class="select2-selection__choice__remove" role="presentation">×</span>Ketchup</li><li class="select2-selection__choice" title="Relish"><span class="select2-selection__choice__remove" role="presentation">×</span>Relish</li><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span> --}}
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <div class="form-actions btn-list ml-auto">
                    <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                    <button form="form-edit-setting" class="btn btn-color-scheme" type="submit">Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('js_custom')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
(function($, global) {
    "use-strict"

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const SettingTagihan = (function () {
        let dtInstance;

        function init () {
            dtInstance = datatableTagihan();
            modalCreateSetting();
            modalEditSetting();
        }

        function datatableTagihan() {
            const dtInstance = $('#tabel-setting').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                ordering:  false,
                ajax: "{{ route('spp-setting_tagihan') }}",
                columns: [
                    { data: 'tahun_ajaran', name: 'tahun_ajaran' },
                    { data: 'nama', name: 'nama' },
                    { data: 'kelas', name: 'kelas' },
                    { data: 'jurusan', name: 'jurusan' },
                    { data: 'periode', name: 'periode' },
                    { data: 'nominal', name: 'nominal' },
                    {
                        data: 'id',
                        name: 'id',
                        render (data) {
                            return `
                                <a
                                    class="btn btn-outline-default ripple mr-l-20"
                                    rel="tombol-edit-setting"
                                    data-id-edit="${data}"
                                    data-toggle="modal"
                                    data-target="#modal-edit-setting"
                                >
                                    <i class="list-icon material-icons">edit</i>
                                </a>
                            `;
                        },
                    },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari pengaturan...",
                },
            });

            $('[data-komponen="ikon-search"]').prependTo('.form-input-icon label');

            return dtInstance;
        }

        function modalCreateSetting() {
            const $modal = $('#modal-create-setting');
            const $form = $modal.find('#form-create-setting');

            $modal.on('shown.bs.modal', function (ev) {
                $form.find('[name="nama"]').focus();
            });

            $modal.on('hidden.bs.modal', function (ev) {
                $form.find('[name="nama"]').val('');
                $form.find('[name="tahun_ajaran"]').val('');
                $form.find('[name="id_pos_pemasukan"]').val('');
                $form.find('[name="id_kelas"]').val('');
                $form.find('[name="nominal"]').val('');
                $form.find('[name="periode"]').val('');
                $form.find('[name="bulan[]"]').val('');
            });

            $form.find('select#field-tahun-ajaran-create').change(function (ev) {
                const tahunSelected = $(ev.target).val();
                const $selectPos = $form.find('select#field-pos-pemasukan-create');
                const $optionAll = $selectPos.find('option');
                $selectPos.val('');
                $selectPos.removeAttr("disabled");
                $optionAll.hide();
                $optionAll.filter(`[data-tahun-ajaran="${tahunSelected}"]`).show();
            });

            $form.submit(async function (ev) {
                ev.preventDefault();

                // TODO: validasi inputnya dulu
                // ...

                try {
                    const respon = await $.ajax("{{ route('spp-setting_create') }}", {
                        method: "POST",
                        data: $(this).serialize(),
                    });
                    dtInstance.ajax.reload();
                    $modal.modal('hide');
                } catch (error) {
                    console.error(error);
                }
            });
        }

        function modalEditSetting() {
            const $modal = $('#modal-edit-setting');
            const $form = $modal.find('#form-edit-setting');

            let rowData;

            $('#tabel-setting tbody').on('click', '[rel="tombol-edit-setting"]', function (ev) {
                const $tr = $(this).closest('tr');
                rowData = dtInstance.row($tr).data();

                $form.find('[name="nama"]').val(rowData.nama).trigger('input');
                $form.find('[name="tahun_ajaran"]').val(rowData.tahun_ajaran).trigger('input').trigger('change');
                $form.find('[name="id_pos_pemasukan"]').val(rowData.id_pos_pemasukan).trigger('input');
                $form.find('[name="id_kelas"]').val(rowData.id_kelas).trigger('input');
                $form.find('[name="nominal"]').val(rowData.nominal).trigger('input');
                $form.find('[name="periode"]').val(rowData.periode).trigger('input');
                // string list id bulan perlu dikonversi ke array (lagi) sebelum diset ke multiple select
                rowData.bulan && $form.find('[name="bulan[]"]').val(rowData.bulan?.split(","));
            });

            $modal.on('shown.bs.modal', function () {
                $form.find('[name="nama"]').focus();
            });

            $modal.on('hidden.bs.modal', function (ev) {
                $form.find('[name="nama"]').val('');
                $form.find('[name="tahun_ajaran"]').val('');
                $form.find('[name="id_pos_pemasukan"]').val('');
                $form.find('[name="id_kelas"]').val('');
                $form.find('[name="nominal"]').val('');
                $form.find('[name="periode"]').val('');
                $form.find('[name="bulan[]"]').val('');
            });

            $form.find('select#field-tahun-ajaran-edit').change(function (ev) {
                const tahunSelected = $(ev.target).val();
                const $selectPos = $form.find('select#field-pos-pemasukan-edit');
                const $optionAll = $selectPos.find('option');
                $selectPos.val('');
                $selectPos.removeAttr("disabled");
                $optionAll.hide();
                $optionAll.filter(`[data-tahun-ajaran="${tahunSelected}"]`).show();
            });

            $form.submit(async function (ev) {
                ev.preventDefault();

                // cek apakah datanya berubah & validasi
                // ...

                try {
                    const endpoint = "{{ route('spp-setting_edit', 'idSetting') }}".replace("idSetting", rowData.id);
                    const respon = await $.ajax(endpoint, {
                        method: "PUT",
                        data: $(this).serialize(),
                    });
                    dtInstance.ajax.reload();
                    $modal.modal('hide');
                } catch (error) {
                    console.error(error);
                }
            });
        }

        return { init };
    })();

    $(document).ready(function () {
        SettingTagihan.init();
    });
})(jQuery, window);
</script>
@endpush
</x-spp.layout>
