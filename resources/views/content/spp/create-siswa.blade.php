<x-spp.layout current-page="create-siswa">
  <div class="container">
    <span class="row justify-content-between">
        <h3>Tambah Data Siswa</h3>
        <span>
            <a class="btn btn-info btn-sm" href="{{ $export }}" >Download template excel</a>
            <!-- modal-toogle -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Upload file
            </button>
            
            <!-- modal-page -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Upload File Excel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- form -->
                        <form method="POST" action="{{ route('spp-siswa_import') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="image">
                            
                        
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                        </form>
                        <!-- end-form -->
                    </div>
                    </div>
                </div>
            </div>
        <!-- end-modal-page -->
            
        </span>
    </span>
    <div class="row p-4">
      <section class="col-md-6">
        <form method="POST" action="{{ route('spp-siswa_store') }}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
              <label for="kode" class="control-label">NIK</label>
              <input type="text" name="nik" class="form-control" id="nik">
          </div>
                                        
          <div class="form-group">
              <label for="kode" class="control-label">NIS</label>
              <input type="text" name="nis" id="nis" class="form-control" required>
          </div>
                                        
          <div class="form-group">
              <label for="kode" class="control-label">NISN</label>
              <input type="text" name="nisn" class="form-control" id="nisn" required>
          </div>

          <div class="form-group">
              <label for="kode" class="control-label">Tahun Angkatan</label>
              <input type="text" name="tahun_angkatan" id="tahun_angkatan" class="form-control" placeholder="2021" required>
          </div>

          <div class="form-group">
              <label for="kode" class="control-label">Nama</label>
              <input type="text" name="nama" class="form-control" id="nama" required>
          </div>

          <div class="form-group">
              <label for="kode" class="control-label">Email</label>
              <input type="email" name="email" class="form-control" id="email">
          </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Lintang</label>
                                    <input type="text" name="lintang" class="form-control" id="lintang">
                                </div>
          
          <div class="form-group">
                                    <label for="kode" class="control-label">Bujur</label>
                                    <input type="text" name="bujur" class="form-control" id="bujur">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Anak ke</label>
                                    <input type="text" name="anak_ke" class="form-control" id="anak_ke">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Status Keluarga</label>
                                    <input type="text" name="status_keluarga" class="form-control" id="status_keluarga">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Nama Ayah</label>
                                    <input type="text" name="nama_ayah" class="form-control" id="nama_ayah">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Pekerjaan Ayah</label>
                                    <input type="text" name="pekerjaan_ayah" class="form-control" id="pekerjaan_ayah">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Nama Ibu</label>
                                    <input type="text" name="nama_ibu" class="form-control" id="nama_ibu">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Pekerjaan Ibu</label>
                                    <input type="text" name="pekerjaan_ibu" class="form-control" id="pekerjaan_ibu">
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Gambar</label>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                </div>

          <div class="col-md-3">
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group" width="100%" style="margin-top: 10px">
                                <div id="delete_foto" style="text-align: center"></div>
          </div>



        
      </section>

      <section class="col-md-6">
        
          <div class="form-group">
            <label for="kode" class="control-label">Jenis Kelamin</label>
            <select name="jenkel" id="jenkel" class="form-control">
              <option value="l">Laki - laki</option>
              <option value="p">Perempuan</option>
            </select>
          </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Agama</label>
                                    <select name="agama" id="agama" class="form-control">
                                        <option value="islam">Islam</option>
                                        <option value="kristen">Kristen</option>
                                        <option value="katholik">Katholik</option>
                                        <option value="hindu">Hindu</option>
                                        <option value="budha">Budha</option>
                                        <option value="lainnya">Lainnya</option>
                                    </select>
                                </div>

          <div class="form-group">
                                    <label for="kode" class="control-label">Telepon</label>
                                    <input type="text" name="telepon" class="form-control" id="telepon">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" required>
                                </div>
            
            <div class="form-group">
                                    <label for="kode" class="control-label">Tanggal Lahir</label>
                                    <input type="text" name="tgl_lahir" class="form-control datepicker" id="tgl_lahir"
                                        value="{{ date('d-m-Y') }}" readonly data-plugin-options='{"autoclose": true}'>
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Alamat</label>
                                    <input type="text" name="alamat" class="form-control" id="alamat">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Tanggal Diterima</label>
                                    <input type="text" name="tgl_diterima" class="form-control datepicker" id="tgl_diterima"
                                        value="{{ date('d-m-Y') }}" readonly data-plugin-options='{"autoclose": true}'
                                        required>
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">No Ijazah</label>
                                    <input type="text" name="no_ijazah" class="form-control" id="no_ijazah">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Tahun Ijazah</label>
                                    <input type="text" name="tahun_ijazah" class="form-control" id="tahun_ijazah">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">No SKHUN</label>
                                    <input type="text" name="no_skhun" class="form-control" id="no_skhun">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Tahun SKHUN</label>
                                    <input type="text" name="th_skhun" class="form-control" id="th_skhun">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Nama Wali</label>
                                    <input type="text" name="nama_wali" class="form-control" id="nama_wali">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Telepon Wali</label>
                                    <input type="text" name="telepon_wali" class="form-control" id="telepon_wali">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Alamat Wali</label>
                                    <input type="text" name="alamat_wali" class="form-control" id="alamat_wali">
                                </div>

            <div class="form-group">
                                    <label for="kode" class="control-label">Pekerjaan Wali</label>
                                    <input type="text" name="pekerjaan_wali" class="form-control" id="pekerjaan_wali">
                                </div>

            

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
      </section>
    </div>
    
  </div>

</x-spp.layout>