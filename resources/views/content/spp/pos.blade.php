<x-spp.layout current-page="Pos Keuangan">
    <x-slot name="halamancss">
        <style>
            .list-pos {
                width: 100%;
            }

            .list-pos tr:hover td {
                background-color: rgb(247, 250, 252);
            }

            .list-pos .list-pos-tombol-edit {
                visibility: hidden;
            }

            .list-pos tr:hover .list-pos-tombol-edit {
                visibility: visible;
            }

            .list-pos td {
                padding: 20px;
            }

            .list-pos .list-pos-kode {
                width: 80px;
            }

            .list-pos .list-pos-action {
                width: 80px;
            }
        </style>
    </x-slot>

    <div class="row">
        <div id="pos" class="col-lg-8 col-md-10 col-sm-12 widget-holder mx-auto">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mr-b-20 clearfix">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <div class="btn-group float-right">
                                <a
                                    class="btn btn-block btn-outline-default ripple"
                                    data-komponen="tombol-create"
                                    data-toggle="modal"
                                    data-target="#modal-create-pos"
                                >
                                    <i class="list-icon material-icons">add</i>
                                    Tambah Baru
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-input-icon d-flex">
                        <i class="list-icon material-icons fs-28">search</i>
                        <input
                            type="search"
                            id="pencarian"
                            name="pencarian"
                            class="fuzzy-search form-control fs-22 fw-300 ml-3 border-0"
                            placeholder="Cari item pos"
                        >
                    </div>

                    <hr>

                    <table
                        class="table-responsive list-pos mt-20"
                        data-komponen="tabel-pos"
                        data-awal-pos='@json($dataPos)'
                    >
                        <tbody class="list">
                            @foreach ($dataPos as $pos)
                            <tr data-id="{{$pos['id']}}">
                                <td class="list-pos-kode text-center text-muted">
                                    <span class="kode badge bg-color-scheme-dark px-2 py-2">{{$pos['kode']}}</span>
                                </td>
                                <td>
                                    <span class="nama">{{$pos['nama']}}</span>
                                    <a
                                        class="list-pos-tombol-edit btn btn-outline-default ripple mr-l-20"
                                        data-komponen="tombol-edit"
                                        data-pos-id="{{$pos['id']}}"
                                        data-toggle="modal"
                                        data-target="#modal-edit-pos"
                                    >
                                        <i class="list-icon material-icons">edit</i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <hr>

                    <div class="row p-3">
                        {{-- TODO: "count" dihitung dinamis dari total item yang sebenarnya --}}
                        <div class="list-pos-count col-7 text-muted mt-1"><span></span> item</div>

                        <div class="col-5">
                            <div class="btn-group float-right">
                                <a href="javascript:void(0)" class="btn btn-sm btn-outline-default ripple disabled">
                                    <i class="list-icon material-icons">keyboard_arrow_left</i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-sm btn-outline-default ripple disabled">
                                    <i class="list-icon material-icons">keyboard_arrow_right</i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <!-- Create -->
    <div id="modal-create-pos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="box-title mr-b-20"><i class="list-icon material-icons text-muted">add</i> Pos Keuangan <small class="text-muted">Tambahkan item.</small></h5>
                    <form id="form-create-pos" data-komponen="form-create" class="form-material">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input data-field="kode" class="form-control" id="field-kode-create" placeholder="Kode pengenal pos keuangan" type="text">
                                    <label for="field-kode-create">Kode</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input data-field="nama" class="form-control" id="field-nama-create" placeholder="Contoh... Sumbangan Pengembangan Institusi" type="text" required>
                            <label for="field-nama-create">Nama</label>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <div class="form-actions btn-list ml-auto">
                        <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button form="form-create-pos" class="btn btn-color-scheme" type="submit">Simpan</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Edit -->
    <div id="modal-edit-pos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="box-title mr-b-20"><i class="list-icon material-icons text-muted">edit</i> Pos Keuangan <small class="text-muted">Perbarui data.</small></h5>
                    <form id="form-edit-pos" data-komponen="form-edit" class="form-material">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input data-field="kode" class="form-control" id="field-kode-edit" placeholder="Kode pengenal pos" type="text">
                                    <label for="field-kode-edit">Kode</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input data-field="nama" class="form-control" id="field-nama-edit" placeholder="Contoh... Sumbangan Pengembangan Institusi" type="text" required>
                            <label for="field-nama-edit">Nama</label>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <div class="form-actions btn-list ml-auto">
                        <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button form="form-edit-pos" class="btn btn-color-scheme" type="submit">Simpan</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <x-slot name="halamanjs">
        {{--
            Children slot akan disisipkan ke bagian bawah dokumen
            di `layout.blade.php` supaya script bisa dieksekusi terakhir
            setelah dokumen & library lain dimuat/sudah siap.
        --}}
        <script>
(function ($, List) {

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const ListPos = (function () {

        let elTabelPos;
        let elListCount;

        let posAll;
        let instanceListJs;
        let idEdit;

        function init() {
            // setup List.js
            // Meng-index data list dengan List.js
            instanceListJs = new List('pos', {
                valueNames: [
                    { data: ['id'] },
                    'kode',
                    'nama',
                ],
            });

            elTabelPos = $('table[data-komponen="tabel-pos"]')
            elListCount = $('.list-pos-count > span');

            elTabelPos.on('click', '[data-komponen="tombol-edit"]', klikTombolEdit);

            elListCount.text(instanceListJs.size());
        }

        function klikTombolEdit (ev) {
            ev.preventDefault();

            const elTombolEditWithID = $(ev.target).closest('[data-komponen="tombol-edit"]');
            idEdit = elTombolEditWithID.data('pos-id');

            // buka modal pakai data yang sudah di-fetch
            ModalEdit.loadPos(idEdit);
        }

        function getItem (idPos) {
            return instanceListJs.get('id', idPos)[0].values();
        }

        function updateItem (data) {
            if(!data.id) {
                throw new Error('Butuh ID item untuk update data item');
            }
            instanceListJs.get('id', data.id)[0].values(data);
        };

        function addItem (data) {
            if (!data) {
                console.error('Data untuk menambahkan item harus dimasukkan.');
            }
            instanceListJs.add(data);
            instanceListJs.sort('id', { order: "desc" });

            elTabelPos
                .find(`[data-id="${data.id}"] [data-komponen="tombol-edit"]`)
                .attr('data-pos-id', data.id);
        }

        return {
            init,
            getItem,
            addItem,
            updateItem,
         };
    })();

    const ModalCreate = (function () {

        let elModalCreate;
        let elFormCreate;
        let elInputNama;
        let elInputKode;

        function init () {
            elModalCreate = $("#modal-create-pos");
            elFormCreate = $('[data-komponen="form-create"]');
            elInputKode = elFormCreate.find('[data-field="kode"]');
            elInputNama = elFormCreate.find('[data-field="nama"]');

            elModalCreate.on('shown.bs.modal', () => elInputKode.focus());
            elModalCreate.on('hidden.bs.modal', onModalClose);
            elFormCreate.on('submit', onSubmitCreate);
        }

        async function onSubmitCreate (ev) {
            ev.preventDefault();
            try {
                const respon = await $.ajax("{{ route('spp-pos_create') }}", {
                    method: 'POST',
                    data: {
                        kode: elInputKode.val(),
                        nama: elInputNama.val(),
                        // `id_sekolah` di set di server...
                    },
                });
                ListPos.addItem(respon.body.data);
                elModalCreate.modal('hide');
            } catch (error) {
                console.error(error.message);
            }
        }

        function onModalClose () {
            elInputNama.val('').trigger('input');
            elInputKode.val('').trigger('input');
            elFormCreate.removeClass('was-validated');
        }

        return { init };
    })();

    const ModalEdit = (function () {

        let elModalEdit;
        let elModalBody;
        let elFormEdit;
        let elInputNama;
        let elInputKode;

        // data (state?)...
        let dataEdit;

        function init () {
            elModalEdit = $("#modal-edit-pos");
            elModalBody = $("#modal-edit-pos .modal-body");
            elFormEdit = $('[data-komponen="form-edit"]');
            elInputNama = elFormEdit.find('[data-field="nama"]');
            elInputKode = elFormEdit.find('[data-field="kode"]');

            elModalEdit.on('shown.bs.modal', () => elInputKode.focus());
            elModalEdit.on('hidden.bs.modal', onModalClose);
            elFormEdit.on('submit', onSubmitEdit);
        }

        async function loadPos(idEdit) {
            // Simpan referensi values item asli sebelum diedit
            dataEdit = ListPos.getItem(idEdit);
            elInputKode.val(dataEdit.kode).trigger('input');
            elInputNama.val(dataEdit.nama).trigger('input');
        }

        async function onSubmitEdit (ev) {
            ev.preventDefault();
            const nama = $(ev.target).find('[data-field="nama"]').val()
            const kode = $(ev.target).find('[data-field="kode"]').val()

            try {
                const endpointEdit = "{{ route('spp-pos_edit', 'idEdit') }}".replace("idEdit", dataEdit.id);
                await $.ajax(endpointEdit, {
                    method: 'PUT',
                    data: {
                        ...dataEdit,
                        nama,
                        kode,
                    },
                });
                const endpointTunggal = "{{ route('spp-pos_single', 'idPos') }}".replace("idPos", dataEdit.id)
                const respon = await $.ajax(endpointTunggal);
                ListPos.updateItem (respon.body.data);
                elModalEdit.modal('hide');
            } catch (error) {
                console.error('error update?', error);
            }
        }

        function onModalClose () {
            // reset semua value modal
            dataEdit = undefined;
            elInputNama.val('').trigger('input');
            elInputKode.val('').trigger('input');
            elFormEdit.removeClass('was-validated');
        }

        return { init, loadPos };
    })();

    $(document).ready(function() {
        ListPos.init();
        ModalCreate.init();
        ModalEdit.init();
    });
})(jQuery, List);

        </script>
    </x-slot>
</x-spp.layout>
