<x-spp.layout :current-page="$currentPage">
    <div class="row">
        <div class="col-md-7 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h1>Informasi Pembayaran</h1>
                    <p>Konten di sini.</p>

                    <h2>Rekening Sekolah</h2>
                    <p>Konten rekening di sini.</p>

                    <h2>Pembayaran Online</h2>
                    <p>Konten pembayaran di sini.</p>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>
</x-spp.layout>
