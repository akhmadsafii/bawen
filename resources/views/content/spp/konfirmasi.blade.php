<x-spp.layout current-page="Transaksi">
@push('gaya_custom')
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
/* ini gaya custom */
</style>
@endpush

<div class="row">
    <div id="konfirmasi" class="col-sm-12 widget-holder mx-auto">
        <div class="widget-bg">
            <div class="widget-body clearfix">
                <div class="row mr-b-20 clearfix">
                    <div class="col-lg-8">
                        <h5 class="box-title mr-b-0">
                            Pembayaran Terkonfirmasi
                        </h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="btn-group float-right">
                            <a
                                class="btn btn-block btn-outline-default ripple"
                                data-toggle="modal"
                                data-target="#modal-create-konfirmasi"
                            >
                                <i class="list-icon material-icons">add</i>
                                Konfirmasi
                            </a>
                        </div>
                    </div>
                </div>

                <i data-komponen="ikon-search" class="list-icon material-icons fs-28">search</i>
                <table class="table-responsive list-pos mt-20" id="tabel-konfirmasi">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Bentuk</th>
                            <th>Tanggal Bayar</th>
                            <th>Nominal Total</th>
                            <th>Nama Siswa</th>
                            <th>NIS</th>
                            <th>Tahun Ajaran</th>
                            <th>Jumlah Tagihan yang Dibayar</th>
                            <th style="width: 80px;"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- View -->
<div
    id="modal-view-konfirmasi"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    #<span rel="field-kode"></span>
                    <small>Konfirmasi Transaksi</small>
                </h5>

                <div class="row">
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Tanggal Bayar</h6>
                        <p rel="field-tgl-bayar" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Nama Siswa Tertagih</h6>
                        <p rel="field-nama" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Nominal Bayar</h6>
                        <p rel="field-nominal" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Nomor Induk Siswa</h6>
                        <p rel="field-nis" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Tahun Ajaran</h6>
                        <p rel="field-tahun-ajaran" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Kelas</h6>
                        <p rel="field-kelas" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Jurusan</h6>
                        <p rel="field-jurusan" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Rombel</h6>
                        <p rel="field-rombel" class="mr-t-0">Namamamama</p>
                    </div>
                </div>

                <hr class="mr-tb-50">

                <h5 class="mr-b-20">Rincian Pembayaran</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div rel="list-detail" class="list-group">
                            <div rel="item-riwayat" class="list-group-item text-center">
                                <span class="text-muted">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Create -->
<div
    id="modal-create-konfirmasi"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <i class="list-icon material-icons text-muted">note_add</i>
                    Konfirmasikan Transaksi
                </h5>

                <form id="form-create-konfirmasi" class="form-material">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="id_kelas_siswa"
                                    id="field-kelas-siswa-create"
                                    class="form-control"
                                    data-placeholder="Pilih Siswa"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataSiswa as $siswa)
                                    <option value="{{ $siswa['id'] }}">{{ $siswa['nama'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-kelas-siswa-create">Siswa</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <input
                                    class="form-control"
                                    id="field-nis-create"
                                    name="nis"
                                    placeholder="Nomor induk siswa..."
                                    type="text"
                                    readonly
                                >
                                <label for="field-nis-create">Nomor Induk Siswa</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="bentuk"
                                    id="field-bentuk-create"
                                    class="form-control"
                                    data-placeholder="Pilih Metode Bayar"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="transfer">Transfer Bank</option>
                                    <option value="epayment">e-Payment (online)</option>
                                </select>
                                <label for="field-bentuk-create">Metode Pembayaran</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="tahun_ajaran"
                                    id="field-tahun-ajaran-create"
                                    class="form-control"
                                    data-placeholder="Pilih Tahun Ajaran"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataTahunAjaran as $tahun)
                                    <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-tahun-ajaran-create">Tahun Ajaran</label>
                            </div>
                        </div>
                    </div>

                    <hr class="mr-tb-50">

                    <h5 class="mr-b-20">Rincian Pembayaran</h5>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="list-details" class="table table-responsive">
                                <tbody class="list" rel="line-list"></tbody>
                            </table>

                            <div rel="line-action-editor">
                                <u>
                                    <a href="javascript:void(0)">
                                        Tambahkan item pembayaran
                                    </a>
                                </u>
                            </div>
                        </div>
                    </div>

                    <div rel="line-inline-editor" style="display: none;">
                        <div class="row mr-t-10">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="id_tagihan"
                                        id="field-line-id-tagihan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option selected disabled></option>
                                    </select>
                                    <label for="field-line-id-tagihan-create">Tagihan</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input
                                        id="field-line-nominal-create"
                                        name="nominal"
                                        class="form-control"
                                        placeholder=""
                                        type="text"
                                        data-values=""
                                        data-periode=""
                                    >
                                    <label for="field-line-nominal-create">Nominal</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="bulan"
                                        id="field-line-bulan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option disabled selected></option>
                                        <option value="1">Juli</option>
                                        <option value="2">Agustus</option>
                                        <option value="3">September</option>
                                        <option value="4">Oktober</option>
                                        <option value="5">November</option>
                                        <option value="6">Desember</option>
                                        <option value="7">Januari</option>
                                        <option value="8">Februari</option>
                                        <option value="9">Maret</option>
                                        <option value="10">April</option>
                                        <option value="11">Mei</option>
                                        <option value="12">Juni</option>
                                    </select>
                                    <label for="field-line-bulan-create" class="form-control-label">Untuk Bulan</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a
                                    class="btn btn-default"
                                    type="button"
                                    rel="line-action-tambah"
                                >+</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="form-actions btn-list ml-auto">
                    <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                    <button class="btn btn-color-scheme" type="submit">Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('js_custom')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
(function($, global) {
    "use-strict"

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const HalamanTransaksi = (function () {

        let $DataTable;

        function init () {
            datatableTagihan();
            modalCreateKonfirmasi();
        }

        function datatableTagihan() {
            $DataTable = $('#tabel-konfirmasi').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                ordering:  false,
                ajax: "{{ route('spp-konfirmasi') }}",
                columns: [
                    { data: 'kode', name: 'kode' },
                    { data: 'bentuk', name: 'bentuk' },
                    { data: 'tgl_bayar', name: 'tgl_bayar' },
                    { data: 'nominal', name: 'nominal' },
                    { data: 'nama', name: 'nama' },
                    { data: 'nis', name: 'nis' },
                    { data: 'tahun_ajaran', name: 'tahun_ajaran' },
                    { data: 'jml_tagihan_dibayar', name: 'jml_tagihan_dibayar' },
                    {
                        data: 'id',
                        name: 'id',
                        render (data) {
                            return `
                                <a
                                    class="btn btn-outline-default ripple mr-l-20"
                                    rel="tombol-view"
                                    data-id-view="${data}"
                                    data-toggle="modal"
                                    data-target="#modal-view-konfirmasi"
                                >
                                    <i class="list-icon material-icons">visibility</i>
                                </a>
                            `;
                        },
                    },
                    // hidden
                    { data: 'kelas', name: 'kelas', visible: false },
                    { data: 'jurusan', name: 'jurusan', visible: false },
                    { data: 'rombel', name: 'rombel', visible: false },
                    { data: 'tipe', name: 'tipe', visible: false },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari data konfirmasi...",
                },
            });

            $('[data-komponen="ikon-search"]').prependTo('.form-input-icon label');

            $('#tabel-konfirmasi tbody').on('click', 'a[rel="tombol-view"]', async function () {
                const elTR = $(this).closest('tr');
                const rowData = $DataTable.row(elTR).data();

                $('[rel="field-kode"]').text(rowData.kode);
                $('[rel="field-nama"]').text(rowData.nama);
                $('[rel="field-tgl-bayar"]').text(rowData.tgl_bayar);
                $('[rel="field-nominal"]').text(rowData.nominal);
                $('[rel="field-nis"]').text(rowData.nis);
                $('[rel="field-tahun-ajaran"]').text(rowData.tahun_ajaran);
                $('[rel="field-kelas"]').text(rowData.kelas);
                $('[rel="field-rombel"]').text(rowData.rombel);
                $('[rel="field-jurusan"]').text(rowData.jurusan);

                // detail...
                const endpoint = "{{ route('spp-konfirmasi_detail', ['id_konfirmasi' => 'idKonfirmasi', 'kode_transaksi' => 'kodeTrx']) }}"
                    .replace("idKonfirmasi", rowData.id)
                    .replace("kodeTrx", encodeURIComponent(rowData.kode));
                const responDetail = await $.ajax(endpoint);
                $('[rel="list-detail"]').html(function () {
                    if (!responDetail.body.data || responDetail.body.data.length === 0) {
                        return `
                            <div rel="item-riwayat" class="list-group-item text-center">
                                <span class="text-muted">
                                    Tidak ada rincian.
                                </span>
                            </div>
                        `;
                    }
                    return responDetail.body.data.map(detail => {
                        return `
                            <div rel="item-detail" class="list-group-item d-flex justify-content-end">
                                <span class="mr-auto">
                                    ${detail.nama_tagihan}
                                    ${detail.bulan ? `(bulan ${detail.bulan})` : null}
                                </span>
                                <span class="fs-12 my-auto">${detail.nominal}</span>
                            </div>
                        `;
                    });
                });
            })

            $('#modal-view-konfirmasi').on('hidden.bs.modal', function (ev) {
                $('[rel="field-kode"]').text('');
                $('[rel="field-nama"]').text('');
                $('[rel="field-tgl-bayar"]').text('');
                $('[rel="field-nominal"]').text('');
                $('[rel="field-nis"]').text('');
                $('[rel="field-tahun-ajaran"]').text('');
                $('[rel="field-kelas"]').text('');
                $('[rel="field-rombel"]').text('');
                $('[rel="field-jurusan"]').text('');
                $('[rel="list-detail"]').html(`
                    <div rel="item-riwayat" class="list-group-item text-center">
                        <span class="text-muted">
                            Loading...
                        </span>
                    </div>
                `);
            });
        }

        function modalCreateKonfirmasi() {
            const $modal = $('#modal-create-konfirmasi');

            const $form = $modal.find('form');
            const $selectSiswa = $form.find('[name="id_kelas_siswa"]');
            const $inputNis = $form.find('[name="nis"]');
            const $selectBentuk = $form.find('[name="bentuk"]');
            const $selectTahunAjaran = $form.find('[name="tahun_ajaran"]');

            const $lineItemTemplate = $modal.find('[rel="line-item"]').detach();
            const $lineActionEditor = $modal.find('[rel="line-action-editor"]');
            const $lineEditor = $modal.find('[rel="line-inline-editor"]');
            const $lineActionTambah = $modal.find('[rel="line-action-tambah"]');

            const $inputTagihan = $lineEditor.find('[name="id_tagihan"]');
            const $inputNominal = $lineEditor.find('[name="nominal"]');
            const $inputBulan = $lineEditor.find('[name="bulan"]');

            const Lines = new List('list-details', {
                valueNames: [
                    { data: ['id_tagihan', 'bulan', 'key'] },
                    'nama_tagihan',
                    'nominal',
                    'nama_bulan',

                ],
                item:   `<tr>` +
                        `<td class="nama_tagihan"></td>` +
                        `<td class="nominal text-right"></td>` +
                        `<td class="nama_bulan"></td>` +
                        `<td style="width: 30px;">` +
                        `   <a href="javascript:void(0)" class="btn btn-sm" title="Hapus item ini">` +
                        `       <i class="material-icons list-icon fs-14 text-muted">delete</i>` +
                        `   </a>` +
                        `</td>` +
                        `</tr>`,
            });

            $modal.on('hidden.bs.modal', function () {
                // Reset form & list rincian bayar
                $form.find('input, select').val('').trigger('input');
                Lines.clear();
                _showLineEditor(false);
            });

            $selectSiswa.change(async function () {
                try {
                    const responSiswa = await $.ajax("{{ route('spp-siswa') }}");
                    const siswa = responSiswa.body.data.find(siswa => siswa.id === Number($selectSiswa.val()));
                    const valNis = siswa?.nis;
                    $inputNis.val(valNis).trigger('input');

                    const endpointTagihan = "{{ route('spp-siswa_tagihan', 'IDSISWA') }}".replace("IDSISWA", siswa.id);
                    const responTagihanSiswa = await $.ajax(endpointTagihan);
                    $inputTagihan.html(function () {
                        return [
                            `<option disabled selected></option>`,
                            ...responTagihanSiswa.body.data.map(tagihan => `<option value="${tagihan.id}">${tagihan.nama}</option>`)
                        ];
                    });

                    const nominalById = {};
                    const periodeById = {};
                    for (let tagihan of responTagihanSiswa.body.data) {
                        nominalById[tagihan.id] = tagihan.nominal;
                        periodeById[tagihan.id] = tagihan.priode;
                    }
                    $inputNominal.data('values', nominalById);
                    $inputNominal.data('periode', periodeById);
                } catch (error) {
                    console.error(error);
                }
            });

            $lineActionEditor.on('click', 'a', function () {
                if ( !($selectSiswa.val()) ) {
                    return $selectSiswa.focus();
                }
                if ( !($inputNis.val()) ) {
                    return $inputNis.focus();
                }
                _showLineEditor(true);
            });

            $inputTagihan.change(function () {
                _renderInputNominal($inputTagihan.val());
            });

            $lineActionTambah.on('click', function (ev) {
                ev.preventDefault();

                if (!_validateEditor()) {
                    return;
                }

                const linesData = Lines.get();
                const keyNew = !linesData.length ? 1 : linesData[linesData.length - 1].values().key + 1;
                const data = {
                    key: keyNew,
                    id_tagihan: $inputTagihan.val(),
                    nama_tagihan: $inputTagihan.find('option:selected').text(),
                    nominal: $inputNominal.val(),
                    bulan: $inputBulan.val(),
                    nama_bulan: $inputBulan.find('option:selected').text(),
                }
                _renderNewLine(data);
                _showLineEditor(false);
            });

            $('[rel="line-list"]').on('click', 'a', function () {
                const key = $(this).closest('tr').data('key');
                Lines.remove('key', key);
            });

            $modal.find('button[type="submit"]').on('click', async function (ev) {
                ev.preventDefault();

                _showLineEditor(false);

                if ( !($selectSiswa.val()) ) {
                    return $selectSiswa.focus();
                }
                if ( !($inputNis.val()) ) {
                    return $inputNis.focus();
                }
                if ( !($selectBentuk.val()) ) {
                    return $selectBentuk.focus();
                }
                if ( !($selectTahunAjaran.val()) ) {
                    return $selectTahunAjaran.focus();
                }

                try {
                    const responPost = await $.ajax("{{ route('spp-konfirmasi_create') }}", {
                        method: "POST",
                        data: {
                            id_kelas_siswa: $selectSiswa.val(),
                            nis: $inputNis.val(),
                            bentuk: $selectBentuk.val(),
                            tahun_ajaran: $selectTahunAjaran.val(),
                            details: Lines.get()?.map(line => line.values()),
                        }
                    });
                    $DataTable.ajax.reload();
                } catch (error) {
                    console.error(error);
                }

                $modal.modal('hide');
            });

            function _showLineEditor(show) {
                if (show) {
                    $lineActionEditor.hide();
                    $lineEditor.show();
                    $inputTagihan.focus();
                } else {
                    $lineActionEditor.show();
                    $lineEditor.hide();
                }
                $inputTagihan.val('');
                $inputNominal.val('');
                $inputBulan.val('');
            }

            function _renderNewLine(data) {
                Lines.add(data);
            }

            function _renderInputNominal(idTagihan) {
                const valNominalById = $inputNominal.data('values')[idTagihan];
                $inputNominal.val(valNominalById).trigger('input');

                // Beberapa jenis tagihan nominalnya bisa dibayar tidak sama dengan settingnya
                // ...jadi perlu editable. Sedangkan bulanan tidak bisa diedit, harus sama
                // dengan settingan.
                if ($inputNominal.data('periode')[idTagihan] === 'bulanan') {
                    $inputNominal.attr('disabled', true);
                } else {
                    $inputNominal.attr('disabled', false);
                }
            }

            function _validateEditor($fields) {
                if (!$inputTagihan.val()) {
                    $inputTagihan.focus();
                    return false
                }
                if (!$inputBulan.val()) {
                    $inputBulan.focus();
                    return false
                }
                return true;
            }
        }

        return { init };
    })();

    $(document).ready(function () {
        HalamanTransaksi.init();
    });
})(jQuery, window);
</script>
@endpush
</x-spp.layout>
