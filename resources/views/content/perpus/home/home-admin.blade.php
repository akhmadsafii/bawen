@extends('content.perpus.admin')
@section('content')
<section class="container-fluid">
	<div class="container d-flex flex-column">
		<div class="row">
			<div class="col-8">
				<h4 class="mb-4">Admin
					<br>
					<small class="subtitle-section">buku pilihan untuk anda</small>
				</h4>
			</div>
			<div class="col-4">
				<div class="my-1">
					<a href="{{ route('perpus-public') }}" class="btn btn-secondary btn-sm float-right">Area public</a>
				</div>
			</div>
		</div>

		<div class="container d-flex bg-white p-3">
			<!-- peminjaman -->
			<div class="col-8">
                <h5 class="mb-0">
                Data peminjaman
                </h5>
			  <canvas id="chartPeminjaman" style="width: 100%"></canvas>

			</div>
		</div>

		<div class="container d-flex my-3 pl-0">
			<!-- koleksi -->
			<div class="col-4 d-flex flex-column justify-content-start bg-white">
                <h5 class="mb-0">
                    Data klasifikasi pustaka
                </h5>
			  <canvas id="chartKoleksi" style="width: 100%"></canvas>

			</div>

			<!-- user -->
			<div class="col-3 d-flex flex-column justify-content-start bg-white mx-3">
                <h5 class="mb-0">
                Data user pustaka
                </h5>
			  <canvas id="chartUser" style="width: 100%"></canvas>
			</div>

            <!-- pengadaan -->
            <div class="col-5 d-flex flex-column justify-content-start bg-white">
                <h5 class="mb-0">
                    Data pengadaan diterima
                    </h5>
                <canvas id="chartPengadaan" style="width: 100%"></canvas>
            </div>

		</div>



	</div>
</section>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script type="text/javascript">

	let peminjaman = JSON.parse(`<?php echo $peminjaman; ?>`);
	let pengadaan = JSON.parse(`<?php echo $pengadaan; ?>`);
	let koleksi = JSON.parse(`<?php echo $koleksi; ?>`);
	let user = JSON.parse(`<?php echo $user; ?>`);

	const data_peminjaman = {
	  labels: peminjaman['label'],
	  datasets: [{
	    label: 'Bulan',
	    backgroundColor: 'rgb(255, 99, 132)',
	    borderColor: 'rgb(255, 99, 132)',
	    data: peminjaman['value'],
	    borderWidth: 1
	  }]
	};

	const data_pengadaan = {
	  labels: pengadaan['label'],
	  datasets: [{
	    label: 'Bulan',
	    backgroundColor: 'rgb(54, 162, 235)',
	    borderColor: 'rgb(54, 162, 235)',
	    data: pengadaan['value'],
	    borderWidth: 1
	  }]
	};

	const data_koleksi = {
	  labels: koleksi['label'],
	  datasets: [{
	    label: 'Data koleksi',
	    backgroundColor: [
	      'rgb(255, 99, 132)',
	      'rgb(75, 192, 192)',
	      'rgb(255, 205, 86)',
	      'rgb(201, 203, 207)',
	      'rgb(54, 162, 235)',
          'rgb(201, 203, 207)',
          'rgb(128,128,0)',
          'rgb(255,0,255)',
          'rgb(199,21,133)',
          'rgb(205,133,63)',
	    ],
	    data: koleksi['value'],
	    hoverOffset: 4
	  }]
	};

	const data_user = {
	  labels: user['label'],
	  datasets: [{
	    label: 'Data user',
	    backgroundColor: [
	      'rgb(255, 205, 86)',
	      'rgb(54, 162, 235)',
	      'rgb(255, 99, 132)',
	      'rgb(75, 192, 192)',
	      'rgb(201, 203, 207)',
          'rgb(128,128,0)',
          'rgb(255,0,255)',
          'rgb(199,21,133)',
          'rgb(205,133,63)',
	    ],
	    data: user['value'],
	    hoverOffset: 4
	  }]
	};

	const config_peminjaman = {
	  type: 'line',
	  data: data_peminjaman,
	  options: {
        responsive: false,
        maintainAspectRatio: false,
	    scales: {
	      y: {
	        beginAtZero: true
	      }
	    }
	  },
	};

	const config_pengadaan = {
	  type: 'bar',
	  data: data_pengadaan,
	  options: {
	    scales: {
	      y: {
	        beginAtZero: true
	      }
	    }
	  },
	};

	const config_koleksi = {
        type: 'doughnut',
        data: data_koleksi,
        options: {
            responsive: false,
            maintainAspectRatio: false,
            plugins : {
                legend : {
                    position : 'right'
                }
            }
        }
	};

	const config_user = {
	  type: 'doughnut',
	  data: data_user,

	};

	const myChartPengadaan = new Chart(
	    document.getElementById('chartPengadaan'),
	    config_pengadaan
	);

	const myChartPeminjaman = new Chart(
	    document.getElementById('chartPeminjaman'),
	    config_peminjaman
	);

	const myChartKoleksi = new Chart(
	    document.getElementById('chartKoleksi'),
	    config_koleksi
	);

	const myChartUser = new Chart(
	    document.getElementById('chartUser'),
	    config_user
	);



</script>
@endsection
