@extends('content.perpus.app')

@section('carousel')
    @include('components.perpus.carousel.carousel')
@endsection

@section('content')
<div class="row flex-column pr-0">

	<section id="main-section" class="container-fluid">

		<div class="container d-flex justify-content-between">
			<h5 class="mb-1">Buku Terbaru
				<br>
				<small class="subtitle-section">buku terbaru rilis</small>
			</h5>
            <h5 class="mb-1">
                <a href="{{ route('perpus-public-latest') }}">
                    <small class="subtitle-section">Lihat <br>semua</small>
                </a>
			</h5>
		</div>
		<div class="container d-flex flex-wrap justify-content-between pr-0">
            <!-- Daftar buku -->
            @forelse($bukus as $buku)
            <div class="col-5 col-md-3 col-xl-2 items mx-2 mx-md-2 my-3 {{ $loop->first ? 'gold' : '' }}">
                <a href="{{ route('detail-buku',$buku['id']) }}" style="width: 100%">
                    <div class="card-body p-1 pt-3">
                    <div class="card-image fit-height">
                        <img class="img-book" src="{{$buku['file']}}">
                    </div>
                    <h6 class="">{{$buku['judul']}}</h6>
                </div>
                </a>
            </div>

            @empty

            <h5>Pustaka tidak ada</h5>

            @endforelse
            <!-- end daftar buku -->
        </div>



        <div class="container d-flex justify-content-between">

			<h5 class="mb-1">Buku Terfavorit
				<br>
				<small class="subtitle-section">buku paling banyak dipinjam</small>
			</h5>
            <h5 class="mb-1">
                <a href="{{ route('perpus-public-fav') }}">
                    <small class="subtitle-section">Lihat <br>semua</small>
                </a>
			</h5>
		</div>

		<div class="container d-flex flex-wrap justify-content-between pr-0">
            <!-- Daftar buku favorite-->
            @forelse($buku_fav as $buku)
            <div class="col-5 col-md-3 col-xl-2 items mx-2 mx-md-2 my-3 {{ $loop->first ? 'gold' : '' }}">
                <a href="{{ route('detail-buku',$buku['id']) }}">
                    <div class="card-body p-1 pt-3">
                        <div class="card-image fit-height">
                            <img class="img-book" src="{{$buku['file']}}">
                        </div>
                        <h6 class="">{{$buku['judul']}}</h6>
                    </div>
                    <p class="my-0 pustaka_pinjam">{{$buku['jumlah_pinjam']}}x dipinjam</p>
                </a>
            </div>
            @empty
            <p>tidak ada buku</p>

            @endforelse
            <!-- end daftar buku -->
        </div>



        {{-- topik --}}
        {{-- <div class="container d-flex flex-column mb-4">
			<h5 class="mb-1">Topik pilihan
				<br>
				<small class="subtitle-section">topik yang mungkin Anda suka</small>
			</h5>

			<div class="container px-0">
			@foreach($topiks as $topik)
			<a href="{{route('perpus-topik-detail',$topik['id'])}}" class="btn btn-outline-info mx-2 my-2">{{$topik['nama']}}</a>
			@endforeach
			</div>
		</div> --}}


		<div class="container d-flex flex-wrap justify-content-between pr-0">
			<h5 class="mb-1">Pustaka digital pilihan
				<br>
				<small class="subtitle-section">buku pilihan untuk anda</small>
			</h5>
            <h5 class="mb-1">
                <a href="{{ route('perpus-public-pusdig') }}">
                    <small class="subtitle-section">Lihat <br>semua</small>
                </a>
			</h5>
		</div>
		@include('components.perpus.pusdig.main')
	</section>

    <h5>Pengumuman</h5>


    <div class="modal fade" tabindex="-1" role="dialog" id="modal-pengumuman">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Pengumuman</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <section id="pengumuman-section" class="{{ empty($pengumumans) ? 'd-none' : 'container-fluid d-flex flex-column'}}">
                    @forelse($pengumumans as $p)

                        <div class="my-0 p-2">
                            <h6 class="my-1">{!! $p['nama'] !!}
                                <br>
                                <small class="subtitle-section">{{ $p['tgl_akhir'] }}</small>
                            </h6>
                        </div>
                        <hr>

                    @empty
                        <p>tidak ada pengumuman</p>
                    @endforelse
                </section>
            </div>

          </div>
        </div>
      </div>

</div>

<script type="text/javascript">

    $(window).on('load', function() {

        setTimeout(function(){
            $('#modal-pengumuman').modal('show');
        }, 2000);

    });

    sessionStorage.setItem('saved', new Date().getTime())

	function cek(){
		let data = $('#pengumuman-section').contents().length;
		console.log(data);
		if(data <= 6){
			$("#main-section").attr('class', 'container-fluid');
			console.log("hilang");
		}
	}

	$('.alert').on('closed.bs.alert', function () {
  		cek();
	});

</script>


@endsection
