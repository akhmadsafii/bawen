@extends('content.perpus.app')
@section('content')
	<div class="container mb-5">
        <h5>Daftar semua
            <br>
            pustaka digital
        </h5>
		<div class="row flex-column flex-sm-row align-items-center">
            @include('components.perpus.pusdig.main')

            <div class="col-10">

                {{-- pagination --}}
                <nav aria-label="...">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="{{ route('perpus-public-pusdig',['page' => $data['current_page']-1]) }}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>

                        @for ( $i = $data['current_page']; $i < $data['current_page']+3 ; $i++ )
                        <li class="page-item ">
                            <a class="page-link" href="{{ route('perpus-public-pusdig',['page' => $i]) }}">{{ $i }}</a>
                        </li>
                        @endfor

                        <li class="page-item">
                            <a class="page-link" href="{{ route('perpus-public-pusdig',['page' => $i]) }}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
		</div>
	</div>
@endsection
