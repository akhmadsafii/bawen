@extends('content.perpus.app')

@section('content')
	<div class="container mb-5 px-3">
		<h4 class="text-center">Tentang</h4>
        <div class="row">

            <div class="container bg-white my-2 p-3">
                <h5>Tata Tertib</h5>
                <p>Dalam rangka menjamin dan memelihara ketersediaan bahan pustaka agar dapat menunjang proses belajar mengajar serta menjaga kenyaman pengguna perpustakaan (pemustaka), maka perlu ditetapkan tata tertib tentang pemanfaatan jasa layanan Perpustakaan Sekolah Tinggi Ilmu Ekonomi Indonesia (STIESIA), yaitu sebagai berikut:</p>
            </div>

            <div class="container bg-white my-2 p-3">
                <h5>Ketentuan Layanan Sirkulasi</h5>
                <ol>
                    <li>Yang berhak meminjam koleksi/bahan pustaka untuk dibawa pulang adalah semua civitas yang telah terdaftar sebagai anggota tetap perpustakaan.</li>
                    <li>Setiap transaksi peminjaman wajib membawa kartu tanda PERPUSTAKAAN milik sendiri dan tidak diperkenankan untuk pinjam meminjam kartu identitas orang lain.</li>
                    <li>Tidak diperkenankan meminjam buku dengan judul yang sama walaupun berbeda edisi atau tahun cetaknya.</li>
                    <li>Peminjaman bahan pustaka hanya dapat dilayani melalui loket peminjaman pada jam layanan. Dengan alasan apapun, Petugas Perpustakaan tidak dibenarkan meminjamkan bahan pustaka tanpa melalui prosedur atau diluar jam layanan.</li>
                    <li>Apabila anggota tetap meminjam 2 (dua) buku maka harus mengembalikan 2 (dua) buku dalam waktu yang bersamaan tanpa perpanjangan dan tidak dapat dikembalikan 1 (satu) per 1 (satu) dalam waktu yng berbeda.</li>
                </ol>
            </div>

            <div class="container bg-white my-2 p-3">
                <h5>Ketentuan Lain</h5>
                <ol>
                    <li>Pemustaka berhak mendapatkan pelayanan yang baik, ramah, sopan, bertanggung jawab dan adil baik dari pustakawan maupun petugas perpustakaan.</li>
                    <li>Anggota tetap berhak meminjam koleksi buku sesuai dengan ketentuan yang berlaku</li>
                    <li>Pemustaka berhak memanfaatkan semua jasa layanan dan fasilitas yang tersedia di perpustakaan</li>
                    <li>Pemustaka berhak menegur secara lisan atau secara tertulis melalui kotak saran yang tersedia pada website dan open public access catalogue (OPAC) apabila mendapat perlakuan yang tidak menyenangkan baik dari pustakawan maupun petugas perpustakaan.</li>
                    <li>Hal-hal lain yang belum diatur dalam Tata Tertib ini akan diatur kemudian</li>
                    <li>Tata tertib ini berlaku mulai sejak tanggal ditetapkan.</li>
                </ol>
            </div>

        </div>



	</div>

		
@endsection