<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.program-perpus.head')
</head>
<style>
    .pace{
        display: none;
    }
    .sidebar-dark .site-sidebar {
        background: #2471d2;
        border-color: rgba(255, 255, 255, 0.2);
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #ffffff;
    }

    .sidebar-dark .side-menu li a {
        color: #ffffff;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #919dad;
    }

    .container{
            border-radius: 10px;
        }

</style>


<body class="header-light sidebar-dark sidebar-expand">
    <div id="wrapper" class="wrapper">
        @include('includes.program-perpus.nav.nav_admin')
        <div class="content-wrapper">
            <aside class="site-sidebar scrollbar-enabled clearfix">
               @include('includes.program-perpus.menu.menu-admin')
            </aside>
            <main class="main-wrapper clearfix">
                <div class="row page-title clearfix" style="height: 0;">
                </div>
                <div class="widget-list">
                    @yield('content')
                </div>
            </main>
        </div>
    </div>
   @include('includes.program-perpus.foot')
</body>

</html>
