@extends('content.perpus.app')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}
</style>

<section class="container">
	<div class="widget-bg">
        <div class="widget-body clearfix">
            <h5 class="box-title">Halaman Profile Admin</h5>
            @if (Session::has('message'))
		      <div class="alert alert-warning alert-dismissible fade show" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		            </button>
		            <p class="mb-1">{{ Session::get('message') }}</p>    
		      </div>
		    @endif
            <div class="tabs tabs-vertical">
                <ul class="nav nav-tabs flex-column">
                    <li class="nav-item"><a class="nav-link" href="#home-tab-v1" data-toggle="tab" aria-expanded="true">Ubah Profile</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#profile-tab-v1" data-toggle="tab" aria-expanded="true">Ubah Password</a>
                    </li>
                </ul>
                <!-- /.nav-tabs -->
                <div class="tab-content">
                	<!-- ubah profil -->
                    <div class="tab-pane active" id="home-tab-v1">
                        <div class="container">
                        	<h5 class="box-title">Update Profile</h5>
                        	<form action="{{ route('update-perpus-admin') }}" method="POST" enctype="multipart/form-data">
                			@csrf
                        		<div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l16">Foto</label>
                                    <div class="col-md-9">
                                        <input id="l16" type="file" name="foto" value="{{$admin['foto']}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Nama</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nama" value="{{$admin['nama']}}" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Username</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="username" value="{{$admin['username']}}" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">NIP</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="no_induk" value="{{$admin['nip']}}" type="number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Email</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="email" value="{{$admin['email']}}" type="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Telepon</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="telepon" value="{{$admin['telepon']}}" type="number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="tempat_lahir" value="{{$admin['tempat_lahir']}}" type="text">
                                    </div>
                                </div>
                                <fieldset class="form-group">
								    <div class="row">
								      <legend class="col-form-label col-md-3 pt-0">Jenis Kelamin</legend>
								      <div class="col-md-9">
								        <div class="form-check">
								          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" name="jenkel" value="l" checked>
								          <label class="form-check-label" for="gridRadios1">
								            Laki-laki
								          </label>
								        </div>
								        <div class="form-check">
								          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" name="jenkel" value="p">
								          <label class="form-check-label" for="gridRadios2">
								            Perempuan
								          </label>
								        </div>
								      </div>
								    </div>
							  	</fieldset>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="tgl_lahir" value="{{$admin['tgl_lahir']}}" type="date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Alamat</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="alamat" value="{{$admin['alamat']}}" type="text">
                                    </div>
                                </div>
                                <button class="btn btn-success" type="submit">Update</button>
                                <button class="btn btn-secondary" type="submit">Batal</button>
                            </form>
                        </div>
                    </div>
                    <!-- ubah password -->
                    <div class="tab-pane" id="profile-tab-v1">
                        <div class="container">
                        	<h5 class="box-title">Ubah Password</h5>
                        	<form action="{{ route('change-perpus-password') }}" method="POST">
                                <div class="form-group mr-t-30">
                                    <label for="sample1UserName">Password Saat Ini</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fas fa-key"></i>
                                        </div>
                                        <input type="text" class="form-control" name="current_password" id="sample1UserName" placeholder="password saat ini">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="sample1Password">Password Baru</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fas fa-key"></i>
                                        </div>
                                        <input type="password" class="form-control" name="new_password" placeholder="password baru">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sample1Password">Konfirmasi Password Baru</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fas fa-key"></i>
                                        </div>
                                        <input type="password" class="form-control" name="confirm_password" placeholder="password baru">
                                    </div>
                                </div>
                                
                                <div class="form-actions">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 btn-list">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                <button type="button" class="btn btn-default">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

</section>

		
@endsection