@extends('content.perpus.app')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}
</style>

<div class="container bg-white p-3">
	<h5>Data Peminjaman</h5>
	<table class="table table-striped table-responsive m-0" id="tabel-peminjaman" style="width: 100%">
	    <thead>
	        <tr>
	            <th>No</td>
	            <th>Buku</th>
	            <th>Kode Buku</th>
	            <th>Keterangan</th>
	            <th>Tanggal pinjam</th>
	            <th>Tanggal kembali</th>
	            <th>Status</th>
	            <th>Denda</th>
	
	        </tr>
	    </thead>
	    <tbody>  
	                    
		</tbody>
	</table>

	<!-- <table class="table table-striped table-responsive" id="tabel-peminjaman">
        <thead>
            <tr>
                <th>No</th>
                <th>Buku</th>
                <th>Kode Buku</th>
                <th>Tanggal pinjam</th>
                <th>Tanggal kembali</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>  
                        
		</tbody>
    </table> -->
</div>


@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')

<script>

	// const konfigUmum = {
 //            responsive: true,
 //            serverSide: true,
 //            processing: true,
 //            ordering: true,
 //            paging: true,
 //            searching: true,
 //       };

	// let tabel = $('#tabel-peminjaman');
	// let url = "{{ route('peminjaman-user-perpus') }}";
 // 	let column = 
	//     [
	//         {
	//             data: 'DT_RowIndex',
	//             name: 'DT_RowIndex',
	//             width: "5%"
	//         },
	//         {
	//             data: 'buku',
	//             name: 'buku',
	//             width: "35%",
	//             orderable :true
	//         },
	//         {
	//             data: 'kode_item',
	//             name: 'kode_item',
	//             width: "10%",
	//             orderable :true
	//         },
	//         {
	//             data: 'tanggal_pinjam',
	//             name: 'tanggal_pinjam',
	//             width: "20%",
	//             orderable :true,
	//             visible: true
	//         },
	//         {
	//             data: 'tanggal_dikembalikan',
	//             name: 'tanggal_dikembalikan',
	//             width: "20%",
	//             orderable :true,
	//             searchable: true
	//         },
	//         {
	//             data: 'status_dipinjam',
	//             name: 'status_dipinjam',
	//             width: "10%",
	//             searchable: true
	//         }
	//     ];

	// function MakeTable(tabel,url,column){
 //        tabel.DataTable({
 //            ...konfigUmum,
 //            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
 //            ajax: {
 //                "url": url,
 //                "method": "GET"
 //            },
 //            columns: column
 //        });
 //    }

 //    MakeTable(tabel,url,column);

    // new

    let tabel = $('#tabel-peminjaman');
	let url = "{{ route('peminjaman-user-perpus') }}";
 	let column = 
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'kode',
	            name: 'kode',
	            width: "35%",
	            orderable :true
	        },
	        {
	            data: 'keterangan',
	            name: 'keterangan',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'tanggal_pinjam',
	            name: 'tanggal_pinjam',
	            width: "20%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_kembali',
	            name: 'tanggal_kembali',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'status',
	            name: 'status',
	            width: "10%",
	            searchable: true
	        },
	        {
	            data: 'denda',
	            name: 'denda',
	            width: "10%",
	            searchable: true
	        }
	    ];

	MakeTable(tabel,url,column);

</script>


@endsection