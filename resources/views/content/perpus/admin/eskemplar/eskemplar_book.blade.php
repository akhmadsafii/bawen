@extends('content.perpus.admin')
@section('content')

    <style type="text/css">
        h6,
        p {
            margin-top: 0.25rem !important;
            margin-bottom: 0.25rem !important;
        }

        img {
            height: 250px;
        }

        .caret {
            display: none !important;
        }

        thead,
        tbody {
            width: 100%;
        }

    </style>

    <section class="container">
        <div class="container d-flex bg-white p-3">
            @include('components.perpus.buku.detail-admin')
        </div>

        <div class="container px-0">
            <div class="widget-bg mb-5 pt-0">
                <div class="widget-body clearfix px-3">

                    <div class="tabs tabs-bordered">
                        <ul class="nav nav-tabs d-flex justify-content-end">
                            <li class="nav-item "><a class="nav-link" href="#home-tab-bordered-1" data-toggle="tab"
                                    aria-expanded="true">Detail Pustaka</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-tab-bordered-1"
                                    data-toggle="tab" aria-expanded="true">Eskemplar</a>
                            </li>

                            <li class="nav-item"><a class="nav-link" href="#riwayat-tab-bordered-1"
                                    data-toggle="tab" aria-expanded="true">Riwayat</a>
                            </li>
                        </ul>

                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <!-- Detail Buku -->
                            <div class="tab-pane" id="home-tab-bordered-1">
                                <div class="row">
                                    <div class="col-4">
                                        <h6>ISBN</h6>
                                        <h6>Deskripsi fisik</h6>
                                        <h6>Jumlah eskemplar</h6>
                                        <h6>Penerbit</h6>
                                        <h6>Tahun terbit</h6>
                                        <h6>Pengarang</h6>
                                        <h6>Bahasa</h6>
                                        <h6>Halaman</h6>
                                        <h6>Lokasi</h6>

                                    </div>
                                    <div class="col-8">
                                        <h6>: {{ $buku['ISBN'] }}</h6>
                                        <h6>: {{ $buku['desc_fisik'] }}</h6>
                                        <h6>: {{ $buku['jumlah_eskemplar'] }}</h6>
                                        <h6>: {{ $buku['penerbit'] }}</h6>
                                        <h6>: {{ $buku['tahun_terbit'] }}</h6>
                                        <h6>:
                                            @foreach ($buku['penulis'] as $key => $penulis)
                                                {{ $penulis['nama'] }}
                                            @endforeach
                                        </h6>
                                        <h6>: {{ $buku['bahasa'] }}</h6>
                                        <h6>: {{ $buku['halaman'] }}</h6>
                                        <h6>: {{ $buku['rak'] }}</h6>

                                    </div>
                                </div>
                            </div>

                            @if ($message = Session::get('error'))
                                <div class="alert alert-error border-error" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <div class="widget-list">
                                        <div class="col-md-12 widget-holder text-center text-red">
                                            <i class="material-icons list-icon md-48">warning</i>
                                            <div class="widget-body clearfix text-center ">
                                                <strong class="text-center ">{{ $message }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <!-- Item Tersedia -->
                            <form action="{{ route('item-prints') }}" method="POST">
                                @csrf
                                <div class="tab-pane" id="profile-tab-bordered-1">
                                    <button type="submit" class="btn btn-info btn-sm mb-1">Cetak</button>
                                    {{-- <button onclick="cetak()" class="btn btn-primary btn-sm mb-1">Cetak</button> --}}
                                    <a onclick="check_all()" class="btn btn-primary text-white btn-sm mb-1">Check all</a>

                                    <div class="list-group bg-transparent auto" id="list-item">
                                        @foreach ($items as $item)
                                            <a href="#"
                                                class="list-group-item list-group-item-action d-flex justify-content-end">
                                                <input type="checkbox" class="check_print" id="print_" name="data[]"
                                                    value="{{ $item['kode'] }}">
                                                <span class="mr-auto">{{ $item['buku'] }} -
                                                    {{ $item['kode'] }}</span>
                                                <div>
                                                    <div>
                                                        <span
                                                            class="font-weight-bold text-{{ $item['status_dipinjam'] == 'tersedia' ? 'success' : 'danger' }} ">{{ $item['status_dipinjam'] }}
                                                        </span>

                                                        @if ($item['status_dipinjam'] != 'tersedia')
                                                            <span class="font-weight-bold text-danger">
                                                                {{ $item['peminjam'] }}</span>
                                                    </div>
                                                    <p class="my-0">kembali pada {{ $item['tanggal_kembali'] }}
                                                    </p>
                                                @else
                                                </div>
                                        @endif
                                    </div>

                                    <span class="ml-2">
                                        <button type="button" onclick="modalEdit({{ $item['id'] }})"
                                            class="btn btn-success btn-sm btn-edit-{{ $item['id'] }}"><i
                                                class="fas fa-pencil-alt"></i></button>
                                        <button type="button" onclick="submit_delete({{ $item['id'] }})"
                                            class="btn btn-primary btn-sm btn-delete-{{ $item['id'] }}"><i
                                                class="fas fa-trash-alt"></i></button>
                                    </span>
                                    </a>
                                    @endforeach
                                </div>
                        </div>
                        <form>

                            <!-- Cetak item -->
                            <div class="tab-pane" id="cetak-tab-bordered-1">
                                <div class="row">
                                    <div class="col-10" id="printArea">
                                    </div>
                                    <div class="col-2 mt-2 mb-2">
                                        <button onclick="PrintWindow()" class="btn btn-primary">Cetak</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="riwayat-tab-bordered-1">
                                <div class="container-fluid">
                                    <table class="table table-striped table-responsive tabel-peminjaman-histori"
                                        style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Peminjam</th>
                                                <th>Kode Buku</th>
                                                <th>Tanggal pinjam</th>
                                                <th>Tanggal kembali</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.tabs -->
            </div>
        </div>
        </div>


        @include('includes.program-perpus.notify')

    </section>

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/barcodes/JsBarcode.ean-upc.min.js"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': `Bearer {{ Session::get('token') }}`
            }
        });

        function noti(tipe, value) {
            $.toast({
                icon: tipe,
                text: value,
                hideAfter: 5000,
                showConfirmButton: true,
                position: 'top-right',
            });
            return true;
        }

        function swa(status, message, icon) {
            swal(
                status,
                message,
                icon
            );
            return true;
        }

        const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: true,
            paging: true,
            searching: true,
        };

        let tabel = $('.tabel-peminjaman-histori');
        let url = "{{ route('peminjaman-buku') }}";
        let column = [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: "5%"
            },
            {
                data: 'peminjam',
                name: 'peminjam',
                width: "15%",
                orderable: true
            },
            {
                data: 'kode_item',
                name: 'kode_item',
                width: "10%",
                orderable: true
            },
            {
                data: 'tanggal_pinjam',
                name: 'tanggal_pinjam',
                width: "20%",
                orderable: true,
                visible: true
            },
            {
                data: 'tanggal_kembali',
                name: 'tanggal_kembali',
                width: "20%",
                orderable: true,
                searchable: true
            },
            {
                data: 'status_dipinjam',
                name: 'status_dipinjam',
                width: "10%",
                searchable: true
            }
        ];

        tabel.DataTable({
            ...konfigUmum,
            dom: '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET",
                "data": {
                    'id': "{{ $buku['id'] }}"
                }
            },
            columns: column
        });

        function check_all() {
            var lenght_checkbox = $('input[type="checkbox"]').length;
            if(lenght_checkbox == 0){
                alert('Ops Data Belum tersedia');
            }else{
                $('input[type="checkbox"]').prop("checked", true);
            }
        }

        function PrintWindow() {
            var divContents = document.getElementById("printArea").innerHTML;
            var a = window.open('', '', 'height=1000, width=1200');
            a.document.write('<html>');
            a.document.write('<body>');
            a.document.write('<div class="main">');
            a.document.write(divContents);
            a.document.write('</div>');
            a.document.write('</body></html>');
            a.document.close();
            a.print();
        }

        function cetak() {

            let items = `
            <style type="text/css">
                * {
                    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
                    color-adjust: exact !important;                 /*Firefox*/
                }

                body{
                    display : inline-block;
                    background-color : lightblue;
                }

                .label-main{
                    margin : 0;
                }

                .label-body{
                    border: 1px solid black;
                    width: 350px;
                    background-color: white;
                }
                    .label-head{
                        border-bottom: 1px solid black;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

                .label-content{
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                }

                .label-barcode{
                    padding: 3px 5px;
                }

                .alt-code{
                    margin-bottom: 0;
                }

                .barcode{
                    display: flex;
                    justify-content: center;
                }

                img{
                    height: 25px;
                    margin: 0 10px;
                }

                .label-main{
                    display: flex;
                }

                h5,h6{
                    margin : 3px 0;
                }


                @media print{

                    body{
                        margin : 0;
                        box-sizing : border-box;
                        display : inline-block;
                    }

                    .label-main{
                        display : inline-block;
                        margin : 1.3px 0;
                    }

                    .alt-code{
                        margin-top : 0;
                        margin-bottom: 0;
                    }

                    h6,h5,p{
                        text-align: center;
                        margin-bottom : 0;
                    }

                }


            </style>
        `;

            function display(item) {
                items += `
            <div class="label-main">
				<div class="label-body">
					<div class="label-head">
						<img src="{{ $label['logo'] }}">
						<span>
							<h5 class="mb-0">{{ $label['nama_label'] }}</h5>
							<h6 class="mb-0 mt-0">{{ $label['nama_instansi'] }}</h6>
							<p class="mb-0">{{ $label['alamat'] }}</p>
						</span>
					</div>

					<div class="label-barcode">
                        <div class="barcode">
                            {!! DNS1D::getBarcodeHTML('${item}', 'C128', 2.6, 25) !!}
                        </div>
                        <p class="alt-code">${item}</p>
                    </div>
				</div>
			</div>
            `;
            }

            let kode = [];
            $("input:checkbox[class=check_print]:checked").each(function() {
                kode.push($(this).val());
            });

            kode.forEach(display);
            $('#printArea').html(items);
            PrintWindow();
        }

        function modalEdit(id) {
            $.ajax({
                type: "get",
                url: "{{ route('item-by-id') }}",
                data: {
                    'id': id
                },
                beforeSend: () => {
                    $('.btn-edit-' + id).html(`
                    <i class="fa fa-spin fa-spinner"></i>
                `);
                },
                success: (data) => {
                    $('#modalEdit').modal('show');
                    $('#inputId').val(data.data.id);
                    $('#inputKode').val(data.data.kode);
                    $('#inputKondisi').val(data.data.id_kondisi);
                    $('#inputTanggal').val(data.data.tanggal_masuk);
                    $('.btn-edit-' + id).html(`
                    <i class="fas fa-pencil-alt"></i>
                `);
                },
                error: function(data) {
                    noti(data.icon, data.message);
                }
            });
        }

        function submit_delete(id) {

            $.ajax({
                type: "post",
                url: "{{ route('item-delete') }}",
                data: {
                    'id': id
                },
                beforeSend: () => {
                    $('.btn-delete-' + id).html(`
                    <i class="fa fa-spin fa-spinner"></i>
                `);
                },
                success: (data) => {
                    noti(data.icon, data.message);
                    $('#modalEdit').modal('hide');
                    $("#list-item").load(" #list-item");
                    $('.btn-delete-' + id).html(`
                    <i class="fas fa-trash-alt"></i>
                `);
                },
                error: function(data) {
                    noti(data.icon, data.message);
                }
            });
        }

        $('#form-edit-item').on('submit', function() {
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type: "POST",
                url: "{{ route('item-store') }}",
                data: data,
                success: (data) => {
                    console.log(data);
                    noti(data.icon, data.message);
                    $("#list-item").load(" #list-item");
                }
            });
        });
    </script>


@endsection
