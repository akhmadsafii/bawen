@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 250px;
	}
</style>

<section class="container">

	<h4 class="mb-1">Daftar Penerbit
		<br>
		<small class="subtitle-section">daftar semua penerbit</small>
	</h4>

	<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
     </button>

	<table class="table table-striped table-responsive" id="tabel-penerbit" width="100%">
	    <thead>
	        <tr>
	            <th data-identifier>No</th>
	            <th data-editable>Kode</th>
	            <th data-editable>Penerbit</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>

		</tbody>
	</table>

</section>
@include('includes.program-perpus.notify')
@include('components.perpus.modal.master-store')
@include('includes.program-perpus.datatable')

<script src="{{ asset('asset/js/jquery.tabledit.js') }}"></script>

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('#tabel-penerbit');
	let url = "{{ route('perpus-penerbit') }}";
	let column = [
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
		{
            data: 'kode',
            name: 'kode',
            width: "25%",
            orderable :true,
            searchable: true
	    },
	    {
            data: 'nama',
            name: 'nama',
            width: "30%",
            orderable :true,
            searchable: true
        },
        {
            data: 'aksi',
            name: 'aksi',
            width: "10%"
        },
	];

	let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar penerbit'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar penerbit'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar penerbit'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

	MakeFullTable(tabel,url,column);

	function edit_button(id){

		if($('.input-kode-'+ id).css('display') == 'none'){
			$('.input-kode-'+ id).css("display", "block");
			$('.show-kode-'+ id).css("display", "none");
			$('.input-nama-'+ id).css("display", "block");
			$('.show-nama-'+ id).css("display", "none");
			$('.tabledit-save-button-'+ id).css("display", "block");
		}else{
			$('.input-kode-'+ id).css("display", "none");
			$('.show-kode-'+ id).css("display", "block");
			$('.input-nama-'+ id).css("display", "none");
			$('.show-nama-'+ id).css("display", "block");
			$('.tabledit-save-button-'+ id).css("display", "none");
		}
	}

	function submit_edit(id){

		let kode = $('.input-kode-'+ id).val();
		let nama = $('.input-nama-'+ id).val();

		$.ajax({
            type : "post",
            url  : "{{route('perpus-penerbit-update')}}",
            data : {
            	'id' : id,
            	'nama' : nama,
            	'kode' : kode
            },
            beforeSend: function() {
                $('.tabledit-save-button-'+ id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#tabel-penerbit').DataTable().ajax.reload();
                $('.tabledit-save-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('.tabledit-save-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
            }
        });

	}

	function submit_delete(id){
		$.ajax({
            type : "post",
            url  : "{{route('perpus-penerbit-delete')}}",
            data : {
            	'id' : id
            },
            beforeSend: function() {
                $('.tabledit-delete-button-'+ id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#tabel-penerbit').DataTable().ajax.reload();
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            }
        });

	}

	function modalCreate(){
		$('#modal-store-master').modal('show');
	}

	$('#form-add-master').on('submit',function(){
		let data = $(this).serialize();

        $.ajax({
            type : "post",
            url  : "{{route('perpus-penerbit-store')}}",
            data : data,
            success : (data) =>{
                noti(data.icon, data.message);
                $('#form-add-master').trigger("reset");
                $('#modal-store-master').modal('hide');
                $('#tabel-penerbit').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);

            }
        });

	});




</script>


@endsection
