@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}
</style>

<section class="container-fluid">

	<div class="widget-bg mb-5">

		<!-- header -->
		<div class="row p-3">
			<div class="col-4">
				<h4>Pengadaan Diterima</h4>

			</div>
		</div>

		<!-- body tab -->
		<div class="widget-body clearfix px-3">
			<!-- tabs -->
			<div class="tabs tabs-bordered">
				<ul class="nav nav-tabs">
					<li class="nav-item "><a class="nav-link" href="{{route('pengadaan-active')}}">Berjalan</a>
                    </li>
                    <li class="nav-item active"><a class="nav-link" href="#accept-tab-bordered-1" data-toggle="tab" aria-expanded="true">Diterima</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('pengadaan-rejected')}}">Ditolak</a>
                    </li>
                </ul>
			</div>

			<!-- content-tab -->
			<div class="tab-pane" id="accept-tab-bordered-1">
				<div class="container">

					<table class="table table-striped table-responsive tabel-pengadaan" id="">
					    <thead>
					        <tr>
					            <th>No</th>
					            <th>Nama</th>
					            <th>Buku</th>
					            <th>Keterangan</th>
					            <th>Jumlah</th>
					            <th>Prioritas</th>
					            <th>Tanggal diterima</th>
					            <th>Total harga</th>
					            <th>Aksi</th>
					        </tr>
					    </thead>
					    <tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</section>


@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });


   let tabel = $('.tabel-pengadaan');
	let url = "{{ route('pengadaan-accepted') }}";
 	let column =
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'nama',
	            name: 'nama',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'keterangan',
	            name: 'keterangan',
	            width: "20%",
	            orderable :true
	        },
	        {
	            data: 'jumlah',
	            name: 'jumlah',
	            width: "5%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'prioritas',
	            name: 'prioritas',
	            width: "15%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_diterima',
	            name: 'tanggal_diterima',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'total_harga',
	            name: 'total_harga',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'aksi',
	            name: 'aksi',
	            width: "10%",
	            searchable: true
	        }
	    ];


    MakeTable(tabel,url,column);


    function submit_delete(id){
    	$.ajax({
    		type : "post",
    		data : {
    			'id' : id
    		},
    		url  : "{{ route('pengadaan-delete') }}",
    		beforeSend : ()=>{
				$('.tabledit-delete-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) =>{
				noti(data.icon, data.message);
				$('.tabledit-delete-button-'+id).html(
                    '<i class="fas fa-trash-alt"></i>');
				tabel.DataTable().ajax.reload();

			}
		});
    }


</script>


@endsection
