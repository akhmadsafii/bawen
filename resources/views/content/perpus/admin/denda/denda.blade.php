@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

</style>

<section class="container">

	<h4 class="mb-1">Daftar riwayat denda
	</h4>

	<table class="table table-striped table-responsive" id="tabel-denda" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Peminjam</th>
	            <th>Hari terlewat</th>
	            <th>Denda</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>  
	                    
		</tbody>
		<tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
	</table>

</section>

<!-- detail denda -->
<div class="modal modal-info fade bs-modal-lg" tabindex="-1" role="dialog" id="modal-detail-denda" aria-hidden="true" style="display: none">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            <h5 class="modal-title" id="myLargeModalLabel">Detail denda</h5>
	        </div>
	        <div id="modal-body-denda" class="modal-body">
	            
	        </div>
	        <div class="modal-footer py-1">
	            <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Tutup</button>
	        </div>
	    </div>
	</div>
</div>

@include('includes.program-perpus.notify')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: true,
        paging: true,
        searching: true,
   };

	let tabel = $('#tabel-denda');
	let url = "{{ route('perpus-denda') }}";
	let column = [
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
    	{
          data: 'nama',
          name: 'nama',
          width: "30%",
          orderable :true,
          searchable: true
      	},
      	{
          data: 'hari_terlewat',
          name: 'hari_terlewat',
          width: "10%",
          orderable :true,
          searchable: true
      	},
      	{
          data: 'denda',
          name: 'denda',
          width: "30%",
          orderable :true,
          searchable: true
      	},
      	{
          data: 'aksi',
          name: 'aksi',
          width: "10%"
      	},
	];

	let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar topik'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar topik'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar topik'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

	tabel.DataTable({
        ...konfigUmum,
        dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
        ajax: {
            "url": url,
            "method": "GET"
        },
        columns: column,
        buttons: buttons,
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var totalHarga = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
            }, 0 );

            $( api.column( 2 ).footer() ).html('Total : ');
            $( api.column( 3 ).footer() ).html(totalHarga.toLocaleString());
        },
        columnDefs:
            [
                {
                    targets: 3,
                    render: $.fn.dataTable.render.number(',', '.', 0, '')
                },
        ]
    });

	function modalDetailDenda(id){
    	$.ajax({
    		type : "POST",
    		url	 : "{{ route('denda-detail-perpus')}}",
    		data : {
    			'id' : id
    		},
    		beforeSend : function(){
    			$('#btnInfo-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>'
                );
    		},
    		success : function(data){
    			console.log(data);
    			$('#modal-detail-denda').modal('show');
    			$('#modal-body-denda').html(`
    				<div class="row">
                    	<div class="col-4">
                            <h6>Nama petugas</h6>
                            <h6>Nama peminjam</h6>
                    		<h6>Buku</h6>
                    		<h6>Kode buku</h6>
                    		<h6>Tanggal pinjam</h6>
                    		<h6>Tanggal kembali</h6>
                    		<h6>Tanggal dikembalikan</h6>
                    		<h6>Keterangan</h6>
                    		<h6>Denda</h6>
                    		<h6>Hari terlewat</h6>
                    	</div>
                    	<div class="col-8">
                            <h6>: ${data.data.nama_petugas}</h6>
                            <h6>: ${data.data.nama_peminjam}</h6>
                    		<h6>: ${data.data.buku}</h6>
                    		<h6>: ${data.data.kode_buku}</h6>
                    		<h6>: ${data.data.tanggal_pinjam}</h6>
                    		<h6>: ${data.data.tanggal_kembali}</h6>
                    		<h6>: ${data.data.tanggal_dikembalikan}</h6>
                    		<h6>: ${data.data.keterangan}</h6>
                    		<h6>: ${data.data.denda}</h6>
                    		<h6>: ${data.data.hari_terlewat}</h6>
                    		
                    	</div>
                    </div>
    			`);
    			$('#btnInfo-'+id).html(
                    '<i class="fas fa-info-circle"></i>'
                );
    		}
    	});
    }

</script>

@endsection