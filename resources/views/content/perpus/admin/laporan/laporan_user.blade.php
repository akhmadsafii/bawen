@extends('content.perpus.admin')
@section('content')

<section class="container-fluid">
	<div class="d-flex justify-content-end mb-2">
		<div class="mr-2">
		<button onclick="PrintWindow()" class="btn btn-primary btn-sm">Print</button>
		</div>
		<div class="dropdown">
		  <a class="btn btn-secondary dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
		    Bulan
		  </a>

		  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 1]) }}">Januari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 2]) }}">Februari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 3]) }}">Maret</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 4]) }}">April</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 5]) }}">Mei</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 6]) }}">Juni</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 7]) }}">Juli</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 8]) }}">Agustus</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 9]) }}">September</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 10]) }}">Oktober</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 11]) }}">November</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-user',['bulan'=> 12]) }}">Desember</a></li>

		  </ul>
		</div>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 80%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan User {{$kartu['nama_sistem']}}</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
            <p class="text-center my-1 mb-3">Bulan {{$bulan}} {{  date('Y') }}</p>
		</div>

		<div class="container my-1">
			<p class="mb-0">Jumlah admin : {{ count($admins) }}</p>
			<p class="mb-0">Jumlah user : {{ $users['jumlah'] }}</p>
		</div>

		<div class="container">
			<h5>User</h5>
			@foreach($users['role'] as $user => $values)
			<p class="my-1 text-uppercase ml-2">{{$user}}</p>
			<table class="table table-bordered ml-2" style="width: 90%">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">No Angoota</th>
			      <th scope="col">Nama</th>
			      <th scope="col">Username</th>
			      <th scope="col">Tipe Angggota</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@php
                    $no = 1;
                @endphp
			  	@foreach($values as $usr => $value)
			    <tr>
			      <td>{{ $no++ }} </td>
			      <td>{{ $value['no_anggota'] }} </td>
			      <td>{{ $value['nama'] }} </td>
			      <td>{{ $value['username'] }} </td>
			      <td>{{ $value['tipe_anggota'] }} </td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
			@endforeach

			<h5>Admin</h5>

			<table class="table table-bordered ml-2" style="width: 90%">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">NIP</th>
			      <th scope="col">Nama</th>
			      <th scope="col">Username</th>
			      <th scope="col">Telepon</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@php
                    $no = 1;
                @endphp
			  	@foreach($admins as $adm)
			    <tr>
			      <td>{{ $no++ }} </td>
			      <td>{{ $adm['nip'] }} </td>
			      <td>{{ $adm['nama'] }} </td>
			      <td>{{ $adm['username'] }} </td>
			      <td>{{ $adm['telepon'] }} </td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>

		</div>
	</div>
</section>

<script type="text/javascript">

    function back() {
        location.reload();
    }

	window.onafterprint = back;

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        let content = `
        	${originalContents}
        `;
        console.log(originalContents);
        document.body.innerHTML = originalContents;
	}



</script>


@endsection
