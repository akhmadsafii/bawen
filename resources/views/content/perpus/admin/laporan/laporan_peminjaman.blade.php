@extends('content.perpus.admin')
@section('content')

<style type="text/css">

	.table>:not(:first-child) {
	    border-top: 1px solid currentColor;
	}
	p{
		margin-bottom: 0;
	}

</style>

<section class="container-fluid">
	<div class="d-flex justify-content-end mb-2">
		<div class="mr-2">
		<button onclick="PrintWindow()" class="btn btn-primary btn-sm">Print</button>
		</div>
		<div class="dropdown">
		  <a class="btn btn-secondary dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
		    Bulan
		  </a>

		  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 1]) }}">Januari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 2]) }}">Februari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 3]) }}">Maret</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 4]) }}">April</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 5]) }}">Mei</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 6]) }}">Juni</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 7]) }}">Juli</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 8]) }}">Agustus</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 9]) }}">September</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 10]) }}">Oktober</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 11]) }}">November</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-peminjaman',['bulan'=> 12]) }}">Desember</a></li>

		  </ul>
		</div>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 90%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan Peminjaman {{$kartu['nama_sistem']}}</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
			<p class="text-center my-1 mb-4">Bulan {{$bulan}}</p>
		</div>

		<div class="container">
			<table class="table table-bordered" style="width: 100%">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">Peminjam</th>
			      <th scope="col">Pustaka</th>
			      <th scope="col">Tanggal pinjam</th>
			      <th scope="col">Tanggal kembali</th>
			      <th scope="col">Status pinjam</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@php
                    $no = 1;
                @endphp
			  	@foreach($peminjaman as $pm )
			    <tr>
			      <td>{{ $no++ }} </td>
			      <td>{{ $pm['peminjam'] }} </td>
			      <td>{{ $pm['buku'][0]}} {{$pm['kode_item']}}</td>
			      <td>{{ $pm['tanggal_pinjam'] }}</td>
			      <td>{{ $pm['tanggal_dikembalikan'] }}</td>
			      <td>{{ $pm['status_dipinjam'] }}</td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
		</div>
	</div>

</section>

<script type="text/javascript">

    function back() {
        location.reload();
    }

	window.onafterprint = back;

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}



</script>


@endsection
