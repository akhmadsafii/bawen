@extends('content.perpus.admin')
@section('content')

<style type="text/css">

	.table>:not(:first-child) {
	    border-top: 1px solid currentColor;
	}
	p{
		margin-bottom: 0;
	}

</style>

<section class="container-fluid">
	<div class="d-flex justify-content-end mb-2">
		<div class="mr-2">
		<button onclick="PrintWindow()" class="btn btn-primary btn-sm">Print</button>
		</div>
		<div class="dropdown">
		  <a class="btn btn-secondary dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
		    Bulan
		  </a>

		  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 1]) }}">Januari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 2]) }}">Februari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 3]) }}">Maret</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 4]) }}">April</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 5]) }}">Mei</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 6]) }}">Juni</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 7]) }}">Juli</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 8]) }}">Agustus</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 9]) }}">September</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 10]) }}">Oktober</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 11]) }}">November</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-pengadaan',['bulan'=> 12]) }}">Desember</a></li>

		  </ul>
		</div>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 90%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan Pengadaan {{$kartu['nama_sistem']}}</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
			<p class="text-center my-1 mb-4">Bulan {{$bulan}}</p>
		</div>

		<div class="container">
			<div class="d-flex my-1">
				<div class="col-4">
					<p class="mb-0">Pengadaan</p>
					<p class="mb-0">Pengadaan diterima</p>
					<p class="mb-1">Pengadaan ditolak</p>
				</div>
				<div class="col-4">
					<p class="mb-0">: {{ $pengadaan['jumlah_pengadaan'] }}</p>
					<p class="mb-0">: {{ $pengadaan['jumlah_pengadaan_diterima'] }}</p>
					<p class="mb-1">: {{ $pengadaan['jumlah_pengadaan_ditolak'] }}</p>
				</div>
			</div>
			<table class="table table-bordered" style="width: 100%">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">Nama</th>
			      <th scope="col">Pustaka</th>
			      <th scope="col">Jumlah</th>
			      <th scope="col">Harga</th>
			      <th scope="col">Status </th>
			    </tr>
			  </thead>
			  <tbody>
			  	@php
                    $no = 1;
                @endphp
			  	@foreach($pengadaan['data'] as $pm )
			    <tr>
			      <td>{{ $no++ }} </td>
			      <td>{{ $pm['nama'] }} </td>
			      <td>{{ $pm['buku'] }}</td>
			      <td class="text-center" >{{ $pm['jumlah'] }}</td>
			      <td><?php echo number_format($pm['total_harga']) ?></td>
			      <td>{{ $pm['diterima'] }}</td>
			    </tr>
			   @endforeach
			  </tbody>
			  <tfoot>
			   	<td class="text-center text-bold" colspan="4">Total harga pengadaan diterima :</td>
			   	<td>Rp .<?php echo number_format($pengadaan['total']) ?></td>
			   	<td></td>
			  </tfoot>
			</table>
		</div>
	</div>

</section>

<script type="text/javascript">

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}

	function back() {
        location.reload();
    }

	window.onafterprint = back;

</script>


@endsection
