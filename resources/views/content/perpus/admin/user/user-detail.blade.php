@extends('content.perpus.admin')
@section('content')

<style type="text/css">

</style>

<div class="container">

	<div class="widget-bg mb-5">
		<div class="row p-3 justify-content-between">
			<div class="col-2">
				<img src="{{$user['foto']}}" style="height: 100% !important;">
			</div>
			<div class="col-7">
				<h4>{{$user['nama']}}</h4>
				<p>{{$user['no_induk']}}</p>
			</div>
            <div class="col-2">
                <span class="badge badge-pill badge-{{$user['status_keanggotaan'] != 'aktif' ? 'primary' : 'success' }}">{{$user['status_keanggotaan']}}</span>
                <p>sampai : {{$user['masa_keanggotaan']}} </p>
                <button onclick="perpanjang_user({{$user['id']}})" class="btn btn-info btn-sm">Perpanjang</button>

            </div>
		</div>
        <div class="widget-body clearfix px-3">

            <div class="tabs tabs-bordered">
                <ul class="nav nav-tabs">
                    <li class="nav-item "><a class="nav-link" href="#home-tab-bordered-1" data-toggle="tab" aria-expanded="true">Detail user</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#peminjaman-baru-tab-bordered-1" data-toggle="tab" aria-expanded="true">Peminjaman Baru</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#peminjaman-tab-bordered-1" data-toggle="tab" aria-expanded="true">Peminjaman</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#denda-tab-bordered-1" data-toggle="tab" aria-expanded="true">Denda</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#kartu-tab-bordered-1" data-toggle="tab" aria-expanded="true">Cetak kartu</a>
                    </li>
                </ul>
                <!-- /.nav-tabs -->
                <div class="tab-content">
                    <!-- Detail user -->
                    <div class="tab-pane" id="home-tab-bordered-1">
                        <div class="row">
                        	<div class="col-2">
                        		<h6>Username</h6>
                        		<h6>Email</h6>
                        		<h6>Telepon</h6>
                        		<h6>Jenis Anggota</h6>
                        		<h6>No anggota</h6>
                                <h6>No induk</h6>
                        		<h6>Role</h6>
                        		<h6>Jenis kelamin</h6>
                        		<h6>Alamat</h6>
                        		<h6>Tempat lahir</h6>
                        		<h6>Tanggal lahir</h6>
                        		<h6>Akhir keanggotaan</h6>
                        	</div>
                        	<div class="col-8">
                        		<h6>: {{$user['username']}}</h6>
                                <h6>: {{$user['email']}}</h6>
                        		<h6>: {{$user['telepon']}}</h6>
                        		<h6>: {{$user['tipe_anggota']}}</h6>
                        		<h6>: {{$user['no_anggota']}}</h6>
                        		<h6>: {{$user['no_induk']}}</h6>
                        		<h6>: {{$user['role']}}</h6>
                        		<h6>: {{$user['jenkel']}}</h6>
                        		<h6>: {{$user['alamat']}}</h6>
                        		<h6>: {{$user['tempat_lahir']}}</h6>
                        		<h6>: {{$user['tgl_lahir']}}</h6>
                                <h6>: {{$user['masa_keanggotaan']}}</h6>
                        	</div>
                        </div>
                    </div>

                    <!-- Peminjaman baru -->
                    <div class="tab-pane" id="peminjaman-baru-tab-bordered-1">
                    	<div class="container">

                    		<!-- pencarian item -->
                    		<div class="row flex-row-reverse">
                    			<form id="form-search-item" action="javascript:void(0)" class="d-flex">
								@csrf
								  <input type="text" class="form-control" name="kode" id="kode" placeholder="cari item">
								  <button id="btn-search" type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
								</form>
                    		</div>

                    		<!-- hasil pencairan -->
                    		<ul class="list-unstyled widget-user-list card-body" id="list-pinjaman">

	                        </ul>
                    	</div>
                    </div>

                    <!-- List peminjaman -->
                    <div class="tab-pane" id="peminjaman-tab-bordered-1">
                    	<div class="container bg-white">
                    		<table class="table table-striped table-responsive m-0" id="tabel-peminjaman" style="width: 100%">
							    <thead>
							        <tr>
							            <th>No</td>
							            <th>Buku</th>
							            <th>Kode Buku</th>
							            <th>Keterangan</th>
							            <th>Tanggal pinjam</th>
							            <th>Tanggal kembali</th>
							            <th>Status</th>
							            <th>Denda</th>
							            <th>Aksi</th>
							        </tr>
							    </thead>
							    <tbody>

								</tbody>
							</table>
                    	</div>
                    </div>

                    <!-- List denda -->
                    <div class="tab-pane" id="denda-tab-bordered-1">
                        <div class="container bg-white">
                            <table class="table table-striped table-responsive m-0" id="tabel-denda" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>Buku</th>
                                        <th>Kode Buku</th>
                                        <th>Keterangan</th>
                                        <th>Tanggal pinjam</th>
                                        <th>Tanggal kembali</th>
                                        <th>Hari terlewat</th>
                                        <th>Denda</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- kartu -->
                    <div class="tab-pane" id="kartu-tab-bordered-1">
                    	<div class="row">
                            <div class="col-8" id="printArea">
                                @include('components.perpus.kartu.kartu')
                            </div>
                            <div class="col-4">
                                <button onclick="PrintWindow()" class="btn btn-primary">Print</button>
                            </div>

                    	</div>
                    </div>



                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.tabs -->
        </div>
        <!-- /.widget-body -->
    </div>


<!-- modal -->
<div class="modal" id="modal-pinjam" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pinjam buku</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-add-peminjaman" action="javascript:void(0)" method="post">
		@csrf
		  <input type="hidden" name="id_item" id="id_item" class="form-control">
		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Keterangan</label>
		    <div class="col-sm-10">
		      <input type="text" name="keterangan" class="form-control">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal pinjam</label>
		    <div class="col-sm-10">
		      <input type="date" name="tgl_pinjam" class="form-control">
		    </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="btn-Submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>


</div>

@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')



<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

    function PrintWindow() {
        var divContents = document.getElementById("printArea").innerHTML;
        var a = window.open('', '', 'height=1000, width=1200');
            a.document.write('<html>');
            a.document.write('<body >');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();

    }


    let tabel = $('#tabel-peminjaman');
	let url = "{{ route('peminjaman-user') }}";
 	let column =
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'kode',
	            name: 'kode',
	            width: "35%",
	            orderable :true
	        },
	        {
	            data: 'keterangan',
	            name: 'keterangan',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'tanggal_pinjam',
	            name: 'tanggal_pinjam',
	            width: "20%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_kembali',
	            name: 'tanggal_kembali',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'status',
	            name: 'status',
	            width: "10%",
	            searchable: true
	        },
	        {
	            data: 'denda',
	            name: 'denda',
	            width: "10%",
	            searchable: true
	        },
	        {
	            data: 'aksi',
	            name: 'aksi',
	            width: "10%"
	        }
	    ];

    let tabel_denda = $('#tabel-denda');
    let url_denda = "{{ route('denda-user') }}";
    let column_denda =
        [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: "5%"
            },
            {
                data: 'buku',
                name: 'buku',
                width: "15%",
                orderable :true
            },
            {
                data: 'kode_buku',
                name: 'kode_buku',
                width: "10%",
                orderable :true
            },
            {
                data: 'keterangan',
                name: 'keterangan',
                width: "10%",
                orderable :true
            },
            {
                data: 'tanggal_pinjam',
                name: 'tanggal_pinjam',
                width: "20%",
                orderable :true,
                visible: true
            },
            {
                data: 'tanggal_dikembalikan',
                name: 'tanggal_dikembalikan',
                width: "20%",
                orderable :true,
                searchable: true
            },
            {
                data: 'hari_terlewat',
                name: 'hari_terlewat',
                width: "10%",
                searchable: true
            },
            {
                data: 'denda',
                name: 'denda',
                width: "10%",
                searchable: true
            }
        ];

    MakeTable(tabel,url,column);
    MakeTable(tabel_denda,url_denda,column_denda);

    function returned(id){
    	$.ajax({
            type : "post",
            url  : "{{route('peminjaman-kembali')}}",
            data : {
            	'id' : id
            },
            beforeSend : ()=>{
            	$('#btnReturn-'+ id).html(`
            		'<i class="fa fa-spin fa-spinner"></i> Loading'
            	`);
            },
            success : (data) =>{
            	noti(data.icon, data.message);
            	$('#tabel-peminjaman').DataTable().ajax.reload();
            	$('#btnReturn-'+ id).html(`
            		Kembali
            	`);
            },
            error: function(data) {
            	noti(data.icon, data.message);
            }
        });
    }

    function perpanjang_user(id){
        $.ajax({
            type : "get",
            url  : "{{route('user-perpus-extends')}}",
            data : {
                'id' : id
            },
            success : (data) =>{
                noti(data.icon, data.message);
                location.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);
            }
        });
    }

    function perpanjang(id){
        $.ajax({
            type : "get",
            url  : "{{route('peminjaman-extends')}}",
            data : {
                'id' : id
            },
            beforeSend : ()=>{
            	$('#btnExtend-'+ id).html(`
            		'<i class="fa fa-spin fa-spinner"></i>'
            	`);
            },
            success : (data) =>{
                noti(data.icon, data.message);
                $('#tabel-peminjaman').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('#btnExtend-'+ id).html(`
            		'perpanjang'
            	`);
            }
        });
    }

    $('#form-search-item').on('submit',function(){
    	let items = '';

    	function display(item,index){

    		items += `
    			<li class="media">
                    <div class="d-flex mr-3">
                        <img src="${item.foto}" class="" alt="" style="height: 100px">
                    </div>
                    <div class="media-body">
                    	<button onclick="modalKembali(${item.id})" class="btn btn-outline-default" id="btn-pinjam-${item.id}">Pinjam</button>
                        <h5 class="media-heading"><a href="#">${item.buku}</a> <small>${item.kode}</small></h5>
                    </div>
                </li>
    		`;
    	}

    	function filterStatus(item,index){
            return item.status_dipinjam == 'tersedia';
        }

        let kode = $('#kode').val();

        $.ajax({
            type : "post",
            url  : "{{route('item-by-kode')}}",
            data : {
                'kode' : kode
            },
            beforeSend: function() {
                $('#btn-search').html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
            	if(data.data != null){
            		data.data.filter(filterStatus).forEach(display);
            		$('#list-pinjaman').html(items);
            	}else{
            		$('#list-pinjaman').html(`
            			<p>Item tidak tersedia<p>
            		`);
            	}

            	$('#btn-search').html(
                    '<i class="fas fa-search"></i>');
            },
            error : (data)=>{
                $('#btn-search').html(
                    '<i class="fas fa-search"></i>');
            }
    	});
	});

	function modalKembali(id){
		$('#modal-pinjam').modal('show');
		$('#id_item').val(id);
	}

	$('#form-add-peminjaman').on('submit',function(){
        let datas = $(this).serialize();

        let id_item = datas.id_item;
        console.log(id_item);

        $.ajax({
            type : "post",
            url  : "{{route('peminjaman-add')}}",
            data : datas,
            beforeSend: function() {
                $('#btn-Submit').html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#form-add-peminjaman').trigger("reset");
                $('#btn-Submit').html(
                    'Simpan');
                $('#btn-pinjam-'+ id_item).attr("disabled", true);
                $('#modal-pinjam').modal('hide');
                $('#tabel-peminjaman').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('#btn-Submit').html(
                    'Simpan');
                $('#btn-pinjam-'+id_item).html('dipinjam');


            }
        });
    });

    $('#modal-pinjam').on('hidden.bs.modal', function () {
	    $(this).find('form').trigger('reset');
	});

</script>

@endsection

