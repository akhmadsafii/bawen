@extends('template.template_default.app')
@section('content')
    <style type="text/css">
        h6,
        p {
            margin-top: 0.25rem !important;
            margin-bottom: 0.25rem !important;
        }

    </style>

    <section class="container widget-bg">
        @if (Session::has('message'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p class="mb-1">{{ Session::get('message') }}</p>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <h3 class="box-title">Daftar Admin</h3>
            </div>
            <div class="col-md-6">
                <button type="button" class="btn btn-info btn-sm float-right" onclick="modalCreate()">
                    <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
                </button>
            </div>
        </div>
        <hr>
        <table class="table table-striped table-responsive" data-toggle="datatables" id="tabel-admin" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Nip</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </section>

    @include('includes.program-perpus.datatable')
    @include('includes.program-perpus.notify')
    @include('components.perpus.modal.admin-modal')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': `Bearer {{ Session::get('token') }}`
            }
        });

        let tabel = $('#tabel-admin');
        let url = "{{ route('admin-perpus-all') }}";
        let column = [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: "5%"
            },
            {
                data: 'nama',
                name: 'nama',
                width: "20%",
                orderable: true,
                searchable: true
            },
            {
                data: 'nip',
                name: 'nip',
                width: "20%",
                orderable: true,
                searchable: true
            },
            {
                data: 'username',
                name: 'username',
                width: "10%",
                orderable: true,
                searchable: true
            },
            {
                data: 'email',
                name: 'email',
                width: "10%",
                orderable: true,
                searchable: true
            },
            {
                data: 'aksi',
                name: 'aksi',
                width: "10%"
            },
        ];

        let buttons = [{
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                exportOptions: {
                    columns: [0, 1, 2]
                },
                title: 'Daftar admin'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                messageTop: null,
                exportOptions: {
                    columns: [0, 1, 2]
                },
                title: 'Daftar admin'
            },

            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                messageBottom: null,
                exportOptions: {
                    columns: [0, 1, 2]
                },
                customize: function(doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                title: 'Daftar admin'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload(null, false);
                }
            }
        ];

        MakeFullTable(tabel, url, column);

        function modalCreate() {
            $('#modal-store-master').modal('show');
        }

        $('#modal-store-master').on('hidden.bs.modal', function() {
            $(this).find('form').trigger('reset');
        });

        function edit_button(id) {
            $.ajax({
                tipe: "post",
                url: "{{ route('admin-perpus-detail') }}",
                data: {
                    'id': id
                },
                beforeSend: () => {
                    $('.tabledit-edit-button-' + id).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: (data) => {
                    console.log(data);
                    $('#password').hide();
                    $('#id').val(data.data.id);
                    $('#nip').val(data.data.nip);
                    $('#nama').val(data.data.nama);
                    $('#username').val(data.data.username);
                    $('#alamat').val(data.data.alamat);
                    $('#email').val(data.data.email);
                    $('#telepon').val(data.data.telepon);
                    $('#no_induk').val(data.data.no_induk);
                    $('#tempat_lahir').val(data.data.tempat_lahir);
                    $('#tgl_lahir').val(data.data.tgl_lahir);
                    $('.btn-action').html('Update');
                    $('#modal-store-master').modal('show');
                    $('.tabledit-edit-button-' + id).html('<i class="fas fa-pencil-alt"><i>');
                }


            });
        }

        function detail_modal(id) {
            $.ajax({
                tipe: "post",
                url: "{{ route('admin-perpus-detail') }}",
                data: {
                    'id': id
                },
                beforeSend: () => {
                    $('.tabledit-detail-button-' + id).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: (data) => {
                    console.log(data.data);
                    $('#modal-detail-admin').modal('show');
                    $('#modal-detail-body').html(`
					<div class="contact-details-profile pd-lr-30">
		                <div class="row">
		                    <div class="col-md-4 mt-1">
		                        <img src="${data.data.foto}" class="m-1" width="200px">
		                    </div>
		                    <div class="col-md-6 mt-1">
		                        <h6 class="text-dark text-uppercase">${data.data.nama}</h6>
		                        <p class="mr-t-0">${data.data.nip}</p>
		                    </div>
		                </div>
		                <hr class="mt-1">
		                <div class="row">
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Email</p>
		                        <h6 class="text-dark text-uppercase">${data.data.email}</h6>
		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Telepon</p>
		                        <h6 class="text-dark text-uppercase">${data.data.telepon}</h6>

		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Alamat</p>
		                        <h6 class="text-dark text-uppercase">${data.data.alamat}</h6>

		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Username</p>
		                        <h6 class="text-dark text-uppercase">${data.data.username}</h6>
		                    </div>
		                </div>
		                <hr class="mt-1">
		                <div class="row">
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Jenis Kelamin</p>
		                        <h6 class="text-dark text-uppercase">${data.data.jenkel}</h6>
		                    </div>
		                </div>
		                <hr class="border-0 mr-tb-50">
		            </div>
				`);
                    $('.tabledit-detail-button-' + id).html('<i class="fas fa-info-circle"><i>');

                }
            });
        }

        function submit_delete(id) {
            $.ajax({
                type: "post",
                url: "{{ route('admin-perpus-delete') }}",
                data: {
                    'id': id
                },
                beforeSend: function() {
                    $('.tabledit-delete-button-' + id).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: (data) => {
                    console.log(data);
                    noti(data.icon, data.message);
                    $('#tabel-admin').DataTable().ajax.reload();
                    $('.tabledit-delete-button-' + id).html('<i class="fas fa-trash-alt"><i>');
                },
                error: function(data) {
                    noti(data.icon, data.message);
                    $('.tabledit-delete-button-' + id).html('<i class="fas fa-trash-alt"><i>');
                }
            });

        }
    </script>
@endsection
