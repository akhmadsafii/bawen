@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}

    .caret{
        display: none !important;
    }

    #dendaBox{
        display: hidden;
    }

</style>

<section class="container-fluid">
	<div class="container d-flex flex-row justify-content-between align-items-center my-2">
		<h4 class="mb-1 mt-0">Cari
			<br>
			<small class="subtitle-section">peminjam</small>
		</h4>

		<form id="form-search-kembali" action="javascript:void(0)" method="post">
		@csrf
		  <select class="selectpicker" data-live-search="true" name="id_peminjam">
	      	@foreach($users as $user)
			  <option value="{{$user['id']}}">{{$user['no_induk']}}</option>
			@endforeach
	  	  </select>
		  <button id="btn-search" type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
		</form>
	
	</div>

	<div class="container p-3 d-flex flex-row">
        <div class="col-3 bg-white mr-2">
            <div class="widget-bg">
                <div class="widget-body clearfix" id="profileBox">
                    
                </div>
            </div>
        </div>

        <div class="col-9 bg-white p-3">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Buku</th>
                  <th scope="col">Kode</th>
                  <th scope="col">Tanggal pinjam</th>
                  <th scope="col">Tanggal kembali</th>
                  <th scope="col">Status</th>
                  <th scope="col">Kembali</th>
                </tr>
              </thead>
              <tbody id="tabel-body">
                
              </tbody>
            </table>
        </div>
		
	</div>

</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>


<script type="text/javascript">
	
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    }

    $('#form-search-kembali').on('submit',function(){
        render();
    });

    function render(){
        let id = $('.selectpicker').val();
        let data = '';
        let datas = '';
        let profile = '';
        let status;
        let now = new Date().toISOString().slice(0, 10);
        console.log(id);

        function myFunction(item,index){
            
            if( item.tanggal_kembali < now){
                status = '<span class="badge badge-danger">Terlambat</span>';
                console.log('terlambat');
            }else{
                status = '<span class="badge badge-success">Berjalan</span>';
                console.log('tidak terlambat');
            }

            data += `
                <tr>
                    <td>${index + 1}</td>
                    <td>${item.buku}</td>
                    <td>${item.kode_item}</td>
                    <td>${item.tanggal_pinjam}</td>
                    <td>${item.tanggal_kembali}</td>
                    <td>${status}</td>
                    <td><button onclick="returned(${item.id})" type="button" id="btn-Back-${item.id}" class="btn btn-info btn-sm">Kembali</button>
                    </td>
                </tr>
            `;

            $('#tabel-body').html(data);
        }

        function filterStatus(item,index){
            return item.status_dipinjam != 'kembali';
        }

        $.ajax({
            type : "post",
            url  : "{{route('peminjaman-user')}}",
            data : {
                'id' : id
            },  
            beforeSend: function() {
                $('#btn-search').html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (datas) =>{
                console.log(datas);
                data = '';
                profile = `
                    <h5 class="mb-1 mt-0">${datas.profile.nama}</h5>
                    <h5 class="mb-0 mt-0">${datas.profile.no_induk}</h5>
                    <h6 class="mb-0">Denda</h6>
                    <h6 class="mt-1"><i class="fas fa-coins"></i>
                        Rp. 
                        <span class="counter">${datas.total}</span>
                    </h6>
                `;

                $('#profileBox').html(profile);

                datas = datas.data;

                if(datas.length != 0){
                    datas.filter(filterStatus).forEach(myFunction);
                }else{
                    $('#tabel-body').html('');
                }    

                $('#btn-search').html(
                    '<i class="fas fa-search"></i>');
            },
            error: function(data) {
            
            } 
        }); 
    }

    function returned(id){
    	$.ajax({
            type : "post",
            url  : "{{route('peminjaman-kembali')}}",
            data : {
            	'id' : id
            },  
            success : (data) =>{
            	noti(data.icon, data.message);
            	$("#btn-Back-" + id).html('');
                render();
            },
            error: function(data) {
            	noti(data.icon, data.message);
            } 
        }); 
    }



</script>




@endsection