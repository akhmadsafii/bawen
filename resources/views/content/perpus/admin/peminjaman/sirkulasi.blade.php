@extends('content.perpus.admin')
@section('content')

<div class="container bg-white p-4">
	<h4>Sirkulasi</h4>
	<p>Silakan masukkan nama anggota perpustakaan</p>
	<div id="search-section" class="col-4">
		<div id="search-book">
			<form id="form-search" action="javascript:void(0)" class="row">
			  <input type="text" class="form-control" id="name" name="nama" style="width: 80%">
			  <button id="btn-search" class="btn btn-primary" type="submit" style="width: 20%"><i class="fas fa-search"></i></button>
			</form>
		</div>
	</div>

    <ul class="list-unstyled widget-user-list card-body bg-white mt-3" id="user-list">

    </ul>
</div>

@include('includes.program-perpus.notify')

<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#form-search').on('submit',function(){
        let data = $(this).serialize();
        let users = '';
        let not_found = '<p>User tidak tersedia</p>';
        console.log(data);

        function display(item,index){
    		users += `
                <li class="media">
                    <div class="d-flex mr-3">
                        <a href="user/details/${item.id}" class="user--online thumb-xs">
                            <img src="${item.foto}" class="rounded-circle" alt="">
                        </a>
                    </div>
                    <div class="media-body"><a href="user/details/${item.id}" class="btn btn-outline-default">Detail</a>
                        <h5 class="media-heading"><a href="user/details/${item.id}">${item.nama}</a> <small>${item.no_anggota}</small></h5>
                    </div>
                </li>
    		`;
    	}

        $.ajax({
            type : "post",
            url  : "{{route('user-perpus-by-nama')}}",
            data : data,
            beforeSend: function() {
                $('#btn-search').html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{

                if(data.data.lenght != 0){
            		data.data.forEach(display);
            		$('#user-list').html(users);
            	}else{
            		$('#user-list').html(not_found);
            	}

                $('#form-search').trigger("reset");
                $('#btn-search').html(
                    '<i class="fas fa-search"></i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('#btn-Submit').html(
                    '<i class="fas fa-search"></i>');
            }
        });
    });

</script>


@endsection
