@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 250px;
	}

	#frame{
		height: 100px !important;
	}

	.caret{
		display: none !important;
	}
</style>

<section class="container">
	@if (Session::has('message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>    
      </div>
    @endif

	<h4 class="mb-1">Daftar Pustaka Digital
		<br>
		<small class="subtitle-section">daftar semua pustaka digital</small>
	</h4>

	<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
     </button>

	<table class="table table-striped table-responsive tabel-buku">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Judul</th>
	            <th>Kode</th>
	            <th>Lampiran</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>  
	                    
		</tbody>
	</table>

	@include('includes.program-perpus.notify')
	@include('components.perpus.modal.buku-detail')
	@include('components.perpus.modal.pustaka-create')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

</section>

@include('includes.program-perpus.datatable')

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('.tabel-buku');
	let url = "{{ route('pusdig-admin') }}";
	let column = [
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
	    {
            data: 'judul',
            name: 'judul',
            width: "15%",
            orderable :true
        },
        {
            data: 'kode',
            name: 'kode',
            width: "15%",
            orderable :true
        },
        {
            data: 'lampiran',
            name: 'lampiran',
            width: "15%"
        },
        {
            data: 'aksi',
            name: 'aksi',
            width: "15%"
        },
	];

	MakeTable(tabel,url,column);

	function modalDetailBuku(id){
		let page;
		let penulis,topik,topik_label;
		$.ajax({
			type : 'POST',
			url  : "{{ route('ajax-detail-pusdig') }}",
			data : {
				id : id
			},
			dataType: 'json',
			beforeSend : ()=>{
				$('.tabledit-detail-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : function(data){
				$('.tabledit-detail-button-'+id).html(
                    '<i class="fas fa-info-circle"></i>');

				penulis = '';
				topik = '';

				if(data.data.penulis.length != 0){
					for(let i = 0 ; i < data.data.penulis.length ; i++){
						penulis += `${data.data.penulis[i].nama} ,`;
					}				
				}

				if(data.data.topik.length != 0){
					for(let i = 0 ; i < data.data.topik.length ; i++){
						topik += `${data.data.topik[i].nama}, `;
					}			
				}

				page = `
					<div class="row">
						<div class="col-4">
							<img src="${data.data.foto}">
							<h5>${data.data.judul} - <strong>${data.data.kode}</strong></h5>
							<a href="${data.data.lampiran}">Lampiran</a>
						</div>
						<div class="col-8">
							<h6>Deskripsi : ${data.data.deskripsi}</h6>
							<p>Download : ${data.data.download} kali</p>
							<div class="row mt-2">
	                        	<div class="col-4">

	                        		<p>ISBN</p>
	                                <p>Koleksi</p>
	                                <p>Topik</p>
	                        		<p>Penerbit</p>
	                        		<p>Tahun terbit</p>
	                        		<p>Pengarang</p>
	                        		<p>Bahasa</p>
	                        		<p>Halaman</p>
	                        		<p>Jenis</p>
	                        	</div>
	                        	<div class="col-8">
	                        		<p>: ${data.data.ISBN}</p>
	                                <p>: ${data.data.koleksi}</p>
	                                <p>: ${topik}</p>
	                        		<p>: ${data.data.penerbit}</p>
	                        		<p>: ${data.data.tahun_terbit}</p>
	                        		<p>: ${penulis}</p>
	                        		<p>: ${data.data.bahasa}</p>
	                        		<p>: ${data.data.halaman}</p>
	                        		<p>: ${data.data.jenis}</p>
	                        	</div>
	                        </div>
                        </div>
						
					</div>
				`;
				$('#body-detail-buku').html(page);
				$('#modal-detail-buku').modal('show');
			},
		});
	}

	function modalCreate(){
		$('#modal-edit-buku').modal('show');
	}

	function modalEditBuku(id){
		$.ajax({
			type : 'POST',
			url  : "{{ route('ajax-detail-pusdig') }}",
			data : {
				id : id
			},
			dataType: 'json',
			beforeSend : ()=>{
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : function(data){
				$('#ids').val(id);
				$('#judul').val(data.data.judul);
				$('#kode').val(data.data.kode);
				$('#deskripsi').val(data.data.deskripsi);
				$('#id_penerbit').val(data.data.id_penerbit);
				$('#id_bahasa').val(data.data.id_bahasa);
				$('#id_koleksi').val(data.data.id_koleksi);
				$('#id_jenis').val(data.data.id_jenis);
				$('#isbn').val(data.data.ISBN);
				$('#tahun_terbit').val(data.data.tahun_terbit);
				$('#halaman').val(data.data.halaman);
				$('#modal-edit-buku').modal('show');
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fas fa-pencil-alt"></i>');
			}
		});
	}

	$('#modal-edit-buku').on('hidden.bs.modal', function () {
	    $(this).find('form').trigger('reset');
	    $("#frame").attr("src",null);
	});

	function preview() {
	    frame.src=URL.createObjectURL(event.target.files[0]);
	}

</script>


@endsection