@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 250px;
	}
</style>

<section class="container">

	<h4 class="mb-1">Daftar Agen
		<br>
		<small class="subtitle-section">daftar semua agen</small>
	</h4>

	<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
     </button>

	<table class="table table-striped table-responsive" id="tabel-agen" width="100%">
	    <thead>
	        <tr>
	            <th data-identifier>No</th>
	            <th data-editable>Nama</th>
	            <th data-editable>Alamat</th>
              <th data-editable>Email</th>
              <th data-editable>Telepon</th>
              <th data-editable>Kontak</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>

		</tbody>
	</table>

</section>

<!-- modal create -->
<div class="modal fade bd-example-modal-lg" id="modal-store-master" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Tambah</h5>
        </div>

        <div class="modal-body">
        <form id="form-add-master" action="javascript:void(0)" method="post">
        @csrf

        <div class="form-group">
            <label for="exampleInputPassword1">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" id="exampleInputPassword1" placeholder="nama">
          </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-8">
              <label for="inputEmail4">Alamat</label>
              <input type="text" class="form-control" name="alamat" id="alamat" placeholder="alamat">
            </div>

            <div class="form-group col-md-4">
              <label for="inputPassword4">Email</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="email">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">kontak</label>
              <input type="text" class="form-control" name="kontak" id="kontak" placeholder="kontak">
            </div>

            <div class="form-group col-md-6 ">
              <label for="inputPassword4">Telepon</label>
              <input type="number" class="form-control" name="telepon" id="telepon" placeholder="telepom">
            </div>
        </div>


      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Tambah</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- end modal create -->

@include('includes.program-perpus.notify')
@include('includes.program-perpus.datatable')

<script src="{{ asset('asset/js/jquery.tabledit.js') }}"></script>

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('#tabel-agen');
	let url = "{{ route('perpus-agen') }}";
	let column = [
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
    {
          data: 'nama',
          name: 'nama',
          width: "15%",
          orderable :true,
          searchable: true
    },
    {
          data: 'alamat',
          name: 'alamat',
          width: "30%",
          orderable :true,
          searchable: true
    },
    {
          data: 'email',
          name: 'email',
          width: "15%",
          orderable :true,
          searchable: true
    },
    {
          data: 'telepon',
          name: 'telepon',
          width: "15%",
          orderable :true,
          searchable: true
    },
    {
          data: 'kontak',
          name: 'kontak',
          width: "15%",
          orderable :true,
          searchable: true
    },
    {
        data: 'aksi',
        name: 'aksi',
        width: "5%"
    },
	];

	let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar agen'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar agen'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar agen'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

	MakeFullTable(tabel,url,column);

	function edit_button(id){

		if($('.input-nama-'+ id).css('display') == 'none'){
			$('.input-alamat-'+ id).css("display", "block");
			$('.show-alamat-'+ id).css("display", "none");
      $('.input-kontak-'+ id).css("display", "block");
      $('.show-kontak-'+ id).css("display", "none");
      $('.input-email-'+ id).css("display", "block");
      $('.show-email-'+ id).css("display", "none");
      $('.input-telepon-'+ id).css("display", "block");
      $('.show-telepon-'+ id).css("display", "none");
      $('.input-alamat-'+ id).css("display", "block");
      $('.show-alamat-'+ id).css("display", "none");
			$('.input-nama-'+ id).css("display", "block");
			$('.show-nama-'+ id).css("display", "none");
			$('.tabledit-save-button-'+ id).css("display", "block");
		}else{
			$('.input-alamat-'+ id).css("display", "none");
			$('.show-alamat-'+ id).css("display", "block");
			$('.input-nama-'+ id).css("display", "none");
			$('.show-nama-'+ id).css("display", "block");
      $('.input-kontak-'+ id).css("display", "none");
      $('.show-kontak-'+ id).css("display", "block");
      $('.input-email-'+ id).css("display", "none");
      $('.show-email-'+ id).css("display", "block");
      $('.input-telepon-'+ id).css("display", "none");
      $('.show-telepon-'+ id).css("display", "block");
			$('.tabledit-save-button-'+ id).css("display", "none");
		}
	}

	function submit_edit(id){

		let alamat = $('.input-alamat-'+ id).val();
    let email = $('.input-email-'+ id).val();
    let kontak = $('.input-kontak-'+ id).val();
    let telepon = $('.input-telepon-'+ id).val();
		let nama = $('.input-nama-'+ id).val();

		$.ajax({
            type : "post",
            url  : "{{route('perpus-agen-update')}}",
            data : {
            	'id' : id,
            	'nama' : nama,
            	'alamat' : alamat,
              'kontak' : kontak,
              'email' : email,
              'telepon' : telepon
            },
            beforeSend: function() {
                $('.tabledit-edit-button-'+ id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#tabel-agen').DataTable().ajax.reload();
                $('.tabledit-edit-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('.tabledit-edit-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
            }
        });

	}

	function submit_delete(id){
		$.ajax({
            type : "post",
            url  : "{{route('perpus-agen-delete')}}",
            data : {
            	'id' : id
            },
            beforeSend: function() {
                $('.tabledit-delete-button-'+ id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#tabel-agen').DataTable().ajax.reload();
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            }
        });

	}

	function modalCreate(){
		$('#modal-store-master').modal('show');
	}

	$('#form-add-master').on('submit',function(){
		let data = $(this).serialize();

        $.ajax({
            type : "post",
            url  : "{{route('perpus-agen-store')}}",
            data : data,
            success : (data) =>{
                noti(data.icon, data.message);
                $('#form-add-master').trigger("reset");
                $('#modal-store-master').modal('hide');
                $('#tabel-agen').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);

            }
        });

	});




</script>


@endsection
