@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

<div class="col-sm-12 col-md-12 widget-holder">
    <div class="widget-bg">
        <div class="widget-body clearfix">
            <div class="row mb-2">
                <div class="col-md-12 col-12">
                    <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                </div>
            </div>
            <div class="container">
                <div class="page-title-right">
                    <button data-toggle="modal" data-target=".bs-modal-lg-info" class="btn btn-sm btn-outline-success mb-2">Tambah</button>
                    <button type="button" class="btn btn-sm btn-outline-secondary mb-2">Import</button>
                    
                </div>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Kategori lomba</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Tahun</th>
                      <th scope="col">Nama peserta</th>
                      <th scope="col">Kelas</th>
                      <th scope="col">Peringkat kabupaten</th>
                      <th scope="col">Peringkat provinsi</th>
                      <th scope="col">Peringkat nasional</th>
                      <th scope="col">Keterangan</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="table-body">
                    @foreach($datas as $data)
                    <tr>
                      <td></td>
                      <td>{{$data['kategori_lomba']}}</td>
                      <td>{{$data['nama_lomba']}}</td>
                      <td>{{$data['tahun']}}</td>
                      <td>{{$data['nama_peserta']}}</td>
                      <td>{{$data['kelas']}}</td>
                      <td>{{$data['peringkat_kab']}}</td>
                      <td>{{$data['peringkat_prov']}}</td>
                      <td>{{$data['peringkat_nas']}}</td>
                      <td>{{$data['keterangan']}}</td>
                      <td>
                          <button type="button" onclick="modalUpdate({{$data['id']}})" class="btn btn-primary btn-sm">U</button>
                          <button type="button" class="btn btn-secondary btn-sm">D</button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- /.modal -->
    <div class="modal modal-info fade bs-modal-lg-info" id="modalPrestasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah prestasi siswa</h5>
                </div>
                <div class="modal-body">
                <form action="javascript:void(0)" id="create-prestasi">
                    @csrf
                    <input class="form-control" id="id" name="id" type="text">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="9">Kategori lomba</label>
                        <div class="col-md-9">
                            <select id="id_lomba" name="id_lomba" class="form-control">
                            @foreach($id_lomba as $lomba)
                                <option value="{{$lomba['id']}}">{{$lomba['nama']}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nama lomba</label>
                        <div class="col-md-9">
                            <input class="form-control" id="nama_lomba" name="nama_lomba" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Nama siswa</label>
                        <div class="col-md-9">
                            <input class="form-control" id="nama_siswa" name="nama_siswa" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                        <div class="col-md-9">
                            <input class="form-control" id="keterangan" name="keterangan" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Tahun</label>
                        <div class="col-md-9">
                            <input class="form-control" id="tahun" name="tahun" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l0">Kelas</label>
                        <div class="col-md-9">
                            <input class="form-control" id="kelas" name="kelas" type="text">
                        </div>
                    </div>
                    <h5>Peringkat</h5>
                    <div class="form-group row justify-content-center">
                        <div class="form-group row col-md-4">
                          <label for="inputCity">Kabupaten</label>
                          <input type="number" name="kab" class="form-control" id="kab">
                        </div>
                        <div class="form-group row col-md-4 mx-3">
                          <label for="inputState">Provinsi</label>
                          <input type="number" name="prov" class="form-control" id="prov">
                        </div>
                        <div class="form-group row col-md-4">
                          <label for="inputZip">Nasional</label>
                          <input type="number" name="nas" class="form-control" id="nas">
                        </div>
                    </div>

                
                <button type="submit" class="btn btn-danger">Simpan</button>
                </form>
                
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    };

    $('#create-prestasi').on('submit', function(event) {
        data = $(this).serialize();

        $.ajax({
            type : "post",
            url  : "{{ route('create-prestasi-siswa') }}",
            data : data,
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $("#table-body").load(" #table-body");
                $("#modalPrestasi").modal('hide');
            },
            error: function(data) {
                noti(data.icon, data.message);   
            } 
        }); 
    });

    function modalUpdate(id){
        let datas;
        $.ajax({
            type : "post",
            url  : "{{ route('detail-prestasi-siswa') }}",
            data : {
                'id' : id
            },
            success : (data) =>{
                datas = data.data;
                console.log(datas)
                $('#id').val(datas.id);
                $('#id_lomba').val(datas.id_kategori_lomba);
                $('#nama_lomba').val(datas.nama_lomba);
                $('#nama_siswa').val(datas.nama_peserta);
                $('#keterangan').val(datas.keterangan);
                $('#tahun').val(datas.tahun);
                $('#kelas').val(datas.kelas);
                $('#kab').val(datas.peringkat_kab);
                $('#prov').val(datas.peringkat_prov);
                $('#nas').val(datas.peringkat_nas);
                $('#modalPrestasi').modal('show');
            },
            error: function(data) {
                noti(data.icon, data.message);   
            } 
        }); 
    }

    $('#modalPrestasi').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });



</script>





@endsection