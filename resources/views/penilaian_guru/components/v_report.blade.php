@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Report </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap </p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="form-inline">
                            <div class="form-group mr-2">
                                <div class="input-group">
                                    <select class="form-control mt-1 " id="periode_list" name="periode_list">
                                        <option value="" disabled="disabled" selected="selected">Pilih Periode </option>
                                        @foreach ($tahunlist as $tahun)
                                            <option value="{{ $tahun }}">{{ $tahun }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_pengaturan" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>Penilai</th>
                                        <th>NIP Penilai</th>
                                        <th>Periode</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>Penilai</th>
                                        <th>NIP Penilai</th>
                                        <th>Periode</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_reportpkg') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'guru',
                            name: 'guru'
                        },
                        {
                            data: 'nip',
                            name: 'nip'
                        },
                        {
                            data: 'penilai',
                            name: 'penilai'
                        },
                        {
                            data: 'nip_penilai',
                            name: 'nip_penilai'
                        },
                        {
                            data: 'periode',
                            name: 'periode'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };
                table_adminx = $('#table_pengaturan').dataTable(config);
                //filter
                $('body').on('change', 'select[name="periode_list"]', function() {
                    var tahun = $(this).val();
                    var url = "{{ route('ajax_periode_report', ['tahun' => ':tahun']) }}";
                    url = url.replace(':tahun', tahun);

                    config = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'guru',
                                name: 'guru'
                            },
                            {
                                data: 'nip',
                                name: 'nip'
                            },
                            {
                                data: 'penilai',
                                name: 'penilai'
                            },
                            {
                                data: 'nip_penilai',
                                name: 'nip_penilai'
                            },
                            {
                                data: 'periode',
                                name: 'periode'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_adminx = $('#table_pengaturan').dataTable(config);

                });
            });
        })(jQuery, window);
    </script>
@endsection
