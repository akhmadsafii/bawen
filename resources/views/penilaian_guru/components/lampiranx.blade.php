@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap Survei Penilaian {{ $nama_guru }} - {{ $nip_guru }}  </p>
            <p> , Penilai : {{ $nama_penilai }} - {{ $nip_penilai }}</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header clearfix ">
                        <p class="text-info">Silakan Gunakan Fitur Print Untuk mulai mencetak Rekap.
                            <button type="button" id="printdata"><i class="fa fa-print"></i></button>
                        </p>
                    </div>
                    <div class="widget-body clearfix">
                        <center>
                            <div class="table-responsive col-md-12">
                                <div class="btn-group" style="flex-wrap: wrap;">
                                    <a href="{{ route('print_lampiran_pkg', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Cover</a>
                                    @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                        @if ($kompetensi_penilaian['id'] == $id_kompetensi)
                                            <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                                class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 btn-info"
                                                data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                        @else
                                            <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                                class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2"
                                                data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                        @endif
                                    @endforeach
                                    <a href="{{ route('print_lampiran1b',['id_guru'=>$id_guru,'id_penilai'=>$id_penilai,'periode'=>$periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 B </a>
                                    <a href="{{ route('print_lampiran1c', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 C </a>
                                    <a href="{{ route('print_lampiran1d', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 D </a>
                                </div>
                            </div>
                        </center>
                        <br>
                        <div id="printable">
                            <p><b>{{ $nama_kompetensi }}</b><br>
                                <small>{{ $keterangan_kompetensi }}</small>
                            </p>
                            <center>
                                <style>
                                    .disabledinput {
                                        opacity: 0.4;
                                        filter: alpha(opacity=40);
                                        background-color: blue;
                                        cursor: not-allowed;
                                        pointer-events: none;
                                    }

                                </style>
                                <table class="table table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10%">No</th>
                                            <th width="80%">Indikator</th>
                                            <th width="10%">Nilai Skor </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total_skor = 0;
                                            $total_indicator = 0;
                                            $skor_maksimal   = 0;
                                            $Persentase      = 0;
                                            $nilai           = 0;
                                        @endphp
                                        @foreach ($list_indikator as $indikator)
                                            @php
                                                $total_skor += intval($indikator['nilai']);
                                                $total_indicator = count($list_indikator);
                                                $skor_maksimal   = intval($total_indicator)*2;
                                                $Persentase      = round((intval($total_skor)/$skor_maksimal) *100,2);
                                                if(0 < $Persentase  && $Persentase <= 25){
                                                    $nilai  = 1;
                                                }else if(25 < $Persentase && $Persentase <= 50){
                                                    $nilai  = 2;
                                                }else if(50 < $Persentase && $Persentase <= 75){
                                                    $nilai  = 3;
                                                }else if(75 < $Persentase && $Persentase <= 100){
                                                    $nilai  = 4;
                                                }
                                            @endphp
                                            <tr>
                                                <td width="10%">{{ $loop->iteration }}</td>
                                                <td width="80%">{{ $indikator['nama'] }} </td>
                                                <td width="10%">{{ intval($indikator['nilai']) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">Total skor untuk {{ $nama_kompetensi }}  </td>
                                            <td>{{ $total_skor }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Skor maksimum {{ $nama_kompetensi }} = jumlah indikator × 2  </td>
                                            <td>{{ $skor_maksimal }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Persentase = (total skor/ {{ $skor_maksimal }} ) × 100%  </td>
                                            <td>{{ $Persentase }} % </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"> Nilai (0% < X ≤ 25% = 1; 25% < X ≤ 50% = 2; 50% < X ≤ 75% = 3; 75% < X ≤ 100% = 4) </td>
                                            <td>{{ $nilai }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $('body').on('click', '#printdata', function() {
                    var printContents = document.getElementById('printable').innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    //window.stateX();
                    document.body.innerHTML = originalContents
                });

                window.stateX = function CheckWindowState() {
                    if (document.readyState == "complete") {
                        window.location.reload();
                    } else {
                        setTimeout("CheckWindowState()", 1000)
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
