@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Guru / Penilai </p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Periode</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshow" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Informasi </h5>
                </div>
                <div class="modal-body">
                    <table class="table mb-0">
                        <legend class="text-muted periode"></legend>
                        <tbody>
                            <tr>
                                <td>Photo Guru</td>
                                <td>:</td>
                                <td class="text-muted photo_guru"></td>
                                <td>Nama Guru</td>
                                <td>:</td>
                                <td class="text-muted nama_guru"></td>
                                <td>NIP Guru</td>
                                <td>:</td>
                                <td class="text-muted nip_guru"></td>
                            </tr>
                            <tr>
                                <td>Photo Penilai</td>
                                <td>:</td>
                                <td class="text-muted photo_penilai"></td>
                                <td>Nama Penilai</td>
                                <td>:</td>
                                <td class="text-muted nama_penilai"></td>
                                <td>NIP Penilai</td>
                                <td>:</td>
                                <td class="text-muted nip_penilai"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Setting Guru / Penilai </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_setting" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr style="display:none;">
                                    <td>Guru</td>
                                    <td class="text-muted">
                                        <select class="form-control" id="id_guru" name="id_guru" required>
                                            <option value="" disabled="disabled" selected="selected">Pilih Guru </option>
                                            @foreach ($guru as $guru)
                                                <option value="{{ $guru['id'] }}">{{ $guru['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Penilai</td>
                                    <td class="text-muted">
                                        <select class="form-control" id="id_penilai" name="id_penilai" required>
                                            <option value="" disabled="disabled" selected="selected">Pilih Penilai </option>
                                            @foreach ($penilai as $guru)
                                                <option value="{{ $guru['id'] }}">{{ $guru['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td>Periode</td>
                                    <td class="text-muted">
                                        <input type="number" name="periode" id="periode" class="form-control" required>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">New Tim Penilai / Guru </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Guru</td>
                                    <td class="text-muted">
                                        <select class="form-control" id="id_guru_post" name="id_guru_post" required>
                                            <option value="" disabled="disabled" selected="selected">Pilih Guru </option>
                                            @foreach ($gurux as $guru)
                                                <option value="{{ $guru['id'] }}">{{ $guru['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Penilai</td>
                                    <td class="text-muted">
                                        <select class="form-control" id="id_penilai_post" name="id_penilai_post" required>
                                            <option value="" disabled="disabled" selected="selected">Pilih Penilai </option>
                                            @foreach ($penilaix as $guru)
                                                <option value="{{ $guru['id'] }}">{{ $guru['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td class="text-muted">
                                        <input type="number" name="periode_post" id="periode_post" class="form-control"
                                            required value="{{ session('tahun') ?? '' }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save " id="savebtn">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="form-inline">
                            <div class="form-group mr-2">
                                <div class="input-group">
                                    <select class="form-control mt-1 " id="periode_list" name="periode_list">
                                        <option value="" disabled="disabled" selected="selected">Pilih Periode </option>
                                        @foreach ($tahunlist as $tahun)
                                            <option value="{{ $tahun }}">{{ $tahun }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_pengaturan" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Photo Guru</th>
                                        <th>Nama Guru</th>
                                        <th>NIP Guru</th>
                                        <th>Nama Penilai</th>
                                        <th>Photo Penilai</th>
                                        <th>NIP Penilai</th>
                                        <th>Periode</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Photo Guru</th>
                                        <th>Nama Guru</th>
                                        <th>NIP Guru</th>
                                        <th>Nama Penilai</th>
                                        <th>Photo Penilai</th>
                                        <th>NIP Penilai</th>
                                        <th>Periode</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_master_settinggurupenilai') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'photo',
                            name: 'photo'
                        },
                        {
                            data: 'guru',
                            name: 'guru'
                        },
                        {
                            data: 'nip',
                            name: 'nip'
                        },
                        {
                            data: 'penilai',
                            name: 'penilai'
                        },
                        {
                            data: 'photo_penilai',
                            name: 'photo_penilai'
                        },
                        {
                            data: 'nip_penilai',
                            name: 'nip_penilai'
                        },
                        {
                            data: 'periode',
                            name: 'periode'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_pengaturan').dataTable(config);

                //filter
                $('body').on('change', 'select[name="periode_list"]', function() {
                    var tahun = $(this).val();
                    var url = "{{ route('ajax_periode_settinggurupenilai', ['tahun' => ':tahun']) }}";
                    url = url.replace(':tahun', tahun);

                    config = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">add_circle</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    $('#modalshowPost').modal('show');
                                }
                            },
                            {
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'guru',
                                name: 'guru'
                            },
                            {
                                data: 'nip',
                                name: 'nip'
                            },
                            {
                                data: 'penilai',
                                name: 'penilai'
                            },
                            {
                                data: 'photo_penilai',
                                name: 'photo_penilai'
                            },
                            {
                                data: 'nip_penilai',
                                name: 'nip_penilai'
                            },
                            {
                                data: 'periode',
                                name: 'periode'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_adminx = $('#table_pengaturan').dataTable(config);

                });

                $('body').on('click', '.show.setting', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_settinggurupenilai', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshow').modal('show');
                                    $('.text-muted.nama_guru').html(rows.guru);
                                    $('.text-muted.nama_penilai').html(rows.penilai);
                                    $('.text-muted.nip_guru').html(rows.nip);
                                    $('.text-muted.nip_penilai').html(rows.nip_penilai);
                                    $('.text-muted.periode').html('Tahun Periode ' + rows
                                        .periode);
                                    if (rows.file != null) {
                                        var flex = `<img src="` + rows.file +
                                            `" width="100px" height="100px">`;
                                        $('.text-muted.photo_guru').html(flex);
                                    }
                                    if (rows.file_penilai != null) {
                                        var flex = `<img src="` + rows.file_penilai +
                                            `" width="100px" height="100px">`;
                                        $('.text-muted.photo_penilai').html(flex);
                                    }
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.setting', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_settinggurupenilai', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_setting"]').val(rows.id);
                                    $("select[name='id_guru'] > option[value=" + rows
                                        .id_guru + "]").prop("selected", true);
                                    $("select[name='id_penilai'] > option[value=" + rows
                                        .id_penilai + "]").prop("selected", true);
                                    $('input[name="periode"]').val(rows.periode);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });


                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_setting"]').val();
                    var id_guru = $('select[name="id_guru"] option:selected').val();
                    var id_penilai = $('select[name="id_penilai"] option:selected').val();
                    if (id_guru == id_penilai) {
                        window.notif('error', 'Silahkan Pilih Guru / Penilai yang lain!');
                    } else {
                        var urlx = '{{ route('update_setting_gurupenilai', ':id') }}';
                        urlx = urlx.replace(':id', id);
                        var formData = new FormData(this);
                        const loader = $('button.update');
                        $.ajax({
                            type: "PUT",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $('#modalshowEdit').modal('hide');
                                        table_adminx.fnDraw(false);
                                        $(loader).html('<i class="fa fa-save"></i> Update');

                                        window.notif(result['info'], result['message']);

                                    } else if (result['info'] == 'error') {
                                        $(loader).html('<i class="fa fa-save"></i> Update');

                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                }

                            }
                        });

                    }

                });

                //remove
                $('body').on('click', '.remove.setting', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_settingGuruPenilai', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });


                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var id_guru = $('select[name="id_guru_post"] option:selected').val();
                    var id_penilai = $('select[name="id_penilai_post"] option:selected').val();
                    if (id_guru == id_penilai) {
                        window.notif('error', 'Silahkan Pilih Guru / Penilai yang lain!');
                    } else {

                        var urlx = '{{ route('post_setting_gurupenilai') }}';
                        var formData = new FormData(this);
                        const loader = $('button.save');
                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $('#modalshowPost').modal('hide');
                                        table_adminx.fnDraw(false);
                                        $(loader).html('<i class="fa fa-save"></i> Save');

                                        window.notif(result['info'], result['message']);

                                        $('#select[name="id_guru_post"] option:first').prop(
                                            'selected', true);
                                        $('#select[name="id_penilai_post"] option:first')
                                            .prop('selected', true);

                                    } else if (result['info'] == 'error') {
                                        $(loader).html('<i class="fa fa-save"></i> Save');

                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                }

                            }
                        });

                    }
                });


                $('.modal').on('hidden.bs.modal', function() {
                    $(this).find('form')[0].reset();
                });


                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_adminx.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }



            });
        })(jQuery, window);
    </script>
@endsection
