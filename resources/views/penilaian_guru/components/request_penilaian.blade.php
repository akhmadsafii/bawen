@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"> Pilih Tim Penilaian Kinerja Guru </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Periode {{ session('tahun') }}</p>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-6 widget-holder ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center mx-auto">
                <div class="widget-bg">
                    <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <table class="table mb-0">
                                <tbody>
                                    <tr style="display:none;">
                                        <td>Guru</td>
                                        <td class="text-muted">
                                            <select class="form-control" id="id_guru_post" name="id_guru_post"
                                                readonly="readonly">
                                                <option value="" disabled="disabled" selected="selected">Pilih Guru
                                                </option>
                                                @foreach ($guru as $guru)
                                                    @if ($guru['id'] == session('id'))
                                                        <option value="{{ $guru['id'] }}" selected="selected">
                                                            {{ $guru['nama'] }}</option>
                                                    @else
                                                        <option value="{{ $guru['id'] }}" disabled="disabled">
                                                            {{ $guru['nama'] }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Penilai</td>
                                        <td class="text-muted">
                                            <select class="form-control" id="id_penilai_post" name="id_penilai_post"
                                                required>
                                                <option value="" disabled="disabled" selected="selected">Pilih Penilai
                                                </option>
                                                @foreach ($penilai as $guru)
                                                    <option value="{{ $guru['id'] }}">{{ $guru['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td>Periode</td>
                                        <td class="text-muted">
                                            <input type="number" name="periode_post" id="periode_post"
                                                class="form-control" required value="{{ session('tahun') ?? '' }}">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info save " id="savebtn">
                                <i class="material-icons list-icon">save</i>
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var id_guru = $('select[name="id_guru_post"] option:selected').val();
                    var id_penilai = $('select[name="id_penilai_post"] option:selected').val();
                    if (id_guru == id_penilai) {
                        window.notif('error', 'Silahkan Pilih Guru / Penilai yang lain!');
                    } else {

                        var urlx = '{{ route('post_setting_gurupenilai') }}';
                        var formData = new FormData(this);
                        const loader = $('button.save');
                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {

                                        $(loader).html('<i class="fa fa-save"></i> Save');

                                        window.notif(result['info'], result['message']);

                                        setTimeout(() => {
                                            window.location.href = "{{ route('dashboard-pkg') }}";
                                        }, 3000);

                                    } else if (result['info'] == 'error') {
                                        $(loader).html('<i class="fa fa-save"></i> Save');

                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                }

                            }
                        });

                    }
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
