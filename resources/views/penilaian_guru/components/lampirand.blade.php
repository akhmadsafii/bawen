@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap Survei Penilaian {{ $lampiran['nama_guru'] }} -
                {{ $lampiran['nip_guru'] }} </p>
            <p> , Penilai : {{ $lampiran['nama_penilai'] }} - {{ $lampiran['nip_penilai'] }}</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header clearfix ">
                        <p class="text-info">Silakan Gunakan Fitur Print Untuk mulai mencetak Rekap.
                            <button type="button" id="printdata"><i class="fa fa-print"></i></button>
                        </p>
                    </div>
                    <div class="widget-body clearfix">
                        <center>
                            <div class="table-responsive col-md-12">
                                <div class="btn-group" style="flex-wrap: wrap;">
                                    <a href="{{ route('print_lampiran_pkg', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Cover</a>
                                    @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                        <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                            class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2"
                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                    @endforeach
                                    <a href="{{ route('print_lampiran1b', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 B </a>
                                    <a href="{{ route('print_lampiran1c', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 ">Lampiran
                                        1 C </a>
                                    <a href="{{ route('print_lampiran1d', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 btn-info">Lampiran
                                        1 D </a>
                                </div>
                            </div>
                        </center>
                        <div id="printable">
                            <style type="text/css">
                                @media print {
                                    .pagebreak {
                                        clear: both;
                                        page-break-after: always;
                                    }

                                    @page {
                                        size: legal portrait;
                                        margin: 10mm;
                                    }
                                }

                            </style>
                            <right>
                                <table width="20%" border="1">
                                    <tbody>
                                        <tr>
                                            <td><b>
                                                    <center>FORMAT 1D
                                                </b></center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </right>
                            <center>
                                <h6>
                                    <b>REKAP HASIL PENILAIAN KINERGA GURU KELAS/MATA PELAJARAN
                                    </b>
                                </h6>
                            </center>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>A.</td>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['nama_guru']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['nip_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Tempat/Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['tempat_lahir'] }} ,
                                            {{ \Carbon\Carbon::parse($lampiran['tgl_lahir'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pangkat/Jabatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pangkat_guru'] }} / {{ $lampiran['jabatan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>TMT Sebagai guru </td>
                                        <td>:</td>
                                        <td>{{ \Carbon\Carbon::parse($lampiran['terhitung_mulai_tanggal'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Masa Kerja </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['masa_kerja_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Jenis Kelamin </td>
                                        <td>:</td>
                                        <td>
                                            @if (!empty($lampiran['jenkel_guru']))
                                                @switch($lampiran['jenkel_guru'])
                                                    @case('l')
                                                        Laki-Laki
                                                    @break
                                                    @case('p')
                                                        Perempuan
                                                    @break
                                                    @default
                                                @endswitch
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pendidikan Terakhir / Spesialis </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pendidikan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Program Keahlian yang diampu </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['mapel_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>B.</td>
                                        <td>Nama Sekolah</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['sekolah']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Desa / Kelurahan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kelurahan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kecamatan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kabupaten / Kota </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kabupaten'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Provinsi</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['provinsi'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table width="100%" border="1">
                                @php
                                    $total_angka_kredit = 0;
                                @endphp
                                <tbody>
                                    <tr>
                                        <td>
                                            <p class="ml-2">Nilai PK GURU Kelas/Mata Pelajaran </p>
                                        </td>
                                        <td>{{ $lampiran['nilai_c'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="ml-2">
                                                Konversi nilai PK GURU ke dalam skala 0 – 100 sesuai Permenneg PAN & RB No.
                                                16 Tahun 2009 dengan rumus
                                                Nilai PKG (100) = Nilai PKG / Nilai PKG tertinggi x 100
                                            </p>
                                        </td>
                                        <td>{{ round($lampiran['nilai_kompetensi']) }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="ml-2">
                                                Berdasarkan hasil konversi ke dalam skala nilai sesuai dengan peraturan
                                                tersebut, selanjutnya ditetapkan sebutan dan persentase angka kreditnya
                                            </p>
                                        </td>
                                        <td>{{ $lampiran['predikat'] }} <br> {{ $lampiran['percent'] }}% </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="ml-2">
                                                Perolehan angka kredit (untuk pembelajaran) yang dihitung berdasarkan rumus
                                                berikut ini
                                                Angka Kredit satu semester = (AKK - AKPKB - AKP) X (JM/JWM) X NPK / jumlah
                                                tahun Masa Kerja
                                            </p>
                                            <p class="ml-2">Keterangan:</p>
                                            <ol>
                                                <li>AKK : Angka Kredit Kumulatif, AKPKB : Angka Kredit dari PKB, AKP : Angka
                                                    Kredit dari unsur penunjang</li>
                                                <li>JM : Jumlah jam mengajar (tatap muka) guru di sekolah</li>
                                                <li>JWM : jumlah jam wajib mengajar (24 – 40 jam tatap muka per minggu)</li>
                                                <li>JM/JWM = 1 jumlah jam wajib mengajar 24 – 40 jam / minggu</li>
                                                <li>JM/JWM = JM/24 jumlah jam mengajar < 24 jam / minggu</li>
                                                <li>NPK : Nilai Perolehan Kinerja dalam bentuk persentase</li>
                                            </ol>
                                        </td>
                                        @php
                                            $total_angka_kredit = ((intval($lampiran['akk']) - intval($lampiran['akpkb']) - intval($lampiran['akp'])) * ((intval($lampiran['jam_mengajar'])) / intval($lampiran['jam_wajib_mengajar'])) * (intval($lampiran['percent'])/100)) / intval($lampiran['lama_mengajar']);
                                        @endphp
                                        <td>{{ intval($total_angka_kredit) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table width="100%">
                                <tbody>
                                    <tr align="center">
                                        <td>Guru yang dinilai</td>
                                        <td>Penilai</td>
                                        <td>Kepala Sekolah</td>
                                    </tr>
                                    <tr align="center">
                                        <td style="height:100px;"></td>
                                        <td style="height:100px;"></td>
                                        <td style="height:100px;"></td>
                                    </tr>
                                    <tr align="center">
                                        <td><b><u>{{ Str::ucfirst($lampiran['nama_guru']) }}</u></b></td>
                                        <td><b><u>{{ Str::ucfirst($lampiran['nama_penilai']) }}</u></b></td>
                                        <td><b><u>{{ Str::ucfirst($lampiran['kepala_sekolah']) }} </u></b></td>
                                    </tr>
                                    <tr align="center">
                                        <td>NIP : {{ $lampiran['nip_guru'] }}</td>
                                        <td>NIP : {{ $lampiran['nip_penilai'] }}</td>
                                        <td>NIP : {{ $lampiran['nip_kepsek'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $('body').on('click', '#printdata', function() {
                    var printContents = document.getElementById('printable').innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    //window.stateX();
                    document.body.innerHTML = originalContents
                });

                window.stateX = function CheckWindowState() {
                    if (document.readyState == "complete") {
                        window.location.reload();
                    } else {
                        setTimeout("CheckWindowState()", 1000)
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
