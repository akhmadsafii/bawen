@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <style>
        .disabledinput {
            opacity: 0.4;
            filter: alpha(opacity=40);
            background-color: blue;
            cursor: not-allowed;
            pointer-events: none;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Form Penilaian Kinerja Guru </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Periode {{ $periode }}</p>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Guru : {{ $nama_guru }}</li>
                <li class="breadcrumb-item active">NIP : {{ $nip_guru }}</li>
                <li class="breadcrumb-item "><span class="loading text-center"></span></li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <ul class="nav nav-tabs">
                                <li class="nav-item active" aria-expanded="false"><a class="nav-link"
                                        href="#home-tab-bordered-1" data-toggle="tab" aria-expanded="false">Kompetensi
                                        Penilaian</a>
                                </li>
                                <li class="nav-item"><a class="nav-link"
                                        href="{{ route('form_penilaiangurusikap', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => '1']) }}"
                                        aria-expanded="false">Kompetensi Sikap</a>
                                </li>
                                <li class="nav-item"><a class="nav-link"
                                        href="{{ route('form_lampiran_formatC', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        aria-expanded="false">Format
                                        C</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home-tab-bordered-1" aria-expanded="false">
                                    <center>
                                        <div class="table-responsive col-md-12">
                                            <div class="btn-group" style="flex-wrap: wrap;">
                                                @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                                    @if ($kompetensi_penilaian['id'] == $id_kompetensi)
                                                        <a href="{{ route('form_penilaianguru', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                                            class="btn btn-outline-default btn-info kompetensi-penilaian"
                                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                                        &nbsp;
                                                    @else
                                                        <a href="{{ route('form_penilaianguru', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                                            class="btn btn-outline-default kompetensi-penilaian"
                                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                                        &nbsp;
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </center>
                                    <br>
                                    <h4> {{ $detail_kompetensi['nama'] }}</h4>
                                    <small>
                                        <p class="text-info">{{ $detail_kompetensi['keterangan'] }}</p>
                                    </small>
                                    <br>
                                    <form id="PFormxNew" name="PFormxNew" class="form-horizontal"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="table-responsive">
                                            <input type="hidden" name="id_guru" id="id_guru" value="{{ $id_guru }}">
                                            <input type="hidden" name="id_penilai" id="id_penilai"
                                                value="{{ $id_penilai }}">
                                            <input type="hidden" name="id_kompetensi" id="id_kompetensi"
                                                value="{{ $id_kompetensi }}">
                                            <input type="hidden" name="periode" id="periode" value="{{ $periode }}">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Indikator</th>
                                                        <th class="text-center">Tidak Terpenuhi (0)</th>
                                                        <th class="text-center">Terpenuhi Sebagian (1)</th>
                                                        <th class="text-center">Seluruhnya Terpenuhi (2)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($list_indikator as $indikator)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $indikator['nama'] }} </td>
                                                            <td class="text-center"><input type="radio"
                                                                    name="option[{{ $indikator['id'] }}]" value="0"
                                                                    @if ($indikator['nilai'] == '0') checked="checked" @endif
                                                                    class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif"
                                                                    ></td>
                                                            <td class="text-center"><input type="radio"
                                                                    name="option[{{ $indikator['id'] }}]" value="1"
                                                                    @if ($indikator['nilai'] == '1') checked="checked" @endif
                                                                    class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif"
                                                                    ></td>
                                                            <td class="text-center"><input type="radio"
                                                                    name="option[{{ $indikator['id'] }}]" value="2"
                                                                    @if ($indikator['nilai'] == '2') checked="checked" @endif
                                                                    class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif"
                                                                  ></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.tabs -->
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    @if (session('role') == 'penilai-pkg')
        <script>
            (function($, global) {

                "use-strict"
                $(document).ready(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    });

                    $('body').on('click', 'input[type="radio"]', function() {
                        $('Form#PFormxNew').submit();
                    });

                    $('body').on('submit', 'Form#PFormxNew', function(e) {
                        e.preventDefault();
                        var urlx = '{{ route('store_survei_penilaian_form') }}';
                        var formData = new FormData(this);
                        const loader = $('span.loading');
                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $(loader).html('');
                                        window.notif(result['info'], result['message']);
                                    } else if (result['info'] == 'error') {
                                        $(loader).html('');
                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);

                                }

                            }
                        });

                    });

                    //toast
                    window.notif = function notif(tipe, value) {
                        $.toast({
                            icon: tipe,
                            text: value,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        return true;
                    }

                });

            })(jQuery, window);
        </script>
    @endif
@endsection
