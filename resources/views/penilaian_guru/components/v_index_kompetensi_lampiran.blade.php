@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Master Data </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Kompetensi Format C</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Format C</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshow" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Informasi Kompetensi Format C </h5>
                </div>
                <div class="modal-body">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Kode</td>
                                <td class="text-muted kode"></td>
                            </tr>
                            <tr>
                                <td>Nama Kompetensi</td>
                                <td class="text-muted nama"></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <span class="box-title"> Daftar Indikator </span> <br>
                    <div class="table-responsive">
                        <table id="table_kompetensi_indikator" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Indikator</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Indikator</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Informasi Kompetensi Format C </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_kompetensi" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Kode</td>
                                    <td class="text-muted">
                                        <input type="text" name="kode" id="kode" autocomplete="off" class="form-control"
                                            placeholder="Kode" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Kompetensi</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama" id="nama" autocomplete="off" class="form-control"
                                            pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">New Kompetensi Format C Informasi </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Kode</td>
                                    <td class="text-muted">
                                        <input type="text" name="kode_post" id="kode_post" autocomplete="off"
                                            class="form-control" placeholder="Kode" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_post" id="nama_post" autocomplete="off"
                                            class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama"
                                            required>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save " id="savebtn">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_kompetensi" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash, table_indikator;
            var config, config_trash, config_indikator;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_master_data_kompetensiFormatC') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_kompetensi').dataTable(config);

                $('body').on('click', '.show.formatc', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_kompetensiFormatC', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshow').modal('show');
                                    $('.text-muted.nama').html(rows.nama);
                                    $('.text-muted.kode').html(rows.kode);
                                    window.appendIndikator(rows.id);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.formatc', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_kompetensiFormatC', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_kompetensi"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="kode"]').val(rows.kode);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_kompetensi"]').val();
                    var urlx = '{{ route('update_master_kompetensiFormatC', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('post_master_kompetensiFormatC') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowPost').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove
                $('body').on('click', '.remove.formatc', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_master_kompetensiFormatC', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //append indikator
                window.appendIndikator = function indikator(id) {
                    var url = "{{ route('ajax_master_data_formatCbyKompetensi', ':id') }}";
                    url = url.replace(':id', id);
                    config_indikator = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                        ],
                    };

                    table_indikator = $('#table_kompetensi_indikator').dataTable(config_indikator);
                }

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_adminx.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                $('.modal').on('hidden.bs.modal', function() {
                    $(this).find('form')[0].reset();
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
