@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Report </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Report Periode {{ $tahun }}</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <p class="text-info">Hasil Rata-rata survei dari tim penilaian
                            <div class="form-inline">
                                <div class="form-group mr-2">
                                    <div class="input-group">
                                        <label class="label form-control mt-1 text-info"> Filter</label>
                                        <select class="form-control mt-1 " id="periode_list" name="periode_list">
                                            <option value="" disabled="disabled" selected="selected">Pilih Periode </option>
                                            @foreach ($tahunlist as $tahunx)
                                                @if ($tahunx ==  $tahun)
                                                    <option value="{{ $tahunx }}" selected="selected">{{ $tahunx }}</option>
                                                    @else
                                                    <option value="{{ $tahunx }}">{{ $tahunx }}</option>
                                                @endif

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </div>
                    <table class="table table-striped">
                        @php
                            $total_angka_kredit = 0;
                        @endphp
                        <tbody>
                            <tr>
                                <td>
                                    <p class="ml-2">Nilai PK GURU Kelas/Mata Pelajaran
                                    </p>
                                </td>
                                <td>{{ $rekap_guru['nilai_c'] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="ml-2">
                                        Konversi nilai PK GURU ke dalam skala 0 – 100 sesuai
                                        Permenneg PAN & RB No.
                                        16 Tahun 2009 dengan rumus
                                        Nilai PKG (100) = Nilai PKG / Nilai PKG tertinggi x 100
                                    </p>
                                </td>
                                <td>{{ round($rekap_guru['nilai_kompetensi'] ?? '0') }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="ml-2">
                                        Berdasarkan hasil konversi ke dalam skala nilai sesuai
                                        dengan peraturan
                                        tersebut, selanjutnya ditetapkan sebutan dan persentase
                                        angka kreditnya
                                    </p>
                                </td>
                                <td>{{ $rekap_guru['predikat'] ?? '-' }} <br>
                                    {{ $rekap_guru['percent'] ?? '-' }}%</td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="ml-2">
                                        Perolehan angka kredit (untuk pembelajaran) yang
                                        dihitung berdasarkan rumus
                                        berikut ini
                                        Angka Kredit satu semester = (AKK - AKPKB - AKP) X
                                        (JM/JWM) X NPK / jumlah
                                        tahun Masa Kerja
                                    </p>
                                    <p class="ml-2">Keterangan:</p>
                                    <ol>
                                        <li>AKK : Angka Kredit Kumulatif, AKPKB : Angka Kredit
                                            dari PKB, AKP : Angka Kredit dari unsur penunjang
                                        </li>
                                        <li>JM : Jumlah jam mengajar (tatap muka) guru di
                                            sekolah</li>
                                        <li>JWM : jumlah jam wajib mengajar (24 – 40 jam tatap
                                            muka per minggu)</li>
                                        <li>JM/JWM = 1 jumlah jam wajib mengajar 24 – 40 jam /
                                            minggu</li>
                                        <li>JM/JWM = JM/24 jumlah jam mengajar < 24 jam / minggu</li>
                                        <li>NPK : Nilai Perolehan Kinerja dalam bentuk
                                            persentase</li>
                                    </ol>
                                </td>
                                @php
                                    if(!empty($rekap_guru)){
                                        $total_angka_kredit = ((intval($rekap_guru['akk']) - intval($rekap_guru['akpkb']) - intval($rekap_guru['akp'])) * ((intval($rekap_guru['jam_mengajar'])*24) / intval($rekap_guru['jam_wajib_mengajar'])) * (intval($rekap_guru['percent'])/100)) / intval($rekap_guru['lama_mengajar']);
                                    }
                                @endphp
                                <td>{{ intval($total_angka_kredit) ?? '-' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                   //filter
                   $('body').on('change', 'select[name="periode_list"]', function() {
                    var tahun = $(this).val();
                    var url = "{{ route('rekap_rata_rata_filterpkg', ['tahun' => ':tahun']) }}";
                    url = url.replace(':tahun', tahun);
                    window.location.href = url;
                });

            });

        })(jQuery, window);
    </script>
@endsection
