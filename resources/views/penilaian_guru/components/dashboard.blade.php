@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Dashboard</h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Dashboard</li>
                <li class="breadcrumb-item"><a href="#informasi">Informasi Data Instansi</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('profilUserpkg') }}#profil">Profil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('profilUserpkg') }}#ubahpassword">Ubah Password</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('auth.logout') }}">Keluar</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-danger text-center">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-info">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading clearfix">
                        <h5 class="text-info">Selamat Datang , {{ session('username') }}</h5>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="row">
                            @if (session('role') == 'guru-pkg' || session('role') == 'penilai-pkg')
                                <div class="col-md-4 mb-2" id="informasi">
                                    <div class="card card-outline-color-scheme">
                                        <div class="card-header">
                                            <h5 class="card-title mt-0 mb-3"> <i class="material-icons list-icon">home</i>
                                                Informasi Data Instansi </h5>
                                        </div>
                                        <div class="card-body">
                                            @if (count($pengaturan) > 0)
                                                <table class="table mb-0">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama Sekolah</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['nama_sekolah'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kelurahan</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['kelurahan'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kecamatan</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['kecamatan'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kabupaten</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['kabupaten'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Provinsi</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['provinsi'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alamat</td>
                                                            <td>
                                                                {{ $pengaturan['alamat'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Mulai Penilaian</td>
                                                            <td class="text-muted">
                                                                {{ \Carbon\Carbon::parse($pengaturan['mulai_penilaian'])->isoFormat('dddd, D MMM Y') ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Akhir Penilaian</td>
                                                            <td class="text-muted">
                                                                {{ \Carbon\Carbon::parse($pengaturan['akhir_penilaian'])->isoFormat('dddd, D MMM Y') ?? '-' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tahun</td>
                                                            <td class="text-muted">
                                                                {{ $pengaturan['tahun'] ?? '-' }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @else
                                                <p class="text-red">Data Instansi Belum Di Setting > Pengaturan </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 mb-2">
                                    <div class="card card-outline-color-scheme">
                                        @if (session('role') == 'penilai-pkg')
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3">Daftar Tugas Periode
                                                    {{ $tahun }} </h5>
                                                <p class="card-subtitle"> Anda Diwajibkan menilai guru-guru dibawah</p>
                                            </div>
                                        @elseif(session('role') == 'guru-pkg')
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3">Daftar Hasil Rata-rata Penilaian </h5>
                                                <p class="card-subtitle">Hasil rata-rata survei dari tim penilaian Periode
                                                    {{ $tahun }} </p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            @if (session('role') == 'penilai-pkg')
                                                <table id="table_guru" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Photo</th>
                                                            <th>Nama</th>
                                                            <th>NIP</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Photo</th>
                                                            <th>Nama</th>
                                                            <th>NIP</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            @elseif(session('role') == 'guru-pkg')
                                                <table class="table table-striped">
                                                    @php
                                                        $total_angka_kredit = 0;
                                                    @endphp
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <p class="ml-2">Nilai PK GURU Kelas/Mata Pelajaran
                                                                </p>
                                                            </td>
                                                            <td>{{ $rekap_guru['nilai_c'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p class="ml-2">
                                                                    Konversi nilai PK GURU ke dalam skala 0 – 100 sesuai
                                                                    Permenneg PAN & RB No.
                                                                    16 Tahun 2009 dengan rumus
                                                                    Nilai PKG (100) = Nilai PKG / Nilai PKG tertinggi x 100
                                                                </p>
                                                            </td>
                                                            <td>{{ round($rekap_guru['nilai_kompetensi']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p class="ml-2">
                                                                    Berdasarkan hasil konversi ke dalam skala nilai sesuai
                                                                    dengan peraturan
                                                                    tersebut, selanjutnya ditetapkan sebutan dan persentase
                                                                    angka kreditnya
                                                                </p>
                                                            </td>
                                                            <td>{{ $rekap_guru['predikat'] }} <br>
                                                                {{ $rekap_guru['percent'] }}%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p class="ml-2">
                                                                    Perolehan angka kredit (untuk pembelajaran) yang
                                                                    dihitung berdasarkan rumus
                                                                    berikut ini
                                                                    Angka Kredit satu semester = (AKK - AKPKB - AKP) X
                                                                    (JM/JWM) X NPK / jumlah
                                                                    tahun Masa Kerja
                                                                </p>
                                                                <p class="ml-2">Keterangan:</p>
                                                                <ol>
                                                                    <li>AKK : Angka Kredit Kumulatif, AKPKB : Angka Kredit
                                                                        dari PKB, AKP : Angka Kredit dari unsur penunjang
                                                                    </li>
                                                                    <li>JM : Jumlah jam mengajar (tatap muka) guru di
                                                                        sekolah</li>
                                                                    <li>JWM : jumlah jam wajib mengajar (24 – 40 jam tatap
                                                                        muka per minggu)</li>
                                                                    <li>JM/JWM = 1 jumlah jam wajib mengajar 24 – 40 jam /
                                                                        minggu</li>
                                                                    <li>JM/JWM = JM/24 jumlah jam mengajar < 24 jam /
                                                                            minggu</li>
                                                                    <li>NPK : Nilai Perolehan Kinerja dalam bentuk
                                                                        persentase</li>
                                                                </ol>
                                                            </td>
                                                            @php
                                                                $total_angka_kredit = ((intval($rekap_guru['akk']) - intval($rekap_guru['akpkb']) - intval($rekap_guru['akp'])) * ((intval($rekap_guru['jam_mengajar'])) / intval($rekap_guru['jam_wajib_mengajar'])) * (intval($rekap_guru['percent'])/100)) / intval($rekap_guru['lama_mengajar']);
                                                            @endphp
                                                            <td>{{ intval($total_angka_kredit) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if (session('role') == 'admin-pkg')
                                <div class="col-md-12">
                                    <div class="row">
                                        <!-- Counter: Sales -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-primary text-inverse">
                                                <div class="widget-body">
                                                    <div class="widget-counter">
                                                        <h6>Guru / Penilai
                                                        </h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_guru }}</span>
                                                        </h3><i class="material-icons list-icon">account_box</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                        <!-- Counter: Subscriptions -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-info text-inverse">
                                                <div class="widget-body clearfix">
                                                    <div class="widget-counter">
                                                        <h6>Jabatan</h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_jabatan }}</span>
                                                        </h3><i class="material-icons list-icon">account_box</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                        <!-- Counter: Users -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-warning text-inverse">
                                                <div class="widget-body clearfix">
                                                    <div class="widget-counter">
                                                        <h6>Pangkat</h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_pangkat }}</span>
                                                        </h3><i class="material-icons list-icon">account_box</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                        <!-- Counter: Pageviews -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-success text-inverse">
                                                <div class="widget-body clearfix">
                                                    <div class="widget-counter">
                                                        <h6>Pendidikan</h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_pendidikan }}</span>
                                                        </h3><i class="material-icons list-icon">account_box</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                        <!-- Counter: Pageviews -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-success text-inverse">
                                                <div class="widget-body clearfix">
                                                    <div class="widget-counter">
                                                        <h6>Guru Ternilai</h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_guru_dinilai }}</span>
                                                        </h3><i class="material-icons list-icon">checklist</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                        <!-- Counter: Pageviews -->
                                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height">
                                            <div class="widget-bg bg-success text-inverse">
                                                <div class="widget-body clearfix">
                                                    <div class="widget-counter">
                                                        <h6>Belum Ternilai</h6>
                                                        <h3 class="h1"><span
                                                                class="counter">{{ $count_guru_belum_dinilai }}</span>
                                                        </h3><i class="material-icons list-icon">alarm</i>
                                                    </div>
                                                    <!-- /.widget-counter -->
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                            <!-- /.widget-bg -->
                                        </div>
                                        <!-- /.widget-holder -->
                                    </div>
                                </div>
                                @if (!empty($array_chart))
                                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                                        <div class="widget-bg">
                                            <div class="widget-body clearfix">
                                                <h6 class="mr-t-0 mr-b-5 fw-700 text-center">Grafik Peringkat Nilai Top 5
                                                    Tertinggi Skor
                                                    Penilaian
                                                    Kompetensi Kinerja Guru</h6>
                                                <div id="barMorris"
                                                    style="margin-left:-15px; margin-right:-15px; height: 270px"></div>
                                            </div>
                                            <!-- /.widget-body -->
                                        </div>
                                        <!-- /.widget-bg -->
                                    </div>
                                @endif
                                @if (!empty($array_table_chart))
                                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                                        <div class="widget-bg">
                                            <div class="widget-body clearfix">
                                                <h6 class="mr-t-0 mr-b-5 fw-700 text-center">Tabel Peringkat Nilai Top 5
                                                    Tertinggi Skor
                                                    Penilaian
                                                    Kompetensi Kinerja Guru {{ session('tahun') }} </h6>
                                                <div class="table-responsive">
                                                    <table class="table-hover table tableWrap">
                                                        <thead class="table-info">
                                                            <th> No</th>
                                                            <th> Nama</th>
                                                            <th> Skor</th>
                                                            <th> Rata - rata </th>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($array_table_chart as $item)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }} </td>
                                                                    <td>{{ $item['nama'] }}</td>
                                                                    <td>{{ $item['nilai'] }}</td>
                                                                    <td>{{ $item['nilai_hasil'] }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if (!empty($array_chart_min))
                                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                                        <div class="widget-bg">
                                            <div class="widget-body clearfix">
                                                <h6 class="mr-t-0 mr-b-5 fw-700 text-center">Grafik Peringkat Nilai Top 5
                                                    Terendah Skor
                                                    Penilaian
                                                    Kompetensi Kinerja Guru</h6>
                                                <div id="barMorris2"
                                                    style="margin-left:-15px; margin-right:-15px; height: 270px"></div>
                                            </div>
                                            <!-- /.widget-body -->
                                        </div>
                                        <!-- /.widget-bg -->
                                    </div>
                                @endif
                                @if (!empty($array_table_chart_min))
                                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                                        <div class="widget-bg">
                                            <div class="widget-body clearfix">
                                                <h6 class="mr-t-0 mr-b-5 fw-700 text-center">Tabel Peringkat Nilai Top 5
                                                    Terendah Skor
                                                    Penilaian
                                                    Kompetensi Kinerja Guru {{ session('tahun') }} </h6>
                                                <div class="table-responsive">
                                                    <table class="table-hover table tableWrap">
                                                        <thead class="table-info">
                                                            <th> No</th>
                                                            <th> Nama</th>
                                                            <th> Skor</th>
                                                            <th> Rata - rata </th>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($array_table_chart_min as $item)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }} </td>
                                                                    <td>{{ $item['nama'] }}</td>
                                                                    <td>{{ $item['nilai'] }}</td>
                                                                    <td>{{ $item['nilai_hasil'] }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-12 mb-2" id="informasi">
                                    <div class="card card-outline-color-scheme">
                                        <div class="card-header">
                                            <h5 class="card-title mt-0 mb-3"> <i class="material-icons list-icon">home</i>
                                                Informasi Data Instansi </h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <center>
                                                    @if (count($pengaturan) > 0)
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Nama Sekolah</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['nama_sekolah'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Kelurahan</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['kelurahan'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Kecamatan</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['kecamatan'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Kabupaten</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['kabupaten'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Provinsi</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['provinsi'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Alamat</td>
                                                                    <td>
                                                                        {{ $pengaturan['alamat'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tanggal Mulai Penilaian</td>
                                                                    <td class="text-muted">
                                                                        {{ \Carbon\Carbon::parse($pengaturan['mulai_penilaian'])->isoFormat('dddd, D MMM Y') ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tanggal Akhir Penilaian</td>
                                                                    <td class="text-muted">
                                                                        {{ \Carbon\Carbon::parse($pengaturan['akhir_penilaian'])->isoFormat('dddd, D MMM Y') ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tahun</td>
                                                                    <td class="text-muted">
                                                                        {{ $pengaturan['tahun'] ?? '-' }}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    @else
                                                        <p class="text-red">Data Instansi Belum Di Setting >
                                                            Pengaturan </p>
                                                    @endif
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                @if (session('role') == 'admin-pkg')
                    var ctx = document.getElementById("barMorris");
                    if ( ctx === null ) return;

                    var chart = Morris.Bar({
                    element: 'barMorris',
                    data:@json($array_chart),
                    xkey: 'periode',
                    ykeys: @json($array_key_chart),
                    labels: @json($array_key_chart),
                    barColors:['#99d683', '#13dafe', '#6164c1', '#ffb600', '#ff6261'],
                    barOpacity: 0.5,
                    barSizeRatio: 0.5,
                    hideHover: 'auto',
                    gridLineColor: '#eef0f2',
                    resize: true
                    });


                    var ctx2 = document.getElementById("barMorris2");
                    if ( ctx2 === null ) return;

                    var chart2 = Morris.Bar({
                    element: 'barMorris2',
                    data:@json($array_chart_min),
                    xkey: 'periode',
                    ykeys: @json($array_key_chart_min),
                    labels: @json($array_key_chart_min),
                    barColors:['#99d683', '#13dafe', '#6164c1', '#ffb600', '#ff6261'],
                    barOpacity: 0.5,
                    barSizeRatio: 0.5,
                    hideHover: 'auto',
                    gridLineColor: '#eef0f2',
                    resize: true
                    });
                @endif

                @if (session('role') == 'penilai-pkg')
                    var table_adminx, table_trash;
                    var config, config_trash;
                    var urlx = "{{ route('ajax_task_penilaian_guru', ['id' => session('id'), 'tahun' => session('tahun')]) }}"
                    config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [
                    {
                    text: '<i class="material-icons list-icon">refresh</i>',
                    className: "btn btn-outline-default ripple",
                    action: function(e, dt, node, config) {
                    dt.ajax.reload();
                    }
                    }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: urlx,
                    columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                    },
                    {
                    data: 'photo',
                    name: 'photo'
                    },
                    {
                    data: 'guru',
                    name: 'guru'
                    },
                    {
                    data: 'nip',
                    name: 'nip'
                    },
                    {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                    }
                    ],
                    };

                    table_adminx = $('#table_guru').dataTable(config);
                @endif

            });
        })(jQuery, window);
    </script>
@endsection
