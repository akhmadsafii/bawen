@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap Survei Penilaian {{ $lampiran['nama_guru'] }} -
                {{ $lampiran['nip_guru'] }} </p>
            <p> , Penilai : {{ $lampiran['nama_penilai'] }} - {{ $lampiran['nip_penilai'] }}</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header clearfix ">
                        <p class="text-info">Silakan Gunakan Fitur Print Untuk mulai mencetak Rekap.
                            <button type="button" id="printdata"><i class="fa fa-print"></i></button>
                        </p>
                    </div>
                    <div class="widget-body clearfix">
                        <center>
                            <div class="table-responsive col-md-12">
                                <div class="btn-group" style="flex-wrap: wrap;">
                                    <a href="{{ route('print_lampiran_pkg', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Cover</a>
                                    @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                        <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                            class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2"
                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                    @endforeach
                                    <a href="{{ route('print_lampiran1b', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 btn-info">Lampiran
                                        1 B </a>
                                    <a href="{{ route('print_lampiran1c', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 C </a>
                                    <a href="{{ route('print_lampiran1d', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 D </a>
                                </div>
                            </div>
                        </center>
                        <div id="printable">
                            <center>
                                <h6>
                                    <b>LAPORAN DAN EVALUASI
                                    </b>
                                    <br>
                                    <b>PENILAIAN KINERJA GURU KELAS / GURU MATA PELAJARAN
                                    </b>
                                </h6>
                            </center>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>A.</td>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['nama_guru']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['nip_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Tempat/Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['tempat_lahir'] }} ,
                                            {{ \Carbon\Carbon::parse($lampiran['tgl_lahir'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pangkat/Jabatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pangkat_guru'] }} / {{ $lampiran['jabatan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>TMT Sebagai guru </td>
                                        <td>:</td>
                                        <td>{{ \Carbon\Carbon::parse($lampiran['terhitung_mulai_tanggal'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Masa Kerja </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['masa_kerja_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Jenis Kelamin </td>
                                        <td>:</td>
                                        <td>
                                            @if (!empty($lampiran['jenkel_guru']))
                                                @switch($lampiran['jenkel_guru'])
                                                    @case('l')
                                                        Laki-Laki
                                                    @break
                                                    @case('p')
                                                        Perempuan
                                                    @break
                                                    @default
                                                @endswitch
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pendidikan Terakhir / Spesialis </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pendidikan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Program Keahlian yang diampu </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['mapel_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>B.</td>
                                        <td>Nama Sekolah</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['sekolah']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Desa / Kelurahan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kelurahan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kecamatan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kabupaten / Kota </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kabupaten'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Provinsi</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['provinsi'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Periode Penilaian </td>
                                        <td>:</td>
                                        <td>{{ \Carbon\Carbon::parse($lampiran['mulai_penilaian'])->isoFormat('dddd, D MMM Y') }}
                                            S/D
                                            {{ \Carbon\Carbon::parse($lampiran['akhir_penilaian'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <center>
                                <b>
                                    <h6>PERSETUJUAN</h6>
                                </b>
                                <br>
                                <p>
                                    (Persetujuan ini harus ditandatangani oleh penilai dan guru yang dinilai)
                                </p>
                                <br>
                                <p>Penilai dan guru yang dinilai menyatakan telah membaca dan memahami semua aspek yang
                                    ditulis / dilaporkan dalam format ini dan menyatakan setuju.
                                </p>
                                <table width="80%">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>Nama Guru</td>
                                            <td>:</td>
                                            <td>{{ Str::ucfirst($lampiran['nama_guru']) }}</td>
                                            <td>Nama Penilai</td>
                                            <td>:</td>
                                            <td>{{ Str::ucfirst($lampiran['nama_penilai']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Tanda Tangan</td>
                                            <td>:</td>
                                            <td></td>
                                            <td>Tanda Tangan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="height:100px;"></td>
                                            <td></td>
                                            <td></td>
                                            <td style="height:100px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table width="80%">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>Tanggal</td>
                                            <td>:</td>
                                            <td>{{ \Carbon\Carbon::parse($lampiran['mulai_penilaian'])->isoFormat('dddd, D MMM Y') }}
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $('body').on('click', '#printdata', function() {
                    var printContents = document.getElementById('printable').innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    //window.stateX();
                    document.body.innerHTML = originalContents
                });

                window.stateX = function CheckWindowState() {
                    if (document.readyState == "complete") {
                        window.location.reload();
                    } else {
                        setTimeout("CheckWindowState()", 1000)
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
