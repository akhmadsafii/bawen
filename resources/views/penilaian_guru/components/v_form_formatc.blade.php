@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <style>
        .disabledinput {
            opacity: 0.4;
            filter: alpha(opacity=40);
            background-color: blue;
            cursor: not-allowed;
            pointer-events: none;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Form Penilaian Lampiran Format C </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Periode {{ $periode }}</p>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Guru : {{ $nama_guru }}</li>
                <li class="breadcrumb-item active">NIP : {{ $nip_guru }}</li>
                <li class="breadcrumb-item "><span class="loading text-center"></span></li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="tabs tabs-bordered">
                            <ul class="nav nav-tabs">
                                <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                        href="{{ route('form_penilaianguru', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => '1']) }}"
                                        aria-expanded="false">Kompetensi
                                        Penilaian</a>
                                </li>
                                <li class="nav-item"><a class="nav-link"
                                        href="{{ route('form_penilaiangurusikap', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => '1']) }}"
                                        aria-expanded="false">Kompetensi Sikap</a>
                                </li>
                                <li class="nav-item active"><a class="nav-link" href="#" aria-expanded="false">Format
                                        C</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home-tab-bordered-1" aria-expanded="false">
                                    <form id="PFormxNew" name="PFormxNew" class="form-horizontal"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="table-responsive">
                                            <input type="hidden" name="id_guru" id="id_guru" value="{{ $id_guru }}">
                                            <input type="hidden" name="id_penilai" id="id_penilai"
                                                value="{{ $id_penilai }}">
                                            <input type="hidden" name="periode" id="periode" value="{{ $periode }}">
                                            <table class="table table-hover">
                                                <thead>
                                                    <th>No</th>
                                                    <th>Kompetensi</th>
                                                    <th>Kurang (1)</th>
                                                    <th>Cukup (2)</th>
                                                    <th>Baik (3)</th>
                                                    <th>Sempurna (4) </th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($list_indikator as $indikator)
                                                        <input type="hidden" name="id_kompetensi_c" id="id_kompetensi_c"
                                                            value="{{ $indikator['id'] }}">
                                                        <tr class="text-info">
                                                            <td>{{ $indikator['kode'] }}</td>
                                                            <td colspan="5">{{ $indikator['nama'] }}</td>
                                                        </tr>
                                                        @foreach ($indikator['indikator'] as $list)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $list['nama'] }}</td>
                                                                <td>
                                                                    <input type="radio" name="option[{{ $list['id'] }}]"
                                                                        value="1"
                                                                        @if ($list['nilai'] == '1') checked="checked" @endif
                                                                        class="radiobox radio-info survei  @if(session('role') == 'guru-pkg') disabledinput  @endif  ">
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="option[{{ $list['id'] }}]"
                                                                        value="2"
                                                                        @if ($list['nilai'] == '2') checked="checked" @endif
                                                                        class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif">
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="option[{{ $list['id'] }}]"
                                                                        value="3"
                                                                        @if ($list['nilai'] == '3') checked="checked" @endif
                                                                        class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif">
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="option[{{ $list['id'] }}]"
                                                                        value="4"
                                                                        @if ($list['nilai'] == '4') checked="checked" @endif
                                                                        class="radiobox radio-info survei @if(session('role') == 'guru-pkg') disabledinput  @endif">
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </form>
                                    <center><a href="{{ route('print_lampiran_pkg',['id_guru'=>$id_guru,'id_penilai'=>$id_penilai,'periode'=>$periode]) }}" class="btn-info btn btn-sm" target="_blank"> <i class="fa fa-eye"></i> Lihat Rekap Penilaian Kinerja Guru </a> </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (session('role') == 'penilai-pkg')
        <script>
            (function($, global) {

                "use-strict"
                $(document).ready(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    });

                    $('body').on('click', 'input[type="radio"]', function() {
                        $('Form#PFormxNew').submit();
                    });

                    $('body').on('submit', 'Form#PFormxNew', function(e) {
                        e.preventDefault();
                        var urlx = '{{ route('store_survei_formFormatC') }}';
                        var formData = new FormData(this);
                        const loader = $('span.loading');
                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $(loader).html('');
                                        window.notif(result['info'], result['message']);
                                    } else if (result['info'] == 'error') {
                                        $(loader).html('');
                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);

                                }

                            }
                        });

                    });

                    //toast
                    window.notif = function notif(tipe, value) {
                        $.toast({
                            icon: tipe,
                            text: value,
                            hideAfter: 5000,
                            showConfirmButton: true,
                            position: 'top-right',
                        });
                        return true;
                    }

                });

            })(jQuery, window);
        </script>
    @endif
@endsection
