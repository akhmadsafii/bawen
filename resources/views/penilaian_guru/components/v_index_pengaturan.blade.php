@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Setting</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Data Instansi</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list col-12 ml-sm-auto col-sm-10 col-md-6 ml-md-auto mx-auto">
        <div class="row">
            <div class="col-md-12 widget-holder ">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive ">
                            <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data"
                                method="POST" action="{{ route('post_setting_pkg') }}">
                                @csrf
                                <table class="table mb-0">
                                    <tbody>
                                        <tr>
                                            <td>Nama Sekolah</td>
                                            <td class="text-muted">
                                                <input type="text" name="nama_sekolah_post" id="nama_sekolah_post"
                                                    autocomplete="off" class="form-control" placeholder="Nama Sekolah" required
                                                    value="{{ old('nama_sekolah_post') ?? ($pengaturan['nama_sekolah'] ?? session('sekolah')) }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kepala Sekolah</td>
                                            <td class="text-muted">
                                                <input type="text" name="kepala_sekolah_post" id="kepala_sekolah_post"
                                                    autocomplete="off" class="form-control" placeholder="Kepala Sekolah"
                                                    required
                                                    value="{{ old('kepala_sekolah_post') ?? ($pengaturan['kepala_sekolah'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NIP Kepala Sekolah</td>
                                            <td class="text-muted">
                                                <input type="text" name="nip_kepala_sekolah_post"
                                                    id="nip_kepala_sekolah_post" autocomplete="off" class="form-control"
                                                    placeholder="NIP Kepala Sekolah" required
                                                    value="{{ old('nip_kepala_sekolah_post') ?? ($pengaturan['nip_kepsek'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kelurahan</td>
                                            <td class="text-muted">
                                                <input type="text" name="kelurahan_post" id="kelurahan_post"
                                                    autocomplete="off" class="form-control" placeholder="Kelurahan"
                                                    required
                                                    value="{{ old('kelurahan_post') ?? ($pengaturan['kelurahan'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td class="text-muted">
                                                <input type="text" name="kecamatan_post" id="kecamatan_post"
                                                    autocomplete="off" class="form-control" placeholder="Kelurahan"
                                                    required
                                                    value="{{ old('kecamatan_post') ?? ($pengaturan['kecamatan'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kabupaten</td>
                                            <td class="text-muted">
                                                <input type="text" name="kabupaten_post" id="kabupaten_post"
                                                    autocomplete="off" class="form-control" placeholder="Kabupaten"
                                                    required
                                                    value="{{ old('kabupaten_post') ?? ($pengaturan['kabupaten'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Provinsi</td>
                                            <td class="text-muted">
                                                <input type="text" name="provinsi_post" id="provinsi_post"
                                                    autocomplete="off" class="form-control" placeholder="Provinsi"
                                                    required
                                                    value="{{ old('provinsi_post') ?? ($pengaturan['provinsi'] ?? '') }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>
                                                <textarea class="form-control" name="alamat_post" id="alamat_post" placeholder="Alamat"
                                                    rows="3">{{ old('alamat_post') ?? ($pengaturan['alamat'] ?? '') }}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Mulai Penilaian</td>
                                            <td class="text-muted">
                                                <div class="input-group">
                                                    <input id="tanggal_mulai" type="text" name="tanggal_mulai"
                                                        value="{{ old('tanggal_mulai') ?? ($pengaturan['mulai_penilaian'] ?? \Carbon\Carbon::now()->format('Y-m-d')) }}"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'
                                                        title="Format harus berisi tanggal " readonly="readonly" >
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Akhir Penilaian</td>
                                            <td class="text-muted">
                                                <div class="input-group">
                                                    <input id="tanggal_akhir" type="text" name="tanggal_akhir"
                                                        value="{{ old('tanggal_akhir') ?? ($pengaturan['akhir_penilaian'] ?? \Carbon\Carbon::now()->format('Y-m-d')) }}"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'
                                                        title="Format harus berisi tanggal " readonly="readonly">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tahun</td>
                                            <td class="text-muted">
                                                <input type="text" name="tahun_post" id="tahun_post" autocomplete="off"
                                                    class="form-control" placeholder="Tahun" required
                                                    value="{{ old('tahun_post') ?? ($pengaturan['tahun'] ?? session('tahun')) }}">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="text-center mt-2">
                                    <button type="submit" class="btn btn-info save " id="savebtn">
                                        <i class="material-icons list-icon">save</i>
                                        Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection
