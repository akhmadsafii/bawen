@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <style>
        td.details-control {
            cursor: pointer;
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_open.png') no-repeat center center;
        }

        tr.shown td.details-control {
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_close.png') no-repeat center center;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Master Data </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Guru / Penilai</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Guru / Penilai</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshow" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Informasi </h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table mb-0" witdh="100%">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted nama"></td>
                                    <td>NIP</td>
                                    <td class="text-muted nip"></td>
                                    <td>Jenis Kelamin</td>
                                    <td class="text-muted jenkel"></td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir </td>
                                    <td class="text-muted tempat_lahir"></td>
                                    <td>Tanggal Lahir </td>
                                    <td class="text-muted tanggal_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Telepon</td>
                                    <td class="text-muted telepon"></td>
                                    <td>Alamat</td>
                                    <td class="text-muted alamat"></td>
                                    <td>Email</td>
                                    <td class="text-muted email"></td>
                                </tr>
                                <tr>
                                    <td>Agama</td>
                                    <td class="text-muted agama"></td>
                                    <td>Pendidikan</td>
                                    <td class="text-muted pendidikan"></td>
                                    <td>Pangkat</td>
                                    <td class="text-muted pangkat"></td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td class="text-muted photo"></td>
                                    <td>NIK </td>
                                    <td class="text-muted nik"></td>
                                    <td>NUPTK </td>
                                    <td class="text-muted nuptk"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Guru Informasi </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_guru" class="form-control">
                    <div class="modal-body">
                        <div class="row mr-b-50">
                            <div class="col-md-3 mb-3 input-has-value">
                                <label for="validationServer01">Nama</label>
                                <input type="text" name="nama" id="nama" autocomplete="off" class="form-control"
                                    pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                    title="Format harus berisi alfanumerik atau huruf saja " placeholder="Nama">
                            </div>
                            <div class="col-md-3 mb-3 input-has-value">
                                <label for="validationServer02">NIK</label>
                                <input type="text" name="nik" id="nik" autocomplete="off" class="form-control"
                                    placeholder="NIK">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer03">NIP</label>
                                <input type="text" name="nip" id="nip" autocomplete="off" class="form-control"
                                    placeholder="NIP">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer03">NUPTK</label>
                                <input type="text" name="nuptk" id="nuptk" autocomplete="off" class="form-control"
                                    placeholder="NUPTK">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Jabatan</label>
                                <select class="form-control" name="jabatan" id="jabatan">
                                    <option value="" disabled="disabled" selected="selected">Pilih Jabatan
                                    </option>
                                    @foreach ($jabatan as $jabatan)
                                        <option value="{{ $jabatan['id'] }}">{{ $jabatan['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pendidikan</label>
                                <select class="form-control" name="pendidikan" id="pendidikan">
                                    <option value="" disabled="disabled" selected="selected">Pilih Pendidikan
                                    </option>
                                    @foreach ($pendidikan as $pendidikan)
                                        <option value="{{ $pendidikan['id'] }}">{{ $pendidikan['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pangkat</label>
                                <select class="form-control" name="pangkat" id="pangkat">
                                    <option value="" disabled="disabled" selected="selected">Pilih Pangkat
                                    </option>
                                    @foreach ($pangkat as $pangkat)
                                        <option value="{{ $pangkat['id'] }}">{{ $pangkat['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pengampu Mapel</label>
                                <select class="form-control" name="mapel" id="mapel">
                                    <option value="" disabled="disabled" selected="selected">Pilih Mata Pelajaran
                                    </option>
                                    @foreach ($mapel as $mapel)
                                        <option value="{{ $mapel['id'] }}">{{ $mapel['nama'] }}</option>
                                    @endforeach
                                    <option value="0">Lainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir" autocomplete="off"
                                    class="form-control" placeholder="Tempat Lahir">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer05">Tanggal Lahir</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="tanggal_lahir"
                                        id="tanggal_lahir" data-date-format="yyyy-mm-dd"
                                        data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                    <span class="input-group-addon"><i
                                            class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer05">Terhitung Mulai Tanggal </label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="terhitung_mulai_tanggal"
                                        id="terhitung_mulai_tanggal" data-date-format="yyyy-mm-dd"
                                        data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                    <span class="input-group-addon"><i
                                            class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Masa Kerja</label>
                                <input type="text" name="masa_kerja" id="masa_kerja" autocomplete="off"
                                    class="form-control" placeholder="Masa Kerja">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Email</label>
                                <input type="text" name="email" id="email" autocomplete="off" class="form-control"
                                    placeholder="Email">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Jenis Kelamin</label>
                                <select class="form-control" name="jenkel" id="jenkel">
                                    <option value="" disabled="disabled" selected="selected">Pilih Jenis Kelamin
                                    </option>
                                    <option value="l">Laki-Laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Agama</label>
                                <input type="text" name="agama" id="agama" autocomplete="off" class="form-control"
                                    placeholder="Agama">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Telepon</label>
                                <input type="tel" name="telepon" id="telepon" autocomplete="off" class="form-control"
                                    placeholder="telepon">
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationServer04">Alamat</label>
                                <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">RT</label>
                                <input type="number" name="rt" id="rt" autocomplete="off" class="form-control"
                                    placeholder="RT">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">RW</label>
                                <input type="number" name="rw" id="rw" autocomplete="off" class="form-control"
                                    placeholder="RT">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Dusun</label>
                                <input type="text" name="dusun" id="dusun" autocomplete="off" class="form-control"
                                    placeholder="Dusun">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Kecamatan</label>
                                <input type="text" name="kecamatan" id="kecamatan" autocomplete="off" class="form-control"
                                    placeholder="Kecamatan">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Kode POS</label>
                                <input type="text" name="kodepos" id="kodepos" autocomplete="off" class="form-control"
                                    placeholder="Kode POS">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Password</label>
                                <div class="input-group">
                                    <input class="form-control password" id="password" name="password"
                                        placeholder="Password" type="password"> <span class="input-group-addon">
                                        <a href="javascript:void(0)"><i
                                                class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                    </span>
                                </div>
                                <a href="javascript:void(0)" class="btn btn-info mt-2 float-right "
                                    id="generate_pass">Generate</a>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Role</label>
                                <select class="form-control" name="role" id="role">
                                    <option value="" disabled="disabled" selected="selected">Pilih Role Login
                                    </option>
                                    <option value="guru">Guru</option>
                                    <option value="penilai">Penilai</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Photo</label>
                                <div class="col-sm-12">
                                    <div class="form-group row mb-0">
                                        <label class="col-md-3 col-form-label" for="l1"></label>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <img id="modal-previewedit" src="https://via.placeholder.com/150"
                                                        alt="Preview" class="form-group mb-1" width="auto" height="auto"
                                                        style="margin-top: 10px">
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group row">
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input id="image" type="file" name="image" accept="image/*"
                                                                    onchange="readURL(this);">
                                                                <input type="hidden" name="hidden_imagex"
                                                                    id="hidden_imagex">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="validationServer04">Informasi Lainnya </label>
                                <textarea class="form-control" name="informasi_lain" id="informasi_lain"
                                    rows="3"></textarea>
                            </div>
                            <div class="col-md-12 mb-3 roles-guru-edit">
                                <label for="validationServer04">Jam Wajib Mengajar (24 – 40 jam tatap muka per
                                    minggu)</label>
                                <input type="number" class="form-control" name="jam_wajib_mengajar"
                                    id="jam_wajib_mengajar" />
                            </div>
                            <div class="col-md-12 mb-3 roles-guru-edit">
                                <label for="validationServer04">Jam Mengajar (tatap muka) guru di sekolah (24 – 40 jam tatap muka per minggu) </label>
                                <input type="number" class="form-control" name="jam_mengajar" id="jam_mengajar" />
                            </div>
                            <div class="col-md-12 mb-3 roles-guru-edit">
                                <label for="validationServer04">Lama Mengajar </label>
                                <input type="number" class="form-control" name="lama_mengajar" id="lama_mengajar" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">New Guru Informasi </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row mr-b-50">
                            <div class="col-md-3 mb-3 input-has-value">
                                <label for="validationServer01">Nama</label>
                                <input type="text" name="nama_post" id="nama_post" autocomplete="off"
                                    class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                    title="Format harus berisi alfanumerik atau huruf saja " placeholder="Nama">
                            </div>
                            <div class="col-md-3 mb-3 input-has-value">
                                <label for="validationServer02">NIK</label>
                                <input type="text" name="nik_post" id="nik_post" autocomplete="off" class="form-control"
                                    placeholder="NIK">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer03">NIP</label>
                                <input type="text" name="nip_post" id="nip_post" autocomplete="off" class="form-control"
                                    placeholder="NIP">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer03">NUPTK</label>
                                <input type="text" name="nuptk_post" id="nuptk_post" autocomplete="off"
                                    class="form-control" placeholder="NUPTK">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Jabatan</label>
                                <select class="form-control" name="jabatan_post" id="jabatan_post">
                                    <option value="" disabled="disabled" selected="selected">Pilih Jabatan
                                    </option>
                                    @foreach ($jabatanx as $jabatan)
                                        <option value="{{ $jabatan['id'] }}">{{ $jabatan['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pendidikan</label>
                                <select class="form-control" name="pendidikan_post" id="pendidikan_post">
                                    <option value="" disabled="disabled" selected="selected">Pilih Pendidikan
                                    </option>
                                    @foreach ($pendidikanx as $pendidikan)
                                        <option value="{{ $pendidikan['id'] }}">{{ $pendidikan['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pangkat</label>
                                <select class="form-control" name="pangkat_post" id="pangkat_post">
                                    <option value="" disabled="disabled" selected="selected">Pilih Pangkat
                                    </option>
                                    @foreach ($pangkatx as $pangkat)
                                        <option value="{{ $pangkat['id'] }}">{{ $pangkat['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Pengampu Mapel</label>
                                <select class="form-control" name="mapel_post" id="mapel_post">
                                    <option value="" disabled="disabled" selected="selected">Pilih Mata Pelajaran
                                    </option>
                                    @foreach ($mapelx as $mapel)
                                        <option value="{{ $mapel['id'] }}">{{ $mapel['nama'] }}</option>
                                    @endforeach
                                    <option value="0">Lainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir_post" autocomplete="off"
                                    class="form-control" placeholder="Tempat Lahir">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer05">Tanggal Lahir</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="tanggal_lahir_post"
                                        id="tanggal_lahir_post" data-date-format="yyyy-mm-dd"
                                        data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                    <span class="input-group-addon"><i
                                            class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer05">Terhitung Mulai Tanggal </label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="terhitung_mulai_tanggal_post"
                                        id="terhitung_mulai_tanggal_post" data-date-format="yyyy-mm-dd"
                                        data-plugin-options='{"autoclose": true}'
                                        value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                    <span class="input-group-addon"><i
                                            class="list-icon material-icons">date_range</i></span>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Masa Kerja</label>
                                <input type="text" name="masa_kerja_post" id="masa_kerja_post" autocomplete="off"
                                    class="form-control" placeholder="Masa Kerja">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Email</label>
                                <input type="text" name="email_post" id="email_post" autocomplete="off"
                                    class="form-control" placeholder="Email">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Jenis Kelamin</label>
                                <select class="form-control" name="jenkel_post" id="jenkel_post">
                                    <option value="" disabled="disabled" selected="selected">Pilih Jenis Kelamin
                                    </option>
                                    <option value="l">Laki-Laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Agama</label>
                                <input type="text" name="agama_post" id="agama_post" autocomplete="off"
                                    class="form-control" placeholder="Agama">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">Telepon</label>
                                <input type="tel" name="telepon_post" id="telepon_post" autocomplete="off"
                                    class="form-control" placeholder="telepon">
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationServer04">Alamat</label>
                                <textarea class="form-control" name="alamat_post" id="alamat_post" rows="3"></textarea>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">RT</label>
                                <input type="number" name="rt_post" id="rt_post" autocomplete="off" class="form-control"
                                    placeholder="RT">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationServer04">RW</label>
                                <input type="number" name="rw_post" id="rw_post" autocomplete="off" class="form-control"
                                    placeholder="RT">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Dusun</label>
                                <input type="text" name="dusun_post" id="dusun_post" autocomplete="off"
                                    class="form-control" placeholder="Dusun">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Kecamatan</label>
                                <input type="text" name="kecamatan_post" id="kecamatan_post" autocomplete="off"
                                    class="form-control" placeholder="Kecamatan">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Kode POS</label>
                                <input type="text" name="kodepos_post" id="kodepos_post" autocomplete="off"
                                    class="form-control" placeholder="Kode POS">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Password</label>
                                <div class="input-group">
                                    <input class="form-control password2" id="password_post" name="password_post"
                                        placeholder="Password" type="password"> <span class="input-group-addon">
                                        <a href="javascript:void(0)"><i
                                                class="material-icons list-icon eye2 text-dark">remove_red_eye</i></a>
                                    </span>
                                </div>
                                <a href="javascript:void(0)" class="btn btn-info mt-2 float-right "
                                    id="generate_pass">Generate</a>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Role</label>
                                <select class="form-control" name="role_post" id="role">
                                    <option value="" disabled="disabled" selected="selected">Pilih Role Login
                                    </option>
                                    <option value="guru">Guru</option>
                                    <option value="penilai">Penilai</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationServer04">Photo</label>
                                <div class="col-sm-12">
                                    <div class="form-group row mb-0">
                                        <label class="col-md-3 col-form-label" for="l1"></label>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <img id="modal-previewpost" src="https://via.placeholder.com/150"
                                                        alt="Preview" class="form-group mb-1" width="auto" height="auto"
                                                        style="margin-top: 10px">
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group row">
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input id="image" type="file" name="image_post"
                                                                    accept="image/*" onchange="readURL2(this);">
                                                                <input type="hidden" name="hidden_imagex"
                                                                    id="hidden_imagex">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="validationServer04">Informasi Lainnya </label>
                                <textarea class="form-control" name="informasi_lain_post" id="informasi_lain_post"
                                    rows="3"></textarea>
                            </div>

                            <div class="col-md-12 mb-3 roles-guru">
                                <label for="validationServer04">Jam Wajib Mengajar (24 – 40 jam tatap muka per minggu)
                                </label>
                                <input type="number" class="form-control" name="jam_wajib_mengajar_post"
                                    id="jam_wajib_mengajar_post" />
                            </div>
                            <div class="col-md-12 mb-3 roles-guru">
                                <label for="validationServer04">Jam Mengajar (tatap muka) guru di sekolah (24 – 40 jam tatap muka per minggu) </label>
                                <input type="number" class="form-control" name="jam_mengajar_post"
                                    id="jam_mengajar_post" />
                            </div>
                            <div class="col-md-12 mb-3 roles-guru">
                                <label for="validationServer04">Lama Mengajar </label>
                                <input type="number" class="form-control" name="lama_mengajar_post"
                                    id="lama_mengajar_post" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save " id="savebtn">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tempat Sampah </h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="table_admin_trashx" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Email</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_guru" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="fa fa-trash list-icon"></i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowTrash').modal('show');
                                openModalTrash();
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_master_data_gurupenilaipkg') }}",
                    columns: [{
                            "class": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nip',
                            name: 'nip'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'jenkel',
                            name: 'jenkel'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'role',
                            name: 'role'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_guru').dataTable(config);
                $('#table_guru tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).parents('tr');
                    var row = table_adminx.api().row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(detailTable(row.data())).show();
                        tr.addClass('shown');
                    }
                });

                window.detailTable = function detail(d) {
                    var kontak
                    if (d.telepon == null) {
                        kontak = '-';
                    } else {
                        kontak = d.telepon;
                    }
                    var data = `
                       <div class="box-title text-center"> Informasi Data Guru / Penilai </div>
                       <div class="row">
                         <div class="col-mo-4"><span class="ml-3">` + d.photo + `</span></div>
                         <div class="col-mo-4">
                            <div class="box-title text-center">Biodata </div>
                           <div class="table-responsive ml-3">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                               <tr>
                                  <td>Nama</td>
                                  <td>:</td>
                                  <td>` + d.nama + `</td>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td>` + d.email + `</td>
                                  <td>NIP</td>
                                  <td>:</td>
                                  <td>` + d.nip + `</td
                               </tr>
                               <tr>
                                <td>Telepon</td>
                                  <td>:</td>
                                  <td>` + d.telepon + `</td>
                                  <td>Alamat</td>
                                  <td>:</td>
                                  <td>` + d.alamat + `</td>
                               </tr>
                            </table>
                           </div>
                         </div>
                       </div>`;
                    return data;
                }

                //show
                $('body').on('click', '.show.penilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_guru_penilaipkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshow').modal('show');
                                    $('.text-muted.nama').html(rows.nama);
                                    $('.text-muted.nip').html(rows.nip);
                                    $('.text-muted.alamat').html(rows.alamat);
                                    $('.text-muted.email').html(rows.email);
                                    $('.text-muted.jenkel').html(rows.jenkel);
                                    $('.text-muted.tempat_lahir').html(rows.tempat_lahir);
                                    $('.text-muted.tanggal_lahir').html(rows.tgl_lahir);
                                    $('.text-muted.pangkat').html(rows.pangkat);
                                    $('.text-muted.pendidikan').html(rows.pendidikan);
                                    $('.text-muted.agama').html(rows.agama);
                                    $('.text-muted.nik').html(rows.nik);
                                    $('.text-muted.nuptk').html(rows.nuptk);
                                    if (rows.file != '') {
                                        var filex = rows.file;
                                        var photo = '<img src="' + filex +
                                            '" class="rounded-circle img-thumbnail">';
                                        $('.text-muted.photo').html(photo);
                                    }
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.penilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_guru_penilaipkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_guru"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="email"]').val(rows.email);
                                    $('input[name="telepon"]').val(rows.telepon);
                                    $('input[name="nik"]').val(rows.nik);
                                    $('input[name="nip"]').val(rows.nip);
                                    $('input[name="nuptk"]').val(rows.nuptk);
                                    $('input[name="dusun"]').val(rows.dusun);
                                    $('input[name="rt"]').val(rows.rt);
                                    $('input[name="rw"]').val(rows.rw);
                                    $('input[name="kecamatan"]').val(rows.kecamatan);
                                    $('textarea[name="alamat"]').val(rows.alamat);
                                    $('input[name="agama"]').val(rows.agama);
                                    $('input[name="tempat_lahir"]').val(rows.tempat_lahir);
                                    $('input[name="tanggal_lahir"]').val(rows.tgl_lahir);
                                    $('input[name="terhitung_mulai_tanggal"]').val(rows
                                        .terhitung_mulai_tanggal);
                                    $('input[name="masa_kerja"]').val(rows.masa_kerja);

                                    $('textarea[name="informasi_lain"]').val(rows
                                        .informasi_lain);
                                    if (rows.file != '') {
                                        var filex = rows.file;
                                        $('#modal-previewedit').removeAttr('src');
                                        $('#modal-previewedit').attr('src', filex);
                                    }
                                    $("select[name='jenkel'] > option[value=" + rows
                                        .jenkel_kode + "]").prop("selected", true);

                                    $("select[name='role'] > option[value=" + rows
                                        .role + "]").prop("selected", true);

                                    $("select[name='jabatan'] > option[value=" + rows
                                        .id_jabatan + "]").prop("selected", true);
                                    $("select[name='pangkat'] > option[value=" + rows
                                        .id_pangkat + "]").prop("selected", true);
                                    $("select[name='pendidikan'] > option[value=" + rows
                                        .id_pendidikan + "]").prop("selected", true);
                                    $("select[name='mapel'] > option[value=" + rows
                                        .id_mapel + "]").prop("selected", true);
                                    $('input[name="lama_mengajar"]').val(rows.lama_mengajar);
                                    $('input[name="jam_mengajar"]').val(rows.jam_mengajar);
                                    $('input[name="jam_wajib_mengajar"]').val(rows.jam_wajib_mengajar);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });
                });

                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('update_master_guru_penilaipkg') }}';
                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('post_master_guru_penilaipkg') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowPost').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove
                $('body').on('click', '.remove.penilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_master_guru_penilaipkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //restore
                $('body').on('click', '.restore.penilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore_master_guru_penilaipkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Kembalikan Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restore(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //restore
                $('body').on('click', '.remove-permanent.penilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_force_master_guru_penilaipkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin Menghapus data ini secara permanent!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove_permanent(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                $('.modal').on('hidden.bs.modal', function() {
                    $(this).find('form')[0].reset();
                });

                $('body').on('change', 'select[name="role_post"]', function() {
                    var role = $(this).val();
                    if (role == 'guru') {
                        $('.roles-guru').show();
                    } else {
                        $('.roles-guru').hide();
                    }
                });

                $('body').on('change', 'select[name="role"]', function() {
                    var role = $(this).val();
                    if (role == 'guru') {
                        $('.roles-guru-edit').show();
                    } else {
                        $('.roles-guru-edit').hide();
                    }
                });

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-previewedit';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewedit').removeClass('hidden');

                    }
                };

                //read file image upload
                window.readURL2 = function name(input, id) {
                    id = id || '#modal-previewpost';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewepost').removeClass('hidden');

                    }
                };

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                const mata2 = document.querySelector(".eye2")
                const inputPass2 = document.querySelector(".password2");
                if (mata2) {
                    mata2.addEventListener("click", () => {
                        mata2.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass2.type === "password") {
                            inputPass2.setAttribute("type", "text")

                        } else if (inputPass2.type === "text") {
                            inputPass2.setAttribute("type", "password")
                            mata2.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    $('input[name="password"]').val(retVal);
                    $('input[name="password_post"]').val(retVal);
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

                //function remove
                window.remove_permanent = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function restore
                window.restore = function remove(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                window.openModalTrash = function modaltrash() {
                    config_trash = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: '{{ route('ajax_trash_data_gurupenilaipkg') }}',
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'email',
                                name: 'email'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_trash = $('#table_admin_trashx').dataTable(config_trash);
                }

            });
        })(jQuery, window);
    </script>
@endsection
