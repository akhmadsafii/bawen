@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Cetak </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap </p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header clearfix ">
                        <p class="text-info">Silakan Gunakan Fitur Print Untuk mulai mencetak Rekap.
                            <button type="button" id="printdata"><i class="fa fa-print"></i></button>
                        </p>
                    </div>
                    <div class="widget-body clearfix">
                        <center>
                            <div class="table-responsive col-md-12">
                                <div class="btn-group" style="flex-wrap: wrap;">
                                    <a href="{{ route('print_lampiran_pkg', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 btn-info">Cover</a>
                                    @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                        <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                            class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2"
                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                    @endforeach
                                    <a href="{{ route('print_lampiran1b',['id_guru'=>$id_guru,'id_penilai'=>$id_penilai,'periode'=>$periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 B </a>
                                    <a href="{{ route('print_lampiran1c', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 C </a>
                                    <a href="{{ route('print_lampiran1d', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}" class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 D </a>
                                </div>
                            </div>
                        </center>
                        <div id="printable">
                            <center>
                                <table witdh="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <center>
                                                    <h1>LAMPIRAN 2</h1>
                                                </center>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <center>
                                                    <h1>INSTRUMEN</h1>
                                                </center>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <center>
                                                    <h1>PK GURU KELAS/MATA PELAJARAN </h1>
                                                </center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $('body').on('click', '#printdata', function() {
                    var printContents = document.getElementById('printable').innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    //window.stateX();
                    document.body.innerHTML = originalContents
                });

                window.stateX = function CheckWindowState() {
                    if (document.readyState == "complete") {
                        window.location.reload();
                    } else {
                        setTimeout("CheckWindowState()", 1000)
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
