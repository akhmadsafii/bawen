@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5"> List Form Survei Penilaian Kinerja Guru </h5>
        </div>
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="col-md-12 mb-2">
                            <div class="card card-outline-color-scheme">
                                <div class="card-header">
                                    <h5 class="card-title mt-0 mb-3">Daftar Tugas </h5>
                                    <p class="card-subtitle"> Anda Diwajibkan menilai guru-guru dibawah</p>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="table_guru" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Photo</th>
                                                    <th>Nama</th>
                                                    <th>NIP</th>
                                                    <th>Periode</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Photo</th>
                                                    <th>Nama</th>
                                                    <th>NIP</th>
                                                    <th>Periode</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                var table_adminx, table_trash;
                var config, config_trash;
                var urlx =
                    "{{ route('ajax_task_penilaian_guru', ['id' => session('id'), 'tahun' => session('tahun')]) }}"
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: urlx,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'photo',
                            name: 'photo'
                        },
                        {
                            data: 'guru',
                            name: 'guru'
                        },
                        {
                            data: 'nip',
                            name: 'nip'
                        },
                        {
                            data: 'periode',
                            name: 'periode'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_guru').dataTable(config);
            });
        })(jQuery, window);
    </script>
@endsection
