@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">Rekap Survei Penilaian {{ $lampiran['nama_guru'] }} -
                {{ $lampiran['nip_guru'] }} </p>
            <p> , Penilai : {{ $lampiran['nama_penilai'] }} - {{ $lampiran['nip_penilai'] }}</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Penilaian Kinerja Guru</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <!-- /.row -->
        <div class="row">
            <!-- Tabs Bordered -->
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header clearfix ">
                        <p class="text-info">Silakan Gunakan Fitur Print Untuk mulai mencetak Rekap.
                            <button type="button" id="printdata"><i class="fa fa-print"></i></button>
                        </p>
                    </div>
                    <div class="widget-body clearfix">
                        <center>
                            <div class="table-responsive col-md-12">
                                <div class="btn-group" style="flex-wrap: wrap;">
                                    <a href="{{ route('print_lampiran_pkg', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Cover</a>
                                    @foreach ($kompetensi_penilaian as $kompetensi_penilaian)
                                        <a href="{{ route('print_lampiranpkg', ['id_guru' => $id_guru,'id_penilai' => $id_penilai,'periode' => $periode,'id_kompetensi' => $kompetensi_penilaian['id']]) }}"
                                            class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2"
                                            data-id="{{ $kompetensi_penilaian['id'] }}">{{ str_replace('Penilaian untuk Kompetensi', '', $kompetensi_penilaian['nama']) }}</a>
                                    @endforeach
                                    <a href="{{ route('print_lampiran1b', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 B </a>
                                    <a href="{{ route('print_lampiran1c', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2 btn-info">Lampiran
                                        1 C </a>
                                    <a href="{{ route('print_lampiran1d', ['id_guru' => $id_guru, 'id_penilai' => $id_penilai, 'periode' => $periode]) }}"
                                        class="btn btn-outline-default kompetensi-penilaian mt-2 ml-2 mb-2">Lampiran
                                        1 D </a>
                                </div>
                            </div>
                        </center>
                        <div id="printable">
                            <style type="text/css">
                                @media print {
                                    .pagebreak {
                                        clear: both;
                                        page-break-after: always;
                                    }

                                    @page {
                                        size: legal portrait;
                                        margin: 10mm;
                                    }
                                }

                            </style>
                            <right>
                                <table width="20%" border="1">
                                    <tbody>
                                        <tr>
                                            <td><b>
                                                    <center>FORMAT 1C
                                                </b></center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </right>
                            <center>
                                <h6>
                                    <b>REKAP HASIL PENILAIAN KINERGA GURU KELAS/MATA PELAJARAN
                                    </b>
                                </h6>
                            </center>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>A.</td>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['nama_guru']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['nip_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Tempat/Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['tempat_lahir'] }} ,
                                            {{ \Carbon\Carbon::parse($lampiran['tgl_lahir'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pangkat/Jabatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pangkat_guru'] }} / {{ $lampiran['jabatan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>TMT Sebagai guru </td>
                                        <td>:</td>
                                        <td>{{ \Carbon\Carbon::parse($lampiran['terhitung_mulai_tanggal'])->isoFormat('dddd, D MMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Masa Kerja </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['masa_kerja_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Jenis Kelamin </td>
                                        <td>:</td>
                                        <td>
                                            @if (!empty($lampiran['jenkel_guru']))
                                                @switch($lampiran['jenkel_guru'])
                                                    @case('l')
                                                        Laki-Laki
                                                    @break
                                                    @case('p')
                                                        Perempuan
                                                    @break
                                                    @default
                                                @endswitch
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pendidikan Terakhir / Spesialis </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['pendidikan_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Program Keahlian yang diampu </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['mapel_guru'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>B.</td>
                                        <td>Nama Sekolah</td>
                                        <td>:</td>
                                        <td>{{ Str::ucfirst($lampiran['sekolah']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Desa / Kelurahan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kelurahan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kecamatan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Kabupaten / Kota </td>
                                        <td>:</td>
                                        <td>{{ $lampiran['kabupaten'] }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Provinsi</td>
                                        <td>:</td>
                                        <td>{{ $lampiran['provinsi'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table width="100%" border="1">
                                <thead>
                                    <tr>
                                        <td>
                                            <center>PERIODE PENILAIAN</center>
                                        </td>
                                        <td>Formatif</td>
                                        <td></td>
                                        <td>
                                            <center>Tahun</center>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <center>
                                                {{ \Carbon\Carbon::parse($lampiran['mulai_penilaian'])->isoFormat('dddd, D MMM Y') }}
                                                S/D
                                                {{ \Carbon\Carbon::parse($lampiran['akhir_penilaian'])->isoFormat('dddd, D MMM Y') }}
                                            </center>
                                        </td>
                                        <td>Sumatif</td>
                                        <td>
                                            <center>X</center>
                                        </td>
                                        <td rowspan="2">
                                            <center>{{ $lampiran['periode'] }}</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kemajuan</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table width="100%" border="1">
                                <thead>
                                    <tr align="center">
                                        <td>No</td>
                                        <td>Kompetensi</td>
                                        <td>Nilai</td>
                                    </tr>
                                </thead>
                                @php
                                    $total_skor = 0;
                                @endphp
                                <tbody>
                                    @foreach ($list_indikator as $indikator)
                                        <tr>
                                            <td align="center">{{ $indikator['kode'] }}</td>
                                            <td>{{ $indikator['nama'] }}</td>
                                            <td></td>
                                        </tr>
                                        @foreach ($indikator['indikator'] as $list)
                                            @php
                                                $total_skor += intval($list['nilai']);
                                            @endphp
                                            <tr>
                                                <td></td>
                                                <td> {{ $loop->iteration }}. {{ $list['nama'] }}</td>
                                                <td align="center">{{ intval($list['nilai']) }}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    <tr>
                                        <td colspan="2">
                                            <center><b>Jumlah (Hasil penilaian kinerja guru)
                                                </b></center>
                                        </td>
                                        <td>
                                            <center>{{ $total_skor }} </center>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="text-info">Nilai Kompetensi berdasarkan laporan hasil dan evaluasi PK Guru. Nilai min = 1  Nilai max = 4  </p>
                            <br>
                            <table width="100%">
                                <tbody>
                                    <tr align="center">
                                        <td>Guru yang dinilai</td>
                                        <td>Penilai</td>
                                        <td>Kepala Sekolah</td>
                                    </tr>
                                    <tr align="center">
                                        <td style="height:100px;"></td>
                                        <td style="height:100px;"></td>
                                        <td style="height:100px;"></td>
                                    </tr>
                                    <tr align="center">
                                        <td><b><u>{{ Str::ucfirst($lampiran['nama_guru']) }}</u></b></td>
                                        <td><b><u>{{ Str::ucfirst($lampiran['nama_penilai']) }}</u></b></td>
                                        <td><b><u>{{ Str::ucfirst($lampiran['kepala_sekolah']) }} </u></b></td>
                                    </tr>
                                    <tr align="center">
                                        <td>NIP : {{ $lampiran['nip_guru'] }}</td>
                                        <td>NIP : {{ $lampiran['nip_penilai'] }}</td>
                                        <td>NIP : {{ $lampiran['nip_kepsek'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $('body').on('click', '#printdata', function() {
                    var printContents = document.getElementById('printable').innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    //window.stateX();
                    document.body.innerHTML = originalContents
                });

                window.stateX = function CheckWindowState() {
                    if (document.readyState == "complete") {
                        window.location.reload();
                    } else {
                        setTimeout("CheckWindowState()", 1000)
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
