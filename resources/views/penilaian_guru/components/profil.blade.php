@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">My Profile</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Profile</li>
                </li>
                <li class="breadcrumb-item"><a href="#ubahpassword">Ubah Password</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-danger text-center">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong class="text-info">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="row">
                        <div class="col-md-6 mb-2" id="profil">
                            <div class="card card-outline-color-scheme">
                                <div class="card-header">
                                    <h5 class="card-title mt-0 mb-3"> <i class="material-icons list-icon">account_circle</i>
                                        Profil </h5>
                                </div>
                                <div class="card-body">
                                    <form id="ProfilForm" name="ProfilForm" method="POST"
                                        class="form-horizontal card-text ml-2 mr-2"
                                        action="{{ route('update_profil_pkg') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <p class="mr-b-0">Nama <span class="text-red">*</span></p>
                                            <div class="input-group">
                                                <input id="nama" type="text" name="nama" placeholder="Agus Budiman"
                                                    class="form-control" value="{{ $profil_data['nama'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="mr-b-0">Alamat <span class="text-red">*</span>
                                            </p>
                                            <div class="input-group">
                                                <textarea name="alamat" id="alamat"
                                                    placeholder="Jl.Sukorejo-Parakan Km 3 Kabupaten Kendal , Jawa Tengah"
                                                    class="form-control">{{ $profil_data['alamat'] ?? '-' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="mr-b-0">Kota <span class="text-red">*</span></p>
                                            <div class="input-group">
                                                <input id="kota" type="text" name="kota" placeholder="Semarang"
                                                    class="form-control"
                                                    value="{{ $profil_data['tempat_lahir'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="mr-b-0">Telepon <span class="text-red">*</span>
                                            </p>
                                            <div class="input-group">
                                                <input id="telephone" type="number" name="telephone"
                                                    placeholder="0824207872613" title="format harus berisi angka saja"
                                                    pattern="^\d{13}$" maxlength="13" class="form-control"
                                                    value="{{ $profil_data['telepon'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="mr-b-0">Email <span class="text-red">*</span>
                                            </p>
                                            <div class="input-group">
                                                <input id="email" type="email" name="email" placeholder="admin@yahoo.co.id"
                                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"
                                                    value="{{ $profil_data['email'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="mr-b-0">Foto / Avatar</p>
                                            <div class="input-group">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                                @isset($profil_data['file'])
                                                    <img id="modal-preview2" src="{{ $profil_data['file'] }}" alt="Preview"
                                                        class="form-group mb-1" width="50px" height="50px">
                                                @endisset
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                            <p class="text-muted ml-2">Kosongkon form ini jika tidak ada perubahan
                                                gambar
                                            </p>
                                        </div>
                                        <div class="form-group mt-2">
                                            <div class="row">
                                                <div class="col-sm-12 btn-list">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="material-icons list-icon">save</i>
                                                        Simpan
                                                    </button>
                                                </div>
                                                <!-- /.col-sm-12 -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-2" id="ubahpassword">
                            <div class="card card-outline-color-scheme">
                                <div class="card-header">
                                    <h5 class="card-title mt-0 mb-3"> <i class="material-icons list-icon">vpn_key</i> Ubah
                                        Password</h5>
                                </div>
                                <div class="card-body">
                                    <form id="PasswordForm" name="PasswordForm" method="POST" class="form-horizontal"
                                        enctype="multipart/form-data" action="{{ route('update-password-pkg') }}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12 widget-holder">
                                                <div class="widget-bg">
                                                    <div class="widget-body">
                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Username</p>
                                                            <div class="input-group">
                                                                <input id="username" type="text" name="username"
                                                                    readonly="readonly" class="form-control"
                                                                    value="{{ session('username') }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Password Lama <span
                                                                    class="text-red">*</span></p>
                                                            <div class="input-group">
                                                                <input id="password_lama" type="password"
                                                                    name="password_lama" class="form-control password"
                                                                    placeholder="xxxx">
                                                                <div class="input-group-addon toggle-password">
                                                                    <a href="javascript:void(0)"><i
                                                                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Password Baru <span
                                                                    class="text-red">*</span></p>
                                                            <div class="input-group">
                                                                <input id="password_baru" type="password"
                                                                    name="password_baru" class="form-control password2"
                                                                    placeholder="xxxx"
                                                                    title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                                                <div class="input-group-addon toggle-password">
                                                                    <a href="javascript:void(0)"><i
                                                                            class="material-icons list-icon eye2 text-dark">remove_red_eye</i></a>
                                                                </div>
                                                                <div class="input-group-addon">
                                                                    <a href="javascript:void(0)" id="generate_pass">Generate
                                                                        Password</a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Konfirm Password Baru <span
                                                                    class="text-red">*</span></p>
                                                            <div class="input-group">
                                                                <input id="password_confirm_baru" type="password"
                                                                    name="password_confirm_baru"
                                                                    class="form-control password3" placeholder="xxxx"
                                                                    title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                                            </div>
                                                        </div>

                                                        <div id="message" style="display: none;">
                                                            <h4>Password must contain the following:</h4>
                                                            <p id="letter" class="invalid">A
                                                                <b>lowercase</b> letter
                                                            </p>
                                                            <p id="capital" class="invalid">A <b>capital
                                                                    (uppercase)</b> letter</p>
                                                            <p id="number" class="invalid">A <b>number</b>
                                                            </p>
                                                            <p id="length" class="invalid">Minimum <b>8
                                                                    characters</b></p>
                                                        </div>

                                                    </div>
                                                    <!-- /.widget-body -->
                                                </div>
                                                <!-- /.widget-bg -->
                                            </div>
                                        </div>
                                        <div class="form-actions text-center">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 btn-list">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="material-icons list-icon">save</i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                    <!-- /.col-sm-12 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                var myInput = document.getElementById("password_baru");
                var letter = document.getElementById("letter");
                var capital = document.getElementById("capital");
                var number = document.getElementById("number");
                var length = document.getElementById("length");

                // When the user clicks on the password field, show the message box
                myInput.onfocus = function() {
                    document.getElementById("message").style.display = "block";
                }

                // When the user clicks outside of the password field, hide the message box
                myInput.onblur = function() {
                    document.getElementById("message").style.display = "none";
                }

                // When the user starts to type something inside the password field
                myInput.onkeyup = function() {
                    // Validate lowercase letters
                    var lowerCaseLetters = /[a-z]/g;
                    if (myInput.value.match(lowerCaseLetters)) {
                        letter.classList.remove("invalid");
                        letter.classList.add("valid");
                    } else {
                        letter.classList.remove("valid");
                        letter.classList.add("invalid");
                    }

                    // Validate capital letters
                    var upperCaseLetters = /[A-Z]/g;
                    if (myInput.value.match(upperCaseLetters)) {
                        capital.classList.remove("invalid");
                        capital.classList.add("valid");
                    } else {
                        capital.classList.remove("valid");
                        capital.classList.add("invalid");
                    }

                    // Validate numbers
                    var numbers = /[0-9]/g;
                    if (myInput.value.match(numbers)) {
                        number.classList.remove("invalid");
                        number.classList.add("valid");
                    } else {
                        number.classList.remove("valid");
                        number.classList.add("invalid");
                    }

                    // Validate length
                    if (myInput.value.length >= 8) {
                        length.classList.remove("invalid");
                        length.classList.add("valid");
                    } else {
                        length.classList.remove("valid");
                        length.classList.add("invalid");
                    }
                }

                const mata = document.querySelector(".eye")
                const mata2 = document.querySelector(".eye2")
                const inputPass = document.querySelector(".password");
                const inputPassBaru = document.querySelector(".password2");
                const inputPassConfirm = document.querySelector(".password3");

                mata.addEventListener("click", () => {
                    mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPass.type === "password") {
                        inputPass.setAttribute("type", "text")

                    } else if (inputPass.type === "text") {
                        inputPass.setAttribute("type", "password")
                        mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                })

                mata2.addEventListener("click", () => {
                    mata2.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPassBaru.type === "password" && inputPassConfirm.type === "password") {
                        inputPassBaru.setAttribute("type", "text"),
                            inputPassConfirm.setAttribute("type", "text")
                    } else if (inputPassBaru.type === "text" && inputPassConfirm.type === "text") {
                        inputPassBaru.setAttribute("type", "password"),
                            inputPassConfirm.setAttribute("type", "password")
                        mata2.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                })

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPassBaru.value = retVal;
                    inputPassConfirm.value = retVal;
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

            });
        })(jQuery, window);
    </script>
@endsection
