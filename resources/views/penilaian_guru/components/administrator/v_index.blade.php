@extends('penilaian_guru.template.apps')
@section('penilaian_guru.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Master Data </h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">Administrator</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Administrator</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshow" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Informasi </h5>
                </div>
                <div class="modal-body">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td class="text-muted nama"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td class="text-muted email"></td>
                            </tr>
                            <tr>
                                <td>Telepon</td>
                                <td class="text-muted telepon"></td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td class="text-muted username"></td>
                            </tr>
                            <tr>
                                <td>Photo</td>
                                <td class="text-muted photo"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tempat Sampah </h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="table_admin_trashx" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Email</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Admin Informasi </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_admin" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama" id="nama" autocomplete="off" class="form-control"
                                            pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="text-muted">
                                        <input type="email" name="email" id="email" autocomplete="off"
                                            class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                            title="Format email salah " placeholder="email">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telepon</td>
                                    <td class="text-muted ">
                                        <input type="tel" name="telepon" id="telepon" autocomplete="off"
                                            class="form-control" placeholder="telepon">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin </td>
                                    <td class="text-muted ">
                                        <select class="form-control" name="jenkel" id="jenkel">
                                            <option value="" disabled="disabled" selected="selected">Pilih Jenis Kelamin</option>
                                            <option value="l">Laki -laki </option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td class="text-muted ">
                                        <input type="text" name="username" id="username" autocomplete="off"
                                            class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td class="text-muted ">
                                        <div class="input-group">
                                            <input class="form-control password" id="password" name="password"
                                                placeholder="Password" type="password"> <span class="input-group-addon">
                                                <a href="javascript:void(0)"><i
                                                        class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-info mt-2 float-right "
                                            id="generate_pass">Generate</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td class="text-muted">
                                        <div class="col-sm-12">
                                            <div class="form-group row mb-0">
                                                <label class="col-md-3 col-form-label" for="l1"></label>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                                            </label>
                                                            <img id="modal-previewedit"
                                                                src="https://via.placeholder.com/150" alt="Preview"
                                                                class="form-group mb-1" width="auto" height="auto"
                                                                style="margin-top: 10px">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input id="image" type="file" name="image"
                                                                            accept="image/*" onchange="readURL(this);">
                                                                        <input type="hidden" name="hidden_imagex"
                                                                            id="hidden_imagex">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">New Admin Informasi </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_post" id="nama_post" autocomplete="off"
                                            class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama"
                                            required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="text-muted">
                                        <input type="email" name="email_post" id="email_post" autocomplete="off"
                                            class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                            title="Format email salah " placeholder="email" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telepon</td>
                                    <td class="text-muted ">
                                        <input type="tel" name="telepon_post" id="telepon_post" autocomplete="off"
                                            class="form-control" placeholder="telepon" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin </td>
                                    <td class="text-muted ">
                                        <select class="form-control" name="jenkel_post" id="jenkel_post">
                                            <option value="" disabled="disabled" selected="selected">Pilih Jenis Kelamin</option>
                                            <option value="l">Laki -laki </option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td class="text-muted ">
                                        <input type="text" name="username_post" id="username_post" autocomplete="off"
                                            class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                            title="Format harus berisi alfanumerik atau huruf saja " placeholder="username"
                                            required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td class="text-muted ">
                                        <div class="input-group">
                                            <input class="form-control password2" id="password_post" name="password_post"
                                                placeholder="Password" type="password" required> <span
                                                class="input-group-addon">
                                                <a href="javascript:void(0)"><i
                                                        class="material-icons list-icon eye2 text-dark">remove_red_eye</i></a>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-info mt-2 float-right "
                                            id="generate_pass">Generate</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td class="text-muted">
                                        <div class="col-sm-12">
                                            <div class="form-group row mb-0">
                                                <label class="col-md-3 col-form-label" for="l1"></label>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img id="modal-previewpost"
                                                                src="https://via.placeholder.com/150" alt="Preview"
                                                                class="form-group mb-1" width="auto" height="auto"
                                                                style="margin-top: 10px">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input id="image_post" type="file" name="image_post"
                                                                            accept="image/*" onchange="readURL2(this);">
                                                                        <input type="hidden" name="hidden_image"
                                                                            id="hidden_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save " id="savebtn">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_admin" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Telepon</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Telepon</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="fa fa-trash list-icon"></i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowTrash').modal('show');
                                openModalTrash();
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_adminpkg') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'telepon',
                            name: 'telepon',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_admin').dataTable(config);
                $('body').on('click', '.show.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_adminpkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshow').modal('show');
                                    $('.text-muted.nama').html(rows.nama);
                                    $('.text-muted.email').html(rows.email);
                                    $('.text-muted.telepon').html(rows.telepon);
                                    $('.text-muted.username').html(rows.username);
                                    if (rows.file != '') {
                                        var filex = rows.file;
                                        var photo = '<img src="' + filex +
                                            '" class="rounded-circle img-thumbnail" width="30%">';
                                        $('.text-muted.photo').html(photo);
                                    }
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_adminpkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_admin"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="email"]').val(rows.email);
                                    $('input[name="telepon"]').val(rows.telepon);
                                    $('input[name="username"]').val(rows.username);
                                    $("select[name='jenkel'] > option[value=" + rows
                                        .jenkel_kode + "]").prop("selected", true);
                                    if (rows.file != '') {
                                        var filex = rows.file;
                                        $('#modal-previewedit').removeAttr('src');
                                        $('#modal-previewedit').attr('src', filex);
                                    }
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                //remove
                $('body').on('click', '.remove.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy_master_admin_pkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_admin"]').val();
                    var urlx = '{{ route('update_master_adminpkg', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('post_master_adminpkg') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowPost').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //restore
                $('body').on('click', '.restore.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore_master_admin_pkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Kembalikan Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restore(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //restore
                $('body').on('click', '.remove-permanent.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_force_master_admin_pkg', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin Menghapus data ini secara permanent!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove_permanent(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-previewedit';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewedit').removeClass('hidden');

                    }
                };

                //read file image upload
                window.readURL2 = function name(input, id) {
                    id = id || '#modal-previewpost';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewepost').removeClass('hidden');

                    }
                };

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                const mata2 = document.querySelector(".eye2")
                const inputPass2 = document.querySelector(".password2");
                if (mata2) {
                    mata2.addEventListener("click", () => {
                        mata2.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass2.type === "password") {
                            inputPass2.setAttribute("type", "text")

                        } else if (inputPass2.type === "text") {
                            inputPass2.setAttribute("type", "password")
                            mata2.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    $('input[name="password"]').val(retVal);
                    $('input[name="password_post"]').val(retVal);
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

                //function remove
                window.remove_permanent = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function restore
                window.restore = function remove(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                window.openModalTrash = function modaltrash() {
                    config_trash = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: '{{ route('ajaxtrashadminpkg') }}',
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'email',
                                name: 'email'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_trash = $('#table_admin_trashx').dataTable(config_trash);
                }

            });
        })(jQuery, window);
    </script>
@endsection
