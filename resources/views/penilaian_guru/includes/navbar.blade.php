<nav class="navbar">
    <!-- Logo Area -->
    <div class="navbar-header">
        <a href="{{ route('dashboard-pkg') }}" class="navbar-brand text-center">
            <img class="logo-expand" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                style="max-width: 25%;">
            <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                style="max-width: 80%;">
        </a>
    </div>
    <!-- /.navbar-header -->
    <!-- Left Menu & Sidebar Toggle -->
    <ul class="nav navbar-nav">
        <li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i
                    class="material-icons list-icon">menu</i></a>
        </li>
    </ul>
    <!-- /.navbar-left -->
    <div class="navbar-search d-none d-md-block" style="width: 15.28571em;">
        <div class="box-title ml-3 mt-4 text-white">Penilaian Kinerja Guru </div>
    </div>
    <div class="spacer"></div>
    <!-- Right Menu -->
    <ul class="nav navbar-nav d-none d-lg-flex">

    </ul>
    <!-- /.navbar-right -->
    <!-- User Image with Dropdown -->
    <ul class="nav navbar-nav">
        <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown">
                <span class="mr-3">{{ str_replace('-pkg', ' ', ucfirst(session('role'))) }}</span>
                <span class="avatar thumb-sm"><img src="{{ session('avatar') ?? '' }}" class="rounded-circle mr-2"
                        alt=""> <i class="material-icons list-icon">expand_more</i></span></a>
            <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-wide">
                <div class="card">
                    <header class="card-heading-extra">
                        <div class="row">
                            <div class="col-8">
                                <h3 class="mr-b-10 sub-heading-font-family fw-300">
                                    {{ Str::ucfirst(session('username')) }}</h3><span
                                    class="user--online">Available <i
                                        class="material-icons list-icon">expand_more</i></span>
                            </div>
                            <div class="col-4 d-flex justify-content-end">
                                <a href="{{ route('profilUserpkg') }}#profil" class="mr-t-10"><i
                                        class="material-icons list-icon">account_circle</i>
                                  </a>
                                <a href="{{ route('profilUserpkg') }}#ubahpassword" class="mr-t-10">
                                    <i
                                        class="material-icons list-icon">vpn_key</i>
                                 </a>
                                <a href="{{ route('auth.logout') }}" class="mr-t-10"><i
                                        class="material-icons list-icon">power_settings_new</i>
                                </a>
                            </div>
                            <!-- /.col-4 -->
                        </div>
                        <!-- /.row -->
                    </header>
                </div>
        </li>
    </ul>
    <!-- /.navbar-right -->
</nav>
