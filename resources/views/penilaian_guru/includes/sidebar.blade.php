<!-- SIDEBAR -->
<aside class="site-sidebar scrollbar-enabled clearfix">
    <!-- User Details -->
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <img src="{{ session('avatar') ?? '' }}" class="media-object rounded-circle" alt="">
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ Str::ucfirst(session('username')) }}</h4></span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <!-- /.side-user -->
    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            <li><a href="{{ route('dashboard-pkg') }}"><i
                class="list-icon material-icons">dashboard</i> <span class="hide-menu">Dashboard</a></li>
            @switch(session('role'))
                @case('admin-pkg')
                    <li class="current-page menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                                class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Master
                                Data</a>
                        <ul class="list-unstyled sub-menu">
                            <li><a href="{{ route('index_admin_pkg') }}">Administrator</a>
                            </li>
                            <li><a href="{{ route('index_jabatanpkg') }}">Jabatan</a>
                            </li>
                            <li><a href="{{ route('index_pendidikanpkg') }}">Pendidikan</a>
                            </li>
                            <li><a href="{{ route('index_pangkatpkg') }}">Pangkat</a>
                            </li>
                            <li><a href="{{ route('index_guru_penilai') }}">Guru / Penilai</a>
                            </li>
                        </ul>
                    </li>
                    <li class="current-page menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                                class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Master
                                Kompetensi</a>
                        <ul class="list-unstyled sub-menu">
                            <li class="menu-item-has-children"><a href="javascript:void(0);">Penilaian</a>
                                <ul class="list-unstyled sub-menu">
                                    <li><a href="{{ route('index_kompetensi') }}">Kompetensi Penilaian</a>
                                    </li>
                                    <li><a href="{{ route('index_indikator_kompetensi') }}">Indikator Penilaian</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children"><a href="javascript:void(0);">Sikap</a>
                                <ul class="list-unstyled sub-menu">
                                    <li><a href="{{ route('index_kompetensi_sikap') }}">Kompetensi Sikap</a>
                                    </li>
                                    <li><a href="{{ route('index_indikator_kompetensiSikap') }}">Indikator Sikap</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children"><a href="javascript:void(0);">Format C</a>
                                <ul class="list-unstyled sub-menu">
                                    <li><a href="{{ route('index_kompetensi_lampiranC') }}">Kompetensi Format C</a>
                                    </li>
                                    <li><a href="{{ route('index_indikator_kompetensiFormatC') }}">Indikator Format C</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>

                    <li class="current-page"><a href="{{ route('report_pkg') }}"><i
                        class="list-icon material-icons">report</i> <span class="hide-menu">Histori Penilaian </a></li>

                    <li class="current-page menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                                class="list-icon material-icons">perm_data_setting</i> <span
                                class="hide-menu">Pengaturan</a>
                        <ul class="list-unstyled sub-menu">
                            <li><a href="{{ route('setting_pkg') }}">
                                Data Instansi </a>
                            </li>
                            <li><a href="{{ route('index_settingGuruPenilai') }}">Guru / Penilai </a>
                            </li>
                        </ul>
                    </li>
                @break
                @case('guru-pkg')
                    <li class="current-page"><a href="{{ route('rekap_rata_rata_surveipkg') }}"><i
                        class="list-icon material-icons">report</i> <span class="hide-menu"> Rekap Hasil Survei </a></li>
                @break
                @case('penilai-pkg')
                    <li class="current-page"><a href="{{ route('list_form_survei') }}"><i class="material-icons">list</i>Task Survei</a>
                    </li>
                @break
                @default
            @endswitch
        </ul>
        <!-- /.side-menu -->
    </nav>
    <!-- /.sidebar-nav -->
</aside>
