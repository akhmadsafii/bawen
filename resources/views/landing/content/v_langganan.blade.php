@extends('landing.layout.main')
@section('content')
    <style>
        section.hero {
            color: #fff;
            background-size: cover !important;
        }

        button.swal2-confirm.btn.btn-success {
            margin-right: 6px;
        }

        a p.text-hero {
            font-size: 1.2em;
            font-weight: 300;
        }

        section {
            padding: 150px 0;
        }

        .align-items-center {
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        p.small-text-hero {
            font-size: 1em;
        }

        .text-primary {
            color: #4dcb44 !important;
        }

        .search-bar {
            border-radius: 100px;
            background: #fff;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
            padding: 5px;
            margin-top: 30px;
            padding-left: 30px;
        }

        group {
            margin-bottom: 0;
            position: relative;
        }

        .search-bar input,
        .search-bar .bootstrap-select {
            background: none;
            border: none;
            padding: 20px 0 15px;
            width: 100%;
        }

        .form-group {
            margin-bottom: 0;
        }

        .search-bar .rightLine::after {
            content: '';
            display: block;
            height: 60%;
            width: 1px;
            background: #ddd;
            position: absolute;
            top: 20%;
            right: 0;
        }

        .search-bar input,
        .search-bar .bootstrap-select {
            background: none;
            border: none;
            padding: 20px 0 15px;
            width: 100%;
        }

        .search-bar .form-group label {
            position: absolute;
            top: 50%;
            right: 15px;
            cursor: pointer;
            transform: translateY(-50%);
            color: #aaa;
            font-size: 0.8em;
        }

        label {
            display: inline-block;
            margin-top: .5rem;
        }

        .search-bar .submit {
            background: #4dcb44;
            color: #fff;
            font-family: "Vidaloka", serif;
            border-radius: 100px;
            font-size: 1.3em;
            cursor: pointer;
            width: 100%;
            border: none;
            padding: 20px 0 15px;
        }

        .search-bar input,
        .search-bar .bootstrap-select {
            background: none;
            border: none;
            padding: 20px 0 15px;
            width: 100%;
        }

        .search-bar .bootstrap-select {
            width: 100% !important;
            padding: 0;
        }

        .search-bar input,
        .search-bar .bootstrap-select {
            background: none;
            border: none;
            padding: 20px 10px 15px;
            width: 100%;
        }

        .search-bar .bootstrap-select button {
            background: none !important;
            outline: 0 !important;
            box-shadow: none;
            padding: 20px 0 15px;
            color: #aaa;
            font-weight: 300;
            font-family: "Poppins", sans-serif;
            border: 0;
            width: 100%;
            text-align: left;
            padding: 0;
        }

        .bootstrap-select.btn-group .dropdown-toggle .filter-option {
            display: inline-block;
            overflow: hidden;
            width: 100%;
            text-align: left;
        }

        .btn-group.open .dropdown-toggle {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .open>.dropdown-menu {
            width: 100%;
        }

        .dropdown-menu p {
            color: #000;
            margin: 0 10px;
            cursor: pointer;
        }

    </style>
    <section id="values" class="values values bg-light">
        <div class="container">
            <p class="small-text-hero"><i class="icon-localizer text-primary mr-1"></i>Lorem ipsum <span
                    class="text-primary">dolor sit</span> amet</p>
            <h1>Let's <span class="text-primary">go </span> anywhere</h1>
            <p class="text-hero">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            <div class="search-bar">
                <form action="javascript:void(0)" id="formCheck">
                    <div class="row">
                        <div class="form-group col-lg-8  col-sm-8 col-md-8">
                            <input type="search" name="subdomain" placeholder="Masukan domain sub domain anda?">
                        </div>
                        <div class="form-group rightLine col-lg-2 col-sm-2 col-md-2">
                            <input type="text" name="location" value=".mysch.my.id" id="location" disabled>
                            <label for="location" class="location"><i class="fa fa-dot-circle-o"></i></label>
                        </div>
                        <div class="form-group col-lg-2  col-sm-2 col-md-2">
                            <button type="submit" class="submit" id="cek">Check</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container mt-5" id="detailDomain" style="display: none">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card h-100">
                    <div class="card-body">
                        <form action="{{ route('proses_save_sekolah') }}" method="post">
                            @csrf
                            <div class="row gutters">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="eMail">URL Domain Lengkap</label>
                                        <input type="text" class="form-control" id="domain" readonly>
                                    </div>
                                </div>
                                <hr class="mt-3">
                                <div class="col-md-6">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-2 text-primary">DATA SEKOLAH</h6>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <input type="hidden" name="domain" id="domain_utama">
                                            <input type="hidden" name="aksi" value="domain">
                                            <label for="fullName">Pilih Tingkat</label>
                                            <select name="tingkat" id="tingkat" class="form-control">
                                                <option value="dasar">Dasar</option>
                                                <option value="menengah">Menengah</option>
                                                <option value="atas">Atas</option>
                                                <option value="kejuruan">Kejuruan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Nama Sekolah</label>
                                            <input type="text" name="nama_sekolah" class="form-control" id="nama_sekolah">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Alamat Sekolah</label>
<<<<<<< HEAD
                                            <input type="text" name="alamat_sekolah" class="form-control" id="alamat_sekolah">
=======
                                            <input type="alamat_sekolah" class="form-control" id="alamat_sekolah">
>>>>>>> 72af603e191047956d5072b075d04fd10a22a54b
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Kabupaten / Kota</label>
                                            <input type="text" name="kabupaten" class="form-control" id="kabupaten">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Kodepos</label>
                                            <input type="number" name="kodepos" class="form-control" id="kodepos">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">No Telp Sekolah</label>
                                            <input type="text" name="telepon_sekolah" class="form-control"
                                                id="telepon_sekolah">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Email Sekolah</label>
                                            <input type="text" name="email_sekolah" class="form-control" id="email_sekolah">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-2 text-primary">DATA KEPALA SEKOLAH</h6>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Nama Kepala Sekolah</label>
                                            <input type="text" class="form-control" name="nama_kepala_sekolah"
                                                id="nama_kepala_sekolah">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">No Handphone</label>
                                            <input type="text" class="form-control" name="handphone_kepsek"
                                                id="handphone_kepsek">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-2 text-primary">DATA ADMIN / OPERATOR</h6>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Nama Admin / Operator</label>
                                            <input type="text" name="nama_admin" class="form-control" id="nama_admin">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">No Handphone</label>
                                            <input type="text" name="handphone_admin" class="form-control"
                                                id="handphone_admin">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Email</label>
                                            <input type="email" class="form-control" name="email_admin" id="email_admin">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                                    <div class="form-group">
                                        <div class="text-right" style="float: right">
                                            <button type="button" id="submit" name="submit"
                                                class="btn btn-secondary">Cancel</button>
                                            <button type="submit" id="submit" name="submit"
                                                class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            $('#formCheck').on('submit', function(event) {
                event.preventDefault();
                $("#cek").html(
                    'Memproses..');
                $("#cek").attr("disabled", true);
                $.ajax({
                    url: "{{ route('proses_cek_domain') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swal({
                                title: 'Selamat!',
                                text: "Domain yang anda masukan masih tersedia. apa anda ingin memprosesnya?",
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ya, Daftarkan!',
                                cancelButtonText: 'Tidak, Batalkan!',
                                confirmButtonClass: 'btn btn-success',
                                cancelButtonClass: 'btn btn-danger',
                                buttonsStyling: false
                            }).then(function() {
                                $('#domain').val(data.data + '.mysch.web.id');
                                $('#domain_utama').val(data.data);
                                $('#detailDomain').show('');
                            }, function(dismiss) {
                                if (dismiss === 'cancel') {
                                    swa("Dibatalkan!", 'Proses dibatalkan', 'error');
                                }
                            })
                        } else {
                            swa(data.status + "!", data.success, data.icon);
                        }
                        $('#cek').html('Check');
                        $("#cek").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        });
    </script>
@endsection
