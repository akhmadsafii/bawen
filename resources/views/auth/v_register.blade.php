<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    <style>
        .head {
            height: 200px;
            background-image: url('https://pixy.org/src/476/4764112.jpg');
        }

        @media only screen and (max-width: 768px) {

            table {
                position: absolute;
            }

            .kembali {
                top: 10px;
                left: 17px !important;
            }
        }

    </style>
    <main>
        {{-- {{ dd($sekolah) }} --}}
        <header class="head pt-4">
            <a class="kembali text-white" href="{{ route('home') }}" style="position: absolute; left: 30px;"><i
                    class="fas fa-arrow-left fa-2x"></i></a>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <table>
                                <tr>
                                    <td rowspan="2" style="width: 20%"><img style="width: 82px"
                                            src="{{ $program['file'] }}" alt="">
                                    </td>
                                    <td style="width: 60%">
                                        <h3 class="text-uppercase mx-3 text-white text-center font-weight-bold">
                                            {{ $program['nama'] }}</h3>
                                    </td>
                                    <td rowspan="2" style="width: 20%"><img style="width: 82px"
                                            src="{{ $sekolah['file'] }}" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="text-uppercase mx-3 text-white text-center">
                                            {{ $sekolah['nama'] }}</p>
                                    </td>
                                </tr>
                            </table>
                            <div class="upper">
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="main-content" style="margin-top: -70px">
                <div class="container-fluid mx-auto" style="max-width: 743px;">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="content bg-white pt-4 pr-5 pb-3 pl-5 rounded"
                                style="-webkit-box-shadow: 0 30px 60px -30px #336799;">
                                <h1>Membuat Akun Baru</h1>
                                <form action="{{ route('auth.verifyRegister') }}" method="post" class="login-form"
                                    id="formLogin" onsubmit="return loader()">
                                    @csrf
                                    <div class="row">
                                        @if (session('config') == 'alumni')
                                            <input type="hidden" name="nama_program" value="alumni">
                                            <input type="text" name="cek_pass" id="cek_pass">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="">Nama Lengkap</label>
                                                    <input name="nama" type="text" class="radius form-control p-2"
                                                        placeholder="Nama Anda" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group mb-1">
                                                    <label for="">Masukan Password</label>
                                                    <input type="password" class="radius form-control p-2"
                                                        placeholder="Password" id="password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group mb-1">
                                                    <label for="">Ulangi Password</label>
                                                    <input name="password" type="password" id="confirm_password"
                                                        class="radius form-control p-2" placeholder="Ulangi Password"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div id="msg"></div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group mt-3">
                                                    <label for="">Masukan NIK</label>
                                                    <input name="nomor_induk" type="text"
                                                        class="radius form-control p-2" placeholder="NIK" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group mt-3">
                                                    <label for="">Program Studi Anda</label>
                                                    <select name="id_jursuan" id="id_jurusan" class="form-control">
                                                        <option value="">-- Pilih Jurusan --</option>
                                                        @foreach ($jurusan as $jr)
                                                            <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Pendidikan Terakhir</label>
                                                    <select name="pendidikan_terakhir" class="form-control"
                                                        id="dropstudy">
                                                        <option value="">-- Pilih Pendidikan Akhir --</option>
                                                        <option value="d3">D3</option>
                                                        <option value="d4">D4</option>
                                                        <option value="s1">S1</option>
                                                        <option value="s2">S2</option>
                                                        <option value="s3">S3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Jenis Kelamin</label>
                                                    <select name="jenkel" id="jenkel" class="form-control">
                                                        <option value="">--Pilih Jenis Kelamin--</option>
                                                        <option value="l">Laki - laki</option>
                                                        <option value="p">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Tahun Angkatan</label>
                                                    <select name="tahun_angkatan" class="form-control select3"
                                                        id="tahun_angkatan">
                                                        <option value="">-- Pilih Tahun Lulus --</option>
                                                        @php
                                                            $firstYear = (int) date('Y');
                                                            $lastYear = $firstYear - 20;
                                                            for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                                echo '<option value=' . $year . '>' . $year . '</option>';
                                                            }
                                                        @endphp
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Tahun Lulus</label>
                                                    <select name="tahun_lulus" class="form-control select3"
                                                        id="tahun_lulus">
                                                        <option value="">-- Pilih Tahun Lulus --</option>
                                                        @php
                                                            $firstYear = (int) date('Y');
                                                            $lastYear = $firstYear - 20;
                                                            for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                                echo '<option value=' . $year . '>' . $year . '</option>';
                                                            }
                                                        @endphp
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="">Email</label>
                                                    <input name="email" type="text" class="radius form-control p-2"
                                                        placeholder="Email Anda" required>
                                                </div>
                                            </div>
                                        @elseif(session('config') == 'bursa_kerja')
                                            <input type="hidden" name="nama_program" value="bursa_kerja">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Nama Lengkap</label>
                                                    <input type="text" placeholder="Masukan Nama" class="form-control"
                                                        name="nama" id="example-email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Alumni Sekolah ini?</label>
                                                    <select name="alumni" id="alumni" class="form-control">
                                                        <option value="ya">Ya</option>
                                                        <option value="tidak">Tidak</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" placeholder="Email" name="email"
                                                        class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 d-none" id="sekolah_asal">
                                                <div class="form-group">
                                                    <label for="">Asal sekolah</label>
                                                    <input type="text" placeholder="Asal Sekolah" class="form-control"
                                                        name="asal_sekolah" id="example-email">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Masukan Password</label>
                                                    <input type="password" placeholder="password" name="password"
                                                        class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Ulangi Password</label>
                                                    <input type="password" placeholder="password" name="password"
                                                        class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Mendaftar Sebagai ?</label>
                                                    <select name="role" class="form-control form-control-line"
                                                        id="dropdownlist" required>
                                                        <option value="superadmin">-- Pilih Role --</option>
                                                        <option value="industri">Industri</option>
                                                        <option value="pelamar">Pelamar</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Jenis Kelamin</label>
                                                    <select name="jenkel" class="form-control" id="dropjk">
                                                        <option value="">-- Pilih Jenis kelamin --</option>
                                                        <option value="l">Laki - laki</option>
                                                        <option value="p">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Pendidikan Terakhir</label>
                                                    <select name="pendidikan_akhir" class="form-control"
                                                        id="dropstudy">
                                                        <option value="">-- Pilih Pendidikan Akhir --</option>
                                                        <option value="sd">SD sederajat</option>
                                                        <option value="smp">SMP Sederajat</option>
                                                        <option value="sma">SMA Sederajat</option>
                                                        <option value="d3">D3</option>
                                                        <option value="s1">S1</option>
                                                        <option value="s2">S2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" id="inputArea">

                                            </div>
                                        @elseif(session('config') == 'ppdb')
                                            <input type="hidden" name="nama_program" value="ppdb">
                                            <div class="col-md-12 mt-4">
                                                <div class="form-group">
                                                    <label for="example-email" style="color: #18254c">Email</label>
                                                    <input type="text" placeholder="password" name="email"
                                                        class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="example-email" style="color: #18254c">Password</label>
                                                    <input type="password" placeholder="password" name="password"
                                                        class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="example-email" style="color: #18254c">Ulangi
                                                        Password</label>
                                                    <input type="password" placeholder="password" name="password"
                                                        class="form-control" required>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-lg-12">
                                            <button class="btn btn-info btn-block" id="btnLogin"
                                                type="submit">REGISTER</button>
                                        </div>
                                        <div class="col-lg-12 mt-2">
                                            <div class="text-center">Sudah Punya Akun? <a
                                                    href="{{ route('auth.login') }}">Login sekatang</a></div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="content login-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dialog">
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            showSekolah("ya");

            $("#confirm_password").keyup(function() {
                if ($("#password").val() != $("#confirm_password").val()) {
                    $("#msg").html("Password do not match").css("color", "red");
                    $("#cek_pass").val("gagal");
                } else {
                    $("#msg").html("Password matched").css("color", "green");
                    $("#cek_pass").val("berhasil");
                }
            });
        });

        function loader() {
            $("#btnLogin").attr("disabled", true);
            $("#btnLogin").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
        }

        function swa(status, message, icon) {
            swal(
                status,
                message,
                icon
            );
            return true;
        }

        $("#dropdownlist").change(function() {
            var numInputs = $(this).val();
            if (numInputs == "industri") {
                $("#inputArea").html(
                    '<div class="form-group"><label for="">Nama Perusahaan</label><input type="text" placeholder="Masukan Nama Perusahaan"class="form-control" name="industri" id="example-email"></div>'
                );
            } else {
                $("#inputArea").html('');
            }

        });

        $("#alumni").change(function() {
            var alumni = $(this).val();
            showSekolah(alumni);
        });


        const mata = document.querySelector(".eye")
        const inputPass = document.querySelector("#exampleInputPassword1");

        mata.addEventListener("click", () => {
            mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

            if (inputPass.type === "password") {
                inputPass.setAttribute("type", "text")

            } else if (inputPass.type === "text") {
                inputPass.setAttribute("type", "password")
                mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
            }
        })

        function showSekolah(alumni) {
            // console.log(alumni);
            if (alumni == "ya") {
                $("#sekolah_asal").addClass('d-none');
            } else {
                $("#sekolah_asal").removeClass('d-none');
            }
        }
    </script>
    <script src="{{ asset('asset/js/service_worker.js') }}"></script>
</body>

</html>
