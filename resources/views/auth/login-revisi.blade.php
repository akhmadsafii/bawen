<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    @if (Session::has('message'))
        <script>
            $(function(){
                swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                    '{{ session('message')['icon'] }}');
            })
        </script>
    @endif
    <style>
        .head {
            height: 200px;
            background-image: url('https://pixy.org/src/476/4764112.jpg');
        }

        @media only screen and (max-width: 768px) {

            table {
                position: absolute;
            }

            .kembali {
                top: 10px;
                left: 17px !important;
            }
        }

    </style>
    <main>
        <header class="head pt-4">
            <a class="kembali text-white" href="{{ url()->previous() }}" style="position: absolute; left: 30px;"><i
                    class="fas fa-arrow-left fa-2x"></i></a>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <table>
                                <tr>
                                    <td rowspan="2" style="width: 20%"><img style="width: 82px"
                                            src="{{ $program['file'] }}" alt="">
                                    </td>
                                    <td style="width: 60%">
                                        <h3 class="text-uppercase mx-3 text-white text-center font-weight-bold">
                                            {{ $program['nama'] }}</h3>
                                    </td>
                                    <td rowspan="2" style="width: 20%"><img style="width: 82px"
                                            src="{{ $sekolah['file'] }}" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="text-uppercase mx-3 text-white text-center">
                                            {{ $sekolah['nama'] }}</p>
                                    </td>
                                </tr>
                            </table>
                            <div class="upper">
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="main-content" style="margin-top: -70px">
                <div class="container-fluid mx-auto" style="max-width: 460px;">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="content bg-white pt-4 pr-5 pb-3 pl-5 rounded"
                                style="-webkit-box-shadow: 0 30px 60px -30px #336799;">
                                <h1>Selamat datang,</h1>
                                <p>Silahkan login dengan username dan password yang telah anda miliki.</p>
                                @if ($message = Session::get('message'))
                                    @if ($message['status'] == 'gagal')
                                        <div class="alert alert-danger border-danger alert-dismissible p-0"
                                            role="alert">
                                            <div class="widget-list m-2">
                                                <div class="col-md-12 widget-holder m-0">
                                                    <div class="widget-body clearfix text-center">
                                                        <i class="material-icons list-icon">warning</i>
                                                        <strong>{{ $message['message'] }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($message['status'] == 'success')
                                        <div class="alert alert-success border-info alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <div class="widget-list">
                                                <div class="col-md-12 widget-holder">
                                                    <div class="widget-body clearfix text-center">
                                                        <i class="material-icons list-icon">check_circle</i>
                                                        <strong>{{ $message['message'] }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @if (session('config') == 'kelulusan')
                                    <p class="text-center text-info">Halaman Administrator</p>
                                @endif
                                <form action="{{ route('auth.verifylogin') }}" method="post"
                                    onsubmit="return loader()" class="form-material">
                                    @csrf
                                    <div class="row">
                                        @if (session('config') != 'mutu' && session('config') != 'kelulusan')
                                            <div class="col-lg-12">
                                                <div class="form-group form-control-icon input-focused">
                                                    <div class="input-group input-focused">
                                                        <i class="fas fa-blind fa-2x"> </i>
                                                        <select class="form-control pb-0" name="role" id="l13">
                                                            @if (session('config') == 'point')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="supervisor">Supervisor</option>
                                                                <option value="bk">BK</option>
                                                                <option value="guru">Guru</option>
                                                                <option value="ortu">Wali Murid</option>
                                                                <option value="siswa">Siswa</option>
                                                            @elseif(session('config') == 'e_learning')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="learning-admin">Admin E Learning</option>
                                                                <option value="guru">Guru</option>
                                                                <option value="siswa">Siswa</option>
                                                                <option value="walikelas">Wali Kelas</option>
                                                                <option value="supervisor">Supervisor</option>
                                                            @elseif(session('config') == 'bursa_kerja')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="bkk-admin">Admin BKK</option>
                                                                <option value="supervisor">Supervisor</option>
                                                                <option value="bkk-perusahaan">Industri</option>
                                                                <option value="bkk-pelamar">Pelamar</option>
                                                            @elseif(session('config') == 'sistem_pkl')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="admin-prakerin">Admin Prakerin</option>
                                                                <option value="supervisor">Supervisor</option>
                                                                <option value="user-kaprodi">Kepala Jurusan/Kaprodi
                                                                </option>
                                                                <option value="guru">Guru</option>
                                                                <option value="siswa">Siswa</option>
                                                                <option value="pembimbing-industri">Pembimbing Industri
                                                                </option>
                                                            @elseif(session('config') == 'raport')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="admin-raport">Admin Raport</option>
                                                                <option value="guru">Guru</option>
                                                                <option value="siswa">Siswa</option>
                                                                <option value="walikelas">Wali Kelas</option>
                                                                <option value="ortu">Orang Tua</option>
                                                            @elseif(session('config') == 'spp')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="tata-usaha">TU Keuangan</option>
                                                                <option value="ortu">Wali Murid</option>
                                                                <option value="siswa">Siswa</option>
                                                            @elseif(session('config') == 'induk')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="induk-admin">Admin Buku Induk</option>
                                                                <option value="supervisor">Supervisor</option>
                                                            @elseif(session('config') == 'kesiswaan')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="admin-kesiswaan">Admin Kesiswaan</option>
                                                                <option value="supervisor">Supervisor</option>
                                                                <option value="guru">Guru</option>
                                                                <option value="walikelas">Wali Kelas</option>
                                                            @elseif(session('config') == 'alumni')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="admin-alumni">Admin Alumni</option>
                                                                <option value="alumni-alumni">Alumni</option>
                                                            @elseif(session('config') == 'ppdb')
                                                                <option value="">-- Login Sebagai? --</option>
                                                                <option value="admin-ppdb">Admin PPDB</option>
                                                                <option value="peserta-ppdb"> Calon Peserta PPDB
                                                                </option>
                                                            @elseif(session('config') == 'sarpras')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-sarpras">Admin</option>
                                                                <option value="user-sarpras">User</option>
                                                            @elseif(session('config') == 'absensi')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-absensi">Admin Absensi</option>
                                                                <option value="siswa">Siswa</option>
                                                                <option value="guru">Guru</option>
                                                            @elseif(session('config') == 'perpustakaan')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-perpus">Admin</option>
                                                                <option value="user-perpus">User</option>
                                                            @elseif(session('config') == 'cbt')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-cbt">Admin</option>
                                                                <option value="siswa">Siswa</option>
                                                                <option value="guru">Guru</option>
                                                            @elseif(session('config') == 'kepegawaian')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-kepegawaian">Admin</option>
                                                                <option value="pegawai">Pegawai</option>
                                                            @elseif(session('config') == 'penilaian_guru')
                                                                <option value="">-- Pilih Role --</option>
                                                                <option value="admin-pkg">Admin</option>
                                                                <option value="penilai-pkg">Penilai</option>
                                                                <option value="guru-pkg">Guru</option>
                                                            @elseif(session('config') == 'surat')
                                                                <option value="admin-surat">Admin</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            @if (session('config') == 'kelulusan')
                                                <input type="hidden" name="role" value="admin-simpen">
                                            @else
                                                <input type="hidden" name="role" value="user-mutu">
                                            @endif
                                        @endif
                                        <div class="col-lg-12">
                                            <div class="form-group form-control-icon input-focused">
                                                <div class="input-group input-focused">
                                                    <i class="fas fa-user-circle fa-2x"> </i>
                                                    <input class="form-control" id="l32" placeholder="Username"
                                                        type="text" name="username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-control-icon input-focused">
                                                <div class="input-group input-focused">
                                                    <i class="fas fa-key fa-2x"></i>
                                                    <a href="javascript:void(0)"
                                                        style="left: auto; right: 0; position: absolute; top: 25%; z-index: 99"><i
                                                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                                    <input class="form-control" name="password"
                                                        id="exampleInputPassword1" placeholder="Password"
                                                        type="password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-info btn-block"
                                                id="btnLogin">Login</button>
                                        </div>
                                        <div class="col-lg-12 mt-2">
                                            @if (session('config') == 'alumni')
                                                <a href="{{ route('auth.resetPassword') }}"
                                                    class="pull-right text-info">Lupa Password</a>
                                            @elseif(session('config') == 'ppdb')
                                                <a href="{{ route('forget_password_peserta') }}"
                                                    class="pull-right text-info">Lupa Password</a>
                                            @elseif(session('config') == 'spp')
                                                <!--a href="{{ route('forget_passwordx') }}"
                                                    class="pull-right text-info">Lupa Password</a-->
                                                <a href="javascript:void(0)" onclick="lupaPassword()"
                                                    class="pull-right text-info">Lupa Password</a>
                                            @else
                                                @if (session('config') == 'absensi')
                                                    <a href="{{ route('halaman_absensi_siswa') }}"
                                                        class="text-success">Absensi Siswa</a>
                                                @endif
                                                <a href="javascript:void(0)" onclick="lupaPassword()"
                                                    class="pull-right text-info">Lupa Password</a>
                                            @endif
                                        </div>
                                    </div>
                                    @if (session('config') == 'alumni' || session('config') == 'bursa_kerja')
                                        <hr class="mb-4">
                                        <a href="{{ route('auth.register') }}"
                                            class="btn btn-purple btn-block">Daftar</a>
                                    @endif
                                </form>
                            </div>

                            <div class="content login-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dialog">
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        function loader() {
            $("#btnLogin").attr("disabled", true);
            $("#btnLogin").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
        }

        function swa(status, message, icon) {
            swal(
                status,
                message,
                icon
            );
            return true;
        }

        function lupaPassword() {
            var role = $('#role').val();
            if (role == 'admin') {
                window.location.href = "{{ url('auth/reset_password') }}";
            } else {
                alert('harap hubungi admin untuk mereset password')
            }
        }

        const mata = document.querySelector(".eye")
        const inputPass = document.querySelector("#exampleInputPassword1");

        mata.addEventListener("click", () => {
            mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

            if (inputPass.type === "password") {
                inputPass.setAttribute("type", "text")

            } else if (inputPass.type === "text") {
                inputPass.setAttribute("type", "password")
                mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
            }
        })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js" fefer></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('asset/js/service_worker.js') }}" defer></script>
</body>

</html>
