<link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
<script type="text/javascript">
    //disabled pace loading
    window.paceOptions = {
        ajax: false,
        restartOnRequestAfter: false,
    };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" sizes="20x20" href="{{ session('logo') ?? asset('asset/img/sma.png') }}">
{{-- <title>E-Learning MYSCH.ID</title> --}}
<title>{{ !empty(Session::get('title')) ? session('title') : 'Demo Smartschool' }}</title>
<!-- CSS -->
<link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
    rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css" rel="stylesheet" type="text/css">
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet"
    type="text/css"> --}}
<link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
<!-- Head Libs -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css"
    rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css" />
<link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
    rel="stylesheet" type="text/css">
<style type="text/css">
    .side-user {
        padding: 0.42857em 0;
        border-bottom: 1 px solid #ddd;
        background: #f7fafc;
    }

    .side-menu ul {
        margin-left: 3.80em;
        position: relative;
        top: -0.38462em;
    }

    .hide-menu.single {
        margin-left: 0.9rem !important;
    }

    .hide-menu.print {
        margin-left: 0.5rem !important;
    }

    .hide-menu.info {
        margin-left: 0rem !important;
    }

    .hide-menu.download {
        margin-left: 0.6rem !important;
    }

    .hide-menu.pesan {
        margin-left: 0.8rem !important;
    }

    .navbar.admin,
    .navbar-header.admin,
    .navbar-brand.admin,
    .site-sidebar.admin,
    .dropdown-card.admin {
        background: #36a7d3;
    }

    .navbar.user,
    .navbar-header.user,
    .navbar-brand.user,
    .site-sidebar.user,
    .dropdown-card.user {
        background: #03a9f3;
    }

    .side-menu li:hover,
    .side-menu li.active {
        background: #0910e9;
    }

    .side-menu>li>a:hover,
    .side-menu>li>a:focus {
        color: #ebe0e0;
        background: #e1d9d900;
    }

    ul.list-unstyled.sub-menu.collapse.in>li>a:before {
        color: dimgrey;
    }

    .side-menu li a {
        color: #f9ecec;
        position: relative;
        display: block;
        padding-top: 0;
        padding-bottom: 0;
    }

    .dropdown-card.dropdown-card-wide {
        min-width: auto;
        width: 13rem;
        max-width: 100vw;
    }

    .navbar-nav>li .dropdown-menu {
        margin-top: 4px;
    }

    .dropdown-card.admin:before {
        position: absolute;
        top: -8px;
        left: 164px;
        display: inline-block;
        border-right: 9px solid transparent;
        border-bottom: 9px solid #36a7d3;
        border-left: 9px solid transparent;
        content: '';
    }

    .dropdown-card.user:before {
        position: absolute;
        top: -8px;
        left: 164px;
        display: inline-block;
        border-right: 9px solid transparent;
        border-bottom: 9px solid #03a9f3;
        border-left: 9px solid transparent;
        content: '';
    }

    .sidecustom {
        margin-top: -19px;
    }

    .card-heading-extra {
        border-bottom: 1px solid #fff;
        padding-bottom: 0;
        margin-bottom: 0;
        text-transform: uppercase;
        font-size: 0.78571em;
    }


    .side-menu>li>a {
        padding-left: 1em;
        font-size: 1.15385em;
        line-height: 3.2em;
        color: #f5f2f2;
        border-left: 3px solid transparent;
        font-family: "Nunito Sans", sans-serif;
        font-weight: 600;
    }

    .modal-primary .modal-content.modal-header {
        background: #fb9678;
        border: 0;
    }

    @media (min-width: 961px) {
        .sidebar-expand .site-sidebar {
            position: fixed;
            width: 15.375rem;
            height: calc(100vh - 5.625rem);
        }

        .sidebar-collapse .navbar-header {
            width: 7.75rem;
        }

        .sidebar-collapse .side-menu .sub-menu li {
            background: #03a9f3;
        }

        .sidebar-toggle::after {
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: transparent;
        }

    }

    @media screen and (max-width: 960px) {
        .sidebar-toggle.admin::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: #03a9f3;
        }

        .sidebar-toggle.user::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: #03a9f3;
        }

        .sidebar-toggle {
            margin-left: 1rem !important;
        }

        .page-title ol {
            display: none;
        }
    }


    .pengumuman:hover {
        background-color: seashell;
    }

</style>
@if (!empty(session('PUSHER_APP_KEY')))
    <script src="//js.pusher.com/3.1/pusher.min.js" defer></script>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                var notificationsWrapper = $('.dropdown-notifications');
                var notificationsToggle = notificationsWrapper.find('a[data-toggle]');
                var notificationsCountElem = notificationsToggle.find('span[data-count]');
                var notificationsCount = parseInt(notificationsCountElem.data('count'));
                var notifications = notificationsWrapper.find('ul.list-message');
                if (notificationsCount <= 0) {
                    notificationsWrapper.hide();
                }
                // Enable pusher logging - don't include this in production
                //Pusher.logToConsole = true;

                var pusher = new Pusher('{{ session('PUSHER_APP_KEY') }}', {
                    encrypted: true,
                    cluster: '{{ session('PUSHER_APP_CLUSTER') }}'
                });

                @switch(session('role'))
                @case('peserta-ppdb')
                var channel_user = pusher.subscribe('my-channel');
                channel_user.bind('my_event', function(data) {
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    notif('info', data);
                    var existingNotifications = notifications.html();
                    var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
                    var newNotificationHtml = `<li><a href="{{ route('history-pembayaran') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                //notifikasi pengumuman
                var channel_new = pusher.subscribe('pengumuman');
                channel_new.bind('event_pengumuman', function(data) {
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    notif('info', data);
                    var existingNotifications = notifications.html();
                    var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
                    var newNotificationHtml = `<li><a href="{{ route('informasi-pengumuman') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                var channel_keputusan = pusher.subscribe('keputusan');
                channel_keputusan.bind('event_keputusan', function(data) {
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    notif('info', data);
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="{{ route('print_pengumuman-user') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                var channel_user = pusher.subscribe('pesan_user');
                channel_user.bind('pesan_user', function(data) {
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    notif('info', data['pesan']);
                    var existingNotifications = notifications.html();
                    var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
                    var newNotificationHtml = `<li><a href="{{ route('pesan-user') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data['pesan'] + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                @break
                @case('admin-ppdb')
                var channel_admin = pusher.subscribe('konfirmasi_pembayaran');
                channel_admin.bind('event_konfirm', function(data) {
                    notif('info', data);
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="{{ route('payment-confirm') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                var channel_admin_pesan = pusher.subscribe('pesan_admin');
                channel_admin_pesan.bind('pesan_admin', function(data) {
                    var urlpsn = "{{ route('pesanUseradmin', ['id' => ':id']) }}";
                    urlpsn = urlpsn.replace(':id', data['id_user']);
                    notif('info', data['pesan']);
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="`+urlpsn+`" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data['pesan'] + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });


                @break
                @case('admin')
                var channel_admin = pusher.subscribe('konfirmasi_pembayaran');
                channel_admin.bind('event_konfirm', function(data) {
                    notif('info', data);
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="{{ route('payment-confirm') }}" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });

                var channel_admin_pesan = pusher.subscribe('pesan_admin');
                channel_admin_pesan.bind('pesan_admin', function(data) {
                    var urlpsn = "{{ route('pesanUseradmin', ['id' => ':id']) }}";
                    urlpsn = urlpsn.replace(':id', data['id_user']);
                    notif('info', data['pesan']);
                    soundnotif('{{ asset('asset/notif-sound.mp3') }}');
                    var existingNotifications = notifications.html();
                    var newNotificationHtml = `<li><a href="`+urlpsn+`" class="media">
                <span class="d-flex user--online thumb-xs">
                <img src="{{ asset('images/default.png') }}" class="rounded-circle" alt=""></span>
                <span class="media-body">
                 <span class="media-content">` + data['pesan'] + `</span>
                </span></a>
                </li>`;
                    notifications.html(newNotificationHtml + existingNotifications);
                    notificationsCount += 1;
                    notificationsCountElem.attr('data-count', notificationsCount);
                    notificationsWrapper.find('.notif-count').text(notificationsCount);
                    notificationsWrapper.show();
                });


                @break
                @endswitch

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                window.soundnotif = function playSound(url) {
                    var audio = document.createElement('audio');
                    audio.style.display = "none";
                    audio.src = url;
                    audio.autoplay = true;
                    audio.onended = function() {
                        audio.remove() //Remove when played.
                    };
                    document.body.appendChild(audio);
                }

            });
        })(jQuery, window);
    </script>
@endif
