<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js" defer></script>
<script src="{{ asset('asset/js/bootstrap.min.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js" defer>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js" defer></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js" defer></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
<script src="{{ asset('asset/js/theme2.js') }}" defer></script>
<script src="{{ asset('asset/js/service_worker.js') }}" defer></script>
<script type="text/javascript">
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('{{ asset('asset/js/service_worker.js') }}').then(function(
                registration) {
                //console.log('ServiceWorker registration :', registration.scope);
            }).catch(function(error) {
                //console.log('ServiceWorker registration failed:', errror);
            });

        });
    }

    var mybutton = document.getElementById("myBtn");

    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }

    (function($) {
        'use strict';
        $(document).ready(function() {
            $('#signup').click(function() {
                var width = window.innerWidth > 0 ? window.innerWidth : screen.width;
                var $body = $("body");
                $('.site-sidebar.scrollbar-enabled').perfectScrollbar('destroy');

                if (width < 961) {
                    $(".site-sidebar").toggle();
                } else if ($body.hasClass("sidebar-expand")) {
                    $body.removeClass("sidebar-expand");
                    $body.addClass("sidebar-collapse");
                    $(".side-user > a").removeClass("active");
                    $(".side-user > a").siblings(".side-menu").hide();
                    $('.side-menu .sub-menu').css('height', 'auto');
                    $('.side-menu').metisMenu('dispose');
                } else if ($body.hasClass("sidebar-collapse")) {
                    $body.removeClass("sidebar-collapse");
                    $body.addClass("sidebar-expand");
                    $('.site-sidebar.scrollbar-enabled').perfectScrollbar();
                    $('.side-menu').metisMenu({
                        preventDefault: true
                    });
                }
            });
            //backtotop
            $('#myBtn').click(function() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            });

        });
    })(jQuery);
</script>
