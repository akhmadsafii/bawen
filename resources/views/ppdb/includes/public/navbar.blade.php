<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li><a href="{{ route('ppdb-public') }} "><i class="material-icons list-icon">rss_feed</i><span
                    class="hide-menu">Beranda</span></a></li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple">
                <i class="material-icons list-icon">info</i>
                <span class="hide-menu">Informasi Pendaftaran</span></a>
            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                <li><a href="{{ route('ppdb-rule-ppdb') }}">Alur Pendaftaran</a>
                </li>
                <li><a href="{{ route('ppdb-syarat-ppdb') }}">Syarat Pendaftaran</a>
                </li>
                <li><a href="{{ route('ppdb-panduan-ppdb') }}">Panduan Pendaftaran</a>
                </li>
                <li><a href="{{ route('ppdb-kegiatan-ppdb') }}">Rangkaian Kegiatan</a>
                </li>
                <li><a href="{{ route('ppdb-faq') }} ">FAQ</a>
                </li>
            </ul>
        </li>
        <li><a href="{{ route('ppdb-pengumuman') }}"><i class="material-icons list-icon">feedback</i><span
                    class="hide-menu">Pengumuman</span></a></li>
        <li><a href="{{ route('data_seleksi_ppdb') }}"><i class="material-icons list-icon">list</i><span
                    class="hide-menu">Seleksi PPDB </span></a></li>
        <li><a href="{{ route('ppdb-download') }}"><i class="material-icons list-icon">cloud_download</i><span
                    class="hide-menu">Download</span></a></li>

        @switch(session('role'))
            @case('admin-ppdb')
                <script>
                    {{ session()->put('message', ['message' => 'Anda masih keadaan login di sistem !','icon' => 'success','status' => 'login']) }}
                    window.location.href = "{{ route('dashboard-ppdb') }}";
                </script>
            @break

            @case('peserta-ppdb')
                <script>
                    {{ session()->put('message', ['message' => 'Anda masih keadaan login di sistem !','icon' => 'success','status' => 'login']) }}
                    window.location.href = "{{ route('user-ppdb') }}";
                </script>
            @break

            @default
                <li><a href="{{ route('form-ppdb') }}" id="signup"><i
                            class="material-icons list-icon">supervisor_account</i><span
                            class="hide-menu">Daftar</span></a></li>
                <li><a href="{{ url('program/ppdb') }} "><i class="material-icons list-icon">arrow_forward</i><span
                            class="hide-menu">Login</span></a></li>
            @break
        @endswitch
    </ul>
    <!-- /.side-menu -->
</nav>
<!-- /.sidebar-nav -->
