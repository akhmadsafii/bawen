<div class="col-md-12 widget-holder">
    <div class="widget-bg banner">
        <div class="widget-body clearfix">
            @if (empty($banner))
                <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada Banner </p>
            @else
                <div class="row">
                    @isset($banner['file'])
                        <div class="col-md-12">
                            <p class="box-title">{{ $banner['judul'] ?? '' }} </p>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="{{ $banner['file'] }}" loading="lazy" class="text-center" >
                                </div>
                                <div class="col-md-6">
                                    <div class="box-body">
                                        {!! $banner['keterangan'] ?? '' !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12">
                            <p class="box-title">{{ $banner['judul'] ?? '' }} </p>
                            <div class="box-body">
                                {!! $banner['keterangan'] ?? '' !!}
                            </div>
                        </div>
                    @endisset
                </div>
                <!-- /.row -->
            @endif

        </div>
        <!-- /.widget-body -->
    </div>
    <!-- /.widget-bg -->
</div>
