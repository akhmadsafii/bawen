<div class="card">
    <div class="card-body">
        <form class="form-material" autocomplete="off" role="presentation" id="sigupform" method="POST"
            action="{{ route('daftar') }}">
            @csrf
            <h5 class="card-title text-body" id="daftar">PENDAFTARAN AKUN</h5>
            @if ($message = Session::get('success'))
                <div class="alert alert-info border-info" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <div class="widget-list">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-body clearfix">
                                <strong class="text-info text-center"> <i class="list-icon fa fa-check-square-o"></i>
                                    {{ $message }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-error border-error" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <div class="widget-list">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-body clearfix">
                                <strong class="text-red text-center"><i class="list-icon fa fa-warning"></i>
                                    {{ $message }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <input class="form-control" type="text" name="nama" required="" placeholder="Nama Lengkap"
                    pattern="[a-zA-Z][a-zA-Z0-9/\s]*" title="Format harus berisi alfanumerik atau huruf saja ">
                <label>Nama Lengkap</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="tel" name="nisn" required="" placeholder="NISN" pattern="[0-9]+"
                    title="Masukan hanya angka saja" autocomplete="false">
                <label>NISN</label>
            </div>
            <div class="form-group ">
                <select class="form-control" name="jenkel" id="l1">
                    <option value="" disabled="disabled">--Pilih jenis kelamin ---</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                </select>
                <label>Jenis Kelamin</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="tel" name="telepon" required="" placeholder="No.Telp/WA"
                    pattern="[0-9]+" title="Masukan hanya angka saja" autocomplete="false">
                <label>No.Telp/WA</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="email" name="email" required="" placeholder="Email"
                    pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" title="format email salah "
                    autocomplete="false">
                <label>Email</label>
            </div>
            <div class="form-group ">
                <label class="mr-b-0">Password</label>
                <div class="input-group">
                    <input class="form-control password" type="password" name="password" required=""
                        placeholder="Password" autocomplete="false">
                    <div class="input-group-addon toggle-password">
                        <a href="javascript:void(0)"><i
                                class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                    </div>
                </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="material-icons">location_on</i>
                    </div>
                    <input class="form-control" type="text" id="alamat" name="alamat" required=""
                        placeholder="  Alamat">
                    <div class="input-group-addon">
                        <a href="javascript:void(0)" onclick="addr_search();"><i
                                class="material-icons list-icon search text-dark">search</i> </a>
                    </div>
                </div>
                <!-- /.input-group -->
                <div id="results"></div>
            </div>
            <div class="form-group">
                <input class="form-control" type="lat" name="lat" id="lat" placeholder="lat" readonly="readonly">
                <label>Koordinat Latitude</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="long" name="long" id="lon" placeholder="Long" readonly="readonly">
                <label>Koordinat Longitude</label>
            </div>

            <div class="form-group" id="map"></div>
            <div class="form-group text-center no-gutters mb-5">
                <button class="btn btn-info btn-lg btn-block text-uppercase ripple" type="submit"><i
                        class=" list-icon fa fa-sign-in"></i> Daftar</button>
            </div>
        </form>
    </div>
</div>
<script>
    (function($, global) {
        "use-strict"
        var options;
        var map;
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            //auto hide alert
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                $(".alert-dismissible").alert('close');
            });

            const mata = document.querySelector(".eye")
            const inputPass = document.querySelector(".password");
            mata.addEventListener("click", () => {
                mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                if (inputPass.type === "password") {
                    inputPass.setAttribute("type", "text")

                } else if (inputPass.type === "text") {
                    inputPass.setAttribute("type", "password")
                    mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                }
            });

            var startlat = '{{ session('koordinat_latitude') ?? '-6.991576' }} ';
            var startlon = '{{ session('koordinat_longitude') ?? '109.122923' }}';

            options = {
                center: [startlat, startlon],
                zoom: 9
            }
            map = L.map('map', options);

            window.getLocation = function geolocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert('Geolocation is not supported by this browser.');
                }
            }

            window.showPosition = function getData(x) {
                var latxg = x.coords.latitude;
                var longx = x.coords.longitude;
                setMaplocation(latxg, longx);
            }

            window.setMaplocation = function setLokasiMap(startlat, startlon) {
                var container = L.DomUtil.get('map');
                if (container != null) {
                    container._leaflet_id = null;
                }
                map.invalidateSize();
                optionsx = {
                    center: [startlat, startlon],
                    zoom: 9
                }
                map = L.map('map', optionsx);
                document.getElementById('lat').value = startlat;
                document.getElementById('lon').value = startlon;
                getAddressMaker(startlat, startlon);
            }

            window.getLocation();

            var nzoom = 12;

            L.tileLayer(
                'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYWtobWFkc2FmaWkiLCJhIjoiY2t3bGxlc21tMjJ4ZjJubXB0YjQ5c3FwOSJ9.HRCDZAsI36CZxjSoEWr2Vw', {
                    attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
                }).addTo(map);

            var myMarker = L.marker([startlat, startlon], {
                title: "Coordinates",
                alt: "Coordinates",
                draggable: true
            }).addTo(map).on('dragend', function() {
                var lat = myMarker.getLatLng().lat.toFixed(8);
                var lon = myMarker.getLatLng().lng.toFixed(8);
                var czoom = map.getZoom();
                if (czoom < 18) {
                    nzoom = czoom + 2;
                }
                if (nzoom > 18) {
                    nzoom = 18;
                }
                if (czoom != 18) {
                    map.setView([lat, lon], nzoom);
                } else {
                    map.setView([lat, lon]);
                }
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat, lon);
            });

            $('body').on('keyup', 'input[name="alamat"]', function() {
                addr_search();
            });

            window.getAddressMaker = function(latx, lonx) {
                $.ajax({
                    url: "https://nominatim.openstreetmap.org/reverse",
                    data: {
                        lat: latx,
                        lon: lonx,
                        format: "json"
                    },
                    beforeSend: function(xhr) {},
                    dataType: "json",
                    type: "GET",
                    async: true,
                    crossDomain: true
                }).done(function(res) {
                    var alamatx = res.display_name;

                    $('input[name="alamat"]').val(alamatx);
                    myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                        alamatx).openPopup();
                }).fail(function(error) {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                });
            }

            window.chooseAddr = function chooseAddr(lat1, lng1) {
                myMarker.closePopup();
                map.setView([lat1, lng1], 18);
                myMarker.setLatLng([lat1, lng1]);
                lat = lat1.toFixed(8);
                lon = lng1.toFixed(8);
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                getAddressMaker(lat1, lng1);
            }

            $('body').on('cllick', '.address', function() {
                $('input[name="alamat"]').val($(this).data('alamat'));
            });

            window.myFunction = function myFunction(arr) {
                var out = "<br />";
                var i;
                if (arr.length > 0) {
                    for (i = 0; i < arr.length; i++) {
                        out +=
                            "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                            arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                            .display_name + "'>" + arr[i].display_name +
                            "</div>";
                    }
                    document.getElementById('results').innerHTML = out;
                } else {
                    document.getElementById('results').innerHTML = "Sorry, no results...";
                }
            }

            window.addr_search = function addr_search() {
                var inp = document.getElementById("alamat");
                var xmlhttp = new XMLHttpRequest();
                var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                    .value;
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var myArr = JSON.parse(this.responseText);
                        myFunction(myArr);
                    }
                };
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            }

        });

    })(jQuery, window);
</script>
