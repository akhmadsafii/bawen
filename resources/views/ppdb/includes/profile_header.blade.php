<ul class="nav navbar-nav d-none d-lg-block">
    <li class="dropdown dropdown-notifications" style="display: none;"><a href="javascript:void(0);"
            class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false"><i
                class="material-icons list-icon">mail_outline</i> <span class="badge badge-border bg-primary"
                data-count="0"><span class="notif-count">0</span></span></a>
        <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-dark animated flipInY">
            <div class="card">
                <header class="card-header">New messages <span
                        class="mr-l-10 badge badge-border badge-border-inverted bg-primary"><span
                            class="notif-count">0</span></span>
                </header>
                <ul class="list-unstyled dropdown-list-group ps ps--theme_default ps--active-y list-message">
                </ul>
                <!-- /.dropdown-list-group -->
            </div>
            <!-- /.card -->
        </div>
    </li>
</ul>
<ul class="nav navbar-nav">
    <li class="dropdown hide">
        @if (session('role') == 'admin')
            <a href="{{ route('dashboard-master') }}"  aria-expanded="true">
                <span class="mr-b-10 sub-heading-font-family fw-300 text-white">
                    Master Sekolah
                </span>
            </a>
        @endif
        <a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="true">
            <span class="mr-b-10 sub-heading-font-family fw-300 text-white">
                {{ session('username') }}
                <i class="material-icons list-icon">expand_more</i></span>
            </span>
        </a>

        @switch (session('role'))
            @case('admin-ppdb')
                <div class="dropdown-menu dropdown-left dropdown-card admin dropdown-card-wide ">
                    <div class="card mb-2">
                        <ul class="list-unstyled sidecustom ">
                            <li><a href="{{ route('ubah_profil_admin') }}" class="media text-white">Ubah Profil</li>
                            <li><a href="{{ route('ubah_password_admin') }}" class="media text-white">Ubah Password</li>
                            <li><a href="{{ route('auth.logout') }}" class="media text-white">Logout</a></li>
                        </ul>
                    </div>
                </div>
            @break
            @case('admin')
                <div class="dropdown-menu dropdown-left dropdown-card admin dropdown-card-wide ">
                    <div class="card mb-2">
                        <ul class="list-unstyled sidecustom ">
                            <li><a href="{{ route('ubah_profil_admin') }}" class="media text-white">Ubah Profil</li>
                            <li><a href="{{ route('ubah_password_admin') }}" class="media text-white">Ubah Password</li>
                            <li><a href="{{ route('auth.logout') }}" class="media text-white">Logout</a></li>
                        </ul>
                    </div>
                </div>
            @break
            @case('peserta-ppdb')
                <div class="dropdown-menu dropdown-left dropdown-card user dropdown-card-wide ">
                    <div class="card mb-2">
                        <ul class="list-unstyled sidecustom ">
                            <li><a href="{{ route('ubah_profil_user') }}" class="media text-white">Ubah Profil</li>
                            <li><a href="{{ route('ubah_password_user') }}" class="media text-white">Ubah Password</li>
                            <li><a href="{{ route('auth.logout') }}" class="media text-white">Logout</a></li>
                        </ul>
                    </div>
                </div>
            @break
        @endswitch
    </li>
</ul>
