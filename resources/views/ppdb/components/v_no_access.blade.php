@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="widget-list">
    <div class="col-md-12 widget-holder">
        <div class="widget-bg">
            <div class="widget-body clearfix text-center">
                <h4>Akses ditolak!</h4>
                <p class="mr-t-10 mr-b-20">Anda tidak memiliki izin untuk mengakses di Halaman ini.</p>
                <p class="mr-t-10 mr-b-20">Anda belum melakukan pembayaran.silahkan klik tagihan pembayaran untuk melakukan konfirmasi.</p>
                <a href="{{ route('user-ppdb') }}" class="btn btn-info btn-lg btn-rounded mr-b-20 ripple">Kembali</a>
                <a href="{{ route('history-pembayaran') }}" class="btn btn-warning btn-lg btn-rounded mr-b-20 ripple">Tagihan Pembayaran</a>
            </div>
        </div>
    </div>
@endsection
