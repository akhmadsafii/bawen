@extends('ppdb.layouts.app')
@section('ppdb.components')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css" rel="stylesheet" type="text/css">
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <style>
        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 150px;
        }

        .address {
            cursor: pointer
        }

        .address:hover {
            color: #AA0000;
            text-decoration: underline
        }

        #mapSearchContainer {
            position: fixed;
            top: 20px;
            right: 40px;
            height: 30px;
            width: 180px;
            z-index: 110;
            font-size: 10pt;
            color: #5d5d5d;
            border: solid 1px #bbb;
            background-color: #f8f8f8;
        }

        .pointer {
            position: absolute;
            top: 86px;
            left: 60px;
            z-index: 99999;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta > Tambah Data Pendaftar </h5>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('add-save') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <span id="form_result"></span>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Nama <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nama" id="nama" autocomplete="off"
                                                        class="form-control" value="{{ old('nama') }}"
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja "
                                                        placeholder="nama">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">NISN <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nisn" id="nisn" autocomplete="off"
                                                        class="form-control" value="{{ old('nisn') }}"
                                                        pattern="[0-9/\s]*" title="Format harus berisi angka saja "
                                                        placeholder="Nisn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Email <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="email" name="email" id="email" autocomplete="off"
                                                        class="form-control" value="{{ old('email') }}"
                                                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                        title="Format email salah " placeholder="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Telepon
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="tel" name="telepon" id="telepon" autocomplete="off"
                                                        class="form-control" value="{{ old('telepon') }}"
                                                        pattern="[0-9/\s]*" title="Format harus berisi angka saja "
                                                        placeholder="telepon">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <p class="col-md-3 col-form-label">Alamat <span class="text-red">*</span>
                                            </p>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <textarea name="alamat" id="alamat" placeholder="Jl.Sukorejo-Parakan Km 3 Kabupaten Kendal , Jawa Tengah"
                                                        class="form-control"></textarea>
                                                </div>
                                                <!-- /.input-group -->
                                                <div id="results"></div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="map"></div>

                                        <div class="form-group row">

                                            <label class="col-md-3 col-form-label">Koordinat Latitude</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input class="form-control" type="lat" name="lat" id="lat"
                                                        placeholder="lat" value="">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Koordinat Longitude</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="long" id="lon"
                                                        placeholder="Long" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select class="form-control" name="jenkel" id="l1">
                                                        <option value="">--Pilih jenis kelamin ---</option>
                                                        <option value="l">Laki-Laki</option>
                                                        <option value="p">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tempat Lahir </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                        autocomplete="off" class="form-control"
                                                        value="{{ old('tempat_lahir') }}"
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja "
                                                        placeholder="tempat lahir">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Agama
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="agama" id="agama" autocomplete="off"
                                                        class="form-control" value="{{ old('agama') }}"
                                                        pattern="[a-zA-Z/\s]*" title="Format harus berisi huruf saja "
                                                        placeholder="Agama">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tanggal Lahir
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <input type="text" id="tanggal_lahir" name="tanggal_lahir"
                                                        value="{{ old('tanggal_lahir') ?? \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'>
                                                    <span class="input-group-addon"><i
                                                            class="list-icon material-icons">date_range</i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <label class="col-md-3 col-form-label" for="l1"></label>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label class="col-md-3 col-form-label pt-1" for="l1">Photo <span
                                                                class="text-red">*</span> </label>
                                                        <img id="modal-preview" src="https://via.placeholder.com/150"
                                                            alt="Preview" class="form-group mb-1" width="150px"
                                                            height="150px" style="margin-top: 10px">
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="form-group row">
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input id="image" type="file" name="image"
                                                                        accept="image/*" onchange="readURL(this);">
                                                                    <input type="hidden" name="hidden_image"
                                                                        id="hidden_image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Password<span
                                                                    class="text-red">*</span></p>
                                                            <div class="input-group">
                                                                <input id="password" type="password" name="password"
                                                                    class="form-control password" placeholder="xxxx"
                                                                    title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                                                <div class="input-group-addon toggle-password">
                                                                    <a href="javascript:void(0)"><i
                                                                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                                                </div>
                                                                <div class="input-group-addon">
                                                                    <a href="javascript:void(0)" id="generate_pass">Generate
                                                                        Password</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('akun-pendaftar') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let config;
            let table_doc;
            var myMarker;
            var map;
            var nzoom;
            var option;
            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        var blob = input.files[0];

                        if (blob.type == 'application/pdf' || blob.type == 'application/doc') {
                            $('#modal-preview').hide();
                            $('#start').hide();
                        } else {
                            reader.onload = function(e) {
                                $(id).attr('src', e.target.result);
                            };
                            reader.readAsDataURL(input.files[0]);
                            $('#modal-preview').show();
                            $('#modal-preview').removeClass('hidden');
                            $('#start').hide();
                        }

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                mata.addEventListener("click", () => {
                    mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPass.type === "password") {
                        inputPass.setAttribute("type", "text")

                    } else if (inputPass.type === "text") {
                        inputPass.setAttribute("type", "password")
                        mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                });

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPass.value = retVal;
                    inputPass.value = retVal;
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

                var startlat = '-6.991576';
                var startlon = '109.122923';

                options = {
                    center: [startlat, startlon],
                    zoom: 9
                }

                map = L.map('map', options);
                nzoom = 12;

                L.tileLayer(
                    'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=Ts9g8McLuNVEfjGFTHeG', {
                        attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
                    }).addTo(map);

                myMarker = L.marker([startlat, startlon], {
                    title: "Coordinates",
                    alt: "Coordinates",
                    draggable: true
                }).addTo(map).on('dragend', function() {
                    var lat = myMarker.getLatLng().lat.toFixed(8);
                    var lon = myMarker.getLatLng().lng.toFixed(8);
                    var czoom = map.getZoom();
                    if (czoom < 18) {
                        nzoom = czoom + 2;
                    }
                    if (nzoom > 18) {
                        nzoom = 18;
                    }
                    if (czoom != 18) {
                        map.setView([lat, lon], nzoom);
                    } else {
                        map.setView([lat, lon]);
                    }
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                    getAddressMaker(lat, lon);
                });

                window.getAddressMaker = function(latx, lonx) {
                    $.ajax({
                        url: "https://nominatim.openstreetmap.org/reverse",
                        data: {
                            lat: latx,
                            lon: lonx,
                            format: "json"
                        },
                        beforeSend: function(xhr) {},
                        dataType: "json",
                        type: "GET",
                        async: true,
                        crossDomain: true
                    }).done(function(res) {
                        var rows = JSON.parse(JSON.stringify(res.address));
                        var alamatx = res.display_name;
                        var kotax = rows.county || rows.city;
                        $('textarea[name="alamat"]').val(alamatx);
                        $('input[name="kota"]').val(kotax);
                        myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                            alamatx).openPopup();
                    }).fail(function(error) {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    });
                }

                window.chooseAddr = function chooseAddr(lat1, lng1) {
                    myMarker.closePopup();
                    map.setView([lat1, lng1], 18);
                    myMarker.setLatLng([lat1, lng1]);
                    lat = lat1.toFixed(8);
                    lon = lng1.toFixed(8);
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                    getAddressMaker(lat1, lng1);
                }

                $('body').on('cllick', '.address', function() {
                    $('textarea[name="alamat"]').val($(this).data('alamat'));
                });

                $('body').on('keyup', 'textarea[name="alamat"]', function() {
                    addr_search();
                });

                window.myFunction = function myFunction(arr) {
                    var out = "<br />";
                    var i;
                    if (arr.length > 0) {
                        for (i = 0; i < arr.length; i++) {
                            out +=
                                "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                                arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                                .display_name + "'>" + arr[i].display_name +
                                "</div>";
                        }
                        document.getElementById('results').innerHTML = out;
                    } else {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    }
                }

                window.addr_search = function addr_search() {
                    var inp = document.getElementById("alamat");
                    var xmlhttp = new XMLHttpRequest();
                    var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                        .value;
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var myArr = JSON.parse(this.responseText);
                            myFunction(myArr);
                        }
                    };
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }


            });

        })(jQuery, window);
    </script>
@endsection
