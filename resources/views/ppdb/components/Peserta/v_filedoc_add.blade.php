@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta > Tambah Dokumen Pendukung </h5>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPeserta" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Nama Peserta</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Nama Peserta</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('doc-save') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <span id="form_result"></span>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="hidden" name="id" id="id_doc">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Peserta
                                                <span class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group ">
                                                    <input type="text" name="id_peserta" id="id_peserta" autocomplete="off"
                                                        class="form-control" value="{{ old('id_peserta') }}"
                                                        readonly="readonly" pattern="[0-9/\s]*"
                                                        title="Format harus berisi angka saja " placeholder="id peserta">
                                                    <span class="input-group-addon btn btn-info "><i
                                                            class="material-icons list-icon search">search</i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Nama <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nama" id="nama" autocomplete="off"
                                                        class="form-control" value="{{ old('nama') }}"
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja "
                                                        placeholder="nama">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label pt-1" for="l1">File <span
                                                    class="text-red">*</span> </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input id="image" type="file" name="image" accept="image/*,doc,docx,pdf"
                                                        onchange="readURL(this);">
                                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row mb-0">
                                            <label class="col-md-3 col-form-label" for="l1"></label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <img id="modal-preview" src="https://via.placeholder.com/300"
                                                            alt="Preview" class="form-group mb-1" width="auto" height="auto"
                                                            style="margin-top: 10px">
                                                    </div>
                                                    <div class="col-md-3" style="position: relative">
                                                        <div id="delete_foto"
                                                            style="position: absolute; top:0;left:0; bottom: 0;display:none;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('doc-support') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let config;
            let table_doc;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        var blob = input.files[0];

                        if (blob.type == 'application/pdf' || blob.type == 'application/doc') {
                            $('#modal-preview').hide();
                            $('#start').hide();
                        } else {
                            reader.onload = function(e) {
                                $(id).attr('src', e.target.result);
                            };
                            reader.readAsDataURL(input.files[0]);
                            $('#modal-preview').show();
                            $('#modal-preview').removeClass('hidden');
                            $('#start').hide();
                        }

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                $('body').on('click', '.search', function() {
                    $('#ajaxModelShowPeserta').modal('show');
                    $('#modelHeading').html("Daftar Peserta PPDB");

                    config = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('data-peserta') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nisn',
                                name: 'nisn'
                            },
                            {
                                data: 'nama_peserta',
                                name: 'nama_peserta'
                            },
                            {
                                data: 'status',
                                name: 'status',
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_doc = $('#table_doc').dataTable(config);

                });

                $('body').on('click', '.download.pilih', function() {
                    var id = $(this).data('id');
                    $('input[name="id_peserta"]').val(id);
                    $('#ajaxModelShowPeserta').modal('hide');
                });

            });

        })(jQuery, window);
    </script>
@endsection
