@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Form Pendaftaran PPDB </h5>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPeserta" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table id="table_doc" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Nama Peserta</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Nama Peserta</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('save-register2') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l13">ID Peserta</label>
                                <div class="col-md-9">
                                    <div class="input-group ">
                                        <input type="text" name="id_peserta" id="id_peserta" autocomplete="off"
                                            class="form-control" readonly="readonly" pattern="[0-9/\s]*"
                                            title="Format harus berisi angka saja " placeholder="id peserta">
                                        <span class="input-group-addon btn btn-info "><i
                                                class="material-icons list-icon search">search</i></span>
                                    </div>
                                </div>
                            </div>
                            @if (!empty($form))
                                @foreach ($form as $key => $val)
                                    @if (!empty($val['tipe']))
                                        @switch($val['tipe'])
                                            @case('option')
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="l13">{{ $val['nama'] }}</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" id="{{ $val['id'] }}"
                                                            name="form[{{ $val['id'] }}]">
                                                            @if ($val['initial'] == 'jenkel')
                                                                <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }}
                                                                    ---
                                                                </option>
                                                                @foreach (explode(',', $val['value']) as $info)
                                                                    <option value="l">
                                                                        {{ $info == 'l' ? 'Laki-Laki' : 'Perempuan' }}</option>
                                                                    <option value="p">
                                                                        {{ $info == 'p' ? 'Laki-laki' : 'Perempuan' }}</option>
                                                                @endforeach
                                                            @elseif ($val['initial'] == 'jurusan' || $val['initial'] == 'jurusan2' || $val['initial'] == 'jurusan3' || $val['initial'] == 'jurusan_diterima')
                                                                <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }}
                                                                    ---
                                                                </option>
                                                                @foreach (explode(',', $val['value']) as $info)
                                                                    <option value="{{ $info }}">{{ $info }}
                                                                    </option>
                                                                @endforeach
                                                            @elseif($val['initial'] == 'gelombang')
                                                                <option value="1"> Gelombang 1
                                                                </option>
                                                                <option value="2"> Gelombang 2
                                                                </option>
                                                                <option value="3"> Gelombang 3
                                                                </option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            @break

                                            @case('date')
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="input-group input-has-value">
                                                            <input type="text" id="{{ $val['id'] }}"
                                                                name="form[{{ $val['id'] }}]"
                                                                value="{{ old('value') ?? ($val['value'] ?? '') }}"
                                                                readonly="readonly" class="form-control datepicker"
                                                                data-date-format="yyyy-mm-dd"
                                                                data-plugin-options='{"autoclose": true}'>
                                                            <span class="input-group-addon"><i
                                                                    class="list-icon material-icons">date_range</i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @break

                                            @case('text')
                                                @if ($val['initial'] == 'jalur_pendaftaran')
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label"
                                                            for="l13">{{ $val['nama'] }}</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control" id="{{ $val['id'] }}"
                                                                name="form[{{ $val['id'] }}]">
                                                                <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }}
                                                                    ---
                                                                </option>
                                                                @foreach (explode(',', $val['value']) as $info)
                                                                    <option value="{{ $info }}">{{ $info }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" name="form[{{ $val['id'] }}]"
                                                                    id="{{ $val['id'] }}" autocomplete="off"
                                                                    class="form-control" value=""
                                                                    title="Format harus berisi angka saja "
                                                                    placeholder="{{ $val['nama'] }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @break

                                            @case('textarea')
                                                <div class="form-group row ">
                                                    <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="input-group input-has-value">
                                                            <textarea name="form[{{ $val['id'] }}]" id="{{ $val['id'] }}" rows="5" cols="5" class="form-control text-left"
                                                                placeholder="{{ $val['nama'] }}">
                                                                                              {{ old('value') ?? ($val['value'] ?? '') }}
                                         </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            @break
                                        @endswitch
                                    @endif
                                @endforeach
                            @endif



                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Tambah
                                            </button>
                                            <a class="btn btn-info" href="{{ route('akun-pendaftar') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_;
            $(document).ready(function() {
                $('body').on('click', '.search', function() {
                    $('#ajaxModelShowPeserta').modal('show');
                    $('#modelHeading').html("Daftar Peserta PPDB");

                    config = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, {
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewAkun'
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('data-peserta') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nisn',
                                name: 'nisn'
                            },
                            {
                                data: 'nama_peserta',
                                name: 'nama_peserta'
                            },
                            {
                                data: 'status',
                                name: 'status',
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_ = $('#table_doc').dataTable(config);
                });

                $('body').on('click', '.download.pilih', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_peserta"]').val(rows.id);
                                    $('input[name="form[8]"]').val(rows.nama);
                                    $('input[name="form[9]"]').val(rows.nisn);
                                    $('#ajaxModelShowPeserta').modal('hide');
                                }
                            }

                        }
                    });
                });

                $('body').on('click', '#createNewAkun', function() {
                    window.open('{{ route('add-form') }}', '_blank');
                });
            });

        })(jQuery, window);
    </script>
@endsection
