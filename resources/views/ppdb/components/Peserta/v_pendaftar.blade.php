@extends('ppdb.layouts.app')
@section('ppdb.components')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css" rel="stylesheet" type="text/css">
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <style>
        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 150px;
        }

        .address {
            cursor: pointer
        }

        .address:hover {
            color: #AA0000;
            text-decoration: underline
        }

        #mapSearchContainer {
            position: fixed;
            top: 20px;
            right: 40px;
            height: 30px;
            width: 180px;
            z-index: 110;
            font-size: 10pt;
            color: #5d5d5d;
            border: solid 1px #bbb;
            background-color: #f8f8f8;
        }

        .pointer {
            position: absolute;
            top: 86px;
            left: 60px;
            z-index: 99999;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta >Akun</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Akun
                </li>
                <li class="breadcrumb-item"><a href="{{ route('register-ulang') }}">Register</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('doc-support') }}">Dokumen Pendukung</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('peserta_diterima') }}">Pendaftar Diterima</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('peserta_ditolak') }}">Pendaftar Ditolak</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- popup detail -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="result-body"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEditPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="PForm" name="PForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeadingx"> Edit Form Peserta Pendaftar
                        </h5>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row page-title clearfix mb-3 ">
                                    <div class="page-title-left">
                                    </div>
                                    <div class="page-title-right d-inline-flex">
                                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*)
                                            Form harus disi!.</p>
                                    </div>
                                </div>
                                <input type="hidden" name="id_peserta" class="form-control ">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NISN <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nisn" id="nisn" autocomplete="off"
                                                class="form-control" pattern="[0-9/\s]*"
                                                title="Format harus berisi angka saja " placeholder="Nisn">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="email" name="email" id="email" autocomplete="off"
                                                class="form-control" value="{{ old('email') }}"
                                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                title="Format email salah " placeholder="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="jenkel" id="l1">
                                                <option value="">--Pilih jenis kelamin ---</option>
                                                <option value="l">Laki-Laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p class="mr-b-0">Alamat <span class="text-red">*</span></p>
                                    <div class="input-group">
                                        <textarea name="alamat" id="alamat" placeholder="Jl.Sukorejo-Parakan Km 3 Kabupaten Kendal , Jawa Tengah"
                                            class="form-control"></textarea>
                                    </div>
                                    <!-- /.input-group -->
                                    <div id="results"></div>
                                </div>
                                <div class="form-group" id="map"></div>

                                <div class="form-group row">

                                    <label class="mr-b-0">Koordinat Latitude</label>
                                    <div class="input-group">
                                        <input class="form-control" type="lat" name="lat" id="lat" placeholder="lat"
                                            value="">
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="mr-b-0">Koordinat Longitude</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="long" id="lon" placeholder="Long"
                                            value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="status">Status<span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="status" id="status">
                                                <option value="">--Pilih Status Pendaftar ---</option>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="col-md-3 col-form-label pt-1" for="l1">Photo </label>
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="150px" height="150px"
                                                    style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input id="image" type="file" name="image" accept="image/*"
                                                                onchange="readURL(this);">
                                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <p class="mr-b-0">Password<span
                                                            class="text-red">*</span>
                                                    </p>
                                                    <div class="input-group">
                                                        <input id="password" type="password" name="password"
                                                            class="form-control password" placeholder="xxxx"
                                                            title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                                        <div class="input-group-addon toggle-password">
                                                            <a href="javascript:void(0)"><i
                                                                    class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                                        </div>
                                                        <div class="input-group-addon">
                                                            <a href="javascript:void(0)" id="generate_pass">Generate</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update jadwal">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Tempat Sampah  -->
    <div class="modal modal-primary fade bs-modal-lg-primary" id="ajaxModelTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="ajaxModelTrashLabel"><i class="list-icon fa fa-trash"></i> Tempat
                        Sampah </h5>
                </div>
                <div class="modal-body">
                    <table id="table_peserta_trash" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Peserta</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama Peserta</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_pendaftar_peserta" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <script>
        (function($, global) {
            "use-strict"

            let table_doc;
            let config, config_trash;
            var myMarker;
            var map;
            var nzoom;
            var option;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewPeserta'
                            }
                        },
                        /*{
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },*/
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('data-pesertav') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama_peserta',
                            name: 'nama_peserta'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'photo',
                            name: 'photo',
                        },
                        {
                            data: 'jenkel',
                            name: 'jenkel',
                        },
                        {
                            data: 'telepon',
                            name: 'telepon',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                    rowReorder: {
                        selector: 'td:nth-child(2)'
                    },
                };

                table_doc = $('#table_pendaftar_peserta').dataTable(config);

                //open modal form redirect
                $('#createNewPeserta').click(function() {
                    window.location.href = '{{ route('add-form') }}';
                });

                $('body').on('click', '.show.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    let profiltag = "";
                    let alamat = '';
                    let email = '';
                    let telepon = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Peserta ");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    bodytag = bodytag + '<div class="row">' +
                                        '<div class="col-md-12 text-center">' +
                                        '<div class="col-md-12 col-lg-12 widget-holder">' +
                                        '<div class="widget-bg">' +
                                        '<div class="widget-body clearfix">' +
                                        '<div class="contact-info">' +
                                        '<header class="text-center">' +
                                        '<figure class="inline-block user--online thumb-lg">' +
                                        '<img src="{{ env('API_URL') }}/public/' + rows
                                        .file +
                                        '" class="rounded-circle img-thumbnail" alt="">' +
                                        '</figure>' +
                                        '<h4 class="mt-1"><a href="#">' + rows.nama +
                                        '</a></h4>' +
                                        '<p>' + rows.nisn + '</p>' +
                                        '</header>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                    $('.result-body').html(bodytag);

                                    $('#ajaxModelShowPendaftar').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Jadwal Kegiatan");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowPendaftar').modal('show');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var gambar = $(this).data('gambar');
                    var url = '{{ route('edit-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    let status;
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Pendaftar");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    //console.log(result['data']);
                                    $('input[name="id_peserta"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="nisn"]').val(rows.nisn);
                                    $('input[name="email"]').val(rows.email);
                                    if (rows.jenkel != ''){
                                            $("select[name='jenkel'] > option[value=" + rows
                                            .jenkel + "]").prop("selected", true);
                                        }else{
                                            $("select[name='jenkel'] > option[value='p']").prop("selected", true);
                                        }

                                    if (rows.status == 'Aktif') {
                                        status = '1';
                                    } else if (rows.status == 'Tidak Aktif') {
                                        status = '0';
                                    }
                                    $("select[name='status'] > option[value=" + status +
                                        "]").prop("selected", true);

                                    if (rows.file !== null || gambar !== null || gambar !=
                                        'null') {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src', rows.file);
                                    } else {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src',
                                            'https://via.placeholder.com/150');
                                    }
                                    $('#ajaxModelEditPendaftar').modal('show');

                                    $('input[name="lat"]').val(rows.latitude);
                                    $('input[name="long"]').val(rows.longitude);
                                    $('textarea[name="alamat"]').val(rows.alamat);

                                    window.getMakerMap(rows.latitude, rows.longitude);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Pendaftar");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelEditPendaftar').modal('show');
                                }
                            }

                        }
                    });
                });

                //update
                $('body').on('submit', '#PForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_peserta"]').val();
                    var urlx = '{{ route('update-pendaftar', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEditPendaftar').modal('hide');
                                    table_doc.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                    document.forms["PForm"].reset();

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove  data
                $('body').on('click', 'button.remove.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                $('body').on('click', 'button#data_trash', function() {
                    $('#ajaxModelTrash').modal('show');
                    config_trash = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('trash-peserta') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    table_trash = $('#table_peserta_trash').dataTable(config_trash);
                });

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_doc.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function restore data
                window.restoreData = function restore(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_doc.fnDraw(false);
                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-undo"></i> restore');

                                    window.notif(result['info'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-undo"></i> restore');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function forcedelete
                window.ForceDelete = function clear(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');

                                    window.notif(result['info'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();
                    }
                };

                window.getExt = function getx(path) {
                    var basename = path.split(/[\\/]/).pop(), // extract file name from full path ...
                        // (supports `\\` and `/` separators)
                        pos = basename.lastIndexOf("."); // get last position of `.`

                    if (basename === "" || pos < 1) // if file name is empty or ...
                        return ""; //  `.` not found (-1) or comes first (0)

                    return basename.slice(pos + 1);
                }

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPass.value = retVal;
                    inputPass.value = retVal;
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

                window.getMakerMap = function makerMap(startlat, startlon) {
                    options = {
                        center: [startlat, startlon],
                        zoom: 9
                    }

                    map = L.map('map', options);
                    nzoom = 12;

                    L.tileLayer(
                        'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=Ts9g8McLuNVEfjGFTHeG', {
                            attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
                        }).addTo(map);

                    myMarker = L.marker([startlat, startlon], {
                        title: "Coordinates",
                        alt: "Coordinates",
                        draggable: true
                    }).addTo(map).on('dragend', function() {
                        var lat = myMarker.getLatLng().lat.toFixed(8);
                        var lon = myMarker.getLatLng().lng.toFixed(8);
                        var czoom = map.getZoom();
                        if (czoom < 18) {
                            nzoom = czoom + 2;
                        }
                        if (nzoom > 18) {
                            nzoom = 18;
                        }
                        if (czoom != 18) {
                            map.setView([lat, lon], nzoom);
                        } else {
                            map.setView([lat, lon]);
                        }
                        document.getElementById('lat').value = lat;
                        document.getElementById('lon').value = lon;
                        getAddressMaker(lat, lon);
                    });

                }

                window.getAddressMaker = function(latx, lonx) {
                    $.ajax({
                        url: "https://nominatim.openstreetmap.org/reverse",
                        data: {
                            lat: latx,
                            lon: lonx,
                            format: "json"
                        },
                        beforeSend: function(xhr) {},
                        dataType: "json",
                        type: "GET",
                        async: true,
                        crossDomain: true
                    }).done(function(res) {
                        var rows = JSON.parse(JSON.stringify(res.address));
                        var alamatx = res.display_name;
                        var kotax = rows.county || rows.city;
                        $('textarea[name="alamat"]').val(alamatx);
                        $('input[name="kota"]').val(kotax);
                        myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                            alamatx).openPopup();
                    }).fail(function(error) {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    });
                }

                window.chooseAddr = function chooseAddr(lat1, lng1) {
                    myMarker.closePopup();
                    map.setView([lat1, lng1], 18);
                    myMarker.setLatLng([lat1, lng1]);
                    lat = lat1.toFixed(8);
                    lon = lng1.toFixed(8);
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                    getAddressMaker(lat1, lng1);
                }

                $('body').on('cllick', '.address', function() {
                    $('textarea[name="alamat"]').val($(this).data('alamat'));
                });

                $('body').on('keyup', 'textarea[name="alamat"]', function() {
                    addr_search();
                });

                window.myFunction = function myFunction(arr) {
                    var out = "<br />";
                    var i;
                    if (arr.length > 0) {
                        for (i = 0; i < arr.length; i++) {
                            out +=
                                "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                                arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                                .display_name + "'>" + arr[i].display_name +
                                "</div>";
                        }
                        document.getElementById('results').innerHTML = out;
                    } else {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    }
                }

                window.addr_search = function addr_search() {
                    var inp = document.getElementById("alamat");
                    var xmlhttp = new XMLHttpRequest();
                    var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                        .value;
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var myArr = JSON.parse(this.responseText);
                            myFunction(myArr);
                        }
                    };
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
