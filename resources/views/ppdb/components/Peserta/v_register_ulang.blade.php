@extends('ppdb.layouts.app')
@section('ppdb.components')
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js" defer></script>
    <style>
        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 250px;
        }

        .address {
            cursor: pointer
        }

        .address:hover {
            color: #AA0000;
            text-decoration: underline
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta >Register</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('akun-pendaftar') }}">Akun</a>
                </li>
                <li class="breadcrumb-item active"> Register
                </li>
                <li class="breadcrumb-item"><a href="{{ route('doc-support') }}">Dokumen Pendukung</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('peserta_diterima') }}">Pendaftar Diterima</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('peserta_ditolak') }}">Pendaftar Ditolak</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif


    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEditPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="UpdateForm" name="UpdateForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeadingx"> Edit Form Peserta Pendaftar
                        </h5>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row page-title clearfix mb-3 ">
                                    <div class="page-title-left">
                                    </div>
                                    <div class="page-title-right d-inline-flex">
                                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*)
                                            Form harus disi!.</p>
                                    </div>
                                </div>
                                <input type="hidden" name="id_peserta" class="form-control ">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama"
                                                disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NISN
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nisn" id="nisn" autocomplete="off"
                                                class="form-control" pattern="[0-9/\s]*"
                                                title="Format harus berisi angka saja " placeholder="Nisn"
                                                disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="jenkel" id="l1" disabled="disabled">
                                                <option disabled="disabled">--Pilih jenis kelamin ---</option>
                                                <option value="l">Laki-Laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="col-md-3 col-form-label pt-1" for="l1">Photo </label>
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="150px" height="150px"
                                                    style="margin-top: 10px">
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="status">Keputusan<span
                                                        class="text-red">*</span></label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <select class="form-control" name="keputusan" id="keputusan">
                                                            <option value="">--Pilih Keputusan ---</option>
                                                            <option value="1">Diterima</option>
                                                            <option value="2">Ditolak</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="status">Peta Zonasi</label>
                                    <div class="col-md-12">
                                        <div id="map"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Catatan
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea class="form-control" id="catatan" name="catatan" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update jadwal">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelStatusEditPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="UpdateFormStatus" name="UpdateFormStatus" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeadingx"> Edit Form Peserta Status
                            Pendaftar Massal
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" name="id_peserta_siswa" id="id_peserta_siswa">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="status">Keputusan<span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="keputusan" id="keputusan">
                                                <option value="">--Pilih Keputusan ---</option>
                                                <option value="1">Diterima</option>
                                                <option value="2">Ditolak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Catatan
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea class="form-control" id="catatan" name="catatan" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info statusx">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <label><input type="checkbox" class="selectAll" name="checkall" value="1"> Check / Uncheck
                            All</label>
                        <table id="table_pendaftar_peserta_register" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Jarak Zonasi </th>
                                    <th>Keputusan</th>
                                    <th>#</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Jarak Zonasi </th>
                                    <th>Keputusan</th>
                                    <th>#</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            let table_reg;
            let config, config_trash;
            $(document).ready(function() {
                var x = '{{ session('koordinat_latitude') }}';
                var y = '{{ session('koordinat_longitude') }}';

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewPeserta'
                            }
                        },
                        {
                            text: '<i class="fa fa-pencil"></i>',
                            attr: {
                                title: 'Update Status',
                                id: 'createUpdate'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('data-register-peserta') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama_peserta',
                            name: 'nama_peserta'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'photo',
                            name: 'photo',
                        },
                        {
                            data: 'jenkel',
                            name: 'jenkel',
                        },
                        {
                            data: 'telepon',
                            name: 'telepon',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'jarak_zonasi',
                            name: 'jarak_zonasi'
                        },
                        {
                            data: 'keputusan',
                            name: 'keputusan'
                        },
                        {
                            data: 'check',
                            name: 'check',
                            orderable: false,
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                    rowReorder: {
                        selector: 'td:nth-child(2)'
                    },
                };

                table_reg = $('#table_pendaftar_peserta_register').dataTable(config);

                // Handle click on "Select all" control
                $('.selectAll').on('click', function() {
                    // Get all rows with search applied
                    var rows = table_reg.api().rows({
                        'search': 'applied'
                    }).nodes();
                    // Check/uncheck checkboxes for all rows in the table
                    $('input[type="checkbox"]', rows).prop('checked', this.checked);
                });

                // Handle click on checkbox to set state of "Select all" control
                $('#table_pendaftar_peserta_register tbody').on('change', 'input[type="checkbox"]', function() {
                    // If checkbox is not checked
                    if (!this.checked) {
                        var el = $('.selectAll').get(0);
                        // If "Select all" control is checked and has 'indeterminate' property
                        if (el && el.checked && ('indeterminate' in el)) {
                            // Set visual state of "Select all" control
                            // as 'indeterminate'
                            el.indeterminate = true;
                        }
                    }
                });

                $('body').on('click', '#createUpdate', function() {
                    var checkbox = $('input[name="listbox[]"]:checked').length;
                    if (checkbox == 0) {
                        window.notif('error', 'Silahkan Pilih Peserta Register  Terlebih Dahulu !');
                    } else {
                        var peserta = $('input[name="listbox[]"]:checked').map(function() {
                            return this.value; // $(this).val()
                        }).get();
                        $('input[name="id_peserta_siswa"]').val(peserta);
                        $('#ajaxModelStatusEditPendaftar').modal('show');
                    }
                });

                 //update
                 $('body').on('submit', '#UpdateFormStatus', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('update_status_massal_ppdb') }}';

                    var formData = new FormData(this);

                    const loader = $('button.statusx');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelStatusEditPendaftar').modal('hide');
                                    $('input[name="checkall"]').prop('checked',false);
                                    $('input[name="id_peserta_siswa"]').val('');
                                    table_reg.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });
                });


                //open modal form redirect
                $('#createNewPeserta').click(function() {
                    window.location.href = '{{ route('register-peserta') }}';
                });

                $('body').on('click', '.edit.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var gambar = $(this).data('gambar');
                    var url = '{{ route('edit-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Register");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('input[name="id_peserta"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="nisn"]').val(rows.nisn);

                                    if (typeof rows.jenkel !== 'underfined') {
                                        if (rows.jenkel != '') {
                                            $("select[name='jenkel'] > option[value=" + rows
                                                .jenkel + "]").prop("selected", true);
                                        } else {
                                            $("select[name='jenkel']").removeAttr(
                                                'disabled');
                                        }
                                    }
                                    var log_keputusan = 0;
                                    if (rows.keputusan == 'Diterima') {
                                        log_keputusan = 1;
                                    } else if (rows.keputusan == 'Ditolak') {
                                        log_keputusan = 2;
                                    }

                                    $("select[name='keputusan'] > option[value=" +
                                        log_keputusan + "]").prop("selected", true);

                                    if (rows.file !== null || gambar !== null) {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src', gambar);
                                    } else {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src',
                                            'https://via.placeholder.com/150');
                                    }

                                    $('#ajaxModelEditPendaftar').modal('show');

                                    window.Makerzonasi(rows.latitude, rows.longitude, x, y);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Register");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelEditPendaftar').modal('show');
                                }
                            }

                        }
                    });
                });

                window.Makerzonasi = function petazonasi(startlat, startlon, x, y) {
                    var options = {
                        center: [startlat, startlon],
                        zoom: 7
                    }

                    // Creating a map object
                    var map = new L.map('map', options);

                    // Creating a Layer object
                    var layer = new L.TileLayer(
                        'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=Ts9g8McLuNVEfjGFTHeG'
                    );

                    // Adding layer to the map
                    map.addLayer(layer);

                    var myMarker = L.marker([startlat, startlon], {
                        title: "Coordinates Student",
                        alt: "Coordinates Student",
                        draggable: false
                    }).addTo(map).on('dragend', function() {
                        var lat = myMarker.getLatLng().lat.toFixed(8);
                        var lon = myMarker.getLatLng().lng.toFixed(8);
                        var czoom = map.getZoom();
                        if (czoom < 18) {
                            nzoom = czoom + 2;
                        }
                        if (nzoom > 18) {
                            nzoom = 18;
                        }
                        if (czoom != 18) {
                            map.setView([lat, lon], nzoom);
                        } else {
                            map.setView([lat, lon]);
                        }
                    });

                    var myMarkerx = L.marker([x, y], {
                        title: "Coordinates School",
                        alt: "Coordinates School",
                        draggable: false
                    }).addTo(map).on('dragend', function() {
                        var lat = myMarker.getLatLng().lat.toFixed(8);
                        var lon = myMarker.getLatLng().lng.toFixed(8);
                        var czoom = map.getZoom();
                        if (czoom < 18) {
                            nzoom = czoom + 2;
                        }
                        if (nzoom > 18) {
                            nzoom = 18;
                        }
                        if (czoom != 18) {
                            map.setView([x, y], nzoom);
                        } else {
                            map.setView([x, y]);
                        }
                    });

                    // Creating multi polygon options
                    var multiPolygonOptions = {
                        color: 'red',
                        weight: 5
                    };

                    var latlang = [
                        [x, y],
                        [startlat, startlon],
                    ];

                    if (latlang.length > 0) {
                        try {
                            // Creating multi polygons
                            var multipolygon = L.multiPolygon(latlang, multiPolygonOptions);
                            // Adding multi polygon to map
                            multipolygon.addTo(map);
                        } catch (err) {
                            //console.log(err);
                        }

                    }

                }

                //update
                $('body').on('submit', '#UpdateForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_peserta"]').val();
                    var urlx = '{{ route('update-pendaftar-status', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update.feed');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEditPendaftar').modal('hide');
                                    table_reg.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
