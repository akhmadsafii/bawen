@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left">
        <h5 class="mr-0 mr-r-5"> Detail Peserta Register PPDB {{ $tahun_ajaran }}  </h5>
    </div>
    <div class="page-title-right d-inline-flex">
        <a class="aler-info" href="{{ route('register-ulang') }}">
            <i class="material-icons list-icon">keyboard_arrow_left</i>
            Kembali
        </a>
        &nbsp; &nbsp;
        <a class="aler-success" href="{{ route('print_register_peserta',['id'=>$id_peserta]) }}" target="_blank">
            <i class="fa fa-print"></i>
            Print
        </a>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        @foreach ( $detail_info as $key => $val )
                            <div class="col-md-4">
                                <h6 class="text-muted text-uppercase">{{ $val['nama_form'] }}</h6>
                                @switch($val['value'])
                                    @case('')
                                    <p class="text-muted mr-t-0">-<p>
                                    @break
                                    @case('l')
                                    <p class="text-muted mr-t-0"> Laki-Laki<p>
                                    @break
                                    @case('p')
                                    <p class="text-muted mr-t-0"> Perempuan<p>
                                    @break
                                    @default
                                    <p class="mr-t-0">{{ $val['value']}}</p>

                                @endswitch

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
