@extends('ppdb.layouts.app')
@section('ppdb.components')
    <style>
        /* CSS talk bubble */
        .talk-bubble {
            margin: 40px;
            display: inline-block;
            position: relative;
            width: 75vw;
            height: auto;
            background-color: lightyellow;
        }

        @media screen and (orientation:landscape) {

            /* Your CSS Here*/
            .talk-bubble {
                margin: 24px;
                display: inline-block;
                position: relative;
                width: 242px;
                height: auto;
                background-color: lightyellow;
            }
        }

        .border {
            border: 8px solid #666;
        }

        .round {
            border-radius: 30px;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;

        }

        /* Right triangle placed top left flush. */
        .tri-right.border.left-top:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: -8px;
            bottom: auto;
            border: 32px solid;
            border-color: #666 transparent transparent transparent;
        }

        .tri-right.left-top:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 0px;
            bottom: auto;
            border: 22px solid;
            border-color: lightyellow transparent transparent transparent;
        }

        /* Right triangle, left side slightly down */
        .tri-right.border.left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: 30px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.left-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 38px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /*Right triangle, placed bottom left side slightly in*/
        .tri-right.border.btm-left:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -8px;
            right: auto;
            top: auto;
            bottom: -40px;
            border: 32px solid;
            border-color: transparent transparent transparent #666;
        }

        .tri-right.btm-left:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 0px;
            right: auto;
            top: auto;
            bottom: -20px;
            border: 22px solid;
            border-color: transparent transparent transparent lightyellow;
        }

        /*Right triangle, placed bottom left side slightly in*/
        .tri-right.border.btm-left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 30px;
            right: auto;
            top: auto;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 transparent transparent #666;
        }

        .tri-right.btm-left-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 38px;
            right: auto;
            top: auto;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow transparent transparent lightyellow;
        }

        /*Right triangle, placed bottom right side slightly in*/
        .tri-right.border.btm-right-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 30px;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.btm-right-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 38px;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /*
             left: -8px;
              right: auto;
              top: auto;
             bottom: -40px;
             border: 32px solid;
             border-color: transparent transparent transparent #666;
             left: 0px;
              right: auto;
              top: auto;
             bottom: -20px;
             border: 22px solid;
             border-color: transparent transparent transparent lightyellow;

            /*Right triangle, placed bottom right side slightly in*/
        .tri-right.border.btm-right:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -8px;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.btm-right:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 0px;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /* Right triangle, right side slightly down*/
        .tri-right.border.right-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -40px;
            top: 30px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 transparent transparent #666;
        }

        .tri-right.right-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -20px;
            top: 38px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow transparent transparent lightyellow;
        }

        /* Right triangle placed top right flush. */
        .tri-right.border.right-top:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -40px;
            top: -8px;
            bottom: auto;
            border: 32px solid;
            border-color: #666 transparent transparent transparent;
        }

        .tri-right.right-top:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -20px;
            top: 0px;
            bottom: auto;
            border: 20px solid;
            border-color: lightyellow transparent transparent transparent;
        }

        /* talk bubble contents */
        .talktext {
            padding: 1em;
            text-align: left;
            line-height: 1.5em;
        }

        .talktext p {
            /* remove webkit p margins */
            -webkit-margin-before: 0em;
            -webkit-margin-after: 0em;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pesan </h5>
        </div>
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!--- Modal show -->
    <div class="modal modal-danger fade bs-modal-md" id="showDetailPesan" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel">Detail Pesan</h5>
                </div>
                <div class="modal-body">
                    <div class="mail-single-message">
                        <hr>
                        <div class="talk-bubble tri-right left-top">
                            <h2 class="fw-100 h1 sub-heading-font-family ml-3 mt-1 mr-b-30 judul_pesan"></h2>
                            <div class="talktext ml-3">
                                <p class="nama_peserta"></p>
                                <div class="pesan-message"></div>
                                <div class="mail-attachment">
                                </div>
                            </div>
                        </div>
                        <!-- /.mail-attachment -->
                        <hr>
                        <p class="text-muted">Tanggapan</p>
                        <section class="scrollbar-enabled scroll-to-bottom ps ps--theme_default ps--active-y">
                            <ul class="list-unstyled pd-t-20 mr-l-20 list-tanggapan">
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!--- Modal show Edit -->
    <div class="modal modal-danger fade bs-modal-lg" id="EditPesan" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="PesanForm" name="PesanForm" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header text-inverse">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myMediumModalLabel">Edit Pesan</h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id_pesan" id="id_pesan" class="form-control">
                        <input type="hidden" name="id_peserta" id="id_peserta" class="form-control">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="l30">Judul Pesan
                                    <span class="text-red">*</span>
                                </label>
                                <input type="text" name="nama" id="nama" autocomplete="off" class="form-control"
                                    pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                    title="Format harus berisi alfanumerik atau huruf saja " placeholder="judul pesan">
                            </div>
                        </div>
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label for="l1">Pesan
                                    <span class="text-red">*</span>
                                </label>
                                <textarea name="pesan" id="pesan" rows="5" cols="5" class="form-control editor text-left" placeholder="isi pesan">
                                                                </textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-3 col-form-label" for="l1"></label>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                        </label>
                                        <img id="modal-preview2" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group mb-1" width="150px" height="150px" style="margin-top: 10px">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input id="image" type="file" name="image" accept="image/*"
                                                        onchange="readURL2(this);">
                                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"> <i
                                class="material-icons list-icon">save</i> Simpan</button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!--- Modal balas -->
    <div class="modal modal-danger fade bs-modal-lg" id="showBalasPesan" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myMediumModalLabel"><i class="fa fa-send"></i> Balas Pesan</h5>
                </div>
                <form id="PesanFormX" name="PesanFormX" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <section class="scrollbar-enabled scroll-to-bottom ps ps--theme_default ps--active-y">
                            <ul class="list-unstyled pd-t-20 mr-l-20 list-balasan" style="max-height: 200px;">
                            </ul>
                        </section>
                        <hr>
                        <div class="col-md-12 ">
                            <input type="hidden" name="id_pesan" id="id_pesan" class="form-control">
                            <input type="hidden" name="id_user" id="id_user" class="form-control">
                            <div class="form-group">
                                <label for="l1">Pesan
                                    <span class="text-red">*</span>
                                </label>
                                <textarea name="pesan" id="pesan" rows="5" cols="5" class="form-control editor text-left" placeholder="isi pesan">
                                                </textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-3 col-form-label" for="l1"></label>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                        </label>
                                        <img id="modal-preview3" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group mb-1" width="150px" height="150px" style="margin-top: 10px">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input id="image" type="file" name="image" accept="image/*"
                                                        onchange="readURL3(this);">
                                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"> <i
                                class="fa fa-send"></i> Balas</button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>


    <!-- Modal Tempat Sampah  -->
    <div class="modal modal-primary fade bs-modal-lg-primary" id="ajaxModelTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="ajaxModelTrashLabel"><i class="list-icon fa fa-trash"></i> Tempat
                        Sampah </h5>
                </div>
                <div class="modal-body">
                    <table id="table_trash_peserta" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Append Create Datatables-->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_pesan_peserta" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pesan Dari</th>
                                    <th>judul</th>
                                    <th>Tanggapan</th>
                                    <th>Sesi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Admin</th>
                                    <th>judul</th>
                                    <th>Tanggapan</th>
                                    <th>Sesi</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <script>
        (function($, global) {
            "use-strict"

            let table_pesan_peserta;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewPesan'
                            }
                        },
                        {
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('source_ajax_pesan_user') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'administrator',
                            name: 'administrator'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'jumlah_tanggapan',
                            name: 'jumlah_tanggapan',
                        },
                        {
                            data: 'sesi',
                            name: 'sesi',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                    rowReorder: {
                        selector: 'td:nth-child(2)'
                    },
                };

                table_pesan_peserta = $('#table_pesan_peserta').dataTable(config);

                $('body').on('click', '#createNewPesan', function() {
                    window.location.href = "{{ route('create-message-peserta') }}";
                });

                $('body').on('click', '.show.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-pesan-peserta', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    console.log(rows);
                                    $('.judul_pesan').html(rows.nama);
                                    $('.nama_peserta').html(rows.peserta);
                                    $('.pesan-message').html(rows.isi);

                                    var attach = "";

                                    if (rows.file !== null) {

                                        if (getExt(rows.file) == 'jpg' || getExt(rows
                                                .file) == 'jpeg' ||
                                            getExt(rows.file) == 'png' || getExt(rows
                                                .file) == 'svg') {
                                            attach = attach + '<img src="' + rows.file +
                                                '" class="form-group mb-1" width="auto" height="auto"></img>'
                                        } else {
                                            attach = attach + rows.file;
                                        }

                                        $('.mail-attachment').html(attach);
                                    }

                                    var balasan = '';

                                    rows.tanggapan.forEach((element) => {

                                        balasan = balasan +
                                            '<li class="media user-left talk-bubble tri-right round right-in">' +
                                            '<div class="d-flex  float-right">' +
                                            '<img src="{{ url('images/default.png') }}" class="form-group mb-1 img-rounded " width="50px" height="50px"></img>' +
                                            '</div>' +
                                            '<div class="media-body talktext">' +
                                            '<p class="mt-2 mb-2">' +
                                            element['isi'] +
                                            '</p>' +
                                            '</div>' +
                                            '</li>';
                                    });

                                    $('ul.list-tanggapan').html(balasan);

                                    $('#showDetailPesan').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    $('#showDetailPesan').modal('show');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var gambar = $(this).data('gambar');
                    var url = '{{ route('edit-pesan-peserta', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="id_pesan"]').val(rows.id);
                                    $('input[name="id_peserta"]').val(rows.id_peserta);

                                    tinymce.get("pesan").setContent(rows.isi);

                                    if (gambar == 'null' || gambar == 'unknown' || gambar ==
                                        '') {
                                        $('#modal-preview2').removeAttr('src');
                                        $('#modal-preview2').attr('src',
                                            'https://via.placeholder.com/150');
                                    } else {
                                        $('#modal-preview2').removeAttr('src');
                                        $('#modal-preview2').attr('src',
                                            gambar);
                                    }


                                    $('#EditPesan').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    $('#EditPesan').modal('show');
                                }
                            }

                        }
                    });

                });

                //update
                $('body').on('submit', '#PesanForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_pesan"]').val();
                    var urlx = '{{ route('update-pesan-peserta', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);
                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#EditPesan').modal('hide');
                                    table_pesan_peserta.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                    document.forms["PesanForm"].reset();

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //balas pesan
                $('body').on('submit', '#PesanFormX', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_pesan"]').val();
                    var urlx = '{{ route('balas-pesan-peserta', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);
                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#showBalasPesan').modal('hide');
                                    table_pesan_peserta.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                    document.forms["PesanFormX"].reset();

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove  data
                $('body').on('click', 'button.remove.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-pesan-peserta', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                $('body').on('click', '.balas.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('response-balasan-admin', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-send"></i>');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_pesan"]').val(id);
                                    $('input[name="id_user"]').val(result['id_peserta']);

                                    var balasan = "";
                                    var attach = "";

                                    rows.forEach((element) => {

                                        balasan = balasan +
                                            '<li class="media user-left talk-bubble tri-right round right-in">' +
                                            '<div class="d-flex  float-right">' +
                                            '<img src="{{ url('images/default.png') }}" class="form-group mb-1 img-rounded " width="50px" height="50px"></img>' +
                                            '</div>' +
                                            '<div class="media-body talktext">' +
                                            '<p class="mt-2 mb-2">' +
                                            element['isi'] +
                                            '</p>' +
                                            '</div>' +
                                            '</li>';
                                    });

                                    $('ul.list-balasan').html(balasan);


                                    $('#showBalasPesan').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');

                                    $('#showBalasPesan').modal('show');
                                }
                            }

                        }
                    });

                });

                window.getExt = function getx(path) {
                    var basename = path.split(/[\\/]/).pop(), // extract file name from full path ...
                        // (supports `\\` and `/` separators)
                        pos = basename.lastIndexOf("."); // get last position of `.`

                    if (basename === "" || pos < 1) // if file name is empty or ...
                        return ""; //  `.` not found (-1) or comes first (0)

                    return basename.slice(pos + 1);
                }

                $('body').on('click', 'button#data_trash', function() {
                    $('#ajaxModelTrash').modal('show');
                    config_trash = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('trash-pesan-peserta') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    table_trash = $('#table_trash_peserta').dataTable(config_trash);
                });

                //action button restore
                $('body').on('click', 'button.restore.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore-pesan-admin', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin pulihkan data ini!",
                        type: "question",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restoreData(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });

                //action button delete permanent
                $('body').on('click', 'button.remove_force.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-pesan-permanent-peserta', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini secara permanen , nantinya tidak dapat dipulihkan lagi!!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.ForceDelete(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });

                $('body').on('click', '.closed.pesan.peserta', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('tutup-pesan-user', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menutup pesan ini",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.ForceUpdateStatus(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });

                window.ForceUpdateStatus = function upstatsu(urlx, loader) {
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_pesan_peserta.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_pesan_peserta.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).find('input[type="text"],input[type="email"],input[type="file"],textarea,select').each(
                        function() {
                            if (this.defaultValue != '' || this.value != this.defaultValue) {
                                this.value = this.defaultValue;
                            } else {
                                this.value = '';
                            }
                        });
                });

                //function restore data
                window.restoreData = function restore(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_pesan_peserta.fnDraw(false);
                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-undo"></i> restore');

                                    window.notif(result['info'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-undo"></i> restore');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function forcedelete
                window.ForceDelete = function clear(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');

                                    window.notif(result['info'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');

                    }
                };

                //read file image upload
                window.readURL2 = function name(input, id) {
                    id = id || '#modal-preview2';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview2').removeClass('hidden');

                    }
                };

                //read file image upload
                window.readURL3 = function name(input, id) {
                    id = id || '#modal-preview3';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview3').removeClass('hidden');

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
