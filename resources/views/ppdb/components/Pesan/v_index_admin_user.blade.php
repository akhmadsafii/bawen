@extends('ppdb.layouts.app')
@section('ppdb.components')
    <style>
        /* CSS talk bubble */
        .talk-bubble {
            display: inline-block;
            position: relative;
            width: 100%;
            height: auto;
            background-color: lightyellow;
        }

        @media screen and (orientation:landscape) {

            /* Your CSS Here*/
            .talk-bubble {
                margin: 24px;
                display: inline-block;
                position: relative;
                width: 242px;
                height: auto;
                background-color: lightyellow;
            }
        }

        .border {
            border: 8px solid #666;
        }

        .round {
            border-radius: 30px;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;

        }

        /* Right triangle placed top left flush. */
        .tri-right.border.left-top:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: -8px;
            bottom: auto;
            border: 32px solid;
            border-color: #666 transparent transparent transparent;
        }

        .tri-right.left-top:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 0px;
            bottom: auto;
            border: 22px solid;
            border-color: lightyellow transparent transparent transparent;
        }

        /* Right triangle, left side slightly down */
        .tri-right.border.left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: 30px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.left-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 38px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /*Right triangle, placed bottom left side slightly in*/
        .tri-right.border.btm-left:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -8px;
            right: auto;
            top: auto;
            bottom: -40px;
            border: 32px solid;
            border-color: transparent transparent transparent #666;
        }

        .tri-right.btm-left:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 0px;
            right: auto;
            top: auto;
            bottom: -20px;
            border: 22px solid;
            border-color: transparent transparent transparent lightyellow;
        }

        /*Right triangle, placed bottom left side slightly in*/
        .tri-right.border.btm-left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 30px;
            right: auto;
            top: auto;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 transparent transparent #666;
        }

        .tri-right.btm-left-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: 38px;
            right: auto;
            top: auto;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow transparent transparent lightyellow;
        }

        /*Right triangle, placed bottom right side slightly in*/
        .tri-right.border.btm-right-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 30px;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.btm-right-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 38px;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        .tri-right.border.btm-right:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -8px;
            bottom: -40px;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.btm-right:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: 0px;
            bottom: -20px;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /* Right triangle, right side slightly down*/
        .tri-right.border.right-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -40px;
            top: 30px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 transparent transparent #666;
        }

        .tri-right.right-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -20px;
            top: 38px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow transparent transparent lightyellow;
        }

        /* Right triangle placed top right flush. */
        .tri-right.border.right-top:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -40px;
            top: -8px;
            bottom: auto;
            border: 32px solid;
            border-color: #666 transparent transparent transparent;
        }

        .tri-right.right-top:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            right: -20px;
            top: 0px;
            bottom: auto;
            border: 20px solid;
            border-color: lightyellow transparent transparent transparent;
        }

        /* talk bubble contents */
        .talktext {
            padding: 1em;
            text-align: left;
            line-height: 1.5em;
        }

        .talktext p {
            /* remove webkit p margins */
            -webkit-margin-before: 0em;
            -webkit-margin-after: 0em;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="mail-single-message">
                            <p class="text-muted">Pesan</p>
                            <div class="talk-bubble tri-right left-top">
                                <h2 class="fw-100 h1 sub-heading-font-family ml-3 mt-1 mr-b-30 judul_pesan"></h2>
                                <div class="talktext ml-3">
                                    @foreach ($detail as $detail)
                                        <p class="nama_peserta">{{ $detail['peserta'] }}</p>
                                        <div class="pesan-message">{!! $detail['isi'] !!}</div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.mail-attachment -->
                            <hr>
                            <p class="text-muted">Tanggapan</p>
                            <section class="scrollbar-enabled scroll-to-bottom ps ps--theme_default ps--active-y">
                                <div class="table-responsive">
                                    <div class="col-md-12">
                                        <ul class="list-unstyled pd-t-20 mr-l-20 list-tanggapan" style="height: 200px;">
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <form id="PesanFormX" name="PesanFormX" method="POST" class="form-horizontal"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="mail-single-message">
                                        <section
                                            class="scrollbar-enabled scroll-to-bottom ps ps--theme_default ps--active-y">
                                            <ul class="list-unstyled pd-t-20 mr-l-20 list-balasan">
                                            </ul>
                                        </section>
                                        <hr>
                                        <div class="col-md-12 ">
                                            <input type="hidden" name="id_pesan" id="id_pesan" class="form-control"
                                                value="{{ $detail['id'] }}">
                                            <input type="hidden" name="id_user" id="id_user" class="form-control"
                                                value="{{ $detail['id_peserta'] }}">
                                            <div class="form-group">
                                                <label for="l1">Pesan
                                                    <span class="text-red">*</span>
                                                </label>
                                                <textarea name="pesan" id="pesan" rows="5" cols="5" class="form-control editor text-left" placeholder="isi pesan">
                                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <label class="col-md-3 col-form-label" for="l1"></label>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                                        </label>
                                                        <img id="modal-preview3" src="https://via.placeholder.com/150"
                                                            alt="Preview" class="form-group mb-1" width="150px"
                                                            height="150px" style="margin-top: 10px">
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="form-group row">
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input id="image" type="file" name="image"
                                                                        accept="image/*" onchange="readURL3(this);">
                                                                    <input type="hidden" name="hidden_image"
                                                                        id="hidden_image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <center>
                                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"> <i
                                                class="fa fa-send"></i> Balas</button>
                                    </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //read file image upload
                window.readURL3 = function name(input, id) {
                    id = id || '#modal-preview3';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview3').removeClass('hidden');

                    }
                };

                //balas pesan
                $('body').on('submit', '#PesanFormX', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_pesan"]').val();
                    var urlx = '{{ route('balas-pesan-admin', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);
                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $(loader).html('<i class="fa fa-save"></i> balas');

                                    window.notif(result['info'], result['message']);

                                    document.forms["PesanFormX"].reset();

                                    balasan();

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> balas');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });


                window.balasan = function balasanx() {
                    var url = '{{ route('detail-pesan-admin', ':id') }}';
                    url = url.replace(':id', '{{ $detail['id'] }}');
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    var balasan = '';

                                    rows.tanggapan.sort(function(a, b) {
                                        return b - a
                                    });

                                    rows.tanggapan.forEach((element) => {
                                        var role = '';
                                        if (element['role'] == 'peserta') {
                                            role = element['nama'];
                                        } else {
                                            role = '';
                                        }
                                        balasan = balasan +
                                            '<li class="media user-left talk-bubble tri-right round right-in">' +
                                            '<div class="d-flex  float-right">' +
                                            '<img src="{{ url('images/default.png') }}" class="form-group mb-1 img-rounded " width="50px" height="50px"></img>' +
                                            role +
                                            '</div>' +
                                            '<div class="media-body talktext">' +
                                            '<p class="mt-2 mb-2">' +
                                            element['isi'] +
                                            '</p>' +
                                            '</div>' +
                                            '</li>';
                                    });

                                    $('ul.list-tanggapan').html(balasan);

                                    $('#showDetailPesan').modal('show');
                                }
                            }

                        }
                    });
                }

                balasan();

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
