@extends('ppdb.layouts.app')
@section('ppdb.components')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css" rel="stylesheet" type="text/css">
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <style>
        #map {
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 150px;
        }

        .address {
            cursor: pointer
        }

        .address:hover {
            color: #AA0000;
            text-decoration: underline
        }

        #mapSearchContainer {
            position: fixed;
            top: 20px;
            right: 40px;
            height: 30px;
            width: 180px;
            z-index: 110;
            font-size: 10pt;
            color: #5d5d5d;
            border: solid 1px #bbb;
            background-color: #f8f8f8;
        }

        .pointer {
            position: absolute;
            top: 86px;
            left: 60px;
            z-index: 99999;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Profil > Ubah Profil User</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Ubah Profil
                </li>
                <li class="breadcrumb-item"><a href="{{ route('ubah_password_user') }}">Ubah Password</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus diisi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <i class="material-icons list-icon md-48">warning</i>
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info text-center alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon md-48">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="ProfilForm" name="ProfilForm" method="POST" class="form-horizontal"
            action="{{ route('profil_simpan_user') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="form-group row">
                                <p class="mr-b-0">NISN <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="nisn" type="number" name="nisn" placeholder="11234567"
                                        title="format harus berisi angka saja" pattern="^\d$" class="form-control"
                                        value="{{ $profil_data['nisn'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Nama <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="nama" type="text" name="nama" placeholder="Agus Budiman"
                                        pattern="[a-zA-Z][a-zA-Z0-9\s]*" title="Format harus berisi alfanumerik atau huruf"
                                        class="form-control" value="{{ $profil_data['nama'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Alamat <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <textarea name="alamat" id="alamat" placeholder="Jl.Sukorejo-Parakan Km 3 Kabupaten Kendal , Jawa Tengah"
                                        class="form-control">{{ $profil_data['alamat'] ?? '-' }}</textarea>
                                </div>
                                <!-- /.input-group -->
                                <div id="results"></div>
                            </div>
                            <div class="form-group" id="map"></div>

                            <div class="form-group row">

                                <label class="mr-b-0">Koordinat Latitude</label>
                                <div class="input-group">
                                    <input class="form-control" type="lat" name="lat" id="lat" placeholder="lat"
                                        value="{{ old('latitude') ?? ($profil_data['latitude'] ?? '') }}">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="mr-b-0">Koordinat Longitude</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="long" id="lon" placeholder="Long"
                                        value="{{ old('longitude') ?? ($profil_data['longitude'] ?? '') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Kota <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="kota" type="text" name="kota" placeholder="Semarang"
                                        title="format harus berisi huruf saja" pattern="[a-zA-Z\s]*" class="form-control"
                                        value="{{ $profil_data['tempat_lahir'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Telephone <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="telephone" type="number" name="telephone" placeholder="0824207872613"
                                        title="format harus berisi angka saja" pattern="^\d{13}$" maxlength="13"
                                        class="form-control" value="{{ $profil_data['telepon'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Email <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="email" type="email" name="email" placeholder="admin@yahoo.co.id"
                                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"
                                        value="{{ $profil_data['email'] ?? '' }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <p class="mr-b-0">Foto / Avatar</p>
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    @isset($profil_data['file'])
                                        <img id="modal-preview2" src="{{ $profil_data['file'] }}" alt="Preview"
                                            class="form-group mb-1" width="50px" height="50px">
                                    @endisset
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group mb-1" width="150px" height="150px">
                                <p class="text-muted ml-2">Kosongkon form ini jika tidak ada perubahan gambar</p>
                            </div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $(document).ready(function() {

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                var startlat = '{{ $profil_data['latitude'] ?? '-6.991576' }}';
                var startlon = '{{ $profil_data['longitude'] ?? '109.122923' }}';

                var options = {
                    center: [startlat, startlon],
                    zoom: 9
                }

                document.getElementById('lat').value = startlat;
                document.getElementById('lon').value = startlon;

                var map = L.map('map', options);
                var nzoom = 12;

                L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=Ts9g8McLuNVEfjGFTHeG', {
                    attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
                }).addTo(map);

                var myMarker = L.marker([startlat, startlon], {
                    title: "Coordinates",
                    alt: "Coordinates",
                    draggable: true
                }).addTo(map).on('dragend', function() {
                    var lat = myMarker.getLatLng().lat.toFixed(8);
                    var lon = myMarker.getLatLng().lng.toFixed(8);
                    var czoom = map.getZoom();
                    if (czoom < 18) {
                        nzoom = czoom + 2;
                    }
                    if (nzoom > 18) {
                        nzoom = 18;
                    }
                    if (czoom != 18) {
                        map.setView([lat, lon], nzoom);
                    } else {
                        map.setView([lat, lon]);
                    }
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                    getAddressMaker(lat, lon);
                });




                window.getAddressMaker = function(latx, lonx) {
                    $.ajax({
                        url: "https://nominatim.openstreetmap.org/reverse",
                        data: {
                            lat: latx,
                            lon: lonx,
                            format: "json"
                        },
                        beforeSend: function(xhr) {},
                        dataType: "json",
                        type: "GET",
                        async: true,
                        crossDomain: true
                    }).done(function(res) {
                        var rows = JSON.parse(JSON.stringify(res.address));
                        var alamatx = res.display_name;
                        var kotax   = rows.county || rows.city;
                        $('textarea[name="alamat"]').val(alamatx);
                        $('input[name="kota"]').val(kotax);
                        myMarker.bindPopup("Lat: " + latx + "<br />Lon:" + lonx + "<br />" +
                            alamatx).openPopup();
                    }).fail(function(error) {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    });
                }

                window.chooseAddr = function chooseAddr(lat1, lng1) {
                    myMarker.closePopup();
                    map.setView([lat1, lng1], 18);
                    myMarker.setLatLng([lat1, lng1]);
                    lat = lat1.toFixed(8);
                    lon = lng1.toFixed(8);
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                    getAddressMaker(lat1, lng1);
                }

                $('body').on('cllick', '.address', function() {
                    $('textarea[name="alamat"]').val($(this).data('alamat'));
                });

                $('body').on('keyup','textarea[name="alamat"]',function(){
                    addr_search();
                });

                window.myFunction = function myFunction(arr) {
                    var out = "<br />";
                    var i;
                    if (arr.length > 0) {
                        for (i = 0; i < arr.length; i++) {
                            out +=
                                "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" +
                                arr[i].lat + ", " + arr[i].lon + ");return false;' data-alamat='" + arr[i]
                                .display_name + "'>" + arr[i].display_name +
                                "</div>";
                        }
                        document.getElementById('results').innerHTML = out;
                    } else {
                        document.getElementById('results').innerHTML = "Sorry, no results...";
                    }
                }

                window.addr_search = function addr_search() {
                    var inp = document.getElementById("alamat");
                    var xmlhttp = new XMLHttpRequest();
                    var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp
                        .value;
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var myArr = JSON.parse(this.responseText);
                            myFunction(myArr);
                        }
                    };
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }

            });

        })(jQuery, window);
    </script>
@endsection
