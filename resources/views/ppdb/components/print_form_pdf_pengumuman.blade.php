<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: auto;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
</head>

<body>
    <table style="width: 100%;" border="0">
        <thead>
            <tr>
                @if ($form_setting['4']['initial'] == 'logo1' && $form_setting['4']['aktif_kode'] == '1')
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($form[4]['initial'] == 'logo1')
                            <img src="{{ $form[4]['value'] }}" style="max-height:100px; min-width: 100px;">
                        @endif
                    </td>
                @endif
                @if ($form_setting['0']['initial'] == 'head1' && $form_setting['0']['aktif_kode'] == '1')
                    <td style="text-align: center">
                        @if ($form[0]['initial'] == 'head1')
                            <b>{{ $form[0]['value'] }}</b>
                        @endif
                    </td>
                @endif
                @if ($form_setting['5']['initial'] == 'logo2' && $form_setting['5']['aktif_kode'] == '1')
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($form[5]['initial'] == 'logo2')
                            <img src="{{ $form[5]['value'] }}" style="max-height:100px; min-width: 100px;">
                        @endif
                    </td>
                @endif
            </tr>
            @if ($form_setting['1']['initial'] == 'head2' && $form_setting['1']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center">
                        @if ($form[1]['initial'] == 'head2')
                            <h5 style="margin: 0"> <b>{{ $form[1]['value'] }}</b></h5>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($form_setting['2']['initial'] == 'head3' && $form_setting['2']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center">
                        @if ($form[2]['initial'] == 'head3')
                            <b>{{ $form[2]['value'] }}</b></h2>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($form_setting['3']['initial'] == 'alamat' && $form_setting['3']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center;font-size:12pt">
                        @if ($form[3]['initial'] == 'alamat')
                            <b>{{ $form[3]['value'] }}</b>
                        @endif
                    </td>
                </tr>
            @endif
            <tr>
                <td colspan="3">
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                    <u> Hasil Pengumuman </u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px; text-align: center;" colspan="3">
                    @if ($form_setting['6']['initial'] == 'prolog' && $form_setting['6']['aktif_kode'] == '1')
                        @if ($form[6]['initial'] == 'prolog')
                            <b>{{ $form[6]['value'] }}</b>
                        @endif
                    @endif

                    @if ($form[15]['initial'] == 'nama')
                        <b>{{ $form[15]['value'] }}</b>
                    @endif
                    <br>
                    @if ($form[16]['initial'] == 'nisn')
                        <b> NISN : {{ $form[16]['value'] }}</b>
                    @endif

                    @if ($form_setting['17']['initial'] == 'keputusan' && $form_setting['17']['aktif_kode'] == '1')
                        @if ($form[17]['initial'] == 'keputusan')
                            <h1><b>{{ $form[17]['value'] }}</b></h1>
                        @endif
                    @endif

                    @if ($form_setting['18']['initial'] == 'keterangan_keputusan' && $form_setting['18']['aktif_kode'] == '1')
                        @if ($form[18]['initial'] == 'keterangan_keputusan')
                            <b> catatan : {{ $form[18]['value'] ?? '-' }}</b>
                        @endif
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form_setting['12']['initial'] == 'tempat_keputusan' && $form_setting['12']['aktif_kode'] == '1')
                        @if ($form[12]['initial'] == 'tempat_keputusan')
                            <small><b>{{ $form[12]['value'] }}</b></small>,
                        @endif
                    @endif
                    @if ($form_setting['13']['initial'] == 'tgl_keputusan' && $form_setting['13']['aktif_kode'] == '1')
                        @if ($form[13]['initial'] == 'tgl_keputusan')
                            <small>{{ $form[13]['value'] }}</small>
                        @endif
                    @endif
                    <h5>Kepala Sekolah</h5>
                    <br>
                </td>
            </tr>
            <tr>
                @if ($form_setting['10']['initial'] == 'stempel' && $form_setting['10']['aktif_kode'] == '1')
                <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form[10]['initial'] == 'stempel')
                    <img src="{{ $form[10]['value'] }}"
                    style=" text-align: right; max-height:148px; min-width: 135px; padding: 13px; margin-right: -16px;">
                    @endif
                </td>
            @endif
                @if ($form_setting['8']['initial'] == 'ttd_kepsek' && $form_setting['8']['aktif_kode'] == '1')
                    <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                        @if ($form[8]['initial'] == 'ttd_kepsek')
                            <img src="{{ $form[8]['value'] }}"
                                style=" text-align: right; max-height:138px; min-width: 125px; padding: 13px; margin-right: -16px;">
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form_setting['9']['initial'] == 'nama_kepsek' && $form_setting['9']['aktif_kode'] == '1')
                        @if ($form[9]['initial'] == 'nama_kepsek')
                            <small><u><b>{{ $form[9]['value'] }}</b></u></small><br>
                        @endif
                    @endif
                    @if ($form_setting['11']['initial'] == 'nip_kepsek' && $form_setting['11']['aktif_kode'] == '1')
                        @if ($form[11]['initial'] == 'nip_kepsek')
                            <small><b>{{ $form[11]['value'] }}</b></small>
                        @endif
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
