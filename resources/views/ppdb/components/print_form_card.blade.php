@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Preview Form </h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Cetak Kartu Peserta
                </li>
                <li class="breadcrumb-item"><a href="{{ route('print_pengumuman-user') }}">Cetak Hasil Pengumuman</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="widget-list" id="print-preview3">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <center>
                        <a href="{{ route('print_kartu_peserta') }}" id="print" target="_blank">
                            <i class="material-icons list-icon">print</i>
                        </a>
                        &nbsp;
                        <a href="{{ route('print_card_pdf') }}" target="_blank">
                            <i class="material-icons list-icon">file_download</i>
                        </a>
                    </center>
                    <hr>
                </div>
                <div class="widget-body clearfix">
                  <div class="table-responsive">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form['kop']['logo'] != null)
                                        <img src="{{ $form['kop']['logo'] }}" style="max-height:138px; min-width: 125px;">
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    <b>{{ $form['kop']['header1'] }}</b>
                                </td>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form['kop']['logo2'] != null)
                                        <img src="{{ $form['kop']['logo2'] }}"
                                            style="max-height:138px; min-width: 125px;">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <h2 style="margin: 0"><b>{{ $form['kop']['header2'] }}</b></h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <b>{{ $form['kop']['header3'] }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <b>{{ $form['kop']['alamat'] }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr style="border: solid 2px #000">
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>Kartu
                                        Peserta</u>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px;" colspan="2">
                                    @foreach ($form_setting as $formx => $list)
                                        @if ($formx == 0)
                                            <table style="width:30%;">
                                                <tbody>
                                                    @foreach ($form['form'] as $key => $val)
                                                        <tr>
                                                            <td style="30%;">{{ $val['nama'] }}</td>
                                                            <td width="1%">:</td>
                                                            <td  class="tbl">
                                                                @isset($val['value'])
                                                                    @switch($val['value'])
                                                                        @case('l')
                                                                            <small> laki -laki </small>
                                                                        @break
                                                                        @case('p')
                                                                            <small> Perempuan </small>
                                                                        @break

                                                                        @default
                                                                            <small>{{ $val['value'] }}</small>
                                                                    @endswitch
                                                                @else
                                                                    <small class="text-muted">-</small>
                                                                @endisset
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif

                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($form_setting as $formx => $list)
                                        @if ($list['initial'] == 'tampil_foto' && $list['aktif'] == 'Aktif')
                                            <img id="modal-preview" class=" float-right" src="{{ session('avatar') }}"
                                                alt="Preview" width="150px" height="150px">
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            @foreach ($form_setting as $formx => $list)
                                @if ($list['initial'] == 'jadwal_ppdb' && $list['aktif'] == 'Aktif')
                                    <tr>
                                        <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                                            <u> Jadwal</u>
                                        </td>
                                    </tr>
                                    <tr>
                                        <table id="table_jadwal" class="table table-striped table-responsive">
                                            <thead>
                                                <tr class="bg-info text-inverse">
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Tanggal Mulai </th>
                                                    <th>Tanggal Berakhir </th>
                                                    <th>Keterangan </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($jadwal as $key => $val)
                                                    <tr>
                                                        <td>#</td>
                                                        <td>{{ $val['nama'] }}</td>
                                                        <td>{{ Carbon\Carbon::parse($val['tgl_mulai'])->formatLocalized('%A %d %B %Y') }}
                                                        </td>
                                                        <td>{{ Carbon\Carbon::parse($val['tgl_akhir'])->formatLocalized('%A %d %B %Y') }}
                                                        </td>
                                                        <td>{{ $val['keterangan'] }}</td>
                                                    </tr>

                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Tanggal Mulai </th>
                                                    <th>Tanggal Berakhir </th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
