@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            @if (empty($rows))
            <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada catatan </p>
            @else
            <h5 class="card-title text-body">{{ $rows['judul'] ?? '' }}</h5>
            <div class="card-text text-body">
                <div class="row">
                     @isset($rows['file'])
                     <div class="col-md-4">
                        <img src="{{ $rows['file'] }}" loading="lazy" class="text-center">
                     </div>
                     <div class="col-md-8 scrollbar-enabled ps ps--theme_default ps--active">
                        <div class="content-text" style="height: 37.8em;">
                            {!! $rows['isi'] ?? '' !!}
                        </div>
                     </div>
                     @else
                     <div class="col-md-12 scrollbar-enabled ps ps--theme_default ps--active">
                        <div class="content-text" style="height: 37.8em;">
                            {!! $rows['isi'] ?? '' !!}
                        </div>
                     </div>
                     @endisset
                </div>
            </div>

            @endif

        </div>
    </div>
@endsection
