@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card ">
        <div class="card-header">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h5 class="mr-0 mr-r-5"><i class="fa fa-bullhorn"></i></h5>
                    <p class="mr-0 text-muted d-none d-md-inline-block">PENGUMUMAN</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-inline-flex">
                    {{ $feed->links() }}
                </div>
                <!-- /.page-title-right -->
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                @if (count($feed) > 0)
                    @foreach ($feed as $list)
                        <div class="col-sm-4 col-md-4 pengumuman">
                            <div class="img-thumbnail">
                                <center>
                                    @isset($list['file'])
                                        <img alt="100%x200" data-src="holder.js/100%x200" style="width: 50%; display: block"
                                            src="{{ $list['file'] }}" data-holder-rendered="true">
                                    @else
                                        <img alt="100%x200" data-src="holder.js/100%x200" style="width: 50%; display: block"
                                            src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjQyIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDI0MiAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MjAwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTVjOGMzOWZlZTAgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMnB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNWM4YzM5ZmVlMCI+PHJlY3Qgd2lkdGg9IjI0MiIgaGVpZ2h0PSIyMDAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI4OS44MDQ2ODc1IiB5PSIxMDUuMSI+MjQyeDIwMDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                            data-holder-rendered="true">
                                    @endisset
                                </center>
                                <div class="caption px-3">
                                    <h4 class="text-center" title="{{ $list['judul'] }}">
                                        {{ Str::limit($list['judul'], 10, '...') ?? '' }}</h4>
                                    <center>{{ $list['diposting'] }}</center>
                                    <center><i class="fa fa-calendar"></i> {{ $list['waktu'] }}</center>
                                    <p>
                                    <div class="row page-title clearfix">
                                        <div class="page-title-left">
                                            <a href="#" class="btn btn-info pengumuman-info text-center "
                                                data-id="{{ $list['id'] ?? '' }}"
                                                data-judul="{{ Str::slug($list['judul'], '-') ?? '' }} "
                                                role="button">Selengkapnya</a>
                                        </div>
                                        <!-- /.page-title-left -->
                                        <div class="page-title-right d-inline-flex">
                                            <span class="text-mute">
                                                <i class="fa fa-eye"></i>
                                                {{ intval($list['dilihat']) }}
                                            </span>
                                        </div>
                                        <!-- /.page-title-right -->
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="text-muted"> Tidak ada catatan pengumuman ! </p>
                @endif
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('body').on('click', '.pengumuman-info', function(e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var slug = $(this).data('judul');
                    var url = '{{ route('view-pengumuman', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: "PUT",
                        url: url,
                        beforeSend: function() {},
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var redirect =
                                        "{{ route('detail-info-feed', ['id' => ':id', 'slug' => ':slug']) }}";
                                    redirect = redirect.replace(':id', id).replace(':slug',
                                        slug);
                                    window.location.href = redirect;
                                } else if (result['info'] == 'error') {
                                    alert($result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });


                });

            });

        })(jQuery, window);
    </script>
@endsection
