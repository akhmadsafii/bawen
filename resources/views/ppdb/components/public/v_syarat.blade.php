@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            @if (empty($syarat))
            <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada catatan </p>
            @else
            <h5 class="card-title text-body">{{ $syarat['judul'] ?? '' }} </h5>
            <div class="row">
                @isset($syarat['file'])
                    <div class="col-md-4">
                        <img src="{{ $syarat['file'] }}" loading="lazy" class="text-center">
                    </div>
                    <div class="col-md-8  scrollbar-enabled ps ps--theme_default ps--active">
                        <div class="content-text" style="height: 37.8em;">
                            {!! $syarat['isi'] ?? '' !!}
                        </div>
                    </div>
                @else
                    <div class="col-md-12 scrollbar-enabled ps ps--theme_default ps--active">
                        <div class="content-text" style="height: 37.8em;">
                            {!! $syarat['isi'] ?? '' !!}
                        </div>
                    </div>
                @endisset
            </div>
            @endif

        </div>
    </div>
@endsection
