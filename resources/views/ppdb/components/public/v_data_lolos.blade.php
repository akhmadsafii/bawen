@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="body_fix mr-4 ml-1 ">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-heading clearfix">
                            <h5>Data Lolos Seleksi PPDB </h5>
                        </div>
                        <!-- /.widget-heading -->
                        <div class="widget-body clearfix">
                            <div class="table-responsive">
                                <table id="table_pendaftar_peserta_register" class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nisn</th>
                                            <th>Nama</th>
                                            <th>Asal Sekolah </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- Append Create Datatables-->
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nisn</th>
                                            <th>Nama</th>
                                            <th>Asal Sekolah </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let table_reg;
            let config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    }, {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        title: 'DATA LOLOS SELEKSI PPDB',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        }
                    }],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_peserta_diterimappdb') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'asal_sekolah',
                            name: 'asal_sekolah'
                        },
                    ],
                };

                table_reg = $('#table_pendaftar_peserta_register').dataTable(config);
            });
        })(jQuery, window);
    </script>
@endsection
