@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-body">FAQ </h5>
            @if (empty($faq))
            <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada catatan </p>
            @else
            <div class="row">
                <ul class="col-md-3 list-unstyled">
                    <li class="widget-holder widget-bg">
                        <a href="#accordion1" class="widget-body d-flex">
                            <div class="mr-3">
                                @isset($faq['file'])
                                <img src="{{ $faq['file'] }}" loading="lazy" class="text-center" width="50px" height="50px">
                                @else
                                <i class="fa fa-diamond fs-24 text-primary"></i>
                                @endisset
                            </div>
                            <div>
                                <h5 class="mt-0 mb-1 fs-16 fw-400 sub-heading-font-family">{{ $faq['judul'] ?? '' }}</h5>
                                <small>
                                    {!! Str::limit($faq['isi'], 50, '...') !!}
                                </small>
                            </div>
                        </a>
                    </li>

                </ul>
                <!-- /.col-md-3 -->
                <div class="col-md-9">
                    <div class="widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <div class="accordion accordion-minimal" id="accordion1" role="tablist"
                                    aria-multiselectable="true">
                                    <div class="d-flex mb-4 mt-2">
                                        <div class="mr-3">
                                            @isset($faq['file'])
                                            <img src="{{ $faq['file'] }}" loading="lazy" class="text-center" width="50px" height="50px">
                                            @else

                                            <i class="fa fa-diamond fs-24 text-primary"></i>
                                            @endisset
                                        </div>
                                        <div>
                                            <h5 class="mt-0 mb-1 fs-22 fw-400 sub-heading-font-family">{{ $faq['judul'] ?? '' }}</h5>
                                            <small>{!! Str::limit($faq['isi'], 50, '...') !!}</small>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="card-title fw-300"><a role="button" data-toggle="collapse"
                                                    data-parent="#accordion1" href="#collapseOne" aria-expanded="false"
                                                    aria-controls="collapseOne" class="collapsed"><strong>{{ $faq['judul'] ?? '' }}</strong></a></h5>
                                        </div>
                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel"
                                            aria-labelledby="headingOne" style="">
                                            <div class="card-block">{!! $faq['isi'] ?? '' !!}</div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.accordion -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                </div>
                <!-- /.col-md-9 -->
            </div>
            @endif

        </div>
    </div>
@endsection
