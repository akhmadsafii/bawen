@extends('ppdb.layouts.app')
@section('ppdb.components')
    <style>
        .delete {
            cursor: pointer !important;
            font-size: 30px;
            position: absolute;
            color: white;
            border: none;
            background: none;
            right: -15px;
            top: -15px;
            line-height: 1;
            z-index: 99;
            padding: 0;
        }

        .delete span {
            height: 30px;
            width: 30px;
            background-color: black;
            border-radius: 50%;
            display: block;
        }

        .box {
            width: calc((100% - 30px) * 0.333);
            margin: 5px;
            height: 250px;
            float: left;
            box-sizing: border-box;
            position: relative;
            box-shadow: 0 0 5px 2px rgba(0, 0, 0, .15);
        }

        .box:hover {
            box-shadow: 0 0 15px 3px rgba(0, 0, 0, 0.5);
        }

        .box .image {
            width: 100%;
            height: 100%;
            position: relative;
            overflow: hidden;
        }

        .box .image img {
            width: 100%;
            min-height: 100%;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
        }

        @media (max-width: 600px) {
            .box {
                width: calc((100% - 20px) * 0.5);
                height: 200px;
            }
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan >Sambutan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('setting-ppdb') }}">Pengaturan PPDB</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
                </li>
                <li class="breadcrumb-item active">Sambutan
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-surat') }}">Template Surat</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-kartu') }}">Template Kartu</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="SambutanForm" name="SambutanForm" method="POST" class="form-horizontal" enctype="multipart/form-data"
            action="{{ route('simpan-pengaturan-sambutan') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="form-group row">
                                <p class="mr-b-0">Judul <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="judul" type="text" name="judul" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                        title="Format harus berisi alfanumerik atau huruf saja " class="form-control"
                                        value="{{ old('judul') ?? ($rows['judul'] ?? '') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Isi <span class="text-red">*</span> </p>
                                <div class="input-group">
                                    <textarea name="isi" id="isi" class="form-control editor">
                                                    {{ old('isi') ?? ($rows['isi'] ?? '') }}
                                                </textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                @isset($rows['file'])
                                    <p class="mr-b-0">Gambar </p>
                                @else
                                    <p class="mr-b-0">Gambar <span class="text-red">*</span></p>
                                @endisset
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                @isset($rows['file'])
                                    <div class="box">
                                        <button type="button" class="delete delete-image">
                                            <span>&times;</span>
                                        </button>
                                        <div class="image">
                                            <img id="modal-preview" src="{{ $rows['file'] }}" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        </div>
                                    </div>
                                    <p class="text-muted ml-4">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                @else
                                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                        class="form-group mb-1" width="150px" height="150px">
                                @endisset
                            </div>
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                $('body').on('click', '.delete-image', function() {
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus photo data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {

                        var urlx = '{{ route('sambutan-remove-photo') }}';
                        var formElem = $("#SambutanForm");
                        var formData = new FormData(formElem[0]);
                        const loader = $('button.update');
                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading'
                                );
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {

                                if (typeof result['message'] !== 'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {

                                        $(loader).html(
                                            '<i class="fa fa-trash-o"></i> save'
                                        );

                                        window.notif(result['info'], result[
                                            'message']);

                                        setTimeout(() => {
                                            window.location.reload();
                                        }, 2000);

                                    } else if (result['info'] == 'error') {
                                        $(loader).html(
                                            '<i class="fa fa-trash-o"></i> save'
                                        );
                                        window.notif(result['info'], result[
                                            'message']);
                                    }
                                }

                            },
                            error: function(data) {
                                //$('#ajaxModelEdit').modal('hide');
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);

                                }

                            }
                        });

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });

                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });

        })(jQuery, window);
    </script>
@endsection
