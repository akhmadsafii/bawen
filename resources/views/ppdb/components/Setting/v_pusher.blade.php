@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Notifikasi </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Pusher
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-title">Panduan integrasi Pusher.</div>
                                <ol>
                                    <li>Daftarkan Akun dengan Email yang aktif jika belum punya akun di
                                        pusher, kunjungi alamat website : <a href="https://pusher.com/">
                                            https://pusher.com/</a></li>
                                    <li>login dengan akun yng sudah didaftarkan di halaman pusher</li>
                                    <li>Buat Chanel Plan dan Pilih Chanel Plan yang telah dibuat tadi.</li>
                                    <li>Ambil APP_ID, APP_SECRET,APP_KEY, APP_CLUSTER di menu App keys lalu isikan
                                        form
                                        yang sudah di tersedia </li>
                                    <li>Klik tombol update untuk mengetahui perubahan</li>
                                </ol>
                            </div>
                            <div class="col-md-6">
                                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data">
                                    @if (count($notifikasi) > 0)
                                        <input type="hidden" id="jenis" class="form-control" value="notification">
                                        @foreach ($notifikasi as $key => $val)
                                            @if ($val['kode'] === 'PUSHER_APP_ID')
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-right" for="l11">App ID
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="app_id_pusher"
                                                            placeholder="APP ID PUSHER " type="text" name="app_id_pusher"
                                                            required="" value="{{ $val['token'] ?? '' }}">
                                                    </div>
                                                </div>
                                            @endif

                                            @if ($val['kode'] === 'PUSHER_APP_KEY')
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-right" for="l11">App Key
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="app_key_pusher"
                                                            placeholder="APP KEY PUSHER" type="text" name="app_key_pusher"
                                                            required="" value="{{ $val['token'] ?? '' }}">
                                                    </div>
                                                </div>
                                            @endif

                                            @if ($val['kode'] === 'PUSHER_APP_SECRET')
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-right" for="l11">App Secret
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="app_key_pusher"
                                                            placeholder="APP SECRET PUSHER" type="text"
                                                            name="app_secret_pusher" required=""
                                                            value="{{ $val['token'] ?? '' }}">
                                                    </div>
                                                </div>
                                            @endif

                                            @if ($val['kode'] === 'PUSHER_APP_CLUSTER')
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-right" for="l11">App Cluster
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="app_key_pusher"
                                                            placeholder="APP CLUSTER PUSHER" type="text"
                                                            name="app_cluster_pusher" required=""
                                                            value="{{ $val['token'] ?? '' }}">
                                                    </div>
                                                </div>
                                            @endif

                                        @endforeach
                                        <div class="form-actions modal-footer float-right">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 btn-list">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="material-icons list-icon">save</i>
                                                            Update
                                                        </button>
                                                    </div>
                                                    <!-- /.col-sm-12 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                    @else
                                        <p class="text-red"> Check Table Environment in your database
                                            serve </p>
                                    @endif
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {

                //update
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();

                    var urlx = "{{ route('store_pusher') }}";

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    window.notif(result['info'], result['message']);
                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }
            });
        })(jQuery, window);
    </script>
@endsection
