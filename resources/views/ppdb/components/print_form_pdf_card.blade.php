<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 1px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
</head>

<body>
    <table style="width:100%;">
        <thead>
            <tr>
                @if ($form_setting['4']['initial'] == 'logo1' && $form_setting['4']['aktif_kode'] == '1')
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($form['kop']['logo'] != null)
                            <img src="{{ $form['kop']['logo'] }}" style="max-height:100px; min-width: 100px;">
                        @endif
                    </td>
                @endif
                @if ($form_setting['0']['initial'] == 'head1' && $form_setting['0']['aktif_kode'] == '1')
                    <td style="text-align: center">
                        <b>{{ $form['kop']['header1'] }}</b>
                    </td>
                @endif
                @if ($form_setting['5']['initial'] == 'logo2' && $form_setting['5']['aktif_kode'] == '1')
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($form['kop']['logo2'] != null)
                            <img src="{{ $form['kop']['logo2'] }}" style="max-height:100px; min-width: 100px;">
                        @endif
                    </td>
                @endif
            </tr>
            @if ($form_setting['1']['initial'] == 'head2' && $form_setting['1']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center">
                        <h5 style="margin: 0"><b>{{ $form['kop']['header2'] }}</b></h5>
                    </td>
                </tr>
            @endif
            @if ($form_setting['2']['initial'] == 'head3' && $form_setting['2']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center">
                        <b>{{ $form['kop']['header3'] }}</b>
                    </td>
                </tr>
            @endif
            @if ($form_setting['3']['initial'] == 'alamat' && $form_setting['3']['aktif_kode'] == '1')
                <tr>
                    <td style="text-align: center">
                        <b>{{ $form['kop']['alamat'] }}</b>
                    </td>
                </tr>
            @endif
            <tr>
                <td colspan="3">
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>Kartu
                        Peserta</u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px;" colspan="2">
                    @foreach ($form_setting as $formx => $list)
                        @if ($formx == 0)
                            <table style="width:80%;">
                                <tbody>
                                    @foreach ($form['form'] as $key => $val)
                                        <tr>
                                            <td style="30%;">{{ $val['nama'] }}</td>
                                            <td width="1%">:</td>
                                            <td class="tbl">
                                                @isset($val['value'])
                                                    @switch($val['value'])
                                                        @case('l')
                                                            <small> laki -laki </small>
                                                        @break

                                                        @case('p')
                                                            <small> Perempuan </small>
                                                        @break

                                                        @default
                                                            <small>{{ $val['value'] }}</small>
                                                    @endswitch
                                                @else
                                                    <small class="text-muted">-</small>
                                                @endisset
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($form_settingx as $formx => $list)
                        @if ($list['initial'] == 'tampil_foto' && $list['aktif'] == 'Aktif')
                            <img id="modal-preview" class=" float-right" src="{{ session('avatar') }}" alt="Preview"
                                style="width:150px;height:150px;">
                        @endif
                    @endforeach
                </td>
            </tr>
            @foreach ($form_settingx as $formx => $list)
                @if ($list['initial'] == 'jadwal_ppdb' && $list['aktif'] == 'Aktif')
                    <tr>
                        <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                            <u>Jadwal</u>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table id="table_jadwal" class="table table-striped table-responsive" width="100%">
                                <thead>
                                    <tr class="bg-info text-inverse">
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Mulai </th>
                                        <th>Tanggal Berakhir </th>
                                        <th>Keterangan </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jadwal as $key => $val)
                                        <tr>
                                            <td>#</td>
                                            <td>{{ $val['nama'] }}</td>
                                            <td>{{ Carbon\Carbon::parse($val['tgl_mulai'])->formatLocalized('%A %d %B %Y') }}
                                            </td>
                                            <td>{{ Carbon\Carbon::parse($val['tgl_akhir'])->formatLocalized('%A %d %B %Y') }}
                                            </td>
                                            <td>{{ $val['keterangan'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Mulai </th>
                                        <th>Tanggal Berakhir </th>
                                        <th>Keterangan</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</body>

</html>
