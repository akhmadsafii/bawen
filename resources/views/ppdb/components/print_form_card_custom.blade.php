<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: auto;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <table style="width:100%;">
        <thead>
            <tr>
                <td rowspan="4" style="vertical-align: middle">
                    @if ($form['kop']['logo'] != null)
                        <img src="{{ $form['kop']['logo'] }}" style="max-height:138px; min-width: 125px;">
                    @endif
                </td>
                <td style="text-align: center">
                    <b>{{ $form['kop']['header1'] }}</b>
                </td>
                <td rowspan="4" style="vertical-align: middle">
                    @if ($form['kop']['logo2'] != null)
                        <img src="{{ $form['kop']['logo2'] }}" style="max-height:138px; min-width: 125px;">
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <h2 style="margin: 0"><b>{{ $form['kop']['header2'] }}</b></h2>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <b>{{ $form['kop']['header3'] }}</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <b>{{ $form['kop']['alamat'] }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>Kartu
                        Peserta</u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px;" colspan="2">
                    @foreach ($form_setting as $formx => $list)
                        @if ($formx == 0)
                            <table style="width:50%;">
                                <tbody>
                                    @foreach ($form['form'] as $key => $val)
                                        <tr>
                                            <td style="30%;">{{ $val['nama'] }}</td>
                                            <td width="1%">:</td>
                                            <td class="tbl">
                                                @isset($val['value'])
                                                    @switch($val['value'])
                                                        @case('l')
                                                            <small> laki -laki </small>
                                                        @break
                                                        @case('p')
                                                            <small> Perempuan </small>
                                                        @break

                                                        @default
                                                            <small>{{ $val['value'] }}</small>
                                                    @endswitch
                                                @else
                                                    <small class="text-muted">-</small>
                                                @endisset
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($form_setting as $formx => $list)
                        @if ($list['initial'] == 'tampil_foto' && $list['aktif'] == 'Aktif')
                            <img id="modal-preview" class=" float-right" src="{{ session('avatar') }}" alt="Preview"
                                width="150px" height="150px">
                        @endif
                    @endforeach
                </td>
            </tr>
            @foreach ($form_setting as $formx => $list)
                @if ($list['initial'] == 'jadwal_ppdb' && $list['aktif'] == 'Aktif')
                    <tr>
                        <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                            <u>Jadwal</u>
                        </td>
                    </tr>
                    <tr>
                        <table id="table_jadwal" class="table table-striped table-responsive">
                            <thead>
                                <tr class="bg-info text-inverse">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal Mulai </th>
                                    <th>Tanggal Berakhir </th>
                                    <th>Keterangan </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jadwal as $key => $val)
                                    <tr>
                                        <td>#</td>
                                        <td>{{ $val['nama'] }}</td>
                                        <td>{{ Carbon\Carbon::parse($val['tgl_mulai'])->formatLocalized('%A %d %B %Y') }}
                                        </td>
                                        <td>{{ Carbon\Carbon::parse($val['tgl_akhir'])->formatLocalized('%A %d %B %Y') }}
                                        </td>
                                        <td>{{ $val['keterangan'] }}</td>
                                    </tr>

                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal Mulai </th>
                                    <th>Tanggal Berakhir </th>
                                    <th>Keterangan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</body>

</html>
