<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: auto;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <table>
        <thead>
            <tr>
                <td rowspan="4" style="vertical-align: middle">
                    @if ($form[4]['initial'] == 'logo1')
                        <img src="{{ $form[4]['value'] }}" style="max-height:138px; min-width: 125px;">
                    @endif
                </td>
                <td style="text-align: center">
                    @if ($form[0]['initial'] == 'head1')
                        <b>{{ $form[0]['value'] }}</b>
                    @endif
                </td>
                <td rowspan="4" style="vertical-align: middle">
                    @if ($form[5]['initial'] == 'logo2')
                        <img src="{{ $form[5]['value'] }}" style="max-height:138px; min-width: 125px;">
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    @if ($form[1]['initial'] == 'head2')
                        <h2 style="margin: 0"><b>{{ $form[1]['value'] }}</b></h2>
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    @if ($form[2]['initial'] == 'head3')
                        <b>{{ $form[2]['value'] }}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    @if ($form[3]['initial'] == 'alamat')
                        <b>{{ $form[3]['value'] }}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="border: solid 2px #000">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                    <u> Hasil Pengumuman </u>
                </td>
            </tr>
            <tr>
                <td style="height: 10px; text-align: center;" colspan="3">
                    @if ($form[6]['initial'] == 'prolog')
                        <b>{{ $form[6]['value'] }}</b>
                    @endif
                    @if ($form[15]['initial'] == 'nama')
                        <b>{{ $form[15]['value'] }}</b>
                    @endif
                    <br>
                    @if ($form[16]['initial'] == 'nisn')
                        <b> NISN : {{ $form[16]['value'] }}</b>
                    @endif

                    @if ($form[17]['initial'] == 'keputusan')
                        <h1><b>{{ $form[17]['value'] }}</b></h1>
                    @endif
                    @if ($form[18]['initial'] == 'keterangan_keputusan')
                        <b> catatan : {{ $form[18]['value'] ?? '-' }}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form[12]['initial'] == 'tempat_keputusan')
                        <small><b>{{ $form[12]['value'] }}</b></small>,
                    @endif
                    @if ($form[13]['initial'] == 'tgl_keputusan')
                        <small>{{ $form[13]['value'] }}</small>
                    @endif
                    <h5>Kepala Sekolah</h5>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form[10]['initial'] == 'stempel')
                        <img src="{{ $form[10]['value'] }}" width="180px" height="180px" style=" text-align: right;
                                   position: absolute;margin-top: -22px;margin-right: -33px;
                                   margin-left: -95px;
                            ">
                    @endif
                </td>
                <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form[8]['initial'] == 'ttd_kepsek')
                        <img src="{{ $form[8]['value'] }}"
                            style=" text-align: right; max-height:138px; min-width: 125px; padding: 13px; margin-right: -16px;">
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                    @if ($form[9]['initial'] == 'nama_kepsek')
                        <small><u><b>{{ $form[9]['value'] }}</b></u></small><br>
                    @endif
                    @if ($form[11]['initial'] == 'nip_kepsek')
                        <small><b>{{ $form[11]['value'] }}</b></small>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
