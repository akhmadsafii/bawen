<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/demo/favicon.png') }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ session('title') ?? 'Password Reset ' }}</title>
    <!-- CSS -->
    <link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="body-bg-full profile-page" style="background-image: url(assets/demo/night.jpg)">
    <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center login-center-mini mx-auto">
            <div class="navbar-header text-center">
                @if ($message = Session::get('error'))
                    <div class="alert alert-error border-error" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <div class="widget-list">
                            <div class="col-md-12 widget-holder">
                                <div class="widget-body clearfix">
                                    <strong>{{ $message }}</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger border-info mt-1" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <div class="widget-list">
                            <div class="col-md-12 widget-holder">
                                <div class="widget-body clearfix">
                                    <div class="row">
                                        <i class="material-icons list-icon md-48">warning</i>
                                        <ul class="mr-t-10">
                                            @foreach ($errors->all() as $error)
                                                <li class="text-red">{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                        </div>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success border-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <div class="widget-list">
                            <div class="col-md-12 widget-holder">
                                <div class="widget-body clearfix text-center">
                                    <i class="material-icons list-icon">check_circle</i>
                                    <strong>{{ $message }}</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <!-- /.navbar-header -->
            <form class="form-material" name="resetpass" id="resetpass" method="POST"
                action="{{ route('store_reset_pass') }}" enctype="multipart/form-data">
                @csrf
                <p class="text-center text-muted">Enter your new password </p>
                <input type="hidden" name="token_reset" id="token_reset" value="{{ $token }}" />
                <div class="form-group no-gutters input-group input-focused">
                    <input type="password" placeholder="xxx" class="form-control form-control-line" name="password_new"
                        id="password_new">
                    <label for="example-password" class="col-md-12 mb-1">Password New</label>
                    <a href="javascript:void(0)"
                        style="left: auto; right: 0; position: absolute; top: 25%; z-index: 99"><i
                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                </div>
                <div class="form-group no-gutters">
                    <input type="password" placeholder="xxx" class="form-control form-control-line"
                        name="password_confirm" id="password_confirm">
                    <label for="example-password-confirm" class="col-md-12 mb-1">Password Confirm</label>
                    <a href="javascript:void(0)"
                    style="left: auto; right: 0; position: absolute; top: 25%; z-index: 99"><i
                        class="material-icons list-icon eye2 text-dark">remove_red_eye</i></a>
                </div>
                <div class="form-group mb-5">
                    <button class="btn btn-block btn-color-scheme ripple" type="submit">Submit</button>
                </div>
            </form>
            <!-- /.form-material -->
            <footer class="col-sm-12 text-center">
                <hr>
                <p>Back to <a href="{{ route('auth.login') }}" class="text-primary m-l-5"><b>Login</b></a>
                </p>
            </footer>
        </div>
        <!-- /.login-right -->
    </div>
    <!-- /.body-container -->
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ asset('asset/js/material-design.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            const mata = document.querySelector(".eye")
            const inputPass = document.querySelector("#password_new");

            const mata2 = document.querySelector(".eye2")
            const inputPasscon = document.querySelector("#password_confirm");

            mata.addEventListener("click", () => {
                mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                if (inputPass.type === "password") {
                    inputPass.setAttribute("type", "text")

                } else if (inputPass.type === "text") {
                    inputPass.setAttribute("type", "password")
                    mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                }
            })

            mata2.addEventListener("click", () => {
                mata2.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                if (inputPasscon.type === "password") {
                    inputPasscon.setAttribute("type", "text")

                } else if (inputPasscon.type === "text") {
                    inputPasscon.setAttribute("type", "password")
                    mata2.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                }
            })
        });
    </script>
</body>

</html>
