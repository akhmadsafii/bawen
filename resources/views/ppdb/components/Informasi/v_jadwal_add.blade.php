@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Informasi > Tambah Jadwal</h5>
        </div>
        <!-- /.page-title-left -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="JadwalForm" name="JadwalForm" method="POST" class="form-horizontal"
                            enctype="multipart/form-data" action="{{ route('jadwal-simpan') }}">
                            @csrf
                            <div class="modal-body">
                                <span id="form_result"></span>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Nama <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nama" id="nama" autocomplete="off"
                                                        class="form-control" value="{{ old('nama') ?? '' }}"
                                                        placeholder="Masukan sebuah nama " pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja ">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tempat <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="tempat" id="tempat" autocomplete="off"
                                                        class="form-control" value="{{ old('tempat') ?? '' }}"
                                                        placeholder="Masukan sebuah tempat "
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tanggal Mulai <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <input type="text" id="tanggal_mulai" name="tanggal_mulai"
                                                        value="{{ old('tanggal_mulai') ?? '' }}" readonly="readonly"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'>
                                                    <span class="input-group-addon"><i
                                                            class="list-icon material-icons">date_range</i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tanggal Akhir <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <input type="text" id="tanggal_akhir" name="tanggal_akhir"
                                                        value="{{ old('tanggal_akhir') ?? '' }}" readonly="readonly"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'>
                                                    <span class="input-group-addon"><i
                                                            class="list-icon material-icons">date_range</i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Urutan <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="urutan" id="urutan" autocomplete="off"
                                                        class="form-control" value="{{ old('urutan') ?? '' }}"
                                                        placeholder="Masukan number urutan kegiatan pada jadwal "
                                                        pattern="[0-9]*" title="Format harus berisi angka saja ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row ">
                                            <label class="col-md-3 col-form-label" for="l1">Deskripsi <span
                                                    class="text-red">*</span> </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <textarea name="isi" id="isi" rows="10" class="form-control isi text-left"
                                                        placeholder="Masukan deskripsi kegiatan">
                                                                {{ old('isi') ?? '' }}
                                                            </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('informasi-jadwal') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"

            $(document).ready(function() {

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });


                var text = $('.isi');
                text.focus().val('').selectRange(0, 0);


                $.fn.selectRange = function(start, end) {
                    if (!end) end = start;
                    return this.each(function() {
                        if (this.setSelectionRange) {
                            this.focus();
                            this.setSelectionRange(start, end);
                        } else if (this.createTextRange) {
                            var range = this.createTextRange();
                            range.collapse(true);
                            range.moveEnd('character', end);
                            range.moveStart('character', start);
                            range.select();
                        }
                    });
                };

            });

        })(jQuery, window);
    </script>
@endsection
