@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta PPDB</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item ">Akun
                </li>
                <li class="breadcrumb-item"><a href="{{ route('administrator') }} ">Administrator</a></li>
                <li class="breadcrumb-item active">Peserta
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
@endsection
