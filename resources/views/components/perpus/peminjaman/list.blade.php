<!-- <div class="container"> -->
<table class="table table-striped table-responsive" data-toggle="datatables" id="tabel-peminjaman">
    <thead>
        <tr>
            <th>No</th>
            <th>Peminjam</th>
            <th>Buku</th>
            <th>Kode Buku</th>
            <th>Tanggal pinjam</th>
            <th>Tanggal kembali</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>  
                    
	</tbody>
</table>
<!-- </div> -->



<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

	const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: true,
        paging: true,
        searching: true,
   };

   let tabel = $('#tabel-peminjaman');
	let url = "{{ route('peminjaman-active') }}";
 	let column = 
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'peminjam',
	            name: 'peminjam',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "35%",
	            orderable :true
	        },
	        {
	            data: 'kode item',
	            name: 'kode item',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'tanggal pinjam',
	            name: 'tanggal pinjam',
	            width: "20%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal dikembalikan',
	            name: 'tanggal dikembalikan',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'status dipinjam',
	            name: 'status dipinjam',
	            width: "10%",
	            searchable: true
	        }
	    ];

	function MakeTable(tabel,url,column){
        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    }

    MakeTable(tabel,url,column);



</script>