<div class="modal fade bd-example-modal-lg" id="modal-edit-buku" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" id="body-edit-buku">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Edit buku</h5>
        </div>

        <div class="modal-body">
    		<form action="{{ route('buku-store') }}" method="POST" enctype="multipart/form-data">
			@csrf

			<input type="hidden" class="form-control" name="ids" id="ids" placeholder="id">

    		<div class="form-group row">
                <label class="col-md-1 col-form-label" for="l16">Foto</label>
                <div class="col-md-2">
                    <input id="image" type="file" name="image" onchange="preview()">
                </div>
                <div class="col-md-3">
                	<img id="frame" src="" width="100px" height="100px"/>
                </div>
                <label class="col-md-1 col-form-label" for="l16">Lampiran</label>
                <div class="col-md-4">
                    <input id="lampiran" type="file" name="lampiran">
                </div>
            </div>
    		<div class="form-row">
			    <div class="form-group col-md-8">
			      <label for="inputEmail4">Judul</label>
			      <input type="text" class="form-control" name="judul" id="judul" placeholder="judul">
			    </div>
			    <div class="form-group col-md-4">
			      <label for="inputPassword4">Kode</label>
			      <input type="text" class="form-control" name="kode" id="kode" placeholder="kode">
			    </div>
			</div>

			<div class="form-group">
			    <label for="inputAddress">Deskripsi</label>
			    <input type="text" class="form-control" id="desc_buku" name="desc_buku">
			</div>

			<div class="form-group">
			    <label for="inputAddress">Deskripsi fisik</label>
			    <input type="text" class="form-control" id="desc_fisik" name="desc_fisik">
			</div>

			<div class="form-row">
			    <div class="form-group col-md-4">
			      <label for="inputState">Penerbit</label>
			      <select id="id_penerbit" name="id_penerbit" class="form-control">
			      	@foreach($penerbit as $pt)
			        <option value="{{$pt['id']}}">{{$pt['nama']}}</option>
			        @endforeach
			      </select>
			    </div>

			    <div class="form-group col-md-4">
			      <label for="inputState">Bahasa</label>
			      <select id="id_bahasa" name="id_bahasa" class="form-control">
			        @foreach($bahasa as $bhs)
			        <option value="{{$bhs['id']}}">{{$bhs['nama']}}</option>
			        @endforeach
			      </select>
			    </div>

			    <div class="form-group col-md-4">
			      <label for="inputState">Koleksi</label>
			      <select id="id_koleksi" name="id_koleksi" class="form-control">
			        @foreach($koleksi as $kl)
			        <option value="{{$kl['id']}}">{{$kl['nama']}}</option>
			        @endforeach
			      </select>
			    </div>    
			</div>

			<div class="form-row">
			    <div class="form-group col-md-4">
			      <label for="inputState">Jenis</label>
			      <select id="id_jenis" name="id_jenis" class="form-control">
			        @foreach($jenis as $jns)
			        <option value="{{$jns['id']}}">{{$jns['nama']}}</option>
			        @endforeach
			      </select>
			    </div>

			    <div class="form-group col-md-4">
			      <label for="inputState">Lokasi</label>
			      <select id="id_lokasi" name="id_lokasi" class="form-control">
			        @foreach($rak as $rk)
			        <option value="{{$rk['id']}}">{{$rk['nama']}}</option>
			        @endforeach
			      </select>
			    </div>

			    <div class="form-group col-md-4">
			      <label for="inputState">Agen</label>
			      <select id="id_agen" name="id_agen" class="form-control">
			        @foreach($agen as $ag)
			        <option value="{{$ag['id']}}">{{$ag['nama']}}</option>
			        @endforeach
			      </select>
			    </div>    
			</div>

			

			<div class="form-row">
			    <div class="form-group col-md-4">
			      <label for="inputState">Penulis</label>
			      <select class="selectpicker" data-live-search="true" id="id_penulis" name="id_penulis[]" multiple>
			        @foreach($pengarang as $ps)
			        <option value="{{$ps['id']}}">{{$ps['nama']}}</option>
			        @endforeach
			      </select>
			    </div>

			    <div class="form-group col-md-4">
			      <label for="inputState">Topik</label>
			      <select class="selectpicker" data-live-search="true" id="id_topik" name="id_topik[]" multiple>
			        @foreach($topik as $tp)
			        <option value="{{$tp['id']}}">{{$tp['nama']}}</option>
			        @endforeach
			      </select>
			    </div>  

			    <div class="form-group col-md-4">
			      <label for="inputState">GMD</label>
			      <select id="id_gmd" name="id_gmd" class="form-control">
			        @foreach($gmd as $gd)
			        <option value="{{$gd['id']}}">{{$gd['nama']}}</option>
			        @endforeach
			      </select>
			    </div>    
			</div>

			<div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="inputEmail4">ISBN</label>
			      <input type="number" name="isbn" id="isbn" class="form-control" id="inputEmail4">
			    </div>
			    <div class="form-group col-md-3">
			      <label for="inputPassword4">Tahun terbit</label>
			      <input type="number" id="tahun_terbit" name="tahun_terbit" class="form-control" id="inputPassword4">
			    </div>
			    <div class="form-group col-md-3">
			      <label for="inputPassword4">Halaman</label>
			      <input type="number" id="halaman" name="halaman" class="form-control" id="inputPassword4">
			    </div>
			</div>

			<div class="form-group" id="form-jumlah">
			    <label for="inputAddress">Jumlah</label>
			    <input type="text" class="form-control" id="jumlah" name="jumlah">
			</div>
    	</div>

    	<div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Save</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
	    </div>
	    </form>
    </div>
  </div>
</div>