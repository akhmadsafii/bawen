
<div class="container d-flex flex-wrap">

	<!-- Daftar buku -->
	@foreach($pusdigs as $pusdig)
	<div class="col-5 col-md-3 col-xl-2 items mx-2 mx-md-2 my-3">
		<a href="{{ route('detail-pusdig',$pusdig['id']) }}">
			<div class="card-body p-1 pt-3">
			<div class="card-image fit-height">
				<img class="img-book" src="{{$pusdig['foto']}}">
			</div>
			<h6 class="">{{$pusdig['judul']}}</h6>
		</div>
		</a>
	</div>

	@endforeach

	<!-- end daftar pusdig -->

</div>
