<div id="search-section" class="row justify-content-center">
	<div id="search-book" class="d-flex px-0">
		<form id="form-search" method="GET" action="{{ route('search-judul-buku')}}">
		  <input type="text" id="sname" name="nama">
		  <button id="btn-search" type="submit"><i class="fas fa-search"></i></button>
		</form>
		<button data-toggle="modal" data-target=".bs-modal-sm" class="mr-3 btn mr-0"><i class="fas fa-sliders-h"></i>
        </button>
	</div>
</div>

<div class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="mySmallModalLabel">Advance Search</h5>
            </div>
            <div class="modal-body">
                <form id="form-search" method="GET" action="{{ route('search-advance-buku')}}">
                	<div class="form-row">
					    <div class="form-group col-md-12">
					      <label for="inputEmail4">Judul</label>
					      <input type="text" class="form-control" name="judul" placeholder="Judul">
                        </div>
					 </div>
					<div class="form-row">
					    <div class="form-group col-md-6">
					      <label for="inputEmail4">Tahun terbit</label>
					      <input type="number" class="form-control" name="tahun" placeholder="tahun terbit">
					    </div>
					    <div class="form-group col-md-6">
					      <label for="inputPassword4">ISBN</label>
					      <input type="number" class="form-control" name="isbn" placeholder="ISBN">
					    </div>
					</div>

                    <div class="form-row">
					    <div class="form-group col-md-6">
					      <label for="inputEmail4">Bahasa</label>
					      <select class="form-control" name="id_bahasa">
                              <option value=""></option>
                              @foreach ($bahasa as $bhs )
                                <option value="{{ $bhs['id'] }}">{{ $bhs['nama'] }}</option>
                              @endforeach
                          </select>
					    </div>
					    <div class="form-group col-md-6">
                            <label for="inputEmail4">Jenis</label>
                            <select class="form-control" name="id_jenis">
                                <option value=""></option>
                                @foreach ($jenis as $jns )
                                  <option value="{{ $jns['id'] }}">{{ $jns['nama'] }}</option>
                                @endforeach
                            </select>
					    </div>
					</div>

                    <div class="form-row">
					    <div class="form-group col-md-6">
					      <label for="inputEmail4">Koleksi</label>
					      <select class="form-control" name="id_koleksi">
                            <option value=""></option>
                              @foreach ($koleksi as $kls )
                                <option value="{{ $kls['id'] }}">{{ $kls['nama'] }}</option>
                              @endforeach
                          </select>
					    </div>
					    <div class="form-group col-md-6">
                            <label for="inputEmail4">GMD</label>
                            <select class="form-control" name="id_gmd">
                                <option value=""></option>
                                @foreach ($gmd as $gd )
                                  <option value="{{ $gd['id'] }}">{{ $gd['nama'] }}</option>
                                @endforeach
                            </select>
					    </div>
					</div>



					<button type="submit" class="btn btn-success">Cari</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

