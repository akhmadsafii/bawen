<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('includes.program-perpus.head')
    <title>Print Page</title>
    <style type="text/css">
        * {
            -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
            color-adjust: exact !important;                 /*Firefox*/
        }

        body{
            display : inline-block;
            background-color : lightblue;
        }

        .label-main{
            margin : 0;
        }

        .label-body{
            border: 1px solid black;
            width: 350px;
            background-color: white;
        }
            .label-head{
                border-bottom: 1px solid black;
                display: flex;
                align-items: center;
                justify-content: center;
            }

        .label-content{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .label-barcode{
            padding: 3px 5px;
        }

        .alt-code{
            margin-bottom: 0;
        }

        .barcode{
            display: flex;
            justify-content: center;
        }

        img{
            height: 25px;
            margin: 0 10px;
        }

        .label-main{
            display: flex;
        }

        h5,h6{
            margin : 3px 0;
        }


        @media print{

            #printArea{
                display: block;
            }

            body{
                margin : 0;
                box-sizing : border-box;
                display : inline-block;
            }

            .label-main{
                display : inline-block;
                margin : 1.3px 0;
            }

            .alt-code{
                margin-top : 0;
                margin-bottom: 0;
            }

            h6,h5,p{
                text-align: center;
                margin-bottom : 0;
            }

        }


    </style>
</head>
<body>
    <div class="container-fluid" id="printArea">
        {{-- loop --}}
        @foreach ($data as $dt)

            <div class="label-main">
                <div class="label-body">

                    <div class="label-head">
                        <img src="{{ $label['logo']}}">
                        <span>
                            <h5 class="mb-0">{{ $label['nama_label']}}</h5>
                            <h6 class="mb-0 mt-0">{{ $label['nama_instansi']}}</h6>
                            <p class="mb-0">{{ $label['alamat']}}</p>
                        </span>
                    </div>

                    <div class="label-barcode">
                        <div class="barcode">
                            {!! DNS1D::getBarcodeHTML( $dt['kode'],'C128',2.0,25) !!}

                        </div>
                        <p class="alt-code">{{ $dt['kode'] }}</p>

                    </div>
                </div>
            </div>

        @endforeach
    </div>

    <script>

        function PrintWindow() {
            let labelMain = `
                <style type="text/css">
                    * {
                        -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
                        color-adjust: exact !important;                 /*Firefox*/
                    }

                    body{
                        display : inline-block;
                        background-color : lightblue;
                    }

                    .label-main{
                        margin : 0;
                    }

                    .label-body{
                        border: 1px solid black;
                        width: 350px;
                        background-color: white;
                    }
                        .label-head{
                            border-bottom: 1px solid black;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                        }

                    .label-content{
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }

                    .label-barcode{
                        padding: 3px 5px;
                    }

                    .alt-code{
                        margin-bottom: 0;
                    }

                    .barcode{
                        display: flex;
                        justify-content: center;
                    }

                    img{
                        height: 25px;
                        margin: 0 10px;
                    }

                    .label-main{
                        display: flex;
                    }

                    h5,h6{
                        margin : 3px 0;
                    }


                    @media print{

                        #printArea{
                            display: block;
                        }

                        body{
                            margin : 0;
                            box-sizing : border-box;
                            display : inline-block;
                        }

                        .label-main{
                            display : inline-block;
                            margin : 1.3px 0;
                        }

                        .alt-code{
                            margin-top : 0;
                            margin-bottom: 0;
                        }

                        h6,h5,p{
                            text-align: center;
                            margin-bottom : 0;
                        }

                    }


                </style>
            `;
            var divContents = document.getElementById("printArea").innerHTML;
            let result = labelMain.concat(divContents);

            var a = window.open('', '', 'height=1000, width=1200');
                a.document.write('<html>');
                a.document.write('<body>');
                a.document.write('<div class="main">');
                a.document.write(result);
                a.document.write('</div>');
                a.document.write('</body></html>');
                a.document.close();
                a.print();
        }

        document.addEventListener('DOMContentLoaded', (event) => {
            PrintWindow();
        });



    </script>
</body>
</html>
