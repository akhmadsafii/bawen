<style>

    .sidebar .nav-link.active {
        color: #007bff;
    }

    .sidebar .nav-link {
        font-weight: 500;
        color: #333;
    }

    .dropdown-menu ,.show{
        background: rgb(248, 249, 250);
        border: none;
        padding-left: 10px;
    }


</style>

<nav class="col-md-2 d-none d-md-block bg-light sidebar py-3">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'dashboard'? 'active' : '' }}" href="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            Dashboard <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'pesan'? 'active' : '' }}" href="{{route('main-pesan')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Pesan
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'data-sekolah'? 'active' : '' }}" href="{{route('main-sekolah')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Data Sekolah
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'data-dokumen'? 'active' : '' }}" href="{{route('main-data-dokumen')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Data Dokumen
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'dokumen'? 'active' : '' }}" href="{{route('Dokumen')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Dokumen
                            </a>
                        </li>
                        
                        @if(session('role-user') === 'admin')
                        <li class="nav-item">
                            <a class="nav-link {{Request::segment(3)== 'data-user'? 'active' : '' }}" href="{{route('user-sekolah')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Data User
                            </a>
                        </li>
                        @endif

                        
                        
                        </ul>
                    </div>
                </nav>