<style>
    .side-user{
        background: #fff;
        padding: 18px;
    }

    .side-user .side-menu li a:hover{
        color: #1f2021 !important;
    }

</style>     
        
<div class="side-user bg-white p-2 pt-3">
    <a class="col-sm-12 media clearfix d-flex align-items-center" href="javascript:void(0);">
        <figure class="media-left media-middle mr-r-10 mr-b-0">
            <img src="{{$profile["file"]}}"
                class="media-object rounded-circle" alt="image profile" height="40px" width="40px">
        </figure>
        <div class="media-body hide-menu">
            <h4 class="media-heading mr-b-5 text-uppercase text-dark">{{$profile["nama"]}}</h4>
            <span class="user-type fs-12">{{$profile["role"]}}</span>
        </div>
    </a>
    <div class="clearfix"></div>

    <ul class="nav in side-menu">
        <li class="my-4">
            <a href="{{route('edit-profile-mutu')}}">
                <i class="list-icon material-icons">face</i>
                Edit profil
            </a>
        </li>
        
        <li class="my-4">
            <a href="{{route('auth.logout')}}">
                <i class="list-icon material-icons">settings_power</i>
                Logout
            </a>
        </li>
    </ul>
</div>