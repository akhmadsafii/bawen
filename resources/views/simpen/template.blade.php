<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
    <script type="text/javascript">
        //disabled pace loading
        window.paceOptions = {
            ajax: false,
            restartOnRequestAfter: false,
        };
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ session('logo') ?? asset('asset/img/sma.png') }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ !empty(Session::get('title')) ? session('title') : 'Demo Smartschool' }}</title>
    <!-- CSS -->
    <link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
        rel="stylesheet" type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"
        integrity="sha512-lteuRD+aUENrZPTXWFRPTBcDDxIGWe5uu0apPEn+3ZKYDwDaEErIK9rvR0QzUGmUQ55KFE2RqGTVoZsKctGMVw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @if (session('role') == 'admin-simpen' || session('role') == 'siswa-simpen')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
            type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css"
            rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet"
            type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css"
            rel="stylesheet" type="text/css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js" defer></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" defer></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" defer></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js" defer></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js" defer></script>
    @endif
    <style>
        .dropdown-list-group {
            height: auto;
            max-height: 21.875rem;
            position: relative;
            margin: 0 -1.78571em 0 -2.14286em;
            padding: 0 1.78571em 0 2.14286em;
        }

    </style>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header">
                @php
                    if (session('login') == true) {
                        $route = route('dashboard-simpen');
                    } else {
                        $route = route('public-simpen');
                    }
                @endphp
                <a href="{{ $route }}" class="navbar-brand text-center">
                    <img class="logo-expand" style="max-width: 30%" alt=""
                        src="{{ session('logo') ?? asset('asset/img/sma.png') }}">
                    <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}">
                    <!-- <p>OSCAR</p> -->
                    <span class="text-white logo-expand ml-3"> SISTEM PENGUMUMAN KELULUSAN </span>
                    <span class="text-white logo-collapse">SIMPEN</span>
                </a>
            </div>
            <!-- /.navbar-header -->
            <!-- Left Menu & Sidebar Toggle -->
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle ml-5"><a href="javascript:void(0)" class="ripple"><i
                            class="material-icons list-icon">menu</i></a>
                </li>
            </ul>
            <!-- /.navbar-left -->
            <div class="spacer"></div>
            @if (session('login') == true)
                <ul class="nav navbar-nav">
                    <li class="dropdown hidden"><a href="javascript:void(0);" class="dropdown-toggle ripple"
                            data-toggle="dropdown" aria-expanded="true">
                            <span
                                class="text-white">{{ str_replace('-simpen', ' ', ucfirst(session('role'))) }}</span>
                            <span class="avatar thumb-sm"><img src="{{ session('avatar') }}" class="rounded-circle"
                                    alt=""> <i class="material-icons list-icon">expand_more</i></span></a>
                        <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-wide hidden">
                            <div class="card">
                                <header class="card-heading-extra">
                                    <div class="row">
                                        <div class="col-6">
                                            <h3 class="mr-b-10 sub-heading-font-family fw-300">
                                                {{ session('username') }}
                                            </h3><span class="user--online">Available
                                                <i class="material-icons list-icon">expand_more</i>
                                            </span>

                                        </div>
                                        <div class="col-6 d-flex justify-content-end">
                                            <ul class="list-unstyled">
                                                @if (session('role') == 'admin-simpen')
                                                    <li> <a href="{{ route('profil-simpen') }}"><i
                                                                class="material-icons list-icon">account_circle</i>
                                                            Update Profil</a></li>
                                                    <li> <a href="{{ route('change-password-simpen') }}"><i
                                                                class="material-icons list-icon">vpn_key</i>
                                                            Change Password</a></li>
                                                @endif
                                                <li><a href="{{ route('logout-simpen') }}"><i
                                                            class="material-icons list-icon">power_settings_new</i>
                                                        Logout</a></li>
                                            </ul>
                                        </div>
                                        <!-- /.col-4 -->
                                    </div>
                                    <!-- /.row -->
                                </header>
                            </div>
                        </div>
                    </li>
                </ul>
            @endif
        </nav>
        <!-- /.navbar -->
        <div class="content-wrapper">
            <!-- SIDEBAR -->
            <aside class="site-sidebar clearfix">
                @include('simpen.includes.navbar')
            </aside>
            <!-- /.site-sidebar -->
            <main class="main-wrapper clearfix">
                @yield('simpen.components')
            </main>
            <!-- /.main-wrappper -->
        </div>
        <!-- /.content-wrapper -->
        <!-- FOOTER -->
        @if (session('role') != 'superadmin')
            <footer class="footer text-center clearfix">
                {{ !empty(Session::get('footer')) ? Session::get('footer') : '2021 © ' . strtoupper(Session::get('sekolah')) }}
            </footer>
        @else
            <footer class="footer text-center clearfix">2021 © Tim Dev Bumitekno</footer>
        @endif
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js" defer>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" defer>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="{{ asset('asset/js/theme2.js') }}"></script>
    <script src="{{ asset('asset/js/service_worker.js') }}"></script>
    @if (session('role') == 'admin-simpen')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
                integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script type="text/javascript">
            (function($, global) {
                "use-strict"
                $(document).ready(function() {
                    tinymce.init({
                        selector: "textarea.editor",
                        branding: false,
                        width: "100%",
                        height: "350",
                        plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table  paste  wordcount"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                        image_title: true,
                        file_picker_types: 'image',
                        file_picker_callback: function(cb, value, meta) {
                            var input = document.createElement('input');
                            input.setAttribute('type', 'file');
                            input.setAttribute('accept', 'image/*');

                            /*
                              Note: In modern browsers input[type="file"] is functional without
                              even adding it to the DOM, but that might not be the case in some older
                              or quirky browsers like IE, so you might want to add it to the DOM
                              just in case, and visually hide it. And do not forget do remove it
                              once you do not need it anymore.
                            */

                            input.onchange = function() {
                                var file = this.files[0];

                                var reader = new FileReader();
                                reader.onload = function() {
                                    /*
                                      Note: Now we need to register the blob in TinyMCEs image blob
                                      registry. In the next release this part hopefully won't be
                                      necessary, as we are looking to handle it internally.
                                    */
                                    var id = 'blobid' + (new Date()).getTime();
                                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                    var base64 = reader.result.split(',')[1];
                                    var blobInfo = blobCache.create(id, file, base64);
                                    blobCache.add(blobInfo);

                                    /* call the callback and populate the Title field with the file name */
                                    cb(blobInfo.blobUri(), {
                                        title: file.name
                                    });
                                };
                                reader.readAsDataURL(file);
                            };

                            input.click();
                        }
                        //images_upload_handler:function(blobInfo, success, failure){}
                    });
                });
            })(jQuery, window);
        </script>
    @endif

</body>
