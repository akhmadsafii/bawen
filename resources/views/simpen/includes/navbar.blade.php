<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        @switch(session('role'))
            @case('admin-simpen')
                <li><a href="{{ $route }}"> <i class="material-icons list-icon">dashboard</i> Dashboard</a>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Master Data
                        </span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="{{ route('master_admin_simpen') }}">Admin</a>
                        </li>
                        <li>
                            <a href="{{ route('master_siswa_index_simpen') }}">Siswa</a>
                        </li>
                        <li>
                            <a href="{{ route('nilai_siswa_index_simpen', ['tahun' => date('Y')]) }}">Input Nilai Siswa </a>
                        </li>

                    </ul>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">folder_open</i> <span class="hide-menu">Master
                            Halaman</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="{{ route('setting-sambutan-simpen') }}">Sambutan</a>
                        </li>
                        <li><a href="{{ route('setting-panduan-simpen') }}">Panduan</a>
                        </li>
                        <li><a href="{{ route('setting-informasi-simpen') }}">Informasi</a>
                        </li>
                    </ul>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">perm_data_setting</i> <span class="hide-menu">Pengaturan
                        </span></a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Template</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting-simpen') }}">Pengumuman Kelulusan</a>
                                </li>

                                <li class="menu-item-has-children"><a href="javascript:void(0);">Surat Keterangan Lainnya</a>
                                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false">

                                        <li><a href="{{ route('setting_template_other_simpen') }}">Surat Keterangan
                                                Lulus</a>
                                        </li>
                                        <li><a href="{{ route('setting_otheralumni_simpen') }}">Surat Keterangan
                                                Alumni</a>
                                        </li>
                                        <li><a href="{{ route('setting_otherskhun_simpen') }}">Surat Pendukung</a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Setting Template</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting-surat-simpen') }}">Pengumuman Kelulusan</a>
                                </li>
                                <li><a href="{{ route('setting_pendukung_surat') }}">Surat Keterangan Lainnya</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Nilai Mata Pelajaran</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting_kategori_nilai_mapel') }}">Kategori Nilai Pelajaran </a>
                                </li>
                                <li><a href="{{ route('setting-nilai-mapel-simpen') }}">Mata Pelajaran </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            @break

            @case('admin')
                <li><a href="{{ $route }}"> <i class="material-icons list-icon">dashboard</i> Dashboard</a>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Master Data
                        </span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="{{ route('master_admin_simpen') }}">Admin</a>
                        </li>
                        <li>
                            <a href="{{ route('master_siswa_index_simpen') }}">Siswa</a>
                        </li>
                        <li>
                            <a href="{{ route('nilai_siswa_index_simpen', ['tahun' => session('tahun')]) }}">Input Nilai
                                Siswa</a>
                        </li>
                    </ul>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">folder_open</i> <span class="hide-menu">Master
                            Halaman</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="{{ route('setting-sambutan-simpen') }}">Sambutan</a>
                        </li>
                        <li><a href="{{ route('setting-panduan-simpen') }}">Panduan</a>
                        </li>
                        <li><a href="{{ route('setting-informasi-simpen') }}">Informasi</a>
                        </li>
                    </ul>
                </li>
                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i
                            class="list-icon material-icons">perm_data_setting</i> <span class="hide-menu">Pengaturan
                        </span></a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Template</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting-simpen') }}">Pengumuman Kelulusan</a>
                                </li>
                                <li class="menu-item-has-children"><a href="javascript:void(0);">Surat Keterangan Lainnya</a>
                                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false">

                                        <li><a href="{{ route('setting_template_other_simpen') }}">Surat Keterangan
                                                Lulus</a>
                                        </li>
                                        <li><a href="{{ route('setting_otheralumni_simpen') }}">Surat Keterangan
                                                Alumni</a>
                                        </li>
                                        <li><a href="{{ route('setting_otherskhun_simpen') }}">Surat Pendukung</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Surat</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting-surat-simpen') }}">Pengumuman Kelulusan</a>
                                </li>
                                <li><a href="{{ route('setting_pendukung_surat') }}">Surat Keterangan Lainnya</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Nilai Mata Pelajaran</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false">
                                <li><a href="{{ route('setting_kategori_nilai_mapel') }}">Kategori Nilai Pelajaran </a>
                                </li>
                                <li><a href="{{ route('setting-nilai-mapel-simpen') }}">Mata Pelajaran </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="{{ route('logout-simpen') }}"><i class="material-icons list-icon">power_settings_new</i>
                        Logout</a>
                </li>
            @break

            @case('siswa-simpen')
                <li><a href="{{ route('dashboard-simpen-siswa') }}"><i class="material-icons">home</i> Beranda</a>
                </li>
                {{-- <li><a href="#panduan"><i class="material-icons">book</i> Panduan</a>
                </li>
                <li><a href="#informasi"><i class="material-icons">info</i> Informasi</a>
                </li> --}}
                <li><a href="{{ route('dashboard-pengumuman_siswa') }}" id="resultx"><i class="material-icons">drafts</i>
                        Hasil Pengumuman </a>
                </li>
            @break

            @default
                <li><a href="#sambutan"> <i class="material-icons">perm_identity</i> Sambutan</a>
                </li>
                <li><a href="#panduan"><i class="material-icons">book</i> Panduan</a>
                </li>
                <li><a href="#informasi"><i class="material-icons">info</i> Informasi</a>
                </li>
                <li><a href="#login" id="loginx"><i class="material-icons">lock</i> Login</a>
                </li>
        @endswitch
    </ul>
</nav>
