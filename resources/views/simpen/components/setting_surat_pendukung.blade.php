@extends('simpen.template')
@section('simpen.components')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Surat Keterangan Lainnya
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <div class="row justify-content-center">
            <div class="col-md-8 widget-holder">
                <div class="widget-bg">
                    <label><input type="checkbox" class="selectAll" name="checkall" value="1"> Check / Uncheck
                        All</label>
                    <form id="SettingForm" name="SettingForm" method="POST" novalidate="novalidate" class="form-horizontal"
                        enctype="multipart/form-data" action="{{ route('store_setting_suratpsim') }}">
                        @csrf
                        <div class="widget-body clearfix">
                            <div class="table-responsive">
                                <table id="table_setting_surat" class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Surat Nilai</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox"
                                                        {{ !empty($rows) && $rows['surat_nilai'] == 1 ? 'checked' : '' }}
                                                        class="checkSetting" data-id="nilai" name="surat_nilai">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Surat Keterangan Lulus</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox"
                                                        {{ !empty($rows) && $rows['surat_keterangan_lulus'] == 1 ? 'checked' : '' }}
                                                        class="checkSetting" data-id="lulus" name="surat_lulus">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Surat Keterangan Alumni</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox"
                                                        {{ !empty($rows) && $rows['surat_keterangan_alumni'] == 1 ? 'checked' : '' }}
                                                        class="checkSetting" data-id="alumni" name="surat_alumni">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Surat Keterangan Lain</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox"
                                                        {{ !empty($rows) && $rows['surat_keterangan_lain'] == 1 ? 'checked' : '' }}
                                                        class="checkSetting" data-id="lain" name="surat_lain">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-actions text-center">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 btn-list">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons list-icon">save</i>
                                            Simpan
                                        </button>
                                    </div>
                                    <!-- /.col-sm-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            $(document).ready(function() {
                $('.selectAll').click(function(event) {
                    if (this.checked) {
                        // Iterate each checkbox
                        $(':checkbox').each(function() {
                            this.checked = true;
                        });
                    } else {
                        $(':checkbox').each(function() {
                            this.checked = false;
                        });
                    }
                });

                $(document).on('click', '.checkSetting', function() {
                    let value = $(this).is(':checked') ? 1 : 0;
                    $(this).val("tes");
                });
            });
        })(jQuery, window);
    </script>
@endsection
