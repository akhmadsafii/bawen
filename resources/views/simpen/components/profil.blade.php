@extends('simpen.template')
@section('simpen.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Profil </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Update Profil
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center mx-auto">
        <form id="ProfilForm" name="ProfilForm" method="POST" class="form-horizontal" action="{{ route('updateProfil-simpen') }}"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="form-group row">
                                <p class="mr-b-0">Nama <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="nama" type="text" name="nama" placeholder="Agus Budiman"
                                        pattern="[a-zA-Z][a-zA-Z0-9\s]*" title="Format harus berisi alfanumerik atau huruf"
                                        class="form-control" value="{{ $profil_data['nama'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Alamat <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <textarea name="alamat" id="alamat"
                                        placeholder="Jl.Sukorejo-Parakan Km 3 Kabupaten Kendal , Jawa Tengah"
                                        class="form-control">{{ $profil_data['alamat'] ?? '-' }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Kota <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="kota" type="text" name="kota" placeholder="Semarang"
                                        title="format harus berisi huruf saja" pattern="[a-zA-Z\s]*" class="form-control"
                                        value="{{ $profil_data['tempat_lahir'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Telepon <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="telephone" type="number" name="telephone" placeholder="0824207872613"
                                        title="format harus berisi angka saja" pattern="^\d{13}$" maxlength="13"
                                        class="form-control" value="{{ $profil_data['telepon'] ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Email <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="email" type="email" name="email" placeholder="admin@yahoo.co.id"
                                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"
                                        value="{{ $profil_data['email'] ?? '' }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <p class="mr-b-0">Foto / Avatar</p>
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    @isset($profil_data['file'])
                                        <img id="modal-preview2" src="{{ $profil_data['file'] }}" alt="Preview"
                                            class="form-group mb-1" width="50px" height="50px">
                                    @endisset
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group mb-1" width="150px" height="150px">
                                <p class="text-muted ml-2">Kosongkon form ini jika tidak ada perubahan gambar</p>
                            </div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions text-center">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $(document).ready(function() {

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

            });

        })(jQuery, window);
    </script>
@endsection
