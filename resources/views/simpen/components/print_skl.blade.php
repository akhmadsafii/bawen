<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style>
        body {
            font-family: arial;
            font-size: 12px;
        }

        .table {
            font-family: sans-serif;
            color: #444;
            border-collapse: collapse;
            width: 50%;
            border: 1px solid #f2f5f7;
        }

        .table tr th {
            background: #35A9DB;
            color: #fff;
            font-weight: normal;
        }

        .table,
        th,
        td {
            padding: 1px 20px;
        }

        .table tr:hover {
            background-color: #f5f5f5;
        }

        .table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
</head>

<body>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td rowspan="5" style="vertical-align: middle">
                    @if ($rows[0]['initial'] == 'logo1' && $rows[0]['aktif'] == 1)
                        <img src="{{ $rows[0]['value'] }}"
                            style="max-height:100px; min-width: 100px;margin-left:12px;">
                    @endif
                </td>

                @if ($rows[2]['initial'] == 'head1' && $rows[2]['aktif'] == 1)
                    <td style="text-align: center">
                        <b>{{ $rows[2]['value'] }}</b>
                    </td>
                @endif


                <td rowspan="5" style="vertical-align: middle">
                    @if ($rows[1]['initial'] == 'logo2' && $rows[1]['aktif'] == 1)
                        <img src="{{ $rows[1]['value'] }}"
                            style="max-height:100px; min-width: 100px;margin-left:15px;">
                    @endif
                </td>

            </tr>
            @if ($rows[3]['initial'] == 'head2' && $rows[3]['aktif'] == 1)
                <tr>
                    <td style="text-align: center">
                        <b>{{ $rows[3]['value'] }}</b>
                    </td>
                </tr>
            @endif
            @if ($rows[4]['initial'] == 'head3' && $rows[4]['aktif'] == 1)
                <tr>
                    <td style="text-align: center">
                        <b>{{ $rows[4]['value'] }}</b>
                    </td>
                </tr>
            @endif
            @if ($rows[5]['initial'] == 'head4' && $rows[5]['aktif'] == 1)
                <tr>
                    <td style="text-align: center">
                        <b>{{ $rows[5]['value'] }}</b>
                    </td>
                </tr>
            @endif
            @if ($rows[6]['initial'] == 'alamat' && $rows[6]['aktif'] == 1)
                <tr>
                    <td style="text-align: center">
                        <b><small>{{ $rows[6]['value'] }}</small></b>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <br>
    <hr style="border: solid 2px #000">
    <br>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                    @if ($rows[7]['initial'] == 'judul' && $rows[7]['aktif'] == 1)
                        <b><u>{{ $rows[7]['value'] }}</u></b>
                    @endif
                    <br>
                    @if ($rows[8]['initial'] == 'no_surat' && $rows[8]['aktif'] == 1)
                        <b> No : {{ $rows[8]['value'] }}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    @if ($rows[9]['initial'] == 'prolog' && $rows[9]['aktif'] == 1)
                        <b>{!! $rows[9]['value'] !!}</b>
                    @endif
                    @if ($rows[10]['initial'] == 'isi' && $rows[10]['aktif'] == 1)
                        {!! $rows[10]['value'] !!}
                    @endif
                    <table>
                        <tbody>
                            @if ($rows[20]['initial'] == 'nama_siswa' && $rows[20]['aktif'] == 1 && !empty($rows[30]['value']))
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td><b>{{ $rows[20]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[25]['initial'] == 'no_peserta' && $rows[25]['aktif'] == 1 && !empty($rows[25]['value']))
                                <tr>
                                    <td>No Peserta </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[25]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[26]['initial'] == 'no_ujian' && $rows[26]['aktif'] == 1 && !empty($rows[26]['value']))
                                <tr>
                                    <td>No Ujian </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[26]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[28]['initial'] == 'nis' && $rows[28]['aktif'] == 1 && !empty($rows[28]['value']))
                                <tr>
                                    <td>NIS </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[28]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[29]['initial'] == 'nisn' && $rows[29]['aktif'] == 1 && !empty($rows[29]['value']))
                                <tr>
                                    <td>NISN </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[29]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[31]['initial'] == 'npsn' && $rows[31]['aktif'] == 1 && !empty($rows[31]['value']))
                                <tr>
                                    <td>NPSN </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[31]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[21]['initial'] == 'kelas' && $rows[21]['aktif'] == 1 && !empty($rows[21]['value']))
                                <tr>
                                    <td>Kelas </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[21]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[24]['initial'] == 'jurusan' && $rows[24]['aktif'] == 1 && !empty($rows[24]['value']))
                                <tr>
                                    <td>Jurusan </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[24]['value'] }}</u></td>
                                </tr>
                            @endif
                            @if ($rows[23]['initial'] == 'ortu' && $rows[23]['aktif'] == 1 && $rows[23]['value'] != null)
                                <tr>
                                    <td>Nama Orang tua </td>
                                    <td>:</td>
                                    <td><b>{{ $rows[23]['value'] }}</u></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <br>
                    <center>
                        @if ($rows[18]['initial'] == 'keputusan' && $rows[18]['aktif'] == 1 && !empty($rows[18]['value']))
                            <b>Dinyatakan </b> <br>
                            <h4><b>{{ Str::ucfirst($rows[18]['value']) }}</b></h4>
                        @endif
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    @if ($rows[11]['initial'] == 'penutup' && $rows[11]['aktif'] == 1)
                        <b>{!! $rows[11]['value'] !!}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                </td>
                <td>
                    <div>
                        @if ($rows[12]['initial'] == 'tempat' && $rows[12]['aktif'] == 1)
                            <small style="font-size: 12px"><b>{{ $rows[12]['value'] }}</b></small>,
                        @endif
                        @if ($rows[13]['initial'] == 'tgl' && $rows[13]['aktif'] == 1)
                            <small
                                style="font-size: 12px">{{ \Carbon\Carbon::parse($rows[13]['value'])->isoFormat('dddd, D MMM Y') }}</small>
                        @endif
                        <br>
                        Kepala Sekolah, <br>
                        @if ($rows[15]['initial'] == 'stempel' && $rows[15]['aktif'] == 1)
                            <img src="{{ $rows[15]['value'] }}"
                                style="text-align: right; position: absolute;margin-top: 10px;margin-right: -33px;margin-left:-50px; max-height:138px; min-width: 125px;">
                        @endif
                        @if ($rows[16]['initial'] == 'ttd_kepsek' && $rows[16]['aktif'] == 1)
                            <img src="{{ $rows[16]['value'] }}"
                                style=" text-align: right; max-height:138px; min-width: 125px; padding: 13px; margin-right:0px;">
                        @endif
                        <br>
                        @if ($rows[14]['initial'] == 'kepsek' && $rows[14]['aktif'] == 1)
                            <small><u><b>{{ $rows[14]['value'] }}</b></u></small><br>
                        @endif
                        @if ($rows[17]['initial'] == 'nip_kepsek' && $rows[17]['aktif'] == 1)
                            <small><b>{{ $rows[17]['value'] }}</b></small>
                        @endif
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="dontsplit"></div>
</body>

</html>
