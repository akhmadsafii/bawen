<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 20.5in;
            padding: 30px 10px 5px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 1px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: A4 portrait;
                margin: 0mm;
            }

            thead {
                display: table-row-group;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    @if (count($pengumuman) > 0)
        <table width="100%">
            <thead>
                <tr>
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($pengumuman[0]['initial'] == 'logo1' && $pengumuman[0]['aktif'] == 1)
                            <img src="{{ $pengumuman[0]['value'] }}"
                                style="max-height:138px; min-width: 125px;margin-left:30px;">
                        @endif
                    </td>
                    <td style="text-align: center">
                        @if ($pengumuman[2]['initial'] == 'head1' && $pengumuman[2]['aktif'] == 1)
                            <b>{{ $pengumuman[2]['value'] }}</b>
                        @endif
                    </td>
                    <td rowspan="4" style="vertical-align: middle">
                        @if ($pengumuman[1]['initial'] == 'logo2' && $pengumuman[1]['aktif'] == 1)
                            <img src="{{ $pengumuman[1]['value'] }}"
                                style="max-height:138px; min-width: 125px; margin-right:10px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        @if ($pengumuman[3]['initial'] == 'head2' && $pengumuman[3]['aktif'] == 1)
                            <b>{{ $pengumuman[3]['value'] }}</b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        @if ($pengumuman[4]['initial'] == 'head3' && $pengumuman[4]['aktif'] == 1)
                            <b>{{ $pengumuman[4]['value'] }}</b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        @if ($pengumuman[5]['initial'] == 'head4' && $pengumuman[5]['aktif'] == 1)
                            <b>{{ $pengumuman[5]['value'] }}</b>
                        @endif
                        @if ($pengumuman[6]['initial'] == 'alamat' && $pengumuman[6]['aktif'] == 1)
                            <br><b>{{ $pengumuman[6]['value'] }}</b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr style="border: solid 2px #000">
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                        @if ($pengumuman[7]['initial'] == 'judul' && $pengumuman[7]['aktif'] == 1)
                            <b><u>{{ $pengumuman[7]['value'] }}</u></b>
                        @endif
                        <br>
                        @if ($pengumuman[8]['initial'] == 'no_surat' && $pengumuman[8]['aktif'] == 1)
                            <b> No : {{ $pengumuman[8]['value'] }}</b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        @if ($pengumuman[9]['initial'] == 'prolog' && $pengumuman[9]['aktif'] == 1)
                            <b>{!! $pengumuman[9]['value'] !!}</b>
                        @endif
                        @if ($pengumuman[10]['initial'] == 'isi' && $pengumuman[10]['aktif'] == 1)
                            {!! $pengumuman[10]['value'] !!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <center>
                            <table width="50%" border="1">
                                <tbody>
                                    @if ($pengumuman[20]['initial'] == 'nama_siswa' && $pengumuman[20]['aktif'] == 1)
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[20]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[25]['initial'] == 'no_peserta' && $pengumuman[25]['aktif'] == 1)
                                        <tr>
                                            <td>No Peserta </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[25]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[26]['initial'] == 'no_ujian' && $pengumuman[26]['aktif'] == 1)
                                        <tr>
                                            <td>No Ujian </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[26]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[28]['initial'] == 'nis' && $pengumuman[28]['aktif'] == 1)
                                        <tr>
                                            <td>NIS </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[28]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[29]['initial'] == 'nisn' && $pengumuman[29]['aktif'] == 1)
                                        <tr>
                                            <td>NISN </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[29]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[31]['initial'] == 'npsn' && $pengumuman[31]['aktif'] == 1)
                                        <tr>
                                            <td>NPSN </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[31]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[21]['initial'] == 'kelas' && $pengumuman[21]['aktif'] == 1)
                                        <tr>
                                            <td>Kelas </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[21]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[24]['initial'] == 'jurusan' && $pengumuman[24]['aktif'] == 1)
                                        <tr>
                                            <td>Jurusan </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[24]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                    @if ($pengumuman[23]['initial'] == 'ortu' && $pengumuman[23]['aktif'] == 1 && $pengumuman[23]['value'] != null)
                                        <tr>
                                            <td>Nama Orang tua </td>
                                            <td>:</td>
                                            <td><b>{{ $pengumuman[23]['value'] }}</u></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" style="text-align: center; font-weight: bold; font-size: 12pt">
                        <br><b>Dinyatakan </b>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" style="text-align: center; font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[18]['initial'] == 'keputusan' && $pengumuman[18]['aktif'] == 1)
                            <h1><b>{{ Str::ucfirst($pengumuman[18]['value']) }}</b></h1>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[19]['initial'] == 'daftar_nilai' && $pengumuman[19]['aktif'] == 1)
                            <p style="text-align:center">
                                Dengan nilai sebagai berikut:
                            </p>
                            <center>
                                <table width="50%" border="1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Mata Pelajaran </th>
                                            <th> Nilai </th>
                                        </tr>
                                    </thead>
                                    @foreach ($pengumuman[19]['value'] as $val)
                                        <tr>
                                            <td>{{ $loop->iteration }} </td>
                                            <td>{{ $val['nama'] }}</td>
                                            <td>{{ $val['nilai'] }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </center>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        @if ($pengumuman[11]['initial'] == 'penutup' && $pengumuman[11]['aktif'] == 1)
                            <b>{!! $pengumuman[11]['value'] !!}</b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[12]['initial'] == 'tempat' && $pengumuman[12]['aktif'] == 1)
                            <small><b>{{ $pengumuman[12]['value'] }}</b></small>,
                        @endif
                        @if ($pengumuman[13]['initial'] == 'tgl' && $pengumuman[13]['aktif'] == 1)
                            <small>{{ \Carbon\Carbon::parse($pengumuman[13]['value'])->isoFormat('dddd, D MMM Y') }}</small>
                        @endif
                        <h5>Kepala Sekolah</h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right; font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[15]['initial'] == 'stempel' && $pengumuman[15]['aktif'] == 1)
                            <img src="{{ $pengumuman[15]['value'] }}" width="125px" height="125px"
                                style="text-align: right; position: absolute;margin-top: -10px;margin-right: -33px;margin-left:-50px;">
                        @endif
                    </td>
                    <td colspan="2" style="text-align: right; font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[16]['initial'] == 'ttd_kepsek' && $pengumuman[16]['aktif'] == 1)
                            <img src="{{ $pengumuman[16]['value'] }}"
                                style=" text-align: right; max-height:128px; min-width: 128px; padding: 5px; margin-right:0px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; font-weight: bold; font-size: 12pt">
                        @if ($pengumuman[14]['initial'] == 'kepsek' && $pengumuman[14]['aktif'] == 1)
                            <small><u><b>{{ $pengumuman[14]['value'] }}</b></u></small><br>
                        @endif
                        @if ($pengumuman[17]['initial'] == 'nip_kepsek' && $pengumuman[17]['aktif'] == 1)
                            <small><b>{{ $pengumuman[17]['value'] }}</b></small>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    @endif
</body>

</html>
