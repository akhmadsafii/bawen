@extends('simpen.template')
@section('simpen.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Kategori Nilai Mata Pelajaran
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshow" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Informasi </h5>
                </div>
                <div class="modal-body">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td class="text-muted nama"></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Kategori Mapel Informasi </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_mapel" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_edit" id="nama_edit" autocomplete="off"
                                            class="form-control" placeholder="Nama">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Mapel Informasi </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Kode</td>
                                    <td class="text-muted">
                                        <input type="text" name="kode_post" id="kode_post" autocomplete="off"
                                            class="form-control" placeholder="Kode">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_post" id="nama_post" autocomplete="off"
                                            class="form-control" placeholder="Nama">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info post ">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_mapel" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_master_kategorisimpen') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_mapel').dataTable(config);


                $('body').on('click', '.show.mapel', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_kategorinsimpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshow').modal('show');
                                    $('.text-muted.nama').html(rows.nama);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.mapel', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_kategorinsimpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_mapel"]').val(rows.id);
                                    $('input[name="nama_edit"]').val(rows.nama);
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });
                });


                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_mapel"]').val();
                    var urlx = '{{ route('store_update_nilaikategori', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //save
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_post_nilaikategori') }}';
                    var formData = new FormData(this);
                    const loader = $('button.post');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowPost').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Save');
                                    $('input[name="kode_post"]').val('');
                                    $('input[name="nama_post"]').val('');
                                    $('input[name="urutan_post"]').val('');

                                    $('select').each(function() {
                                        $(this).val($(this).find("option[selected]")
                                            .val());
                                    });

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });


                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

                window.openModalTrash = function modaltrash() {
                    config_trash = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }

                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: '{{ route('ajax_trash_mapel_simpen') }}',
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'kode',
                                name: 'kode'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'jenis',
                                name: 'jenis',
                            },
                            {
                                data: 'urutan',
                                name: 'urutan',
                            }, {
                                data: 'status_tampil',
                                name: 'status_tampil',
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_trash = $('#table_mapel_trashx').dataTable(config_trash);
                }

            });
        })(jQuery, window);
    </script>
@endsection
