@extends('simpen.template')
@section('simpen.components')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Surat Pengumuman Kelulusan
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->

                    <label><input type="checkbox" class="selectAll" name="checkall" value="1"> Check / Uncheck
                        All</label>

                    <form id="SettingForm" name="SettingForm" method="POST" novalidate="novalidate" class="form-horizontal"
                        enctype="multipart/form-data" action="{{ route('update-form-setting-surat-simpen') }}">
                        @csrf
                        <div class="widget-body clearfix">
                            <div class="table-responsive">
                                <table id="table_setting_surat" class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Inisial</th>
                                            <th>Nama</th>
                                            <th>Jenis</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <input type="hidden" name="jumlah" value="{{ count($rows) }}">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($rows as $form => $list)
                                            <tr>
                                                <input type="hidden" name="id_surat_{{ $no }}"
                                                    value="{{ $list['id'] }}">
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $list['initial'] }}</td>
                                                <td>{{ $list['nama'] }}</td>
                                                <td>{{ $list['jenis'] }} </td>
                                                <td>
                                                    @if ($list['status_tampil'] == 1)
                                                        <i class="fa fa-eye"></i>
                                                    @else
                                                        <i class="fa fa-eye-slash"></i>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="hidden" name="status_tampil_{{ $no }}"
                                                        value="0">
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                            {{ $list['status_tampil'] == 1 ? 'checked' : '' }}
                                                            class="surat_check" name="status_tampil_{{ $no }}"
                                                            data-id="{{ $list['id'] }}" value="1"
                                                            onclick='handleClick(this);'>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                        <div class="form-actions text-center">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 btn-list">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons list-icon">save</i>
                                            Simpan
                                        </button>
                                    </div>
                                    <!-- /.col-sm-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </form>

                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
    <script>
        (function($, global) {
            $(document).ready(function() {
                $('.selectAll').click(function(event) {
                    if (this.checked) {
                        // Iterate each checkbox
                        $(':checkbox').each(function() {
                            this.checked = true;
                        });
                    } else {
                        $(':checkbox').each(function() {
                            this.checked = false;
                        });
                    }
                });

            });
        })(jQuery, window);

        function handleClick(cb) {
            cb.value = cb.checked ? 1 : 0;
            console.log(cb.value);
        }
    </script>
@endsection
