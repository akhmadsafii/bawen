@extends('simpen.template')
@section('simpen.components')
    <style>
        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: red;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }

        #myBtn:hover {
            background-color: #555;
        }

        /* Extra small devices (phones, 600px and down) */
        @media only screen and (max-width: 600px) {

            .printpreview,
            .printwindows {
                display: none;
            }
        }

        @media only screen and (min-width: 600px) {

            .printpreview,
            .printwindows {
                display: none;
            }
        }

        @media only screen and (min-width: 768px) {
            .printpreview {
                display: block;
            }

            .printwindows {
                display: inline-block;
            }
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Beranda</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Halaman Utama</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder" id="sambutan">
                <div class="widget-bg ">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"> <i class="material-icons">perm_identity</i> Sambutan</h5>
                        @if (count($sambutan) > 0)
                            @if (!empty($sambutan['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $sambutan['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $sambutan['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $sambutan['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Sambutan </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <div class="col-md-12 widget-holder" id="panduan">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"> <i class="material-icons">book</i> Panduan</h5>
                        @if (count($panduan) > 0)
                            @if (!empty($panduan['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $panduan['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $panduan['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $panduan['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Panduan </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <div class="col-md-12 widget-holder" id="informasi">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"><i class="material-icons">info</i> Informasi </h5>
                        @if (count($informasi) > 0)
                            @if (!empty($informasi['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $informasi['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $informasi['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $informasi['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Informasi </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                @if (!empty($rows['tgl_buka']))
                    $('#clock').countdown("{{ $rows['tgl_buka'] }}", {
                    elapse: true
                    })
                    .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                    $("#clock").hide();
                    $("#xpengumuman" ).show();
                    //$('#resultx').show();
                    } else {
                    $this.html(event.strftime(
                    'Pengumuman dapat dilihat: <span>%H Jam %M Menit %S Detik</span> lagi'
                    ));
                    $( "#xpengumuman" ).hide();
                    //$('#resultx').hide();
                    }
                    });
                @endif

                //Get the button
                var mybutton = document.getElementById("myBtn");

                // When the user scrolls down 20px from the top of the document, show the button
                window.onscroll = function() {
                    scrollFunction()
                };

                function scrollFunction() {
                    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                        mybutton.style.display = "block";
                    } else {
                        mybutton.style.display = "none";
                    }
                }

                // When the user clicks on the button, scroll to the top of the document
                window.topFunction = function topFunction() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }

                @if (empty(session('role')))
                    var route_logout = "{{ route('logout-simpen') }}";
                    window.location.href = route_logout;
                @endif

            });

        })(jQuery, window);
    </script>
@endsection
