@extends('simpen.template')
@section('simpen.components')
    <style>
        li.xcount {
            display: inline-block;
            font-size: 1.5em;
            list-style-type: none;
            padding: 1em;
            text-transform: uppercase;
        }

        li.xcount span {
            display: block;
            font-size: 3.5rem;
        }

        @media all and (max-width: 768px) {
            h1 {
                font-size: calc(1.5rem * var(--smaller));
            }

            li.xcount {
                font-size: calc(1.125rem * var(--smaller));
            }

            li.xcount span {
                font-size: calc(3.375rem * var(--smaller));
            }
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Beranda</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <!-- /.widget-holder -->
            @php
                if (empty($jadwal_pengumuman)) {
                    $col = '12';
                } else {
                    $col = '4';
                }
            @endphp
            <div class="col-md-{{ $col }} widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title">Informasi Pengaturan</h5>
                        <div class="alert alert-info border-info" role="alert">
                            <p><strong>Sebelum Aplikasi ini digunakan, Silahkan atur terlebih dahulu pengaturan
                                    meliputi:</strong>
                            </p>
                            <ul class="mr-t-10">
                                <li><a href="{{ route('setting-simpen') }}">Template</a>
                                </li>
                                <li><a href="{{ route('setting-surat-simpen') }}">Surat</a>
                                </li>
                                <li><a href="{{ route('setting_kategori_nilai_mapel') }}">Kategori Nilai Pelajaran </a>
                                </li>
                                <li><a href="{{ route('setting-nilai-mapel-simpen') }}">Mata Pelajaran </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            @if (!empty($jadwal_pengumuman))
                <div class="col-md-8 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <center>
                                <h1>Selamat Datang, {{ session('username') ?? '' }}</h1>
                                <p> <i class="material-icons list-icon">event_available</i> Jadwal Pengumuman :
                                    <span class="text-info">
                                        {{ \Carbon\Carbon::parse($jadwal_pengumuman)->isoFormat('dddd, D MMM Y') }}
                                    </span>
                                <div id="xpengumuman">
                                    <h1 class="text-center text-info"><i
                                            class="material-icons list-icon md-48">school</i>Telah Dibuka Untuk Umum </h1>
                                </div>
                                </p>
                            </center>
                            <br>
                            <div id="clock" class="lead text-center"></div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            @endif
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="row">
                            <!-- Counter: Sales -->
                            <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                                <div class="widget-bg bg-primary text-inverse">
                                    <div class="widget-body">
                                        <div class="widget-counter">
                                            <h6>Total Admin</h6>
                                            <h3 class="h1"><span
                                                    class="counter">{{ $count_admin }}</span></h3>
                                            <i class="material-icons list-icon">account_circle</i>
                                        </div>
                                        <!-- /.widget-counter -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <!-- Counter: Subscriptions -->
                            <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                                <div class="widget-bg bg-color-scheme text-inverse">
                                    <div class="widget-body clearfix">
                                        <div class="widget-counter">
                                            <h6>Total Siswa</h6>
                                            <h3 class="h1"><span
                                                    class="counter">{{ $count_siswa }}</span></h3>
                                            <i class="material-icons list-icon">account_box</i>
                                        </div>
                                        <!-- /.widget-counter -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                                <div class="widget-bg bg-color-scheme text-inverse">
                                    <div class="widget-body clearfix">
                                        <div class="widget-counter">
                                            <h6>Total Siswa Lulus</h6>
                                            <h3 class="h1"><span
                                                    class="counter">{{ $count_lulus }}</span></h3>
                                            <i class="material-icons list-icon">account_box</i>
                                        </div>
                                        <!-- /.widget-counter -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                                <div class="widget-bg bg-color-scheme text-inverse">
                                    <div class="widget-body clearfix">
                                        <div class="widget-counter">
                                            <h6>Total Siswa Tidak Lulus</h6>
                                            <h3 class="h1"><span
                                                    class="counter">{{ $count_tidak_lulus }}</span></h3>
                                            <i class="material-icons list-icon">account_box</i>
                                        </div>
                                        <!-- /.widget-counter -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                @if (empty(session('role')))
                    var route_logout = "{{ route('logout-simpen') }}";
                    window.location.href = route_logout;
                @endif

                @if (!empty($jadwal_pengumuman))
                    $('#clock').countdown("{{ $jadwal_pengumuman }}", {
                    elapse: true
                    })
                    .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                    $("#clock").hide();
                    $('#xpengumuman').show();
                    } else {
                    const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;
                    const countDown = new Date("{{ $jadwal_pengumuman }}").getTime();
                    const now = new Date().getTime(),
                    distance = countDown - now;
                    var xtime = `
                    <i class="material-icons list-icon md-48">alarm</i>
                    <ul>
                        <li class="xcount"><span id="days">`+Math.floor(distance / (day))+`</span> Hari</li>
                        <li class="xcount"><span id="hours">%H</span> Jam</li>
                        <li class="xcount"><span id="minutes">%M</span> Menit</li>
                        <li class="xcount"><span id="seconds">%S</span> Detik</li>
                    </ul>
                    <span class="text-info"> Menuju Pembukaan </span>
                    `;
                    $this.html(event.strftime(
                    xtime
                    ));
                    $("#xpengumuman" ).hide();
                    }
                    });
                @endif
            });
        })(jQuery, window);
    </script>
@endsection
