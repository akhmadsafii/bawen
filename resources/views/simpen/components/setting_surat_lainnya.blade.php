@extends('simpen.template')
@section('simpen.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Template Surat Keterangan Lulus
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right d-inline-flex">
            <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
        </div>
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="SettingForm" name="SettingForm" method="POST" novalidate="novalidate" class="form-horizontal"
            enctype="multipart/form-data" action="{{ route('post_setting_surat_kelulusan') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <center>
                                <div class="form-group row col-md-4 ">
                                    <p class="mr-b-0">Kategori Surat <span class="text-red">*</span></p>
                                    <div class="input-group">
                                        <select name="kategori_surat" class="form-control" id="kategori_surat">
                                            <option value="" disabled="disabled"> Pilih Kategori
                                                Surat</option>
                                            <option value="surat_keterangan_lulus" selected="selected"> Surat Keterangan
                                                Kelulusan</option>
                                            <option value="surat_keterangan_alumni"> Surat Keterangan Alumni</option>
                                            <option value="surat_keterangan_lainnya"> Surat Keterangan Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                            </center>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p class="mr-b-0">Header 1 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head1" type="text" name="head1"
                                                value="{{ old('head1') ?? ($rows['header1'] ?? '') }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <p class="mr-b-0">Header 2 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head2" type="text" name="head2"
                                                value="{{ old('head2') ?? ($rows['header2'] ?? '') }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*">
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <p class="mr-b-0">Header 3 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head3" type="text" name="head3"
                                                value="{{ old('head3') ?? ($rows['header3'] ?? '') }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*">
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <p class="mr-b-0">Header 4 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head3" type="text" name="head4"
                                                value="{{ old('head4') ?? ($rows['header4'] ?? '') }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Alamat <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" class="form-control"
                                                rows="9">{{ old('alamat') ?? ($rows['alamat'] ?? '') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p class="mr-b-0">Nama Kepsek <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nama_kepsek" type="text" name="nama_kepsek"
                                                value="{{ old('nama_kepsek') ?? ($rows['nama_kepsek'] ?? '') }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="mr-b-0">NIP Kepsek <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nip_kepsek" type="text" name="nip_kepsek"
                                                value="{{ old('nip_kepsek') ?? ($rows['nip_kepsek'] ?? '') }}"
                                                class="form-control" title="">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <p class="mr-b-0">Tingkat <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <select name="tingkat" class="form-control" id="tingkat">
                                                <option value="" disabled="disabled" selected="selected"> Pilih Jenjang
                                                    Tingkat Sekolah</option>
                                                <option value="sd"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'sd')  selected="selected" @endif
                                                    @endif>SD</option>
                                                <option value="smp"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'smp')  selected="selected" @endif
                                                    @endif>SMP</option>
                                                <option value="sma"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'sma')  selected="selected" @endif
                                                    @endif>SMA</option>
                                                <option value="smk"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'smk')  selected="selected" @endif
                                                    @endif>SMK</option>
                                                <option value="ma"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'ma')  selected="selected" @endif
                                                    @endif>MA</option>
                                                <option value="mak"
                                                    @if (!empty($rows['tingkat'])) @if ($rows['tingkat'] == 'sd')  selected="selected" @endif
                                                    @endif>MAK</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <p class="mr-b-0">Judul <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="judul" type="text" name="judul"
                                                value="{{ old('judul') ?? ($rows['judul'] ?? 'Surat Keterangan Lulus') }}"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="mr-b-0">Tanggal Surat <span class="text-red">*</span>
                                        </p>
                                        <div class="input-group">
                                            <input id="tgl_surat" type="text" name="tgl_surat"
                                                value="{{ old('tgl_surat') ?? ($rows['tgl_keputusan'] ?? \Carbon\Carbon::now()->format('Y-m-d')) }}"
                                                class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'
                                                title="Format harus berisi tanggal " readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="mr-b-0">Tempat Pembuatan Surat <span
                                                class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="tempat_pembuatan" type="text" name="tempat_pembuatan"
                                                value="{{ old('tempat_pembuatan') ?? ($rows['tempat_keputusan'] ?? '') }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <p class="mr-b-0">Nomor Surat <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nomor_surat" type="text" name="nomor_surat"
                                                value="{{ old('nomor_surat') ?? ($rows['nomor_surat'] ?? '') }}"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p class="mr-b-0">Prolog <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="prolog" id="prolog" rows="3"
                                                class="form-control editor ">{{ old('prolog') ?? ($rows['prolog'] ?? '') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p class="mr-b-0">Isi <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="isi" id="isi" rows="3"
                                                class="form-control editor">{{ old('isi') ?? ($rows['isi'] ?? '') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p class="mr-b-0">Penutup <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="penutup" id="penutup" rows="3"
                                                class="form-control editor">{{ old('penutup') ?? ($rows['penutup'] ?? '') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['logo1'])
                                            <p class="mr-b-0">Logo 1</p>
                                        @else
                                            <p class="mr-b-0">Logo 1 <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="logo1" type="file" name="logo1" accept="image/*"
                                                onchange="readURL(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['logo1'])
                                            <img id="modal-preview" src="{{ $rows['logo1'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['logo2'])
                                            <p class="mr-b-0">Logo 2</p>
                                        @else
                                            <p class="mr-b-0">Logo 2 <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="logo2" type="file" name="logo2" accept="image/*"
                                                onchange="readURL2(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['logo2'])
                                            <img id="modal-preview2" src="{{ $rows['logo2'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview2" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['stempel'])
                                            <p class="mr-b-0">Stempel</p>
                                        @else
                                            <p class="mr-b-0">Stempel <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="stempel" type="file" name="stempel" accept="image/*"
                                                onchange="readURL3(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['stempel'])
                                            <img id="modal-preview3" src="{{ $rows['stempel'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview3" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['stempel'])
                                            <p class="mr-b-0">TTD Kepsek</p>
                                        @else
                                            <p class="mr-b-0">TTD Kepsek <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="ttd_kepsek" type="file" name="ttd_kepsek" accept="image/*"
                                                onchange="readURL4(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['ttd_kepsek'])
                                            <img id="modal-preview4" src="{{ $rows['ttd_kepsek'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview4" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions text-center">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>

    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL2 = function name(input, id) {
                    id = id || '#modal-preview2';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview2').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL3 = function name(input, id) {
                    id = id || '#modal-preview3';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview3').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL4 = function name(input, id) {
                    id = id || '#modal-preview4';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview4').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                $('body').on('change', 'select[name="kategori_surat"]', function() {
                    var selectx = $(this).val();
                    var urlx;
                    if (selectx == 'surat_keterangan_lulus') {
                        urlx = "{{ route('setting_template_other_simpen') }}";
                    } else if (selectx == 'surat_keterangan_alumni') {
                        urlx = "{{ route('setting_otheralumni_simpen') }}";
                    } else if (selectx == 'surat_keterangan_lainnya') {
                        urlx = "{{ route('setting_otherskhun_simpen') }}";
                    }
                    window.location.href = urlx;
                });

            });

        })(jQuery, window);
    </script>
@endsection
