@extends('simpen.template')
@section('simpen.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px) {

            /* Force table to not be like tables anymore */
            table,
            thead,
            tbody,
            th,
            td,
            tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr {
                border: 1px solid #ccc;
            }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Nilai Siswa </h5>
        </div>
        <!-- /.page-title-left -->
        {{-- <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Nilai Siswa
                </li>
            </ol>
        </div> --}}
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tempat Sampah </h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="table_mapel_trashx" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Initial</th>
                                    <th>Nilai</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Nilai Siswa </h5>
                </div>
                <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_mapel" class="form-control">
                    <input type="hidden" name="id_siswa" class="form-control">
                    <input type="hidden" name="id_nilai_mapel" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Initial</td>
                                    <td class="text-muted">
                                        <input type="text" name="initial_edit" id="initial_edit" autocomplete="off"
                                            class="form-control" placeholder="Initial" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mata Pelajaran</td>
                                    <td class="text-muted">
                                        <input type="text" name="mata_pelajaran_edit" id="mata_pelajaran_edit"
                                            autocomplete="off" class="form-control" placeholder="Initial" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nilai</td>
                                    <td class="text-muted">
                                        <input type="text" name="nilai_edit" id="nilai_edit" autocomplete="off"
                                            class="form-control" placeholder="Nilai">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowSiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Data Siswa </h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="table_siswa" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Photo</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>NISN</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-10 widget-holder ml-md-auto login-center mx-auto" id="inputNilaiForm" style="display:none;">
                <div class="widget-bg">
                    <form name="inputNilai" id="inputNilai" enctype="multipart/form-data" id="recommendationDiv">
                        @csrf
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Input Nilai Siswa </h5>
                            <hr>
                            <div class="table-responsive">
                                <table class="table mb-0 table-condensed cf">
                                    <tbody>
                                        <tr>
                                            <td><label class="col-md-3 col-form-label" for="l3">Siswa</label> </td>
                                            <td class="text-muted">
                                                <div class="input-group">
                                                    <input type="text" name="siswa_edit" id="siswa_edit" autocomplete="off"
                                                        class="form-control search" placeholder="Siswa" readonly>
                                                    <input type="hidden" name="siswa_id" id="siswa_id" autocomplete="off"
                                                        class="form-control">
                                                    <span class="input-group-addon search"><i
                                                            class="material-icons list-icon" id="search">search</i></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table id="table_mapelx" width="100%" class="table table-striped table-responsive">
                                    <thead>
                                        <tr class="table-info">
                                            <th>Mata Pelajaran <span class="text-red">*</span></th>
                                            <th>initial</th>
                                            <th>Kategori</th>
                                            <th>Nilai</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="clonable form-clone" parentId="1">
                                            <td>
                                                <select class="form-control akun_mapel_post" id="akun_mapel_post"
                                                    name="akun_mapel_post[]" required="">
                                                    <option disabled="disabled" selected="true" value="">Pilih Mata
                                                        Pelajaran
                                                    </option>
                                                    @foreach ($mapel as $mapel)
                                                        <option value="{{ $mapel['id'] }}">
                                                            {{ $mapel['nama'] }} - {{ $mapel['kategori'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td><input name="initial_post[]" type="text" id="initial_post"
                                                    class="form-control" placeholder="Initial" readonly>
                                            </td>
                                            <td><input name="kategori_post[]" type="text" id="kategori_post"
                                                    class="form-control" placeholder="Kategori" readonly>
                                            </td>
                                            <td><input name="nilai_post[]" type="text" id="nilai_post"
                                                    class="form-control" placeholder="Nilai" required="required">
                                            </td>
                                            <td width="10%" class="btn-group text-center" role="group" width="100%">
                                                <button type="button" name="remove" id="removex"
                                                    class="btn btn-danger remove-clone remove"><i
                                                        class="fa fa-minus"></i></button> &nbsp;
                                                <button type="button" name="add" id="addx"
                                                    class="btn btn-info add-clone add"><i
                                                        class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info upload text-center">
                                <i class="fa fa-save list-icon"></i>
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.widget-bg -->
            </div>

            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->

                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_mapel" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Siswa</th>
                                        <th>Initial</th>
                                        <th>Mata Pelajaran </th>
                                        <th>Nilai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                var groupColumn = 1;

                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#inputNilaiForm').show();
                            }
                        },
                        {
                            text: '<i class="fa fa-trash list-icon"></i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowTrash').modal('show');
                                openModalTrash();
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_nilai_siswa_simpen') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'siswa',
                            name: 'siswa'
                        },
                        {
                            data: 'initial',
                            name: 'initial'
                        },
                        {
                            data: 'mapel',
                            name: 'mapel'
                        },
                        {
                            data: 'nilai',
                            name: 'nilai',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                    columnDefs: [{
                        "visible": false,
                        "targets": groupColumn
                    }],
                    drawCallback: function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;

                        api.column(groupColumn, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td colspan="5">' + group +
                                    '</td></tr>'
                                );

                                last = group;
                            }
                        });
                    }
                };

                table_adminx = $('#table_mapel').dataTable(config);
                // Order by the grouping
                $('#table_mapel tbody').on('click', 'tr.group', function() {
                    var currentOrder = table_adminx.api().order()[0];
                    if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
                        table_adminx.api().order([groupColumn, 'desc']).draw();
                    } else {
                        table_adminx.api().order([groupColumn, 'asc']).draw();
                    }
                });

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('input[type="number"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });

                $('body').on('click', '.edit.nilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_nilai_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-pencil"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_mapel"]').val(rows.id);
                                    $('input[name="id_nilai_mapel"]').val(rows
                                        .id_nilai_mapel);
                                    $('input[name="id_siswa"]').val(rows.id_siswa);
                                    $('input[name="initial_edit"]').val(rows.initial);
                                    $('input[name="nilai_edit"]').val(rows.nilai);
                                    $('input[name="mata_pelajaran_edit"]').val(rows.mapel);

                                } else {
                                    $(loader).html('<i class="fa fa-pencil"></i>');
                                }
                            }

                        }
                    });
                });


                //update
                $('body').on('submit', 'Form#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_mapel"]').val();
                    var urlx = '{{ route('update_nilai_siswa_simpen', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove
                $('body').on('click', '.remove.nilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy_master_nilai_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //restore
                $('body').on('click', '.restore.nilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore_master_nilai_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Kembalikan Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restore(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //remove permanet
                $('body').on('click', '.remove-permanent.nilai', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove_permanent_master_nilai_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin Menghapus data ini secara permanent!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove_permanent(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //pilih siswa
                $('body').on('click', '.search', function() {
                    $('#modalshowSiswa').modal('show');
                    var configx = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('ajax_data_siswa_pilih_simpen') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'nis',
                                name: 'nis'
                            },
                            {
                                data: 'nisn',
                                name: 'nisn'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    var table_adminxx = $('#table_siswa').dataTable(configx);
                });

                $('body').on('click', '.pilih.siswa', function() {
                    var id = $(this).data('id');
                    var nama = $(this).data('nama');
                    var url = '{{ route('getnilai_siswa_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    var loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            loader.html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    /*if (rows.length > 0) {
                                        loader.html('<i class="fa fa-plus"></i>');
                                        window.notif('warning', 'Input Nilai Siswa ' +
                                            nama + ' sudah dimasukan sebelumnya!');
                                    } else {
                                        $('input[name="siswa_id"]').val(id);
                                        $('input[name="siswa_edit"]').val(nama);
                                        $('#modalshowSiswa').modal('hide');
                                        loader.html('<i class="fa fa-plus"></i>');
                                    }*/
                                    $('input[name="siswa_id"]').val(id);
                                    $('input[name="siswa_edit"]').val(nama);
                                    $('#modalshowSiswa').modal('hide');
                                    loader.html('<i class="fa fa-plus"></i>');
                                }
                            }

                        }
                    });
                });

                //update
                $('body').on('submit', 'Form#inputNilai', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_nilai_siswa_simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $('#inputNilaiForm').hide();

                                    table_adminx.fnDraw(false);

                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function remove
                window.remove_permanent = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                $('body').on('change', 'select.akun_mapel_post', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    var id = $(this).val();
                    var url = "{{ route('show_master_mapel_simpen', ':id') }}";
                    url = url.replace(':id', id);
                    if (id) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="initial_post[]"]').val(rows
                                            .nama);
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="kategori_post[]"]').val(rows
                                            .kategori);
                                    } else if (result['info'] == 'error') {
                                        window.notif('error', result['message']);
                                    }
                                }
                            }
                        });

                    }

                });

                //function restore
                window.restore = function remove(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_trash.fnDraw(false);
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                window.openModalTrash = function modaltrash() {
                    config_trash = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('ajax_trash_nilai_simpen') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'initial',
                                name: 'initial'
                            },
                            {
                                data: 'nilai',
                                name: 'nilai',
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_trash = $('#table_mapel_trashx').dataTable(config_trash);
                }

            });
        })(jQuery, window);
    </script>
@endsection
