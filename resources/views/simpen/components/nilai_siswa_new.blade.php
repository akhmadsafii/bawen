@extends('simpen.template')
@section('simpen.components')
    <style>
        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #fff;
            font-size: 13px;
        }

        .pagination>li {
            display: inline;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            /* margin: 20px 0; */
            border-radius: 4px;
            /* float: right; */
        }

        .pagination>li>a,
        .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

    </style>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">{{ Session::get('title') }}</h3>
                        </div>
                        <div class="col-md-6">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="inputPassword6">Tahun</label>
                                    <select name="tahun" id="tahun" class="form-control mx-sm-3">
                                        <option value="">Pilih Tahun..</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ substr($th['tahun_ajaran'], 0, 4) }}"
                                                {{ $_GET['tahun'] == substr($th['tahun_ajaran'], 0, 4) ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>

                    <hr>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <div class="row justify-content-end">
                                <div class="col-md-3">
                                    <form class="navbar-form pull-right mb-3" role="search" action="javascript:void(0)"
                                        onsubmit="filter('{{ $routes }}')">
                                        <div class="input-group">
                                            @php
                                                $serc = str_replace('-', ' ', $search);
                                            @endphp
                                            <input type="text" value="{{ $serc }}" id="search" name="search"
                                                class="form-control" placeholder="Search">
                                            <div class="input-group-btn">
                                                <button type="submit" id="fil" class="btn btn-info"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <table id="table_mapel" class="table table-bordered table-responsive">
                                <thead class="bg-info">
                                    <tr>
                                        <th>No</th>
                                        <th>Profil</th>
                                        <th>Nomor Peserta</th>
                                        <th>Nomor Ujian</th>
                                        <th>Telepon</th>
                                        <th>Keputusan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($siswa))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $sw)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td>
                                                    <b>{{ $sw['nama'] ?? '-' }}</b>
                                                    <p class="m-0">Username : {{ $sw['username'] ?? '-' }}</p>
                                                </td>
                                                <td class="vertical-middle">{{ $sw['nomor_peserta'] ?? '-' }}</td>
                                                <td class="vertical-middle">{{ $sw['nomor_ujian'] ?? '-' }}</td>
                                                <td class="vertical-middle">{{ $sw['telepon'] ?? '-' }}</td>
                                                <td class="vertical-middle">{{ $sw['keputusan'] ?? '-' }}</td>
                                                <td class="vertical-middle">
                                                    <a href="javaript:void(0)" data-toggle="collapse"
                                                        data-target="#nilai{{ $sw['id'] }}" class="btn btn-info">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-success tambah"
                                                        data-id="{{ $sw['id'] }}">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="nilai{{ $sw['id'] }}">
                                                        <center>
                                                            <h3 class="box-title mx-4 my-1">Daftar Nilai dari
                                                                {{ $sw['nama'] }}</h3>
                                                            <table class="w-50 mt-1 mb-3 mx-3">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">No</th>
                                                                        <th>Mapel</th>
                                                                        <th>Initial</th>
                                                                        <th class="text-center">Nilai</th>
                                                                        <th class="text-center">Opsi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="dataNilai{{ $sw['id'] }}">
                                                                    @if (!empty($sw['nilai']))
                                                                        @php
                                                                            $nomer = 1;
                                                                        @endphp
                                                                        @foreach ($sw['nilai'] as $nilai)
                                                                            <tr>
                                                                                <td class="text-center">
                                                                                    {{ $nomer++ }}</td>
                                                                                <td>{{ $nilai['mapel'] }}</td>
                                                                                <td>{{ $nilai['initial'] }}</td>
                                                                                <td class="text-center">
                                                                                    {{ $nilai['nilai'] }}</td>
                                                                                <td class="text-center">
                                                                                    <a href="javascript:void(0)"
                                                                                        class="btn btn-info btn-sm edit"
                                                                                        data-id="{{ $nilai['id'] }}">
                                                                                        <i class="fa fa-pencil"
                                                                                            aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:void(0)"
                                                                                        class="btn btn-danger btn-sm delete"
                                                                                        data-id="{{ $nilai['id'] }}">
                                                                                        <i class="fa fa-trash"
                                                                                            aria-hidden="true"></i>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr>
                                                                            <td colspan="5" class="text-center">Data saat
                                                                                ini
                                                                                tidak tersedia</td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </center>

                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">Data saat ini belum tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="float-right">
                                {!! $pagination !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="nilaiModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="nilaiForm">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12 lis-data">
                                <input type="hidden" name="id" id="id_nilai">
                                <input type="hidden" name="id_siswa" id="id_siswa">
                                <div class="input-mapel">
                                    <div class="row mt-2 list-mapel">
                                        <div class="col-md-8">
                                            <div class="form-group mb-1">
                                                <label for="name" class="col-sm-12 control-label">Mata Pelajaran</label>
                                                <div class="col-sm-12">
                                                    <select name="mapel[]" id="id_mapel" class="form-control">
                                                        <option value="">Pilih Mapel..</option>
                                                        @foreach ($mapel as $mapel)
                                                            <option value="{{ $mapel['id'] }}">
                                                                {{ $mapel['nama'] }} - {{ $mapel['kategori'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-1">
                                                <label for="name" class="col-sm-12 control-label">Nilai</label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="nilai" name="nilai[]"
                                                        autocomplete="off" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="fomAddMapel">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" class="btn btn-success btn-sm addData"><i
                                                class="fa fa-plus"></i> tambah
                                            baris</a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm deleteData"><i
                                                class="fa fa-minus"></i> Hapus Baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script> --}}
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}

    <script>
        (function($, global) {

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                /*$('body').on('click', '.tambah', function() {
                    alert('tambah');
                });*/

                $('body').on('click', '.addData', function() {
                    $('.addData').closest('.lis-data').find('.input-mapel').first().clone().appendTo(
                        '.fomAddMapel');
                });

                $('body').on('click', '.deleteData', function() {
                    $('.deleteData').closest('.lis-data').find('.input-mapel').not(':first').last()
                        .remove();
                });

                $("#tahun").change(function() {
                    var tahun = $(this).val();
                    if (tahun) {
                        window.location.href = "nilai_siswa?tahun=" + tahun;
                    }
                });

                $('body').on('click', '.delete', function() {
                    var id = $(this).data('id');
                    let loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        $.ajax({
                            url: "{{ route('destroy_master_nilai_simpen') }}",
                            type: "POST",
                            data: {
                                id
                            },
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i>');
                            },
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    window.location.reload();
                                } else {
                                    $(loader).html(
                                        '<i class="fa fa-trash"></i>');
                                }
                                swa(data.status.toUpperCase() + "!", data.message,
                                    data
                                    .info);
                            }
                        })
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    })
                });

                $('body').on('click', '.edit', function() {
                    console.log('edit data');
                    var id = $(this).data('id');
                    let loader = $(this);
                    $('#form_result').html('');
                    $('.fomAddMapel').html('');
                    $('.tambahBaris').hide('');
                    var url = "{{ route('show_master_nilai_simpen', ':id') }}";
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(res) {
                            console.log(res);
                            $(loader).html('<i class="fa fa-pencil"></i>');
                            $('#modelHeading').html("Edit Nilai");
                            $('#id_nilai').val(res.data.id);
                            $('#id_siswa').val(res.data.id_siswa);
                            $('#id_mapel').val(res.data.id_nilai_mapel);
                            $('#nilai').val(res.data.nilai);
                            $('#action').val('Edit');
                            $('#nilaiModal').modal('show');
                        }
                    });
                });

                $(document).on('click', '.tambah', function() {
                    var id = $(this).data('id');
                    let loader = $(this);
                    $('#nilaiForm').trigger("reset");
                    $('#id_siswa').val(id);
                    $('.fomAddMapel').html('');
                    $('.tambahBaris').show('');
                    $('#modelHeading').html("Tambah Nilai");
                    $('#nilaiModal').modal('show');
                    $('#action').val('Add');
                });

                $('#nilaiForm').on('submit', function(event) {
                    event.preventDefault();
                    $("#saveBtn").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#saveBtn").attr("disabled", true);
                    var action_url = '';

                    if ($('#action').val() == 'Add') {
                        action_url = "{{ route('store_nilai_siswa_simpen') }}";
                        method_url = "POST";
                    }

                    if ($('#action').val() == 'Edit') {
                        action_url = "{{ route('update_nilai_siswa_simpen') }}";
                        method_url = "PUT";
                    }
                    $.ajax({
                        url: action_url,
                        method: method_url,
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                window.location.reload();
                            } else {
                                $('#saveBtn').html('Simpan');
                                $("#saveBtn").attr("disabled", false);
                            }
                            swa(data.status.toUpperCase() + "!", data.message, data.icon);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                    });
                });

            });

            function swa(status, message, icon) {
                swal(
                    status,
                    message,
                    icon
                );
                return true;
            }

            function filter(routes) {
                var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value :
                    "";
                var search = searchs.replace(/ /g, '-')
                var tahun = $('#tahun').val();
                var url_asli = window.location.href;
                var explode_url = url_asli.split("?")[0];
                var url = explode_url + "?search=" + search + "&tahun=" + tahun;
                document.location = url;
            }

        })(jQuery, window);
    </script>

@endsection
