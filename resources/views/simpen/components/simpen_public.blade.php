@extends('simpen.template')
@section('simpen.components')
    <style>
        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: red;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }

        #myBtn:hover {
            background-color: #555;
        }

        li.xcount {
            display: inline-block;
            font-size: 1.5em;
            list-style-type: none;
            padding: 1em;
            text-transform: uppercase;
        }

        li.xcount span {
            display: block;
            font-size: 4.5rem;
        }

        @media all and (max-width: 768px) {
            h1 {
                font-size: calc(1.5rem * var(--smaller));
            }

            li.xcount {
                font-size: calc(1.125rem * var(--smaller));
            }

            li.xcount span {
                font-size: calc(3.375rem * var(--smaller));
            }
        }

    </style>
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <div class="widget-list">
        <div class="row">
            <!-- /.widget-holder -->
            @if ($message = Session::get('message'))
                <div class="col-md-12 widget-holder alert-dismissible">
                    <div class="widget-bg">
                        <!-- /.widget-heading -->
                        <div class="widget-body clearfix">
                            @if ($message['status'] == 'gagal')
                                <div class="alert alert-danger border-danger alert-dismissible p-0" role="alert">
                                    <div class="widget-list m-2">
                                        <div class="col-md-12 widget-holder m-0">
                                            <div class="widget-body clearfix text-center">
                                                <i class="material-icons list-icon">warning</i>
                                                <strong>{{ $message['message'] }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if ($message['status'] == 'success')
                                <div class="alert alert-success border-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <div class="widget-list">
                                        <div class="col-md-12 widget-holder">
                                            <div class="widget-body clearfix text-center">
                                                <i class="material-icons list-icon">check_circle</i>
                                                <strong>{{ $message['message'] }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            @endif

            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header text-center box-title">
                        {{ strtoupper(session('footer')) ?? strtoupper(session('sekolah')) }}
                    </div>
                    <div class="widget-body clearfix">
                        @if (!empty($rows['tgl_buka']))
                            <h5 class="box-title">
                                <h4 class="text-center">Pengumuman Kelulusan Tahun Ajaran {{ $rows['tahun_ajaran'] ?? '-' }} </h4>
                            </h5>
                            <div id="clock" class="lead text-center"></div>
                            <div id="xpengumuman">
                                <p class="text-center text-info">Telah dibuka , untuk melihat pengumuman status kelulusan
                                    silahkan login terlebih dahulu </p>
                            </div>
                        @else
                            <p class="text-red text-center">Silahkan Lakukan Pengaturan template website terlebih dahulu!.
                            </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <div class="col-md-8 widget-holder" id="sambutan">
                <div class="widget-bg ">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"> <i class="material-icons">perm_identity</i> Sambutan</h5>
                        @if (count($sambutan) > 0)
                            @if (!empty($sambutan['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $sambutan['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $sambutan['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $sambutan['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Sambutan </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->

            <div class="col-md-4 widget-holder" id="login">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <p class="text-center"> Silahkan login dengan username dan password yang telah anda miliki.
                        </p>
                        <form class="mr-t-30" action="{{ route('verify_login_siswa') }}" method="post">
                            @csrf
                            <input type="hidden" name="role" id="role" class="form-control" value="siswa-simpen">
                            <!-- /.form-group -->
                            <div class="form-group row">
                                <label for="sample3UserName" class="text-sm-right col-sm-3 col-form-label">
                                    Username</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="material-icons">person</i>
                                        </div>
                                        <input type="text" class="form-control" id="sample3UserName"
                                            placeholder="Username" name="username" required="">
                                    </div>
                                    <!-- /.input-group -->
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group row input-has-value">
                                <label for="sample3Password" class="text-sm-right col-sm-3 col-form-label">Password</label>
                                <div class="col-sm-9">
                                    <div class="input-group input-has-value">
                                        <div class="input-group-addon"><i class="material-icons">lock</i>
                                        </div>
                                        <input type="password" class="form-control password" id="sample3Password"
                                            placeholder="Password" name="password" required>
                                        <span class="input-group-addon">
                                            <a href="javascript:void(0)"><i
                                                    class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                        </span>
                                    </div>
                                    <!-- /.input-group -->
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /.form-group -->

                            <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-sm-9 ml-auto btn-list">
                                        <button type="submit" class="btn btn-primary">Login</button>
                                        <a href="javascript:void(0)" onclick="lupaPassword()"
                                            class="pull-right text-info">Lupa Password</a>
                                    </div>
                                    <!-- /.col-sm-12 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.form-actions -->
                        </form>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->

            <div class="col-md-12 widget-holder" id="panduan">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"> <i class="material-icons">book</i> Panduan</h5>
                        @if (count($panduan) > 0)
                            @if (!empty($panduan['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $panduan['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $panduan['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $panduan['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Panduan </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->

            <div class="col-md-12 widget-holder" id="informasi">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title text-center"><i class="material-icons">info</i> Informasi </h5>
                        @if (count($informasi) > 0)
                            @if (!empty($informasi['file']))
                                <section class="row">
                                    <div class="col-md-4 text-center mr-b-20 ">
                                        <img src="{{ $informasi['file'] }}" alt="" class="rounded-circle img-thumbnail"
                                            style="width: 250px;height:250px;">
                                    </div>
                                    <div class="col-md-8">
                                        {!! $informasi['isi'] ?? '-' !!}
                                    </div>
                                </section>
                            @else
                                {!! $informasi['isi'] ?? '-' !!}
                            @endif
                        @else
                            <p class="text-red"> Belum ada Informasi </p>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {
                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });


                @if (!empty($rows['tgl_buka']))
                    $('#clock').countdown("{{ $rows['tgl_buka'] }}", {
                    elapse: true
                    })
                    .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                    $("#clock").hide();
                    $("#xpengumuman").show();
                    $('#login').show();
                    $('#loginx').show();
                    document.getElementById("sambutan").classList.remove('col-md-12');
                    document.getElementById("sambutan").classList.add('col-md-8');
                    } else {
                    const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;
                    const countDown = new Date("{{ $rows['tgl_buka'] }}").getTime();
                    const now = new Date().getTime(),
                    distance = countDown - now;
                    var xtime =`
                    <div class="col-10 ml-sm-auto col-sm-6 col-md-6 ml-md-auto login-center mx-auto">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h6 class="card-subtitle">Pengumuman Akan Dimulai Pada :</h6>
                            </div>
                            <div class="card-body">
                                <div id="countdown">
                                    <ul>
                                        <li class="xcount"><span id="days">`+Math.floor(distance / (day))+`</span> Hari</li>
                                        <li class="xcount"><span id="hours">%H</span> Jam</li>
                                        <li class="xcount"><span id="minutes">%M</span> Menit</li>
                                        <li class="xcount"><span id="seconds">%S</span> Detik</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    $this.html(event.strftime(
                    xtime
                    ));
                    $("#xpengumuman" ).hide();
                    $('#login').hide();
                    $('#loginx').hide();
                    document.getElementById("sambutan").classList.remove('col-md-8');
                    document.getElementById("sambutan").classList.add('col-md-12');
                    }
                    });
                @endif

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }


                //Get the button
                var mybutton = document.getElementById("myBtn");

                // When the user scrolls down 20px from the top of the document, show the button
                window.onscroll = function() {
                    scrollFunction()
                };

                function scrollFunction() {
                    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                        mybutton.style.display = "block";
                    } else {
                        mybutton.style.display = "none";
                    }
                }

                // When the user clicks on the button, scroll to the top of the document
                window.topFunction = function topFunction() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }

                window.lupaPassword = function lupaPassword() {
                    var role = $('#role').val();
                    if (role == 'admin-simpen') {
                        window.location.href = "{{ url('auth/reset_password') }}";
                    } else {
                        alert('harap hubungi admin untuk mereset password')
                    }
                }
            });

        })(jQuery, window);
    </script>
@endsection
