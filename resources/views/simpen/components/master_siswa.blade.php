@extends('simpen.template')
@section('simpen.components')
    <style>
        td.details-control {
            cursor: pointer;
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_open.png') no-repeat center center;
        }

        tr.shown td.details-control {
            background: url('https://datatables.net/dev/accessibility/DataTables_1_10/examples/resources/details_close.png') no-repeat center center;
        }

        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

        /* Set a fixed scrollable wrapper */
        .tableWrap {
            height: 200px;
            border: 2px solid black;
            overflow: auto;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Siswa </h5>
        </div>
        <!-- /.page-title-left -->
        {{-- <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Siswa
                </li>
            </ol>
        </div> --}}
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPost" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Siswa Informasi </h5>
                </div>
                <form id="PFormxNew" name="PFormxNew" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>NIS</td>
                                    <td class="text-muted">
                                        <input type="text" name="nis_post" id="nis_post" autocomplete="off"
                                            class="form-control" placeholder="NIS">
                                    </td>
                                    <td>NISN</td>
                                    <td class="text-muted">
                                        <input type="text" name="nisn_post" id="nisn_post" autocomplete="off"
                                            class="form-control" placeholder="NISN">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir</td>
                                    <td class="text-muted">
                                        <input type="text" name="tempat_lahir_post" id="tempat_lahir_post"
                                            autocomplete="off" class="form-control" placeholder="Tempat Lahir ">
                                    </td>
                                    <td>Tanggal Lahir</td>
                                    <td class="text-muted">
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" name="tgl_lahir_post"
                                                id="tgl_lahir_post" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'
                                                value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_post" id="nama_post" autocomplete="off"
                                            class="form-control" placeholder="Nama">
                                    </td>
                                    <td>NIK</td>
                                    <td class="text-muted">
                                        <input type="text" name="nik_post" id="nik_post" autocomplete="off"
                                            class="form-control" placeholder="NIK">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tahun Ajaran</td>
                                    <td class="text-muted">
                                        <input type="text" name="tahun_ajaran_post" id="tahun_ajaran_post"
                                            autocomplete="off" class="form-control" placeholder="Tahun Ajaran">
                                    </td>
                                    <td>Kurikulum </td>
                                    <td class="text-muted">
                                        <input type="text" name="kurikulum_post" id="kurikulum_post" autocomplete="off"
                                            class="form-control" placeholder="Kurikulum">
                                    </td>
                                </tr>
                                <tr>
                                    <td>NPSN</td>
                                    <td class="text-muted">
                                        <input type="text" name="npsn_post" id="npsn_post" autocomplete="off"
                                            class="form-control" placeholder="NPSN">
                                    </td>
                                    <td>Nomor Ujian</td>
                                    <td class="text-muted">
                                        <input type="text" name="nomor_ujian_post" id="nomor_ujian_post" autocomplete="off"
                                            class="form-control" placeholder="Nomor Ujian">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Orang Tua </td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_orang_tua_post" id="nama_orang_tua_post"
                                            autocomplete="off" class="form-control" placeholder="Nama Orang Tua">
                                    </td>
                                    <td>Nomor Peserta</td>
                                    <td class="text-muted">
                                        <input type="text" name="nomor_peserta_post" id="nomor_peserta_post"
                                            autocomplete="off" class="form-control" placeholder="Nomor Ujian">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telepon </td>
                                    <td class="text-muted">
                                        <input type="text" name="telepon_post" id="nama_orang_tua_post" autocomplete="off"
                                            class="form-control" placeholder="Telepon">
                                    </td>
                                    <td>Keputusan</td>
                                    <td class="text-muted ">
                                        <select class="form-control" name="keputusan_post" id="keputusan_post">
                                            <option value="" disabled="disabled" selected="selected">Pilih Keputusan
                                            </option>
                                            <option value="lulus">Lulus</option>
                                            <option value="tidak_lulus">Tidak Lulus</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kelas</td>
                                    <td class="text-muted">
                                        <input type="text" name="kelas_post" id="kelas_post" autocomplete="off"
                                            class="form-control" placeholder="Jurusan">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jurusan</td>
                                    <td class="text-muted">
                                        <input type="text" name="jurusan_post" id="jurusan_post" autocomplete="off"
                                            class="form-control" placeholder="Jurusan">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td class="text-muted" colspan="4">
                                        <div class="col-sm-12">
                                            <div class="form-group row mb-0">
                                                <label class="col-md-3 col-form-label" for="l1"></label>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img id="modal-previewpost"
                                                                src="https://via.placeholder.com/150" alt="Preview"
                                                                class="form-group mb-1" width="100px" height="100px"
                                                                style="margin-top: 10px">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input id="image_post" type="file" name="image_post"
                                                                            accept="image/*" onchange="readURL2(this);">
                                                                        <input type="hidden" name="hidden_image"
                                                                            id="hidden_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save ">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Siswa Informasi </h5>
                </div>
                <form id="PFormxNewUpdate" name="PFormxNewUpdate" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_siswa" id="id_siswa" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>NIS</td>
                                    <td class="text-muted">
                                        <input type="text" name="nis_edit" id="nis_edit" autocomplete="off"
                                            class="form-control" placeholder="NIS">
                                    </td>
                                    <td>NISN</td>
                                    <td class="text-muted">
                                        <input type="text" name="nisn_edit" id="nisn_edit" autocomplete="off"
                                            class="form-control" placeholder="NISN">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir</td>
                                    <td class="text-muted">
                                        <input type="text" name="tempat_lahir_edit" id="tempat_lahir_edit"
                                            autocomplete="off" class="form-control" placeholder="Tempat Lahir ">
                                    </td>
                                    <td>Tanggal Lahir</td>
                                    <td class="text-muted">
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" name="tgl_lahir_edit"
                                                id="tgl_lahir_edit" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'
                                                value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" readonly="readonly">
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_edit" id="nama_edit" autocomplete="off"
                                            class="form-control" placeholder="Nama">
                                    </td>
                                    <td>NIK</td>
                                    <td class="text-muted">
                                        <input type="text" name="nik_edit" id="nik_edit" autocomplete="off"
                                            class="form-control" placeholder="NIK">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tahun Ajaran</td>
                                    <td class="text-muted">
                                        <input type="text" name="tahun_ajaran_edit" id="tahun_ajaran_edit"
                                            autocomplete="off" class="form-control" placeholder="Tahun Ajaran">
                                    </td>
                                    <td>Kurikulum </td>
                                    <td class="text-muted">
                                        <input type="text" name="kurikulum_edit" id="kurikulum_edit" autocomplete="off"
                                            class="form-control" placeholder="Kurikulum">
                                    </td>
                                </tr>
                                <tr>
                                    <td>NPSN</td>
                                    <td class="text-muted">
                                        <input type="text" name="npsn_edit" id="npsn_edit" autocomplete="off"
                                            class="form-control" placeholder="NPSN">
                                    </td>
                                    <td>Nomor Ujian</td>
                                    <td class="text-muted">
                                        <input type="text" name="nomor_ujian_edit" id="nomor_ujian_edit" autocomplete="off"
                                            class="form-control" placeholder="Nomor Ujian">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Orang Tua </td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_orang_tua_edit" id="nama_orang_tua_edit"
                                            autocomplete="off" class="form-control" placeholder="Nama Orang Tua">
                                    </td>
                                    <td>Nomor Peserta</td>
                                    <td class="text-muted">
                                        <input type="text" name="nomor_peserta_edit" id="nomor_peserta_edit"
                                            autocomplete="off" class="form-control" placeholder="Nomor Ujian">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telepon </td>
                                    <td class="text-muted">
                                        <input type="text" name="telepon_edit" id="nama_orang_tua_edit" autocomplete="off"
                                            class="form-control" placeholder="Telepon">
                                    </td>
                                    <td>Keputusan</td>
                                    <td class="text-muted ">
                                        <select class="form-control" name="keputusan_edit" id="keputusan_edit">
                                            <option value="" disabled="disabled" selected="selected">Pilih Keputusan
                                            </option>
                                            <option value="lulus">Lulus</option>
                                            <option value="tidak_lulus">Tidak Lulus</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jurusan</td>
                                    <td class="text-muted">
                                        <input type="text" name="jurusan_edit" id="jurusan_edit" autocomplete="off"
                                            class="form-control" placeholder="Jurusan">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kelas</td>
                                    <td class="text-muted">
                                        <input type="text" name="kelas_edit" id="kelas_edit" autocomplete="off"
                                            class="form-control" placeholder="Jurusan">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td class="text-muted" colspan="4">
                                        <div class="col-sm-12">
                                            <div class="form-group row mb-0">
                                                <label class="col-md-3 col-form-label" for="l1"></label>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img id="modal-previewedit"
                                                                src="https://via.placeholder.com/150" alt="Preview"
                                                                class="form-group mb-1" width="100px" height="100px"
                                                                style="margin-top: 10px">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input id="image_edit" type="file" name="image_edit"
                                                                            accept="image/*" onchange="readURL2(this);">
                                                                        <input type="hidden" name="hidden_image"
                                                                            id="hidden_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowEditX" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Keputusan Kelulusan </h5>
                </div>
                <form id="PFormxNewUpdateX" name="PFormxNewUpdateX" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_siswax" id="id_siswax" class="form-control">
                    <div class="modal-body">
                        <table class="table mb-0" width="100%">
                            <tbody>
                                <tr>
                                    <td>NIS</td>
                                    <td class="text-muted">
                                        <input type="text" name="nis_editx" id="nis_editx" autocomplete="off"
                                            class="form-control" placeholder="NIS" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NISN</td>
                                    <td class="text-muted">
                                        <input type="text" name="nisn_editx" id="nisn_editx" autocomplete="off"
                                            class="form-control" placeholder="NISN" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="text-muted">
                                        <input type="text" name="nama_editx" id="nama_editx" autocomplete="off"
                                            class="form-control" placeholder="Nama" readonly>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                        <div class="nilai-apende tableWrap "></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Keputusan</td>
                                    <td class="text-muted ">
                                        <select class="form-control" name="keputusan_editx" id="keputusan_edit">
                                            <option value="" disabled="disabled" selected="selected">Pilih Keputusan
                                            </option>
                                            <option value="lulus">Lulus</option>
                                            <option value="tidak_lulus">Tidak Lulus</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalshowPostNilai" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Nilai Siswa </h5>
                </div>
                <form id="PFormxNewNilai" name="PFormxNewNilai" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <td><label class="col-md-3 col-form-label" for="l3">Siswa</label> </td>
                                        <td class="text-muted">
                                            <div class="input-group">
                                                <input type="text" name="siswa_edit" id="siswa_edit" autocomplete="off"
                                                    class="form-control search" placeholder="Siswa" readonly>
                                                <input type="hidden" name="siswa_id" id="siswa_id" autocomplete="off"
                                                    class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table id="table_mapelx" width="100%" class="table table-striped table-responsive">
                                <thead>
                                    <tr class="table-info">
                                        <th width="30%">Mata Pelajaran <span class="text-red">*</span></th>
                                        <th width="20%">initial</th>
                                        <th width="20%">Nilai</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="clonable form-clone" parentId="1">
                                        <td>
                                            <select class="form-control akun_mapel_post" id="akun_mapel_post"
                                                name="akun_mapel_post[]" required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Mata
                                                    Pelajaran
                                                </option>
                                                @foreach ($mapel as $mapel)
                                                    <option value="{{ $mapel['id'] }}">
                                                        {{ $mapel['nama'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input name="initial_post[]" type="text" id="initial_post"
                                                class="form-control" placeholder="Initial" readonly>
                                        </td>
                                        <td><input name="nilai_post[]" type="text" id="nilai_post" class="form-control"
                                                placeholder="Nilai" required="required">
                                        </td>
                                        <td width="10%" class="btn-group text-center" role="group" width="100%">
                                            <button type="button" name="remove" id="removex"
                                                class="btn btn-danger remove-clone remove"><i
                                                    class="fa fa-minus"></i></button> &nbsp;
                                            <button type="button" name="add" id="addx"
                                                class="btn btn-info add-clone add"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info save ">
                            <i class="material-icons list-icon">save</i>
                            Save
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder ml-md-auto login-center mx-auto" id="import_file_siswa"
                style="display:none;">
                <div class="widget-bg">
                    <form name="importfile" id="importfile" enctype="multipart/form-data" id="recommendationDiv">
                        @csrf
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Import Excel </h5>
                            <p class="text-muted">Import data siswa dari file excel</p>
                            <div class="fallback">
                                <input type="file" name="file" id="file1" accept="xlsx,xls,csv">
                            </div>
                        </div>
                        <!-- /.widget-body -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info upload text-center">
                                <i class="fa fa-cloud-upload list-icon"></i>
                                Upload
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.widget-bg -->
            </div>

            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="form-inline">
                            <div class="form-group mr-2">
                                <label class="sr-only" for="exampleInputTahun">Tahun</label>
                                <div class="input-group mb-2">
                                    <select class="form-control" id="tahun" name="tahun" required="">
                                        <option value="" disabled="disabled" selected="selected">Pilih Tahun </option>
                                        @foreach ($tahunlist as $tahunx)
                                            <option value="{{ $tahunx }}">{{ $tahunx }}
                                            </option>
                                        @endforeach
                                    </select>
                                    &nbsp;
                                    <a class="btn btn-primary mb-2" href="javascript: void(0);" id="filter">Filter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_siswa" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Nomor Peserta</th>
                                        <th>Nomor Ujian</th>
                                        <th>Tahun Ajaran</th>
                                        <th>Keputusan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            var table_adminx, table_trash;
            var config, config_trash;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#modalshowPost').modal('show');
                            }
                        },
                        {
                            text: '<i class="fa fa-download list-icon"></i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                var anchor = document.createElement('a');
                                anchor.href = '{{ route('download-file-siswa-simpen') }}';
                                anchor.target = '_blank';
                                anchor.click();
                            }
                        },
                        {
                            text: '<i class="fa fa-cloud-upload list-icon"></i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                $('#import_file_siswa').show();
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax_data_siswa_simpen') }}",
                    columns: [{
                            "class": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'username',
                            name: 'username'
                        },
                        {
                            data: 'nomor_peserta',
                            name: 'nomor_peserta',
                        },
                        {
                            data: 'nomor_ujian',
                            name: 'nomor_ujian',
                        },
                        {
                            data: 'tahun_ajaran',
                            name: 'tahun_ajaran',
                        },
                        {
                            data: 'keputusan',
                            name: 'keputusan',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_siswa').dataTable(config);

                window.detailTable = function detail(d) {
                    var kontak
                    if (d.telepon == null) {
                        kontak = '-';
                    } else {
                        kontak = d.telepon;
                    }
                    var data = `
                       <div class="box-title text-center"> Informasi Siswa </div>
                       <div class="row">
                         <div class="col-mo-4"><span class="ml-3">` + d.photo + `</span></div>
                         <div class="col-mo-4">
                            <div class="box-title text-center">Biodata </div>
                           <div class="table-responsive ml-3">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                               <tr>
                                  <td>Nama</td>
                                  <td>:</td>
                                  <td>` + d.nama + `</td>
                                  <td>NIS</td>
                                  <td>:</td>
                                  <td>` + d.nis + `</td>
                                  <td>NISN</td>
                                  <td>:</td>
                                  <td>` + d.nisn + `</td
                               </tr>
                               <tr>
                                <td>Tempat Lahir </td>
                                  <td>:</td>
                                  <td>` + d.tempat_lahir + `</td>
                                  <td>Tanggal Lahir </td>
                                  <td>:</td>
                                  <td>` + d.tgl_lahir + `</td>
                               </tr>
                               <tr>
                                  <td>Kelas</td>
                                  <td>:</td>
                                  <td>` + d.kelas + `</td>
                                  <td>Jurusan</td>
                                  <td>:</td>
                                  <td>` + d.jurusan + `</td>
                                  <td>Tahun Ajaran </td>
                                  <td>:</td>
                                  <td>` + d.tahun_ajaran + `</td>
                               </tr>
                               <tr>
                                <td>Telepon</td>
                                  <td>:</td>
                                  <td>` + d.telepon + `</td>
                               </tr>
                            </table>
                           </div>
                         </div>
                         <div class="col-md-4">
                            <div class="box-title text-center">Daftar Nilai Mata Pelajaran </div>
                            <div class="print-pengumuman` + d.id + `"></div><br>
                            <div id="appendNilai` + d.id + `"></div>
                         </div>
                       </div>`;

                    window.appendNilai(d.id, d.nama);

                    return data;
                }

                $('#table_siswa tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).parents('tr');
                    var row = table_adminx.api().row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(detailTable(row.data())).show();
                        tr.addClass('shown');
                    }
                });

                //filter tahun
                $('body').on('click', 'a#filter', function() {
                    var tahun = $('select[name="tahun"] option:selected').val();
                    var urlx = '{{ route('filter_data_siswa_simpen', ':tahun') }}';
                    urlx = urlx.replace(':tahun', tahun);

                    config = {
                        dom: 'Bfrtip',
                        destroy: true,
                        buttons: [{
                                text: '<i class="material-icons list-icon">add_circle</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    $('#modalshowPost').modal('show');
                                }
                            },
                            {
                                text: '<i class="fa fa-download list-icon"></i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    var anchor = document.createElement('a');
                                    anchor.href =
                                        '{{ route('download-file-siswa-simpen') }}';
                                    anchor.target = '_blank';
                                    anchor.click();
                                }
                            },
                            {
                                text: '<i class="fa fa-cloud-upload list-icon"></i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    $('#import_file_siswa').show();
                                }
                            },
                            {
                                text: '<i class="material-icons list-icon">refresh</i>',
                                className: "btn btn-outline-default ripple",
                                action: function(e, dt, node, config) {
                                    dt.ajax.reload();
                                }
                            }
                        ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: urlx,
                        columns: [{
                                "class": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": ''
                            },
                            {
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'username',
                                name: 'username'
                            },
                            {
                                data: 'nomor_peserta',
                                name: 'nomor_peserta',
                            },
                            {
                                data: 'nomor_ujian',
                                name: 'nomor_ujian',
                            },
                            {
                                data: 'tahun_ajaran',
                                name: 'tahun_ajaran',
                            },
                            {
                                data: 'keputusan',
                                name: 'keputusan',
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    table_adminx = $('#table_siswa').dataTable(config);

                });

                //read file image upload
                window.readURL2 = function name(input, id) {
                    id = id || '#modal-previewpost';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewepost').removeClass('hidden');

                    }
                };

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-previewedit';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewedit').removeClass('hidden');

                    }
                };

                //edit siswa
                $('body').on('click', '.edit.siswa', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_siswa_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-exchange"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEdit').modal('show');
                                    $('input[name="id_siswa"]').val(rows.id);
                                    $('input[name="nis_edit"]').val(rows.nis);
                                    $('input[name="nisn_edit"]').val(rows.nisn);
                                    $('input[name="nama_edit"]').val(rows.nama);
                                    $('input[name="tgl_lahir_edit"]').val(rows.tgl_lahir);
                                    $('input[name="tempat_lahir_edit"]').val(rows
                                        .tempat_lahir);
                                    $('input[name="tahun_ajaran_edit"]').val(rows
                                        .tahun_ajaran);

                                    $('input[name="nomor_ujian_edit"]').val(rows
                                        .nomor_ujian);
                                    $('input[name="nomor_peserta_edit"]').val(rows
                                        .nomor_peserta);
                                    $('input[name="kelas_edit"]').val(rows.kelas);
                                    $('input[name="jurusan_edit"]').val(rows.jurusan);

                                    $("select[name='keputusan_edit'] > option[value=" + rows
                                        .keputusan + "]").prop("selected", true);

                                    if (rows.file != null || rows.file != '') {
                                        $('#modal-previewedit').removeAttr('src');
                                        $('#modal-previewedit').attr('src', rows.file);
                                    }

                                    $('input[name="nik_edit"]').val(rows.nik);
                                    $('input[name="nama_orang_tua_edit"]').val(rows
                                        .nama_ortu);
                                    $('input[name="npsn_edit"]').val(rows.npsn);
                                    $('input[name="telepon_edit"]').val(rows.telepon);
                                    $('input[name="kurikulum_edit"]').val(rows.kurikulum);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });
                });


                //edit siswa
                $('body').on('click', '.edit-keputusan.siswa', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('show_master_siswa_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('#modalshowEditX').modal('show');
                                    $('input[name="id_siswax"]').val(rows.id);
                                    $('input[name="nis_editx"]').val(rows.nis);
                                    $('input[name="nisn_editx"]').val(rows.nisn);
                                    $('input[name="nama_editx"]').val(rows.nama);

                                    $("select[name='keputusan_editx'] > option[value=" +
                                        rows
                                        .keputusan + "]").prop("selected", true);

                                    var apend_nilai = '';
                                    var indexof = 0;
                                    apend_nilai += '<table class="table">';
                                    apend_nilai += '<thead class="text-info">';
                                    apend_nilai += '<tr>';
                                    apend_nilai += '<td>No</td>';
                                    apend_nilai += '<td>Nama</td>';
                                    apend_nilai += '<td>Nilai</td>';
                                    apend_nilai += '</tr>';
                                    apend_nilai += '</thead>';
                                    apend_nilai += '<tbody>';

                                    if (rows.nilai.length > 0) {
                                        for (const element of
                                                rows
                                                .nilai) { // You can use `let` instead of `const` if you like
                                            indexof++;
                                            apend_nilai += '<tr>';
                                            apend_nilai += '<td>' + indexof + '</td>';
                                            apend_nilai += '<td>' + element['mapel'] +
                                                '</td>';
                                            apend_nilai += '<td>' + element['nilai'] +
                                                '</td>';
                                            apend_nilai += '</tr>';
                                        }

                                    } else {
                                        apend_nilai += '<tr>';
                                        apend_nilai +=
                                            '<td colspan="3"><p class="text-red">Belum ada nilai </p></td>';
                                        apend_nilai += '</tr>';
                                    }

                                    $('.nilai-apende').html(apend_nilai);

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                }
                            }
                        }
                    });
                });



                //save master siswa
                $('body').on('submit', 'Form#PFormxNew', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_master_siswa_simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowPost').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    $('input[name="nis_post"]').val('');
                                    $('input[name="nisn_post"]').val('');
                                    $('input[name="nama_post"]').val('');
                                    $('input[name="tempat_lahir_post"]').val('');
                                    $('input[name="tahun_ajaran_post"]').val('');

                                    $('input[name="nomor_ujian_post"]').val('');
                                    $('input[name="nomor_peserta_post"]').val('');
                                    $('input[name="kelas_post"]').val('');
                                    $('input[name="jurusan_post"]').val('');

                                    $('select').each(function() {
                                        $(this).val($(this).find("option[selected]")
                                            .val());
                                    });

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //update master siswa
                $('body').on('submit', 'Form#PFormxNewUpdate', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('update_master_siswa_simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEdit').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //update keputusan Kelulusan
                $('body').on('submit', 'Form#PFormxNewUpdateX', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('update_keputusan_kelulusan_simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#modalshowEditX').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //upload
                $('body').on('submit', 'Form#importfile', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('upload-file-import-simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.upload');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#import_file_siswa').hide();
                                    $('input#file1').val('');
                                    table_adminx.fnDraw(false);
                                    $(loader).html(
                                        '<i class="fa fa-cloud-upload"></i> Upload');
                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html(
                                        '<i class="fa fa-cloud-upload"></i> Upload');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //append nilai siswa
                window.appendNilai = function nilaimapel(id, nama) {
                    var url = '{{ route('getnilai_siswa_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    var loader = $('#appendNilai' + id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: '',
                        beforeSend: function() {
                            loader.html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    var apend_nilai = '';
                                    var indexof = 0;
                                    apend_nilai += '<table class="table">';
                                    apend_nilai += '<thead>';
                                    apend_nilai += '<tr>';
                                    apend_nilai += '<td>No</td>';
                                    apend_nilai += '<td>Nama</td>';
                                    apend_nilai += '<td>Nilai</td>';
                                    apend_nilai += '</tr>';
                                    apend_nilai += '</thead>';
                                    apend_nilai += '<tbody>';
                                    if (rows.length > 0) {
                                        for (const element of
                                                rows) { // You can use `let` instead of `const` if you like
                                            indexof++;
                                            apend_nilai += '<tr>';
                                            apend_nilai += '<td>' + indexof + '</td>';
                                            apend_nilai += '<td>' + element['mapel'] + '</td>';
                                            apend_nilai += '<td>' + element['nilai'] + '</td>';
                                            apend_nilai += '</tr>';
                                        }
                                        var print_url =
                                            '{{ route('print-pengumuman-simpen', ':id') }}';
                                        print_url = print_url.replace(':id', id);
                                        $('.print-pengumuman' + id).html('<a href="' +
                                            print_url +
                                            '" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-print"></i></a>'
                                        );
                                    } else {
                                        apend_nilai += '<tr>';
                                        apend_nilai +=
                                            '<td colspan="3"><p class="text-red">Belum ada nilai </p><a href="javascript:void(0);" data-id="' +
                                            id + '" data-nama="' + nama +
                                            '" class="btn btn-sm btn-primary" id="inputNilai"> Input Nilai <i class="fa fa-plus"></i></a></td>';
                                        apend_nilai += '</tr>';
                                    }

                                    apend_nilai += '</tbody>';
                                    apend_nilai += '</table>';

                                    $('#appendNilai' + id).html(apend_nilai);
                                }
                            }

                        }
                    });
                }

                $('body').on('click', 'a#inputNilai', function() {
                    var id = $(this).data('id');
                    var nama = $(this).data('nama');
                    $('input[name="siswa_id"]').val(id);
                    $('input[name="siswa_edit"]').val(nama);
                    $('#modalshowPostNilai').modal('show');
                });

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('input[type="number"]').each(function() {
                        return $(this).val('');
                    });

                    clonned.find('textarea').each(function() {
                        return $(this).val('');
                    });
                    $(this).hide();
                    $('.clonable').parents('tbody').append(clonned);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                });

                //select mapel
                $('body').on('change', 'select.akun_mapel_post', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    var id = $(this).val();
                    var url = "{{ route('show_master_mapel_simpen', ':id') }}";
                    url = url.replace(':id', id);
                    if (id) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="initial_post[]"]').val(rows
                                            .nama);
                                    } else if (result['info'] == 'error') {
                                        window.notif('error', result['message']);
                                    }
                                }
                            }
                        });

                    }
                });

                //submit input nilai siswa
                $('body').on('submit', 'Form#PFormxNewNilai', function(e) {
                    e.preventDefault();

                    var nama_siswa = $('input[name="siswa_edit"]').val();
                    var id_siswa = $('input[name="siswa_id"]').val();

                    var urlx = '{{ route('store_nilai_siswa_simpen') }}';
                    var formData = new FormData(this);
                    const loader = $('button.save');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    //reload data append

                                    window.appendNilai(id_siswa, nama_siswa);

                                    $('input[name="siswa_id"]').val('');
                                    $('input[name="siswa_edit"]').val('');
                                    $('#modalshowPostNilai').modal('hide');


                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Save');

                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {

                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove
                $('body').on('click', '.remove.siswa', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('destroy_siswa_simpen', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: '',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });
        })(jQuery, window);
    </script>
@endsection
