@extends('simpen.template')
@section('simpen.components')
    <style>
        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: red;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }

        #myBtn:hover {
            background-color: #555;
        }

        /* Extra small devices (phones, 600px and down) */
        @media only screen and (max-width: 600px) {

            .printpreview,
            .printwindows {
                display: none;
            }
        }

        @media only screen and (min-width: 600px) {

            .printpreview,
            .printwindows {
                display: none;
            }
        }

        @media only screen and (min-width: 768px) {
            .printpreview {
                display: block;
            }

            .printwindows {
                display: inline-block;
            }
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengumuman</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"><a href="{{ route('dashboard-simpen-siswa') }}">Halaman Utama</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <div class="widget-list">
        <div class="row">
            <!-- /.widget-holder -->
            <div class="widget-holder col-12 ml-sm-auto col-sm-6 col-md-7 ml-md-auto login-center mx-auto"
                id="hasil_pengumuman">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <h5 class="box-title">
                            <h4 class="text-center">Pengumuman Kelulusan Tahun Ajaran
                                {{ $rows['tahun_ajaran'] ?? '-' }} </h4>
                        </h5>
                        <hr>
                        <div id="clock" class="lead text-center"></div>
                        <div id="xpengumuman">
                            <center>
                                <a href="{{ route('print-pengumuman-simpen', ['id' => session('id')]) }}"
                                    class="btn btn-sm btn-info printwindows mt-2" target="_blank" style="display:none;"><i
                                        class="fa fa-print"></i> Cetak Pengumuman </a>

                                <a href="{{ route('print-pdf-pengumuman-simpen', ['id' => session('id')]) }}"
                                    id="download-file-pdf" class="btn btn-sm btn-info mt-2 " target="_blank"><i
                                        class="fa fa-download"></i> Pengumuman </a>

                                @if (!empty($surat_pendukung))
                                    @if ($surat_pendukung['surat_keterangan_lulus'] == '1')
                                        <a href="{{ route('print_skl_siswa', ['id_siswa' => session('id')]) }}"
                                            id="download-file-pdf" class="btn btn-sm btn-info mt-2" target="_blank"><i
                                                class="fa fa-download"></i> Surat Keterangan Kelulusan </a>
                                    @endif

                                    @if ($surat_pendukung['surat_nilai'] == '1')
                                        <a href="{{ route('print_nilai_siswa', ['id_siswa' => session('id')]) }}"
                                            id="download-file-pdf" class="btn btn-sm btn-info mt-2" target="_blank"><i
                                                class="fa fa-download"></i> Surat Keterangan Nilai Hasil Ujian </a>
                                    @endif

                                    @if ($surat_pendukung['surat_keterangan_alumni'] == '1')
                                        <a href="{{ route('print_ska_siswa', ['id_siswa' => session('id')]) }}"
                                            id="download-file-pdf" class="btn btn-sm btn-info mt-2" target="_blank"><i
                                                class="fa fa-download"></i> Surat Keterangan Alumni </a>
                                    @endif

                                    @if ($surat_pendukung['surat_keterangan_lain'] == '1')
                                        <a href="{{ route('print_suratlain_siswa', ['id_siswa' => session('id')]) }}"
                                            id="download-file-pdf" class="btn btn-sm btn-info mt-2" target="_blank"><i
                                                class="fa fa-download"></i> Surat Keterangan Lain - Lain </a>
                                    @endif
                                @endif
                            </center>
                            <br><br>
                            @if (count($pengumuman) > 0)
                                <div class="table-responsive printpreview" id="contentx">
                                    <table width="100%">
                                        <thead>
                                            <tr>
                                                <td rowspan="4" style="vertical-align: middle">
                                                    @if ($pengumuman[0]['initial'] == 'logo1' && $pengumuman[0]['aktif'] == 1)
                                                        <img src="{{ $pengumuman[0]['value'] }}"
                                                            style="max-height:138px; min-width: 125px;margin-left:30px;">
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    @if ($pengumuman[2]['initial'] == 'head1' && $pengumuman[2]['aktif'] == 1)
                                                        <b>{{ $pengumuman[2]['value'] }}</b>
                                                    @endif
                                                </td>
                                                <td rowspan="4" style="vertical-align: middle">
                                                    @if ($pengumuman[1]['initial'] == 'logo2' && $pengumuman[1]['aktif'] == 1)
                                                        <img src="{{ $pengumuman[1]['value'] }}"
                                                            style="max-height:138px; min-width: 125px; margin-right:10px;">
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    @if ($pengumuman[3]['initial'] == 'head2' && $pengumuman[3]['aktif'] == 1)
                                                        <b>{{ $pengumuman[3]['value'] }}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    @if ($pengumuman[4]['initial'] == 'head3' && $pengumuman[4]['aktif'] == 1)
                                                        <b>{{ $pengumuman[4]['value'] }}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    @if ($pengumuman[5]['initial'] == 'head4' && $pengumuman[5]['aktif'] == 1)
                                                        <b>{{ $pengumuman[5]['value'] }}</b>
                                                    @endif
                                                    @if ($pengumuman[6]['initial'] == 'alamat' && $pengumuman[6]['aktif'] == 1)
                                                        <br><b>{{ $pengumuman[6]['value'] }}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <hr style="border: solid 2px #000">
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3"
                                                    style="text-align: center; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[7]['initial'] == 'judul' && $pengumuman[7]['aktif'] == 1)
                                                        <b><u>{{ $pengumuman[7]['value'] }}</u></b>
                                                    @endif
                                                    <br>
                                                    @if ($pengumuman[8]['initial'] == 'no_surat' && $pengumuman[8]['aktif'] == 1)
                                                        <b> No : {{ $pengumuman[8]['value'] }}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center" colspan="3">
                                                    @if ($pengumuman[9]['initial'] == 'prolog' && $pengumuman[9]['aktif'] == 1)
                                                        <b>{!! $pengumuman[9]['value'] !!}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    @if ($pengumuman[10]['initial'] == 'isi' && $pengumuman[10]['aktif'] == 1)
                                                        {!! $pengumuman[10]['value'] !!}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <center>
                                                        <table width="50%" border="1">
                                                            <tbody>
                                                                @if ($pengumuman[20]['initial'] == 'nama_siswa' && $pengumuman[20]['aktif'] == 1 && !empty($pengumuman[20]['value']))
                                                                    <tr>
                                                                        <td>Nama</td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[20]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[25]['initial'] == 'no_peserta' && $pengumuman[25]['aktif'] == 1 && !empty($pengumuman[25]['value']))
                                                                    <tr>
                                                                        <td>No Peserta </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[25]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[26]['initial'] == 'no_ujian' && $pengumuman[26]['aktif'] == 1 && !empty($pengumuman[26]['value']))
                                                                    <tr>
                                                                        <td>No Ujian </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[26]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[28]['initial'] == 'nis' && $pengumuman[28]['aktif'] == 1 && !empty($pengumuman[28]['value']))
                                                                    <tr>
                                                                        <td>NIS </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[28]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[29]['initial'] == 'nisn' && $pengumuman[29]['aktif'] == 1 && !empty($pengumuman[29]['value']))
                                                                    <tr>
                                                                        <td>NISN </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[29]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[31]['initial'] == 'npsn' && $pengumuman[31]['aktif'] == 1 && !empty($pengumuman[31]['value']))
                                                                    <tr>
                                                                        <td>NPSN </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[31]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[21]['initial'] == 'kelas' && $pengumuman[21]['aktif'] == 1)
                                                                    <tr>
                                                                        <td>Kelas </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[21]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[24]['initial'] == 'jurusan' && $pengumuman[24]['aktif'] == 1)
                                                                    <tr>
                                                                        <td>Jurusan </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[24]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if ($pengumuman[23]['initial'] == 'ortu' && $pengumuman[23]['aktif'] == 1 && $pengumuman[23]['value'] != null)
                                                                    <tr>
                                                                        <td>Nama Orang tua </td>
                                                                        <td align="center"><span
                                                                                class="text-center">:</span></td>
                                                                        <td align="center">
                                                                            <b>{{ $pengumuman[23]['value'] }}</u>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </center>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3"
                                                    style="text-align: center; font-weight: bold; font-size: 14pt">
                                                    <br><b>Dinyatakan </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3"
                                                    style="text-align: center; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[18]['initial'] == 'keputusan' && $pengumuman[18]['aktif'] == 1)
                                                        <h1><b>{{ Str::ucfirst($pengumuman[18]['value']) }}</b></h1>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="font-weight: bold; font-size: 12pt">
                                                    @if ($pengumuman[19]['initial'] == 'daftar_nilai' && $pengumuman[19]['aktif'] == 1)
                                                        <p style="text-align:center">
                                                            Dengan nilai sebagai berikut:
                                                        </p>
                                                        <center>
                                                            <table width="50%" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th align="center" class="text-center"> No</th>
                                                                        <th align="center" class="text-center"> Nama Mata
                                                                            Pelajaran </th>
                                                                        <th align="center" class="text-center"> Nilai
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                @foreach ($pengumuman[19]['value'] as $val)
                                                                    <tr>
                                                                        <td align="center">{{ $loop->iteration }} </td>
                                                                        <td align="center"> {{ $val['nama'] }}</td>
                                                                        <td align="center">{{ $val['nilai'] }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </center>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">
                                                    @if ($pengumuman[11]['initial'] == 'penutup' && $pengumuman[11]['aktif'] == 1)
                                                        <b>{!! $pengumuman[11]['value'] !!}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"
                                                    style="text-align: right; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[12]['initial'] == 'tempat' && $pengumuman[12]['aktif'] == 1)
                                                        <small><b>{{ $pengumuman[12]['value'] }}</b></small>,
                                                    @endif
                                                    @if ($pengumuman[13]['initial'] == 'tgl' && $pengumuman[13]['aktif'] == 1)
                                                        <small>{{ \Carbon\Carbon::parse($pengumuman[13]['value'])->isoFormat('dddd, D MMM Y') }}</small>
                                                    @endif
                                                    <h5>Kepala Sekolah</h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"
                                                    style="text-align: right; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[15]['initial'] == 'stempel' && $pengumuman[15]['aktif'] == 1 && !empty($pengumuman[15]['value']))
                                                        <img src="{{ $pengumuman[15]['value'] }}" width="150px"
                                                            height="150px"
                                                            style="text-align: right; position: absolute;margin-top: -75px;margin-right: -33px;margin-left:-50px;">
                                                    @endif
                                                </td>
                                                <td colspan="2"
                                                    style="text-align: right; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[16]['initial'] == 'ttd_kepsek' && $pengumuman[16]['aktif'] == 1 && !empty($pengumuman[16]['value']))
                                                        <img src="{{ $pengumuman[16]['value'] }}"
                                                            style=" text-align: right; max-height:138px; min-width: 125px; padding: 13px; margin-right:0px;">
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"
                                                    style="text-align: right; font-weight: bold; font-size: 14pt">
                                                    @if ($pengumuman[14]['initial'] == 'kepsek' && $pengumuman[14]['aktif'] == 1)
                                                        <small><u><b>{{ $pengumuman[14]['value'] }}</b></u></small><br>
                                                    @endif
                                                    @if ($pengumuman[17]['initial'] == 'nip_kepsek' && $pengumuman[17]['aktif'] == 1)
                                                        <small><b>{{ $pengumuman[17]['value'] }}</b></small>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="elementH"></div>
                            @endif
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                @if (!empty($rows['tgl_buka']))
                    $('#clock').countdown("{{ $rows['tgl_buka'] }}", {
                    elapse: true
                    })
                    .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                    $("#clock").hide();
                    $("#xpengumuman" ).show();
                    //$('#resultx').show();
                    } else {
                    $this.html(event.strftime(
                    'Pengumuman dapat dilihat: <span>%H Jam %M Menit %S Detik</span> lagi'
                    ));
                    $( "#xpengumuman" ).hide();
                    //$('#resultx').hide();
                    }
                    });
                @endif

                //Get the button
                var mybutton = document.getElementById("myBtn");

                // When the user scrolls down 20px from the top of the document, show the button
                window.onscroll = function() {
                    scrollFunction()
                };

                function scrollFunction() {
                    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                        mybutton.style.display = "block";
                    } else {
                        mybutton.style.display = "none";
                    }
                }

                // When the user clicks on the button, scroll to the top of the document
                window.topFunction = function topFunction() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }

                @if (empty(session('role')))
                    var route_logout = "{{ route('logout-simpen') }}";
                    window.location.href = route_logout;
                @endif

            });

        })(jQuery, window);
    </script>
@endsection
