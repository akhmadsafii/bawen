<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <nav class="navbar admin">
            <div class="navbar-header" style="left: 10%">
                <a href="#" class="navbar-brand admin">
                    <img class="logo-expand" alt=""
                        src="{{ session('logo_sekolah') != null ? session('logo_sekolah') : asset('asset/img/sma.png') }}"
                        style="max-width: 25%;">
                    <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                        style="max-width: 50%;">
                    <span class="text-white logo-expand text-uppercase">
                        {{ str_replace('_', ' ', session()->get('nama_program')) }} </span>
                    <span class="text-white logo-collapse text-uppercase">
                        {{ str_replace('_', ' ', session()->get('nama_program')) }} </span>
                </a>
            </div>
            <div class="spacerx" style="margin-left: 32px;"></div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <div class="spacer"></div>
            @include('includes.profil_header')
        </nav>




        {{-- <nav class="navbar bg-info">
            <!-- Logo Area -->
            <div class="navbar-header" style="left: 10%;">
                <a href="#" class="navbar-brand admin bg-info">
                    <img class="logo-expand" alt=""
                        src="{{ session('logo_sekolah') != null ? session('logo_sekolah') : asset('asset/img/sma.png') }}"
                        style="max-height: 59px;">
                    <img class="logo-collapse" alt=""
                        src="{{ session('logo_sekolah') != null ? session('logo_sekolah') : asset('asset/img/sma.png') }}"
                        style="max-height: 59px;">
                    <span class="text-white logo-expand text-uppercase mx-2">
                        {{ str_replace('_', ' ', session()->get('nama_program')) }} </span>
                    <span class="text-white logo-collapse text-uppercase mx-2">
                        {{ str_replace('_', ' ', session()->get('nama_program')) }} </span>
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <div class="spacer"></div>

            @include('includes.profil_header')
        </nav> --}}
        <div class="content-wrapper">
            @php
                $program = Session::get('config');
                $role = Session::get('role');
            @endphp
            @if (session('role') == 'admin-cbt')
                @include('includes.program-' . $program . '.menu.menu-' . $role)
                @yield('content')
            @else
                <aside class="site-sidebar clearfix">

                    @if (session('role') == 'superadmin')
                        @include('includes.menu-superadmin')
                    @else
                        @include('includes.program-' . $program . '.menu.menu-' . $role)
                    @endif
                </aside>

                <main class="main-wrapper clearfix">
                    <div class="widget-list">
                        @yield('content')
                    </div>
                </main>
            @endif
        </div>
        @include('includes.footer')
        <script>
            document.addEventListener('contextmenu', function(e) {
                e.preventDefault();
            });
        </script>
    </div>
    @include('includes.foot')
</body>

</html>
