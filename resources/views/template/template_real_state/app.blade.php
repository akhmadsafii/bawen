<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body class="header-dark sidebar-light sidebar-collapse">
    <div id="wrapper" class="wrapper">
        <nav class="navbar">
            <div class="navbar-header">
                <a href="{{ route('dashboard-ppdb') }} " class="navbar-brand admin">
                    <img class="logo-expand" alt="" src="{{ asset('asset/img/sma.png') }}" style="max-height: 59px;">
                    <img class="logo-collapse" alt="" src="{{ asset('asset/img/sma.png') }}" style="max-height: 59px;">
                    <span class="text-white logo-expand text-uppercase"> {{ str_replace('_', ' ', session()->get('config')) }} </span>
                    <span class="text-white logo-collapse text-uppercase"> {{ str_replace('_', ' ', session()->get('config')) }} </span>
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <form class="navbar-search d-none d-md-block" role="search">
                <i class="material-icons list-icon">search</i>
                <input type="text" class="search-query" placeholder="Search anything...">
                <a href="javascript:void(0);" class="remove-focus">
                    <i class="material-icons">clear</i>
                </a>
            </form>
            <div class="spacer"></div>
            @include('includes.profil_header')
        </nav>
        <div class="content-wrapper">
            <aside class="site-sidebar scrollbar-enabled clearfix">
                <div class="side-user">
                    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
                        <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                            <img src="{{ asset('asset/demo/users/user-image.png') }}"
                                class="media-object rounded-circle" alt="">
                        </figure>
                        <div class="media-body hide-menu">
                            <h4 class="media-heading mr-b-5 text-uppercase">Scott Adams</h4>
                            <span class="user-type fs-12">Edit Profile (...)</span>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                    <ul class="nav in side-menu">
                        <li>
                            <a href="page-profile.html">
                                <i class="list-icon material-icons">face</i>
                                My Profile
                            </a>
                        </li>
                        <li>
                            <a href="app-inbox.html">
                                <i class="list-icon material-icons">mail_outline</i>
                                Inbox
                            </a>
                        </li>
                        <li>
                            <a href="page-lock-screen.html">
                                <i class="list-icon material-icons">settings</i>
                                Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="page-login.html">
                                <i class="list-icon material-icons">settings_power</i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
                @php
                    $program = Session::get('config');
                    $role = Session::get('role');
                @endphp
                @include('includes.program-'.$program.'.menu.menu-'.$role)
            </aside>
            <main class="main-wrapper clearfix">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h5 class="mr-0 mr-r-5">Collapsed Sidebar</h5>
                        <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
                    </div>
                    <div class="page-title-right d-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.html">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Collapsed Sidebar</li>
                        </ol>
                        <div class="d-none d-sm-inline-flex justify-center align-items-center">
                            <a href="javascript: void(0);"
                                class="btn btn-outline-primary mr-l-20 btn-sm btn-rounded hidden-xs hidden-sm ripple"
                                target="_blank">Buy Now
                            </a>
                        </div>
                    </div>
                </div>
                <div class="widget-list">
                    @yield('content')
                </div>
            </main>
        </div>
        @include('includes.footer')
        <script>
            document.addEventListener('contextmenu', function(e) {
                e.preventDefault();
            });
        </script>
    </div>
    @include('includes.foot')
</body>

</html>
