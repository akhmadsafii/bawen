<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body class="sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <nav class="navbar">
            <div class="navbar-header">
                <a href="#" class="navbar-brand admin">
                    <img class="logo-expand" alt="" src="{{ asset('asset/img/sma.png') }}"
                        style="max-height: 59px;">
                    <img class="logo-collapse" alt="" src="{{ asset('asset/img/sma.png') }}"
                        style="max-height: 59px;">
                    <span class="text-white logo-expand text-uppercase">
                        {{ str_replace('_', ' ', session()->get('config')) }} </span>
                    <span class="text-white logo-collapse text-uppercase">
                        {{ str_replace('_', ' ', session()->get('config')) }} </span>
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <form class="navbar-search d-none d-md-block" role="search">
                <i class="material-icons list-icon">search</i>
                <input type="text" class="search-query" placeholder="Search anything...">
                <a href="javascript:void(0);" class="remove-focus">
                    <i class="material-icons">clear</i>
                </a>
            </form>
            <div class="spacer"></div>
            @include('includes.profil_header')
        </nav>
        <div class="content-wrapper">
            <aside class="site-sidebar clearfix">
                <!-- ini sidebar atas-->
                @php
                    $program = Session::get('config');
                    $role = Session::get('role');
                @endphp
                @include('includes.program-' . $program . '.menu.menu-' . $role)
            </aside>
            <main class="main-wrapper clearfix">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h5 class="mr-0 mr-r-5">Horizontal Navigation</h5>
                        <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
                    </div>
                    <div class="page-title-right d-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.html">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Horizontal Navigation</li>
                        </ol>
                        <div class="d-none d-sm-inline-flex justify-center align-items-center">
                            <a href="javascript: void(0);"
                                class="btn btn-outline-primary mr-l-20 btn-sm btn-rounded hidden-xs hidden-sm ripple"
                                target="_blank">Buy Now
                            </a>
                        </div>
                    </div>
                </div>
                <div class="widget-list">
                    @yield('content')
                </div>
            </main>
        </div>
        @include('includes.footer')
        <script>
            document.addEventListener('contextmenu', function(e) {
                e.preventDefault();
            });
        </script>
    </div>
    @include('includes.foot')
</body>

</html>
