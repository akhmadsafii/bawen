<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.mobile.head')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-horizon/0.1.0/bootstrap-horizon.min.css">
</head>

<body>
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader bg-primary scrolled">
        <div class="left">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#sidebarPanel">
                <ion-icon name="menu-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            E-Learning
        </div>
        <div class="right">
            <!-- notifikasi -->
            <a href="javascript:;" class="headerButton" data-toggle="dropdown">
                <ion-icon name="notifications-outline"></ion-icon>
                <span class="badge badge-danger">{{ count($notif) }}</span>
            </a>
            <div class="dropdown-menu">
                @if (empty($notif))
                    <h6 class="dropdown-header">Tidak ada pemberitahuan.</h6>
                @else
                    <a href="{{ route('room-read_all_notif') }}">tandai baca semua</a>
                    @foreach ($notif as $nt)
                        <a class="dropdown-item" href="{{ $nt['url'] }}" class="media"
                            onclick="updateRead({{ $nt['id'] }})">
                            <ion-icon name="chatbox-outline"></ion-icon>
                            {{ $nt['isi'] }}
                        </a>
                    @endforeach
                @endif
            </div>
            <!-- end notifikasi -->

            <!-- profil -->
            <div class="dropdown">
                <a href="javascript(0)" class="headerButton" data-toggle="dropdown" data-target="dropdown-profile">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <div class="dropdown-menu" id="dropdown-profile">
                    <a href="{{ url('program/e_learning/profile/edit') }}" class="item">
                        <div class="col">
                            <ion-icon name="create-outline"></ion-icon>
                            <p>Edit profil</p>
                        </div>
                    </a>

                    <a href="{{ route('auth.logout') }}" class="item">
                        <div class="col">
                            <ion-icon name="log-out-outline"></ion-icon>
                            <p>Logout</p>
                        </div>
                    </a>

                    <a href="{{ url('program/e_learning') }}" class="item">
                        <div class="col">
                            <ion-icon name="return-down-back-outline"></ion-icon>
                            <p>Dasboard</p>
                        </div>
                    </a>

                </div>

            </div>
        </div>
    </div>
    <div id="appCapsule" style="background:#f9f9f9;">
        @yield('content')
        @include('includes.mobile.footer')
        <script>
            document.addEventListener('contextmenu', function(e) {
                e.preventDefault();
            });
        </script>

    </div>
    @include('includes.mobile.menu_bottom')
    @include('includes.program-e_learning.menu.menu-siswa_mobile')
    <script>
        $('.modal-header').html('<h5 class="modal-title"></h5><a href="javascript:;" data-dismiss="modal">Close</a>');
    </script>
    @include('includes.mobile.foot')
</body>

</html>
