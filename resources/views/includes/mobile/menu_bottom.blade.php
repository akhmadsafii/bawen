<div class="appBottomMenu">
    <a href="{{ route('master-kalender_rombel') }}" class="item {{ Request::segment(3) === 'kalender_rombel' ? 'active' : '' }}">
        <div class="col">
            <ion-icon name="calendar-clear-outline"></ion-icon>
            <strong>Pengumuman Kelas</strong>
        </div>
    </a>
    <a href="{{ route('room-rombel') }}" class="item {{ ( Request::segment(3) === 'rombel' || Request::segment(3) === 'room' ) ? 'active' : '' }}">
        <div class="col">
            <ion-icon name="cube-outline"></ion-icon>
            <strong>Kelas</strong>
        </div>
    </a>
    <a href="{{ url('program/e_learning') }}" class="item {{ empty(Request::segment(3)) ? 'active' : '' }}">
        <div class="col">
            <ion-icon name="home-outline"></ion-icon>
            <strong>Beranda</strong>
        </div>
    </a>
    <a href="{{ url('program/e_learning/feeds_rombel') }}" class="item {{ Request::segment(3) === 'feeds_rombel'? 'active' : '' }}">
        <div class="col">
            <ion-icon name="chatbubbles-outline"></ion-icon>
            <strong>Feed Kelas</strong>
        </div>
    </a>
    <a href="{{ route('master-kalender') }}" class="item {{ Request::segment(3) === 'kalendar' ? 'active' : '' }}">
        <div class="col">
            <ion-icon name="calendar-outline"></ion-icon>
            <strong>Kalender Akademik</strong>
        </div>
    </a>
    <!-- <a href="{{ url('program/e_learning/profile/edit') }}" class="item {{ Request::segment(3) === 'profile' ? 'active' : '' }}">
        <div class="col">
            <ion-icon name="person-outline"></ion-icon>
            <strong>Profile</strong>
        </div>
    </a> -->
</div>
