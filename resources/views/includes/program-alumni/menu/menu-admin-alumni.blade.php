<style>
    /* .sidebar-horizontal.header-centered .side-menu {
        justify-content: left;
    } */

    #loading {
        background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
        height: 300px;
        width: 300px;
        display: block;
        margin: auto;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        text-align: center;
        top: 66px;
    }

    .pace {
        display: none;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        /* margin: 20px 0; */
        border-radius: 4px;
        /* float: right; */
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
@if (Session::has('message'))
    {{-- {{ dd("testing") }} --}}
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif

<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/alumni') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>
        <li class="menu-item-has-children">
            <a href="javascript:void(0);">
                <i class="fas fa-database"></i> <span class="hide-menu">Master</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('alumni-fakultas') }}">Fakultas & Jurusan</a>
                </li>
                <li>
                    <a href="{{ route('alumni-jenis_pekerjaan') }}">Jenis Pekerjaan</a>
                </li>
                <li>
                    <a href="{{ route('alumni-kategori_blog') }}">Kategori Blog</a>
                </li>
                <li>
                    <a href="{{ route('alumni-kategori_galeri') }}">Kategori Galeri</a>
                </li>
                <li>
                    <a href="{{ route('alumni-pekerjaan_alumni_tracking', ['by' => 'all', 'key' => 'null']) }}">Tracking
                        Pekerjaan</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="{{ route('alumni-blog') }}" class="ripple">
                <i class="fas fa-newspaper"></i>
                <span class="hide-menu">Blog</span>
            </a>
        </li>
        <li>
            <a href="{{ route('alumni-agenda') }}" class="ripple">
                <i class="fas fa-calendar-alt"></i>
                <span class="hide-menu">Agenda</span>
            </a>
        </li>

        <li class="menu-item-has-children">
            <a href="javascript:void(0);">
                <i class="fas fa-address-card"></i> <span class="hide-menu">User</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('alumni-alumni') }}">Alumni</a>
                </li>
                <li><a href="{{ route('alumni-admin') }}">Admin</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('alumni-survey') }}" class="ripple">
                <i class="fas fa-poll"></i>
                <span class="hide-menu">Survey</span>
            </a>
        </li>
        <li>
            <a href="{{ route('alumni-galeri') }}" class="ripple">
                <i class="fas fa-images"></i>
                <span class="hide-menu">Galeri</span>
            </a>
        </li>
    </ul>
</nav>
