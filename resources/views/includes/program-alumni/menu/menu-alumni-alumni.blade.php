<style>
    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        /* margin: 20px 0; */
        border-radius: 4px;
        /* float: right; */
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
@if (Session::has('message'))
    <script>
        swa('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/alumni') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>

        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('alumni-alumni') }}" class="ripple">
                <i class="fas fa-address-card"></i>
                <span class="hide-menu">Alumni</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('alumni-blog') }}" class="ripple">
                <i class="fas fa-newspaper"></i>
                <span class="hide-menu">Blog</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('alumni-agenda') }}" class="ripple">
                <i class="fas fa-calendar-alt"></i>
                <span class="hide-menu">Agenda</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('alumni-galeri') }}" class="ripple">
                <i class="fas fa-images"></i>
                <span class="hide-menu">Galeri</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('alumni-survey') }}" class="ripple">
                <i class="fas fa-poll"></i>
                <span class="hide-menu">Survey</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan_email' ? 'current-page active' : '' }}">
            <a href="{{ route('detail-profile_alumni', ['method' => 'profil']) }}" class="ripple">
                <i class="fas fa-user-shield"></i>
                <span class="hide-menu">Profil Saya</span>
            </a>
        </li>
    </ul>
</nav>
