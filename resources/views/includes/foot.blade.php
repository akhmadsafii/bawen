<script>
    function test(routes) {
        console.log(routes);
        var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
        var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
        var sort = (document.getElementById("sort") != null) ? document.getElementById("sort").value : "";
        var per_page = (document.getElementById("per_page") != null) ? document.getElementById("per_page").value : "10";
        var url = window.location.href + "?search=" + search + "&status=" + status + "&sort=" + sort + "&per_page=" +
            per_page;
        // console.log(search);
        var search = searchs.replace(/ /g, '-')
        var url_asli = window.location.href;
        var explode_url = url_asli.split("?")[0];
        var url = explode_url + "?search=" + search + "&status=" + status + "&sort=" + sort + "&per_page=" +
            per_page;
        document.location = url;
    }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
<script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css"
    rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"
defer>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" defer>
</script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js" defer>
</script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.15/js/bootstrap-select.min.js" defer></script>

<script src="{{ asset('asset/js/theme.js') }}" defer></script>
<script src="{{ asset('asset/js/custom.js') }}" defer></script>
<script>
    $('#radioBtn a').on('click', function() {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);
        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass(
            'notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    })

    $(".select2").select2({
        tags: true,
        createTag: function(params) {
            return {
                id: params.term,
                text: params.term,
                newOption: true
            }
        }
    });

    $(".select3").select2({});

    jQuery('.datepicker, #datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "id",
        autoclose: true,
    });

    jQuery('.datepickerYear').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        language: "id",
        autoclose: true,
    });

    function rubahRibuan(x) {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }

    $('.ribuan').keyup(function(event) {
        if (event.which >= 37 && event.which <= 40) return;
        // format number
        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
    });

    $('.ribuans').keyup(function(event) {
        if (event.which >= 37 && event.which <= 40) return;
        // format number
        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
    });

    function convertDate(inputFormat) {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }
        var d = new Date(inputFormat)
        return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('-')
    }

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    }

    function swa(status, message, icon) {
        swal(
            status,
            message,
            icon
        );
        return true;
    }

    function swa_api(message) {
        swal(
            message
        );
        return true;
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');
            $('#start').hide();
        }
    }

    function readURLS(input, id) {
        id = id || '#modal-previews';
        if (input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-previews').removeClass('hidden');
            $('#starts').hide();
        }
    }

    $('body').on('submit', '#importFile', function(e) {
        e.preventDefault();
        var actionType = $('#btn-save').val();
        $('#importBtn').html('Sending..');
        var formDatas = new FormData(document.getElementById("importFile"));
        $.ajax({
            type: "POST",
            url: url_import,
            data: formDatas,
            cache: false,
            contentType: false,
            processData: false,
            success: (data) => {
                if (data.status == 'berhasil') {
                    $('#importFile').trigger("reset");
                    $('#importModal').modal('hide');
                    $('#importBtn').html('Simpan');
                    var oTable = $('#data-tabel').dataTable();
                    oTable.fnDraw(false);
                    noti(data.icon, data.success);
                } else {
                    noti(data.icon, data.success);
                    $('#importBtn').html('Simpan');
                }
            },
            error: function(data) {
                console.log('Error:', data);
                $('#importBtn').html('Simpan');
            }
        });
    });

    function resetPass(id, params) {
        // let action_url = '';
        if (params == 'supervisor') {
            action_url = "{{ route('reset_password-supervisor') }}";
        } else if (params == 'walikelas') {
            action_url = "{{ route('reset_password-walikelas') }}";
        } else if (params == 'bk') {
            action_url = "{{ route('reset_password-bk') }}";
        } else if (params == 'siswa') {
            action_url = "{{ route('reset_password-siswa') }}";
        } else if (params == 'admin') {
            action_url = "{{ url('super_admin/master/sekolah/' . Request::segment(4) . '/admin/reset_password') }}";
        } else if (params == 'ortu') {
            action_url = "{{ route('reset_password-ortu') }}";
        } else if (params == 'tu') {
            action_url = "{{ route('reset_password-tata_usaha') }}";
        } else if (params == 'admin_prakerin') {
            action_url = "{{ route('reset_password-admin_prakerin') }}";
        } else if (params == 'admin_bkk') {
            action_url = "{{ route('reset_password-admin_bkk') }}";
        } else if (params == 'admin_induk') {
            action_url = "{{ route('reset_password-admin_induk') }}";
        } else if (params == 'guru') {
            action_url = "{{ route('reset_password-guru') }}";
        }

        swal({
            title: "Apa kamu yakin?",
            text: "ingin Mereset Password User!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                url: action_url,
                type: "POST",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".key-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        if (params == 'siswa') {
                            location.reload();
                        }
                        $('#data-tabel').dataTable().fnDraw(false);
                    }
                    $(".key-" + id).html('<i class="fa fa-key"></i> Reset Password');
                    swa(data.status + "!", data.message, data.icon);
                }
            })
        }, function(dismiss) {
            if (dismiss === 'cancel') {
                swa("Dibatalkan!", 'Proses dibatalkan', 'error');
            }
        })
    }

    var settingTable = {
        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "",
        buttons: [{
                text: 'Tambah Data',
                className: 'btn btn-sm btn-facebook',
                attr: {
                    title: 'Tambah Data',
                    id: 'createNewCustomer'
                }
            },
            {
                text: '<i class="fa fa-upload"></i>',
                className: 'btn btn-sm',
                attr: {
                    title: 'Import Data',
                    id: 'import'
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },

            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function(doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            {
                text: '<i class="fas fa-sync-alt"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload(null, false);
                }
            },
            {
                text: '<i class="fa fa-undo"></i>',
                attr: {
                    title: 'Data Trash',
                    id: 'data_trash'
                }
            },
            'colvis',
        ],
    }

    var alert_swal = {
        title: 'Apa anda yakin?',
        text: "Data anda nantinya tidak dapat dipulihkan lagi!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }
</script>
<script src="{{ asset('asset/js/service_worker.js') }}"></script>
