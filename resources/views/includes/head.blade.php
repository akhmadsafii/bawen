{{-- <link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" sizes="20x20"
    href="{{ session()->has('favicon') ? session('favicon') : $sekolah['file'] }}">
{{-- <title>E-Learning MYSCH.ID</title> --}}
<title>{{ !empty(Session::get('title')) ? session('title') : $sekolah['nama'] }}</title>
<!-- CSS -->
<link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
    rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
    rel="stylesheet" type="text/css">
<link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">

<style>
    .side-user {
        padding: 0.42857em 0;
        border-bottom: 1 px solid #ddd;
        background: #f7fafc;
    }

    .side-menu ul {
        margin-left: 3.80em;
        position: relative;
        top: -0.38462em;
    }

    .hide-menu.single {
        margin-left: 0.9rem !important;
    }

    .hide-menu.print {
        margin-left: 0.5rem !important;
    }

    .hide-menu.info {
        margin-left: 0rem !important;
    }

    .hide-menu.download {
        margin-left: 0.6rem !important;
    }

    .hide-menu.pesan {
        margin-left: 0.8rem !important;
    }

    .navbar.admin,
    .navbar-header.admin,
    .navbar-brand.admin,
    .site-sidebar.admin,
    .dropdown-card.admin {
        background: #36a7d3;
    }

    .navbar.user,
    .navbar-header.user,
    .navbar-brand.user,
    .site-sidebar.user,
    .dropdown-card.user {
        background: #03a9f3;
    }

    /* .side-menu li:hover,
    .side-menu li.active {
        background: #0910e9;
    } */

    .side-menu>li>a:hover,
    .side-menu>li>a:focus {
        /* color: #ebe0e0; */
        background: #e1d9d900;
    }

    ul.list-unstyled.sub-menu.collapse.in>li>a:before {
        color: dimgrey;
    }

    /* .side-menu li a {
        color: #f9ecec;
        position: relative;
        display: block;
        padding-top: 0;
        padding-bottom: 0;
    } */

    .dropdown-card.dropdown-card-wide {
        min-width: auto;
        width: 13rem;
        max-width: 100vw;
    }

    .navbar-nav>li .dropdown-menu {
        margin-top: 4px;
    }

    .dropdown-card.admin:before {
        position: absolute;
        top: -8px;
        left: 164px;
        display: inline-block;
        border-right: 9px solid transparent;
        border-bottom: 9px solid #36a7d3;
        border-left: 9px solid transparent;
        content: '';
    }

    .dropdown-card.user:before {
        position: absolute;
        top: -8px;
        left: 164px;
        display: inline-block;
        border-right: 9px solid transparent;
        border-bottom: 9px solid #03a9f3;
        border-left: 9px solid transparent;
        content: '';
    }

    .sidecustom {
        margin-top: -19px;
    }

    .card-heading-extra {
        border-bottom: 1px solid #fff;
        padding-bottom: 0;
        margin-bottom: 0;
        text-transform: uppercase;
        font-size: 0.78571em;
    }


    .side-menu>li>a {
        padding-left: 1em;
        font-size: 1.15385em;
        line-height: 3.2em;
        /* color: #f5f2f2; */
        border-left: 3px solid transparent;
        font-family: "Nunito Sans", sans-serif;
        font-weight: 600;
    }

    .modal-primary .modal-content.modal-header {
        background: #fb9678;
        border: 0;
    }

    @media (min-width: 961px) {
        .sidebar-expand .site-sidebar {
            position: fixed;

            height: calc(100vh - 5.625rem);
        }

        .sidebar-collapse .side-menu .sub-menu li {
            background: #03a9f3;
        }

        .sidebar-toggle::after {
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: transparent;
        }

    }

    @media screen and (max-width: 960px) {
        .sidebar-toggle.admin::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: #03a9f3;
        }

        .sidebar-toggle.user::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: #03a9f3;
        }

        .sidebar-toggle {
            margin-left: 1rem !important;
        }

        .page-title ol {
            display: none;
        }
    }


    .pengumuman:hover {
        background-color: seashell;
    }

    table.dataTable thead .sorting,
    table.dataTable thead .sorting_asc,
    table.dataTable thead .sorting_desc,
    table.dataTable thead .sorting_asc_disabled,
    table.dataTable thead .sorting_desc_disabled {
        cursor: pointer;
        *cursor: hand;
        background-repeat: no-repeat !important;
        background-position: center right !important;
    }

    .dataTables_wrapper table.dataTable thead .sorting::before {
        content: 'sort';
        opacity: 0;
    }

    .table thead th {
        vertical-align: middle;
    }

</style>
@if (session('config') == 'e_learning')
    <style>
        @media (max-width: 960px) {
            /* .navbar-header {
                width: 15.75rem;
            } */

            .navbar .logo-collapse {
                width: 78px;
            }

            .navbar-brand img {
                margin: 0 10px;
            }

            .sidebar-expand .site-sidebar {
                width: 15.375rem;
            }
        }

    </style>
@else
    <style>
        @media (max-width: 960px) {
            .sidebar-collapse .navbar-header {
                width: 7.75rem;
            }
        }

    </style>
@endif
