<a href="{{ route('admin_learning-login') }}" class="btn btn-warning mb-3"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
<div class="panel panel-primary">
    <div class="panel-body widget-bg mb-3">
        <div class="row border border-purple rounded m-1">
            <div class="col-xs-12 col-md-12 text-center">
                <h2 class="box-title mt-1">Menu Room</h2>
                <hr>
                <a href="{{ route('multiple_rombel', (new \App\Helpers\Help())->encode(session('id_rombel'))) }}"
                    class="btn btn-{{ request()->segment(3) == 'rombel' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-comments"></i>
                    <br />Beranda</a>
                <a href="{{ route('data_siswa_rombels') }}"
                    class="btn btn-{{ request()->segment(5) == 'data_siswa_rombel' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-users-cog"></i> <br />
                    Data Siswa</a>
                <a href="{{ route('walikelas-guru_mapel') }}"
                    class="btn btn-{{ request()->segment(4) == 'guru_mapel' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-user-tie"></i> <br />
                    Guru Mapel</a>
                <a href="{{ route('walikelas-bahan_ajar') }}"
                    class="btn btn-{{ request()->segment(4) == 'bahan_ajar' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-paste"></i>
                    <br />Bahan Ajar</a>
                <a href="{{ route('walikelas-absensi') }}"
                    class="btn btn-{{ request()->segment(4) == 'absensi' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-user-edit"></i>
                    <br />Absensi</a>
                <a href="{{ route('walikelas-tugas_pengetahuan') }}"
                    class="btn btn-{{ request()->segment(5) == 'pengetahuan' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-laptop-code"></i>
                    <br />Tugas Pengetahuan</a>
                <a href="{{ route('walikelas-tugas_ketrampilan') }}"
                    class="btn btn-{{ request()->segment(5) == 'ketrampilan' ? 'success' : 'secondary' }} btn-lg my-2"
                    role="button"><i class="fas fa-tools"></i>
                    <br />Tugas
                    Ketrampilan</a>
            </div>
        </div>
    </div>
</div>
