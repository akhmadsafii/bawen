<style>
    span.room-menu {
        margin-left: 11px;
    }

    .list-group-item.active {
        z-index: 2;
        color: #fff;
        background-color: #51d2b7;
        border-color: #51d2b7;
    }

</style>
@if (session('role') == 'guru')
    <a href="{{ route('room-logout', session('id_encode')) }}" class="btn btn-block btn-default"
        style="margin-bottom: 10px; background-color: #a20909; border-color: #ffffff; color: #fff;">
        <i class="material-icons list-icon">power_settings_new</i>
        <span> Akhiri Sesi</span>
    </a>
@endif
<div class="sidebar-widgets-wrap">
    <div class="widget widget_links clearfix">
        <div class="list-group">
            @php
                $active = '';
                if (Request::segment(4) == 'beranda') {
                    $active = 'active';
                }
            @endphp
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/room/rombel') ? 'active' : '' }}"
                href="{{ route('room-rombel') }}" title="Dashboard Kelas">
                <i class="list-icon material-icons">home</i>
                <span class="room-menu"> Beranda Kelas</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/data_siswa/data_siswa_rombel') ? 'active' : '' }}"
                href="{{ route('data_siswa_rombels') }}" title="Data Siswa">
                <i class="list-icon material-icons">people</i>
                <span class="room-menu"> Data Siswa</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/guru_mapel') ? 'active' : '' }}"
                href="{{ route('walikelas-guru_mapel') }}" title="Data Guru Mapel">
                <i class="list-icon material-icons">school</i>
                <span class="room-menu"> Data Guru Mapel</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/bahan_ajar') ? 'active' : '' }}"
                href="{{ route('walikelas-bahan_ajar') }}" title="Absensi Kelas">
                <i class="list-icon material-icons">transfer_within_a_station</i>
                <span class="room-menu"> Bahan Ajar</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/absensi') ? 'active' : '' }}"
                href="{{ route('walikelas-absensi') }}" title="Jurnal Guru">
                <i class="list-icon material-icons">menu</i>
                <span class="room-menu"> Absensi</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/tugas/pengetahuan') ? 'active' : '' }}"
                href="{{ route('walikelas-tugas_pengetahuan') }}" title="Penilaian Pengetahuan">
                <i class="list-icon material-icons">laptop_windows</i>
                <span class="room-menu"> Tugas Pengetahuan</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/wali_kelas/tugas/ketrampilan') ? 'active' : '' }}"
                href="{{ route('walikelas-tugas_ketrampilan') }}" title="Penilaian Ketrampilan">
                <i class="list-icon material-icons">laptop_windows</i>
                <span class="room-menu"> Tugas Ketrampilan</span>
            </a>
            @if (session('role') == 'guru')
                <a class="list-group-item list-group-item-action d-flex {{ request()->is('program/e_learning/room/pengaturan') ? 'active' : '' }}"
                    href="{{ route('room-pengaturan') }}" title="Computer Based Test (CBT)">
                    <i class="list-icon material-icons">settings</i>
                    <span class="room-menu"> Pengaturan Kelas</span>
                </a>
            @endif
        </div>
    </div>
</div>
