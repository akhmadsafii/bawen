<style>
    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    /* .color-color-scheme .material-icons{
        color:#51d2b7 !important;
    } */

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

    @media (max-width: 960px) {
        .navbar-header {
            width: 10.75rem !important;
        }
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/e_learning') }}" class="ripple">
                <span class="{{ empty(Request::segment(3)) ? 'color-color-scheme' : '' }}">
                    <i
                        class="list-icon material-icons {{ empty(Request::segment(3)) ? 'color-color-scheme' : '' }}">network_check</i>
                    <span class="hide-menu">Dashboard</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/e_learning/kalendar') }}" class="ripple">
                <span class="{{ Request::segment(3) == 'kalendar' ? 'color-color-scheme' : '' }}">
                    <i
                        class="list-icon material-icons {{ Request::segment(3) == 'kalendar' ? 'color-color-scheme' : '' }}">perm_contact_calendar</i>
                    <span class="hide-menu">Kalender Akademik</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('e-learning-kalender_rombel') }}" class="ripple">
                <span class="{{ Request::segment(3) == 'kalendar_rombel' ? 'color-color-scheme' : '' }}">
                    <i
                        class="list-icon material-icons {{ Request::segment(3) == 'kalendar_rombel' ? 'color-color-scheme' : '' }}">headset_mic</i>
                    <span class="hide-menu">Pengumuman Kelas</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('feed_rombel-beranda') }}" class="ripple">
                <span class="{{ Request::segment(3) == 'feeds_rombel' ? 'color-color-scheme' : '' }}">
                    <i
                        class="list-icon material-icons {{ Request::segment(3) == 'feeds_rombel' ? 'color-color-scheme' : '' }}">forum</i>
                    <span class="hide-menu">Feeds Rombel</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('room-rombel') }}" class="ripple">
                <span
                    class="{{ Request::segment(3) == 'rombel' || !empty(Request::segment(4)) ? 'color-color-scheme' : '' }}">
                    <i
                        class="list-icon material-icons {{ Request::segment(3) == 'rombel' || !empty(Request::segment(4)) ? 'color-color-scheme' : '' }}">school</i>
                    <span class="hide-menu">Kelas</span>
                </span>
            </a>
        </li>
    </ul>
</nav>
