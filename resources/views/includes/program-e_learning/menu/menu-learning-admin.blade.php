{{-- {{dd($tema)}} --}}
<style>
    .pace {
        display: none;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                style="background-image: url('{{ session('avatar') }}')"></div>
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/e_learning') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'guru_pelajaran' || request()->segment(3) == 'kelas_siswa'? 'current-page active': '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'tes' ? 'color-color-scheme' : '' }}"><i
                        class="fas fa-cogs"></i>
                    <span
                        class="hide-menu {{ Request::segment(2) === 'tes' ? 'color-color-scheme' : '' }}">Setting</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('e_learning-guru_pelajaran', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                        class="{{ Request::segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Mapel per
                        Kelas</a>
                    <a href="{{ route('e_learning-kelas_siswa', ['based' => 'siswa']) }}"
                        class="{{ Request::segment(3) === 'kelas_siswa' ? 'color-color-scheme' : '' }}">Siswa per
                        Kelas</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ Request::segment(2) === 'user' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}"><i
                        class="fas fa-users-cog"></i>
                    <span class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">Setting
                        User</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('user-wali_kelas', ['tahun' => session('tahun')]) }}"
                        class="{{ Request::segment(3) === 'wali-kelas' ? 'color-color-scheme' : '' }}">Wali Kelas
                    </a>
                    <a href="{{ route('user-guru') }}"
                        class="{{ Request::segment(3) === 'guru' ? 'color-color-scheme' : '' }}">Guru
                    </a>
                    <a href="{{ route('user-siswa') }}"
                        class="{{ Request::segment(3) === 'siswa' ? 'color-color-scheme' : '' }}">Siswa
                    </a>
                    <a href="{{ route('user-supervisor') }}"
                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Supervisor
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(4) == 'room' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class=""><i class="fas fa-chalkboard-teacher"></i>
                    <span class="hide-menu">Learning</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('admin_learning-room', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                        class="{{ Request::segment(4) === 'room' ? 'color-color-scheme' : '' }}">Data Room
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(4) == 'login' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_learning-login') }}" class="ripple">
                <i class="fas fa-chalkboard"></i>
                <span class="hide-menu">Masuk ke Program</span>
            </a>
        </li>

    </ul>
</nav>
