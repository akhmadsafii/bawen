<style>
    .image-listview>li a.item:after {
        background-image: none !important;
    }

</style>
<div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="profileBox">
                    <div class="image-wrapper">
                        <img src="{{ $profile['file'] }}" alt="image"
                            class="imaged rounded">
                    </div>
                    <div class="in">
                        <strong>{{ ucwords($profile['nama']) }}</strong>
                        <div class="text-muted">
                            <ion-icon name="location"></ion-icon>
                            ...{{ substr($profile['sekolah'], -17) }}
                        </div>
                    </div>
                    <a href="javascript:;" class="close-sidebar-button" data-dismiss="modal">
                        <ion-icon name="close"></ion-icon>
                    </a>
                </div>
                <!-- * profile box -->

                <ul class="listview flush transparent no-line image-listview mt-2">
                    {{-- <li style="background-color: #09ab31; border-radius: 29px;">
                        <a href="index.html" class="item">
                            <div class="icon-box bg-success">
                                <ion-icon name="alarm-outline"></ion-icon>
                            </div>
                            <div class="in" style="color: #fff">
                                Absensi Kehadiran
                            </div>
                        </a>
                    </li> --}}
                    <li>
                        <a href="{{ url('program/e_learning/room', Request::segment(4)) }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="reorder-four-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Beranda Room
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/standar_kompetensi') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="stats-chart-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Standar Kompetensi
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/bahan_ajar') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="reader-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Bahan Ajar
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/data_siswa') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="people-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Data Siswa
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/absensi') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="person-add-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Absensi Kelas
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/jurnal_guru') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="clipboard-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Jurnal Guru
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/tugas_pengetahuan') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="receipt-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Tugas Pengetahuan
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/tugas_ketrampilan') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="construct-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Tugas Ketrampilan
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/kalendar_kelas') }}" class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="calendar-clear-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Kalender Kelas</div>
                            </div>
                        </a>
                    </li>
                </ul>

                {{-- <div class="listview-title mt-2 mb-1">
                    <span>Friends</span>
                </div>
                <ul class="listview image-listview flush transparent no-line">
                    <li>
                        <a href="page-chat.html" class="item">
                            <img src="{{asset('asset/mobile/img/sample/avatar/avatar7.jpg')}}"alt="image" class="image">
                            <div class="in">
                                <div>Sophie Asveld</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="page-chat.html" class="item">
                            <img src="{{asset('asset/mobile/img/sample/avatar/avatar3.jpg')}}"alt="image" class="image">
                            <div class="in">
                                <div>Sebastian Bennett</div>
                                <span class="badge badge-danger">6</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="page-chat.html" class="item">
                            <img src="{{asset('asset/mobile/img/sample/avatar/avatar10.jpg')}}"alt="image" class="image">
                            <div class="in">
                                <div>Beth Murphy</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="page-chat.html" class="item">
                            <img src="{{asset('asset/mobile/img/sample/avatar/avatar2.jpg')}}"alt="image" class="image">
                            <div class="in">
                                <div>Amelia Cabal</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="page-chat.html" class="item">
                            <img src="{{asset('asset/mobile/img/sample/avatar/avatar5.jpg')}}"alt="image" class="image">
                            <div class="in">
                                <div>Henry Doe</div>
                            </div>
                        </a>
                    </li>
                </ul> --}}

            </div>

            <!-- sidebar buttons -->
            {{-- <div class="sidebar-buttons">
                <a href="javascript:;" class="button">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <a href="javascript:;" class="button">
                    <ion-icon name="archive-outline"></ion-icon>
                </a>
                <a href="javascript:;" class="button">
                    <ion-icon name="settings-outline"></ion-icon>
                </a>
                <a href="javascript:;" class="button" style="justify-content: flex-end; margin-right: 9px">
                    <ion-icon name="log-out-outline"></ion-icon>
                    <span style="font-size: 9px"> keluar kelas</span>
                </a>
            </div> --}}
        </div>
    </div>
</div>
