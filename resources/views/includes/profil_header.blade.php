<ul class="nav navbar-nav d-none d-lg-block">
    <li class="dropdown dropdown-notifications" style="display: none;"><a href="javascript:void(0);"
            class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false"><i
                class="material-icons list-icon">mail_outline</i> <span class="badge badge-border bg-primary"
                data-count="0"><span class="notif-count">0</span></span></a>
        <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-dark animated flipInY">
            <div class="card">
                <header class="card-header">New messages <span
                        class="mr-l-10 badge badge-border badge-border-inverted bg-primary"><span
                            class="notif-count">0</span></span>
                </header>
                <ul class="list-unstyled dropdown-list-group ps ps--theme_default ps--active-y list-message">
                </ul>
            </div>
            <!-- /.card -->
        </div>
    </li>
</ul>
<ul class="nav navbar-nav">
    <li class="dropdown hide">
        {{-- @if (session('role') == 'admin')
            <a href="{{ route('dashboard-master') }}" aria-expanded="true">
                <span class="mr-b-10 sub-heading-font-family fw-300 text-white">
                    Master Sekolah
                </span>
            </a>
        @endif --}}
        <a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="true">
            <span class="mr-b-10 sub-heading-font-family fw-300 text-white">
                {{ session('username') }}
                <i class="material-icons list-icon">expand_more</i></span>
            </span>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card admin dropdown-card-wide ">
            <div class="card mb-2">
                <ul class="list-unstyled sidecustom ">
                    <li><a href="{{ session('role') == 'admin' ? route('edit-profile_admin') : route('edit-profile', request()->segment(2)) }}"
                            class="media text-white my-1">Ubah Profil</li>
                    <li><a href="{{ route('auth.logout') }}" class="media text-white my-1">Logout</a></li>
                </ul>
            </div>
        </div>

    </li>
</ul>

{{-- <ul class="nav navbar-nav d-none d-lg-flex">
    <li class="dropdown">
        @if (session('role') != 'admin' and session('role') != 'admin-kesiswaan' and session('role') != 'superadmin' and session('role') != 'pembimbing-industri' and session('role') != 'admin-prakerin' and session('role') != 'learning-admin' and session('role') != 'bk' and session('role') != 'ortu' and session('role') != 'bkk-admin' and session('role') != 'walikelas' and session('role') != 'supervisor' and session('role') != 'induk-admin' and session('role') != 'alumni-alumni' and session('role') != 'admin-alumni' and session('role') != 'user-kaprodi' and session('role') != 'bkk-perusahaan' and session('role') != 'admin-raport' and session('role') != 'admin-absensi' and session('role') != 'admin-cbt' and session('role') != 'admin-kepegawaian' and session('role') != 'pegawai')
            <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
                <i class="material-icons list-icon">public</i>
                <span class="badge badge-pill bg-primary px-2">{{ count($notif) }}</span>
            </a>
            <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
                <div class="card">
                    <header class="card-header">New notifications
                        <span class="mr-l-10 badge badge-border bg-primary">{{ count($notif) }}</span>
                    </header>
                    <ul class="list-unstyled dropdown-list-group">
                        @if (empty($notif))
                            <li>
                                <a href="#" class="media">
                                    <span class="media-body">
                                        <span class="media-heading">Tidak ada pemberitahuan yang tersedia</span>
                                    </span>
                                </a>
                            </li>
                        @else
                            <a href="{{ route('room-read_all_notif') }}">tandai baca semua</a>
                            @foreach ($notif as $nt)
                                <li>
                                    <a href="{{ $nt['url'] }}" class="media"
                                        onclick="updateRead({{ $nt['id'] }})">
                                        <span class="d-flex">
                                            <i class="material-icons list-icon">comment</i>
                                        </span>
                                        <span class="media-body">
                                            <span class="media-heading">{{ $nt['isi'] }}</span>
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        @endif
    </li>
</ul> --}}


<script>
    function updateRead(id) {
        $('#form_result').html('');
        $.ajax({
            type: 'POST',
            url: "{{ route('room-read_notif') }}",
            data: {
                id
            },
            success: function(data) {
                console.log(data);
            }
        });
    }
</script>
