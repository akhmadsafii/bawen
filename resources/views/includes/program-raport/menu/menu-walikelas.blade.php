<style>
    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                style="background-image: url('{{ session('avatar') }}')"></div>
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        {{-- <li class="current-page active"> --}}
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="list-icon material-icons">network_check</i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>
        <li class="{{ Request::segment(3) === 'nilai-sikap_spr' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-nilai_sikap_spiritual') }}" class="ripple">
                <i class="list-icon material-icons">wb_sunny</i>
                <span class="hide-menu">Nilai Sikap Spiritual</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'nilai-sikap_sosial' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-nilai_sikap_sosial') }}" class="ripple">
                <i class="list-icon material-icons">record_voice_over</i>
                <span class="hide-menu">Nilai Sikap Sosial</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'absensi' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-absensi') }}" class="ripple">
                <i class="list-icon material-icons">people</i>
                <span class="hide-menu">Absensi</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'nilai-ekstrakurikuler' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-nilai_ekstrakurikuler') }}" class="ripple">
                <i class="list-icon material-icons">nature_people</i>
                <span class="hide-menu">EkstraKurikuler</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'prestasi' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-prestasi') }}" class="ripple">
                <i class="list-icon material-icons">stars</i>
                <span class="hide-menu">Prestasi</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'catatan' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-catatan') }}" class="ripple">
                <i class="list-icon material-icons">event_note</i>
                <span class="hide-menu">Catatan</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'cetak-raport' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-cetak_raport', ['tahun' => session('tahun')]) }}" class="ripple">
                <i class="list-icon material-icons">import_contacts</i>
                <span class="hide-menu">Cetak Raport</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'cetak-leger' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-cetak_leger', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                class="ripple">
                <i class="list-icon material-icons">description</i>
                <span class="hide-menu">Cetak Leger</span>
            </a>
        </li>
    </ul>
</nav>
