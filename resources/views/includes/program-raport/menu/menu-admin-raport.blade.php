<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                style="background-image: url('{{ session('avatar') }}')"></div>
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'sikap_sosial' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-sikap_sosial') }}" class="ripple">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Sikap Sosial</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'sikap_spiritual' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-sikap_spiritual') }}" class="ripple">
                <i class="fas fa-praying-hands"></i>
                <span class="hide-menu">Sikap Spiritual</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-predikat') }}" class="ripple">
                <i class="fas fa-star"></i>
                <span class="hide-menu">Predikat</span>
            </a>
        </li>


        <li
            class="menu-item-has-children {{ request()->segment(3) == 'config_pts' ||request()->segment(3) == 'kkm' ||request()->segment(4) == 'raport_set_nilai'? 'active': '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}"><i
                        class="fas fa-tools"></i>
                    <span class="hide-menu {{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                        Settingan Nilai</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('raport-config_pts', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                        class="{{ Request::segment(3) === 'config_pts' ? 'color-color-scheme' : '' }}">Nilai PTS
                    </a>
                    <a href="{{ route('raport-setting_nilai') }}"
                        class="{{ Request::segment(4) === 'raport_set_nilai' ? 'color-color-scheme' : '' }}">Nilai
                        PAS
                    </a>
                    <a href="{{ route('raport-kkm', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'jr' => 'null','kl' => 'null']) }}"
                        class="{{ Request::segment(3) === 'kkm' ? 'color-color-scheme' : '' }}">KKM
                    </a>
                </li>
            </ul>
        </li>

        <li
            class="menu-item-has-children {{ request()->segment(4) === 'config' ||request()->segment(4) === 'config-template' ||request()->segment(4) === 'template_sampul'? 'active': '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class=""><i class="fas fa-cogs"></i>
                    <span class="hide-menu">
                        Config</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('raport-config', ['key' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                        class="{{ Request::segment(4) === 'config' ? 'color-color-scheme' : '' }}">Setting
                    </a>
                    <a href="{{ route('raport-config_template', ['key' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                        class="{{ Request::segment(4) === 'config-template' ? 'color-color-scheme' : '' }}">Template
                    </a>
                    <a href="{{ route('raport-template_sampul', ['key' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                        class="{{ Request::segment(4) === 'template_sampul' ? 'color-color-scheme' : '' }}">Template
                        Sampul
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(4) == 'kop_sampul' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-kop_sampul') }}" class="ripple">
                <i class="fas fa-envelope-open-text"></i>
                <span class="hide-menu">Kop Sampul</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'admin_raport' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-beranda') }}" class="ripple">
                <i class="fas fa-user-cog"></i>
                <span class="hide-menu">Admin Raport</span>
            </a>
        </li>
    </ul>
</nav>
