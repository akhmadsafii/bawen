<style>
    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
                {{-- <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt=""> --}}
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="fas fa-columns"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li>
            <a href="{{ route('raport-cetak_raport', ['tahun' => session('tahun')]) }}" class="ripple">
                <i class="fas fa-file-contract"></i>
                <span class="hide-menu">Lihat Raport</span>
            </a>
        </li>
    </ul>
</nav>
