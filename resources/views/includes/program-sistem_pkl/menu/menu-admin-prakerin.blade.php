<style>
    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }


    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    div#data-tabel_length {
        margin-top: 2.14286em;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/sistem_pkl') }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'industri' || request()->segment(3) == 'pembimbing_industri' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i
                        class="fa fa-industry"></i>
                    <span
                        class="hide-menu">Industri
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_industri-beranda') }}"
                        class="{{ Request::segment(3) === 'tahun_ajaran' ? 'color-color-scheme' : '' }}">Perusahaan</a>
                    <a href="{{ route('pkl_pembimbing_industri-beranda') }}"
                        class="{{ Request::segment(3) === 'jurusan' ? 'color-color-scheme' : '' }}">Pembimbing
                        Industri</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'kelompok' || request()->segment(3) == 'peserta' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i
                        class="fa fa-tools"></i>
                    <span class="hide-menu">PKL
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    {{-- <a href="{{ route('pkl_kelompok-list_data') }}" >Kelompok  PKL</a> --}}
                    <a href="{{ route('pkl_siswa-beranda') }}">Peserta PKL</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'jenis_nilai' || request()->segment(3) == 'nilai' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i class="fa fa-pencil-ruler"></i>
                    <span class="hide-menu">Nilai
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_jenis_nilai-beranda') }}">Jenis
                        Nilai</a>
                    <a href="{{ route('pkl_nilai-beranda') }}">Input Nilai</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'informasi' || request()->segment(3) == 'pedoman' || request()->segment(3) == 'slider' || request()->segment(3) == 'config' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i class="fa fa-cogs"></i>
                    <span
                        class="hide-menu">Setting
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_informasi-beranda') }}">Informasi</a>
                    <a href="{{ route('pkl_pedoman-beranda') }}">Pedoman</a>
                    <a href="{{ route('pkl_slider-beranda') }}">Slider</a>
                    <a href="{{ route('pkl_config-beranda') }}">Tampilan Pengunjung</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
