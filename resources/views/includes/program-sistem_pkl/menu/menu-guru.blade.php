{{-- {{dd($tema)}} --}}
<style>
    #loading {
        background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
        height: 300px;
        width: 300px;
        display: block;
        margin: auto;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        text-align: center;
        top: 66px;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/' . Request::segment(2)) }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>
        <li>
            <a href="{{ route('pkl_tugas-beranda') }}" class="ripple">
                <i class="fa fa-tasks"></i>
                <span class="hide-menu">Kegiatan Siswa</span>
            </a>
        </li>
        <li>
            <a href="{{ route('pkl_nilai_siswa-list_siswa') }}" class="ripple">
                <i class="fas fa-star"></i>
                <span class="hide-menu">Nilai Siswa</span>
            </a>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="fas fa-file-invoice"></i>
                <span class="hide-menu">Sertifikat</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('prakerin-template_sertifikat') }}">Template Sertifikat</a>
                </li>
                <li><a href="{{ route('pembimbing_sertifikat-beranda') }}">Pengaturan Template</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
