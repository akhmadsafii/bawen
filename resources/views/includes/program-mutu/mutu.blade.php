<style>
    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    #radioBtn .notActive {
        color: #3276b1;
        background-color: #fff;
        border: 1px solid #fff;
    }

    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    button.btn.btn-danger.btn-sm,
    a.btn.btn-success.btn-sm {
        margin-top: 3px;
    }

    .color-color-scheme{
        color: #ffffff !important;
    }

    .sidebar-dark .site-sidebar {
        background: #ffffff;
        border-right: 1px solid #ddd;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active,
    .sidebar-dark .side-menu li.act {
        background: #6ea1e0;
        width: 100%;
    }

    .sidebar-dark .side-menu li a:hover {
        color: #fff;
    }

    span.hide-menu {
        color: #fff;
    }

    .sidebar-dark .side-menu li a {
        color: #d6d6d6;
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #e6e6e6;
    }

    .sidebar-dark .side-menu li.active>a {
        color: #a8a0a0;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }

    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

</style>

<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{Request::segment(3) == '' ? 'active act' : ''}}">
            <a href="{{ url('program/mutu') }}" class="ripple">
                <i class="list-icon material-icons">dashboard</i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>

        <li class="menu-item-has-children {{Request::segment(3) == 'master' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="list-icon material-icons">group_work</i>
                    <span class="hide-menu ">Master
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('main-Jenjang')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Jenjang
                    </a>
                    <a href="{{route('main-Fakultas')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Fakultas
                    </a>
                    <a href="{{route('main-Kejuruan')}}"
                        class="{{Request::segment(4) == 'kejuruan' ? 'color-color-scheme' : ''}}">Kejuruan
                    </a>
                    <a href="{{route('main-kategori')}}"
                        class="{{Request::segment(4) == 'kategori' ? 'color-color-scheme' : ''}}">Kategori Dokumen
                    </a>
                    <a href="{{route('main-subkategori')}}"
                        class="{{Request::segment(4) == 'sub-kategori' ? 'color-color-scheme' : ''}}">Subkategori Dokumen
                    </a>
                    
                </li>
            </ul>
        </li>

        <li class="{{Request::segment(3) == 'pesan' ? 'active act' : ''}}">
            <a href="{{route('inbox-pesan')}}" class="ripple">
                <span class="{{Request::segment(3) == 'pesan' ? 'color-color-scheme' : ''}}">
                <i class="list-icon material-icons">mail</i>
                <span class="hide-menu">Pesan</span>
                </span>
            </a>
        </li>

        @if (session('role-user') == 'admin')
        <li class="{{Request::segment(3) == 'user' ? 'active act' : ''}}">
            <a href="{{ route('user-mutu') }}" class="ripple">
                <span class="{{Request::segment(3) == 'user' ? 'color-color-scheme' : ''}}"> 
                <i class="list-icon material-icons">group</i>
                <span class="hide-menu">User</span>              
                </span>
            </a>
        </li>
        @endif

        <li class="menu-item-has-children {{Request::segment(3) == 'dokumen' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{Request::segment(3) == 'dokumen' ? 'color-color-scheme' : ''}}"> 
                <i class="list-icon material-icons">description</i>
                <span class="hide-menu">Dokumen</span>              
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('main-dokumen')}}"
                        class="{{Request::segment(4) == 'publish' ? 'color-color-scheme' : ''}}">Publish Dokumen
                    </a>
                    <a href="{{route('unpublish-dokumen')}}"
                        class="{{Request::segment(4) == 'unpublish' ? 'color-color-scheme' : ''}}">Unpublish Dokumen
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</nav>