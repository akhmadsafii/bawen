<style>
    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

    .dataTables_wrapper .dataTables_length {
        margin: 0 !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #03a9f3 !important;
    }

    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    #radioBtn .notActive {
        color: #3276b1;
        background-color: #fff;
        border: 1px solid #fff;
    }

    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    .sidebar-dark .site-sidebar {
        background: #ffffff;
        border-right: 1px solid #ddd;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #ffffff;
    }

    .sidebar-dark .side-menu li a:hover {
        color: #51d2b7;
    }

    span.hide-menu {
        color: #aea7a7;
    }

    .sidebar-dark .side-menu li a {
        color: #9e9595;
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #796f6f;
    }

    .sidebar-dark .side-menu li.active>a {
        color: #a8a0a0;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }

    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

</style>
@if (Session::has('error_api'))
    <script>
        $(function() {
            swal({
                title: 'Gagal!',
                text: "{{ session('error_api')['message'] }}",
                timer: 5000,
                type: "{{ session('error_api')['icon'] }}"
            }).then((value) => {
                //location.reload();
            }).catch(swal.noop);
        })
    </script>
@endif
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
                {{-- <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt=""> --}}
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ request()->segment(3) == 'dashboard' ? 'current-page active' : '' }}">
            <a href="{{ url('admin/master/dashboard') }}" class="ripple">
                <i
                    class="fas fa-tachometer-alt {{ request()->segment(3) == 'dashboard' ? 'color-color-scheme' : '' }}"></i>
                <span
                    class="hide-menu {{ request()->segment(3) == 'dashboard' ? 'color-color-scheme' : '' }}">Dashboard</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(2) === 'master' &&
            request()->segment(3) != 'guru_pelajaran' &&
            request()->segment(3) != 'kelas_siswa' &&
            request()->segment(3) != 'template' &&
            request()->segment(3) != 'kalendar' &&
            request()->segment(3) != 'program' &&
            request()->segment(3) != 'program_template' &&
            request()->segment(3) != 'slider' &&
            request()->segment(3) != 'environment' &&
            request()->segment(3) != 'config'
                ? 'active'
                : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span
                    class="{{ request()->segment(2) === 'master' &&
                    request()->segment(3) != 'guru_pelajaran' &&
                    request()->segment(3) != 'kelas_siswa' &&
                    request()->segment(3) != 'template' &&
                    request()->segment(3) != 'kalendar' &&
                    request()->segment(3) != 'program' &&
                    request()->segment(3) != 'config' &&
                    request()->segment(3) != 'slider' &&
                    request()->segment(3) != 'environment' &&
                    request()->segment(3) != 'program_template'
                        ? 'color-color-scheme'
                        : '' }}">
                    <i class="fas fa-database"></i>
                    <span
                        class="hide-menu {{ request()->segment(2) === 'master' &&
                        request()->segment(3) != 'guru_pelajaran' &&
                        request()->segment(3) != 'kelas_siswa' &&
                        request()->segment(3) != 'template' &&
                        request()->segment(3) != 'kalendar' &&
                        request()->segment(3) != 'program' &&
                        request()->segment(3) != 'config' &&
                        request()->segment(3) != 'slider' &&
                        request()->segment(3) != 'environment' &&
                        request()->segment(3) != 'program_template'
                            ? 'color-color-scheme'
                            : '' }}">Master
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('master-tahun_ajaran') }}"
                        class="{{ request()->segment(3) === 'tahun_ajaran' ? 'color-color-scheme' : '' }}">Set Tahun
                        Aktif</a>
                    <a href="{{ route('e_learning-jurusan') }}"
                        class="{{ request()->segment(3) === 'jurusan' ? 'color-color-scheme' : '' }}">Jurusan</a>
                    <a href="{{ route('e_learning-kelas') }}"
                        class="{{ request()->segment(3) === 'kelas' ? 'color-color-scheme' : '' }}">Kelas</a>
                    <a href="{{ route('e_learning-rombel') }}"
                        class="{{ request()->segment(3) === 'rombongan_belajar' ? 'color-color-scheme' : '' }}">Rombel</a>
                    <a href="{{ route('e_learning-mapel') }}"
                        class="{{ request()->segment(3) === 'mapel' ? 'color-color-scheme' : '' }}">Mata
                        Pelajaran</a>
                    <a href="{{ route('master-kategori_ekstrakurikuler') }}"
                        class="{{ request()->segment(3) === 'kategori_ekstra' ? 'color-color-scheme' : '' }}">Kategori
                        Ekstrakurikuler</a>
                    <a href="{{ route('e_learning-kompetensi_inti') }}"
                        class="{{ request()->segment(3) === 'kompetensi_inti' ? 'color-color-scheme' : '' }}">Kompetensi
                        Inti</a>
                    <a href="{{ route('master-jadwal') }}"
                        class="{{ request()->segment(3) === 'jadwal_pelajaran' ? 'color-color-scheme' : '' }}">Jadwal
                        Pelajaran</a>
                    <a href="{{ route('master-jabatan_supervisor') }}"
                        class="{{ request()->segment(3) === 'jabatan' ? 'color-color-scheme' : '' }}">Jabatan
                        Supervisor</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(2) === 'user' && request()->segment(3) != 'admin_sekolah' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span
                    class="{{ request()->segment(2) === 'user' && request()->segment(3) != 'admin_sekolah' ? 'color-color-scheme' : '' }}"><i
                        class="fa fa-users"></i>
                    <span
                        class="hide-menu {{ request()->segment(2) === 'user' && request()->segment(3) != 'admin_sekolah' ? 'color-color-scheme' : '' }}">User</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('user-supervisor') }}"
                        class="{{ request()->segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Supervisor
                    </a>
                    <a href="{{ route('user-kaprodi') }}"
                        class="{{ request()->segment(3) === 'kaprodi' ? 'color-color-scheme' : '' }}">Kaprodi /
                        Kepala Jurusan
                    </a>
                    <a href="{{ route('user-guru') }}"
                        class="{{ request()->segment(3) === 'guru' ? 'color-color-scheme' : '' }}">Guru
                    </a>
                    <a href="{{ route('user-wali_kelas', ['tahun' => session('tahun')]) }}"
                        class="{{ request()->segment(3) === 'wali-kelas' ? 'color-color-scheme' : '' }}">Wali Kelas
                    </a>
                    <a href="{{ route('user-tata_usaha') }}"
                        class="{{ request()->segment(3) === 'tata-usaha' ? 'color-color-scheme' : '' }}">Tata Usaha
                    </a>
                    <a href="{{ route('user-bk') }}"
                        class="{{ request()->segment(3) === 'bk' ? 'color-color-scheme' : '' }}">BK
                    </a>
                    <a href="{{ route('user-siswa') }}"
                        class="{{ request()->segment(3) === 'siswa' ? 'color-color-scheme' : '' }}">Siswa
                    </a>
                    <a href="{{ route('user-orang_tua') }}"
                        class="{{ request()->segment(3) === 'ortu' ? 'color-color-scheme' : '' }}">Orang Tua
                    </a>
                    <a href="{{ route('user-alumni') }}"
                        class="{{ request()->segment(3) === 'alumni' ? 'color-color-scheme' : '' }}">Alumni
                    </a>
                    <a href="{{ route('user-bkk') }}"
                        class="{{ request()->segment(3) === 'bkk' ? 'color-color-scheme' : '' }}">BKK
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('master-kalender') }}"
                class="ripple {{ request()->segment(3) === 'kalendar' ? 'color-color-scheme' : '' }}">
                <i class="fa fa-calendar "></i>
                <span
                    class="hide-menu {{ request()->segment(3) === 'kalendar' ? 'color-color-scheme' : '' }}">Kalender
                    Akademik</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(2) === 'setting' ||
            request()->segment(3) === 'kelas_siswa' ||
            request()->segment(3) === 'guru_pelajaran' ||
            request()->segment(3) === 'program' ||
            request()->segment(3) === 'config' ||
            request()->segment(3) === 'admin_sekolah' ||
            request()->segment(3) === 'template' ||
            request()->segment(3) === 'slider' ||
            request()->segment(3) === 'program_template'
                ? 'active'
                : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span
                    class="{{ request()->segment(2) === 'setting' ||
                    request()->segment(3) === 'kelas_siswa' ||
                    request()->segment(3) === 'guru_pelajaran' ||
                    request()->segment(3) === 'program' ||
                    request()->segment(3) === 'config' ||
                    request()->segment(3) === 'slider' ||
                    request()->segment(3) === 'admin_sekolah' ||
                    request()->segment(3) === 'template' ||
                    request()->segment(3) === 'program_template'
                        ? 'color-color-scheme'
                        : '' }}"><i
                        class="fa fa-wrench"></i>
                    <span
                        class="hide-menu {{ request()->segment(2) === 'setting' ||
                        request()->segment(3) === 'kelas_siswa' ||
                        request()->segment(3) === 'guru_pelajaran' ||
                        request()->segment(3) === 'program' ||
                        request()->segment(3) === 'config' ||
                        request()->segment(3) === 'slider' ||
                        request()->segment(3) === 'admin_sekolah' ||
                        request()->segment(3) === 'template' ||
                        request()->segment(3) === 'program_template'
                            ? 'color-color-scheme'
                            : '' }}">Setting</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('e_learning-sekolah') }}"
                        class="{{ request()->segment(3) === 'admin_sekolah' ? 'color-color-scheme' : '' }}">Sekolah</a>
                    <a href="{{ route('e_learning-guru_pelajaran', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                        class="{{ request()->segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Mapel
                        per
                        Kelas</a>
                    <a href="{{ route('e_learning-kelas_siswa', ['based' => 'siswa']) }}"
                        class="{{ request()->segment(3) === 'kelas_siswa' ? 'color-color-scheme' : '' }}">Siswa per
                        Kelas</a>
                </li>
                <li
                    class="menu-item-has-children {{ request()->segment(3) === 'program' || request()->segment(3) === 'program_template' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'program' || request()->segment(3) === 'program_template'? 'color-color-scheme': '' }}">Program</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin-program') }}"
                                class="{{ request()->segment(3) === 'program' ? 'color-color-scheme' : '' }}">List
                                Program</a>
                        </li>
                        <li>
                            <a href="{{ route('admin-program_mobile') }}"
                                class="{{ request()->segment(3) === 'program_template' ? 'color-color-scheme' : '' }}">Mobile</a>
                        </li>
                    </ul>
                </li>

                <li
                    class="menu-item-has-children {{ request()->segment(3) === 'template' || request()->segment(3) === 'program_template' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'template' || request()->segment(3) === 'program_template'? 'color-color-scheme': '' }}">Template</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('e_learning-program_sekolah') }}"
                                class="{{ request()->segment(3) === 'program_template' ? 'color-color-scheme' : '' }}">Template
                                program</a>
                        </li>
                    </ul>
                </li>

                <li
                    class="menu-item-has-children {{ request()->segment(3) === 'config' || request()->segment(3) === 'slider' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'config' || request()->segment(3) === 'slider' ? 'color-color-scheme' : '' }}">Tampilan</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('master_config-beranda') }}"
                                class="{{ request()->segment(3) === 'config' ? 'color-color-scheme' : '' }}">Config</a>
                            <a href="{{ route('master_slider-beranda') }}"
                                class="{{ request()->segment(3) === 'slider' ? 'color-color-scheme' : '' }}">Slider</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children {{ request()->segment(3) == 'environment' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ request()->segment(3) === 'environment' ? 'color-color-scheme' : '' }}"><i
                        class="fa fa-briefcase"></i>
                    <span
                        class="hide-menu {{ request()->segment(3) === 'environment' ? 'color-color-scheme' : '' }}">Bisnis
                        Sistem</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('master_env-beranda') }}"
                        class="{{ request()->segment(3) === 'environment' ? 'color-color-scheme' : '' }}">Aktivasi
                        Method</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(2) === 'program' ||request()->segment(1) === 'program' ||request()->segment(2) === 'induk'? 'active': '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span
                    class="{{ request()->segment(2) === 'program' ||request()->segment(1) === 'program' ||request()->segment(2) === 'induk'? 'color-color-scheme': '' }}">
                    <i class="fa fa-window-restore"></i>
                    <span
                        class="hide-menu {{ request()->segment(2) === 'program' ||request()->segment(1) === 'program' ||request()->segment(2) === 'induk'? 'color-color-scheme': '' }}">Program</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="menu-item-has-children {{ request()->segment(3) === 'learning' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'learning' ? 'color-color-scheme' : '' }}">Elearning</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('learning_admin-beranda') }}"
                                class="{{ request()->segment(4) === 'admin_prakerin' ? 'color-color-scheme' : '' }}">Admin
                                Elearning</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_learning-room', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                                class="{{ request()->segment(4) === 'room' ? 'color-color-scheme' : '' }}">Room</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_learning-login') }}"
                                class="{{ request()->segment(4) === 'room' ? 'color-color-scheme' : '' }}">Masuk ke
                                sistem</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(3) === 'raport' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'raport' ? 'color-color-scheme' : '' }}">Raport</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_raport-sikap_sosial') }}"
                                class="{{ request()->segment(4) === 'sikap_sosial' ? 'color-color-scheme' : '' }}">Sikap
                                Sosial</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_raport-sikap_spiritual') }}"
                                class="{{ request()->segment(4) === 'sikap_spiritual' ? 'color-color-scheme' : '' }}">Sikap
                                Spiritual</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-predikat') }}"
                                class="{{ request()->segment(4) === 'raport_predikat' ? 'color-color-scheme' : '' }}">Predikat</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-setting_nilai') }}"
                                class="{{ request()->segment(4) === 'raport_set_nilai' ? 'color-color-scheme' : '' }}">Settingan
                                Nilai</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-config', ['key' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                                class="{{ request()->segment(4) === 'config' ? 'color-color-scheme' : '' }}">Config</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-kop_sampul') }}"
                                class="{{ request()->segment(4) === 'kop_sampul' ? 'color-color-scheme' : '' }}">Kop
                                Sampul</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-template_sampul', ['key' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar'))]) }}"
                                class="{{ request()->segment(4) === 'template_sampul' ? 'color-color-scheme' : '' }}">Template
                                Sampul</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_raport-beranda') }}"
                                class="{{ request()->segment(4) === 'template_sampul' ? 'color-color-scheme' : '' }}">Admin
                                Raport</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'point' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'point' ? 'color-color-scheme' : '' }}">Point
                    </a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children {{ request()->segment(2) === 'point' ? 'active' : '' }}">
                            <a href="javascript:void(0);"
                                class="{{ request()->segment(3) === 'kategori_pelanggaran' ||request()->segment(3) === 'pelanggaran' ||request()->segment(3) === 'sanksi'? 'color-color-scheme': '' }}">Data
                                Tata Tertib</a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('kesiswaan_ketegori_pelanggaran-beranda') }}"
                                        class="{{ request()->segment(3) === 'kategori_pelanggaran' ? 'color-color-scheme' : '' }}">Kategori
                                        Pelanggaran</a>
                                </li>
                                <li>
                                    <a href="{{ route('kesiswaan_sanksi-beranda') }}"
                                        class="{{ request()->segment(3) === 'sanksi' ? 'color-color-scheme' : '' }}">Sanksi
                                        Pelanggaran</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('point_pelanggaran_siswa-beranda') }}">Pelanggaran Siswa</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'bursa_kerja' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'bursa_kerja' ? 'color-color-scheme' : '' }}">BKK</a>
                    <ul class="list-unstyled sub-menu">
                        <li
                            class="menu-item-has-children {{ request()->segment(3) === 'industri' ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="ripple">
                                <span class="{{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                                    <span
                                        class="hide-menu {{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">Manajemen
                                        Mitra</span>
                                </span>
                            </a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('bkk_industri-beranda') }}"
                                        class="{{ request()->segment(3) === 'industri' ? 'color-color-scheme' : '' }}">Mitra
                                        Industri
                                    </a>
                                    <a href="{{ route('user_bkk-perusahaan') }}"
                                        class="{{ request()->segment(4) === 'perusahaan' ? 'color-color-scheme' : '' }}">User
                                        Perusahaan
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children {{ request()->segment(2) === 'user' ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="ripple">
                                <span class="{{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                                    <span
                                        class="hide-menu {{ request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">Bursa
                                        Kerja</span>
                                </span>
                            </a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('bkk_bidang_loker-beranda') }}"
                                        class="{{ request()->segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Bidang
                                        Loker
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('lowongan-create') }}"
                                        class="{{ request()->segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Input
                                        Loker
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('lowongan-beranda') }}"
                                        class="{{ request()->segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">List
                                        Loker
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('bkk_admin-beranda') }}"
                                class="{{ request()->segment(3) === 'admin_bkk' ? 'color-color-scheme' : '' }}">Admin
                                Bursa Kerja</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(3) === 'prakerin' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'prakerin' ? 'color-color-scheme' : '' }}">Prakerin</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_prakerin-beranda') }}"
                                class="{{ request()->segment(4) === 'admin_prakerin' ? 'color-color-scheme' : '' }}">Admin
                                Prakerin</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'induk' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'induk' ? 'color-color-scheme' : '' }}">Buku Induk</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_induk-beranda') }}"
                                class="{{ request()->segment(3) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                Buku Induk </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'kesiswaan' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'kesiswaan' ? 'color-color-scheme' : '' }}">Kesiswaan</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('kesiswaan_admin-beranda') }}"
                                class="{{ request()->segment(3) === 'admin_kesiswaan' ? 'color-color-scheme' : '' }}">Admin
                                Kesiswaan</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(3) === 'induk' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'induk' ? 'color-color-scheme' : '' }}">Alumni</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('alumni-admin') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                Alumni</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(3) === 'induk' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(3) === 'induk' ? 'color-color-scheme' : '' }}">CBT</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('cbt-admin') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                CBT</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'mutu' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'mutu' ? 'color-color-scheme' : '' }}">Penjamin
                        Mutu</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('user-mutu') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">User
                                Mutu</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'mutu' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'mutu' ? 'color-color-scheme' : '' }}">Absensi</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('absensi-admin') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                Absensi</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'sarpras' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'sarpras' ? 'color-color-scheme' : '' }}">Sarpras</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('main-admin-barang') }}"
                                class="{{ request()->segment(3) === 'admin' ? 'color-color-scheme' : '' }}">Admin
                                Sarpras</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'perpustakaan' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'perpustakaan' ? 'color-color-scheme' : '' }}">Perpustakaan</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin-perpus-all') }}"
                                class="{{ request()->segment(4) === 'all' ? 'color-color-scheme' : '' }}">Admin
                                Perpustakaan</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'mutu' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'mutu' ? 'color-color-scheme' : '' }}">PPDB</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_ppdb-beranda') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                PPDB</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ request()->segment(2) === 'mutu' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ request()->segment(2) === 'mutu' ? 'color-color-scheme' : '' }}">Kepegawaian</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('kepegawaian-admin') }}"
                                class="{{ request()->segment(4) === 'admin_induk' ? 'color-color-scheme' : '' }}">Admin
                                Kepegawaian</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>
    </ul>
</nav>
