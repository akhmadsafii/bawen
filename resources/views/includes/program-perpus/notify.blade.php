<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>

<script type="text/javascript">
    
function noti(tipe, value) {
    $.toast({
        icon: tipe,
        text: value,
        hideAfter: 5000,
        showConfirmButton: true,
        position: 'top-right',
    });
    return true;
}

function swa(status, message, icon) {
    swal(
        status,
        message,
        icon
    );
    return true;
}

</script>
