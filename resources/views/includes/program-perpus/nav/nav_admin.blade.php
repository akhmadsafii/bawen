
<style type="text/css">

</style>


	<nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between">
	  <a class="navbar-brand " href="{{ route('home-perpus')}}">PERPUSTAKAAN</a>
	  <div class="dropdown d-none d-md-flex float-right">
		    	<a href="javascript:void(0);" class="p-2" data-toggle="dropdown">
		    		<div class="d-md-flex align-items-center">

			    		<p class="mb-0 mt-2"><strong>{{ $profile['nama'] }}</strong></p>


			    		<span class="avatar thumb-sm ml-3">
		                    <img src="{{ $profile["file"] }}" class="rounded-circle" alt="">
		                </span>
		    		</div>

		    	</a>

	            <div class="dropdown-menu dropdown-left animated flipInY">
	            	<a class="dropdown-item" href="{{ route('profil-admin-perpus') }}">Profil Saya</a>
	            	<!--a class="dropdown-item" href="{{ route('peminjaman-admin-perpus') }}">Setting</a-->
	                <div class="dropdown-divider"></div>

	                <a class="dropdown-item" href="{{route('auth.logout')}}">
	                	<i class="fas fa-sign-out-alt"></i>
	                	<strong>Logout</strong>
	                </a>

	            </div>
	        </div>

	  <!-- <div class="collapse navbar-collapse bg-light" id="navbarSupportedContent"> -->

	  <!-- </div> -->
	</nav>



