<style>
    .pace {
        display: none;
    }

    @media (max-width: 960px) {
        .navbar-header {
            float: left;
            width: 10.75rem;
        }

        .navbar-brand {
            padding: 6px;
            justify-content: flex-start;
        }

    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif

<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/point') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>

        <li class="{{ Request::segment(3) == 'siswa' ? 'current-page active' : '' }}">
            <a href="{{ route('point-siswa') }}" class="ripple">
                <i class="fas fa-users"></i>
                <span class="hide-menu">Siswa</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) == 'pesan' ? 'current-page active' : '' }}">
            <a href="{{ url('/program/point/pesan/inbox') }}" class="ripple">
                <i class="fas fa-envelope-open-text"></i>
                <span class="hide-menu">Pesan
                    <span
                        class="badge badge-border badge-border-inverted bg-primary float-right mr-3">{{ $dibaca }}</span>
                </span>
            </a>
        </li>

        <li
            class="menu-item-has-children {{ Request::segment(3) == 'pelanggaran_siswa' || Request::segment(3) == 'histori' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple"><i class="fas fa-grimace"></i> <span
                    class="hide-menu">Pelanggaran</a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('point_pelanggaran_siswa') }}">Input Pelanggaran</a>
                </li>
                <li><a href="{{ route('point_histori-beranda') }}">Histori Pelanggaran</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ Request::segment(3) == 'beasiswa' || Request::segment(3) == 'jenis-beasiswa' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple"><i class="fas fa-hand-holding-usd"></i> <span
                    class="hide-menu">Beasiswa</a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('point_jenis_beasiswa-beranda') }}">Jenis Beasiswa</a>
                </li>
                <li><a href="{{ route('point_beasiswa-beranda') }}">Beasiswa</a>
                </li>
            </ul>
        </li>
        <li class="{{ Request::segment(3) == 'karir' ? 'current-page active' : '' }}">
            <a href="{{ route('point_karir-beranda') }}" class="ripple">
                <i class="fas fa-user-graduate"></i>
                <span class="hide-menu">Karir</span>
            </a>
        </li>
    </ul>
</nav>
