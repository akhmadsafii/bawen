<style>
    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

</style>


<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
                {{-- <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt=""> --}}
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/point') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ Request::segment(3) == 'pelanggaran_siswa' || Request::segment(3) == 'histori' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple"><i class="fas fa-clipboard-list"></i> <span
                    class="hide-menu">Pelanggaran</a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('point_pelanggaran_siswa') }}">Input Pelanggaran</a>
                </li>
                <li><a href="{{ route('point_histori-beranda') }}">Histori Pelanggaran</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
