<style>
    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

</style>
@if (Session::has('message'))
    <script>
        $(function() {
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        })
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <div class="side-user">
            <a class="col-sm-12 media clearfix" href="javascript:void(0);">
                <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                    <div id="profile-avatar" class="rounded-circle"
                        style="background-image: url('{{ session('avatar') }}')"></div>
                    {{-- <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt=""> --}}
                </figure>
                <div class="media-body hide-menu">
                    <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                        class="user-type fs-12">{{ session('role') }}</span>
                </div>
            </a>
        </div>
        <li class="current-page">
            <a href="{{ url('program/kesiswaan') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="{{ route('point_pelanggaran_siswa-beranda') }}"
                class="{{ request()->segment(3) === 'pelanggaran_siswa' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-keyboard"></i>
                <span class="hide-menu">Input Pelanggaran</span>
            </a>
        </li>
        <li>
            <a href="{{ route('kesiswaan_histori-list_point') }}"
                class="{{ request()->segment(3) === 'histori' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-poll"></i>
                <span class="hide-menu">Point Siswa</span>
            </a>
        </li>
    </ul>
</nav>
