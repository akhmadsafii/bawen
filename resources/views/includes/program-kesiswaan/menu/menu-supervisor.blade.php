<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<style>
    .pace {
        display: none;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    h6.media-heading.mr-b-5.text-uppercase.text-info{
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info{
        color: #fff !important;
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/kesiswaan') }}"
                class="{{ empty(request()->segment(3)) ? 'color-color-scheme' : '' }}">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
            <a href="{{ route('kesiswaan_siswa-beranda') }}"
                class="{{ request()->segment(3) == 'siswa' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Data Siswa / Absensi</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'sanksi' || request()->segment(3) == 'histori'  ? 'current-page active' : '' }}">
            <a href="javascript:void(0);"><i class="fas fa-laptop-medical"></i>
                <span class="hide-menu">Pelanggaran</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('kesiswaan_histori-beranda') }}">History Pelanggaran</a>
                </li>
                <li><a href="{{ route('kesiswaan_sanksi-beranda') }}">Sanksi Pelanggaran</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(3) == 'program_kerja' ? 'current-page active' : '' }}">
            <a href="{{ route('kesiswaan-program_kerja') }}"
                class="{{ request()->segment(3) === 'program_kerja' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-file-signature"></i>
                <span class="hide-menu">Program Kerja</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'ekstrakurikuler' ? 'current-page active' : '' }}">
            <a href="{{ route('kesiswaan-anggota_ekskul') }}"
                class="{{ request()->segment(3) === 'ekstrakurikuler' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-volleyball-ball"></i>
                <span class="hide-menu">Ekstrakurikuler</span>
            </a>
        </li>
    </ul>
</nav>
