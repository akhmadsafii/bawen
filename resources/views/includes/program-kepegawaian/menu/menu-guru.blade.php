<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<div class="side-user bg-facebook">
    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
        <div class="media-body hide-menu text-center">
            <h4 class="media-heading mr-b-5 text-uppercase">Tahun Pelajaran : {{ $tahun['tahun_ajaran'] }}</h4>
            <span class="user-type fs-12">Semester {{ $tahun['semester'] }}</span>
        </div>
    </a>
    <div class="clearfix"></div>
</div>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/cbt') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="{{ request()->segment(3) == 'bank_soal' ? 'current-page active' : '' }}">
            <a href="{{ route('cbt-bank_soal') }}" class="ripple">
                <i class="fas fa-th-list"></i>
                <span class="hide-menu">Bank Soal</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'jadwal' ? 'current-page active' : '' }}">
            <a href="{{ route('cbt-jadwal') }}" class="ripple">
                <i class="fas fa-calendar"></i>
                <span class="hide-menu">Jadwal</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'token' ? 'current-page active' : '' }}">
            <a href="{{ route('cbt-token') }}" class="ripple">
                <i class="fas fa-key"></i>
                <span class="hide-menu">Token</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'status_siswa' ? 'current-page active' : '' }}">
            <a href="{{ route('cbt_status_siswa') }}" class="ripple">
                <i class="fas fa-user-check"></i>
                <span class="hide-menu">Status Siswa</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'hasil_ujian' ? 'current-page active' : '' }}">
            <a href="{{ route('cbt_hasil_ujian') }}" class="ripple">
                <i class="fas fa-th-list"></i>
                <span class="hide-menu">Hasil Ujian</span>
            </a>
        </li>
    </ul>
</nav>
