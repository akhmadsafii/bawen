<style>
    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>
<div class="side-user">
    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
        <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
            <div id="profile-avatar" class="rounded-circle" style="background-image: url('{{ session('avatar') }}')">
            </div>
        </figure>
        <div class="media-body hide-menu">
            <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                class="user-type fs-12">{{ session('role') }}</span>
        </div>
    </a>
    <div class="clearfix"></div>
</div>
<div class="side-user bg-facebook">
    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
        <div class="media-body hide-menu text-center">
            <h4 class="media-heading mr-b-5 text-uppercase">Tahun Pelajaran : {{ $tahun['tahun_ajaran'] }}</h4>
            <span class="user-type fs-12">Semester {{ $tahun['semester'] }}</span>
        </div>
    </a>
    <div class="clearfix"></div>
</div>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/kepegawaian') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'jenis_file' || request()->segment(3) == 'tunjangan-potongan' || request()->segment(3) == 'sk' || request()->segment(3) == 'status' || request()->segment(3) == 'golongan' || request()->segment(3) == 'pangkat' || request()->segment(3) == 'jabatan' || request()->segment(3) == 'jenis_jabatan' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-folder-open"></i> <span class="hide-menu">Master</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'pangkat' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-pangkat') }}">Pangkat/Golongan</a>
                </li>
                <li class="{{ request()->segment(3) == 'jabatan' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-jabatan') }}">Jabatan</a>
                </li>
                <li class="{{ request()->segment(3) == 'jenis_jabatan' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-jenis_jabatan') }}">Jenis Jabatan</a>
                </li>
                <li class="{{ request()->segment(3) == 'status' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-status') }}">Status Kepegawaian</a>
                </li>
                <li class="{{ request()->segment(3) == 'golongan' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-golongan') }}">Data Golongan Ijazah</a>
                </li>
                <li class="{{ request()->segment(3) == 'sk' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-sk') }}">Data SK</a>
                </li>
                <li class="{{ request()->segment(3) == 'tunjangan-potongan' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-tunjangan_potongan') }}">Tunjangan & Potongan</a>
                </li>
                <li class="{{ request()->segment(3) == 'jenis_file' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-jenis_file') }}">Jenis File Berkas</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(3) == 'pegawai' ? 'current-page active' : '' }}">
            <a href="{{ route('kepegawaian-pegawai') }}" class="ripple">
                <i class="fas fa-users"></i>
                <span class="hide-menu">Data kepegawaian</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'tunjangan-potongan-tahun' || request()->segment(3) == 'penggajian' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-invoice-dollar"></i> <span class="hide-menu">Penggajian</span>
            </a>
            <ul class="list-unstyled sub-menu">

                <li class="{{ request()->segment(3) == 'tunjangan-potongan-tahun' ? 'current-page active' : '' }}">
                    <a
                        href="{{ route('kepegawaian-tunjangan_potongan_tahun', ['tahun' => session('tahun')]) }}">Setting</a>
                </li>
                <li class="{{ request()->segment(3) == 'penggajian' ? 'current-page active' : '' }}">
                    <a
                        href="{{ route('kepegawaian-tunjangan_penggajian', ['bulan' => strtolower((new \App\Helpers\Help())->getNumberMonthIndo(date('m'))), 'tahun' => date('Y')]) }}">Data
                        Penggajian</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(3) == 'mutasi' ? 'current-page active' : '' }}">
            <a href="{{ route('kepegawaian-mutasi_guru', ['jenis' => 'semua', 'tahun' => session('tahun')]) }}"
                class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Data Mutasi</span>
            </a>
        </li>

        <li class="menu-item-has-children {{ request()->segment(3) == 'whatsapp' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fab fa-whatsapp"></i> <span class="hide-menu">WhatsApp</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(4) == 'kirim-pesan' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-whatsap_create') }}">Kirim Pesan</a>
                </li>
                <li class="{{ request()->segment(3) == 'setting' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-whatsapp_setting') }}">Setting</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'admin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-angle-double-right"></i> <span class="hide-menu">Info lainnya</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-setting_create') }}">Setting</a>
                </li>
                <li class="{{ request()->segment(3) == 'admin' ? 'current-page active' : '' }}">
                    <a href="{{ route('kepegawaian-admin') }}">List User</a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
