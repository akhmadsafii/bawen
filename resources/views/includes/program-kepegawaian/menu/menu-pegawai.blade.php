<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

</style>


<nav class="sidebar-nav">
    <div class="side-user bg-facebook">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <div class="media-body hide-menu text-center">
                <h4 class="media-heading mr-b-5 text-uppercase">Tahun Pelajaran : {{ $tahun['tahun_ajaran'] }}</h4>
                <span class="user-type fs-12">Semester {{ $tahun['semester'] }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
                {{-- <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt=""> --}}
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>

    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/kepegawaian') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li
            class="{{ request()->segment(3) == 'pegawai' && empty(request()->segment(4)) ? 'current-page active' : '' }}">
            <a href="{{ route('kepegawaian-pegawai') }}" class="ripple">
                <i class="fas fa-users"></i>
                <span class="hide-menu">Data kepegawaian</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'detail' ? 'current-page active' : '' }}">
            <a href="{{ route('kepegawaian-pegawai_edit', ['method' => 'biodata', 'k' => (new \App\Helpers\Help())->encode(session('id'))]) }}"
                class="ripple">
                <i class="fas fa-user-alt"></i>
                <span class="hide-menu">Profil</span>
            </a>
        </li>

    </ul>
</nav>
