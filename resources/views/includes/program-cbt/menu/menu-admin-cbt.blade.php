<style>
    @media (max-width: 960px) {
        .navbar-header {
            width: 6.75rem !important;
        }
    }

    #profile-avatar {
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: auto 60px;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    span.mr-b-10.sub-heading-font-family.fw-300.text-white {
        color: #36a7d3 !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

</style>
@if (Session::has('message'))
    <script>
        $(function() {
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        })
    </script>
@endif

<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <div id="profile-avatar" class="rounded-circle"
                    style="background-image: url('{{ session('avatar') }}')"></div>
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <div class="side-user bg-facebook">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <div class="media-body hide-menu text-center">
                <h4 class="media-heading mr-b-5 text-uppercase">Tahun Pelajaran : {{ $tahun['tahun_ajaran'] }}</h4>
                <span class="user-type fs-12">Semester {{ $tahun['semester'] }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/cbt') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'mapel' ||
            request()->segment(3) == 'kelas_siswa' ||
            request()->segment(3) == 'guru' ||
            request()->segment(3) == 'guru_pelajaran'
                ? 'current-page active'
                : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-folder-open"></i> <span class="hide-menu">Master</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'mapel' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-mapel') }}"
                        class="{{ request()->segment(3) == 'mapel' ? 'color-color-scheme' : '' }}">Mapel</a>
                </li>
                <li class="{{ request()->segment(3) == 'kelas_siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('e_learning-kelas_siswa', ['based' => 'siswa']) }}"
                        class="{{ request()->segment(3) == 'kelas_siswa' ? 'color-color-scheme' : '' }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('user-guru') }}"
                        class="{{ request()->segment(3) == 'guru' ? 'color-color-scheme' : '' }}">Guru</a>
                </li>
                <a href="{{ route('e_learning-guru_pelajaran', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                    class="{{ request()->segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Guru
                    Pelajaran</a>
                <a href="{{ route('cbt-pengumuman', ['th' => (new \App\Helpers\Help())->encode(session('id_tahun_ajar')),'rb' => 'all']) }}"
                    class="{{ request()->segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Pengumuman</a>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'jenis' ||
            request()->segment(3) == 'sesi' ||
            request()->segment(3) == 'bank_soal' ||
            request()->segment(3) == 'ruang' ||
            request()->segment(3) == 'jadwal' ||
            request()->segment(3) == 'ruang_kelas' ||
            request()->segment(3) == 'token' ||
            request()->segment(3) == 'nomor_peserta'
                ? 'current-page active'
                : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-signature"></i> <span class="hide-menu">Data Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'jenis' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jenis') }}"
                        class="{{ request()->segment(3) == 'jenis' ? 'color-color-scheme' : '' }}">Jenis Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'sesi' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}"
                        class="{{ request()->segment(3) == 'sesi' ? 'color-color-scheme' : '' }}">Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}"
                        class="{{ request()->segment(3) == 'ruang' ? 'color-color-scheme' : '' }}">Ruang</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang_kelas' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-kelas_ruang') }}"
                        class="{{ request()->segment(3) == 'ruang_kelas' ? 'color-color-scheme' : '' }}">Atur Ruang &
                        Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'bank_soal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-bank_soal') }}"
                        class="{{ request()->segment(3) == 'bank_soal' ? 'color-color-scheme' : '' }}">Bank Soal</a>
                </li>
                <li class="{{ request()->segment(3) == 'bank_soal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_nomor_peserta') }}"
                        class="{{ request()->segment(3) == 'nomor_peserta' ? 'color-color-scheme' : '' }}">Atur Nomor
                        Peserta</a>
                </li>
                <li class="{{ request()->segment(3) == 'jadwal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jadwal') }}"
                        class="{{ request()->segment(3) == 'jadwal' ? 'color-color-scheme' : '' }}">Jadwal</a>
                </li>
                <li class="{{ request()->segment(3) == 'token' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-token') }}"
                        class="{{ request()->segment(3) == 'token' ? 'color-color-scheme' : '' }}">Token</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'cetak' ||
            request()->segment(3) == 'status_siswa' ||
            request()->segment(3) == 'hasil_ujian' ||
            request()->segment(3) == 'analisa' ||
            request()->segment(3) == 'rekap'
                ? 'current-page active'
                : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-contract"></i> <span class="hide-menu">Pelaksanaan Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_cetak') }}"
                        class="{{ request()->segment(3) == 'cetak' ? 'color-color-scheme' : '' }}">Cetak</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_status_siswa') }}"
                        class="{{ request()->segment(3) == 'status_siswa' ? 'color-color-scheme' : '' }}">Status
                        Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_hasil_ujian') }}"
                        class="{{ request()->segment(3) == 'hasil_ujian' ? 'color-color-scheme' : '' }}">Hasil
                        Ujian</a>
                </li>
                {{-- <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_analisa_soal') }}">Analisis Soal</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt_rekap_nilai') }}">Rekap Nilai Ujian</a>
                </li> --}}
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-users-cog"></i> <span class="hide-menu">User Management</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-admin') }}">Administrator</a>
                </li>
                {{-- <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-siswa') }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-guru') }}">Guru</a>
                </li> --}}
            </ul>
        </li>

    </ul>
</nav>
