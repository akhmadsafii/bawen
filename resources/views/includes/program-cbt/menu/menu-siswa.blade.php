<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/cbt') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'mapel' || request()->segment(3) == 'siswa' || request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-folder-open"></i> <span class="hide-menu">Master</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'mapel' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-mapel') }}">Mapel</a>
                </li>
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('user-siswa') }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('user-guru') }}">Guru</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'jenis' || request()->segment(3) == 'sesi' || request()->segment(3) == 'bank_soal' || request()->segment(3) == 'ruang' || request()->segment(3) == 'jadwal' || request()->segment(3) == 'ruang_kelas' || request()->segment(3) == 'token' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-signature"></i> <span class="hide-menu">Data Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'jenis' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jenis') }}">Jenis Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'sesi' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}">Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}">Ruang</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang_kelas' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-kelas_ruang') }}">Atur Ruang & Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'bank_soal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-bank_soal') }}">Bank Soal</a>
                </li>
                <li class="{{ request()->segment(3) == 'jadwal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jadwal') }}">Jadwal</a>
                </li>
                <li class="{{ request()->segment(3) == 'token' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-token') }}">Token</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-contract"></i> <span class="hide-menu">Pelaksanaan Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jenis') }}">Cetak</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}">Status Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}">Hasil Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}">Analisis Soal</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}">Rekap Nilai Ujian</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-users-cog"></i> <span class="hide-menu">User Management</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-admin') }}">Administrator</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-siswa') }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-guru') }}">Guru</a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
