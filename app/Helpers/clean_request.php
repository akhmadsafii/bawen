<?php
namespace App\Helpers;
class Clean_request{
    /**
     * clean number character
     */
    public static function sanitize_number($number) {
        return filter_var($number, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * clean desimal character
     */

    public static function sanitize_decimal($decimal) {
        return filter_var($decimal, FILTER_SANITIZE_NUMBER_FLOAT);
    }

    /**
     * clean string character
     */

    public static function sanitize_string($string) {
        $string = trim(htmlspecialchars(strip_tags($string)));
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    /**
     * clean html character
     */

    public static function sanitize_html($string) {
        $string = strip_tags($string, '<a><strong><em><hr><br><p><u><ul><ol><li><dl><dt><dd><table><thead><tr><th><tbody><td><tfoot>');
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    /**
     * clean url character
     */
    public static function sanitize_url($url) {
        return filter_var($url, FILTER_SANITIZE_URL);
    }

    /**
     * clean slug character
     */
    public static function sanitize_slug($string) {
        $string = str_slug($string);
        return filter_var($string, FILTER_SANITIZE_URL);
    }

    /**
     * clean email character
     */
    public static function sanitize_email($string) {
        return filter_var($string, FILTER_SANITIZE_EMAIL);
    }
}
