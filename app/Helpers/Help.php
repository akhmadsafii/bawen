<?php


namespace App\Helpers;

use App\ApiService\Raport\PredikatApi;
use Carbon\Carbon;
use Hashids\Hashids;
use Terbilang;

class Help
{
    private $hash;

    public function __construct()
    {
        $hash = new Hashids();
    }

    public static function status($param)
    {
        switch ($param) {
            case 1:
                return 'Aktif';
                break;
            case 2:
                return 'Tidak Aktif';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function gender($param)
    {
        switch ($param) {
            case 1:
                return 'Laki-laki';
                break;
            case 2:
                return 'Perempuan';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function nama_angka($param)
    {
        switch ($param) {
            case 1:
                return 'Satu';
                break;
            case 2:
                return 'Dua';
                break;
            case 3:
                return 'Tiga';
                break;
            case 4:
                return 'Empat';
                break;
            case 5:
                return 'Lima';
                break;
            case 6:
                return 'Enam';
                break;
            case 7:
                return 'Tujuh';
                break;
            case 8:
                return 'Delapan';
                break;
            case 9:
                return 'Sembilan';
                break;
            case 10:
                return 'Sepuluh';
                break;
            case 11:
                return 'Sebelas';
                break;
            case 12:
                return 'Dua belas';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function getStNikah($param)
    {

        switch ($param) {
            case 1:
                return 'Belum Menikah';
                break;
            case 2:
                return 'Menikah';
                break;
            case 3:
                return 'Janda';
                break;
            case 4:
                return 'Duda';
                break;

            default:
                return 'Error';
                break;
        }
    }

    public static function getNumberMonthIndo($month)
    {
        switch ($month) {
            case 1:
                return 'Januari';
                break;
            case 2:
                return 'Februari';
                break;
            case 3:
                return 'Maret';
                break;
            case 4:
                return 'April';
                break;
            case 5:
                return 'Mei';
                break;
            case 6:
                return 'Juni';
                break;
            case 7:
                return 'Juli';
                break;
            case 8:
                return 'Agustus';
                break;
            case 9:
                return 'September';
                break;
            case 10:
                return 'Oktober';
                break;
            case 11:
                return 'November';
                break;
            case 12:
                return 'Desember';
                break;
            default:
                return 0;
                break;
        }
    }

    public static function getMonthToIndo($number)
    {
        switch ($number) {
            case "januari":
                return "01";
                break;
            case "februari":
                return "02";
                break;
            case "maret":
                return "03";
                break;
            case "april":
                return "04";
                break;
            case "mei":
                return "05";
                break;
            case "juni":
                return "06";
                break;
            case "juli":
                return "07";
                break;
            case "agustus":
                return "08";
                break;
            case "september":
                return "09";
                break;
            case "oktober":
                return "10";
                break;
            case "november":
                return "11";
                break;
            case "desember":
                return "12";
                break;
            default:
                return 0;
                break;
        }
    }

    public static function getKelas($param)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($param > 0) {
            foreach ($map as $roman => $int) {
                if ($param >= $int) {
                    $param -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public static function getTanggal($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F Y');
        return $value;
    }

    public static function timeHuman($param)
    {
        $value = Carbon::parse($param)->diffForHumans();
        return $value;
    }

    public static function getMonthYear($param)
    {
        $value = Carbon::parse($param)->translatedFormat('F Y');
        return $value;
    }

    public static function getDayMonth($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F');
        return $value;
    }

    public static function getDayOnly($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d');
        return $value;
    }

    public static function getMonthOnly($param)
    {
        $value = Carbon::parse($param)->translatedFormat('F');
        return $value;
    }

    public static function getHoursMinute($param)
    {
        $value = Carbon::parse($param)->translatedFormat('d F Y  H:i');
        return $value;
    }

    public static function getDay($param)
    {
        $value = Carbon::parse($param)->translatedFormat('l, d F Y');
        return $value;
    }

    public static function getHariOnly($param)
    {
        $value = Carbon::parse($param)->translatedFormat('l');
        return $value;
    }

    public static function getTanggalLengkap($param)
    {
        $value = Carbon::parse($param)->translatedFormat('l, d F Y  H:i');
        return $value;
    }

    public static function getTime($param)
    {
        $value = Carbon::parse($param)->translatedFormat('H:i:s');
        return $value;
    }

    public static function check_and_make_dir($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
    }

    public static function pagination($data, $request)
    {
        return [
            "totalCount" => $data->total(),
            "pageSize" => $data->perPage(),
            "currentPage" => $data->currentPage(),
            "totalPages" => $data->lastPage(),
            "previousPage" => is_null($data->previousPageUrl()) ? false : $data->appends($request->all())->previousPageUrl(),
            "nextPage" => is_null($data->nextPageUrl()) ? false : $data->appends($request->all())->nextPageUrl(),
        ];
    }

    public static function encode($id)
    {
        $hash = new Hashids();
        $value = $hash->encode($id);
        return $value;
    }

    public static function decode($id)
    {
        $hash = new Hashids();
        $value = $hash->decode($id)[0];
        return $value;
    }

    public static function alphabet($number)
    {
        $number = intval($number);
        if ($number <= 0) {
            return '';
        }
        $alphabet = '';
        while ($number != 0) {
            $p = ($number - 1) % 26;
            $number = intval(($number - $p) / 26);
            $alphabet = chr(65 + $p) . $alphabet;
        }
        return $alphabet;
    }

    public static function no_img_base64()
    {
        return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAM00lEQVR42u1dDYwdVRWeN2/e7779rf1Ztl1taWs3bZFt6Y/VGhJqW4qW0igu6AYTChUjJCpbs8SGNEioGGurxmRtg4lEY1GMkRKLLUIoIia1KIikZCuCQIsVKG3plr7d6jnsvDK8vjf33J+ZOzN7JzmhQN83Z+535t5zz/3uHcsyl7lkr2XLLk1VzOAlD490IxU3NHjRwqPczPZYyuAlB491M7vaDF5y8Cg3S3vMNnjJwWPdLF1tBi85eKzxBW/geCwtOs4YvGjhUW7m1LCUwYs/HvVmGY+pcN7gRQCPOsZU31DG+UwNM3ga8HjId0zjJo9830KRZ2qhaswyZEWQ/JoYVfNKQ37yyD9XKTwPx1NO9M4tTeMmj3zbj3zbkJ9IvHOVQkoA1CsnTgXrB9sDdhhsGOx/1ZZKpc6zWn+PagnHG3bbEtu0H347LUDy037kp3zIR+J3go2McbLCwBuxbfuXxWJhuiLyvZXC2msFjPXjXrCThqzQ8U6m0+kvKqwY1l8o8iF/gyFLO94GyRziXADw4vQasiKD1yuRQAqRP9V0+5HCO+lyIjJ7EKrj7DRkRQ5vZ1jkTzXZfiTxRur1AqrrBv0E559zHGftxIkTmk1RRg4P2xDa8ir4X/8gBE5/GBXDPSzy29paPmAqcsrxmglBsCeMJeLDft0WvvmGrMDwVjMC4HAY+gDf8q7p9gPFKxLKxoGLQ3wTFkNW4Hi+eUAYyiDfbNWQFTgeKwACl4X5TlUMWYHj+U4dw9AEsuaphqxg8XzrBmFoAllFiriTNQ9sAGwQrIwGz3fItu3t2Wx2YQT8CzQHI6z+WqwKVVzJxwx7ByvHgUC4J5NxGjT2JIHlYK6P/ppAQnkyruTv4yjH7nN/oyPYA8nBqsivLwkj1Kbj2O3vsPhr8ds1BXtQORhNE0hYmIjjmC+6EDMv7OcNKAejawIJCxJxy84HJFbhBsJ+XoJ/vO3HrQkUCoAIT80GLfEl2MGwn1dxDiakCeQOgIjPy8uW+Hp+OeznDSAH49YEcgVADHbElCXEHOWwnzeAHIxbE0gOgDhU5LDII6HkGQz7eQn+8bYftyyM5EBcyrFY4ZOQcQ2E/byKczAhTSDTgTjV4rG8K6Hhm6fhebXnYKGuR4eBh+VdAfK3awp2rTkYJQBitwrn1vb3cZD/bilYU7Bry8GYDqhcj9bQuEX3raa8+UWNPZ2WHIzpgMr1aM3DyHnLwe6fBzSN+dpzMKYDGjSBSMRDYENgb4L9FKw9hkUoowkUaIyPg52q8fAvgI1POPmh5mBMBzRoAmeBve7TAI+AOQkmP7QcjOmABk0gdvH/IoyBW40mMHmawBLYAeo0zT05I6kC0zGnCcQufbfFt6t2KCICziDwxpwm8B5LYEu1badeKpVK7QmUlo8pTeAmS24//bmkMCHkB5mDRU4TuM5Sc5jC1hDIagPbDPYX1za7/81oAgUbd5VbgVN1kkZvgOR3uTWI8+oS4Mtsownkb9z5FvEAKtu2d4M9QhiWhiyGgleQ/GVgx3yC81gm46wymkB64+IZN0do5Kf2T5gwvqWpqbEd/vwiYT3/xepKoST56/x6KQ9JZZiWfsVoAtmNOw7sILHbP9TYWOrwiDouqVMerlsplCAf//vdAsPSNrC0DPlJ1gQWwJ4gkv+fQiE/qwbeNcSkcasE+ejn/RI5yYNgjTIJaRI1gTju/JpI/kl42z/qg3c3MQh6BcifCPZnBQnp02CdogmplUBN4A+I5Jcdx1lNCCZK1XAom80s5CB/DmsdglNajnnOIsGENFGawD7qVA8SqRuIzrdant0+9fCwUggJ5AUEvBVgb1nqD4nEmcnVAmQlRhOIY/ZZIvmbOJ3HN/YEu1xsPzpjxrS8D96XWPWIKrxdYA9adG0hPv9GTrISoQm8FOwdCvmo2xd0fm11gNV5U2tVCnEo+S7f2oP9/YsvnpuD2QnOMn5k8SmM74XcJj9WNIFz/IonVeQ/MHt2V17C+TuI3bS3Uoji0N9wkD8MPdQtNfy7ldLDeeoaj2NNI+mawA6wfxPJf7K9fWKTpPP4m98SxuhKpRBFJ/s5yD/uSUxr+fcZF5uEB0Hwz3w+N7elpXkc/DuWw+8E2wt2COw4JaCirAlscqdAFPIPNjc3TVJRPm1tbWlDPEKC9pJrVLJezuWy8wn+LQE7ypFAYsCMWPwbVSpimJtVndWscj0a568PExv3sMIPIr2Lkc/nZwPuW6qOcIeAOtDQ0PBBDv+mgz2vaPZA6ZlOgI/fK5UaLoiCJhAduJfo/HF4qxYEsf4OXfWVPGOyD/kP4BqEgH9Y6v5jCOR78XDY2CChiVCiCbyL6PyZTMa5PGDxxTclyd/W3X1RTsK/PNhODR+XQI3CTF7yVWgCbyI6f9Zx0teFoLzBf/5KoHEx079ZhX8w3esGvFMaviyCvcFqgo/KNIFXspKZCgY07m0hyq5QYfwMR+Meh57p0yr8g2Hok6gR0PxZmfUE8qU1gYtZy7OebvWHGjR3F4K9QfEvk8ksV0T+ChzmIvCNIsyDenxkYdKawBl+054q8u+XHFNlih7LLcK3jcHHvV1dH87L+AeJ7UcE3/z/gt3nrpmsAVsKv10CwbQSes0bsfoI9pRAMJ0G+1gN8pVoAgeJ5D82ZUpHSbPato/4Zn1H1D+YLnZUFEsc5O8CW1ktIKn3vICPSqpNbsBQg+Ao+DIjCE0ghfxnW1tbxkdEav1zYrfaI+IfLg5xkL/fHT5Fn7eHk48nFy26JKNaE8iqoL1SLBanRUhnjyqfA4Qx9W2wi3j8g676ag7y76w3Xyc+7xVu187FBwwl61VrAv3G1GO5XK47atu1CoX8heDbUQJZWJNvo/jX1tY6DoL9VQL5uNz8ecnnFSLftdcLhcIklYqgejc7DRn1sqju1YMLp2hlwpv6kLtc7IsHb9btxDdfJ/kV/7YEEgCem5yF7vCaqG/UBNK+Sny2zX54M2dOL+BQR+z2dZOP9qbF8f0D7ptBw349Rrt0f0J8xs/Ww8NgJyZ8TgTIr9g6avuVLT6lzJaYbdHOWQz1r/XeJ9vn1Dl38GHC7xeHRP5peAGvBS5eY+Q4+6lt+DIH+b9YvHhBNoZbtFG4QtmhNNjc3DTei9XePqlEIGdXWORDb/Qpd3jrZ+QkZ1kJbuXaTST/D52dk4sx3p+PlbIzBPHK7zzVTNy4cRmBmJVhko+GAhvwjaXDXE1pyz7KGAMR9zWw1piSX7nW03Ic+y4P3h2E8m46TPI9m0pYimVSxXNydQ1dw/p2nPHuC4H8K+rgsWY5e6kv1RZDvjBenyby8foE4/fPUAMAD1d+wZAvhLdGE/l4TWFgvEYeV2HcW4UFHkM+ty0NgPwhIH8VIccpMnCGK1VOP0lYRTkCOV66B2XMhnwurCWayK8sfLFk5VlfTWCFfNfg3s5SlgDEkP+eoZhDMfmXc8xuJrB6AKYmsCoA0u5fwr3zqP49ZshnbnS9URP5eC1g+HeEKQmrCoDqmzW5U43fg71aT3I1lmcPKONSRP5KgbrG9Qz//kQNANsSvBJ+kDNe32JUSJ+SJP+UIPl4/YwR7NuYmkBDPvO6jNWTuBq+sMnHRa43GMH5OYom0JDPlpi9wxhGNgmSv0LCv2tZw1w2m5lM0QQa8tnXo4wcAtcDekIkH//e04ye6e9WENdYIx9/B5n+9QoTSCR/uaR/NxAS3FsM+YrwOjraG70iU83kf8jyHHJVh3xUPDcb8hXipdP2tyNA/vu+tOIzLO0w5CvGa2goTuYtjHnsbdxIKulfC9hjBPLx7e805AeDd5Mm8udannOXGQnpNwz5wZ5H8ITFt1awRsI/PIBio3d2wSD/WWv0+B5DfoB4s6hDgUvQEZhFbCyVGqZw+IfH4ONRMK9wlLOx659vyA8HD6uDZyy+tYdhdwzHotFVYN3W6PcUplmjR9qhsOR2rDlYNaT6DPJHKsIUVeQ7hnwm3nURWsjqM+TrwVtf/baGTD5q/29VmfSlawSAIZ8BZY3uxQubfDyyZ20Q5DuGfL4LiOkC+2uI5D8HtlDgmf01gVVfnTLkc+DhIdi4eUblCaY1yMe3/jawrAj5JE2gIV+6YohSbTxe/oRC8nGKt92dNfD6KKQJNOTL4+HHpL4M9jcJ8g+6SZ7oljxbVhNoyFeDh0e24FHzW63R84XxoGnUEFQ0lnjk3PO2bT8O9mPHcb5QLBY7Jf2jnRNoZGFa8fA8o2wA/tHPCTTkJw6P+5xAQ37yyOc6J1BVxdBR4LzBk8fj/nawbKQ5ipw3eOrwAiU/XaOrSRm8+ONRy8XvM4OXHDxKUSHtm10avNji8VSUbEXOG7yI4FHGmfolRYMXazzKzVIq6gYGL3p4XDc0eMnA+z8gGW/JClti9AAAAABJRU5ErkJggg==";
    }

    public static function hour_min($minutes)
    { // Total
        if ($minutes <= 0) return '00 Hours 00 Minutes';
        else
            return sprintf("%02d", floor($minutes / 60)) . ' Hours ' . sprintf("%02d", str_pad(($minutes % 60), 2, "0", STR_PAD_LEFT)) . " Minutes";
    }


    public static function terbilang($x)
    {
        $terbilang = Terbilang::make($x, ' rupiah');
        return $terbilang;
    }

    public static function getPredikat($value)
    {
        $newPredikat = new PredikatApi();
        $predikat = $newPredikat->get_range($value);
        $predikat = $predikat['body']['data'];
        return $predikat['predikat'];
    }
}
