<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\TerminableInterface;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class PageAnalytics  implements TerminableInterface
{

    protected $startTime;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->startTime = round(microtime(true) - LARAVEL_START, 3);
        $current = Carbon::now();
        Log::info("Load Page Time:".$this->startTime.' - '. $current->toDateTimeString()."-".$request->fullUrl());
        return $next($request);
    }

    public function terminate(Request $request, Response $response)
    {

    }
}
