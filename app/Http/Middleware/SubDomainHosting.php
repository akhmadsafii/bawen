<?php

namespace App\Http\Middleware;

use Closure;

class SubDomainHosting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $filteredUrl = 'localhost:8000/';
        $visitedUrl = parse_url(url()->full())['host'];
        if ($filteredUrl === $visitedUrl) {
        // if (parse_url(url()->full())['host'] === 'https://www.elearning.smkn1bawen.sch.id') {
            return redirect('https://localhost:8000/program/e_learning');
            // return redirect('https://www.smkn1bawen.sch.id/program/e_learning');
        }
        // abort(403);
    }
}
