<?php

namespace App\Http\Controllers\Pengunjung;

use App\ApiService\Master\EkstrakurikulerApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Raport\ConfigTemplateApi;
use App\ApiService\Raport\LihatPtsApi;
use App\ApiService\Raport\LihatRaportApi;
use App\ApiService\Raport\PredikatApi;
use App\ApiService\Raport\TemplateSampulApi;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Http\Request;

class RaportController extends Controller
{
    private $rombelApi;
    private $kelasSiswaApi;
    private $templateApi;
    private $predikatApi;
    private $lihatApi;
    private $sampulApi;
    private $lihatPtsApi;
    private $ekstraApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->templateApi = new ConfigTemplateApi();
        $this->predikatApi = new PredikatApi();
        $this->lihatApi = new LihatRaportApi();
        $this->lihatPtsApi = new LihatPtsApi();
        $this->sampulApi = new TemplateSampulApi();
        $this->ekstraApi = new EkstrakurikulerApi();
    }

    public function download($id_kelas_siswa, $id_tahun_ajar)
    {
        // $id_kelas_siswa = 1;
        // $id_tahun_ajar = 1;

        $kelas_siswa = $this->kelasSiswaApi->get_by_id($id_kelas_siswa);
        // dd($kelas_siswa);
        $kelas_siswa = $kelas_siswa['body']['data'];
        $ekstra = $this->ekstraApi->get_by_id_sekolah($kelas_siswa['id_sekolah']);
        // dd($ekstra);
        $ekstra = $ekstra['body']['data'];
        // dd(session()->all());
        $rombel = $this->rombelApi->get_by_id($kelas_siswa['id_rombel']);
        $rombel = $rombel['body']['data'];
        $template = $this->templateApi->by_tahun_jurusan($kelas_siswa['id_jurusan'], $id_tahun_ajar);
        // dd($template);
        $template = $template['body']['data'];
        if ($template == null) {
            $pesan = array(
                'message' => "Template Raport dengan tahun ajar ini belum tersedia",
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $predikat = $this->predikatApi->get_by_id_sekolah($kelas_siswa['id_sekolah']);
        $predikat = $predikat['body']['data'];
        // dd($predikat);
        $kop = $this->sampulApi->get_by_S4($id_kelas_siswa);
        if ($template['jenis'] == 'pas') {
            if ($template['template'] == 'k16') {
                $lihat = $this->lihatApi->k16_kelas_siswa($id_kelas_siswa, $id_tahun_ajar);
                // dd($lihat);
            } elseif ($template['template'] == 'manual') {
                $lihat = $this->lihatApi->manual_kelas_siswa($id_kelas_siswa, $id_tahun_ajar);
            } else {
                $lihat = $this->lihatApi->kd_kelas_siswa($id_kelas_siswa, $id_tahun_ajar);
                if ($lihat['code'] != 200) {
                    $pesan = array(
                        'message' => 'Pastikan guru telah mengisi nilai kd',
                        'icon' => 'error',
                        'status' => 'gagal'
                    );
                    return redirect()->back()->with('error_api', $pesan);
                }
            }
        } else {
            if ($template['template'] == 'k16') {
                $lihat = $this->lihatPtsApi->k16_kelas_siswa($id_kelas_siswa, $id_tahun_ajar);
                // dd($lihat);
            } elseif ($template['template'] == 'manual' || $template['template'] == 'sd') {
                $lihat = $this->lihatPtsApi->manual_kelas_siswa($id_kelas_siswa, $id_tahun_ajar);
            } elseif ($template['template'] == 'smp' || $template['template'] == 'smk') {
                $lihat = $this->lihatPtsApi->kd_kelas_siswa_v2($id_kelas_siswa, $id_tahun_ajar);
            } else {
                $lihat = $this->lihatPtsApi->kd_kelas_siswa_v1($id_kelas_siswa, $id_tahun_ajar);
            }
        }
        $lihat = $lihat['body']['data'];
        if ($lihat['nilai_absensi'] == null) {
            $pesan = array(
                'message' => 'Harap mengisi absensi di siswa di wali kelas dengan tahun ajar ini',
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        // sd  pas
        $view = 'content.raport.guru.cetak_raport.' . $template['template'] . '.v_print_' . $template['jenis'];

        session()->put('title', str_replace(' ', '-', $lihat['nama']));
        $kop = $kop['body']['data'];
        // return view($view)->with([
        //     'lihat' => $lihat, 'kop' => $kop, 'predikat' => $predikat, 'ekstra' => $ekstra
        // ]);
        $pdf = PDF::loadview($view, [
            'lihat' => $lihat, 'kop' => $kop, 'predikat' => $predikat, 'ekstra' => $ekstra
        ]);
        return $pdf->download($kelas_siswa['nis'] . '.pdf');
        // return $pdf->stream();
    }

    // $siswa = $this->kelasSiswaApi->get_by_id(session('id_kelas_siswa'));
    // $profile = $siswa['body']['data'];
    // // dd($profile);
    // $pdf = PDF::loadview('content.absensi.pengunjung.v_qr_code', ['profile'=>$profile]);
    // return $pdf->download($profile['nama'].'_'.$profile['akun_absen'].'.pdf');
}
