<?php

namespace App\Http\Controllers\Pengunjung;

use App\ApiService\Absensi\AkunApi;
use App\ApiService\Absensi\HadirApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class AbsensiController extends Controller
{
    private $hadirApi;
    private $kelasSiswaApi;
    private $akunApi;
    private $tahunApi;

    public function __construct()
    {
        $this->hadirApi = new HadirApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->akunApi = new AkunApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        $informasi = $this->hadirApi->form_sekolah(session('id_sekolah'));
        // dd($informasi);
        $informasi = $informasi['body']['data'];
        // dd($informasi);
        session()->forget('title');
        return view('content.absensi.pengunjung.v_absensi')->with(['informasi' => $informasi]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $tahun_aktif = $this->tahunApi->get_aktif(session('id_sekolah'));
        // dd($tahun_aktif);
        $tahun_aktif = $tahun_aktif['body']['data'];

        $data = array(
            'kode' => $request['kode'],
            'id_ta_sm' => $tahun_aktif['id'],
            'id_sekolah' => session('id_sekolah')
        );

        $result = $this->hadirApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $profil = $this->akunApi->get_by_kode(json_encode($data));
            // dd($profil);
            $profil = $profil['body']['data'];
            $html = '';
            if (!empty($profil)) {
                $html .= '<div class="alert alert-icon alert-success border-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close"><span aria-hidden="true">×</span>
                </button> <i class="material-icons list-icon">check_circle</i>
                <strong>Absensi Sukses!</strong> '.$result['body']['message'].'.
            </div>
            <table class="table table-stripped">
                <tr class="bg-success">
                    <th colspan="6"><i class="fas fa-user-circle"></i> Profile User</th>
                </tr>
                <tr>
                    <td rowspan="5" class="text-center vertical-middle"><img src="'.$profil['avatar'].'" alt="" height="91"></td>
                </tr>
                <tr>
                    <th width="150">Nama</th>
                    <td colspan="3">'.strtoupper($profil['nama']).'</td>
                </tr>
                <tr>
                    <th width="150">Kode Akun</th>
                    <td colspan="3">'.$profil['kode'].'</td>
                </tr>

                <tr>
                    <th width="150">Posisi</th>
                    <td colspan="3">'.$profil['role'].'</td>
                </tr>
                <tr>
                    <th width="150">Sekolah</th>
                    <td colspan="3">'.$profil['sekolah'].'</td>
                </tr>
            </table>';
            }
            return response()->json([
                'status' => 'berhasil',
                'profile' => $html,
            ]);
        } else {
            $html = '<div class="alert alert-icon alert-danger border-danger alert-dismissible fade show"
            role="alert">
                <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close"><span aria-hidden="true">×</span>
                </button> <i class="material-icons list-icon">cancel</i>
                <strong>Absensi Gagal!</strong> '.$result['body']['message'].'
            </div>
            <div class="w-100 text-center">
                <a href="" class="text-info"><i class="fas fa-redo-alt"></i> Ulangi lagi</a>
            </div>';
            return response()->json([
                'status' => 'gagal',
                'profile' => $html,
            ]);
        }
    }

    public function download_pdf()
    {
        $siswa = $this->kelasSiswaApi->get_by_id(session('id_kelas_siswa'));
        $profile = $siswa['body']['data'];
        // dd($profile);
        $pdf = PDF::loadview('content.absensi.pengunjung.v_qr_code', ['profile'=>$profile]);
        return $pdf->download($profile['nama'].'_'.$profile['akun_absen'].'.pdf');
    }

    private function get_data_profile_code($data)
    {
        $html = '';
        if (!empty($data)) {
            $html .= '';
        } else {
            # code...
        }
    }
}
