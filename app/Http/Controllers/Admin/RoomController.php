<?php

namespace App\Http\Controllers\Admin;

use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class RoomController extends Controller
{
    private $roomApi;
    private $guruApi;
    private $mapelApi;
    private $jurusanApi;
    private $tahunApi;

    public function __construct()
    {
        $this->roomApi = new RoomApi();
        $this->mapelApi = new MapelApi();
        $this->guruApi = new GuruApi();
        $this->jurusanApi = new JurusanApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Room');

        $jurusan = $this->jurusanApi->menu();
        // dd($jurusan);
        $jurusan = $jurusan['body']['data'];
        $mapel = $this->mapelApi->get_by_sekolah();
        // dd($mapel);
        $mapel = $mapel['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        if ($_GET['rb'] == 'all') {
            $room = $this->roomApi->get_by_sekolah();
        } else {
            $room = $this->roomApi->by_rombel_ta_sm(Help::decode($_GET['rb']), Help::decode($_GET['th']));
        }
        // dd($room);
        $room = $room['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($room)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('guru', function ($row) {
                return '<b>' . $row['guru'] . '</b><p class="m-0">NIP. ' . $row['nip'] . '</p>';
            });
            $table->editColumn('rombel', function ($row) {
                return '<b>' . $row['rombel'] . '</b><p class="m-0">Kelas. ' . $row['kelas'] . ', Jurusan. ' . $row['jurusan'] . '</p>';
            });
            $table->editColumn('status', function ($row) {
                $checked = '';
                if ($row['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                <input type="checkbox" ' . $checked . '
                    class="room_check" data-id="' . $row['id'] . '">
                <span class="slider round"></span>
            </label>';
            });
            $table->rawColumns(['action', 'guru', 'rombel', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }

        return view('content.admin.learning.v_room')->with(["mapel" => $mapel, "jurusan" => $jurusan, 'tahun' => $tahun]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_guru' => $request->id_guru,
            'id_mapel' => $request->id_mapel,
            'id_rombel' => Help::decode($request->id_rombel),
            'id_ta_sm' => Help::decode($request->id_ta_sm),
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->roomApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $room  = $this->roomApi->get_by_id($request['id']);
        // dd($room);
        $result = $room['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'id_guru' => $request->id_guru,
            'id_mapel' => $request->id_mapel,
            'id_rombel' => $request->id_rombel,
            'id_ta_sm' => Help::decode($request->id_ta_sm),
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->roomApi->update_info(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->roomApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function updateNonAktif(Request $request)
    {
        $update = $this->roomApi->update_status_nonAktif($request['id']);
        return redirect()->back()->with('success', $update['body']['message']);
    }

    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'value' => $request['value']
        );
        $update = $this->roomApi->update_status_custom(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_kelas(Request $request)
    {
        $rombel = $this->roomApi->get_rombel_by_kelas($request['id_kelas']);
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        $html = '';
        if (empty($rombel)) {
            $html .= '<tr><td colspan="5" class="text-center">Data untuk saat ini belum tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($rombel as $rmb) {
                $html .= '
                <tr>
                    <td>' . $no++ . '</td>
                    <td>' . $rmb['jurusan'] . '</td>
                    <td>' . $rmb['kelas'] . '</td>
                    <td>' . $rmb['nama'] . '</td>
                    <td>
                        <button data-toggle="collapse" class="btn btn-success btn-sm" data-target="#room' . $rmb['id'] . '"><i
                                class="fas fa-eye"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse" id="room' . $rmb['id'] . '">
                            <button class="btn btn-info btn-sm my-3 pull-right"
                                onclick="tambahRoom(' . $rmb['id'] . ')"><i
                                    class="fas fa-plus-circle"></i>
                                Tambah Room</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-success">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Guru</th>
                                        <th class="text-center">Mapel</th>
                                        <th class="text-center">Rombel</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_room' . $rmb['id'] . '">';
                if (!empty($rmb['rooms'])) {
                    $nomer = 1;
                    foreach ($rmb['rooms'] as $room) {
                        $status = '<i class="fas fa-lock-open"></i>';
                        $kelas = 'danger';
                        if ($room['status_kode'] != 1) {
                            $status = '<i class="fas fa-lock"></i>';
                            $kelas = 'info';
                        }
                        $html .= '
                                        <tr>
                                            <td>' . $nomer++ . '</td>
                                            <td>' . $room['guru'] . '</td>
                                            <td>' . $room['mapel'] . '</td>
                                            <td>' . $room['rombel'] . '</td>
                                            <td class="text-center"><a href="javascript:void(0)" class="open_key text-' . $kelas . '" data-id="' . $room['id'] . '" data-rombel="' . $room['id_rombel'] . '" data-value="' . $room['status_kode'] . '">' . $status . '</a></td>
                                            <td>
                                                <a href="javacript:void(0)" data-id="' . $room['id'] . '" data-rombel="' . $room['id_rombel'] . '" class="edit btn btn-purple btn-sm"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                                <a href="javacript:void(0)" data-id="' . $room['id'] . '" data-rombel="' . $room['id_rombel'] . '" class="delete btn btn-danger btn-sm"><i
                                                    class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        ';
                    }
                } else {
                    $html .= '<tr><td colspan="6" class="text-center">Data Room untuk saat ini tidak tersedia</td></tr>';
                }

                $html .= '</tbody></table></div></td></tr>';
            }
        }
        return response()->json($html);
    }
}
