<?php

namespace App\Http\Controllers\Admin;

use App\ApiService\Master\SekolahApi;
use App\ApiService\User\AdminApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class AdminSekolahController extends Controller
{
    private $sekolahApi;
    private $profileApi;
    private $adminApi;

    public function __construct()
    {
        $this->sekolahApi = new SekolahApi();
        $this->adminApi = new AdminApi();
        $this->profileApi = new ProfileApi();
    }

    public function index()
    {
        Session::put('title', 'Data Sekolah');
        $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
        $sekolah = $sekolah['body']['data'];
        return view('content.admin.setting.v_sekolah')->with(['template' => session('template'), 'sekolah' => $sekolah]);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'npsn' => $request->npsn,
            'jenjang' => $request->jenjang,
            'nama' => $request->nama,
            'status_sekolah' => $request->status_sekolah,
            'alamat' => $request->alamat,
            'dusun' => $request->dusun,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'provinsi' => $request->provinsi,
            'kode_pos' => $request->kode_pos,
            'negara' => $request->negara,
            'lintang' => $request->lintang,
            'bujur' => $request->bujur,
            'sk_pendirian' => $request->sk_pendirian,
            'tgl_sk_pendirian' => $request->tgl_sk,
            'sk_izin_operasional' => $request->sk_izin,
            'telepon' => $request->telepon,
            'fax' => $request->fax,
            'email' => $request->email,
            'website' => $request->website,
            'status' => 1
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->sekolahApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->sekolahApi->update(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'],'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'],'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update = $this->adminApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }
}
