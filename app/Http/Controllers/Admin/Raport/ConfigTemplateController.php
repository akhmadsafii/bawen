<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigTemplateApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class ConfigTemplateController extends Controller
{
    private $tahunApi;
    private $templateApi;
    private $sekolahApi;

    public function __construct()
    {
        $this->tahunApi = new TahunAjaranApi();
        $this->sekolahApi = new SekolahApi();
        $this->templateApi = new ConfigTemplateApi();
    }

    public function index()
    {
        // dd(session()->all());
        session()->put('title', 'Settingan Template');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $template = $this->templateApi->by_tahun_ajar(Help::decode($_GET['key']));
        $template = $template['body']['data'];
        return view('content.admin.raport.v_config_template')->with(['tahun' => $tahun, 'template' => $template]);
    }

    public function store(Request $request)
    {
        // dd($request);
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            $data = array(
                'jenis' => $request['jenis_' . $i],
                'template' => $request['template_' . $i],
                'id_jurusan' => $request['id_jurusan_' . $i],
                'id_ta_sm' => Help::decode($request['tahun_ajaran']),
                'id_sekolah' => session('id_sekolah')
            );
            $result = $this->templateApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }



    public function print_preview()
    {
        $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        $view = 'content.raport.sample.' . $_GET['level'] . '.v_print_' . $_GET['jenis'];
        $pdf = PDF::loadview($view, ['sekolah' => $sekolah]);
        return $pdf->stream();
    }
}
