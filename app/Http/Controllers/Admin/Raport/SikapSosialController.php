<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Raport\SikapSosialApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SikapSosialController extends Controller
{
    private $sikapSosialApi;

    public function __construct()
    {
        $this->sikapSosialApi = new SikapSosialApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Sikap Sosial');
        $sikap = $this->sikapSosialApi->get_by_sekolah();
        // dd($sikap);
        $result = $sikap['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.raport.v_sikap_sosial')->with([]);
    }

    public function edit(Request $request)
    {
        $post  = $this->sikapSosialApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $sikap = $this->sikapSosialApi->create(json_encode($data));
        }
        if ($sikap['code'] == 200) {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $sikap = $this->sikapSosialApi->update_info(json_encode($data));
        if ($sikap['code'] == 200) {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->sikapSosialApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
