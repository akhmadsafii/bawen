<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\NilaiConfigApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NilaiConfigController extends Controller
{
    private $configApi;
    private $tahunApi;

    public function __construct()
    {
        $this->configApi = new NilaiConfigApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        session()->put('title', 'Settingan Nilai');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->get_by_sekolah();
        // dd($config);
        $config = $config['body']['data'];
        return view('content.admin.raport.v_nilai_config')->with(['tahun' => $tahun, 'config' => $config]);
    }

    public function edit(Request $request)
    {
        $config  = $this->configApi->get_by_id($request['id']);
        $config = $config['body']['data'];
        return response()->json($config);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nilai_uh_rata' => $request['nilai_uh_rata'],
            'nilai_uts' => $request['nilai_uts'],
            'nilai_uas' => $request['nilai_uas'],
            'id_ta_sm' => $request['id_tahun'],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $result = $this->configApi->create(json_encode($data));
        // dd($config);
        if ($result['code'] == 200) {
            $config = $this->data_nilai();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'config' => $config
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $nilai = $request['nilai_uh_rata'] + $request['nilai_uts'] + $request['nilai_uas'];
        // dd($nilai);
        if ($nilai != 100) {
            return response()->json([
                'message' => "Jumlah nilai pengaturan " . $nilai . " <br>isian harus 100",
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        } else {
            $data = array(
                'id' => $request['id'],
                'nilai_uh_rata' => $request['nilai_uh_rata'],
                'nilai_uts' => $request['nilai_uts'],
                'nilai_uas' => $request['nilai_uas'],
                'id_ta_sm' => $request['id_tahun'],
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $result = $this->configApi->update_info(json_encode($data));
            if ($result['code'] == 200) {
                $config = $this->data_nilai();
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'success',
                    'status'  => 'berhasil',
                    'config' => $config
                ]);
            } else {
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status'  => 'gagal'
                ]);
            }
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->configApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $config = $this->data_nilai();
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'config' => $config
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    private function data_nilai()
    {
        $config = $this->configApi->get_by_sekolah();
        $config = $config['body']['data'];
        $html = '';
        if (!empty($config)) {
            foreach ($config as $cf) {
                $html .= ' <tr>
                <td class="vertical-middle text-center">
                    <h3 class="m-0">' . $cf['tahun_ajaran'] . '</h3>
                    <small>' . $cf['semester'] . '</small>
                </td>
                <td>
                    <table class="table">
                        <tr>
                            <td>Bobot Ulangan Harian</td>
                            <td>' . $cf['nilai_uh_rata'] . '</td>
                        </tr>
                        <tr>
                            <td>Bobot UTS</td>
                            <td>' . $cf['nilai_uts'] . '</td>
                        </tr>
                        <tr>
                            <td>Bobot UAS</td>
                            <td>' . $cf['nilai_uas'] . '</td>
                        </tr>
                        <tr>
                            <th>Jumlah</th>
                            <th>100</th>
                        </tr>
                    </table>
                </td>
                <td class="vertical-middle text-center">
                    <a href="javascript:void(0)" data-id="' . $cf['id'] . '"
                        class="btn btn-sm btn-info edit"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="' . $cf['id'] . '"
                        class="btn btn-sm btn-danger delete"><i
                            class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="3" style="text-align: center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
