<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\TemplateSampulApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class TemplateSampulController extends Controller
{
    private $sampulApi;
    private $tahunApi;

    public function __construct()
    {
        $this->sampulApi = new TemplateSampulApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        $tahun = $this->tahunApi->get_by_sekolah();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        Session::put('title', 'Settingan Config');
        $sampul = $this->sampulApi->by_tahun_ajar(Help::decode($_GET['key']));
        // dd($sampul);
        $sampul = $sampul['body']['data'];
        return view('content.admin.raport.v_sampul')->with(['tahun' => $tahun, 'sampul' => $sampul]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'title' => $request->title1,
            'sub_title' => $request->title2,
            'footer' => $request->footer,
            'instruksi' => $request->instruksi,
            'id_ta_sm' => Help::decode($request->id_ta_sm),
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->sampulApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function logo_atas(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
            'id_ta_sm' => Help::decode($request['tahun_ajaran'])
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->sampulApi->logo_atas(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function logo_tengah(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
            'id_ta_sm' => Help::decode($request['tahun_ajaran'])
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->sampulApi->logo_tengah(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
