<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Raport\PredikatApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class PredikatController extends Controller
{
    private $predikatApi;

    public function __construct()
    {
        $this->predikatApi = new PredikatApi();
    }

    public function index()
    {
        Session::put('title', 'Nilai Predikat');
        $url = $this->predikatApi->import();
        // dd($url);
        $predikat = $this->predikatApi->get_by_sekolah();
        $predikat = $predikat['body']['data'];
        
        return view('content.admin.raport.v_predikat')->with(['url' => $url, 'predikat' => $predikat]);
    }

    public function edit(Request $request)
    {
        $predikat  = $this->predikatApi->get_by_id($request['id']);
        $predikat = $predikat['body']['data'];
        return response()->json($predikat);
    }

    public function store(Request $request)
    {
        $data = array(
                'skor1' => $request->skor1,
                'skor2' => $request->skor2,
                'predikat' => $request->predikat,
                'nilai_predikat' => $request->nilai_predikat,
                'keterangan' => $request->deskripsi,
                'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->predikatApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $predikat = $this->data_predikat();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'predikat' => $predikat
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
                'id' => $request->id,
                'skor1' => $request->skor1,
                'skor2' => $request->skor2,
                'predikat' => $request->predikat,
                'nilai_predikat' => $request->nilai_predikat,
                'keterangan' => $request->deskripsi,
                'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->predikatApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $predikat = $this->data_predikat();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'predikat' => $predikat
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->predikatApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $predikat = $this->data_predikat();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'predikat' => $predikat
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->predikatApi->upload_excel(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            $predikat = $this->data_predikat();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'predikat' => $predikat
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_predikat()
    {
        $predikat = $this->predikatApi->get_by_sekolah();
        $predikat = $predikat['body']['data'];
        $html = '';
        if (!empty($predikat)) {
            foreach ($predikat as $pd) {
                $html .= '<tr>
                <td class="text-center vertical-middle"><b>'.$pd['predikat'].'</b></td>
                <td>
                    <b>'.$pd['skor1'] . ' - ' . $pd['skor2'].'</b>
                    <br><small>Keterangan : '.$pd['keterangan'].'</small>
                </td>
                <td>'.$pd['nilai_predikat'].'</td>
                <td>
                    <a href="javascript:void(0)" data-id="'.$pd['id'].'" class="btn btn-sm btn-info edit"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="'.$pd['id'].'" class="btn btn-sm btn-danger delete"><i class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="5" style="text-align: center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
