<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class ConfigController extends Controller
{
    private $configApi;
    private $tahunApi;

    public function __construct()
    {
        $this->configApi = new ConfigApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        Session::put('title', 'Settingan Config');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->by_tahun_aktif(Help::decode($_GET['key']));
        // dd($config);
        $config = $config['body']['data'];
        return view('content.admin.raport.v_config')->with(['tahun' => $tahun, 'config' => $config]);
    }

    public function store(Request $request)
    {
        $data = array(
            'kepala_sekolah' => $request->kepala_sekolah,
            'nip_kepala_sekolah' => $request->nip_kepsek,
            'tgl_pts' =>  date('Y-m-d', strtotime($request->tgl_pts)),
            'tgl_pts_kelas_akhir' =>  date('Y-m-d', strtotime($request->tgl_pts_kelas_akhir)),
            'tgl_raport' =>  date('Y-m-d', strtotime($request->tgl_raport)),
            'tgl_raport_kelas_akhir' =>  date('Y-m-d', strtotime($request->tgl_kelas_akhir)),
            'jenis' => $request->jenis,
            'id_ta_sm' => Help::decode($request->tahun_ajaran),
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->configApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->configApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }



    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->configApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
