<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Raport\SikapSpiritualApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SikapSpiritualController extends Controller
{
    private $sikapSpiritualApi;

    public function __construct()
    {
        $this->sikapSpiritualApi = new SikapSpiritualApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Sikap Spiritual');
        $sikap = $this->sikapSpiritualApi->get_by_sekolah();
        $result = $sikap['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.raport.v_sikap_spiritual')->with([]);
    }

    public function edit(Request $request)
    {
        $post  = $this->sikapSpiritualApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $sikap = $this->sikapSpiritualApi->create(json_encode($data));
        }
        if ($sikap['code'] == 200) {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $sikap = $this->sikapSpiritualApi->update_info(json_encode($data));
        if ($sikap['code'] == 200) {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sikap['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->sikapSpiritualApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }
}
