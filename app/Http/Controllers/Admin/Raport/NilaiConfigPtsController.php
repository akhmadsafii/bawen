<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\NilaiConfigPtsApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NilaiConfigPtsController extends Controller
{
    private $configApi;
    private $tahunApi;

    public function __construct()
    {
        $this->configApi = new NilaiConfigPtsApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        // dd('tes');
        session()->put('title', 'Config PTS');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->by_tahun_ajar(Help::decode($_GET['th']));
        $ajar = $this->tahunApi->get_by_id(Help::decode($_GET['th']));
        // dd($ajar);
        $ajar = $ajar['body']['data'];
        // dd($config);
        $config = $config['body']['data'];
        return view('content.admin.raport.v_config_pts')->with([
            'tahun' => $tahun, 'config' => $config,
            'ajar' => $ajar
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nilai_uh_rata' => $request['nilai_uh'],
            'nilai_uts' => $request['nilai_uts'],
            'id_ta_sm' => Help::decode($request['id_ta_sm']),
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->configApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
