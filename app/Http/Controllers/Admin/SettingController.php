<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return view('content.e_learning.admin.setting.v_dashboard')->with(['template' => session('template')]);
    }
}
