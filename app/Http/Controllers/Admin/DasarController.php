<?php

namespace App\Http\Controllers\Admin;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\KompetensiDasarApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiDasarApi;
use App\ApiService\Raport\NilaiKDKetrampilanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DasarController extends Controller
{
    private $kompetensiDasarApi;
    private $nilaiDasarApi;
    private $kelasSiswaApi;
    private $nilaiketrampilanApi;
    private $intiApi;

    public function __construct()
    {
        $this->kompetensiDasarApi = new KompetensiDasarApi();
        $this->nilaiDasarApi = new NilaiDasarApi();
        $this->nilaiketrampilanApi = new NilaiKDKetrampilanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->intiApi = new KompetensiApi();
    }

    public function edit_kd(Request $request)
    {
        $id = $request['id_kompetensi_dasar'];
        $post  = $this->kompetensiDasarApi->get_by_id($id);
        if ($post['code'] != 200) {
            $result['id'] = "";
            $result['mode'] = "add";
            $result['id_kompetensi_inti'] = "Pilih Kompetensi inti";
            $result['id_mapel'] = "";
            $result['id_kelas'] = "";
            $result['kode_kompetensi'] = "";
            $result['nama_kompetensi'] = "";
        } else {
            $result = $post['body']['data'];
        }
        return response()->json($result);
    }

    public function simpan(Request $request)
    {
        // dd($request);
        $d['status'] = "";
        $d['success'] = "";
        $d['message'] = "";
        $data = array(
            'id_mapel' => $request['id_mapel'],
            'id_kelas' => $request['id_kelas'],
            'id_kompetensi_inti' => $request['inti'],
            'nama' => $request['kode']."#".$request['nama'],
            'id_sekolah' => session('id_sekolah')
        );
        if ($request['_mode'] == "add") {
            $add = $this->kompetensiDasarApi->create(json_encode($data));
            // dd($add);

            $status = "ok";
            $succes = "success";
            $message = $add['body']['message'];
        } elseif ($request['_mode'] == "edit") {
            $data['id'] = $request['_id'];
            $update = $this->kompetensiDasarApi->update_info(json_encode($data));
            $status = "ok";
            $succes = "success";
            $message = $update['body']['message'];
        } else {
            $status = "gagal";
            $succes = "error";
            $message = "Kesalahan sistem";
        }
        return response()->json([
            'status' => $status,
            'success' => $succes,
            'message' => $message
        ]);
    }
    
    public function simpan_ketrampilan(Request $request)
    {
        // dd($request);
        $d['status'] = "";
        $d['success'] = "";
        $d['message'] = "";
        $data = array(
            'id_mapel' => $request['id_mapel'],
            'id_kelas' => $request['id_kelas'],
            'id_kompetensi_inti' => $request['inti'],
            'nama' => $request['kode']."#".$request['nama'],
            'id_sekolah' => session('id_sekolah')
        );
        if ($request['_mode'] == "add") {
            $add = $this->kompetensiDasarApi->create(json_encode($data));
            // dd($add);

            $status = "ok";
            $succes = "success";
            $message = $add['body']['message'];
        } elseif ($request['_mode'] == "edit") {
            $data['id'] = $request['_id'];
            $update = $this->kompetensiDasarApi->update_info(json_encode($data));
            $status = "ok";
            $succes = "success";
            $message = $update['body']['message'];
        } else {
            $status = "gagal";
            $succes = "error";
            $message = "Kesalahan sistem";
        }
        return response()->json([
            'status' => $status,
            'success' => $succes,
            'message' => $message
        ]);
    }

    public function ambil_siswa(Request $request)
    {
        // dd($request);
        $id_kd = $request['id_kompetensi_dasar'];
        $id_mapel = $request['id_mapel'];
        $id_rombel = $request['id_rombel'];
        $id_guru = $request['id_guru'];
        $id_kelas = $request['id_kelas'];
        $jenis = $request['jenis'];
        $id_tahun_ajar = session('id_tahun_ajar');
        $list_data = array();
        $ambil_nilais = $this->nilaiDasarApi->get_by_tampil_gabungan($id_kd, $id_mapel, $id_kelas, $id_rombel, $jenis, $id_guru, session('tahun'), $id_tahun_ajar);
        // dd($ambil_nilais);
        $ambil_nilai = $ambil_nilais['body']['data'];
        // $ambil_nilai = [];
        // foreach ($ambil_nilais as $ambils) {
        //     $ambil_nilai[] = array(
        //         'id' => $ambils['id'],
        //         'nama' => $ambils['siswa'],
        //         'nilai' => $ambils['nilai'],
        //     );
        // }
        // if (empty($ambil_nilai)) {
        //     $list_datas = $this->kelasSiswaApi->get_by_rombel($id_rombel, session('tahun'));
        //     $list_datas = $list_datas['body']['data'];
        //     $list_data = [];
        //     foreach($list_datas as $ld){
        //         $list_data[] = array(
        //             'id' => $ld['id'],
        //             'nama' => $ld['nama'],
        //             'nilai' => 0
        //         );
        //     }
        //     $d['sik_endi'] = "belum ada";
        // } else {
        //     $list_data = $ambil_nilai;
        //     $d['sik_endi'] = "sudah ada";
        // }

        // $d['id_guru_mapel'] = $id_guru_mapel;
        $d['status'] = "ok";
        $d['data'] = $ambil_nilai;
        return response()->json($d);
    }
   
    public function ambil_siswa_ketrampilan(Request $request)
    {
        // dd($request);
        $id_kd = $request['id_kompetensi_dasar'];
        $id_mapel = $request['id_mapel'];
        $id_rombel = $request['id_rombel'];
        $id_guru = $request['id_guru'];
        $id_kelas = $request['id_kelas'];
        $id_tahun_ajar = session('id_tahun_ajar');
        $list_data = array();
        
        $ambil_nilai = $this->nilaiketrampilanApi->get_gabungan($id_kd, $id_guru, $id_mapel, $id_kelas, $id_rombel, session('tahun'), $id_tahun_ajar);
        // dd($ambil_nilai);
        $ambil_nilai = $ambil_nilai['body']['data'];
        // $ambil_nilai = [];
        // foreach ($ambil_nilais as $ambils) {
        //     $ambil_nilai[] = array(
        //         'id' => $ambils['id'],
        //         'nama' => $ambils['siswa'],
        //         'nilai' => $ambils['nilai'],
        //     );
        // }
        // if (empty($ambil_nilai)) {
        //     $list_datas = $this->kelasSiswaApi->get_by_rombel($id_rombel, session('tahun'));
        //     $list_datas = $list_datas['body']['data'];
        //     $list_data = [];
        //     foreach($list_datas as $ld){
        //         $list_data[] = array(
        //             'id' => $ld['id'],
        //             'nama' => $ld['nama'],
        //             'nilai' => 0
        //         );
        //     }
        //     $d['sik_endi'] = "belum ada";
        // } else {
        //     $list_data = $ambil_nilai;
        //     $d['sik_endi'] = "sudah ada";
        // }

        // $d['id_guru_mapel'] = $id_guru_mapel;
        $d['status'] = "ok";
        $d['data'] = $ambil_nilai;
        return response()->json($d);
    }

    public function delete(Request $request)
    {
        $id_dasar = $request['id_kompetensi_dasar'];
        $delete = $this->kompetensiDasarApi->soft_delete($id_dasar);
        // dd($delete);
        if ($delete['code'] == 200) {
            $d['icon'] = "success";
            $d['message'] = $delete['body']['message'];
        } else {
            $d['icon'] = "danger";
            $d['message'] = $delete['body']['message'];
        }
        return response()->json($d);
    }
}
