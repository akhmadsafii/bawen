<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class PemasukanTrans extends Controller
{
    //
    //
    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Transaksi Pemasukan Lain -lain ');
        $rekening_akun = $this->SppApiservice->get_akun_pemasukan();
        $sumber = $this->SppApiservice->get_akun_harta();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun  = $rekening_akun['body']['data'] ?? array();
        $parse_sumber = $sumber['body']['data'] ?? array();

        $filterBy   = "piutang";
        $filter_akun_sumber = array_filter($parse_sumber, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) === FALSE;
        });

        $configApi  = $this->configApi();
        $param      = [
            'tahun_ajaran' => $parse_tahun,
            'akun' => $parse_akun,
            'sumber' => $filter_akun_sumber
        ];
        return view('spp.components.v_transaksi_pemasukan')->with(array_merge($configApi, $param));
    }

    /** store pemasukan lain -  lain */
    public function store_pendapatan_more(Request $request)
    {
        if (!empty($request->akun_pemasukan)) {

            $nominal    = str_replace(",", "", $request->nominal);
            if (empty($nominal)) {
                return back()->with('error', 'Nominal harus di isi !');
            }
            $datapost = array();
            $postinput = array();
            $nominal_total = 0;
            foreach ($request->akun_pemasukan as $key => $val) {
                $data = array();
                $data['id_akun'] = $val;
                if (empty($request->catatan[$key])) {
                    $data['keterangan']      = "-";
                } else {
                    $data['keterangan']      = $request->catatan[$key];
                }
                $data['nominal']      = $nominal[$key];
                $nominal_total        += intval($nominal[$key]);
                $datapost[] = $data;
            }

            $postinput['pemasukans']  = $datapost;
            $postinput['operator']     = session('username');
            $postinput['id_akun']      = $request->sumber;
            $postinput['keterangan']   = $request->keterangan;
            $postinput['no_referensi'] = $request->no_referensi;
            $postinput['tgl_bayar']    = $request->tgl_bayar;
            $postinput['tahun_ajaran'] = $request->tahun_ajaran;
            $postinput['nominal']      = $nominal_total;
            $postinput['id_sekolah']  = session('id_sekolah');

            //dd($postinput);

            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $postinput['path'] = $path;
                $response_update = $this->SppApiservice->post_pemasukan_jurnal_file(json_encode($postinput));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->post_pemasukan_jurnal(json_encode($postinput));
            }

            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }
}
