<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SPPSumbangan extends Controller
{

    protected $SppApiservice;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Dashboard SPPK Online');
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $tahun  = session('tahun');
        $datachart = array();
        $datachart2 = array();

        if (session('role') == 'siswa') {
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
            $xdash = '';
            $laba_rugi = '';
            $arus_kas  = '';
            $saldoakun = '';
            $cek_labarugi = '';
            $notif ='';
        } else if (session('role') == 'ortu') {
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id_kelas_siswa'), $bulan);
            $xdash = '';
            $laba_rugi = '';
            $arus_kas  = '';
            $saldoakun = '';
            $cek_labarugi = '';
            $notif ='';
        } else if (session('role') == 'tata-usaha') {
            $saldoakun = $this->SppApiservice->saldo_awal_bysekolah($tahun);
            //$realisasi_grafik = $this->SppApiservice->dashboard_anggaran($tahun);
            $realisasi_grafik = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
            $realisasi_table = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
            $realisasi_table_pengeluaran = $this->SppApiservice->report_realisasi_pengeluaran_dashboard($tahun);
            $xdash     = $this->SppApiservice->dashboard($tahun);
            //$laba_rugi = $this->laba_rugi();
            $laba_rugi = $this->SppApiservice->laba_rugi_dashboard($bulan, $tahun);
            $notif = $this->SppApiservice->get_notifikasi_admin(session('id'));
        }

        //$parse_xdash = $xdash['body']['data'] ?? array();
        $parse_saldo_akun = $saldoakun['body']['data'] ?? array();
        $parse_data     = $tagihanspp['body']['data'] ?? array();
        $realisasi_data = $realisasi_grafik['body']['data'] ?? array();
        $realisasi_data_table = $realisasi_table['body']['data'] ?? array();
        $parse_laba_rugi      = $laba_rugi['body']['data'] ?? array();
        $realisasi_data_pengeluaran = $realisasi_table_pengeluaran['body']['data'] ?? array();

        //dd($realisasi_data_pengeluaran);

        $sumnotif = $notif['body']['data'] ?? array();

        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        //check data chart
        if (count($realisasi_data) > 0) {
            foreach ($realisasi_data as $keyx => $valx) {
                foreach($valx['bulan'] as $x => $d){
                    if (!isset($datachart[$x])) {
                        $datachart[$x] = 0;
                    }
                    $datachart[$x] += $d['nominal'];
                }
            }
        }

        //check data grafik 2
        if (count($realisasi_data_pengeluaran) > 0) {
            foreach ($realisasi_data_pengeluaran as $keyx => $valx) {
                foreach($valx['bulan'] as $x => $d){
                    if (!isset($datachart2[$x])) {
                        $datachart2[$x] = 0;
                    }
                    $datachart2[$x] += $d['nominal'];
                }
            }
        }


        if (!empty($sumnotif)) {
            foreach ($sumnotif as $key => $val) {
                if (!empty($val['id_kelas_siswa'])) {
                    unset($sumnotif[$key]);
                } else if (empty($val['id_kelas_siswa']) && $val['dibaca2'] == 1) {
                    unset($sumnotif[$key]);
                }
            }
        }

        $param = [
            'bulan' => $bulan, 'tagihan' => $parse_data,
            'realisasi' => $realisasi_data_table,
            'data_chart' => $datachart,
            'realisasi_pengeluaran' => $realisasi_data_pengeluaran,
            'data_chart_pengeluaran' => $datachart2,
            'list_bulan' => $list_bulan,
            'tahun' => $tahun,
            'report_x' => $parse_laba_rugi,
            'saldo_akun' => $parse_saldo_akun,
            'notif_count' => count($sumnotif)
        ];

        return view('spp.components.v_spp_dashboard')->with(array_merge($param, $configApi));
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_tahun($tahun)
    {
        Session::put('title', 'Dashboard SPPK Online');
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $datachart = array();
        $datachart2 = array();

        if (session('role') == 'siswa') {
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
            $xdash = '';
            $laba_rugi = '';
            $arus_kas  = '';
            $saldoakun = '';
            $cek_labarugi = '';
            $notif ='';
        } else if (session('role') == 'ortu') {
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id_kelas_siswa'), $bulan);
            $xdash = '';
            $laba_rugi = '';
            $arus_kas  = '';
            $saldoakun = '';
            $cek_labarugi = '';
            $notif ='';
        } else if (session('role') == 'tata-usaha') {
            $saldoakun = $this->SppApiservice->saldo_awal_bysekolah($tahun);
            //$realisasi_grafik = $this->SppApiservice->dashboard_anggaran($tahun);
            $realisasi_grafik = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
            $realisasi_table = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
            $realisasi_table_pengeluaran = $this->SppApiservice->report_realisasi_pengeluaran_dashboard($tahun);
            $xdash     = $this->SppApiservice->dashboard($tahun);
            //$laba_rugi = $this->laba_rugi();
            $laba_rugi = $this->SppApiservice->laba_rugi_dashboard($bulan, $tahun);
            $notif = $this->SppApiservice->get_notifikasi_admin(session('id'));
        }

        //$parse_xdash = $xdash['body']['data'] ?? array();
        $parse_saldo_akun = $saldoakun['body']['data'] ?? array();
        $parse_data     = $tagihanspp['body']['data'] ?? array();
        $realisasi_data = $realisasi_grafik['body']['data'] ?? array();
        $realisasi_data_table = $realisasi_table['body']['data'] ?? array();
        $parse_laba_rugi      = $laba_rugi['body']['data'] ?? array();
        $realisasi_data_pengeluaran = $realisasi_table_pengeluaran['body']['data'] ?? array();

        //dd($realisasi_data_pengeluaran);

        $sumnotif = $notif['body']['data'] ?? array();

        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        //check data chart
        if (count($realisasi_data) > 0) {
            foreach ($realisasi_data as $keyx => $valx) {
                foreach($valx['bulan'] as $x => $d){
                    if (!isset($datachart[$x])) {
                        $datachart[$x] = 0;
                    }
                    $datachart[$x] += $d['nominal'];
                }
            }
        }

        //check data grafik 2
        if (count($realisasi_data_pengeluaran) > 0) {
            foreach ($realisasi_data_pengeluaran as $keyx => $valx) {
                foreach($valx['bulan'] as $x => $d){
                    if (!isset($datachart2[$x])) {
                        $datachart2[$x] = 0;
                    }
                    $datachart2[$x] += $d['nominal'];
                }
            }
        }


        if (!empty($sumnotif)) {
            foreach ($sumnotif as $key => $val) {
                if (!empty($val['id_kelas_siswa'])) {
                    unset($sumnotif[$key]);
                } else if (empty($val['id_kelas_siswa']) && $val['dibaca2'] == 1) {
                    unset($sumnotif[$key]);
                }
            }
        }

        $param = [
            'bulan' => $bulan, 'tagihan' => $parse_data,
            'realisasi' => $realisasi_data_table,
            'data_chart' => $datachart,
            'realisasi_pengeluaran' => $realisasi_data_pengeluaran,
            'data_chart_pengeluaran' => $datachart2,
            'list_bulan' => $list_bulan,
            'tahun' => $tahun,
            'report_x' => $parse_laba_rugi,
            'saldo_akun' => $parse_saldo_akun,
            'notif_count' => count($sumnotif)
        ];

        return view('spp.components.v_spp_dashboard')->with(array_merge($param, $configApi));
    }

    /** laba rugi bulan ini  */
    private function laba_rugi()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $rugilaba     = $this->SppApiservice->report_labarugi($bulan, session('tahun'));
        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $total_pendapatan = 0;
        $total_pengeluaran = 0;
        $total_laba_bersih = 0;
        foreach ($parse_rugilaba as $key => $laporan) {
            foreach ($laporan['sub_akun_kategori'] as $subakun) {
                foreach ($subakun['akun'] as $akun) {
                    if ($laporan['id'] == 4) {
                        $total_pendapatan += intval($akun['nominal']);
                    } elseif ($laporan['id'] == 5) {
                        $total_pengeluaran += intval($akun['nominal']);
                    }
                }
            }
        }
        $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
        $data = array();
        $data['pendapatan'] = intval($total_pendapatan);
        $data['pengeluaran'] = intval($total_pengeluaran);
        $data['laba_rugi']   = intval($total_laba_bersih);
        return $data;
    }

    /** arus kas  */
    public function arus_kas()
    {
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $data_pemasukan = array();
        $data_pengeluaran = array();
        $list = array();
        foreach ($list_bulan as $bulan) {
            $list[] = $this->laba_rugi_month($bulan, session('tahun'));
        }

        foreach ($list as $x) {
            $data_pemasukan[] = $x['pendapatan'];
            $data_pengeluaran[] = $x['pengeluaran'];
        }

        return response()->json(
            [
                'message' => 'success',
                'data' => array('pemasukan' => $data_pemasukan, 'pengeluaran' => $data_pengeluaran, 'list_bulan' => $list_bulan),
                'info' => 'success',
            ]
        );
    }

    /** laba rugi by bulan  */
    private function laba_rugi_month($bulan, $tahun)
    {
        $rugilaba     = $this->SppApiservice->report_labarugi($bulan, $tahun);
        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $total_pendapatan = 0;
        $total_pengeluaran = 0;
        $total_laba_bersih = 0;
        foreach ($parse_rugilaba as $key => $laporan) {
            foreach ($laporan['sub_akun_kategori'] as $subakun) {
                foreach ($subakun['akun'] as $akun) {
                    if ($laporan['id'] == 4) {
                        $total_pendapatan += intval($akun['nominal']);
                    } elseif ($laporan['id'] == 5) {
                        $total_pengeluaran += intval($akun['nominal']);
                    }
                }
            }
        }
        $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
        $data = array();
        $data['laba_rugi']   = intval($total_laba_bersih);
        $data['pendapatan']   = intval($total_pendapatan);
        $data['pengeluaran']   = intval($total_pengeluaran);
        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profilUser()
    {
        Session::put('title', 'My Profile');
        $data_profile = $this->SppApiservice->get_info_profil();
        $parse_profil = $data_profile['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param  = [
            'profil_data' => $parse_profil,
        ];
        return view('spp.components.my_profil')->with(array_merge($param, $configApi));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required|max:255',
            'alamat' => 'required|max:255',
            'kota' => 'required',
            'telephone' => 'required|numeric',
            'email' => 'required|email|unique:users'
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'alamat.required' => 'Form alamat tidak boleh kosong!',
            'kota.required' => 'Form kota tidak boleh kosong!',
            'telephone.required' => 'Form telephone tidak boleh kosong!',
            'telephone.numeric' => 'Nomor telephone hanya diperbolehkan berupa angka!',
            'email.required' => 'Form email tidak boleh kosong!',
            'email.email' => 'Format email tidak valid ! ',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
        ]);

        // from request input form
        $data_post = array();
        $data_post['username']     = session('username');
        if (!empty(session('role'))) {
            if (session('role') == 'tata-usaha') {
                $data_post['nip']          = $request->nip;
            }
        }
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->SppApiservice->update_profil_withfile($json_body);
            File::delete($path);
        } else {
            $json_body = json_encode($data_post);
            //update ke api backend
            $response_update = $this->SppApiservice->update_profil($json_body);
        }

        //dd($response_update);


        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->nama);
            return back()->with('success', 'Successfully Profil Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Profil Admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_password()
    {
        Session::put('title', 'Ubah Password ');
        $configApi  = $this->configApi();
        return view('spp.components.v_password')->with(array_merge($configApi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update_password(Request $request)
    {
        $request->validate([
            //'username'=>'required|max:255',
            'password_lama' => 'required|max:255',
            'password_baru' => 'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru' => 'required',
        ], [
            //'username.required'=>'Form username tidak boleh kosong!',
            'password_lama.required' => 'Form password lama tidak boleh kosong!',
            'password_baru.required' => 'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required' => 'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' => 'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        //$data_post['username']         = $request->username;
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->SppApiservice->update_password($json_body);

        //dd($response_update);

        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->username);
            return back()->with('success', 'Successfully Password Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Password Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** public function forget password */
    public function forget_password()
    {
        Session::put('title', 'Lupa Password');
        $configApi  = $this->configApi();
        return view('spp.components.forget_pass')->with(array_merge($configApi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_forget_pass(Request $request)
    {
        $request->validate([
            'email' => 'required |email',
        ], [
            'email.required' => 'Form Email tidak boleh kosong!',
        ]);

        $data = array();
        $data['email'] = $request->email;

        $response_update = $this->SppApiservice->forget_pass(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** get Token */
    public function token_passwordx($token)
    {
        Session::put('title', 'Reset Password Baru ');
        $configApi  = $this->configApi();
        $param = ['token' => $token];
        return view('spp.components.forget_pass_reset')->with(array_merge($configApi, $param));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_reset_pass(Request $request)
    {
        $request->validate([
            'password_new' => 'required',
            'password_confirm' => 'required | same:password_new',
        ], [
            'password_new.required' => 'Form Password Baru tidak boleh kosong!',
            'password_confirm.required' => 'Form Password Konfirm tidak boleh kosong!',
        ]);

        $token = $request->token_reset;
        $response_check = $this->SppApiservice->get_token_pass($token);

        if ($response_check['code'] == '200') {
            $data = array();
            $data['as'] = $response_check['body']['data']['as'];
            $data['email'] = $response_check['body']['data']['email'];
            $data['password'] = $request->password_new;
            $data['password_confirmation'] = $request->password_confirm;
            $response_update = $this->SppApiservice->reset_pass(json_encode($data));
            if ($response_update['code'] == '200') {
                Session::put('title', ' Auth Login ');
                $pesan = array(
                    'message' => $response_update['body']['message'],
                    'icon' => 'berhasil',
                    'status' => 'success'
                );
                return redirect()->route('auth.login')->with(['message' => $pesan]);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return redirect()->route('forget_passwordx')->with('error', $response_check['body']['message']);
        }
    }
}
