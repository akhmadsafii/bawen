<?php
namespace App\Http\Controllers\SumbanganSpp;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use App\ApiService\SumbanganSpp\SppApiservice;
class ExportFile_neraca  implements FromView, ShouldAutoSize, WithHeadings, WithEvents
{
     //
     protected $SppApiservice;
     public $tahun;
     /**
      * __construct function new class Api
      */

     public function __construct($tahun)
     {
         $this->SppApiservice = new SppApiservice();
         $this->tahun         = $tahun;
     }

     public function headings(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
    public function view(): View{
        $report_neraca = $this->SppApiservice->report_neraca($this->tahun);
        $parse_report_neraca = $report_neraca['body']['data'] ?? array();
        $laba_rugi    = $this->laba_rugibytahun($this->tahun);
        $param = [
            'xtahun' => $this->tahun,
            'report_neraca' => $parse_report_neraca,
            'report_laba_rugi' => $laba_rugi
        ];
        return view('spp.components.v_reportneracax')->with(array_merge($param));
    }

     /** laba rugi by tahun  */
     private function laba_rugibytahun($tahun)
     {
         $rugilaba     = $this->SppApiservice->report_labarugibytahun($tahun);
         $parse_rugilaba = $rugilaba['body']['data'] ?? array();
         //dd($rugilaba);
         $total_pendapatan = 0;
         $total_pengeluaran = 0;
         $total_laba_bersih = 0;
         foreach ($parse_rugilaba as $key => $laporan) {
             foreach ($laporan['sub_akun_kategori'] as $subakun) {
                 foreach ($subakun['akun'] as $akun) {
                     if ($laporan['id'] == 4) {
                         $total_pendapatan += intval($akun['nominal']);
                     } elseif ($laporan['id'] == 5) {
                         $total_pengeluaran += intval($akun['nominal']);
                     }
                 }
             }
         }
         $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
         $data = array();
         $data['laba_rugi']   = intval($total_laba_bersih);
         $data['pendapatan']   = intval($total_pendapatan);
         $data['pengeluaran']   = intval($total_pengeluaran);
         return $data;
     }
}
