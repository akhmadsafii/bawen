<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;


class ReportController extends Controller
{
    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Session::put('title', 'Laporan Tagihan SPP ');
        if (empty($request->id_rombel) && empty($request->tahun_ajaran)) {
            $tahun = session('tahun');
            $sample = $this->SppApiservice->report_tagihan('1', $tahun);
        } else {
            $tahun_get = explode("/", $request->tahun_ajaran);
            $tahun = $tahun_get[0];
            $sample = $this->SppApiservice->report_tagihan($request->id_rombel, $tahun);
        }
        $datapos = $this->SppApiservice->get_data_pos();
        $data_rombel = $this->SppApiservice->rombel_data();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_rombel = $data_rombel['body']['data'] ?? array();
        $parse_pos = $datapos['body']['data'] ?? array();
        $data_sample = $sample['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = [
            'report_tagihan' => $data_sample,
            'pos' => $parse_pos,
            'rombelx' => $parse_rombel,
            'tahun_ajaran' => $parse_tahun,
            'request_tahun' => $tahun,
            'request_rombel' => $request->id_rombel ?? '1'
        ];
        return view('spp.components.v_report_tagihan')->with(array_merge($configApi, $param));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function print_report_tagihan($id, $tahun, $name_kelas, $name_tahun)
    {
        Session::put('title', 'Print Laporan Tagihan SPP ');
        $sample = $this->SppApiservice->report_tagihan($id, $tahun);
        $datapos = $this->SppApiservice->get_data_pos();
        $data_rombel = $this->SppApiservice->rombel_data();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_rombel = $data_rombel['body']['data'] ?? array();
        $parse_pos = $datapos['body']['data'] ?? array();
        $data_sample = $sample['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = [
            'report_tagihan' => $data_sample,
            'pos' => $parse_pos,
            'rombelx' => $parse_rombel,
            'tahun_ajaran' => $parse_tahun,
            'nama_kelas' => $name_kelas,
            'nama_tahun_ajaran' => $name_tahun
        ];
        return view('spp.components.v_report_tagihan_print')->with(array_merge($configApi, $param));
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_history_transaksi(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi_siswa_id(session('id'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('nama', function ($row) {
                return ucfirst($row['nama']);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('tgl_filter', function ($row) {
                return $row['tgl_bayar'];
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar', 'nama', 'tgl_filter']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_history_transaksiortu(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi_siswa_id(session('id_kelas_siswa'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_transaksi()
    {
        Session::put('title', 'Histori Transaksi SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
        return view('spp.components.v_transaksi_history_siswa')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_transaksiortu()
    {
        if (session('role') == 'ortu') {
            Session::put('title', 'Histori Transaksi SPP Online');
            $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
            $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
            $configApi  = $this->configApi();
            $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
            return view('spp.components.v_transaksi_history_ortu')->with(array_merge($param, $configApi));
        } else {
            return back()->with('error', ' error hanya untuk orang tua saja lihat pembayaran history ini   !');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tagihan_siswa_bulan()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data];
        return view('spp.components.v_tagihan_siswa')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tagihan_ortu_bulan()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id_kelas_siswa'), $bulan);
        $data_ortu  = $this->SppApiservice->get_data_ortu(session('id_kelas_siswa'));
        $data_rekening = $this->SppApiservice->rekeningspp(session('id_sekolah'));
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $parse_datax = $data_ortu['body']['data'] ?? array();
        $parse_rekening = $data_rekening['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data, 'ortu' => $parse_datax, 'rekening' => $parse_rekening];
        return view('spp.components.v_tagihan_ortu')->with(array_merge($configApi, $param));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function print_tagihan_siswa()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data];
        return view('spp.components.v_tagihan_siswa_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function print_tagihan_ortu()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $data_ortu  = $this->SppApiservice->get_data_ortu(session('id_kelas_siswa'));
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $parse_datax = $data_ortu['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data, 'ortu' => $parse_datax];
        return view('spp.components.v_tagihan_ortu_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi()
    {
        $tahun  = session('tahun');
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $realisasi = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi['body']['data'] ?? array();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan, 'tahunlist' => $tahun_data];
        return view('spp.components.realisasi_anggaran')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_pengeluaran()
    {
        $tahun  = session('tahun');
        Session::put('title', 'LAPORAN REALISASI PENGGUNAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $realisasi_pengeluaran = $this->SppApiservice->get_data_realisasi_pengeluaran(session('tahun'));
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi_pengeluaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan, 'tahunlist' => $tahun_data];
        return view('spp.components.realisasi_anggaran_pengeluaran')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_pengeluaran_filter($tahun)
    {
        Session::put('title', 'LAPORAN REALISASI PENGGUNAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $realisasi_pengeluaran = $this->SppApiservice->get_data_realisasi_pengeluaran(session('tahun'));
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi_pengeluaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan, 'tahunlist' => $tahun_data];
        return view('spp.components.realisasi_anggaran_pengeluaran')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_pengeluaran_print($tahun)
    {
        Session::put('title', 'LAPORAN REALISASI PENGGUNAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $realisasi_pengeluaran = $this->SppApiservice->get_data_realisasi_pengeluaran(session('tahun'));
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi_pengeluaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan];
        return view('spp.components.realisasi_anggaran_pengeluaran_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasiTahun($tahun)
    {
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $realisasi = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan, 'tahunlist' => $tahun_data];
        return view('spp.components.realisasi_anggaran')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasiTahunprint($tahun)
    {
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN ' . $tahun);
        $bulan  = '';
        $realisasi = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();

        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data, 'list_bulan' => $list_bulan, 'tahun_ajaran' => $parse_tahun_ajaran, 'request_tahun' => $tahun, 'request_bulan' => $bulan];
        return view('spp.components.realisasi_anggaran_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_filter($tahun, $bulan)
    {
        $list_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        //$realisasi = $this->SppApiservice->report_realisasi_filter($tahun, $list_bulan[intval($bulan) - 1]);
        $realisasi = $this->SppApiservice->report_realisasi_pemasukan_dashboard($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param      = [
            'realisasi' => $parse_data,
            'list_bulan' => $list_bulan,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'request_tahun' => $tahun,
            'request_bulan' => $bulan,
            'nama_bulan'  => $list_bulan[intval($bulan) - 1],
            'tahunlist'   => $tahun_data
        ];
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE BULAN ' . ucwords($list_bulan[intval($bulan) - 1]) . ' TAHUN ' . $tahun);
        return view('spp.components.realisasi_anggaran_filter')->with(array_merge($configApi, $param));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_filterprint($tahun, $bulan)
    {
        $list_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $realisasi = $this->SppApiservice->report_realisasi_filter($tahun, $list_bulan[intval($bulan) - 1]);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data = $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
            'realisasi' => $parse_data,
            'list_bulan' => $list_bulan,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'request_tahun' => $tahun,
            'request_bulan' => $bulan,
            'nama_bulan'  => $list_bulan[intval($bulan) - 1]
        ];
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE BULAN ' . ucwords($list_bulan[intval($bulan) - 1]) . ' TAHUN ' . $tahun);
        return view('spp.components.realisasi_anggaran_filter_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_jurnalUmum()
    {
        Session::put('title', 'LAPORAN JURNAL UMUM ');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $report_jurnal = $this->SppApiservice->report_jurnal($bulan, session('tahun'));
        $parse_report_jurnal = $report_jurnal['body']['data'] ?? array();

        $index_bulan = array_keys($list_bulan, $bulan);

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $bulan,
            'index_bulan' => intval($index_bulan[0]) + 1,
            'tahun' => session('tahun'),
            'tahunlist' => $tahun_data,
            'report' => $parse_report_jurnal
        ];
        return view('spp.components.v_reportjurnal_umum')->with(array_merge($configApi, $param));
    }

    /** filter jurnal umum */
    public function filter_report_jurnal($bulan, $tahun)
    {
        Session::put('title', 'LAPORAN JURNAL UMUM ');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $report_jurnal = $this->SppApiservice->report_jurnal($list_bulan[intval($bulan) - 1], $tahun);
        $parse_report_jurnal = $report_jurnal['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'index_bulan' => intval($bulan),
            'bulan' => $list_bulan[intval($bulan) - 1],
            'tahun' => $tahun,
            'tahunlist' => $tahun_data,
            'report' => $parse_report_jurnal
        ];
        return view('spp.components.v_reportjurnal_umum')->with(array_merge($configApi, $param));
    }

    /** filter jurnal umum print */
    public function filter_report_jurnalprint($bulan, $tahun)
    {
        Session::put('title', 'LAPORAN JURNAL UMUM ');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $report_jurnal = $this->SppApiservice->report_jurnal($list_bulan[intval($bulan) - 1], $tahun);
        $parse_report_jurnal = $report_jurnal['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'index_bulan' => intval($bulan),
            'bulan' => $list_bulan[intval($bulan) - 1],
            'tahun' => $tahun,
            'tahunlist' => $tahun_data,
            'report' => $parse_report_jurnal
        ];
        return view('spp.components.v_reportjurnal_umumprint')->with(array_merge($param));
    }

    /** group by array */
    private function array_group_by($arr, $fldName)
    {
        $groups = array();
        $list   = array();
        foreach ($arr as $rec) {
            $cekitem = array();
            $cekitem['id'] = $rec['id'];
            $cekitem['id_akun'] = $rec['id_akun'];
            $cekitem['kode_transaksi'] = $rec['kode_transaksi'];
            $cekitem['no_referensi'] = $rec['no_referensi'];
            $cekitem['kode_akun'] = $rec['kode_akun'];
            $cekitem['akun'] = $rec['akun'];
            $cekitem['debet'] = $rec['debet'];
            $cekitem['kredit'] = $rec['kredit'];
            $cekitem['tahun_ajaran'] = $rec['tahun_ajaran'];
            $cekitem['keterangan'] = $rec['keterangan'];
            $cekitem['id_sekolah'] = $rec['id_sekolah'];
            $cekitem['tgl_bayar']  = $rec['tgl_bayar'];
            $list[] = $cekitem;
            $groups[$rec[$fldName]] = $list;
        }
        return $groups;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_labarugi()
    {
        Session::put('title', 'LAPORAN LABA RUGI');
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $bulan_index = array_keys($list_bulan, $bulan);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $rugilaba     = $this->SppApiservice->report_labarugi($bulan, session('tahun'));
        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $bulan,
            'tahun' => session('tahun'),
            'tahunlist' => $tahun_data,
            'report' => $parse_rugilaba,
            'index_bulan' => intval($bulan_index[0]) + 1,
            'mode' => '1',
        ];
        return view('spp.components.v_reportlabarugi')->with(array_merge($configApi, $param));
    }

    /** laba rugi by bulan  */
    private function laba_rugi($bulan, $tahun)
    {
        $rugilaba     = $this->SppApiservice->report_labarugi($bulan, $tahun);
        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $total_pendapatan = 0;
        $total_pengeluaran = 0;
        $total_laba_bersih = 0;
        foreach ($parse_rugilaba as $key => $laporan) {
            foreach ($laporan['sub_akun_kategori'] as $subakun) {
                foreach ($subakun['akun'] as $akun) {
                    if ($laporan['id'] == 4) {
                        $total_pendapatan += intval($akun['nominal']);
                    } elseif ($laporan['id'] == 5) {
                        $total_pengeluaran += intval($akun['nominal']);
                    }
                }
            }
        }
        $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
        $data = array();
        $data['laba_rugi']   = intval($total_laba_bersih);
        $data['pendapatan']   = intval($total_pendapatan);
        $data['pengeluaran']   = intval($total_pengeluaran);
        return $data;
    }

    /** laba rugi by tahun  */
    private function laba_rugibytahun($tahun)
    {
        $rugilaba     = $this->SppApiservice->report_labarugibytahun($tahun);
        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        //dd($rugilaba);
        $total_pendapatan = 0;
        $total_pengeluaran = 0;
        $total_laba_bersih = 0;
        foreach ($parse_rugilaba as $key => $laporan) {
            foreach ($laporan['sub_akun_kategori'] as $subakun) {
                foreach ($subakun['akun'] as $akun) {
                    if ($laporan['id'] == 4) {
                        $total_pendapatan += intval($akun['nominal']);
                    } elseif ($laporan['id'] == 5) {
                        $total_pengeluaran += intval($akun['nominal']);
                    }
                }
            }
        }
        $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
        $data = array();
        $data['laba_rugi']   = intval($total_laba_bersih);
        $data['pendapatan']   = intval($total_pendapatan);
        $data['pengeluaran']   = intval($total_pengeluaran);
        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_labarugi_filter($bulan, $tahun, $mode)
    {
        Session::put('title', 'LAPORAN LABA RUGI');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $bulanx =  \Carbon\Carbon::now()->isoFormat('MMMM');
        if ($bulan == '') {
            $rugilaba     = $this->SppApiservice->report_labarugi($bulanx, $tahun);
        } else {
            $rugilaba     = $this->SppApiservice->report_labarugi($list_bulan[intval($bulan) - 1], $tahun);
        }

        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();

        if ($mode == 2) {
            $report = $this->filter_group_kategori($parse_rugilaba);
        } else if ($mode == 3) {
            $report = $this->filter_group_subkategori($parse_rugilaba);
        } else if ($mode == 1) {
            $report   =  $parse_rugilaba;
        }

        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $list_bulan[intval($bulan) - 1],
            'tahun' => $tahun,
            'tahunlist' => $tahun_data,
            'report' => $report,
            'mode' => $mode,
            'index_bulan' => intval($bulan),
        ];

        return view('spp.components.v_reportlabarugi_custom')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_labarugi_filterprint($bulan, $tahun, $mode)
    {
        Session::put('title', 'LAPORAN LABA RUGI');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $bulanx =  \Carbon\Carbon::now()->isoFormat('MMMM');
        if ($bulan == '') {
            $rugilaba     = $this->SppApiservice->report_labarugi($bulanx, $tahun);
        } else {
            $rugilaba     = $this->SppApiservice->report_labarugi($list_bulan[intval($bulan) - 1], $tahun);
        }

        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();

        if ($mode == 2) {
            $report = $this->filter_group_kategori($parse_rugilaba);
        } else if ($mode == 3) {
            $report = $this->filter_group_subkategori($parse_rugilaba);
        } else if ($mode == 1) {
            $report   =  $parse_rugilaba;
        }

        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $list_bulan[intval($bulan) - 1],
            'tahun' => $tahun,
            'tahunlist' => $tahun_data,
            'report' => $report,
            'mode' => $mode,
            'index_bulan' => intval($bulan),
        ];

        return view('spp.components.v_reportlabarugi_custom_print')->with(array_merge($configApi, $param));
    }

    /** mode laba rugi filter kategori */
    private function filter_group_kategori($data)
    {
        $group = array();
        $total_penggunaan = 0;
        $total_pendapatan = 0;
        foreach ($data as $key => $x) {
            $array_item = array();
            if ($x['id'] == '4') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_pendapatan  += intval($o['nominal']);
                    }
                }

                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_pendapatan);
            }
            if ($x['id'] == '5') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_penggunaan  += intval($o['nominal']);
                    }
                }
                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_penggunaan);
            }
            $group[] = $array_item;
        }
        return $group;
    }

    /** mode laba rugi filter kategori */
    private function filter_group_subkategori($data)
    {
        $group = array();
        $total_penggunaan = 0;
        $total_pendapatan = 0;
        foreach ($data as $key => $x) {
            $array_item = array();
            $sub_akun   = array();
            if ($x['id'] == '4') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_pendapatan  += intval($o['nominal']);
                        $sub_akun[]  = $o;
                    }
                }

                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_pendapatan);
                $array_item['sub_akun'] = $sub_akun;
            }
            if ($x['id'] == '5') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_penggunaan  += intval($o['nominal']);
                        $sub_akun[]  = $o;
                    }
                }
                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_penggunaan);
                $array_item['sub_akun'] = $sub_akun;
            }
            $group[] = $array_item;
        }

        return $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_neraca()
    {
        Session::put('title', 'LAPORAN NERACA');
        $report_neraca = $this->SppApiservice->report_neraca(session('tahun'));
        $parse_report_neraca = $report_neraca['body']['data'] ?? array();
        //$bulan       =  \Carbon\Carbon::now()->isoFormat('MMMM');
        //$laba_rugi  = $this->laba_rugi($bulan,session('tahun'));
        $laba_rugi    = $this->laba_rugibytahun(session('tahun'));

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahunlist' => $tahun_data,
            'tahun' => session('tahun'),
            'report_neraca' => $parse_report_neraca,
            'report_laba_rugi' => $laba_rugi
        ];
        return view('spp.components.v_reportneraca')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_neraca_filter($tahun)
    {
        Session::put('title', 'LAPORAN NERACA');
        $report_neraca = $this->SppApiservice->report_neraca($tahun);
        $parse_report_neraca = $report_neraca['body']['data'] ?? array();
        //$bulan       =  \Carbon\Carbon::now()->isoFormat('MMMM');
        //$laba_rugi  = $this->laba_rugi($bulan,session('tahun'));

        $laba_rugi    = $this->laba_rugibytahun($tahun);

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahunlist' => $tahun_data,
            'tahun' => $tahun,
            'report_neraca' => $parse_report_neraca,
            'report_laba_rugi' => $laba_rugi
        ];
        return view('spp.components.v_reportneraca')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_neraca_print($tahun)
    {
        Session::put('title', 'LAPORAN NERACA');
        $report_neraca = $this->SppApiservice->report_neraca($tahun);
        $parse_report_neraca = $report_neraca['body']['data'] ?? array();
        //$bulan       =  \Carbon\Carbon::now()->isoFormat('MMMM');
        //$laba_rugi  = $this->laba_rugi($bulan,session('tahun'));
        $laba_rugi    = $this->laba_rugibytahun($tahun);

        $configApi  = $this->configApi();
        $param = [
            'tahun' => $tahun,
            'report_neraca' => $parse_report_neraca,
            'report_laba_rugi' => $laba_rugi
        ];
        return view('spp.components.v_reportneraca_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_bukubesar()
    {
        Session::put('title', 'LAPORAN BUKU BESAR ');
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $bulan_index = array_keys($list_bulan, $bulan);
        $akun_rekening = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $buku_besar   = $this->SppApiservice->report_bukubesar('1', $bulan, session('tahun'));
        $parse_bukubesar = $buku_besar['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun         = $akun_rekening['body']['data'] ?? array();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $bulan,
            'index_bulan' => intval($bulan_index[0]) + 1,
            'tahunlist' => $tahun_data,
            'tahun' => session('tahun'),
            'rekening_akun' => $parse_akun,
            'report' => $parse_bukubesar,
            'akun'  => '1'
        ];
        return view('spp.components.v_reportbukubesar')->with(array_merge($configApi, $param));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_bukubesar_filter($bulan, $tahun, $akun)
    {
        Session::put('title', 'LAPORAN BUKU BESAR ');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $akun_rekening = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $buku_besar   = $this->SppApiservice->report_bukubesar($akun, $list_bulan[intval($bulan) - 1], $tahun);
        $parse_bukubesar = $buku_besar['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun         = $akun_rekening['body']['data'] ?? array();
        $tahun_data = array();

        //dd($parse_bukubesar);

        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $list_bulan[intval($bulan) - 1],
            'index_bulan' => $bulan,
            'tahunlist' => $tahun_data,
            'tahun' => $tahun,
            'rekening_akun' => $parse_akun,
            'report' => $parse_bukubesar,
            'akun'  => $akun
        ];
        return view('spp.components.v_reportbukubesar')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_bukubesar_print($bulan, $tahun, $akun)
    {
        Session::put('title', 'LAPORAN BUKU BESAR ');
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $akun_rekening = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $buku_besar   = $this->SppApiservice->report_bukubesar($akun, $list_bulan[intval($bulan) - 1], $tahun);
        $parse_bukubesar = $buku_besar['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun         = $akun_rekening['body']['data'] ?? array();

        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $list_bulan[intval($bulan) - 1],
            'bulan_ke' => $bulan,
            'tahun' => $tahun,
            'rekening_akun' => $parse_akun,
            'report' => $parse_bukubesar,
            'akun'  => $akun
        ];
        return view('spp.components.v_reportbukubesar_print')->with(array_merge($configApi, $param));
    }

    /** export file excel rugi laba */
    public function export_laba_rugi($bulan, $tahun, $mode)
    {
        return \Excel::download(new ExportFile_rugilaba($bulan, $tahun, $mode), 'report_laba_rugi' . $bulan . '-' . $tahun . '.xlsx');
    }

    /** export file excel neraca */
    public function export_neraca($tahun)
    {
        return \Excel::download(new ExportFile_neraca($tahun), 'report_neraca - ' . $tahun . '.xlsx');
    }
}
