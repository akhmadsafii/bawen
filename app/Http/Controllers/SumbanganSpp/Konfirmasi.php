<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Carbon\Carbon;


class Konfirmasi extends Controller
{
    protected $SppApiservice;
    protected $config;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_upload()
    {
        Session::put('title', 'Konfirmasi Pembayaran');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran
        ];
        return view('spp.components.upload_bukti_file')->with($param);
    }

    /** ajax tagihan bulan ini */
    public function get_tagihan_siswa()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        if (!empty($parse_data)) {
            return response()->json(
                [
                    'message' => 'success',
                    'info' => 'success',
                    'data' => $parse_data
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'error',
                    'info' => 'error',
                    'data' => $parse_data
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_epayment(Request $request)
    {
        $request->validate([
            'tahun_ajaran' => 'required',
            'tgl_bayar' => 'required',
        ], [
            'tahun_ajaran.required' => 'Pilih Tahun ajaran terlebih dahulu, tidak boleh kosong!',
            'tgl_bayar.required' => 'Tanggal Pembayaran tidak boleh kosong!',
        ]);

        $data_transaksi = array();
        $data_transaksi['id_kelas_siswa'] = $request->id_siswa;
        $data_transaksi['tgl_bayar']      = $request->tgl_bayar;
        $data_transaksi['tahun_ajaran']   = $request->tahun_ajaran;

        $data_transaksi['nis']            = $request->nisn;
        $data_transaksi['id_sekolah']     = session('id_sekolah');

        $nominal    = str_replace(",", "", $request->nominal);
        $cekarray = array();

        $list_bulan = [
            '-',
            'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
            'Januari', 'Februari', 'Maret',
            'April', 'Mei', 'Juni'
        ];

        foreach ($request->id_tagihan as $key => $n) {
            $array = array();
            $r                   = explode('|', $n);
            $array['id_tagihan'] = $r[0];
            $array['nama_tagihan'] = $request->nama_tagihan[$key] ?? $r[1];
            $array['periode']      = $request->periode[$key];
            $array['bulan']      = $request->bulan[$key];
            $array['nama_bulan'] = $list_bulan[$request->bulan[$key]];
            $array['nominal']    = $nominal[$key];
            $cekarray[] = $array;
        }

        $data_transaksi['details'] = $cekarray;

        $item_details = array();
        foreach ($data_transaksi['details'] as $detail) {
            $item = array();
            $item['id']    = $detail['id_tagihan'];
            $item['price'] = $detail['nominal'] ?? '0';
            $item['quantity'] = '1';
            $item['name']     = $detail['nama_tagihan'];
            $item['periode']   = $detail['periode'];
            $item_details[] = $item;
        }

        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        }
                    }
                }
            }
        }

        $serverKey = env('MIDTRANS_SERVER_KEY') ?? $MIDTRANS_CLIENT_KEY;
        if (!empty($serverKey)) {

            $headers = [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($serverKey . ':'),
            ];

            $transactions = [
                'transaction_details' => [
                    'order_id' => 'TRX-' . $request->nis . '-' . Carbon::now()->timestamp . '',
                    'gross_amount' => collect($data_transaksi['details'])->sum('nominal')
                ],
                'customer_details' => [
                    'first_name' => $request->nama,
                    'Nisn'       => $request->nisn,
                    'tahun_ajaran' => $request->tahun_ajaran,
                    'sekolah'     => session('sekolah')
                ],
                'item_details' => $item_details,
            ];

            $client = new Client(array(
                'verify' => false
            ));
            $resp = $client->post('https://app.sandbox.midtrans.com/snap/v1/transactions', [
                'headers' => $headers,
                'json' => $transactions
            ]);

            return response()->json([
                'info' => 'success',
                'message' => 'success',
                'data' => json_decode($resp->getBody()->getContents())
            ]);
        }
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_upload(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kode' => 'required',
            'tanggal_bayar' => 'required',
        ], [
            'tanggal_bayar.required' => 'Form tanggal bayar tidak boleh kosong!',
            'kode.required' => 'Form kode tidak boleh kosong!',
            'image.required' => 'Form Gambar tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
        ]);

        $data = array();
        $data['id_sekolah'] = session('id_sekolah');
        $data['kode'] = $request->kode;
        $data['tgl_bayar'] = $request->tanggal_bayar;
        $data['bentuk']    = 'transfer_bank';
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bukti/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->SppApiservice->post_upload_konfirmfile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->SppApiservice->post_upload_konfirm(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $message  = "Kode Transaksi " . $request->kode . " telah diupdate dengan upload bukti pembayaran. Mohon segera diperiksa!";
            $this->SppApiservice->send_notif('konfirmasi_pembayaran_spp', 'event_konfirm_spp', $message);
            // push notifikasi save table
            $data_notikasi = array();
            $data_notikasi['id_kelas_siswa'] = '';
            $data_notikasi['url'] = route('konfirmasi-transaksi');
            $data_notikasi['isi'] = $message;
            $data_notikasi['id_sekolah'] = session('id_sekolah');
            $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));
            $messagex = Str::title($response_update['body']['message']) . ' bukti transfer secepatnya akan kami periksa.';
            return back()->with('success', $messagex);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** ajax notifikasi pembayaran midtrans
     * * *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function message_payment($id)
    {
        $order_id = $id['order_id'];
        $amount    = number_format($id['gross_amount'], 0);
        $type     = $id['payment_type'];
        $time     = $id['transaction_time'];
        $message  = "Kode Transaksi " . $order_id . " telah melakukan metode pembayaran " . $type . " dengan jumlah nominal " . $amount . " pada waktu " . $time . " . Mohon segera diperika!";
        return $this->SppApiservice->send_notif('konfirmasi_pembayaran_spp', 'event_konfirm_spp', $message);
    }



    /** Approve pembayaran transaksi konfirmasi
     * *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve_transaction($id, $id_siswa, $id_akun)
    {
        $response_show_detail = $this->SppApiservice->approve_konfirmasi_pembayaran($id, $id_akun);
        if ($response_show_detail['code'] == '200') {
            //push notifikasi ke pusher
            $message  = [
                'id_siswa' => $id_siswa,
                'message'  => 'Pembayaran telah diterima oleh Administrator / Bendahara sekolah ',
                'tolak'    => false,
                'link'     => route('history-pembayaran-ortu'),
                'kode_trans' => ''
            ];
            $this->SppApiservice->send_notif('spp-channel-ortu', 'spp_event_ortu', $message);

            // push notifikasi save table
            $data_notikasi = array();
            $data_notikasi['id_kelas_siswa'] = $id_siswa;
            $data_notikasi['url'] = $message['link'];
            $data_notikasi['isi'] = $message['message'];
            $data_notikasi['id_sekolah'] = session('id_sekolah');
            $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /** Approve pembayaran transaksi konfirmasi
     * *
     * @return \Illuminate\Http\Response
     */
    public function approve_transaction_pembayaran(Request $request)
    {

        $data = array();
        $data['id'] = $request->id_trans;
        $data['id_akun'] = $request->id_akun;
        $data['operator'] = session('username');
        $data['keterangan'] = 'Lunas';

        $response_show_detail = $this->SppApiservice->approve_konfirmasi_pembayaran(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            //push notifikasi ke pusher
            $message  = [
                'id_siswa' => $request->id_siswa,
                'message'  => 'Pembayaran telah diterima oleh Administrator / Bendahara sekolah ',
                'tolak'    => false,
                'link'     => route('history-pembayaran-ortu'),
                'kode_trans' => ''
            ];
            $this->SppApiservice->send_notif('spp-channel-ortu', 'spp_event_ortu', $message);

            // push notifikasi save table
            $data_notikasi = array();
            $data_notikasi['id_kelas_siswa'] = $request->id_siswa;
            $data_notikasi['url'] = $message['link'];
            $data_notikasi['isi'] = $message['message'];
            $data_notikasi['id_sekolah'] = session('id_sekolah');
            $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

            //kirim struct kwantansi pembayaran

            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /** Reject pembayaran transaksi konfirmasi
     * *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject_transaction($id, $id_siswa)
    {
        $response_show_detail = $this->SppApiservice->reject_konfirmasi_pembayaran($id);
        if ($response_show_detail['code'] == '200') {
            $message  = [
                'id_siswa' => $id_siswa,
                'message'  => 'Pembayaran telah ditolak ',
                'tolak'    => true,
                'link'     => '',
                'kode_trans' => ''
            ];
            $this->SppApiservice->send_notif('spp-channel-ortu', 'spp_event_ortu', $message);
            // push notifikasi save table
            $data_notikasi = array();
            $data_notikasi['id_kelas_siswa'] = $id_siswa;
            $data_notikasi['url'] = '';
            $data_notikasi['isi'] = 'Pembayaran telah diterima';
            $data_notikasi['id_sekolah'] = session('id_sekolah');
            $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function tagihan_by_code($id)
    {
        Session::put('title', 'Tagihan Pembayaran');
        if (empty(session('id_sekolah'))) {
            Session::put('id_sekolah', '1');
        }

        $tagihanspp = $this->SppApiservice->get_transconfirm($id);
        $rekening = $this->SppApiservice->setting_spp(session('id_sekolah'));
        $sekolah = $this->SppApiservice->sekolah_data_by_id(session('id_sekolah'));
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $parse_rekening = $rekening['body']['data'] ?? array();
        $parse_sekolah  = $sekolah['body']['data'] ?? array();
        if (!empty($parse_data)) {
            $profil_ortu = $this->SppApiservice->search_siswa_ortu($parse_data['id_kelas_siswa']);
            $parse_profil_ortu = $profil_ortu['body']['data'] ?? array();
        } else {
            $parse_profil_ortu = array();
        }

        $configApi  = $this->configApi();
        $param      = [
            'tagihan' => $parse_data,
            'profil_ortu' => $parse_profil_ortu,
            'rekening'    => $parse_rekening,
            'sekolah'     =>  $parse_sekolah
        ];
        return view('spp.components.v_pembayaran')->with(array_merge($configApi, $param));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function transfer_by_code($id)
    {
        Session::put('title', 'Pembayaran transfer bank');
        $tagihanspp = $this->SppApiservice->get_transconfirm($id);
        if (empty(session('id_sekolah'))) {
            $rekening = $this->SppApiservice->setting_spp('1');
        } else {
            $rekening = $this->SppApiservice->setting_spp(session('id_sekolah'));
        }
        $parse_rekening =  $rekening['body']['data'] ?? array();
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tagihan' => $parse_data, 'rekening' => $parse_rekening];
        return view('spp.components.v_pembayaran_bank')->with(array_merge($configApi, $param));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function transaksi_success($id, $total, $type, $message)
    {
        Session::put('title', 'Pembayaran Berhasil');
        $configApi  = $this->configApi();
        $param = [
            'kode_transaksi' => $id,
            'total' => number_format(intval($total), 0),
            'type' => $type,
            'pesan' => $message
        ];
        return view('spp.component.transaksi_success')->with(array_merge($configApi, $param));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function get_status_pembayaran($via, $kode)
    {
        $check_status = $this->SppApiservice->paid_spp_konfirmasi($via, $kode);
        $parse_data   = $check_status['body']['data'] ?? '';
        $configApi  = $this->configApi();
        if ($parse_data == 'true') {
            Session::put('title', 'Pembayaran Berhasil');
            $param = ['kode_transaksi' => $kode];
            return view('spp.components.v_transaction_status_succes')->with(array_merge($configApi,$param));
        } else if ($parse_data == 'false') {
            Session::put('title', 'Pembayaran Gagal');
            $param = ['kode_transaksi' => $kode];
            return view('spp.components.v_transaction_status_failed')->with(array_merge($configApi,$param));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function transaksi_success_Transfer($message)
    {
        Session::put('title', 'Pembayaran Berhasil');
        return view('spp.components.transaksi_success_bank')->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cek_statusOrder($id, $role)
    {
        $configApi  = $this->configApi();
        $cek_status = $this->SppApiservice->get_cek_statusOrder($id, session('id_sekolah'));
        $cekdata   = $cek_status['body']['data'] ?? array();
        if ($role == 'ortu') {
            if (!empty($cekdata)) {
                $order_id = $cekdata['order_id'];
                $amount    = number_format($cekdata['gross_amount'], 0);
                $type     = $cekdata['payment_type'];
                $time     = $cekdata['transaction_time'];
                $message  = "Kode Transaksi " . $order_id . " telah melakukan metode pembayaran " . $type . " dengan jumlah nominal " . $amount . " pada waktu " . $time . " . Mohon segera diperiksa!";
                $this->SppApiservice->send_notif('konfirmasi_pembayaran_spp', 'event_konfirm_spp', $message);

                // push notifikasi save table
                $data_notikasi = array();
                $data_notikasi['id_kelas_siswa'] = '';
                $data_notikasi['url'] = route('konfirmasi-transaksi');
                $data_notikasi['isi'] = $message;
                $data_notikasi['id_sekolah'] = session('id_sekolah');
                $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

                // send struk kwantansi pembayaran

            }
        }

        $param      = ['transaction' => $cekdata, 'id_trans' => $id];
        return view('spp.components.v_transaction_status')->with(array_merge($configApi, $param));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function change_metode_bayar(Request $request)
    {
        $kode_trans = $request->kode_trans;
        $metode     = $request->metode;
        if ($metode == 'transfer_bank') {
            return response()->json(
                [
                    'message' => 'success',
                    'data' => 'redirect',
                    'info' => 'success',
                    'metode' => $metode,
                    'url'  => route('transfer_by_code', ['id' => $kode_trans])
                ]
            );
        } else {
            $change_mode = $this->SppApiservice->change_metode($kode_trans, $metode);
            if ($change_mode['code'] == '200') {
                return response()->json(
                    [
                        'message' => $change_mode['body']['message'],
                        'data' => $change_mode['body']['data'] ?? array(),
                        'info' => 'success',
                        'metode' => $metode,
                        'url'  => ''
                    ]
                );
            } else {
                return response()->json(
                    [
                        'message' => $change_mode['body']['message'],
                        'data' => $change_mode['body']['data'] ?? array(),
                        'info' => 'error',
                        'url'  => '',
                        'metode' => $metode,
                    ]
                );
            }
        }
    }

    /** get curl force download */

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download_file($id)
    {
        $konfirmasi = $this->SppApiservice->get_transconfirm_detail($id);
        $result = $konfirmasi['body']['data'];
        $nama_file = explode('/', $result['file']);
        $nama = end($nama_file);
        $fileSource = $result['file'];
        $fileName   = $nama;
        $PecahStr = explode(".", $fileName);
        $headers = ['Content-Type:' . $PecahStr[1]];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers)->deleteFileAfterSend(true);
    }
}
