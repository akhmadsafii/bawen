<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class HutangPiutanTrans extends Controller
{
    //
    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Transaksi Hutang');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $rekening_akun = $this->SppApiservice->get_akun_hutang();
        $rekening_sumber = $this->SppApiservice->get_akun_harta();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun  = $rekening_akun['body']['data'] ?? array();
        $parse_akun_sumber  = $rekening_sumber['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $filterBy   = "piutang";
        $filter_akun_sumber = array_filter($parse_akun_sumber, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) === FALSE;
        });
        $param      = [
            'tahun_ajaran' => $parse_tahun,
            'akun' => $parse_akun,
            'sumber' => $filter_akun_sumber,
            'sumberx' => $filter_akun_sumber
        ];
        return view('spp.components.v_transaksi_hutang')->with(array_merge($configApi, $param));
    }

    /** store hutang  */
    public function store_transaksi_hutang(Request $request)
    {
        if (!empty($request->akun_hutang)) {

            $datapost = array();
            $postinput = array();
            $nominal_total = 0;

            $nominal    = str_replace(",", "", $request->nominal);
            if (empty($nominal)) {
                return back()->with('error', 'Nominal harus di isi !');
            }

            foreach ($request->akun_hutang as $key => $val) {
                $data = array();
                $data['id_akun'] = $val;
                if (empty($request->catatan[$key])) {
                    $data['keterangan']      = "-";
                } else {
                    $data['keterangan']      = $request->catatan[$key];
                }
                $data['nominal']      = $nominal[$key];
                $nominal_total        += intval($nominal[$key]);
                $datapost[] = $data;
            }

            $postinput['detail_hutangs']  = $datapost;
            $postinput['operator']     = session('username');
            $postinput['id_akun']      = $request->sumber;
            $postinput['kreditur']     = $request->kreditor;
            $postinput['telepon']      = $request->no_kontak;
            $postinput['jenis_angsuran'] = $request->jenis_angsuran;
            $postinput['keterangan']   = $request->keterangan;
            $postinput['tgl_transaksi']  = $request->tgl_trans;
            $postinput['tgl_jatuh_tempo'] = $request->jatuh_tempo;
            $postinput['tahun_ajaran'] = $request->tahun_ajaran;
            $postinput['nominal']      = $nominal_total;
            $postinput['id_sekolah']  = session('id_sekolah');

            //dd($postinput);

            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $postinput['path'] = $path;
                $response_update = $this->SppApiservice->post_hutang_with_file(json_encode($postinput));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->post_hutang(json_encode($postinput));
            }

            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

    /** store cicilan hutang  */
    public function store_cicilan_hutang(Request $request)
    {
        $datapost = array();
        $datapost['operator'] = session('username');
        $datapost['id_akun']  = $request->akun_sumber;
        $datapost['id_hutang'] = $request->id_hutang;
        $datapost['nominal']    = str_replace(",", "", $request->nominal_pembayaran);
        $datapost['keterangan'] = $request->catatan_cicilan;
        $datapost['id_sekolah'] = session('id_sekolah');
        $datapost['tgl_transaksi'] = $request->tgl_trans_pembayaran;
        $datapost['tahun_ajaran']  = $request->tahun_ajaranx;

        if ($files = $request->file('bukti_pembayaran')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bukti_nota/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $datapost['path'] = $path;
            $response_update = $this->SppApiservice->post_hutang_cicilan_with_file(json_encode($datapost));
            if (!empty($path)) {
                File::delete($path);
            }
        } else {
            $response_update = $this->SppApiservice->post_hutang_cicilan(json_encode($datapost));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store cicilan hutang  */
    public function store_cicilan_piutang(Request $request)
    {
        $datapost = array();
        $datapost['operator'] = session('username');
        $datapost['id_akun']  = $request->akun_sumber;
        $datapost['id_piutang'] = $request->id_piutang;
        $datapost['nominal']    = str_replace(",", "", $request->nominal_pembayaran);
        $datapost['keterangan'] = $request->catatan_cicilan;
        $datapost['id_sekolah'] = session('id_sekolah');
        $datapost['tgl_transaksi'] = $request->tgl_trans_pembayaran;
        $datapost['tahun_ajaran']  = $request->tahun_ajaranx;

        if ($files = $request->file('bukti_pembayaran')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bukti_nota/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $datapost['path'] = $path;
            $response_update = $this->SppApiservice->post_piutang_cicilan_with_file(json_encode($datapost));
            if (!empty($path)) {
                File::delete($path);
            }
        } else {
            $response_update = $this->SppApiservice->post_piutang_cicilan(json_encode($datapost));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store hutang  */
    public function store_transaksi_piutang(Request $request)
    {
        if (!empty($request->akun_piutang)) {

            $datapost = array();
            $postinput = array();
            $nominal_total = 0;

            $nominal    = str_replace(",", "", $request->nominal);
            if (empty($nominal)) {
                return back()->with('error', 'Nominal harus di isi !');
            }

            foreach ($request->akun_piutang as $key => $val) {
                $data = array();
                $data['id_akun'] = $val;
                if (empty($request->catatan[$key])) {
                    $data['keterangan']      = "-";
                } else {
                    $data['keterangan']      = $request->catatan[$key];
                }
                $data['nominal']      = $nominal[$key];
                $nominal_total        += intval($nominal[$key]);
                $datapost[] = $data;
            }

            $postinput['detail_hutangs']  = $datapost;
            $postinput['operator']     = session('username');
            $postinput['id_akun']      = $request->sumber;
            $postinput['debitur']     = $request->debitor;
            $postinput['telepon']      = $request->no_kontak;
            $postinput['jenis_angsuran'] = $request->jenis_angsuran;
            $postinput['keterangan']   = $request->keterangan;
            $postinput['tgl_transaksi']  = $request->tgl_trans;
            $postinput['tgl_jatuh_tempo'] = $request->jatuh_tempo;
            $postinput['tahun_ajaran'] = $request->tahun_ajaran;
            $postinput['nominal']      = $nominal_total;
            $postinput['id_sekolah']  = session('id_sekolah');

            //dd($postinput);

            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $postinput['path'] = $path;
                $response_update = $this->SppApiservice->post_piutang_with_file(json_encode($postinput));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->post_piutang(json_encode($postinput));
            }
            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']."<a href=".route('print_piutang',['id'=>$response_update['body']['data']['id']])."");
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_list_hutang(Request $request)
    {
        $pengaturan = $this->SppApiservice->list_hutang();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $sisa = 0;
                    $nominal = intval($data['nominal']);
                    $angsuran = intval($data['total_angsuran']);
                    $sisa     = intval($nominal) - intval($angsuran);

                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show hutang transaksi " title="lihat histori transaksi" data-id="' . $data['id'] . '" data-id_detail="' . $data['id_transaksi_detail'] . '"><i class="fa fa-eye"></i> Detail </button>&nbsp;';
                    if ($sisa > 0) {
                        $button .= '<a  class="btn btn-warning btn-sm hutang history transaksi pembayaran " id="pembayaran" title="Transaksi Pembayaran"  data-id="' . $data['id'] . '"  data-id_detail="' . $data['id_transaksi_detail'] . '" style="color: #fff;"><i class="fa fa-money"></i> Bayar </a>&nbsp;';
                    }
                    $button .= '<a  class="btn btn-info btn-sm hutang history transaksi print" title="print histori transaksi "  data-id="' . $data['id'] . '" data-id_detail="' . $data['id_transaksi_detail'] . '" style="color: #fff;display:none;"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('jenis_angsuran', function ($row) {
                return ucfirst($row['jenis_angsuran']);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('tgl_jatuh_tempo', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_jatuh_tempo'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('total_angsuran', function ($row) {
                return number_format($row['total_angsuran'], 0);
            });

            $table->editColumn('sisa_angsuran', function ($row) {
                $sisa = 0;
                $nominal = intval($row['nominal']);
                $angsuran = intval($row['total_angsuran']);
                $sisa     = intval($nominal) - intval($angsuran);
                return number_format($sisa, 0);
            });

            $table->editColumn('status', function ($row) {
                $sisa = 0;
                $nominal = intval($row['nominal']);
                $angsuran = intval($row['total_angsuran']);
                $sisa     = intval($nominal) - intval($angsuran);
                if ($sisa > 0) {
                    return "<span class='text-red'>Belum Lunas</span>";
                } else {
                    return "<span class='text-success'>Lunas</span>";
                }
            });

            $table->rawColumns(['action', 'nominal', 'tgl_transaksi', 'total_angsuran', 'tgl_jatuh_tempo', 'sisa_angsuran', 'status', 'jenis_angsuran']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_list_piutang(Request $request)
    {
        $pengaturan = $this->SppApiservice->list_piutang();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $sisa = 0;
                    $nominal = intval($data['nominal']);
                    $angsuran = intval($data['total_angsuran']);
                    $sisa     = intval($nominal) - intval($angsuran);

                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show hutang transaksi " title="lihat histori transaksi" data-id="' . $data['id'] . '" data-id_detail="' . $data['id_transaksi_detail'] . '"><i class="fa fa-eye"></i> Detail </button>&nbsp;';
                    if ($sisa > 0) {
                        $button .= '<a  class="btn btn-warning btn-sm hutang history transaksi pembayaran " id="pembayaran" title="Transaksi Pembayaran"  data-id="' . $data['id'] . '"  data-id_detail="' . $data['id_transaksi_detail'] . '" style="color: #fff;"><i class="fa fa-money"></i> Bayar </a>&nbsp;';
                    }
                    $button .= '<a  href="' . route('print_piutang', ['id' => $data['id']]) . '" target="_blank"  class="btn btn-info btn-sm hutang history transaksi print" title="print histori transaksi "  data-id="' . $data['id'] . '" data-id_detail="' . $data['id_transaksi_detail'] . '" style="color: #fff;"><i class="fa fa-print"></i> Cetak </a>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('jenis_angsuran', function ($row) {
                return ucfirst($row['jenis_angsuran']);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('tgl_jatuh_tempo', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_jatuh_tempo'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('total_angsuran', function ($row) {
                return number_format($row['total_angsuran'], 0);
            });

            $table->editColumn('sisa_angsuran', function ($row) {
                $sisa = 0;
                $nominal = intval($row['nominal']);
                $angsuran = intval($row['total_angsuran']);
                $sisa     = intval($nominal) - intval($angsuran);
                return number_format($sisa, 0);
            });

            $table->editColumn('status', function ($row) {
                $sisa = 0;
                $nominal = intval($row['nominal']);
                $angsuran = intval($row['total_angsuran']);
                $sisa     = intval($nominal) - intval($angsuran);
                if ($sisa > 0) {
                    return "<span class='text-red'>Belum Lunas</span>";
                } else {
                    return "<span class='text-success'>Lunas</span>";
                }
            });

            $table->rawColumns(['action', 'nominal', 'tgl_transaksi', 'total_angsuran', 'tgl_jatuh_tempo', 'sisa_angsuran', 'status', 'jenis_angsuran']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show_by_kode_ajax_detail($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->get_detail_transaksi_bycode($id);

        $result = $response_show_detail['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                if (!empty($row['tgl_bayar'])) {
                    return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['nominal', 'tgl_transaksi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * * @param  int  $tahun
     * @return \Illuminate\Http\Response
     */

    public function show_hutang_by_id($id)
    {
        $response_show_detail = $this->SppApiservice->detail_hutang_by_id($id);

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * * @param  int  $tahun
     * @return \Illuminate\Http\Response
     */

    public function show_cicilan_hutang_by_id($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->get_cicilan_hutang_by_id($id);
        $result = $response_show_detail['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                if (!empty($row['tgl_transaksi'])) {
                    return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->editColumn('file', function ($row) {
                if(!empty($row['file'])){
                    return '<a class="btn btn-info" href="' . route('download-bukti-pembayaran-hutang', ['id' => $row['id'], 'namafile' => basename($row['file'])]) . '"><i class="fa fa-download"></i></a>';
                }else{
                    return "-";
                }

            });

            $table->rawColumns(['nominal', 'tgl_transaksi', 'file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * * @param  int  $tahun
     * @return \Illuminate\Http\Response
     */

    public function show_cicilan_piutang_by_id($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->get_cicilan_piutang_by_id($id);
        $result = $response_show_detail['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                if (!empty($row['tgl_transaksi'])) {
                    return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->editColumn('file', function ($row) {
                if(!empty($row['file'])){
                    return '<a class="btn btn-info" href="' . route('download-bukti-pembayaran-piutang', ['id' => $row['id'], 'namafile' => basename($row['file'])]) . '"><i class="fa fa-download"></i></a>';
                }else{
                    return "-";
                }
            });

            $table->rawColumns(['nominal', 'tgl_transaksi', 'file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** force download bukti pembayaran  */
    public function download_bukti_pembayaran_hutang($id, $namaFile)
    {
        $post = $this->SppApiservice->get_cicilan_hutang_detailid($id);
        if (!empty($post['body']['data'])) {
            $PecahStr = explode(".", $namaFile);
            $headers = ['Content-Type:' . $PecahStr[1]];
            $pathToFile = storage_path('app/' . $namaFile);
            $getContent = $this->curl_get_file_contents($post['body']['data']['file']);
            file_put_contents($pathToFile, $getContent);
            return response()->download($pathToFile, $namaFile, $headers)->deleteFileAfterSend(true);
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /** force download bukti pembayaran  */
    public function download_bukti_pembayaran_piutang($id, $namaFile)
    {
        $post = $this->SppApiservice->get_cicilan_piutang_detailid($id);
        if (!empty($post['body']['data'])) {
            $PecahStr = explode(".", $namaFile);
            $headers = ['Content-Type:' . $PecahStr[1]];
            $pathToFile = storage_path('app/' . $namaFile);
            $getContent = $this->curl_get_file_contents($post['body']['data']['file']);
            file_put_contents($pathToFile, $getContent);
            return response()->download($pathToFile, $namaFile, $headers)->deleteFileAfterSend(true);
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /** get curl force download */

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }


    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * * @param  int  $tahun
     * @return \Illuminate\Http\Response
     */

    public function show_piutang_by_id($id)
    {
        $response_show_detail = $this->SppApiservice->detail_piutang_by_id($id);

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index_piutang()
    {
        Session::put('title', 'Transaksi Piutang');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $rekening_akun = $this->SppApiservice->get_akun_harta();
        //$rekening_sumber = $this->SppApiservice->get_akun_harta();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun  = $rekening_akun['body']['data'] ?? array();
        //$parse_akun_sumber  = $rekening_sumber['body']['data'] ?? array();
        $configApi  = $this->configApi();

        $filterBy   = "piutang";
        $filter_akun_piutang = array_filter($parse_akun, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) !== FALSE;
        });

        $filter_akun_sumber = array_filter($parse_akun, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) === FALSE;
        });

        $param      = [
            'tahun_ajaran' => $parse_tahun,
            'akun' => $filter_akun_piutang,
            'sumber' => $filter_akun_sumber,
            'sumberx' => $filter_akun_sumber
        ];
        return view('spp.components.v_transaksi_piutang')->with(array_merge($configApi, $param));
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * * @param  int  $tahun
     * @return \Illuminate\Http\Response
     */
    public function print_piutang($id)
    {
        Session::put('title', 'Print Transaksi Piutang');
        $piutang = $this->SppApiservice->detail_piutang_by_id($id);
        if (!empty($piutang['body']['data'])) {
            $detail_item_piutang = $this->SppApiservice->get_detail_transaksi_bycode($piutang['body']['data']['kode_transaksi']);
            $cicilan = $this->SppApiservice->get_cicilan_piutang_by_id($id);
            $param   = [
                'data_piutang' => $piutang['body']['data'] ?? array(),
                'data_detail_item' => $detail_item_piutang['body']['data'] ?? array(),
                'cicilan_pembayaran' => $cicilan['body']['data'] ?? array()
            ];
            return view('spp.components.v_print_piutang')->with($param);
        } else {
            return redirect()->route('transaksi-piutang')->with('error', ' data tidak ditemukan  !');
        }
    }
}
