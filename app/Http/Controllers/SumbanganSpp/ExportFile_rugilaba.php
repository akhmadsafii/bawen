<?php

namespace App\Http\Controllers\SumbanganSpp;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use App\ApiService\SumbanganSpp\SppApiservice;


class ExportFile_rugilaba  implements FromView, ShouldAutoSize, WithHeadings, WithEvents
{
    //
    protected $SppApiservice;
    public $bulan;
    public $mode;
    public $tahun;
    /**
     * __construct function new class Api
     */

    public function __construct($bulan, $tahun, $mode)
    {
        $this->SppApiservice = new SppApiservice();
        $this->bulan         = $bulan;
        $this->tahun         = $tahun;
        $this->mode          = $mode;
    }

    public function headings(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


    public function view(): View
    {

        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $index_bulan  = array_keys($list_bulan, $this->bulan);
        $bulan_par    = intval($index_bulan[0]);
        //dd($list_bulan[$bulan_par]);
        $bulanx =  \Carbon\Carbon::now()->isoFormat('MMMM');
        if ($this->bulan == '') {
            $rugilaba     = $this->SppApiservice->report_labarugi($bulanx,  $this->tahun);
        } else {
            $rugilaba     = $this->SppApiservice->report_labarugi($list_bulan[$bulan_par],  $this->tahun);
        }

        $parse_rugilaba = $rugilaba['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 1950; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        if ($this->mode == 2) {
            $report = $this->filter_group_kategori($parse_rugilaba);
        } else if ($this->mode == 3) {
            $report = $this->filter_group_subkategori($parse_rugilaba);
        } else if ($this->mode == 1) {
            $report   =  $parse_rugilaba;
        }

        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'bulan' => $list_bulan[$bulan_par],
            'tahun' =>  $this->tahun,
            'tahunlist' => $tahun_data,
            'report' => $report,
            'mode' => $this->mode,
            'index_bulan' => $bulan_par,
        ];
        return view('spp.components.v_reportlabarugi_custom_printx')->with(array_merge($param));
    }

    /** mode laba rugi filter kategori */
    private function filter_group_kategori($data)
    {
        $group = array();
        $total_penggunaan = 0;
        $total_pendapatan = 0;
        foreach ($data as $key => $x) {
            $array_item = array();
            if ($x['id'] == '4') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_pendapatan  += intval($o['nominal']);
                    }
                }

                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_pendapatan);
            }
            if ($x['id'] == '5') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_penggunaan  += intval($o['nominal']);
                    }
                }
                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_penggunaan);
            }
            $group[] = $array_item;
        }
        return $group;
    }

    /** mode laba rugi filter kategori */
    private function filter_group_subkategori($data)
    {
        $group = array();
        $total_penggunaan = 0;
        $total_pendapatan = 0;
        foreach ($data as $key => $x) {
            $array_item = array();
            $sub_akun   = array();
            if ($x['id'] == '4') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_pendapatan  += intval($o['nominal']);
                        $sub_akun[]  = $o;
                    }
                }

                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_pendapatan);
                $array_item['sub_akun'] = $sub_akun;
            }
            if ($x['id'] == '5') {
                foreach ($x['sub_akun_kategori']  as $k) {
                    foreach ($k['akun'] as $o) {
                        $total_penggunaan  += intval($o['nominal']);
                        $sub_akun[]  = $o;
                    }
                }
                $array_item['id'] = $x['id'];
                $array_item['kode'] = $x['kode'];
                $array_item['nama'] = $x['nama'];
                $array_item['nominal'] = intval($total_penggunaan);
                $array_item['sub_akun'] = $sub_akun;
            }
            $group[] = $array_item;
        }

        return $group;
    }
}
