<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AkuntansiController extends Controller
{
    protected $SppApiservice;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Akun Kategori');
        $configApi  = $this->configApi();
        $param      = [];
        return view('spp.components.v_akun_kategori_index')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexsub()
    {
        //
        Session::put('title', 'Akun Sub Kategori');
        $kategori_akun = $this->SppApiservice->akuntansi_kategori();
        $parse_kategori = $kategori_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['kategori' => $parse_kategori];
        return view('spp.components.v_akun_subkategori_index')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function akuntansi_akun()
    {
        Session::put('title', 'Akuntansi Akun');
        $kategori_akun = $this->SppApiservice->akuntansi_kategori();
        $subkategori_akun = $this->SppApiservice->akuntansi_subkategori();
        $parse_kategori = $kategori_akun['body']['data'] ?? array();
        $parse_subkategori = $subkategori_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
            'kategori_akun' => $parse_kategori,
            'subkategori_akun' => $parse_subkategori,
        ];
        return view('spp.components.v_akun_index')->with(array_merge($configApi, $param));
    }

    /** transaksi jurnal */

    public function TransaksiJurnal()
    {
        //
        Session::put('title', 'Transaksi Jurnal');
        $rekening_akun = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun    = $rekening_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
            'rekening_akun' => $parse_akun,
            'tahun_ajaran' => $parse_tahun_ajaran,
        ];
        return view('spp.components.v_transaksi_jurnal')->with(array_merge($configApi, $param));
    }

    /** transaksi saldo akun  */
    public function TransaksiSaldoAkun()
    {
        Session::put('title', 'Transaksi Saldo Awal Akun');
        $rekening_akun = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun    = $rekening_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2000; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param      = [
            'rekening_akun' => $parse_akun,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'tahunlist' => $tahun_data,
            'tahun' => session('tahun')
        ];

        return view('spp.components.v_transaksi_saldo_akun')->with(array_merge($configApi, $param));
    }

    /** transaksi saldo akun  */
    public function TransaksiSaldoAkunMulti()
    {
        Session::put('title', 'Transaksi Saldo Awal Akun');
        $rekening_akun = $this->SppApiservice->saldo_awal_bysekolah(session('tahun'));
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun    = $rekening_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2000; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param      = [
            'rekening_akun' => $parse_akun,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'tahunlist' => $tahun_data,
            'tahun' => session('tahun')
        ];

        return view('spp.components.v_transaksi_saldo_akun_multi')->with(array_merge($configApi, $param));
    }

    /** transaksi saldo akun  */
    public function TransaksiSaldoAkunMultiTahun($tahun)
    {
        Session::put('title', 'Transaksi Saldo Awal Akun');
        $rekening_akun = $this->SppApiservice->saldo_awal_bysekolah($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun    = $rekening_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2000; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param      = [
            'rekening_akun' => $parse_akun,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'tahunlist' => $tahun_data,
            'tahun' => $tahun
        ];

        return view('spp.components.v_transaksi_saldo_akun_multi')->with(array_merge($configApi, $param));
    }

    /** display history transaksi jurnal  */
    public function history_jurnal()
    {
        Session::put('title', 'Histori transaksi jurnal ');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = [
            'tahun_ajaran' => $parse_tahun_ajaran,
        ];

        return view('spp.components.v_history_jurnal')->with(array_merge($configApi, $param));
    }

    /** ajax data history jurnal */
    public function ajax_data_history_jurnal(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi_jurnal();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history akuntansi jurnal " data-id="' . $data['kode'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    //$button .= '<a  class="btn btn-warning btn-sm edit akuntansi jurnal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('no_referensi', function ($row) {
                return $row['no_referensi'] ?? '-';
            });

            $table->editColumn('tgl_bayar', function ($row) {
                if (!empty($row['tgl_bayar'])) {
                    return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar','no_referensi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax data history jurnal */
    public function ajax_data_history_jurnal_bydate($id, Request $request)
    {
        $pengaturan = $this->SppApiservice->report_transaksijurnal_by_date($id);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history akuntansi jurnal " data-id="' . $data['kode'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    //$button .= '<a  class="btn btn-warning btn-sm edit akuntansi jurnal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('no_referensi', function ($row) {
                return $row['no_referensi'] ?? '-';
            });

            $table->editColumn('tgl_bayar', function ($row) {
                if (!empty($row['tgl_bayar'])) {
                    return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar','no_referensi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** detail jurnal by transaksi kode  */
    public function ajax_data_detailjurnal_by_transcode($id, Request $request)
    {
        $pengaturan = $this->SppApiservice->get_detail_jurnalbykode($id);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('akun', function ($data) {
                    return $data['kode_akun'] . "-" . $data['akun'];
                });

            $table->editColumn('debet', function ($row) {
                return number_format($row['debet'], 0);
            });

            $table->editColumn('kredit', function ($row) {
                return number_format($row['kredit'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                if (!empty($row['tgl_bayar'])) {
                    return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->editColumn('keterangan', function ($row) {
                if (!empty($row['keterangan'])) {
                    return $row['keterangan'];
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['debit', 'kredit', 'tgl_bayar', 'akun', 'keterangan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data saldo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_saldoakun(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_saldo_akun();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi saldoakun " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi saldoakun"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('saldo_awal', function ($row) {
                return number_format($row['saldo_awal'], 0);
            });

            $table->editColumn('saldo_akhir', function ($row) {
                return number_format($row['saldo_akhir'], 0);
            });

            $table->editColumn('updated_at', function ($row) {
                if (!empty($row['updated_at'])) {
                    return \Carbon\Carbon::parse($row['updated_at'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['action', 'saldo_awal', 'saldo_akhir', 'updated_at']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_saldoAkun()
    {
        //
        Session::put('title', 'Histori Saldo Akun');
        $rekening_akun = $this->SppApiservice->akuntansi_rekening();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_akun    = $rekening_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 1950; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param      = [
            'rekening_akun' => $parse_akun,
            'tahun_ajaran' => $parse_tahun_ajaran,
            'tahunlist' => $tahun_data,
            'tahun' => session('tahun')
        ];
        return view('spp.components.v_saldoakun_index')->with(array_merge($configApi, $param));
    }

    /** store saldo Akun
     * * @param  \Illuminate\Http\Request  $request
     */
    public function store_saldo_akun(Request $request)
    {
        if (!empty($request->akun)) {
            $saldo_awal    = str_replace(",", "", $request->saldo_awal);
            $saldo_akhir     = str_replace(",", "", $request->saldo_akhir);

            $datapost = array();
            $postinput = array();

            foreach ($request->akun as $key => $val) {
                $data = array();
                $data['id_akun'] = $val;
                $data['kode']   = $request->kode[$key];
                $data['saldo_awal'] = $saldo_awal[$key];
                $data['saldo_akhir'] = $saldo_akhir[$key];
                $data['tahun'] = $request->tahun;
                $data['id_sekolah']  = session('id_sekolah');

                $datapost[] = $data;
            }

            $postinput['saldo_akuns'] = $datapost;

            //dd($postinput);

            $response_update = $this->SppApiservice->transaksi_saldo_akun(json_encode($postinput));

            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

    /** store saldo Akun
     * * @param  \Illuminate\Http\Request  $request
     */
    public function store_saldo_akunmulti(Request $request)
    {
        if (!empty($request->id_akun)) {
            $saldo_awal    = str_replace(",", "", $request->saldo_awal);
            $saldo_akhir     = str_replace(",", "", $request->saldo_akhir);

            $datapost = array();
            $postinput = array();

            foreach ($request->id_akun as $key => $val) {
                $data = array();
                $data['id_akun'] = $val;
                $data['kode']   = $request->kode[$key];
                $data['saldo_awal'] = $saldo_awal[$key];
                $data['saldo_akhir'] = $saldo_akhir[$key];
                $data['tahun'] = $request->tahun;
                $data['id_sekolah']  = session('id_sekolah');

                $datapost[] = $data;
            }

            $postinput['saldo_akuns'] = $datapost;

            //dd($postinput);

            $response_update = $this->SppApiservice->transaksi_saldo_akun(json_encode($postinput));

            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

    /** update store saldo akun
     * * @param  int  $id
     * * @param  \Illuminate\Http\Request  $request
     */
    public function update_saldo_akun(Request $request, $id)
    {
        if (!empty($request->akun)) {
            $saldo_awal    = str_replace(",", "", $request->saldo_awal);
            $saldo_akhir   = str_replace(",", "", $request->saldo_akhir);
            $data['id_akun'] = $request->akun;
            $data['saldo_awal'] = $saldo_awal;
            $data['saldo_akhir'] = $saldo_akhir;
            $data['tahun'] = $request->tahun;
            $data['id_sekolah']  = session('id_sekolah');
            $response_update = $this->SppApiservice->update_saldo_akun(json_encode($data), $id);

            if ($response_update['code'] == '200') {
                return response()->json(
                    [
                        'message' => $response_update['body']['message'],
                        'data' => $response_update['body']['data'] ?? array(),
                        'info' => 'success',
                    ]
                );
            } else {
                return response()->json(
                    [
                        'message' => $response_update['body']['message'],
                        'data' => $response_update['body']['data'] ?? array(),
                        'error' => 'error',
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'message' => 'All Fields required!',
                    'data' => '',
                    'error' => 'error',
                ]
            );
        }
    }

    /**
     * Display Saldo Akun  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_saldo_akun($id)
    {
        $akun = $this->SppApiservice->show_saldo_akun($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store transaksi jurnal  */
    public function store_transaksi_jurnal(Request $request)
    {
        $cekarray = array();
        $cekarray_ = array();
        $merge     = array();
        $total_nominal = 0;
        $check_deskripsi = false;
        $path            = '';
        if (!empty($request->akun)) {
            $data_transaksi  = array();
            $data_transaksi['no_referensi'] = $request->no_referensi ?? '-';
            $data_transaksi['operator']  = session('username');
            $data_transaksi['tgl_bayar'] = $request->tgl_bayar;
            $data_transaksi['keterangan'] = $request->catatan ?? '-';
            $data_transaksi['tahun_ajaran'] = $request->tahun_ajaran;
            $data_transaksi['id_sekolah']   = session('id_sekolah');
            $kredit    = str_replace(",", "", $request->kredit);
            $debit     = str_replace(",", "", $request->debit);


            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data_transaksi['path'] = $path;
            }

            foreach ($request->akun as $key => $val) {
                $item = array();
                $detailx = array();
                //cek value deskripsi akun sangat penting
                if (empty($request->deskripsi[$key])) {
                    $check_deskripsi = true;
                }

                $total_nominal += intval($debit[$key]) ?? intval($kredit[$key]);
                $item['id_akun'] = $val;
                $item['nominal'] = intval($debit[$key]) ?? intval($kredit[$key]);
                $item['keterangan'] = $request->deskripsi[$key] ?? '-';
                $item['debet'] = $debit[$key];
                $item['kredit'] = $kredit[$key];
                $cekarray[] = $item;
            }


            $data_transaksi['nominal'] = $total_nominal;
            $data_transaksi['details'] = $cekarray;


            //dd($data_transaksi);

            if ($request->file('image')) {
                $response_update = $this->SppApiservice->transaksi_jurnal_with_file(json_encode($data_transaksi));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->transaksi_jurnal(json_encode($data_transaksi));
            }

            if ($response_update['code'] == '200') {

                //dd($response_update['body']);

                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form nya !');
        }

        if ($check_deskripsi == true) {
            return back()->with('error', 'cek kembali form deskripsi akun nya !');
        }
    }


    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_akun(Request $request)
    {
        $pengaturan = $this->SppApiservice->akuntansi_rekening();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi akun " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi akun"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('jenis_transaksi', function ($row) {
                switch ($row['jenis_transaksi']) {
                    case 'input':
                        return 'Pemasukan';
                        break;
                    case 'output':
                        return 'Pengeluaran';
                        break;
                    case 'liabilities':
                        return 'Kewajiban';
                        break;
                    default:
                        '';
                }
            });

            $table->rawColumns(['action', 'jenis_transaksi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Akun Kategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_akuntansi_akun($id)
    {
        $akun = $this->SppApiservice->akuntansi_rekening_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display kode akun the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_akuntansi_kodeakun($id)
    {
        $akun = $this->SppApiservice->show_akuntansi_kode_akun($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display kode akun the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_akuntansi_kodesubakun($id)
    {
        $akun = $this->SppApiservice->show_akuntansi_kode_subkategori($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_kategori(Request $request)
    {
        $pengaturan = $this->SppApiservice->akuntansi_kategori();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi kategori " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi kategori"  data-id="' . $data['id'] . '" style="color: #fff;"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_subkategori(Request $request)
    {
        $pengaturan = $this->SppApiservice->akuntansi_subkategori();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi subkategori " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi subkategori"  data-id="' . $data['id'] . '" style="color: #fff;"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Akun Kategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_akuntansi_subkabyAK($id)
    {
        $akun = $this->SppApiservice->show_akuntansi_subkategori_byAk($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store update akun  a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update_akun(Request $request, $id)
    {
        // dd($request);
        //
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'id_kategori' => 'required',
            'id_subkategori' => 'required',
            //'tipe_transaksi' => 'required'
        ], [
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
            'id_kategori.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'id_subkategori.required' => 'Pilih terlebih dahulu Sub Akun Kategori !',
            //'tipe_transaksi.required' => 'Pilih terlebih dahulu Tipe Transaksi !',
        ]);

        $data = array();
        $data['kode'] = $request->kode;
        $data['nama'] = $request->nama;
        $data['id_akun_kategori'] = $request->id_kategori;
        $data['id_sub_akun_kategori'] = $request->id_subkategori;
        $data['jenis_transaksi']       = $request->tipe_transaksi;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->update_akuntansi_rekening(json_encode($data), $id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_akun_akuntansi(Request $request)
    {
        $request->validate([
            'kode_post' => 'required',
            'nama_post' => 'required',
            'id_kategori_post' => 'required',
            'id_subkategori_post' => 'required',
            //'tipe_transaksi_post' => 'required'
        ], [
            'kode_post.required' => 'Kode tidak boleh kosong!',
            'nama_post.required' => 'Nama tidak boleh kosong!',
            'id_kategori_post.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'id_subkategori_post.required' => 'Pilih terlebih dahulu Sub Akun Kategori !',
            //'tipe_transaksi_post.required' => 'Pilih terlebih dahulu Tipe Transaksi !',
        ]);
        $data = array();
        $data['kode'] = $request->kode_post;
        $data['nama'] = $request->nama_post;
        $data['id_akun_kategori'] = $request->id_kategori_post;
        $data['id_sub_akun_kategori'] = $request->id_subkategori_post;
        $data['jenis_transaksi']       = $request->tipe_transaksi_post ?? '';
        $data['id_sekolah'] = session('id_sekolah');
        $response_show_detail = $this->SppApiservice->post_akuntansi_rekening(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_kategori(Request $request)
    {
        //
        $request->validate([
            'kodepost' => 'required',
            'namapost' => 'required'
        ], [
            'kodepost.required' => 'Kode tidak boleh kosong!',
            'namapost.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode'] = $request->kodepost;
        $data['nama'] = $request->namapost;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->post_akuntansi_kategori(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_subkategori(Request $request)
    {
        $request->validate([
            'id_kategori' => 'required',
            'namapost' => 'required',
            'kodepost' => 'required'
        ], [
            'id_kategori.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'namapost.required' => 'Nama tidak boleh kosong!',
            'kodepost.required' => 'Kode tidak boleh kosong!',
        ]);

        $data = array();
        $data['id_akun_kategori'] = $request->id_kategori;
        $data['nama'] = $request->namapost;
        $data['kode'] = $request->kodepost;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->post_akuntansi_subkategori(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display Akun Kategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_kategori($id)
    {
        $notif = $this->SppApiservice->show_akuntansi_kategori($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_subkategori($id)
    {
        $notif = $this->SppApiservice->show_akuntansi_subkategori($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_kategori(Request $request, $id)
    {
        //
        $request->validate([
            'kode' => 'required',
            'nama' => 'required'
        ], [
            //'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode'] = $request->kode;
        $data['nama'] = $request->nama;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->update_akuntansi_kategori(json_encode($data), $id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_subkategori(Request $request, $id)
    {
        $request->validate([
            'id_kategori_edit' => 'required',
            'nama_edit' => 'required'
        ], [
            'id_kategori_edit.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'nama_edit.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['id_akun_kategori'] = $request->id_kategori_edit;
        $data['nama'] = $request->nama_edit;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->update_akuntansi_subkategori(json_encode($data), $id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_penyusutan()
    {
        //
        Session::put('title', 'Transaksi Penyusutan Aktiva ');
        $akun_aktiva = $this->SppApiservice->get_akun_aktiva();
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
            'akun_aktiva' => $parse_aktiva,
        ];
        return view('spp.components.v_transaksi_penyusutan')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_pembelian_asset()
    {
        //
        Session::put('title', 'Transaksi Pembelian Asset');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $rekening_sumber = $this->SppApiservice->get_akun_harta();
        $akun_aktiva_asset = $this->SppApiservice->get_asset_tetap();
        $akun_aktiva = $this->SppApiservice->get_akun_aktiva();
        $parse_akun_sumber  = $rekening_sumber['body']['data'] ?? array();
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $parse_aktiva_asset = $akun_aktiva_asset['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $filterBy   = "piutang";
        $filter_akun_sumber = array_filter($parse_akun_sumber, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) === FALSE;
        });


        $filterByAsset   = "penyusutan";
        $filter_akun_aktiva = array_filter($parse_aktiva, function ($option) use ($filterByAsset) {
            return strpos(ucfirst($option['nama']), ucfirst($filterByAsset)) === FALSE;
        });

        //dd($parse_aktiva_asset);

        $configApi  = $this->configApi();
        $param      = [
            'sumberx' => $filter_akun_sumber,
            'tahun_ajaran' => $parse_tahun,
            'akun_aktiva' =>  $filter_akun_aktiva,
            'akun_asset' => $parse_aktiva_asset
        ];
        return view('spp.components.v_transaksi_purchase_asset')->with(array_merge($configApi, $param));
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_penjualan_asset()
    {
        Session::put('title', 'Transaksi Penjualan Asset');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $rekening_sumber = $this->SppApiservice->get_akun_harta();
        $akun_aktiva_asset = $this->SppApiservice->check_stock_asset();
        $akun_aktiva = $this->SppApiservice->get_akun_aktiva();
        $parse_akun_sumber  = $rekening_sumber['body']['data'] ?? array();
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $parse_aktiva_asset = $akun_aktiva_asset['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $filterBy   = "piutang";
        $filter_akun_sumber = array_filter($parse_akun_sumber, function ($option) use ($filterBy) {
            return strpos(ucfirst($option['nama']), ucfirst($filterBy)) === FALSE;
        });

        $filterByAsset   = "penyusutan";
        $filter_akun_aktiva = array_filter($parse_aktiva, function ($option) use ($filterByAsset) {
            return strpos(ucfirst($option['nama']), ucfirst($filterByAsset)) === FALSE;
        });

        //dd($parse_aktiva_asset);

        $configApi  = $this->configApi();
        $param      = [
            'sumberx' => $filter_akun_sumber,
            'tahun_ajaran' => $parse_tahun,
            'akun_aktiva' =>  $filter_akun_aktiva,
            'akun_asset' => $parse_aktiva_asset
        ];
        return view('spp.components.v_transaksi_sales_asset')->with(array_merge($configApi, $param));
    }

    /** penyusutan Asset  */
    public function penyusutan_asset(){
        Session::put('title', 'Transaksi Penyusutan Asset');
        $akun_aktiva_asset = $this->SppApiservice->get_asset_tetap();
        $akun_aktiva = $this->SppApiservice->get_akun_aktiva();
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $parse_aktiva_asset = $akun_aktiva_asset['body']['data'] ?? array();
        $filterByAsset   = "penyusutan";
        $filter_akun_aktiva = array_filter($parse_aktiva, function ($option) use ($filterByAsset) {
            return strpos(ucfirst($option['nama']), ucfirst($filterByAsset)) === FALSE;
        });
        $configApi  = $this->configApi();
        $param      = [
            'akun_aktiva' =>  $filter_akun_aktiva,
            'akun_asset' => $parse_aktiva_asset
        ];
        return view('spp.components.v_transaksi_penyusutan_asset')->with(array_merge($configApi, $param));
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_asset(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_asset_tetap();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show asset akun " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit asset akun"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_asset_aktiva($id)
    {
        $notif = $this->SppApiservice->get_asset_tetapid($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function asset_aktiva()
    {
        Session::put('title', 'Aset Aktiva Tetap');
        $configApi  = $this->configApi();
        $akun_aktiva = $this->SppApiservice->get_akun_aktiva();
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $filterByAsset   = "penyusutan";
        $filter_akun_aktiva = array_filter($parse_aktiva, function ($option) use ($filterByAsset) {
            return strpos(ucfirst($option['nama']), ucfirst($filterByAsset)) === FALSE;
        });
        $param      = [
            'akun' => $filter_akun_aktiva,
        ];
        return view('spp.components.v_asset')->with(array_merge($configApi, $param));
    }

    /** store penambahan asset aktiva tetap */
    public function store_aktiva_tetap(Request $request)
    {
        if (!empty($request->akun_aktiva)) {
            foreach ($request->akun_aktiva as $key => $val) {
                $postinput = array();
                $postinput['nama'] = $request->nama_item[$key];
                $postinput['kode'] = $request->kode_item[$key];
                $postinput['id_akun'] = $val;
                $postinput['id_akun_kategori'] = $request->id_akun_kategori[$key];
                $postinput['id_akun_sub_kategori'] = $request->id_akun_sub_kategori[$key];
                $postinput['keterangan'] = $request->keterangan ?? '-';
                $postinput['id_sekolah'] = session('id_sekolah');
                $response_update = $this->SppApiservice->asset_post_add(json_encode($postinput));
            }
            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

    /** update store aset aktiva  */
    public function store_update_asset_aktiva(Request $request)
    {
        $datapost = array();
        $datapost['id_akun'] = $request->id_akun_asset;
        $datapost['id_akun_kategori'] = $request->id_akun_kategori_asset;
        $datapost['id_akun_sub_kategori'] = $request->id_akun_subkategori_asset;
        $datapost['nama']                 = $request->nama_barang;
        $datapost['keterangan']           = $request->catatan_barang;
        $datapost['id_sekolah']           = session('id_sekolah');

        $response_update = $this->SppApiservice->asset_post_update(json_encode($datapost), $request->id_asset);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store pembelian asset tetap */
    public function store_transaksi_purchase(Request $request)
    {
        if (!empty($request->akun_aktiva)) {

            $datapost = array();
            $postinput = array();
            $nominal_total = 0;

            $nominal    = str_replace(",", "", $request->nominal);
            if (empty($nominal)) {
                return back()->with('error', 'Nominal harus di isi !');
            }

            $subtotal_nominal    = str_replace(",", "", $request->subtotal);

            foreach ($request->id_akun_asset as $key => $val) {
                $data = array();
                $data['id_akun']                = $val;
                $data['id_asset_aktiva']        = $request->id_asset[$key];
                $data['nama_asset']             = $request->nama_asset[$key];
                if (empty($request->catatan[$key])) {
                    $data['keterangan']      = "-";
                } else {
                    $data['keterangan']      = $request->catatan[$key];
                }
                $data['harga_beli']           = $nominal[$key];
                $data['jumlah']               = $request->qty[$key];
                $data['total']                = $subtotal_nominal[$key];
                $nominal_total               += intval($nominal[$key]);
                $datapost[] = $data;
            }

            $postinput['transaksi_aktivas'] = $datapost;
            $postinput['id_akun']      = $request->sumber;
            $postinput['keterangan']   = $request->keterangan;
            $postinput['tgl_transaksi']  = $request->tgl_trans;
            $postinput['tgl_diterima']  = $request->tgl_diterima;
            $postinput['operator']     = session('username');
            $postinput['tahun_ajaran'] = $request->tahun_ajaran;
            $postinput['nominal']      = $nominal_total;
            $postinput['id_sekolah']  = session('id_sekolah');
            $postinput['suplayer']  = $request->supplier;
            $postinput['kontak_suplayer']  = $request->telepon;
            $postinput['alamat_suplayer']  = $request->alamat;

            //dd($postinput);

            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $postinput['path'] = $path;
                $response_update = $this->SppApiservice->purchase_aktiva_with_file(json_encode($postinput));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->purchase_aktiva(json_encode($postinput));
            }

            if ($response_update['code'] == '200') {
                return back()->with('success', $response_update['body']['message']);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
    }

     /** store pembelian asset tetap */
     public function store_transaksi_sales(Request $request)
     {
        if (!empty($request->akun_aktiva)) {

            $datapost = array();
            $postinput = array();
            $nominal_total = 0;

            $nominal    = str_replace(",", "", $request->nominal);
            if (empty($nominal)) {
                return back()->with('error', 'Nominal harus di isi !');
            }

            $subtotal_nominal    = str_replace(",", "", $request->subtotal);

            foreach ($request->id_akun_asset as $key => $val) {
                $data = array();
                $data['id_akun']                = $val;
                $data['id_asset_aktiva']        = $request->id_asset[$key];
                $data['nama_asset']             = $request->nama_asset[$key];
                if (empty($request->catatan[$key])) {
                    $data['keterangan']      = "-";
                } else {
                    $data['keterangan']      = $request->catatan[$key];
                }
                $data['harga_jual']           = $nominal[$key];
                $data['jumlah']               = $request->qty[$key];
                $data['total']                = $subtotal_nominal[$key];
                $nominal_total               += intval($nominal[$key]);
                $datapost[] = $data;
            }

            $postinput['transaksi_aktivas'] = $datapost;
            $postinput['id_akun']      = $request->sumber;
            $postinput['keterangan']   = $request->keterangan;
            $postinput['tgl_transaksi']  = $request->tgl_trans;
            $postinput['tgl_diterima']   = $request->tgl_trans;
            $postinput['operator']     = session('username');
            $postinput['tahun_ajaran'] = $request->tahun_ajaran;
            $postinput['nominal']      = $nominal_total;
            $postinput['id_sekolah']  = session('id_sekolah');
            $postinput['pelanggan']  = $request->pelanggan;
            $postinput['kontak_pelanggan']  = $request->telepon;
            $postinput['alamat_pelanggan']  = $request->alamat;

            //dd($postinput);

            if ($files = $request->file('image')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bukti_nota/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $postinput['path'] = $path;
                $response_update = $this->SppApiservice->sales_aktiva_with_file(json_encode($postinput));
                if (!empty($path)) {
                    File::delete($path);
                }
            } else {
                $response_update = $this->SppApiservice->sales_aktiva(json_encode($postinput));
            }

            if ($response_update['code'] == '200') {
                $link = [
                    'message' => $response_update['body']['message'],
                    'data'    => $response_update['body']['data']['kode']
                ];
                return back()->with('success', $link);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return back()->with('error', 'cek kembali form akun !');
        }
     }

     /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_asset_stock(Request $request)
    {
        $pengaturan = $this->SppApiservice->check_stock_asset();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_asset_purchase(Request $request)
    {
        $pengaturan = $this->SppApiservice->gettransaksi_purchase_asset();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show asset akun " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <a href="'.route('print_asset_aktiva_purchase_item',['id'=>$data['id']]).'" target="_blank" class="btn btn-info btn-sm " data-id="' . $data['id'] . '"><i class="fa fa-print"></i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('satuan', function ($row) {
                return number_format($row['satuan'], 0);
            });

            $table->editColumn('penyusutan', function ($row) {
                return number_format($row['penyusutan'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('tgl_diterima', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_diterima'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action','nominal','tgl_transaksi','tgl_diterima']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

     /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_asset_penyusutan(Request $request)
    {
        $pengaturan = $this->SppApiservice->gettransaksi_purchase_asset();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show asset akun " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <a href="'.route('print_asset_aktiva_purchase_item',['id'=>$data['id']]).'" target="_blank" class="btn btn-info btn-sm " data-id="' . $data['id'] . '"><i class="fa fa-print"></i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('satuan', function ($row) {
                return number_format($row['satuan'], 0);
            });

            $table->editColumn('penyusutan', function ($row) {
                return number_format($row['penyusutan'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('tgl_diterima', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_diterima'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action','nominal','tgl_transaksi','tgl_diterima']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_asset_sales(Request $request)
    {
        $pengaturan = $this->SppApiservice->gettransaksi_sales_asset();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show asset akun " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <a href="'.route('print_asset_aktiva_sales_item',['id'=>$data['id']]).'" target="_blank" class="btn btn-info btn-sm " data-id="' . $data['id'] . '"><i class="fa fa-print"></i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('satuan', function ($row) {
                return number_format($row['satuan'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_transaksi'])->isoFormat('dddd, D MMMM Y');
            });

            $table->editColumn('kode_transaksi', function ($row) {
                return $row['kode_transaksi']. "";
            });

            $table->rawColumns(['action','nominal','tgl_transaksi','kode_transaksi','satuan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show_by_kode_ajax_detail($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->get_detail_transaksi_bycode($id);

        $result = $response_show_detail['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_transaksi', function ($row) {
                if (!empty($row['tgl_bayar'])) {
                    return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y ');
                } else {
                    return '-';
                }
            });

            $table->rawColumns(['nominal', 'tgl_transaksi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

      /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_asset_aktiva_transaksi($id)
    {
        $notif = $this->SppApiservice->gettransaksi_purchase_assetid($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

     /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_asset_aktiva_akun($id)
    {
        $notif = $this->SppApiservice->get_asset_byakun($id);
        if ($notif['code'] == '200') {
            if(count($notif['body']['data']) > 0){
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
          }else{
            return response()->json(
                [
                    'message' => 'Aset yang dicari Belum ada silahkan tambahkan aset terlebih dahulu',
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
          }
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

        /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_asset_aktiva_transaksi_sales($id)
    {
        $notif = $this->SppApiservice->gettransaksi_sales_assetid($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_asset_aktiva_purchase($id)
    {
        Session::put('title', 'Print Transaction Purchase Asset Fixed ');
        $akun_aktiva = $this->SppApiservice->gettransaksi_purchase_assetid($id);
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        if(!empty($parse_aktiva['kode_transaksi'])){
            $detailitem = $this->SppApiservice->get_detail_transaksi_bycode($parse_aktiva['kode_transaksi']);
            $parse_item = $detailitem['body']['data'] ?? array();
        }
        $param      = [
            'detail' => $parse_aktiva,
            'detail_item' => $parse_item
        ];
        return view('spp.components.v_print_purchase_asset')->with($param);
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_asset_aktiva_purchase_item($id)
    {
        Session::put('title', 'Print Transaction Purchase Asset Fixed ');
        $akun_aktiva = $this->SppApiservice->gettransaksi_purchase_assetid($id);
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $param      = [
            'detail' => $parse_aktiva,
        ];
        return view('spp.components.v_print_purchase_asset_item')->with($param);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_asset_aktiva_sales($id)
    {
        Session::put('title', 'Print Transaction Sales Asset Fixed ');
        $akun_aktiva = $this->SppApiservice->gettransaksi_sales_assetid($id);
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();

        if(!empty($parse_aktiva['kode_transaksi'])){
            $detailitem = $this->SppApiservice->get_detail_transaksi_bycode($parse_aktiva['kode_transaksi']);
            $parse_item = $detailitem['body']['data'] ?? array();
        }
        if(count($parse_item) > 0 ){
            $param      = [
                'detail' => $parse_aktiva,
                'detail_item' => $parse_item
            ];
            return view('spp.components.v_print_sales_asset')->with($param);
        }else{
            return back()->with('error', 'data tidak ditemukan !');
        }

    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print_asset_aktiva_salesbycode($id)
    {
        $response_show_detail = $this->SppApiservice->get_detail_transaksi_bycode($id);
        dd($response_show_detail['body']['data']);
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_asset_aktiva_sales_item($id)
    {
        Session::put('title', 'Print Transaction Purchase Asset Fixed ');
        $akun_aktiva = $this->SppApiservice->gettransaksi_sales_assetid($id);
        $parse_aktiva = $akun_aktiva['body']['data'] ?? array();
        $param      = [
            'detail' => $parse_aktiva,
        ];
        return view('spp.components.v_print_sales_asset_item')->with($param);
    }

    /** store update usia ekonomis asset  */
    public function store_update_penyusutan_asset(Request $request){
        $datapost = array();
        $datapost['id'] = $request->id_edit_item;
        $datapost['usia_tahun'] = $request->usia_tahun;
        $datapost['usia_bulan'] = $request->usia_bulan;

        $notif = $this->SppApiservice->update_asset_usiaekonomis(json_encode($datapost));
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }

    }
}
