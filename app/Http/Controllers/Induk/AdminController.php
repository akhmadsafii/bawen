<?php

namespace App\Http\Controllers\Induk;

use App\ApiService\Induk\AdminApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class AdminController extends Controller
{
    private $adminApi;
    private $profileApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
        $this->profileApi = new ProfileApi();
    }

    public function index()
    {
        Session::put('title', 'Admin Buku Induk');
        $admin = $this->adminApi->sekolah();
        // dd($admin);
        $admin = $admin['body']['data'];
        return view('content.admin.induk.v_admin')->with(['admin' => $admin]);
    }

    public function edit()
    {
        // dd($request);
        $admin = $this->adminApi->get_by_id(Help::decode($_GET['code']));
        $admin = $admin['body']['data'];
        return view('content.admin.induk.v_edit_admin')->with(['admin' => $admin]);
    }

    public function detail(Request $request)
    {
        $post  = $this->adminApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'nip' => $request->nip,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'first_password' => $request->password,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->adminApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->adminApi->update(json_encode($data));
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        $admin = $this->data_admin();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'admin' => $admin
            ]);
        }
    }



    private function data_admin()
    {
        $admin = $this->adminApi->sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (!empty($admin)) {
            $no = 1;
            foreach ($admin as $adm) {
                $html .= '
                <tr>
                <td>' . $no++ . '</td>
                <td>' . $adm['nama'] . '</td>
                <td>' . $adm['nip'] . '</td>
                <td>' . $adm['telepon'] . '</td>
                <td>' . $adm['email'] . '</td>
                <td>
                    <a href="' . route('admin_induk-edit', ['code' => Help::encode($adm['id']), 'name' => str_slug($adm['nama'])]) . '" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" data-id="' . $adm['id'] . '"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function res_pass(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->adminApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->adminApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->adminApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
