<?php

namespace App\Http\Controllers\Induk;

use App\ApiService\User\GuruApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    private $guruApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Guru');
        $routes = "guru_induk-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->guruApi->get_sekolah_pagination($page, $search);
        // dd($result);
        $guru = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        return view('content.admin.induk.v_guru')->with(['routes' => $routes, 'pagination' => $pagination, 'guru' => $guru, 'search' => $search]);
    }
}
