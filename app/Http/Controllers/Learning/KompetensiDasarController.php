<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\JenisStandarKompetensiApi;
use App\ApiService\Learning\KompetensiDasarApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KompetensiDasarController extends Controller
{
    private $jenisApi;
    private $kompetensiApi;
    private $kompetensiDasarApi;
    private $hashids;

    public function __construct()
    {
        $this->jenisApi = new JenisStandarKompetensiApi();
        $this->kompetensiApi = new KompetensiApi();
        $this->kompetensiDasarApi = new KompetensiDasarApi();
        $this->hashids = new Hashids();
    }

    public function index(Request $request)
    {
        $pengetahuan = $this->kompetensiApi->by_mapel_kelas_jenis(session('id_room('.request()->segment(4).').id_mapel'), session('id_room('.request()->segment(4).').id_kelas'), "pengetahuan");
        if ($pengetahuan['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        $pengetahuan = $pengetahuan['body']['data'];
        $keterampilan = $this->kompetensiApi->by_mapel_kelas_jenis(session('id_room('.request()->segment(4).').id_mapel'), session('id_room('.request()->segment(4).').id_kelas'), "keterampilan");
        if ($keterampilan['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        $keterampilan = $keterampilan['body']['data'];
        // dd($pengetahuan);
        // dd($keterampilan);
        if(session("role") == "siswa")
        {
            return view('content.e_learning.room.v_kompetensi_siswa')->with(['template' => session('template'), 'keterampilan' => $keterampilan, 'pengetahuan' => $pengetahuan]);
        }
        return view('content.e_learning.room.v_kompetensi_dasar')->with(['template' => session('template'), 'keterampilan' => $keterampilan, 'pengetahuan' => $pengetahuan]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data =
            [
                'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
                'id_kelas' => session('id_room('.request()->segment(4).').id_kelas'),
                'id_kompetensi_inti' => $request->kompetensi_inti,
                'nama' => $request->kode."#".$request->nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            ];
            // dd($data);
        $create = $this->kompetensiDasarApi->create(json_encode($data));
        // dd($tes);
        if ($create['code'] == 200) {
            return response()->json([
                'status' => 'berhasil',
                'success' => $create['body']['message'],
                'icon'  => 'success',
                'id_kompetensi_inti'  => $create['body']['data'],
            ]);
        }else{
            return response()->json([
                'status' => 'gagal',
                'success' => $create['body']['message'],
                'icon'  => 'error'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data =
            [
                'id' => $request->id_kd,
                'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
                'id_kelas' => session('id_room('.request()->segment(4).').id_kelas'),
                'id_kompetensi_inti' => $request->kompetensi_inti,
                'nama' => $request->kode."#".$request->nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            ];
        $tes =  $this->kompetensiDasarApi->create(json_encode($data));
        if ($tes['code'] == 200) {
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'id_kompetensi_inti'  => $tes['body']['data'],
            ]);
        }else{
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function load(Request $request)
    {
        $id = $request['id_kompetensi_inti'];
        $post  = $this->kompetensiDasarApi->get_by_inti($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function kompetensi_dasar(Request $request)
    {
        $id_ki = $request['id_kompetensi_inti'];
        $id = $request['id_kompetensi_dasar'];
        $post  = $this->kompetensiDasarApi->get_by_inti($id_ki);
        $result = $post['body']['data'];
        $output = '';
        foreach ($result as $brand){
            $brand_id = $brand['id'];
            $brand_name = $brand['nama'];
            $output .= '<option value="'.$brand_id.'" '.(($brand_id == $id) ? 'selected="selected"':"").'>'.$brand_name.'</option>';

        }
        return $output;
    }

    public function edit(Request $request)
    {
        $id = $request['id_kompetensi'];
        $post  = $this->kompetensiApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function edit_kd(Request $request)
    {
        $id = $request['id_kompetensi_dasar'];
        $post  = $this->kompetensiDasarApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function trash_kd(Request $request)
    {
        $id = $request['id_kompetensi_dasar'];
        $post  = $this->kompetensiDasarApi->soft_delete($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function trash(Request $request)
    {
        $id = $request['id_kompetensi'];
        $delete = $this->kompetensiApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function delete($id)
    {
        $delete = $this->kompetensiApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function data_trash(Request $request)
    {
        Session::put('title', 'Data Trash Jurusan');
        $jurusan = $this->kompetensiApi->all_trash();
        $result = $jurusan['body']['data'];
        $template = session('template');
        // dd($template);
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm " onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.master.jurusan.v_trash_jurusan')->with(['data' => $result, 'template' => $template]);
    }

    public function restore($id)
    {

        $restore = $this->kompetensiApi->restore($id);
        if ($restore['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function get_inti_checkbox(Request $request)
    {
        $id_kompetensi_inti = $request['id_kompetensi_inti'];
        $kompetensi_dasar = $request['kompetensi_dasar'];
        // dd($kompetensi_dasar);
        $post  = $this->kompetensiDasarApi->get_by_inti($id_kompetensi_inti);
        $result = $post['body']['data'];
        // dd($result);
        $output = '';
        foreach($result as $brand){
            $selected = '';
            $brand_id = $brand['id'];
            $brand_name = $brand['nama'];
            foreach($kompetensi_dasar as $kd){
                if($brand_id == $kd['id']){
                    $selected = 'checked';
                    break;
                }
            }
            // $output .= '<option value="'.$brand_id.'" '.$selected.'>'.$brand_name.'</option>';
            $output .= '<input type="checkbox" name="kompetensi_dasar[]" id="kompetensi_dasar" value="'.$brand_id.'" '.$selected.'>'.$brand_name.'<br>';
        }
        return $output;

    }
}
