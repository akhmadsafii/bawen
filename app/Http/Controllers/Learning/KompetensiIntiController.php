<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\MapelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class KompetensiIntiController extends Controller
{
    private $kelasApi;
    private $kompetensiApi;
    private $mapelApi;
    private $jurusanApi;
    private $guruMapelApi;

    public function __construct()
    {
        $this->kompetensiApi = new KompetensiApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
        $this->guruMapelApi = new GuruPelajaranApi();
    }

    public function index(Request $request)
    {
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $guruM = $this->guruMapelApi->group_by_mapel_kelas(session('id_tahun_ajar'));
        // dd($guruM);
        $guruM = $guruM['body']['data'];
        // dd($guruM);
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $kelas = $this->kelasApi->get_jurusan_null();
        $kelas = $kelas['body']['data'];
        // dd($kelas);
        $kompetensi = $this->kompetensiApi->get_by_sekolah();
        // dd($kompetensi);
        $result = $kompetensi['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-id="' .$data['id'] . '"  class="btn btn-info btn-sm edit"><i class="fas fa-pencil-alt"></i></a>';
                $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                return $button;
            });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Kompetensi Inti Pelajaran');
        return view('content.admin.master.v_kompetensi')->with(['template' => session('template'), 'mapel' => $mapel, "jurusan" => $jurusan, 'guru_pel' => $guruM, 'kelas' => $kelas]);
    }

    public function edit(Request $request)
    {
        $kompetensi = $this->kompetensiApi->get_by_id($request['id']);
        $result = $kompetensi['body']['data'];
        if ($result['id_jurusan'] == null) {
            $result['id_jurusan'] = 0;
        }
        // dd($result);
        return response()->json($result);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Kompetensi berhasil ditambahkan',
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        }
        $data = [
            'id_mapel' => $request->id_mapel,
            'id_kelas' => $request->id_kelas,
            'jenis_kompetensi' => $request->jenis_kompetensi,
            'kompetensi_inti' => $request->kompetensi_inti,
            'indikator' => $request->indikator,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        ];
        $result = $this->kompetensiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Kompetensi berhasil diupdate',
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        }
        $data = [
            'id' => $request->id,
            'id_mapel' => $request->id_mapel,
            'id_kelas' => $request->id_kelas,
            'jenis_kompetensi' => $request->jenis_kompetensi,
            'kompetensi_inti' => $request->kompetensi_inti,
            'indikator' => $request->indikator,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        ];
        $update = $this->kompetensiApi->update_info(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Kompetensi berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }
        $delete = $this->kompetensiApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->kompetensiApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $ekstra = $this->kompetensiApi->all_trash();
        // dd($ekstra);
        $result = $ekstra['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->kompetensiApi->restore($request['id']);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        // dd($request);
        $id_mapel = $request['id_mapels'];
        $id_kelas = $request['id_kelass'];
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->kompetensiApi->upload_excel(json_encode($data), $id_mapel, $id_kelas);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function template(Request $request)
    {
        // dd($request);
        $link = $this->kompetensiApi->import($request['id_mapel'], $request['id_kelas']);
        return $link;
        // dd($link);
    }
}
