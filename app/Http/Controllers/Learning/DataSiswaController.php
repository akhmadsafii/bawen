<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DataSiswaController extends Controller
{
    private $dataSiswaApi;
    private $kelasSiswaApi;
    private $profileApi;
    private $siswaApi;
    private $tahunajaranApi;
    private $rombelApi;
    private $hash;

    public function __construct()
    {
        $this->dataSiswaApi = new DataSiswaApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->profileApi = new ProfileApi();
        $this->siswaApi = new SiswaApi();
        $this->tahunajaranApi = new TahunAjaranApi();
        $this->rombelApi = new RombelApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        $data_rombel = $this->kelasSiswaApi->get_by_rombel(session('id_room(' . request()->segment(4) . ').id_rombel'), session('tahun'));
        if ($data_rombel['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        // dd(session()->all());
        $rombel = $this->rombelApi->get_by_id(session('id_room(' . request()->segment(4) . ').id_rombel'));
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        $siswa = $this->dataSiswaApi->get_absensi(session('id_room(' . request()->segment(4) . ').id_mapel'), session('id_room(' . request()->segment(4) . ').id_rombel'), session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        Session::put('title', 'Data Siswa');
        $param = array(
            'id_mapel' => session('id_room(' . request()->segment(4) . ').id_mapel'),
            'id_kelas' => session('id_room(' . request()->segment(4) . ').id_kelas'),
            'id_rombel' => session('id_room(' . request()->segment(4) . ').id_rombel'),
            'id_room' => session('id_room(' . request()->segment(4) . ').id_room'),
        );

        if (session("role") == "siswa") {
            return view('content.e_learning.room.data_siswa.v_data_siswa_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'rombel' => $rombel]);
        }
        return view('content.e_learning.room.data_siswa.v_data_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'rombel' => $rombel, 'param' => $param]);
    }

    public function export_absensi()
    {
        $kelas_siswa = $_GET['ks'];
        $mapel = $_GET['mp'];
        $rombel = $_GET['rb'];
        $tahun = $_GET['th'];
        return \Excel::download(new ExportFile_absensi($kelas_siswa, $mapel, $rombel, $tahun), 'absensi - ' . $_GET['name'] . '.xlsx');
    }

    public function data_table_detail(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kelas_siswa' => $request['id_kelas_siswa'],
            'pertemuan' => $request['pertemuan']
        );
        // dd($data);
        $result = $this->dataSiswaApi->get_pertemuan(json_encode($data));
        // dd($result);
        $result = $result['body']['data'];
        $table = datatables()->of($result)
            ->editColumn('aksi', function ($row) {
                // dd($row);
                if ($row['status'] == 'Tergabung') {
                    return '<a href="javascript:void(0)" onclick="gantiAbsensi(' . $row['id'] . ',\'' . $row['absensi'] . '\')"><span class="badge badge-success">' . $row['status'] . '</span></a>';
                } else {
                    return '<a href="javascript:void(0)" onclick="gantiAbsensi(' . $row['id'] . ',\'' . $row['absensi'] . '\')"><span class="badge badge-danger">' . $row['status'] . '</span></a>';
                }
            });
        $table->rawColumns(['aksi']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function rombel(Request $request)
    {
        $data_siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'));
        $result = $data_siswa['body']['data'];

        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('select_orders', function ($row) {
                $check =  '<input type="checkbox" class="delete_check" id="delcheck_' . $row['id'] . '" onclick="checkcheckbox();" value="' . $row['id'] . '">';
                return $check;
            });
            $table->rawColumns(['action', 'select_orders']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Data Siswa');


        return view('content.e_learning.room.data_siswa.v_siswa_rombel')->with(['template' => session('template')]);
    }

    public function store(Request $request)
    {
        if ($request->kotak = 2) {
            $deleteids_arr = array();
            if (isset($request->deleteids_arr)) {
                // dd("lanjut");
                $deleteids_arr = $request->deleteids_arr;
            }
            foreach ($deleteids_arr as $deleteid) {
                // dd($deleteid);
                $data = [
                    'id_kelas_siswa' => $deleteid,
                    'id_room' => session('id_room'),
                    'id_pertemuan' => session('pertemuan'),
                    'id_guru_pelajaran' => 1,
                    'absensi' => "alfa",
                    'id_mapel_kelas' => session('id_mapel_kelas'),
                    'id_sekolah' => session('id_sekolah'),
                    'status' => 3
                ];

                $tes = $this->dataSiswaApi->create(json_encode($data));
            }
            return  response()->json(['success' => ' Jurusan saved successfully!']);

            // echo 1;
            // exit;
        } else {
            dd("salah");
        }
    }

    public function edit($id)
    {
        $siswa = $this->siswaApi->get_by_id($id);
        $siswa = $siswa['body']['data'];
        $end = explode('/', $siswa['file']);
        $siswa['file_edit'] = end($end);
        return response()->json($siswa);
    }

    public function detail_siswa($id)
    {
        $siswa = $this->siswaApi->get_by_id($id);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="profile__avatar">
                    <img src="' . $siswa['file'] . '" alt="...">
                </div>
                <div class="profile__header">
                    <h4>' . ucwords($siswa['nama']) . '</h4>
                    <p class="text-muted">
                        ' . $siswa['alamat'] . '
                    </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table profile__table">
                    <tbody>
                        <tr>
                            <th><strong>NIS</strong></th>
                            <td>' . $siswa['nis'] . '</td>
                        </tr>
                        <tr>
                            <th><strong>NISN</strong></th>
                            <td>' . $siswa['nisn'] . '</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">User Info</h4>
            </div>
            <div class="panel-body">
                <table class="table profile__table">
                    <tbody>
                        <tr>
                            <th><strong>NIK</strong></th>
                            <td>' . $siswa['nik'] . '</td>
                        </tr>
                        <tr>
                            <th><strong>Email</strong></th>
                            <td>' . $siswa['email'] . '</td>
                        </tr>
                        <tr>
                            <th><strong>Telepon</strong></th>
                            <td>' . $siswa['telepon'] . '</td>
                        </tr>
                        <tr>
                            <th><strong>Agama</strong></th>
                            <td>' . $siswa['agama'] . '</td>
                        </tr>

                        <tr>
                            <th><strong>Jenis Kelamin</strong></th>
                            <td>' . $siswa['jenkel'] . '</td>
                        </tr>
                        <tr>
                            <th><strong>Tempat, Tanggal lahir</strong></th>
                            <td>' . ucwords($siswa['tempat_lahir']) . ', ' . Help::getTanggal($siswa['tgl_lahir']) . '</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        ';
        return response()->json($html);
    }


    public function update(Request $request)
    {
        $data_update = array(
            'id' => $request['id'],
            'nis' => $request['nis'],
            'nama' => $request['nama'],
            'jenkel' => $request['jenkel'],
            'agama' => $request['agama'],
            'telepon' => $request['telepon'],
            'tempat_lahir' => $request['tempat_lahir'],
            'tanggal_lahir' => $request['tanggal'],
            'alamat' => $request['alamat'],
            'tahun_angkatan' => $request['tahun_masuk'],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status'],
        );
        $siswa = $this->siswaApi->update_info(json_encode($data_update));
        return response()->json(['success' => ' Siswa Update successfully!']);
    }

    public function update_status(Request $request)
    {
        $update = array(
            'id' => $request['id'],
            'absensi' => $request['absensi'],
        );
        $update = $this->dataSiswaApi->update_absensi(json_encode($update));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function data_siswa_rombel()
    {
        session()->put('title', "Data Siswa");
        $data_siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
        $siswa = $data_siswa['body']['data'];
        return view('content.e_learning.rombel.v_data_siswa')->with(['template' => session('template'), 'siswa' => $siswa]);
    }

    public function store_siswa(Request $request)
    {
        $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
        $rombel = $rombel['body']['data'];
        $data =
            [
                'id_siswa' => $request->id_siswa,
                'id_rombel' => session('id_rombel'),
                'id_kelas' => $rombel['id_kelas'],
                'id_tahun_ajaran' => $request->id_tahun_ajaran,
                'id_jurusan' => $rombel['id_jurusan'],
                'id_sekolah' => $rombel['id_sekolah'],
                'status' => $request->status,
            ];
        $tes = $this->kelasSiswaApi->create(json_encode($data));
        if ($tes['code'] == 201) {
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'warning'
            ]);
        }
    }

    public function get_by_id_kelas_siswa($id)
    {
        $siswa = $this->kelasSiswaApi->get_by_id($id);
        $siswa = $siswa['body']['data'];
        return response()->json($siswa);
    }

    public function update_by_wali_kelas(Request $request)
    {
        $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
        $rombel = $rombel['body']['data'];
        $data_update = array(
            'id' => $request['id'],
            'id_siswa' => $request->id_siswa,
            'id_rombel' => session('id_rombel'),
            'id_kelas' => $rombel['id_kelas'],
            'id_tahun_ajaran' => $request->id_tahun_ajaran,
            'id_jurusan' => $rombel['id_jurusan'],
            'id_sekolah' => $rombel['id_sekolah'],
            'status' => $request->status,
        );
        $siswa = $this->kelasSiswaApi->update_info(json_encode($data_update));
        if ($siswa['code'] == 200) {
            return response()->json([
                'success' => $siswa['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $siswa['body']['message'],
                'icon'  => 'warning'
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->kelasSiswaApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
            $icon = "success";
        } else {
            $success = false;
            $message =  $delete['body']['message'];
            $icon = "error";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
            'icon' => $icon,
        ]);
    }
}
