<?php

namespace App\Http\Controllers\Learning;

use App\Http\Controllers\Controller;
use App\ApiService\Learning\MapelWaliKelasApi;
use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\GuruPelajaranApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class AbsensiWaliKelasController extends Controller
{
    private $mapelWaliKelasApi;
    private $datasiswaApi;
    private $gurupelajaranApi;

    public function __construct()
    {
        $this->mapelWaliKelasApi = new MapelWaliKelasApi();
        $this->datasiswaApi = new DataSiswaApi();
        $this->gurupelajaranApi = new GuruPelajaranApi();
    }

    public function index(Request $request)
    {
        $mapel_kelas = $this->gurupelajaranApi->by_rombel(session('id_rombel'), session('id_tahun_ajar'));
        $mapel_kelas = $mapel_kelas['body']['data'];
        Session::put('title', 'Absensi Siswa');
        return view('content.e_learning.rombel.absensi.v_data_absensi')->with(['template' => session('template'), 'mapel' => $mapel_kelas]);
    }

    public function get_datatable(Request $request)
    {
        $id_mapel = $request['id_mapel'];
        $kelas = $this->datasiswaApi->get_by_mapel($id_mapel, session('id_rombel') , session('id_tahun_ajar'));
        if ($id_mapel == 0) {
            $result = [];
            $table = datatables()->of($result);
        }else{
            $result = $kelas['body']['data'];
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" data-toggle="tooltip"  data-id_kelas_siswa="' . $data['id_kelas_siswa'] . '" data-id_mapel="' . $data['id_mapel'] . '" data-id_rombel="' . $data['id_rombel'] . '" data-id_ta="' . $data['id_ta_sm'] . '" data-original-title="Edit" class="edit editData-'.$data['id_kelas_siswa'].' btn btn-info btn-xs edit"><i class="fa fa-eye"></i> Lihat</a>';
                });
            $table->rawColumns(['action']);
        }
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function detail_absensi(Request $request)
    {
        // dd($request);
        $id_kelas_siswa = $request['id_kelas_siswa'];
        $id_mapel = $request['id_mapel'];
        $id_rombel = $request['id_rombel'];
        $id_ta = $request['id_ta'];
        if($id_kelas_siswa && $id_mapel && $id_rombel && $id_ta){
            $data_absensi = $this->datasiswaApi->get_detail_absensi($id_kelas_siswa, $id_mapel, $id_rombel, $id_ta);
            $result = $data_absensi['body']['data'];
        }else{
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-danger btn-xs restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Hapus</a>';
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function detail($id)
    {
        Session::put('id_room', $id);
        $mapel = $this->datasiswaApi->get_by_absensi($id);
        $result = $mapel['body']['data'];
        return response()->json($result);
    }

    public function detail_pertemuan(Request $request, $pertemuan)
    {
        $absensi = $this->datasiswaApi->get_by_absensi_pertemuan(session('id_room'), $pertemuan);
        // dd($absensi);
        $result = $absensi['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Guru Mapel');
        return view('content.e_learning.rombel.absensi.v_detail_absensi')->with(['absensi' => $result]);
    }
}
