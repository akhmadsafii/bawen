<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\TugasKetrampilanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use Illuminate\Support\Str;

class TugasKetrampilanController extends Controller
{
    private $tugasketrampilanApi;
    private $kompetensiApi;

    public function __construct()
    {
        $this->kompetensiApi = new KompetensiApi();
        $this->tugasketrampilanApi = new TugasKetrampilanApi();
    }

    public function index(Request $request)
    {
        $kompetensi = $this->kompetensiApi->by_mapel_kelas_jenis(session('id_room('.request()->segment(4).').id_mapel'), session('id_room('.request()->segment(4).').id_kelas'), "keterampilan");
        if ($kompetensi['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        $kompetensi = $kompetensi['body']['data'];
        if (session("role") == "siswa") {
            $tugas_ketrampilan = $this->tugasketrampilanApi->get_room_by_siswa(session('id_room('.request()->segment(4).').id_room'));
            $result = $tugas_ketrampilan['body']['data'];
        }else{
            $tugas_ketrampilan = $this->tugasketrampilanApi->get_room(session('id_room('.request()->segment(4).').id_room'));
            $result = $tugas_ketrampilan['body']['data'];
        }
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm edit"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="btn btn-success btn-sm hasil"> <i class="fas fa-users-cog"></i></a>';
                    return $button;
                });
            $table->editColumn('tombol', function ($aksi) {
                return '<a href="javascript:void(0)" data-id="' . $aksi['id'] . '"  class="edit  btn btn-danger btn-xs edit"><i class="fa fa-upload"></i> Upload Jawaban</a>';
            });
            $table->editColumn('waktu', function ($row) {
                $currentDate = date('Y-m-d');
                $kelas = 'info';
                if($currentDate >= $row['maks_pengumpulan']){
                    $kelas = 'danger';
                }
                if ($row['maks_pengumpulan'] != null) {
                    return '<b class="text-'.$kelas.'">'. Help::getTanggalLengkap($row['maks_pengumpulan']).'</b>';
                } else {
                    return '<b class="text-purple">Tidak ada batas waktu</b>';
                }
            });
            $table->editColumn('download', function ($aksi) {
                if ($aksi['file'] == null) {
                    $download = '<a class="btn btn-dark btn-sm text-white"><i class="fas fa-times-circle"></i></a>';
                } else {
                    $download = '<a href="tugas_ketrampilan/download/'.Help::encode($aksi['id']).'" target="_blank" class="btn btn-purple btn-sm"><i class="fa fa-download"></i></a>';
                }
                return $download;
            });
            $table->editColumn('keterangan', function ($row) {
                $detail = '';
                if(strlen(strip_tags($row['keterangan'])) > 30){
                    $detail = '<a href="javascript:void(0)" class="btn btn-sm detailInstruksi mt-0" data-id="'.$row['id'].'"><i class="fas fa-eye"></i></a>';
                }
                return Str::limit(strip_tags($row['keterangan']), 30, '...').$detail;
            });
            if (session("role") == "siswa") {
                $table->editColumn('status_kumpul', function ($kumpul) {
                    if ($kumpul['pengumpulan'] == 0) {
                        $kumpuls = '<small class="text-danger"><i class="fas fa-exclamation-circle"></i> Belum mengumpulkan</small><br><a href="javascript:void(0)" data-id="' . $kumpul['id'] . '" data-status="0"  class="btn btn-info btn-sm upload_tugas"><i class="fa fa-upload"></i></a>';
                    } else {
                        $kumpuls = '<small class="text-success"><i class="fas fa-check-circle"></i> Sudah mengumpulkan</small><br><a href="javascript:void(0)" data-id="' . $kumpul['id'] . '" data-status="1" class="btn btn-success btn-sm hasil"><i class="fa fa-eye"></i></a>';
                    }
                    return $kumpuls;
                });
                $table->rawColumns(['action','download','tombol','status_kumpul', 'waktu', 'keterangan']);
            }else{
                $table->rawColumns(['action','download','tombol', 'waktu', 'keterangan']);
            }
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Tugas Ketrampilan');
        if (session("role") == "siswa") {
            return view('content.e_learning.room.v_tugas_ketrampilan_siswa')->with(['template' => session('template')]);
        }
        return view('content.e_learning.room.v_tugas_ketrampilan')->with(['template' => session('template'), 'kompetensi' => $kompetensi]);
    }

    public function get_by_id($id)
    {
        $kompetensi = $this->kompetensiApi->get_by_id($id);
        $kompetensi = $kompetensi['body']['data'];
        $data = explode('<br>', $kompetensi['kompetensi_dasar']);
        // dd($data);
        return json_encode($data);
    }

    public function store(Request $request)
    {
        // $kompetensi_dasar = join("<br>", $request->kompetensi_dasar);

        $data =
            [
                'nama' => $request->nama,
                // 'kompetensi_dasar' => $kompetensi_dasar,
                'keterangan' => $request->keterangan,
                'maks_pengumpulan' => $request->tgl_maks_pengumpulan." ".$request->waktu_maks_pengumpulan,
                'id_guru' => session('id'),
                'id_sekolah' => session('id_sekolah'),
                'id_room' => session('id_room('.request()->segment(4).').id_room'),
                'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
                'status' => 1
            ];

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->tugasketrampilanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->tugasketrampilanApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $ketrampilan  = $this->tugasketrampilanApi->get_by_id($request['id']);
        $result = $ketrampilan['body']['data'];
        $skemas = explode('<br>', $result['skema']);
        $skema = [];
        foreach ($skemas as $value) {
            $skema[] = [$value];
        }
        $nama_file = explode('/', $result['file_lengkap']);
        $nama_file = end($nama_file);
        $result['nama_file'] = $nama_file;
        $result['skema'] = $skema;
        $result['tgl_pengumpulan'] = date('Y-m-d', strtotime($result['maks_pengumpulan']));
        $result['waktu_pengumpulan'] = date('H:i', strtotime($result['maks_pengumpulan']));
        $result['format_pengumpulan'] = Help::getTanggalLengkap($result['maks_pengumpulan']);
        return response()->json($result);
    }


    public function update(Request $request)
    {
        // $kompetensi_dasar = join("<br>", $request->kompetensi_dasar);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            // 'kompetensi_dasar' => $kompetensi_dasar,
            'keterangan' => $request->keterangan,
            'maks_pengumpulan' => $request->tgl_maks_pengumpulan." ".$request->waktu_maks_pengumpulan,
            'id_sekolah' => session('id_sekolah'),
            'id_guru' => session('id'),
            'id_room' => session('id_room('.request()->segment(4).').id_room'),
            'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->tugasketrampilanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->tugasketrampilanApi->update_info(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->tugasketrampilanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->tugasketrampilanApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function data_trash(Request $request)
    {
        Session::put('title', 'Data Trash Jurusan');
        $jurusan = $this->tugasketrampilanApi->all_trash();
        $result = $jurusan['body']['data'];
        $template = session('template');
        // dd($template);
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm " onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.master.jurusan.v_trash_jurusan')->with(['data' => $result, 'template' => $template]);
    }

    public function restore($id)
    {
        $restore = $this->tugasketrampilanApi->restore($id);
        if ($restore['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function download($id)
    {
        $post  = $this->tugasketrampilanApi->get_by_id(Help::decode(last(request()->segments())));
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        // dd($nama_file);
        $nama = $nama_file[1];
        $fileSource = $result['file_lengkap'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
