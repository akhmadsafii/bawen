<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\CommentRombelApi;
use App\ApiService\Learning\FeedApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class FeedsRombelController extends Controller
{
    private $feedApi;
    private $profileApi;
    private $commentApi;
    public $perPage = 5;

    public function __construct()
    {
        $this->feedApi = new FeedApi();
        $this->profileApi = new ProfileApi();
        $this->commentApi = new CommentRombelApi();
    }

    public function index(Request $request)
    {
        $this->perPage = $request->paginate ? $request->paginate : 5;
        $feed = $this->feedApi->get_by_rombel(session('id_rombel'));
        $feed = $feed['body']['data'];
        $myCollectionObj = collect($feed);
        $feed = $this->paginate($myCollectionObj);
        $feed->withPath('');
        return view('content.e_learning.feed_rombel.v_feed_rombel')->with(['template' => session('template'), 'feed' => $feed]);
    }

    public function paginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $this->perPage), $items->count(), $this->perPage, $page, $options);
    }

    public function detail(Request $request)
    {
        $id = $request['id'];
        $feed = $this->feedApi->get_by_id_feed_rombel($id);
        $result = $feed['body']['data'];
        $profile = $this->profileApi->get_profile();
        $result['my_profile'] =  $profile['body']['data']['file'];
        // dd($result);
        $html = '
        <div class="d-flex flex-column comment-section">
            <div class="bg-white p-2">
                <input type="hidden" name="id_feed" value="'.$result['id'].'">
                <div class="d-flex flex-row user-info"><img class="rounded-circle"
                        src="'.$result['user_profil'].'" width="40">
                    <div class="d-flex flex-column justify-content-start ml-2"><span
                            class="d-block font-weight-bold name">'.ucwords($result['pengirim']).'</span><span
                            class="date text-black-50">'.$result['role'].' - '.$result['terbit'].'</span></div>
                </div>
                <div class="mt-2">
                    <p class="comment-text">'.$result['berita'].'.
                    </p>
                </div>
            </div>
            <div class="bg-white">
                <div class="d-flex flex-row fs-12">
                    <div class="like p-2 cursor">
                        <i class="fa fa-commenting-o"></i>
                        <span class="ml-1">10
                            Comment</span>
                    </div>
                </div>
            </div>
            <div class="bg-light p-2">
                <div class="d-flex flex-row align-items-start">
                    <img class="rounded-circle" src="'.$result['my_profile'].'" width="40">
                    <textarea class="form-control ml-1 shadow-none textarea" name="comment"></textarea>
                </div>
                <div class="mt-2 text-right">
                    <button class="btn btn-primary btn-sm shadow-none" type="submit" id="tambahKomentar">Post comment</button>
                    <button class="btn btn-outline-primary btn-sm ml-1 shadow-none" type="button"
                        data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
        ';
        return response()->json($html);
    }

    public function store(Request $request)
    {
        // dd($request);
        $insert_data = array(
            "berita" => $request['berita'],
            "id_rombel" => session("id_rombel"),
            "role" => session("role"),
        );
        $tes = $this->feedApi->create_rombel(json_encode($insert_data));
        // dd($tes);
        $tes = $tes['body']['data'];
        // dd($tes);
        $data = '
        <div class="media-block">
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture"
                    src="'.$tes['user_profil'].'"></a>
            <div class="media-body">
                <div class="mar-btm">
                    <a href="#"
                        class="btn-link text-semibold media-heading box-inline">'.ucwords($tes['pengirim']).'.</a>
                    <p class="text-muted text-sm"><i class="fa fa-user-circle-o fa-lg"></i> - '. $tes['role'] .' -
                        '. $tes['terbit'] .'
                    </p>
                </div>
                <p>'. $tes['berita'] .'.</p>
                <div class="pad-ver">
                    
                    <button class="ml-2 btn btn-sm btn-transparent"
                        data-target="#collapseExample_'.$tes['id'].' " data-toggle="collapse"
                        href="#collapseExample_'. $tes['id'].'" role="button" aria-expanded="false"
                        aria-controls="collapseExample_'. $tes['id'].'">
                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                        0
                    </button>
                </div>
                <hr>
                <div class="collapse" id="collapseExample_'. $tes['id'].'">
                    <div id="dataKomentar_'.$tes['id'].'">
                        <div class="ml-3 mb-5">
                            <form class="p-3 comment">
                                <div class="form-group mb-2">
                                    <label for="exampleInputEmail1">Komentar</label>
                                    <input type="text" class="form-control"
                                        id="InputKomentar_'. $tes['id'] .'"
                                        placeholder="Tulis komentar anda">
                                </div>
                                <a onclick="submitComment('. $tes['id'].')" id="submitComment"
                                    class="btn btn-sm btn-success text-white">Balas</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        ';
        return response()->json($data);
    }

    public function store_comment(Request $request)
    {
        $insert_data = array(
            "komentar" => $request['comment'],
            "id_feed_rombel" => $request['id_feed'],
            "id_rombel" => session("id_rombel"),
            "role" => session("role"),
        );
        $result = $this->commentApi->create(json_encode($insert_data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'id_feed'  => $request['id_feed'],
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
