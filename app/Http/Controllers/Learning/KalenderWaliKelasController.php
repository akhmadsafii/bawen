<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\KalenderWaliKelasApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;

class KalenderWaliKelasController extends Controller
{
    private $kalenderApi;

    public function __construct()
    {
        $this->kalenderApi = new KalenderWaliKelasApi();
    }

    public function index(Request $request)
    {
        // dd("tes");
        Session::put('title', 'Kalender Rombel');
        // dd(session()->all());
        if (request()->ajax()) {
            $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
            $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
            $kalender = $this->kalenderApi->get_acara($start, $end, session('id_rombel'));
            $data = $kalender['body']['data'];
            return Response::json($data);
        }
        return view('content.e_learning.rombel.kalender.v_kalender')->with(['template' => session('template')]);
    }

    public function store(Request $request)
    {
        $insertArr = [
            'title' => $request->title,
            'start' => $request->start,
            'end' => $request->end,
            'id_rombel' => session('id_rombel'),
            'id_sekolah' => session('id_sekolah'),
            'role' => session('role')
        ];
        $result = $this->kalenderApi->create(json_encode($insertArr));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderApi->get_by_id($request->id);
        // dd($kalender);
        $kalender = $kalender['body']['data'];

        $updateArr = [
            'id' => $request->id,
            'title' => $request->title,
            'start' => $request->start,
            'end' => $request->end,
            'id_rombel' => session('id_rombel'),
            'id_sekolah' => $kalender['id_sekolah'],
        ];
        $result  = $this->kalenderApi->update_info(json_encode($updateArr));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->kalenderApi->soft_delete($request->id);
        if ($delete['code'] == 200) {
            $status = "berhasil";
            $message = $delete['body']['message'];
            $icon = 'success';
        } else {
            $status = "gagal";
            $message =  $delete['body']['message'];
            $icon = "error";
        }
        // dd($delete);
        return Response::json([
            'status' => $status,
            'message' => $message,
            'icon' => $icon
        ]);
    }
}
