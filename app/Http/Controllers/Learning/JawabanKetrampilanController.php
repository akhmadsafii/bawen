<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\JawabanKetrampilanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class JawabanKetrampilanController extends Controller
{
    private $jawabanKetrampilanApi;

    public function __construct()
    {
        $this->jawabanKetrampilanApi = new JawabanKetrampilanApi();
    }

    public function get_by_tugas(Request $request)
    {
        $jawaban = $this->jawabanKetrampilanApi->get_by_tugas($request['id_tugas']);
        // dd($jawaban);
        $result = $jawaban['body']['data'];
        if ($request['id_tugas'] == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('download', function ($row) {
                // dd($row);
                $jawaban = $row['jawaban'];
                $downloads = '';
                $kelas = '';
                if (empty($jawaban)) {
                    $downloads .= '<small class="text-danger">Tidak ada tugas</small>';
                } else {
                    foreach ($jawaban as $key) {
                        if ($key['nilai'] != null) {
                            $nilai = $key['nilai'];
                            $kelas = 'info';
                        } else {
                            $nilai = "Belum Diinput";
                            $kelas = 'danger';
                        }
                        $downloads .= '<a href="jawaban_ketrampilan/download/' . Help::encode($key['id']) . '" target="_blank" class="btn btn-purple btn-sm"><i class="fa fa-download"></i></a> &nbsp;<a href="javascript:void(0)" class="btn btn-' . $kelas . ' btn-sm" onclick="input_nilai(' . $key['id'] . ',' . $key['nilai'] . ')">' . $nilai . '</a> &nbsp;';
                    }
                }
                return $downloads;
            });
        $table->editColumn('dikumpul', function ($row) {
            $jawaban = $row['jawaban'];
            $kumpul = '';
            if (empty($jawaban)) {
                $kumpul .= '<small class="text-danger">Belum Mengumpulkan</small>';
            } else {
                foreach ($jawaban as $key) {
                    $kumpul .= Help::getTanggalLengkap($key['created_at']);
                }
            }
            return $kumpul;
        });
        $table->editColumn('terlambat', function ($row) {
            $jawaban = $row['jawaban'];
            $terlambat = '';
            if (empty($jawaban)) {
                $terlambat .= '<small class="text-warning">Belum Mengumpulkan</small>';
            } else {
                foreach ($jawaban as $key) {
                    if ($key['created_at'] > $row['maks_pengumpulan']) {
                        $terlambat .= '<a class="text-danger"><i class="fas fa-exclamation-circle"></i> Terlambat</a>';
                    } else {
                        $terlambat .= '<a class="text-success"><i class="fas fa-check-circle"></i> Tepat Waktu</a>';
                    }
                }
            }

            if ($row['maks_pengumpulan'] == null) {
                $terlambat .= '<a class="text-success"><i class="fas fa-check-circle"></i> Tepat Waktu</a>';
            }
            return $terlambat;
        });
        $table->rawColumns(['download', 'dikumpul', 'terlambat']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_by_jawaban_siswa(Request $request)
    {
        $jawaban = $this->jawabanKetrampilanApi->get_by_tugas_siswa($request['id_tugas']);
        $result = $jawaban['body']['data'];
        if ($request['id_tugas'] == 0) {
            $result = [];
        }
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('download', function ($row) {
                if ($row['nilai'] != null) {
                    $nilai = $row['nilai'];
                    return '<a href="jawaban_ketrampilan/download/' . Help::encode($row['id']) . '" target="_blank" class="btn btn-purple btn-sm"><i class="fa fa-download"></i></a> &nbsp;<a href="javascript:void(0)" class="btn btn-info btn-sm">' . $nilai . '</a> &nbsp;';
                } else {
                    $nilai = "Belum dinilai";
                    return '<a href="jawaban_ketrampilan/download/' . Help::encode($row['id']) . '" target="_blank" class="btn btn-purple btn-sm"><i class="fa fa-download"></i></a> &nbsp;<a href="javascript:void(0)" class="btn btn-warning btn-sm">Belum Dinilai</a> &nbsp;<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="' . $row['id'] . '"><i class="fas fa-trash-alt"></i></a>';
                }
            });
        $table->editColumn('pengumpulans', function ($rows) {
            return Help::getTanggalLengkap($rows['pengumpulan']);
        });
        $table->editColumn('status_kumpul', function ($row) {
            $status = 'danger';
            if ($row['status_pengumpulan'] != 'Terlambat') {
                $status = 'success';
            }
            return '<small class="text-' . $status . '">' . $row['status_pengumpulan'] . '</small>';
        });
        $table->rawColumns(['download', 'pengumpulans', 'status_kumpul']);
        $table->addIndexColumn();

        return $table->make(true);
    }


    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_tugas_keterampilan' => $request['id_tugas_ketrampilan'],
            'id_room' => session('id_room(' . request()->segment(4) . ').id_room'),
            'id_kelas_siswa' => session('id_kelas_siswa'),
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->jawabanKetrampilanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->jawabanKetrampilanApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
            File::delete($path);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_guru'];
        $post  = $this->jawabanKetrampilanApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'informasi_lain' => $request->informasi_lain,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->jawabanKetrampilanApi->update_info(json_encode($data));
            File::delete($path);
            if ($result['code'] == 200) {
                return response()->json([
                    'success' => $result['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'success' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal'
                ]);
            }
        } else {
            $result = $this->jawabanKetrampilanApi->update(json_encode($data));
            if ($result['code'] == 200) {
                return response()->json([
                    'success' => $result['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'success' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal'
                ]);
            }
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->jawabanKetrampilanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function save_nilai(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'nilai' => $request['nilai'],
        );
        $update = $this->jawabanKetrampilanApi->save_nilai(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->mapelApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function download($id)
    {
        $post  = $this->jawabanKetrampilanApi->get_by_id(Help::decode(last(request()->segments())));
        // dd($post);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        $nama = end($nama_file);
        $fileSource = $result['file_asset'];
        // dd($fileSource);
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            return $contents;
        } else {
            return false;
        }
    }
}
