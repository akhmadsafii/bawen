<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\MapelApi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportFile_absensi  implements FromView, ShouldAutoSize, WithHeadings, WithEvents
{
    //
    protected $dataSiswaApi;
    protected $kelasSiswaApi;
    protected $mapelApi;
    public $kelas_siswa;
    public $mapel;
    public $rombel;
    public $tahun;
    /**
     * __construct function new class Api
     */

    public function __construct($kelas_siswa, $mapel, $rombel, $tahun)
    {
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->dataSiswaApi = new DataSiswaApi();
        $this->mapelApi = new MapelApi();
        $this->kelas_siswa  = $kelas_siswa;
        $this->mapel        = $mapel;
        $this->rombel       = $rombel;
        $this->tahun        = $tahun;
    }

    public function headings(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
    public function view(): View
    {
        $absensi = $this->dataSiswaApi->get_detail_absensi($this->kelas_siswa, $this->mapel, $this->rombel, $this->tahun);
        // dd($absensi);
        $absensi = $absensi['body']['data'];
        $siswa = $this->kelasSiswaApi->get_by_id($this->kelas_siswa);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $mapel = $this->mapelApi->get_by_id($this->mapel);
        // dd($mapel);
        $mapel = $mapel['body']['data'];
        $profil = array(
            'nama' => $siswa['nama'],
            'nis' => $siswa['nis'],
            'rombel' => $siswa['rombel'],
            'mapel' => $mapel['nama'],
        );
        $param = [
            'profil' => $profil,
            'absensi' => $absensi,
        ];
        return view('content.e_learning.export.v_export_absensi')->with($param);
    }

    /** laba rugi by tahun  */
    // private function laba_rugibytahun($tahun)
    // {
    //     $rugilaba     = $this->SppApiservice->report_labarugibytahun($tahun);
    //     $parse_rugilaba = $rugilaba['body']['data'] ?? array();
    //     //dd($rugilaba);
    //     $total_pendapatan = 0;
    //     $total_pengeluaran = 0;
    //     $total_laba_bersih = 0;
    //     foreach ($parse_rugilaba as $key => $laporan) {
    //         foreach ($laporan['sub_akun_kategori'] as $subakun) {
    //             foreach ($subakun['akun'] as $akun) {
    //                 if ($laporan['id'] == 4) {
    //                     $total_pendapatan += intval($akun['nominal']);
    //                 } elseif ($laporan['id'] == 5) {
    //                     $total_pengeluaran += intval($akun['nominal']);
    //                 }
    //             }
    //         }
    //     }
    //     $total_laba_bersih = intval($total_pendapatan) - intval($total_pengeluaran);
    //     $data = array();
    //     $data['laba_rugi']   = intval($total_laba_bersih);
    //     $data['pendapatan']   = intval($total_pendapatan);
    //     $data['pengeluaran']   = intval($total_pengeluaran);
    //     return $data;
    // }
}
