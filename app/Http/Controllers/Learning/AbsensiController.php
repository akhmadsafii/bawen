<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\DataSiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AbsensiController extends Controller
{
    private $dataSiswaApi;

    public function __construct()
    {
        $this->dataSiswaApi = new DataSiswaApi();
    }

    public function index(Request $request)
    {
        $pertemuan = $this->dataSiswaApi->get_loop_pertemuan(session('id_room('.request()->segment(4).').id_room'));
        $pertemuan = $pertemuan['body']['data'];

        $data_siswa = $this->dataSiswaApi->rooms(session('id_room('.request()->segment(4).').id_room'));
        $result = $data_siswa['body']['data'];

        if ($request->ajax()) {
            $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                return $button;
            });
            $table->editColumn('pertemuan', function ($row) {
                return "Pertemuan ke " .$row['pertemuan'];
            });
            $table->rawColumns(['action', 'pertemuan']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Absensi Siswa');
        return view('content.e_learning.room.v_absensi_kelas')->with(['template' => session('template'), 'pertemuan' => $pertemuan]);
    }

    public function edit(Request $request)
    {
        $id = $request['id_absensi'];
        $data_siswa = $this->dataSiswaApi->get_by_id($id);
        // dd($data_siswa);
        $result = $data_siswa['body']['data'];
        $result['tanggal'] = Help::getTanggal($result['tgl_lahir']);
        // dd($result);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'id_kelas_siswa' => $request->id_kelas_siswa,
                'id_room' => $request->id_room,
                'id_rombel' => $request->id_rombel,
                'id_kelas' => $request->id_kelas,
                'pertemuan' => $request->id_pertemuan,
                'id_guru' => $request->id_guru,
                'id_mapel' => $request->id_mapel,
                'id_ta_sm' => session('id_tahun_ajar'),
                'absensi' => $request->absensi,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            ];
        $update = $this->dataSiswaApi->create(json_encode($data)); //jgn lupa di json_encode waktu mau kirim ke apinya.
            // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function filter_date(Request $request)
    {
        if (!empty($request->from_date)) {
         $data = $this->dataSiswaApi->filter_date(session('id_room('.request()->segment(4).').id_room'), $request->from_date, $request->to_date, $request->pertemuan);
        //  dd($data);
         $data = $data['body']['data'];
        } else {
            $data = $this->dataSiswaApi->rooms(session('id_room('.request()->segment(4).').id_room'));
            $data = $data['body']['data'];
        }
        $table = datatables()->of($data)
        ->addColumn('action', function ($data) {
            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
            return $button;
        });
        $table->editColumn('pertemuan', function ($row) {
            return "Pertemuan ke " .$row['pertemuan'];
        });
        $table->rawColumns(['action', 'pertemuan']);
        $table->addIndexColumn();
        return $table->make(true);
    }
}
