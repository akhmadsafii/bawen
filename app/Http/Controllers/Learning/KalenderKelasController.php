<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\User\ProfileApi;
use App\ApiService\Learning\KalenderKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class KalenderKelasController extends Controller
{
    private $kalenderkelasApi;
    private $profileApi;

    public function __construct()
    {
        $this->kalenderkelasApi = new KalenderKelasApi();
        $this->profileApi = new ProfileApi();
    }

    public function index()
    {
        Session::put('title', 'Kalender Kelas');
        if (request()->ajax()) {
            $kalender = $this->kalenderkelasApi->get_acara(session('id_room(' . request()->segment(4) . ').id_room'));
            // dd($kalender);
            $data = $kalender['body']['data'];
            return Response::json($data);
        }
        if (session('role') == 'siswa') {
            return view('content.e_learning.room.siswa.v_kalender_kelas')->with(['template' => session('template')]);
        }

        return view('content.e_learning.room.v_kalender_kelas')->with(['template' => session('template')]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $insertArr = [
            'title' => $request->title,
            'link' => $request->link,
            'start' => date('Y-m-d', strtotime($request->start)),
            'end' => date('Y-m-d', strtotime($request->end)),
            'id_room' => session('id_room(' . request()->segment(4) . ').id_room'),
            'role' => session('role'),
            'id_sekolah' => session('id_sekolah'),
        ];
        $result = $this->kalenderkelasApi->create(json_encode($insertArr));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderkelasApi->get_by_id($request->id);
        $kalender = $kalender['body']['data'];
        // dd($kalender);

        $updateArr = [
            'id' => $request->id,
            'title' => $request->title,
            'link' => $request->link,
            'start' => $request->start,
            'end' => $request->end,
            'id_room' => $kalender['id_room'],
            'pembuat' => $kalender['pembuat'],
            'role' => $kalender['role'],
            'id_sekolah' => $kalender['id_sekolah'],
        ];
        $result  = $this->kalenderkelasApi->update_info(json_encode($updateArr));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderkelasApi->get_by_id($request->id);
        $kalender = $kalender['body']['data'];
        return response()->json($kalender);
    }

    public function trash(Request $request)
    {
        $result = $this->kalenderkelasApi->soft_delete($request['id']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function data_kalender()
    {
        $kalender = $this->kalenderkelasApi->get_by_room(session('id_room(' . request()->segment(4) . ').id_room'));
        // dd($kalender);
        $result = $kalender['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<button type="button" name="delete" class="btn btn-danger btn-sm del-' . $data['id'] . '" onclick="deletes(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
            $table->editColumn('tanggal', function ($row) {
                return Help::getTanggal($row['start'])." - ".Help::getTanggal($row['end']);
            });
        $table->rawColumns(['action','tanggal']);
        $table->addIndexColumn();
        return $table->make(true);
    }
}
