<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\JawabanKetrampilanApi;
use App\ApiService\Learning\JawabanpengetahuanApi;
use App\ApiService\Learning\TugasKetrampilanApi;
use App\ApiService\Learning\TugasPengetahuanApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TugasWaliKelasController extends Controller
{
    private $tugasketrampilanApi;
    private $pengetahuanApi;
    private $pelajaranApi;
    private $jawabanKetrampilanApi;
    private $jawabanpengetahuanApi;

    public function __construct()
    {
        $this->tugasketrampilanApi = new TugasKetrampilanApi();
        $this->pengetahuanApi = new TugasPengetahuanApi();
        $this->pelajaranApi = new GuruPelajaranApi();
        $this->jawabanKetrampilanApi = new JawabanKetrampilanApi();
        $this->jawabanpengetahuanApi = new JawabanpengetahuanApi();
    }

    public function pengetahuan()
    {
        session()->put('title', 'Tugas Pengetahuan');
        $mapel_kelas = $this->pelajaranApi->by_rombel(session('id_rombel'), session('id_tahun_ajar'));
        $mapel_kelas = $mapel_kelas['body']['data'];
        return view('content.e_learning.rombel.tugas.v_tugas_pengetahuan')->with(['template' => session('template'), 'mapel' => $mapel_kelas]);
    }
    public function ketrampilan()
    {
        $mapel_kelas = $this->pelajaranApi->by_rombel(session('id_rombel'), session('id_tahun_ajar'));
        // dd($mapel_kelas);
        $mapel_kelas = $mapel_kelas['body']['data'];
        return view('content.e_learning.rombel.tugas.v_tugas_ketrampilan')->with(['template' => session('template'), 'mapel' => $mapel_kelas]);
    }

    public function get_ketrampilan_datatable(Request $request)
    {
        $id_mapel = $request['id_mapel'];
        $ketrampilan = $this->tugasketrampilanApi->get_by_mapel_wali_kelas(session('id_rombel'), $id_mapel);
        // dd($ketrampilan);
        $result = $ketrampilan['body']['data'];
        if ($id_mapel == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            // $table->editColumn('kd', function ($k_dasar) {
            //     $kade = '';
            //     foreach ($k_dasar['kompetensi_dasar'] as $km) {
            //         $kade .= '<span class="badge badge-info">' . $km['nama'] . '</span> &nbsp;';
            //     }
            //     return $kade;
            // });
            ->addColumn('download', function ($aksi) {
                if ($aksi['file'] == null) {
                    $download = '-';
                } else {
                    $download = '<a href="ket_download/' . Help::encode($aksi['id']) . '" class="btn btn-warning btn-xs">download file <i class="fa fa-download"></i></a>';
                }
                return $download;
            });
        $table->editColumn('jawaban', function ($jaw) {
            return '<a href="javascript:void(0)" data-id="' . $jaw['id'] . '" class="hasil-' . $jaw['id'] . ' btn btn-success btn-sm hasil"> <i class="fa fa-chevron-right"></i> Lihat Jawaban Siswa</a>';
        });
        $table->rawColumns(['download', 'jawaban']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_pengetahuan_datatable(Request $request)
    {
        // $id_rombel =
        $id_mapel = $request['id_mapel'];
        $pengetahuan = $this->pengetahuanApi->get_by_mapel_rombel(session('id_rombel'), $id_mapel);
        // dd($pengetahuan);
        $result = $pengetahuan['body']['data'];
        if ($id_mapel == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="hasil-' . $data['id'] . ' btn btn-success btn-xs hasil"> <i class="fa fa-chevron-right"></i> Lihat Jawaban Siswa</a>';
            });
        $table->editColumn('skema', function ($row) {
            $skema1 = $row['skema_penilaian'];
            $jajal = '';
            $skema = explode('<br>', $skema1);
            foreach ($skema as $key) {
                $jajal .= '<span class="badge badge-success">' . $key . '</span> &nbsp;';
            }
            return $jajal;
        });
        $table->editColumn('tombol', function ($aksi) {
            return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="' . $aksi['id'] . '" data-original-title="Edit" class="edit upload-' . $aksi['id'] . ' btn btn-danger btn-xs edit"><i class="fa fa-upload"></i> Upload Jawaban</a>';
        });

        $table->editColumn('instruksi', function ($row) {
            return strip_tags($row['instruksi']);
        });
        $table->editColumn('download', function ($aksi) {
            if ($aksi['file'] == null) {
                $download = '-';
            } else {
                $download = '<a href="peng_download/' . $aksi['id'] . '" class="btn download-' . $aksi['id'] . ' btn-warning btn-xs">download file <i class="fa fa-download"></i></a>';
            }
            return $download;
        });
        $table->rawColumns(['action', 'skema', 'tombol', 'download', 'instruksi']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function download_ket($id)
    {
        $id_bahan_ajar = request()->segment(6);
        $post  = $this->tugasketrampilanApi->get_by_id($id_bahan_ajar);
        // dd($post);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        // dd($nama_file);
        $nama = $nama_file[1];
        $fileSource = $result['file_lengkap'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    public function download_peng($id)
    {
        $id_bahan_ajar = request()->segment(6);
        $post  = $this->pengetahuanApi->get_by_id($id_bahan_ajar);
        // dd($post);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        // dd($nama_file);
        $nama = $nama_file[1];
        $fileSource = $result['file_lengkap'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function download_jawaban_peng($id)
    {
        $post  = $this->jawabanpengetahuanApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        $nama = $nama_file[1];
        $fileSource = $result['file_lengkap'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    public function get_by_tugas(Request $request)
    {
        $jawaban = $this->jawabanKetrampilanApi->get_by_tugas($request['id_tugas']);
        $result = $jawaban['body']['data'];
        if ($request['id_tugas'] == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('download', function ($row) {
                $jawaban = $row['jawaban'];
                $downloads = '';
                foreach ($jawaban as $key) {
                    $downloads .= '<a href="download/jawaban/ketrampilan/' . Help::encode($key['id']) . '" class="btn btn-warning btn-xs">download file <i class="fa fa-download"></i></a> &nbsp;';
                }
                return $downloads;
            });
        $table->rawColumns(['download']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_by_tugas_peng(Request $request)
    {
        $jawaban = $this->jawabanpengetahuanApi->get_by_tugas($request['id_tugas']);
        // dd($jawaban);
        $result = $jawaban['body']['data'];
        if ($request['id_tugas'] == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('download', function ($row) {
                $jawaban = $row['jawaban'];
                $downloads = '';
                foreach ($jawaban as $key) {
                    $downloads .= '<a href="' . route('download-wali_kelas_hasil_peng', Help::encode($key['id'])) . '" class="btn btn-warning btn-xs">download file <i class="fa fa-download"></i></a> &nbsp;';
                }
                return $downloads;
            });
        $table->rawColumns(['download']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
