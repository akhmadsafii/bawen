<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\JurnalApi;
use App\ApiService\User\GuruApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\MapelKelasApi;
use App\ApiService\Learning\MapelWaliKelasApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JadwalApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MapelWaliKelasController extends Controller
{
    private $mapelWaliKelasApi;
    private $guruApi;
    private $gurupelajaranApi;
    private $jadwalApi;
    private $jurnalApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
        $this->mapelWaliKelasApi = new MapelWaliKelasApi();
        $this->rombelApi = new RombelApi();
        $this->mapelkelasApi = new MapelKelasApi();
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->roomApi = new RoomApi();
        $this->jurnalApi = new JurnalApi();
        $this->jadwalApi = new JadwalApi();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        $mapel_kelas = $this->gurupelajaranApi->by_rombel(session('id_rombel'), session('id_tahun_ajar'));
        $mapel_kelas = $mapel_kelas['body']['data'];
        session()->put('title', 'Guru Yang Mengajar');
        return view('content.e_learning.rombel.data_mapel.v_data_mapel')->with(['template' => session('template'), 'mapel' => $mapel_kelas]);
    }

    public function detail(Request $request)
    {
        $guru = $this->gurupelajaranApi->get_by_id($request['id_guru']);
        $guru = $guru['body']['data'];
        // dd($guru);
        $html = '';
        if (!empty($guru)) {
            $html .= '<div class="table-responsive">
            <p></p>
            <table class="table table-hover">
                <tr class="bg-info">
                    <th colspan="5">
                        <h3 class="box-title text-white"><i class="fas fa-user-alt"></i> Detail Guru</h3>
                    </th>
                </tr>
                <tr>
                    <td rowspan="5" colspan="3" class="vertical-middle text-center">
                        <img src="'.$guru['file'].'"
                            width="200" alt="">
                    </td>
                </tr>
                <tr>
                    <th class="vertical-middle text-center">Nama</th>
                    <td class="vertical-middle text-center">'.strtoupper($guru['guru']).'</td>
                </tr>
                <tr>
                    <th class="vertical-middle text-center">NIP</th>
                    <td class="vertical-middle text-center">'.$guru['nip'].'</td>
                </tr>
                <tr>
                    <th class="vertical-middle text-center">Rombel</th>
                    <td class="vertical-middle text-center">'.$guru['rombel'].'</td>
                </tr>
                <tr>
                    <th class="vertical-middle text-center">Mapel</th>
                    <td class="vertical-middle text-center">'.$guru['mapel'].'</td>
                </tr>
                <tr>
                    <td colspan="5"><button class="btn btn-purple" data-toggle="collapse" data-target="#demo">Lihat Jurnal <span class="badge badge-pill bg-danger">'.count($guru['jurnal']).'</span></button></td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse" id="demo">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-purple">
                                        <th class="text-center text-white">No</th>
                                        <th class="text-center text-white">Tanggal Jurnal</th>
                                        <th class="text-center text-white">Bahan Ajar</th>
                                        <th class="text-center text-white">Hambatan</th>
                                        <th class="text-center text-white">Pemecahan</th>
                                        <th class="text-center text-white">Dibuat</th>
                                    </tr>
                                </thead>
                                <tbody>';
            if (empty($guru['jurnal'])) {
                $html.= '<tr><td colspan="6" class="text-center">Saat ini data jurnal tidak tersedia</td></tr>';
            } else {
                $no = 1;
                foreach ($guru['jurnal'] as $jurnal) {
                    $html.= '<tr>
                            <td class="text-center">' . $no++ . '</td>
                            <td class="text-center">' . Help::getTanggal($jurnal['tanggal']) . '</td>
                            <td class="text-center">' . $jurnal['bahan_ajar'] . '</td>
                            <td class="text-center">' . $jurnal['hambatan'] . '</td>
                            <td class="text-center">' . $jurnal['pemecahan'] . '</td>
                            <td class="text-center">' . Help::getTanggal($jurnal['created_at']). '</td>
                        </tr>';
                }
            }
            $html .= '</tbody></table></div></td></tr></table></div>';
        }


        return response()->json($html);
    }

    public function jadwal_datatable(Request $request)
    {
        $id_guru = $request['id_guru'];
        $jadwal = $this->jadwalApi->get_by_rombel_guru($id_guru, session('id_rombel'), session('id_tahun_ajar'));
        dd($jadwal);
        $result = $jadwal['body']['data'];
        if ($request['id_guru'] == 0) {
            $result = [];
        }
        $table = datatables()->of($result);
        $table->addIndexColumn();

        return $table->make(true);
    }
}
