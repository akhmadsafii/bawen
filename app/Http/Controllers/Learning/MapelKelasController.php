<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Alumni\DashboardApi;
use App\ApiService\Learning\CommentApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Learning\FeedApi;
use App\ApiService\Learning\PertemuanApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\DashboardApi as MasterDashboardApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Facades\DataTables;

class MapelKelasController extends Controller
{
    private $profileApi;
    private $datasiswaApi;
    private $roomApi;
    private $rombelApi;
    private $feedApi;
    private $commentApi;
    private $jurusanApi;
    private $kelassiswaApi;
    private $dashboardApi;
    private $hashids;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->roomApi = new RoomApi();
        $this->rombelApi = new RombelApi();
        $this->datasiswaApi = new DataSiswaApi();
        $this->feedApi = new FeedApi();
        $this->jurusanApi = new JurusanApi();
        $this->commentApi = new CommentApi();
        $this->pertemuanApi = new PertemuanApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->dashboardApi = new MasterDashboardApi();
        $this->hashids = new Hashids();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        Session::put('title','Halaman Utama Smart School');
        if (session("role") == "siswa") {
            $mapel = $this->roomApi->get_siswa(session('id_tahun_ajar'));
            $mapel = $mapel['body']['data'];
            return view('content.e_learning.room.kelas.v_data_mapel')->with(['template' => session('template'), 'mapel' => $mapel]);
        } elseif (session("role") == "guru") {
            $rombel = $this->profileApi->get_rombel(session('id_tahun_ajar'));
            // dd($rombel);
            $rombel = $rombel['body']['data'];
            return view('content.e_learning.room.kelas.v_data_kelas')->with(['template' => session('template'), 'rombel' => $rombel]);
        } elseif (session("role") == "walikelas") {
            $profile_guru = $this->profileApi->get_profile();
            // dd($profile_guru);
            $profile_guru = $profile_guru['body']['data'];
            // dd($profile_guru);
            $id_rombel = $profile_guru['id_rombel'];
            session()->put('id_rombel', $id_rombel);
            $feed = $this->feedApi->get_by_rombel_wali_kelas(session('id_rombel'));
            $feed = $feed['body']['data'];
            return view('content.e_learning.rombel.v_feeds')->with(['template' => session('template'), 'feed' => $feed, "profile" => $profile_guru]);
        } elseif (session("role") == "admin") {
            $dashboard = $this->dashboardApi->get_statistic();
            // dd($)
            $dashboard = $dashboard['body']['data'];
            $data = [];
            $master = [];
            foreach ($dashboard['user'] as $key => $das) {
                $data['label'][] = $key;
                $data['data'][] = (int) $das;
            }
            foreach ($dashboard['master'] as $key => $ms) {
                $master['label'][] = $key;
                $master['data'][] = (int) $ms;
            }
            $template = 'default';
            return view('content.dashboard.learning_admin')->with(['list_user' => json_encode($data), 'list_master' => json_encode($master), 'template' => $template]);
        } elseif (session('role') == 'superadmin') {
            return view('content.dashboard.superadmin')->with(['template' => 'default']);
        } elseif (session('role') == 'supervisor') {
            $rombel = $this->rombelApi->get_by_sekolah();
            $rombel = $rombel['body']['data'];
            // dd($rombel);
            return view('content.supervisor.learning.kelas.v_select_class')->with(['template' => session('template'), 'rombel' => $rombel]);
        }
    }

    public function store(Request $request)
    {
        // dd(session()->all());
        $insert_data = array(
            "berita" => $request['berita'],
            "id_rombel" => session("id_rombel"),
            "role" => session("role"),
            "id_sekolah" => session("id_sekolah"),
            "id_sosial" => "tahu",
        );
        $tes = $this->feedApi->create_rombel(json_encode($insert_data));
        $key = $tes['body']['data'];
        // dd($key);
        $data = '<div class="bg-white border mt-2">
            <div>
                <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                    <div class="d-flex flex-row align-items-center feed-text px-2"><img class="rounded-circle" src="' . $key['user_profil'] . '" width="45">
                        <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">' . $key['pengirim'] . '</span><span class="text-black-50 time">' . $key['terbit'] . '</span></div>
                    </div>
                    <div class="feed-icon px-2"><i class="fa fa-ellipsis-v text-black-50"></i></div>
                </div>
            </div>
            <div class="p-2 px-3"><span>' . $key['berita'] . '</span></div>
            <div class="d-flex justify-content-end socials p-2 py-3"><i class="fa fa-thumbs-up"></i><a href="' . URL::route('wali_kelas-detail_post', $key['id_feed_encode']) . '"><i class="fa fa-comments-o"><span class="count">' . $key['jumlah_komentar'] . '</span></a></i> <i class="fa fa-share"></i></div>
        </div>';
        return response()->json($data);
    }

    public function detail_post($id)
    {
        $id_post = $this->hashids->decode($id)[0];
        $feed = $this->feedApi->rombel_wali_kelas_detail($id_post);
        // dd($feed);
        $feed = $feed['body']['data'];
        Session::put('title', 'Data Sekolah');
        return view('content.e_learning.rombel.v_post_detail')->with(['template' => session('template'), 'feed' => $feed]);
    }

    public function comment(Request $request)
    {
        // dd($request);
        $idfeed = intval($request['id_feed']);
        // dd($idfeed);

        $insert_data = array(
            "id_feed_rombel" => $idfeed,
            "komentar" => $request['komentar'],
            "nama" => session("username"),
            "id_rombel" => session("id_rombel"),
            "role" => session("role"),
            "id_sekolah" => session("id_sekolah"),
            "id_sosial" => "tes",
        );
        // dd($insert_data);
        $feed = $this->commentApi->create_rombel(json_encode($insert_data));
        // dd($feed);
        $feed = $feed['body']['data'];
        $data = '
        <ul class="comments-list">
              <li class="comment">
                  <a class="pull-left" href="#">
                      <img class="avatar" src="' . $feed['user_profil'] . '" alt="avatar">
                  </a>
                  <div class="comment-body">
                      <div class="comment-heading">
                          <h4 class="user">' . $feed['nama'] . '</h4>
                          <h5 class="time">' . $feed['terbit'] . '</h5>
                      </div>
                      <p>' . $feed['komentar'] . '</p>
                  </div>
              </li>
          </ul>
        ';
        return response()->json($data);
    }

    public function get_pertemuan_by_room(Request $request)
    {
        $id_kelas_siswa = $this->kelassiswaApi->get_by_id_siswa(session('id'));
        $id_kelas_siswa = $id_kelas_siswa['body']['data'];
        $id_kelas_siswa = $id_kelas_siswa[0]['id'];
        $get_pertemuan = $this->datasiswaApi->get_by_room($request['id_room'], $id_kelas_siswa);
        // dd($get_pertemuan);
        $result = $get_pertemuan['body']['data'];
        if ($request['id_room'] == 0) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-xs edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-xs" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function rombel($id)
    {
        // dd($id);
        $feed = $this->feedApi->get_by_rombel_wali_kelas($this->hashids->decode($id)[0]);
        $feed = $feed['body']['data'];
        session()->put('id_rombel', $this->hashids->decode($id)[0]);
        return view('content.e_learning.rombel.v_feeds')->with(['template' => session('template'), 'feed' => $feed]);
    }
}
