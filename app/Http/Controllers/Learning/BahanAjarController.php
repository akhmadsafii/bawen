<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\BahanAjarApi;
use App\ApiService\Learning\PertemuanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class BahanAjarController extends Controller
{
    private $bahanAjarApi;
    private $pertemuanApi;

    public function __construct()
    {
        $this->bahanAjarApi = new BahanAjarApi();
        $this->pertemuanApi = new PertemuanApi();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        $pertemuan = $this->pertemuanApi->get_all();
        $pertemuan = $pertemuan['body']['data'];
        $bahan_ajar = $this->bahanAjarApi->get_by_rombel_tahun(session('id_room(' . request()->segment(4) . ').id_rombel'), session('id_room(' . request()->segment(4) . ').id_mapel'), session('id_tahun_ajar'));
        if ($bahan_ajar['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        // dd($bahan_ajar);
        $result = $bahan_ajar['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '"class="btn btn-info btn-sm edit"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i></button>';
                    if ($data['file'] != null) {
                        $button .= '<a href="bahan_ajar/download/' . $data['id'] . '" target="blank" class="btn btn-warning btn-sm"><i class="fa fa-download"></i></a>';
                    } else {
                        $button .= '<a href="javascript:void" style="cursor: context-menu" class="btn btn-dark btn-sm"><i class="fa fa-times-circle"></i></a>';
                    }
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['gambar'] . '" border="0" width="20" class="img-rounded" align="center" />';
            });
            $table->editColumn('download', function ($rows) {
                if ($rows['file'] != null) {
                    return '<a href="bahan_ajar/download/' . $rows['id'] . '" target="blank" class="btn btn-warning btn-sm">Download File <i class="fa fa-download"></i></a>';
                } else {
                    return '-';
                }
            });

            $table->editColumn('link', function ($row) {
                if ($row['link'] != null) {
                    return '<a href="' . $row['link'] . '" target="_blank" class="text-info">' . Str::limit($row['link'], 15, '...') . '<i class="fas fa-directions"></i></a>';
                } else {
                    return '<a class="text-danger">Tidak tersedia link<a>';
                }
            });
            $table->rawColumns(['action', 'gambar', 'download', 'link']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Bahan Ajar');
        if (session("role") == "siswa") {
            return view('content.e_learning.room.v_bahan_ajar_siswa')->with(['template' => session('template')]);
        }
        return view('content.e_learning.room.v_bahan_ajar')->with(['template' => session('template'), 'pertemuan' => $pertemuan]);
    }

    public function get_by_id($id)
    {
        $kompetensi = $this->kompetensiApi->get_by_id($id);
        $kompetensi = $kompetensi['body']['data'];
        $data = explode('<br>', $kompetensi['kompetensi_dasar']);
        // dd($data);
        return json_encode($data);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'judul' => $request->judul,
            'link' => $request->link,
            'id_sekolah' => session('id_sekolah'),
            'id_mapel' => session('id_room(' . request()->segment(4) . ').id_mapel'),
            'id_rombel' => session('id_room(' . request()->segment(4) . ').id_rombel'),
            'pertemuan' => $request['pertemuan'],
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_guru' => session('id'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bahan_ajar/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bahanAjarApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->bahanAjarApi->create(json_encode($data));
        }

        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function show($id)
    {
        // dd('ping');
        $post  = $this->bahanAjarApi->get_by_id($id);
        $result = $post['body']['data'];
        // dd($post);
        return response()->json($result);
    }

    public function download($id)
    {
        $id_bahan_ajar = request()->segment(7);
        $post  = $this->bahanAjarApi->get_by_id($id_bahan_ajar);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['nama_file']);
        // dd($nama_file);
        $nama = end($nama_file);
        $fileSource = $result['file'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);
        if ($contents) return $contents;
        else return FALSE;
    }


    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'judul' => $request->judul,
            'link' => $request->link,
            'id_sekolah' => session('id_sekolah'),
            'id_mapel' => session('id_room(' . request()->segment(4) . ').id_mapel'),
            'id_rombel' => session('id_room(' . request()->segment(4) . ').id_rombel'),
            'pertemuan' => $request['pertemuan'],
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_guru' => session('id'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bahan_ajar/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bahanAjarApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->bahanAjarApi->update_info(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_bahan_ajar'];
        // dd($id);
        $bahan_ajar = $this->bahanAjarApi->get_by_id($id);
        $bahan_ajar = $bahan_ajar['body']['data'];
        $files = explode('/', $bahan_ajar['nama_file']);
        $last = end($files);
        // dd($last);
        $bahan_ajar['files'] = $last;
        return response()->json($bahan_ajar);
    }

    public function softdelete(Request $request)
    {
        $id = $request['id_bahan_ajar'];
        $delete = $this->bahanAjarApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
