<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\PendidikanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    private $pendidikanApi;

    public function __construct()
    {
        $this->pendidikanApi = new PendidikanApi();
    }

    public function index(Request $request)
    {
        if (session("role") == 'bkk-pelamar') {
            $pendidikan = $this->pendidikanApi->get_by_user(session('id'));
            $pendidikan = $pendidikan['body']['data'];
            return view('content.bkk.dashboard.' . session('role') . '.profile.v_pendidikan')->with(['pendidikan' => $pendidikan]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        if (session('role') == 'bkk-pelamar') {
            $data_insert = array(
                'id_user' => session('id'),
                'nama' => strtolower($request['nama']),
                'jenjang' => $request['kualifikasi'],
                'jurusan' => $request['jurusan'],
                'tahun_lulus' => $request['tahun_dari']."-".$request['bulan_dari']."-01",
                'nilai_akhir' => $request['nilai_akhir'],
                'skor_nilai_akhir' => $request['nilai_skor']."#".$request['dari'],
                'informasi' => $request['informasi'],
                'id_sekolah' => session('id_sekolah'),
            );
            // dd($data_insert);
            $result = $this->pendidikanApi->create(json_encode($data_insert));
            // dd($result);
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        if (session('role') == 'bkk-pelamar') {
            $data_update = array(
                'id' => $request['id'],
                'id_user' => $request['id_user'],
                'nama' => strtolower($request['nama']),
                'jenjang' => $request['kualifikasi'],
                'jurusan' => $request['jurusan'],
                'tahun_lulus' => $request['tahun_dari']."-".$request['bulan_dari']."-01",
                'nilai_akhir' => $request['nilai_akhir'],
                'skor_nilai_akhir' => $request['nilai_skor']."#".$request['dari'],
                'informasi' => $request['informasi'],
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->pendidikanApi->update_info(json_encode($data_update));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $pendidikan = $this->pendidikanApi->get_by_id($request['id']);
        $pendidikan = $pendidikan['body']['data'];
        $pendidikan['bulan'] = date('m', strtotime($pendidikan['tahun_lulus']));
        $pendidikan['tahun'] = date('Y', strtotime($pendidikan['tahun_lulus']));
        if ($pendidikan['skor_nilai_akhir'] != null) {
            $nilai = explode('#', $pendidikan['skor_nilai_akhir']);
            $pendidikan['skor'] = $nilai[0];
            $pendidikan['skor_dari'] = $nilai[1];
        }
        return response()->json($pendidikan);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->pendidikanApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
