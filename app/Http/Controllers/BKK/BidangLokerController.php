<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\BidangLokerApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BidangLokerController extends Controller
{
    private $bidangLokerApi;

    public function __construct()
    {
        $this->bidangLokerApi = new BidangLokerApi();
    }

    public function index(Request $request)
    {
        $url = $this->bidangLokerApi->import();
        $bidang = $this->bidangLokerApi->get_by_sekolah();
        $bidang = $bidang['body']['data'];
        session()->put('title', 'Bidang Lowongan Kerja');
        $template = session('template');
        if (session('role') == 'bkk-admin' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.bkk.admin.v_bidang_loker')->with(['bidang' => $bidang, 'template' => $template]);
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $result = $this->bidangLokerApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $bidang = $this->data_bidang();
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'bidang' => $bidang
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $bidang = $this->bidangLokerApi->get_by_id($request['id']);
        $bidang = $bidang['body']['data'];
        return response()->json($bidang);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'][0],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->bidangLokerApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $bidang = $this->data_bidang();
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'bidang' => $bidang
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->bidangLokerApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $bidang = $this->data_bidang();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'bidang' => $bidang
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function hard_delete($id)
    {
        $delete = $this->bidangLokerApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->bidangLokerApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->bidangLokerApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_bidang()
    {
        $bidang = $this->bidangLokerApi->get_by_sekolah();
        $bidang = $bidang['body']['data'];
        $html = '';
        if (empty($bidang)) {
            $html .= ' <tr>
            <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
        </tr>';
        } else {
            $no = 1;
            foreach ($bidang as $bd) {
                $status = 'success';
                if ($bd['status'] != 1) {
                    $status = 'danger';
                }
                $html .= ' <tr>
                <td>'.$no++.'</td>
                <td>'.strtoupper($bd['nama']).'</td>
                <td><span
                        class="btn btn-'.$status.' btn-xs">'.Help::status($bd['status']).'</span>
                </td>

                <td>
                    <a href="javascript:void(0)" data-id="'.$bd['id'].'"
                        class="btn btn-sm btn-info edit"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                        data-id="'.$bd['id'].'"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>';
            }
        }
        return $html;
    }
}
