<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\KategoriIndustriApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KategoriIndustriController extends Controller
{
    private $katIndustriApi;

    public function __construct()
    {
        $this->katIndustriApi = new KategoriIndustriApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Kategori Industri');
        $kategori = $this->katIndustriApi->get_by_sekolah(session('id_sekolah'));
        // dd($kategori);
        if ($kategori['code'] != 200) {
            $pesan = array(
                'message' => $kategori['body']['message'],
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $kategori['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('status', function ($row) {
                $check = '';
                if ($row['status'] == 1) {
                    $check = 'checked';
                }
                return ' <label class="switch">
                <input type="checkbox" ' . $check . ' class="kat_check" data-id="' . $row['id'] . '">
                    <span class="slider round"></span>
                </label>';
            });
            $table->rawColumns(['action', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.bkk.admin.v_kategori_industri')->with(['template' => 'default']);
    }

    public function store(Request $request)
    {
        // dd($request);
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $result = $this->katIndustriApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $kategori = $this->katIndustriApi->get_by_id($request['id']);
        $kategori = $kategori['body']['data'];
        return response()->json($kategori);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'][0],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->katIndustriApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->katIndustriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->katIndustriApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
