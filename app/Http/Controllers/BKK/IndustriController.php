<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\IndustriApi;
use App\ApiService\BKK\KategoriIndustriApi;
use App\ApiService\BKK\UserApi;
use App\ApiService\Master\EnvironmentApi;
use App\ApiService\Master\IndonesiaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class IndustriController extends Controller
{
    private $userApi;
    private $industriApi;
    private $indonesiaApi;
    private $katIndustriApi;
    private $envApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->userApi = new UserApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->katIndustriApi = new KategoriIndustriApi();
        $this->envApi = new EnvironmentApi();
    }

    public function index(Request $request)
    {
        if (session('role') == 'bkk-admin' || session('role') == 'admin' || session('role') == 'supervisor') {
            Session::put('title', 'Industri');
            $routes = "bkk_industri-beranda";
            $kategori = $this->katIndustriApi->get_by_sekolah(session('id_sekolah'));
            $kategori = $kategori['body']['data'];
            $status = (isset($_GET["status"])) ? $_GET["status"] : "";
            $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
            $search = (isset($_GET["search"])) ? $_GET["search"] : "";
            // dd($search);
            $result = $this->industriApi->search($page, $status, $search, session('id_sekolah'));
            // dd($result);
            if ($result['code'] != 200) {
                $pesan = array(
                    'message' => $result['body']['message'],
                    'icon' => 'error'
                );
                return redirect()->back()->with('message', $pesan);
            }
            // dd($result);
            $template = session('template');
            if (session('role') == 'bkk-admin' || session('role') == 'admin') {
                $template = 'default';
            }
            $industri = $result['body']['data'];
            $meta = $result['body']['meta'];
            $pagination = Utils::simpleWithStatus($meta, $routes, $status, $search);

            return view('content.bkk.dashboard.bkk-admin.industri.v_industri')->with([
                'industri' => $industri, 'template' => $template, 'kategori' => $kategori,
                'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'status' => $status
            ]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        $data_industri = array(
            'nama' => $request['nama'],
            'id_user' => $request['id_user'],
            'id_kategori' => $request['kategori_industri'],
            'id_sekolah' => session('id_sekolah'),
            'telepon' => $request['telepon'],
            'deskripsi' => $request['deskripsi_perusahaan'],
            'website' => $request['website'],
            'alamat' => $request['alamat'],
            'id_provinsi' => $request['provinsi'],
            'id_kabupaten' => $request['kabupaten'],
            'id_kecamatan' => $request['kecamatan'],
            'id_desa' => $request['desa'],
            'lat' => $request['lat_perusahaan'],
            'long' => $request['long_perusahaan'],
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/perusahaan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_industri['path'] = $path;
            $result = $this->industriApi->create_file(json_encode($data_industri));
            File::delete($path);
        } else {
            $result = $this->industriApi->create(json_encode($data_industri));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $industri = $this->industriApi->get_by_id($request['id']);
        $industri = $industri['body']['data'];
        // dd($industri)
        $file = explode('/', $industri['file']);
        $industri['file_edit'] = end($file);
        // dd($industri);
        return response()->json($industri);
    }

    public function change()
    {
        $industri = $this->industriApi->get_by_id(Help::decode($_GET['code']));
        // dd($industri);
        $industri = $industri['body']['data'];
        $provinsi = $this->indonesiaApi->get_provinsi();
        $provinsi = $provinsi['body']['data'];
        $leaflet = $this->envApi->by_jenis_sekolah('maps', session('id_sekolah'));
        $leaflet = $leaflet['body']['data'];
        $user = $this->userApi->perusahaan();
        $user = $user['body']['data'];
        $kategori = $this->katIndustriApi->get_by_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        return view('content.bkk.dashboard.bkk-admin.industri.v_edit_industri')->with([
            'industri' => $industri, 'provinsi' => $provinsi,
            'user' => $user, 'leaflet' => $leaflet, 'kategori' => $kategori
        ]);
    }

    public function update(Request $request)
    {
        // dd($request);
        if (session('role') == 'bkk-admin' || session('role') == 'admin') {
            $data_industri = array(
                'id' => $request['id'],
                'nama' => $request['nama'],
                'id_user' => $request['id_user'],
                'id_sekolah' => session('id_sekolah'),
                'telepon' => $request['telepon'],
                'deskripsi' => $request['deskripsi'],
                'kabupaten' => $request['kabupaten'],
                'id_kategori' => $request['id_kategori'],
                'website' => $request['website'],
                'alamat' => $request['alamat'],
                'id_provinsi' => $request['provinsi'],
                'lat' => $request['lat'],
                'long' => $request['long'],
            );
        } else {
            // dd($request);
            $data_industri = array(
                'id' => $request['id'],
                'nama' => $request['nama'],
                'id_user' => $request['id_user'],
                'id_sekolah' => session('id_sekolah'),
                'telepon' => $request['telepon'],
                'deskripsi' => $request['deskripsi'],
                'id_kategori' => $request['id_kategori'],
                'kabupaten' => $request['kabupaten'],
                'website' => $request['website'],
                'alamat' => $request['alamat'],
                'id_provinsi' => $request['provinsi'],
                'lat' => $request['lat'],
                'long' => $request['long'],
            );
        }

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/perusahaan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_industri['path'] = $path;
            $result = $this->industriApi->create_file(json_encode($data_industri));
            File::delete($path);
        } else {
            $result = $this->industriApi->create(json_encode($data_industri));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'success'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'success'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->industriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function detail()  //Dipake di role industri
    {
        // dd(session()->all());
        $industri = $this->industriApi->get_by_user(session('id'));
        $industri = $industri['body']['data'];
        $key_maps = $this->envApi->by_jenis_sekolah('maps', session('id_sekolah'));
        $key_maps = $key_maps['body']['data'];
        return view('content.bkk.dashboard.bkk-perusahaan.v_detail_industri')->with(['industri' => $industri, 'key_maps' => $key_maps]);
    }

    public function update_create(Request $request)
    {
        $data_industri = array(
            'nama' => $request['perusahaan'],
            'id_user' => session('id'),
            'id_sekolah' => session('id_sekolah'),
            'telepon' => $request['telepon'],
            'deskripsi' => $request['deskripsi'],
            'website' => $request['website'],
            'alamat' => $request['alamat'],
            'kabupaten' => $request['kabupaten'],
            'lat' => $request['lat'],
            'long' => $request['long'],
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/perusahaan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_industri['path'] = $path;
            $result = $this->industriApi->update_create_file(json_encode($data_industri));
            File::delete($path);
        } else {
            $result = $this->industriApi->update_create(json_encode($data_industri));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->industriApi->delete_profile($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->industriApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
