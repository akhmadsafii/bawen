<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\DokumenApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use Hashids\Hashids;

class DokumenController extends Controller
{
    private $dokumenApi;
    private $hashids;

    public function __construct()
    {
        $this->dokumenApi = new DokumenApi();
        $this->hashids = new Hashids();
    }

    public function index(Request $request)
    {

        if (session('role') == 'bkk-pelamar') {
            $dokumen = $this->dokumenApi->get_by_login_pelamar();
            // dd($dokumen);
            $result = $dokumen['body']['data'];
        } else {
            // $dokumen =
        }
        // dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Hapus</button>';
                    return $button;
                });
            $table->editColumn('download', function ($aksi) {
                if ($aksi['file'] == null) {
                    $download = '-';
                } else {
                    $id_encode = $this->hashids->encode($aksi['id']);
                    $download = '<a href="/program/bursa_kerja/dokumen/view/' . $id_encode . '" target="_blank" class="btn download-' . $aksi['id'] . ' btn-warning btn-xs">Lihat file <i class="fa fa-eye"></i></a>';
                }
                return $download;
            });
            $table->rawColumns(['action', 'download']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Bidang Lowongan Kerja');
        if (session('role') == 'bkk-pelamar') {
            return view('content.bkk.dashboard.' . session('role') . '.v_dokumen')->with([]);
        } else {
            # code...
        }
    }

    public function store(Request $request)
    {
        // dd(session()->all());
        // dd($request);
        $insert_dokumen = array(
            'id_user' => session('id'),
            'jenis' => $request['jenis'],
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/dokumen/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $insert_dokumen['path'] = $path;
            $result = $this->dokumenApi->create_file(json_encode($insert_dokumen));
            File::delete($path);
        } else {
            $result = $this->dokumenApi->create(json_encode($insert_dokumen));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function view_document($id)
    {
        $dokumen = $this->dokumenApi->get_by_id($this->hashids->decode($id)[0]);
        $dokumen = $dokumen['body']['data'];
        $nama_file = explode('.', $dokumen['file']);
        $ext = end($nama_file);
        if ($ext == 'pdf') {
            $content_types = 'application/pdf';
        } elseif ($ext == 'doc') {
            $content_types = 'application/msword';
        } elseif ($ext == 'docx') {
            $content_types = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        } elseif ($ext == 'xls') {
            $content_types = 'application/vnd.ms-excel';
        } elseif ($ext == 'xlsx') {
            $content_types = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        } elseif ($ext == 'txt') {
            $content_types = 'application/octet-stream';
        }
        return response(file_get_contents($dokumen['file']), 200)
            ->header('Content-Type', $content_types);
    }

    public function edit(Request $request)
    {
        $dokumen = $this->dokumenApi->get_by_id($request['id_dokumen']);
        $result = $dokumen['body']['data'];
        // $result[]
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'jenis' => $request->jenis,
            'id_user' => session('id'),
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/dokumen/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->dokumenApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->dokumenApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function resume(Request $request)
    {
        $url_now = url()->previous();
        $url_prev =  session('URL_PWD');
        $exp = explode('/', $url_prev);
        $exp1 = explode('/', $url_now);
        $current_exp = end($exp1);
        $current_exp1 = end($exp);
        if ($current_exp != $current_exp1 && $current_exp != "lowongan") {
            $request->session()->put("URL_PWD", $request->getUri());
        }
        $resume = $this->dokumenApi->get_by_login_pelamar();
        $resume = $resume['body']['data'];
        if ($resume != null) {
            $resume['id_code'] = $this->hashids->encode($resume['id']);
        }
        return view('content.bkk.dashboard.' . session('role') . '.profile.v_resume')->with(['resume' => $resume]);
    }


    public function download($id)
    {
        $post  = $this->dokumenApi->get_by_id(Help::decode($id));
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        $nama = end($nama_file);
        $fileSource = $result['file'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);

    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    public function hard_delete(Request $request)
    {
        $delete = $this->dokumenApi->delete($this->hashids->decode($request['id'])[0]);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
