<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\PengalamanApi;
use App\ApiService\Master\IndonesiaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PengalamanController extends Controller
{
    private $indonesiaApi;
    private $pengalamanApi;

    public function __construct()
    {
        $this->indonesiaApi = new IndonesiaApi();
        $this->pengalamanApi = new PengalamanApi();
    }

    public function index(Request $request)
    {
        if (session("role") == 'bkk-pelamar') {
            // dd(session('id'));
            $provinsi = $this->indonesiaApi->get_provinsi();
            $provinsi = $provinsi['body']['data'];
            // dd($provinsi);
            $pengalamans = $this->pengalamanApi->get_by_user(session('id'));
            // dd($pengalamans);
            $pengalamans = $pengalamans['body']['data'];
            $pengalaman = [];
            foreach($pengalamans as $pgl){
                $date1 = $pgl['tgl_mulai'];
                $date2 = $pgl['tgl_akhir'];
                $ts1 = strtotime($date1);
                $ts2 = strtotime($date2);
                $year1 = date('Y', $ts1);
                $year2 = date('Y', $ts2);
                $month1 = date('m', $ts1);
                $month2 = date('m', $ts2);

                $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                $pengalaman[] = array(
                    'id' => $pgl['id'],
                    'id_user' => $pgl['id_user'],
                    'nama' => $pgl['nama'],
                    'industri' => $pgl['industri'],
                    'alamat' => $pgl['alamat'],
                    'tgl_mulai' => $pgl['tgl_mulai'],
                    'tgl_akhir' => $pgl['tgl_akhir'],
                    'jumlah_bulan' => $diff,
                    'deskripsi' => $pgl['deskripsi'],
                    'provinsi' => $pgl['provinsi'],
                    'status' => $pgl['status'],
                );
                // dd($pengalaman);
            }

            return view('content.bkk.dashboard.' . session('role') . '.profile.v_pengalaman')->with(['pengalaman' => $pengalaman, 'provinsi' => $provinsi]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        if (session('role') == 'bkk-pelamar') {
            $data_insert = array(
                'id_user' => session('id'),
                'nama' => $request['nama'],
                'industri' => $request['industri'],
                'alamat' => $request['alamat'],
                'deskripsi' => $request['deskripsi'],
                'provinsi' => $request['provinsi'],
                'id_sekolah' => session('id_sekolah'),
                'tgl_mulai' => $request['tahun_dari']."-".$request['bulan_dari']."-01",
                'tgl_akhir' => $request['tahun_sampai']."-".$request['bulan_sampai']."-01",
            );
            // dd($data_insert);
            $result = $this->pengalamanApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $data_update = array(
                'id' => $request['id'],
                'id_user' => $request['id_user'],
                'nama' => $request['nama'],
                'industri' => $request['industri'],
                'alamat' => $request['alamat'],
                'deskripsi' => $request['deskripsi'],
                'provinsi' => $request['provinsi'],
                'id_sekolah' => session('id_sekolah'),
                'tgl_mulai' => $request['tahun_dari']."-".$request['bulan_dari']."-01",
                'tgl_akhir' => $request['tahun_sampai']."-".$request['bulan_sampai']."-01",
            );
            $result = $this->pengalamanApi->update_info(json_encode($data_update));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $pengalaman = $this->pengalamanApi->get_by_id($request['id']);
        $pengalaman = $pengalaman['body']['data'];
        $pengalaman['bulan_mulai'] = date('m', strtotime($pengalaman['tgl_mulai']));
        $pengalaman['tahun_mulai'] = date('Y', strtotime($pengalaman['tgl_mulai']));
        $pengalaman['bulan_akhir'] = date('m', strtotime($pengalaman['tgl_akhir']));
        $pengalaman['tahun_akhir'] = date('Y', strtotime($pengalaman['tgl_akhir']));
        return response()->json($pengalaman);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->pengalamanApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
