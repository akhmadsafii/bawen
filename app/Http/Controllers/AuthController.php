<?php

namespace App\Http\Controllers;

use App\Api;
use App\ApiService\Alumni\AlumniApi;
use App\ApiService\Alumni\JurusanApi;
use App\ApiService\AuthApi;
use App\ApiService\BKK\IndustriApi;
use App\ApiService\BKK\UserApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\ProgramApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\User\ProfileApi;
use Session;
use App\ApiService\User\SuperadminApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use App\ApiService\Ppdb\Akun\PesertaPpdbApi;
use App\ApiService\SumbanganSpp\SppApiservice;
use App\ApiService\Simpen\SimpenApi;
use App\ApiService\PenilaianGuru\ApiPkgservice;

class AuthController extends Controller
{
    private $api;
    private $programApi;
    private $roomApi;
    private $profileApi;
    private $sekolahApi;
    private $kelassiswaApi;
    private $bkkUserApi;
    private $alumniJurusanApi;
    private $alumniApi;
    private $industriApi;
    private $AdminPpdbApi;
    private $PesertaPpdbApi;
    protected $SppApiservice;
    protected $SimpenApi;
    protected $ApiPkgservice;

    public function __construct()
    {
        $this->api = new Api();
        $this->programApi = new ProgramApi();
        $this->sekolahApi = new SekolahApi();
        $this->apiUser = new SuperadminApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->profileApi = new ProfileApi();
        $this->apiAuth = new AuthApi();
        $this->roomApi = new RoomApi();
        $this->bkkUserApi = new UserApi();
        $this->industriApi = new IndustriApi();
        $this->alumniApi = new AlumniApi();
        $this->alumniJurusanApi = new JurusanApi();
        $this->urlApi = env("API_URL");
        $this->AdminPpdbApi = new AdminPpdbApi();
        $this->PesertaPpdbApi = new PesertaPpdbApi();
        $this->SppApiservice  = new SppApiservice();
        $this->SimpenApi      = new SimpenApi();
        $this->ApiPkgservice  = new ApiPkgservice();
    }


    public function login()
    {
        session()->put('title', 'Login');
        $program = $this->programApi->get_by_slug(session('config'));
        if ($program['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, silahkan memilih program terlebih dahulu",
                'icon' => 'error'
            );
            return redirect()->route('home')->with('message', $pesan);
        }
        // dd($program);
        $program = $program['body']['data'];
        //dd($program);
        $program_sekolah = $this->programApi->get_all();
        //dd($program_sekolah);
        $program_sekolah = $program_sekolah['body']['data'];
        return view('auth/login-revisi')->with(['program' => $program, 'program_sekolah' => $program_sekolah]);
    }

    public function login_adm()
    {
        return view('auth/login_admin')->with([]);
    }

    public function superadmin()
    {
        return view('auth/login-superadmin')->with([]);
    }

    public function register()
    {
        $program = $this->programApi->get_by_slug(session('config'));

        if ($program['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, silahkan memilih program terlebih dahulu",
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->route('home')->with('message', $pesan);
        }
        $program = $program['body']['data'];
        if (session('config') == "alumni") {
            $jurusan = $this->alumniJurusanApi->id_sekolah(session('id_sekolah'));
            $jurusan = $jurusan['body']['data'];
        } else {
            $jurusan = [];
        }

        if (session('config') == "ppdb") { }

        $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
        $sekolah = $sekolah['body']['data'];
        return view('auth/v_register')->with(['program' => $program, 'sekolah' => $sekolah, 'jurusan' => $jurusan]);
    }

    public function login_admin()
    {
        return view('auth/login_admin');
    }


    public function verifyLogin(Request $request)
    {
        // dd($request);
        $username = $request->username;
        $password = $request->password;
        $role = $request->role;
        $result = $this->api->login($username, $password, $role);
        // dd($result);
        if (isset($result['data']['access_token'])) {
            session()->put('token', $result['data']['access_token']);
            session()->put('role', $role);
            if (!empty($result['data']['id_sekolah'])) {
                if ($role == 'user-mutu') {
                    session()->put('role-user', $result['data']['role']);
                } elseif ($role == 'admin-sarpras') {
                    session()->put('role-sarana', 'admin');
                } elseif ($role == 'user-sarpras') {
                    session()->put('role-sarana', 'user');
                }

                session()->put('id_sekolah', $result['data']['id_sekolah']);
                $sekolah = $this->sekolahApi->get_by_id($result['data']['id_sekolah']);
                $sekolah = $sekolah['body']['data'];
                session()->put('sekolah', $sekolah['nama']);
            }
            session()->put('username', $result['data']['nama']);
            session()->put('status', $result['status']);
            !empty($result['data']['id']) ? session()->put('id', $result['data']['id']) : '';
            !empty($result['data']['sosial']) ? session()->put('sosial', $result['data']['sosial']) : '';
            if (session('config') == 'sistem_pkl' && $role == 'siswa') {
                if (empty($result['data']['id_prakerin_siswa'])) {
                    $message = array(
                        'message' => 'Siswa dengan akun ini belum di set jadi anggota prakerin',
                        'icon' => 'error',
                        'status' => 'gagal'
                    );
                    return redirect()->back()->with(['message' => $message]);
                }
            }
            !empty($result['data']['id_prakerin_siswa']) ? session()->put('id_prakerin_siswa', $result['data']['id_prakerin_siswa']) : '';
            !empty($result['data']['file']) ? session()->put('avatar', $result['data']['file']) : '';
            !empty($result['data']['id_jurusan']) ? session()->put('id_jurusan', $result['data']['id_jurusan']) : '';
            !empty($result['data']['id_industri']) ? session()->put('id_industri', $result['data']['id_industri']) : '';
            !empty($result['data']['tahun']) ? session()->put('tahun', $result['data']['tahun']) : '';
            !empty($result['data']['id_ta_aktif']) ? session()->put('id_tahun_ajar', $result['data']['id_ta_aktif']) : '';
            !empty($result['data']['id_industri']) ? session()->put('id_industri', $result['data']['id_industri']) : '';
            !empty($result['data']['id_rombel']) ? session()->put('id_rombel', $result['data']['id_rombel']) : '';
            if ($role == "siswa") {
                $id_kelas_siswa = $this->kelassiswaApi->get_by_id_siswa($result['data']['id']);
                session()->put('id_rombel', $result['data']['id_rombel']);
                session()->put('id_kelas_siswa', $id_kelas_siswa['body']['data'][0]['id']);
            } elseif ($role == "ortu") {
                session()->put('id_kelas_siswa', $result['data']['id_kelas_siswa_terakhir']);
            } elseif ($role == "superadmin" || $role == "admin") {
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                return redirect()->route('dashboard-master')->with(['message' => $message]);
            } elseif ($role == "admin-ppdb") {
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                $profil_admin = $this->AdminPpdbApi->get_info_profil_admin();
                if(!empty($profil_admin['body']['data'])){
                    $profil_data  = $profil_admin['body']['data'];
                    session()->put('avatar', $profil_data['file']);
                }
                return redirect()->route('dashboard-ppdb')->with(['message' => $message]);
            } elseif ($role == "peserta-ppdb") {
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                $profil_user = $this->PesertaPpdbApi->get_info_profil_user();
                $profil_data  = $profil_user['body']['data'];
                session()->put('avatar', $profil_data['file']);
                return redirect()->route('user-ppdb')->with(['message' => $message]);
            } elseif ($role == "user-perpus") {
                session()->put('role-perpus', 'user');
            } elseif ($role == "admin-perpus") {
                session()->put('role-perpus', 'admin');
            }

            if ((session('config') == 'spp' && $role == 'tata-usaha')
                || (session('config') == 'spp' && $role == 'siswa'
                    || (session('config') == 'spp' && $role == 'ortu'))
            ) {
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );

                $profil_admin = $this->SppApiservice->get_info_profil();
                $profil_data  = $profil_admin['body']['data'];
                session()->put('avatar',  $profil_data['file']);
                return redirect()->route('sumbangan-spp')->with(['message' => $message]);
            }

            if ((session('config') == 'penilaian_guru' && $role == 'admin-pkg')
                || (session('config') == 'penilaian_guru' && $role == 'penilai-pkg'
                    || (session('config') == 'penilaian_guru' && $role == 'guru-pkg'))
            ){
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                $profil_datax = $this->ApiPkgservice->get_info_profil();
                $profil_data  = $profil_datax['body']['data'];
                session()->put('avatar',  $profil_data['file']);
                return redirect()->route('dashboard-pkg')->with(['message' => $message]);
            }

            if ((session('config') == 'kelulusan' && $role == 'admin-simpen')
                || (session('config') == 'kelulusan' && $role == 'siswa-simpen')
            ){
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                $profil_admin = $this->SimpenApi->get_info_profil();
                $profil_data  = $profil_admin['body']['data'];
                session()->put('avatar',  $profil_data['file']);
                session()->put('login',true);
                if(!empty($result['data']['tahun']) &&  !empty($result['data']['tahun_ajaran'])){
                    session()->put('tahun',$result['data']['tahun']);
                    session()->put('tahun_ajaran',$result['data']['tahun_ajaran']);
                }
                return redirect()->route('dashboard-simpen')->with(['message' => $message]);
            }

                // TODO: Pastika perlu session khusus untuk user `tata-usaha`?
                // elseif ($role == "tata-usaha") { /.../ }

                session()->put('role', $role);
            $pesan = array(
                'message' => $result['message'],
                'status' => 'berhasil',
                'icon' => 'success',
            );
            return redirect()->to(session('URL_PWD'))->with(['message' => $pesan]);
        } else {
            if($result == null){
                $message = "Tidak bisa login, ada kesalahan saat pemrosesan";
            }else{
                $message = $result['message'];
            }
            if ($request->role == 'superadmin') {
                return response()->json([
                    'message' => $result['message'],
                    'status' => 'gagal',
                    'icon' => 'error',
                ]);
            } else {
                $pesan = array(
                    'message' => $result['message'],
                    'icon' => 'error',
                    'status' => 'gagal'
                );
                return redirect()->back()->with(['message' => $pesan]);
            }
        }
    }



    public function verifyregister(Request $request)
    {
        if ($request['nama_program'] == 'bursa_kerja') {
            $data_register = array(
                'nama' => $request['nama'],
                'asal_sekolah' => $request['asal_sekolah'],
                'email' => $request['email'],
                'first_password' => $request['password'],
                'jenkel' => $request['jenkel'],
                'pendidikan_akhir' => $request['pendidikan_akhir'],
                'role' => $request['role'],
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->bkkUserApi->create(json_encode($data_register));
            // dd($result);
            if ($result['code'] != 200) {
                $pesan = array(
                    'message' => $result['body']['message'],
                    'icon' => 'error',
                    'status' => 'gagal',
                );
                return redirect()->route('auth.register')->with('message', $pesan);
            } else {
                if ($request['role'] == 'industri') {
                    $data_industri = array(
                        'id_user' => $result['body']['data']['id'],
                        'nama' => $request['industri'],
                        'id_sekolah' => session('id_sekolah'),
                    );
                    $add_industri = $this->industriApi->create(json_encode($data_industri));
                    // dd($add_industri);
                    if ($add_industri['code'] != 200) {
                        $pesan = array(
                            'message' => $add_industri['body']['message'],
                            'icon' => 'error',
                            'status' => 'gagal',
                        );
                        return redirect()->route('auth.register')->with('message', $pesan);
                    } else {
                        session()->put('id_industri', $add_industri['body']['data']);
                    }
                }
            }

            if ($result['code'] == 200) {
                $pesan = array(
                    'message' => $result['body']['message'],
                    'icon' => 'success',
                    'status' => 'gagal',
                );
            } else {
                $pesan = array(
                    'message' => $result['body']['message'],
                    'icon' => 'error',
                    'status' => 'gagal',
                );
            }
            if ($request['role'] == 'industri') {
                $role = 'bkk-perusahaan';
            } else {
                $role = 'bkk-pelamar';
            }
            $login = $this->api->login($request['email'], $request['password'], $role);
            // dd($login);
            if (isset($login['data']['access_token'])) {
                Session::put('token', $login['data']['access_token']);
                Session::put('role', $role);
                Session::put('id', $result['body']['data']['id']);
                Session::put('nama', $result['body']['data']['nama']);
            };
            return redirect($request->session()->pull("URL_PWD"));
        } else {
            if ($request['cek_pass'] == 'gagal') {
                $pesan = array(
                    'message' => "Password yang anda masukan tidak sama",
                    'status' => 'gagal',
                    'icon' => 'warning'
                );
                return redirect()->back()->with(['message' => $pesan]);
            }
            $data = array(
                'no_induk' => $request['nomor_induk'],
                'id_jurusan' => $request['id_jursuan'],
                'nama' => $request['nama'],
                'email' => $request['email'],
                'jenkel' => $request['jenkel'],
                'tahun_lulus' => $request['tahun_lulus'],
                'tahun_angkatan' => $request['tahun_angkatan'],
                'password' => $request['password'],
                'pendidikan_terakhir' => $request['pendidikan_terakhir'],
                'id_sekolah' => session('id_sekolah'),
            );
            // dd($data);
            $register = $this->alumniApi->register_form(json_encode($data));
            // dd($register);
            if ($register['code'] == 200) {
                $pesan = array(
                    'message' => "Akun berhasil didaftarkan, admin sedang meninjau data yg anda kirimkan",
                    'status' => 'berhasil',
                    'icon' => 'success',
                );
                return redirect()->route('auth.login')->with(['message' => $pesan]);
            } else {
                $pesan = array(
                    'message' => $register['body']['message'],
                    'status' => 'gagal',
                    'icon' => 'warning'
                );
                return redirect()->back()->with(['message' => $pesan]);
            }
        }
    }

    public function login_demo(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $role = $request->role;

        $result = $this->api->login($username, $password, $role);
        // dd($result);
        if (isset($result['data']['access_token'])) {
            Session::put('token', $result['data']['access_token']);
            Session::put('role', $role);
            if (!empty($result['data']['id_sekolah'])) {
                Session::put('id_sekolah', $result['data']['id_sekolah']);
                $sekolah = $this->sekolahApi->get_by_id($result['data']['id_sekolah']);
                $sekolah = $sekolah['body']['data'];
                Session::put('sekolah', $sekolah['nama']);
            }
            Session::put('username', $result['data']['nama']);
            Session::put('status', $result['status']);
            !empty($result['data']['id']) ? Session::put('id', $result['data']['id']) : '';
            !empty($result['data']['id_industri']) ? Session::put('id_industri', $result['data']['id_industri']) : '';
            !empty($result['data']['tahun']) ? Session::put('tahun', $result['data']['tahun']) : '';
            !empty($result['data']['id_ta_aktif']) ? Session::put('id_tahun_ajar', $result['data']['id_ta_aktif']) : '';
            !empty($result['data']['id_industri']) ? Session::put('id_industri', $result['data']['id_industri']) : '';
            !empty($result['data']['id_rombel']) ? Session::put('id_rombel', $result['data']['id_rombel']) : '';
            if ($role == "siswa") {
                $id_kelas_siswa = $this->kelassiswaApi->get_by_id_siswa($result['data']['id']);
                Session::put('id_rombel', $result['data']['id_rombel']);
                Session::put('id_kelas_siswa', $id_kelas_siswa['body']['data'][0]['id']);
            } elseif ($role == "ortu") {
                Session::put('id_kelas_siswa', $result['data']['id_kelas_siswa_terakhir']);
            }
            Session::put('role', $role);
            Session::put('demo', true);
            if (session('role') == 'admin') {
                return redirect('/admin/master/dashboard');
            } else {
                return redirect('/demo');
            }
        } else {
            return Redirect::back()->withErrors(['msg', $result['message']]);
        }
    }

    public function logout(Request $request)
    {
        if (session("role") == "guru" && $request->segment(2) == "e_learning") {
            $room = $this->roomApi->get_guru(session('id_tahun_ajar'));
            $room = $room['body']['data'];
            // dd($room);
            foreach ($room as $key) {
                $data = array(
                    "id" => $key['id'],
                    "id_rombel" => $key['id_rombel'],
                    "id_kelas" => $key['id_kelas'],
                    "id_mapel" => $key['id_mapel'],
                    "id_guru" => session('id'),
                    "id_sekolah" => session('id_sekolah'),
                    "status" => 2
                );
                $update = $this->roomApi->update_info(json_encode($data));
            }
        }
        // dd("ping");
        $result = $this->api->logout();
        Session::flush();
        if (session('demo')) {
            $url = request()->url();
            $ex = explode('/', $url);
            $domain = $ex[2];
            $domain = preg_replace("/www./", "", $domain);
            $data = array(
                'domain' => $domain
            );
            $sekolah = $this->sekolahApi->search_by_domain(json_encode($data));
            $sekolah = $sekolah['body']['data'];
            Session::put('id_sekolah', $sekolah['id']);
        }
        return redirect()->route('home');
    }

    public function changePassword()
    {
        return view('auth/change_password');
    }

    public function resetPassword()
    {
        return view('auth/reset_password');
    }

    public function reset_password()
    {
        return view('auth/reset_password');
    }

    public function dashboard()
    {
        if (!session('token')) {
            return redirect(route('auth.login'));
        }
        return view('content/dashboard');
    }

    public function checkEmail(Request $request)
    {
        $data = array(
            "email" => $_POST['email'],
        );
        if (session('role') == "admin") {
            $update = $this->profileApi->reset_password_email(json_encode($data));
        } else {
            $update = $this->profileApi->reset_password_alumni(json_encode($data));
        }
        // dd($update);

        if ($update['code'] == 200) {
            $message = array(
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            );
            return redirect()->route('auth.login')->with(['message' => $message]);
        } else {
            $message = array(
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
            return redirect()->back()->with(['message' => $message]);
        }
    }

    public function saveNewPassword()
    {
        $id = Session::get('user_id');
        $url = array('url' => 'dashboard');

        // dd($test);
        $data = array(
            'new_password' => $_POST['new_password'],
            'confirm_new_password' => $_POST['confirm_new_password'],
        );
        $result = $this->api->savePassword($data, $id);
        // dd($result);
        if (isset($result['errors'])) {
            $error = $result['errors'];
            return redirect(route('change_password'))->with('error', $error);
        }
        // dd($url);
        return redirect($url['url']);
    }

    public function error403()
    {
        return view('content/403');
    }
}
