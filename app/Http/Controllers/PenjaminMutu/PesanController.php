<?php

namespace App\Http\Controllers\PenjaminMutu;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\PenjaminMutu\ProfileUser\UserMutuApi;
use App\ApiService\PenjaminMutu\PesanApi;

class PesanController extends Controller
{
	public $PesanApi;
    public $UserApi;


    public function __construct()
    {
        $this->UserApi = new UserMutuApi();
        $this->PesanApi = new PesanApi();
    }


    public function inbox(Request $request){
    	
    	$pesanMasuk = $this->PesanApi->get_inbox();
    	$dataUser = $this->UserApi->get_by_sekolah();

    	
    	$pesanMasuk = $pesanMasuk['body']['data'];
        $dataUser = $dataUser['body']['data'];

        $dataPesan = $pesanMasuk;
        //dd($pesanMasuk);

        if($request->ajax()){
        	$table = datatables()->of($dataPesan);
            $table->addIndexColumn();

            $table->editColumn('subjects', function ($row) {
                
                return '<a onclick="Detail('.$row['id'].')">'.$row['subject'].'</a>';

            });

            $table->rawColumns(['subjects']);

            return $table->make(true);
        }

         return view('content.penjamin_mutu.pesan.inbox_pesan',compact('dataUser'));

    }

    public function send(Request $request){
    	$pesanKeluar = $this->PesanApi->get_send();
    	
    	$dataUser = $this->UserApi->get_all();

    	$pesanKeluar = $pesanKeluar['body']['data'];
    	
        $dataUser = $dataUser['body']['data'];

        $dataPesan = $pesanKeluar;

        if($request->ajax()){
        	$table = datatables()->of($dataPesan);
            $table->addIndexColumn();

            $table->editColumn('subjects', function ($row) {
                return '<a onclick="Detail('.$row['id'].')">'.$row['subject'].'</a>';
            });
            $table->rawColumns(['subjects']);


            return $table->make(true);
        }

         return view('content.penjamin_mutu.pesan.send_pesan',compact('dataUser'));

    }

    public function create(Request $request){

        $data_pesan = array(
            'email' => $request->email,
            'subject' => $request->subject,
            'isi' => $request->isi,
        );
        //dd($data_pesan);

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/mutu/pesan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data_pesan['path'] = $path;
            //dd($data_pesan);
            $result = $this->PesanApi->create_file(json_encode($data_pesan));
            File::delete($path);
            //dd($result);
            
        }else{
        	$result = $this->PesanApi->create(json_encode($data_pesan));
        }
        dd($result);

        $result = $result['body']['message'];
        Session::flash('message', $result);
        
        return redirect()->back();
    }

    public function detail(Request $request){
    	$id = $request->id;
    	$data = $this->PesanApi->get_detail($id);
    	return $data;
    }

    public function delete(Request $request){
        $id = $request->id;
        $data = $this->PesanApi->delete($id);
        
        if ($data['code'] == 200) {
            return response()->json([
                'success' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

}
