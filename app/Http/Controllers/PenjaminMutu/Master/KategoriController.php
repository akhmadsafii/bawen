<?php

namespace App\Http\Controllers\PenjaminMutu\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\PenjaminMutu\DataDokumen\KategoriApi;

class KategoriController extends Controller
{
    public $KategoriApi;

    public function __construct()
    {
        $this->KategoriApi = new KategoriApi("api/data/mutu/kategori");
    }

    public function index(Request $request){
        $dataKategori = $this->KategoriApi->get_by_sekolah();
        //dd($dataKategori);
        $dataKategori = $dataKategori['body']['data'];
        
        if ($request->ajax()) {
            $table = datatables()->of($dataKategori);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="ShowModal('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Delete('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            
            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.penjamin_mutu.master.kategori_dokumen')->with([]);
    }

    public function create(Request $request){

        $id_jenis = $request->id_jenis;
        $id_sekolah = session('id_sekolah');
        $nama = $request->nama;

        $data = array(
            "id_sekolah" => 1,
            "id_jenis" => $id_jenis,
            "nama" => $nama,
        );

        $Kategori = $this->KategoriApi->create(json_encode($data));

        if ($Kategori['code'] == 200) {
            return response()->json([
                'message' => $Kategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $Kategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function show($id)
    {
        //
    }

    
    public function edit(Request $request)
    {
        $id = $request->id_item;

        $keterangan = $request->keterangan;
        $id_sekolah = session('id_sekolah');
        $nama = $request->nama;

        $data = array(
            "id_sekolah" => session('id_sekolah'),
            "keterangan" => $keterangan,
            "nama" => $nama,
        );

        //dd($id);
        $Kategori = $this->KategoriApi->update($id,json_encode($data));
        
        if ($Kategori['code'] == 200) {
            return response()->json([
                'message' => $Kategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $Kategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
