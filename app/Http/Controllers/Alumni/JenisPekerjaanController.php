<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\JenisPekerjaanApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class JenisPekerjaanController extends Controller
{
    private $jenisPekerjaanApi;

    public function __construct()
    {
        $this->jenisPekerjaanApi = new JenisPekerjaanApi();
    }

    public function index_pagination(Request $request)
    {
        session()->put('title', 'Jenis pekerjaan');
        $routes = "alumni-jenis_pekerjaan";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $sort = (isset($_GET["sort"])) ? $_GET["sort"] : "ASC";
        $perPage = (isset($_GET["per_page"])) ? $_GET["per_page"] : "10";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->jenisPekerjaanApi->Pagination_sekolah($search, $status, $sort, $perPage, $page);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        $data = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pages = Utils::getPerPage();
        $sorts = Utils::getSort();
        $statuses = Utils::getStatus();
        $pagination = Utils::createLinksMetronic($meta, $routes, $search, $status, $sort);
        $template = session('template');
        return view('content.alumni.jenis_pekerjaan.v_jenis_pekerjaan_pagination', compact('template', 'data', 'pagination', 'pages', 'sorts', 'statuses', 'routes', 'search', 'status', 'sort', 'perPage', 'page'));
    }

    public function index(Request $request)
    {
        // dd($request);
        session()->put('title', 'Jenis Pekerjaan');
        $jenis =  $this->jenisPekerjaanApi->sekolah(session('id_sekolah'));
        // dd($jenis);
        $result = $jenis['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('status', function ($row) {
                $checked = '';
                if ($row['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                <input type="checkbox"
                    ' . $checked . '
                    class="check_jenis"
                    data-id="' . $row['id'] . '">
                <span class="slider round"></span>
            </label>';
            });
            $table->rawColumns(['action', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.alumni.jenis_pekerjaan.v_jenis_pekerjaan')->with(['template' => session('tamplate')]);
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_fakultas' => $request['id_fakultas'],
                'id_sekolah' => session('id_sekolah')

            );
            $result = $this->jenisPekerjaanApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->jenisPekerjaanApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'id_fakultas' => $request->id_fakultas,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jenisPekerjaanApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->jenisPekerjaanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->jenisPekerjaanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jenis_pekerjaan = $this->jenisPekerjaanApi->all_trash();
        // dd($jenis_pekerjaan);
        $result = $jenis_pekerjaan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->jenisPekerjaanApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jenisPekerjaanApi->upload_excel(json_encode($data));
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update_status = $this->jenisPekerjaanApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
