<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\FakultasApi;
use App\ApiService\Alumni\JurusanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Hashids\Hashids;
use Illuminate\Http\Request;

class JurusanController extends Controller
{
    private $fakultasApi;
    private $jurusanApi;
    private $hash;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->fakultasApi = new FakultasApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Jurusan Alumni');
        $routes = "alumni-fakultas";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $sort = (isset($_GET["sort"])) ? $_GET["sort"] : "ASC";
        $perPage = (isset($_GET["per_page"])) ? $_GET["per_page"] : "10";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->jurusanApi->sekolah($search, $status, $sort, $perPage, $page);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        $data = $result['body']['data'];
        $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
        $fakultas = $fakultas['body']['data'];
        $meta = $result['body']['meta'];
        $pages = Utils::getPerPage();
        $sorts = Utils::getSort();
        $statuses = Utils::getStatus();
        $pagination = Utils::createLinksMetronic($meta, $routes, $search, $status, $sort);
        $template = session('template');
        return view('content.alumni.jurusan.v_jurusan', compact('fakultas','template', 'data', 'pagination', 'pages', 'sorts', 'statuses', 'routes', 'search', 'status', 'sort', 'perPage', 'page'));
    }

    public function store(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        foreach ($request['nama'] as $nama) {
            $data =array(
                    'nama' => $nama,
                    'id_fakultas' => $request['id_fakultas'],
                    'id_sekolah' => $id_sekolah,
                    'status' => $request['status']

                );
            $result = $this->jurusanApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $jurusan = $this->data_jurusan_fakultas($request['id_fakultas']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jurusan' => $jurusan
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->jurusanApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'id_fakultas' => $request->id_fakultas,
                'id_sekolah' => session('id_sekolah'),
                'status' => $request['status']
            ];

        $result = $this->jurusanApi->update(json_encode($data));
        if ($result['code'] == 200) {
            $jurusan = $this->data_jurusan_fakultas($request['id_fakultas']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jurusan' => $jurusan
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->jurusanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $jurusan = $this->data_jurusan_fakultas($request['id_fakultas']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'jurusan' => $jurusan
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->jurusanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        // dd($jurusan);
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->jurusanApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update_status = $this->jurusanApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function by_fakultas(Request $request)
    {
        $jurusan = $this->jurusanApi->get_by_fakultas(Help::decode($request['id_fakultas']));
        $jurusan = $jurusan['body']['data'];
        return response()->json($jurusan);
    }



    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jurusanApi->upload_excel(json_encode($data));
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_jurusan_fakultas($id_fakultas){
        $jurusan = $this->jurusanApi->get_by_fakultas($id_fakultas);
        $jurusan = $jurusan['body']['data'];
        $html = '';
        if (!empty($jurusan)) {
            $no = 1;
            foreach($jurusan as $jr){
                $checked = '';
                if ($jr['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td>'.$no++.'</td>
                <td>'.$jr['nama'].'</td>
                <td>
                    <label class="switch">
                        <input type="checkbox" '.$checked.'  class="jurusan_check" data-id="'.$jr['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td>
                    <a href="javascript:void(0)" data-id="'.$jr['id'].'" class="editJurusan btn btn-info btn-sm" data-fakultas="'.$id_fakultas.'"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="'.$jr['id'].'" data-fakultas="'.$id_fakultas.'" class="btn btn-danger btn-sm deleteJurusan"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data tidak tersedia</td></tr>';
        }
        return $html;
    }
}
