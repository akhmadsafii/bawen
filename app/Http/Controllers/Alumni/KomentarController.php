<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\BlogKomentarApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class KomentarController extends Controller
{
    private $komentarApi;
    private $hash;

    public function __construct()
    {
        $this->komentarApi = new BlogKomentarApi();
        $this->hash = new Hashids();
    }

    public function store(Request $request){
        // dd($request);
        $data = array(
            'id_blog' => $request['id_blog'],
            'komentar' => $request['komentar']        
        );
       $result = $this->komentarApi->create(json_encode($data));
       if ($result['code'] == 200) {
        return response()->json([
            'message' => $result['body']['message'],
            'icon'  => 'success',
            'status'  => 'berhasil'
        ]);
    } else {
        return response()->json([
            'message' => $result['body']['message'],
            'icon'  => 'error',
            'status'  => 'gagal'
        ]);
    }
    }

    public function detail($id)
    {
        $blog = $this->blogApi->get_by_id($this->hash->decode($id)[0]);
        // dd($blog);
        $blog = $blog['body']['data'];
        return view('content.alumni.blog.v_detail_blog')->with(['template' => session('template'), 'blog' => $blog]);
    }
}
