<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\JenisPekerjaanApi;
use App\ApiService\Alumni\PekerjaanAlumniApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PekerjaanAlumniController extends Controller
{
    private $pekerjaanApi;
    private $jenisApi;

    public function __construct()
    {
        $this->pekerjaanApi = new PekerjaanAlumniApi();
        $this->jenisApi = new JenisPekerjaanApi();
    }

    public function tracking(Request $request)
    {
        // dd("tracking");
        // dd(session('template'));
        session()->put('title', 'Tracking Pekerjaan Alumni');
        $jenis = $this->jenisApi->sekolah(session('id_sekolah'));
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        if ($_GET['by'] == 'all') {
            $pekerjaan = $this->pekerjaanApi->id_sekolah(session('id_sekolah'));
        } else {
            $pekerjaan = $this->pekerjaanApi->by_jenis(Help::decode($_GET['key']));
        }
        $result = $pekerjaan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('alumni', function ($data) {
                    return '<a href="' . route('detail-alumni_alumni', ['method' => 'pendidikan', 'key' => Help::encode($data['id_alumni']), 'name' => str_slug($data['alumni'])]) . '" class="media">
                        <span class="d-flex user--online thumb-xs">
                            <img src="' . $data['file'] . '" class="rounded-circle" alt="">
                        </span>
                        <span class="media-body">
                            <span class="media-heading">' . $data['alumni'] . '</span> <br>
                            <small>Telepon : ' . $data['telepon'] . '</small>
                        </span>
                    </a>';
                });
            $table->rawColumns(['alumni']);
            $table->addIndexColumn();

            return $table->make(true);
        }

        return view('content.alumni.pekerjaan_alumni.v_list_tracking')->with(['template' => session('template'), 'jenis' => $jenis]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_jenis_pekerjaan' => $request['jenis_pekerjaan'],
            'posisi' => $request['posisi'],
            'industri' => $request['industri'],
            'keterangan' => $request['keterangan'],
            'id_alumni' => session('id'),
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->pekerjaanApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->pekerjaanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }
}
