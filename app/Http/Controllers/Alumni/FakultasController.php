<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\FakultasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class FakultasController extends Controller
{
    private $fakultasApi;

    public function __construct()
    {
        $this->fakultasApi = new FakultasApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Fakultas & Jurusan');
        $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
        // dd($fakultas);
        $fakultas = $fakultas['body']['data'];
        $template = session('template');
        return view('content.alumni.fakultas.v_fakultas', compact('template', 'fakultas'));
    }

    public function store(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        foreach ($request['nama'] as $nama) {
            $data =array(
                    'nama' => $nama,
                    'id_sekolah' => $id_sekolah,
                    'status' => $request['status']
                );
            $result = $this->fakultasApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $fakultas = $this->data_fakultas();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'fakultas' => $fakultas
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->fakultasApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'id_sekolah' => $id_sekolah,
                'status' => $request->status
            ];

        $result = $this->fakultasApi->update(json_encode($data));
        if ($result['code'] == 200) {
            $fakultas = $this->data_fakultas();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'fakultas' => $fakultas
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->fakultasApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $fakultas = $this->data_fakultas();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'fakultas' => $fakultas
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->fakultasApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->fakultasApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $fakultas = $this->fakultasApi->all_trash();
        // dd($fakultas);
        $result = $fakultas['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->fakultasApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->fakultasApi->upload_excel(json_encode($data));
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_fakultas()
    {
        $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
        $fakultas = $fakultas['body']['data'];
        $html = '';
        if (!empty($fakultas)) {
            $no = 1;
            foreach ($fakultas as $fk) {
                $check = '';
                if ($fk['status'] == 1) {
                    $check = 'checked';
                }

                $html .= '<tr>
                <td>'.$no++.'</td>
                <td>'.$fk['nama'].'</td>
                <td>
                    <label class="switch">
                        <input type="checkbox" '.$check.' class="fakultas_check" data-id="'.$fk['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td>
                    <button data-toggle="collapse" data-target="#jurusan'.$fk['id'].'"
                        class="btn btn-sm btn-purple accordion-toggle"><i
                            class="fas fa-wrench"></i>
                    </button>
                    <a href="javascript:void(0)" data-id="'.$fk['id'].'"
                        class="edit btn btn-info btn-sm edit"><i
                            class="fas fa-pencil-alt"></i></a>
                    <button type="button" name="delete" data-id="'.$fk['id'].'"
                        class="btn btn-danger btn-sm delete"><i
                            class="fas fa-trash-alt"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="jurusan'.$fk['id'].'">
                        <button class="btn btn-purple btn-sm my-3 pull-right"
                            onclick="addJurusan('.$fk['id'].')"><i
                                class="fas fa-plus-circle"></i>
                            Tambah Jurusan</button>
                        <table class="table table-striped">
                            <thead>
                                <tr class="bg-purple text-white">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataJurusan'.$fk['id'].'">';
                if (!empty($fk['jurusan'])) {
                    $nomer = 1;
                    foreach ($fk['jurusan'] as $jr) {
                        $check_jurusan = '';
                        if ($jr['status'] == 1) {
                            $check_jurusan = 'checked';
                        }
                        $html .= '<tr>
                                   <td>'.$nomer++.'</td>
                                   <td>'.$jr['nama'].'</td>
                                   <td>
                                       <label class="switch">
                                           <input type="checkbox" '.$check_jurusan.' status="off" class="toggle"
                                               menu="2" role="1">
                                           <span class="slider round"></span>
                                       </label>
                                   </td>
                                   <td>
                                       <a href="javascript:void(0)"
                                           data-id="'.$jr['id'].'"
                                           class="editJurusan btn btn-info btn-sm" data-fakultas="'.$fk['id'].'"><i
                                               class="fas fa-pencil-alt"></i></a>
                                       <a href="javascript:void(0)"
                                           data-id="'.$jr['id'].'" data-fakultas="'.$fk['id'].'"
                                           class="btn btn-danger btn-sm deleteJurusan"><i
                                               class="fas fa-trash-alt"></i></a>
                                   </td>
                               </tr>';
                    }
                } else {
                    $html .= '<tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }
}
