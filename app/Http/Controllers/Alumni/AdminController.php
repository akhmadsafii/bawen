<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AdminApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AdminController extends Controller
{
    private $adminApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Admin');
        $routes = "alumni-admin";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->adminApi->sekolah($search, $page);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        // dd($result);
        $admin = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $template = session('template');
        if(session('role') == 'admin'){
            $template = 'default';
        }
        return view('content.alumni.admin.v_admin', compact('template', 'admin', 'pagination', 'routes', 'search',  'page'));
    }

    public function edit(Request $request)
    {
        $admin = $this->adminApi->get_by_id($request['id']);
        // dd($admin);
        $admin = $admin['body']['data'];
        return response()->json($admin);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'username' => $request['username'],
            'nama' => $request['nama'],
            'jenkel' => $request['jenkel'],
            'email' => $request['email'],
            'first_password' => $request['password'],
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'username' => $request['username'],
            'nama' => $request['nama'],
            'jenkel' => $request['jenkel'],
            'email' => $request['email'],
            'id_sekolah' => session('id_sekolah'),
        );
        if ($request->password != null || $request->password != '' || !empty($request->password)) {
            $data['first_password'] = $request['password'];
        }
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->adminApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->adminApi->update(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update_status = $this->adminApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
