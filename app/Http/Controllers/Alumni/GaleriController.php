<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\GaleriApi;
use App\ApiService\Alumni\KatGaleriApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class GaleriController extends Controller
{
    private $galeriApi;
    private $katGaleriApi;

    public function __construct()
    {
        $this->galeriApi = new GaleriApi();
        $this->katGaleriApi = new KatGaleriApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Kategori Galeri & Galeri');
        $kategori = $this->katGaleriApi->id_sekolah(session('id_sekolah'));
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        // dd($kategori);
        $total = count($this->galeriApi->get_status(json_encode(["status" => 2]))['body']['data']);
        $template = session('template');
        if (session('role') == 'admin-alumni') {
            return view('content.alumni.galeri.v_galeri_admin', compact('template', 'kategori', 'total'));
        } else {
            $galeri_list   = $this->galeriApi->get_my_galeri();
            $galeri        = $galeri_list['body']['data'] ?? array();
            return view('content.alumni.galeri.v_galeri', compact('template', 'kategori', 'galeri'));
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'id_kategori' => $request->id_kategori,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->galeriApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            return response()->json([
                'message' => "Galeri wajib diisi",
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->galeriApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'id_kategori' => $request->id_kategori,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->galeriApi->update_image(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->galeriApi->update(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function detailKat()
    {
        // dd("tes");
        $galeri = $this->galeriApi->get_kategori(Help::decode($_GET['key']));
        // dd($galeri);
        $galeri = $galeri['body']['data'];
        $kategori = $this->katGaleriApi->get_by_id(Help::decode($_GET['key']));
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        return view('content.alumni.galeri.v_detail_galeri')->with(['galeri' => $galeri, 'template' => session('template'), 'kategori' => $kategori]);;
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->galeriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_approve(Request $request)
    {

        $routes = "alumni-galeri_data_approve";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->galeriApi->get_by_parameter(2, $page, $search);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $galeri = $result['body']['data'];
        $meta = $result['body']['meta'];
        session()->put('title', 'Galeri');
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $template = session('template');
        return view('content.alumni.galeri.v_galeri_approve', compact('template', 'galeri', 'search', 'routes', 'pagination'));
    }


    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        // dd($data);
        $update_status = $this->galeriApi->update_status(json_encode($data));
        // dd($update_status);
        if ($update_status['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_galeri_kategori($id_kategori)
    {
        $galeri = $this->galeriApi->get_kategori($id_kategori);
        $galeri = $galeri['body']['data'];
        $html = '';
        if (!empty($galeri)) {
            $no = 1;
            foreach ($galeri as $glr) {
                $check = '';
                if ($glr['status'] == 1) {
                    $check = 'checked';
                }
                $html .= '<tr>
                <td>' . $no++ . '</td>
                <td><img src="' . $glr['file'] . '" alt="" height="50">
                </td>
                <td>
                    <b
                        class="text-dark">' . $glr['nama'] . '</b>
                    <span class="text-dark">by ' . $glr['pembuat'] . '</span>
                    <br>
                    <small class="m-0 text-dark">' . Str::limit($glr['keterangan'], 100, '...') . '</small>
                </td>
                <td>
                    <label class="switch">
                        <input type="checkbox" ' . $check . ' class="galeri_check" data-id="' . $glr['id'] . '">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td>' . Help::getTanggalLengkap($glr['created_at']) . '<br><small>' . $glr['dibuat'] . '</small></td>
                <td>
                    <a href="javascript:void(0)" data-id="' . $glr['id'] . '" class="editGaleri btn btn-info btn-sm"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="' . $glr['id'] . '" data-kategori="' . $id_kategori . '" class="btn btn-danger btn-sm deleteGaleri"><i
                            class="fas fa-trash-alt"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    private function data_galeri()
    {
        $galeri = $this->galeriApi->get_my_galeri();
        $galeri = $galeri['body']['data'];
        $html = '';
        if (!empty($galeri)) {
            foreach ($galeri as $glr) {
                $checked = '';
                if ($glr['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td><img src="' . $glr['file'] . '" alt="" height="50">
                </td>
                <td>
                    <b class="text-dark">' . $glr['nama'] . '</b>
                    <span class="text-dark">by ' . $glr['pembuat'] . '</span>
                    <br>
                    <small class="m-0 text-dark">' . Str::limit($glr['keterangan'], 100, '...') . '
                    </small>
                </td>
                <td>' . $glr['kategori'] . '</td>
                <td>
                    <label class="switch">
                        <input type="checkbox" ' . $checked . ' class="galeri_check" data-id="' . $glr['id'] . '">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td>' . Help::getTanggalLengkap($glr['created_at']) . '<br><small>' . $glr['dibuat'] . '</small>
                </td>
                <td>
                    <a href="javascript:void(0)" data-id="' . $glr['id'] . '" class="btn btn-info btn-sm editGaleri"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="' . $glr['id'] . '" class="btn btn-danger btn-sm deleteGaleri"><i
                            class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini belum tersedia</td></tr>';
        }

        return $html;
    }
}
