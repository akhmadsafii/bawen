<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AlumniApi;
use App\ApiService\Alumni\OrganisasiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrganisasiController extends Controller
{
    private $organisasiApi;
    private $alumniApi;

    public function __construct()
    {
        $this->organisasiApi = new OrganisasiApi();
        $this->alumniApi = new AlumniApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data jurusan');
        $jurusan = $this->organisasiApi->sekolah();
        if ($jurusan['code'] != 200) {
            $pesan = array(
                'message' => $jurusan['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jurusan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('tombol', function ($row) {
                return '<a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-sm btn-success data" data-id="' . $row['id'] . '"><i class="fa fa-bolt"></i> Pintasan Data</a>';
            });
            $table->rawColumns(['action', 'tombol']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.alumni.v_jurusan')->with(['fakultas' => $fakultas]);
    }

    public function store(Request $request)
    {
        $data = array(
            'posisi' => $request['posisi'],
            'organisasi' => $request['organisasi'],
            'keterangan' => $request['keterangan'],
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
            'id_alumni' => session('role') == 'alumni-alumni' ? session('id') : $request['id_alumni'],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status']
        );
        if ($request['aktif']) {
            $data['aktif'] = 1;
        } else {
            $data['aktif'] = 0;
        }
        $result = $this->organisasiApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $organisasi  = $this->organisasiApi->get_by_id($request['id']);
        $result = $organisasi['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'posisi' => $request['posisi'],
            'organisasi' => $request['organisasi'],
            'keterangan' => $request['keterangan'],
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
            'id_alumni' => session('role') == 'alumni-alumni' ? session('id') : $request['id_alumni'],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status']
        );
        if ($request['aktif']) {
            $data['aktif'] = 1;
        } else {
            $data['aktif'] = 0;
        }
        $result = $this->organisasiApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->organisasiApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {

            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }


    public function detail_by_alumni(Request $request)
    {
        if ($request->params == "auth") {
            $params = "auth";
            $id = null;
        } else {
            $params = "alumni";
            $id = $request['id'];
        }
        return response()->json($this->detail_alumni($params, $id));
    }

    public function detail_alumni($params, $id)
    {
        if ($params == 'auth') {
            $alumni = $this->alumniApi->get_by_id(session('id'));
        } else {
            $alumni = $this->alumniApi->get_by_id($id);
        }

        $alumni = $alumni['body']['data'];
        $html = '<table style="width: 100%">';
        if (empty($alumni['organisasi'])) {
            if (session('id') == $alumni['id']) {
                $html .= '
               <tr>
                    <td colspan="3">
                        <form id="formOrganisasi" name="formOrganisasi" action="javascript:void(0)"  onsubmit="addOrganisasi(this)" class="form-horizontal">
                            <div class="form-group row">
                                <input type="hidden" name="id_alumni" value="' . $alumni['id'] . '">
                                <input type="hidden" name="byDetail" value="true">
                                <label class="col-md-3 col-form-label" for="l0">Posisi</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="posisi" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Organisasi</label>
                                <div class="col-md-9">
                                    <input type="text" name="nama_organisasi" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Masa Organisasi</label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="checkbox" name="checkOrg" id="checkOrg" onclick="displayOrg()">
                                            <label for="" style="font-weight: normal">Masih Aktif</label>
                                        </div>
                                        <div class="col-md-12" id="aktifOrg" style="display: none">
                                            <small>*tanggal mulai</small>
                                            <input type="date" name="tanggal_mulai_on" id="tanggal_mulai_on"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="row" id="notOrg">
                                        <div class="col-md-6">
                                            <small>*tanggal mulai</small>
                                            <input type="date" name="tanggal_mulai" id="tanggal_mulai"
                                                class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <small>*tanggal selesai</small>
                                            <input type="date" name="tanggal_selesai" id="tanggal_selesai"
                                                class="form-control" value="tanggal selesai">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l15">Keterangan</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="keterangan" id="keterangan" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l15"></label>
                                <div class="col-md-9">
                                    <div class="button pull-right">
                                        <input type="hidden" name="action" id="action" value="Add" />
                                        <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                            value="create">Simpan</button>
                                        <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
               ';
            } else {
                $html .= '<tr> <td style="text-align: center">Data Organisasi masih kosong</td> </tr>';
            }
        } else {
            if (session('id') == $alumni['id']) {
                $html .= '<tr>
                <td style="width: 20%">
                    <a href="javascript:void(0)" onclick="addOrgn()" class="btn btn-outline-info mb-3">
                        <i class="fa fa-plus-circle"></i> Tambah Organisasi
                    </a>
                </td>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>';
            }
            $html .= '<tbody id="outputPengalaman" class="mt-4">';
            foreach ($alumni['organisasi'] as $org) {
                if ($org['aktif'] == 1) {
                    $saat_ini = 'saat ini';
                } else {
                    $saat_ini = Help::getMonthYear($org['tgl_selesai']);
                }
                $html .= '
                <tr class="mt-2" style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                    <td style="vertical-align: middle; text-align: center" class="pr-2 pl-2">
                        ' . Help::getMonthYear($org['tgl_mulai']) . ' <p class="mb-0">-
                        </p>
                       ' . $saat_ini . '
                    </td>
                    <td style="vertical-align: middle">
                        <h5 class="mt-0">
                            <b>' . strtoupper($org['posisi']) . '</b>
                        </h5>
                        <h5 class="mt-0">' . strtoupper($org['organisasi']) . '</h5>
                        <table style="width: 100%">
                            <tr>
                                <td>Keterangan : ' . ucwords($org['keterangan']) . '</td>
                            </tr>
                        </table>
                    <td>
                ';
                if (session('id') == $alumni['id']) {
                    $html .= '<td style="text-align: right; vertical-align: middle" class="pr-2 pl-2">
                        <ul class="social-links list-inline mb-0 mt-2">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-info btn-sm editOrg" data-original-title="Edit Organisasi"
                                    data-id="' . $org['id'] . '">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-danger btn-sm deleteOrg" data-original-title="Hapus Organisasi"
                                    data-id="' . $org['id'] . '">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </td>';
                }
                $html .= ' </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>';
            }
            $html .= '</tbody>';
        }
        $html .= '</table>';
        return $html;
    }
}
