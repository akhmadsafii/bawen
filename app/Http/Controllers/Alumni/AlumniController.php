<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AlumniApi;
use App\ApiService\Alumni\BlogApi;
use App\ApiService\Alumni\FakultasApi;
use App\ApiService\Alumni\JenisPekerjaanApi;
use App\ApiService\Alumni\JurusanApi;
use App\ApiService\Alumni\KategoriBlogApi;
use App\ApiService\Alumni\KatGaleriApi;
use App\ApiService\Alumni\PekerjaanAlumniApi;
use App\ApiService\Alumni\PendidikanApi;
use App\ApiService\Master\IndonesiaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class AlumniController extends Controller
{
    private $alumniApi;
    private $kategoriApi;
    private $fakultasApi;
    private $katGaleriApi;
    private $indonesiaApi;
    private $pekerjaanApi;
    private $blogApi;
    private $jenisPekerjaanApi;

    public function __construct()
    {
        $this->alumniApi = new AlumniApi();
        $this->alumniJurusanApi = new JurusanApi();
        $this->pendidikanApi = new PendidikanApi();
        $this->fakultasApi = new FakultasApi();
        $this->kategoriApi = new KategoriBlogApi();
        $this->katGaleriApi = new KatGaleriApi();
        $this->katGaleriApi = new KatGaleriApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->pekerjaanApi = new PekerjaanAlumniApi();
        $this->jenisPekerjaanApi = new JenisPekerjaanApi();
        $this->blogApi = new BlogApi();
    }

    public function index(Request $request)
    {
        $routes = "alumni-alumni";
        $jurusan = $this->alumniJurusanApi->id_sekolah(session('id_sekolah'));
        $jurusan = $jurusan['body']['data'];
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        if (isset($_GET["fakultas"])) {
            $id_fakultas = $_GET["fakultas"] != '' ? Help::decode($_GET["fakultas"]) : '';
        } else {
            $id_fakultas = '';
        }
        if (isset($_GET["jurusan"])) {
            $id_jurusan = $_GET["jurusan"] != '' ? Help::decode($_GET["jurusan"]) : '';
        } else {
            $id_jurusan = '';
        }
        $th_angkatan = (isset($_GET["angkatan"])) ? $_GET["angkatan"] : '';
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->alumniApi->sekolah($page, $id_fakultas, $id_jurusan, $th_angkatan, $search);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        $alumni = $result['body']['data'];
        // dd($alumni);
        $meta = $result['body']['meta'];
        $fk = isset($_GET["fakultas"]) ? $_GET["fakultas"] : '';
        $jr = isset($_GET["jurusan"]) ? $_GET["jurusan"] : '';
        $pagination = Utils::filterAlumni($meta, $routes, $fk, $jr, $th_angkatan, $search);
        $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
        $fakultas = $fakultas['body']['data'];
        $angkatan = $this->alumniApi->tahun_angkatan(session('id_sekolah'));
        $angkatan = $angkatan['body']['data'];
        $approve = count($this->alumniApi->get_status(json_encode(['status' => 2]))['body']['data']);

        return view('content.alumni.v_alumni')->with([
            'template' => session('template'), 'angkatan' => $angkatan, 'fakultas' => $fakultas, 'alumni' => $alumni, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search,
            'jurusan' => $jurusan, 'approve' => $approve, 'fkl' => $fk, 'jrs' => $jr, 'th_ak' => $th_angkatan
        ]);
    }

    public function tabel()
    {
        session()->put('title', "Data Alumni");
        $routes = "alumni-tampilan_tabel";
        $jurusan = $this->alumniJurusanApi->id_sekolah(session('id_sekolah'));
        $jurusan = $jurusan['body']['data'];
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        // dd($search);
        $result = $this->alumniApi->sekolah($search);
        // dd($result);
        $data = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
        // dd($fakultas);
        $fakultas = $fakultas['body']['data'];
        $angkatan = $this->alumniApi->tahun_angkatan(session('id_sekolah'));
        $angkatan = $angkatan['body']['data'];
        return view('content.alumni.admin.v_alumni')->with(['template' => session('template'), 'angkatan' => $angkatan, 'fakultas' => $fakultas, 'data' => $data, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'jurusan' => $jurusan]);
    }

    public function pending(Request $request)
    {
        session()->put('title', 'Data Alumni');
        $alumni = $this->alumniApi->get_status(json_encode(['status' => 2]));
        if ($alumni['code'] != 200) {
            $pesan = array(
                'message' => $alumni['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('message', $pesan);
        }
        $result = $alumni['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="info btn btn-info btn-sm "><i class="fa fa-info-circle"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="verifikasi btn btn-success btn-sm"><i class="fa fa-check-circle"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-times-circle"></i></button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }

        return view('content.alumni.v_pending_alumni')->with(['template' => session('template')]);
    }

    public function detail(Request $request)
    {
        // dd("tes");
        $alumni = $this->alumniApi->get_by_id(Help::decode($_GET['key']));
        $alumni = $alumni['body']['data'];
        // dd($alumni);
        $kategori = $this->kategoriApi->id_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        if ($_GET['method'] == 'pendidikan') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['pendidikan'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('detail', function ($row) {
                    return '<h3 class="m-0 text-uppercase">' . $row['jenjang'] . '</h3><p class="m-0">Jurusan ' . $row['jurusan'] . '</p>';
                });
                $table->editColumn('angkatan', function ($row) {
                    return $row['th_masuk'] . ' - ' . $row['th_lulus'];
                });
                $table->rawColumns(['action', 'status', 'detail', 'angkatan']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.v_pendidikan')->with(['alumni' => $alumni, 'template' => session('template')]);
        }
        if ($_GET['method'] == 'pekerjaan') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['pekerjaan'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('masa_kerja', function ($row) {
                    $tgl_berakhir = 'saat ini';
                    if ($row['aktif'] != 1) {
                        $tgl_berakhir = Help::getTanggal($row['tgl_selesai']);
                    }
                    return Help::getTanggal($row['tgl_mulai']) . " - " . $tgl_berakhir;
                });
                $table->rawColumns(['action', 'status', 'masa_kerja']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.v_pekerjaan')->with(['alumni' => $alumni, 'template' => session('template')]);
        }
        if ($_GET['method'] == 'organisasi') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['organisasi'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('masa_kerja', function ($row) {
                    $tgl_berakhir = 'saat ini';
                    if ($row['aktif'] != 1) {
                        $tgl_berakhir = Help::getTanggal($row['tgl_selesai']);
                    }
                    return Help::getTanggal($row['tgl_mulai']) . " - " . $tgl_berakhir;
                });
                $table->rawColumns(['action', 'status', 'masa_kerja']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.v_organisasi')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'sosial') {
            return view('content.alumni.detail.v_sosial')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'galeri') {
            return view('content.alumni.detail.v_galeri')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'blog') {
            return view('content.alumni.detail.v_blog')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'agenda') {
            if (request()->ajax()) {
                return response()->json($alumni['agenda']);
            }
            return view('content.alumni.detail.v_agenda')->with(['alumni' => $alumni, 'template' => session('template')]);
        }


        // return view('content.alumni.detail.v_detail')->with(['alumni' => $alumni, 'kategori' => $kategori, 'template' => session('template')]);
    }

    public function my_profile(Request $request)
    {
        $alumni = $this->alumniApi->get_by_id(session('id'));
        $alumni = $alumni['body']['data'];
        // dd($alumni);
        if ($_GET['method'] == 'profil') {
            $jurusan = $this->alumniJurusanApi->id_sekolah(session('id_sekolah'));
            $jurusan = $jurusan['body']['data'];
            $fakultas = $this->fakultasApi->get_id_sekolah(session('id_sekolah'));
            // dd($fakultas);
            $fakultas = $fakultas['body']['data'];
            $kabupaten = $this->indonesiaApi->get_kabupaten();
            // dd($kabupaten);
            $kabupaten = $kabupaten['body']['data'];
            return view('content.alumni.detail.profile.v_profile')->with([
                'alumni' => $alumni, 'template' => session('template'),
                'jurusan' => $jurusan, 'fakultas' => $fakultas, 'kabupaten' => $kabupaten
            ]);
        }
        if ($_GET['method'] == 'pendidikan') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['pendidikan'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('detail', function ($row) {
                    return '<h3 class="m-0 text-uppercase">' . $row['jenjang'] . '</h3><p class="m-0">Jurusan ' . $row['jurusan'] . '</p>';
                });
                $table->editColumn('angkatan', function ($row) {
                    return $row['th_masuk'] . ' - ' . $row['th_lulus'];
                });
                $table->rawColumns(['action', 'status', 'detail', 'angkatan']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.profile.v_pendidikan')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'pekerjaan') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['pekerjaan'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('masa_kerja', function ($row) {
                    $tgl_berakhir = 'saat ini';
                    if ($row['aktif'] != 1) {
                        $tgl_berakhir = Help::getTanggal($row['tgl_selesai']);
                    }
                    return Help::getTanggal($row['tgl_mulai']) . " - " . $tgl_berakhir;
                });
                $table->rawColumns(['action', 'status', 'masa_kerja']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.profile.v_pekerjaan')->with(['alumni' => $alumni, 'template' => session('template')]);
        }
        if ($_GET['method'] == 'organisasi') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['organisasi'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('masa_kerja', function ($row) {
                    $tgl_berakhir = 'saat ini';
                    if ($row['aktif'] != 1) {
                        $tgl_berakhir = Help::getTanggal($row['tgl_selesai']);
                    }
                    return Help::getTanggal($row['tgl_mulai']) . " - " . $tgl_berakhir;
                });
                $table->rawColumns(['action', 'status', 'masa_kerja']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.profile.v_organisasi')->with(['alumni' => $alumni, 'template' => session('template')]);
        }
        if ($_GET['method'] == 'agenda') {
            if (request()->ajax()) {
                return response()->json($alumni['agenda']);
            }
            return view('content.alumni.detail.profile.v_agenda')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'sosial') {
            if ($request->ajax()) {
                $table = datatables()->of($alumni['sosial'])
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit mx-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete mx-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                        return $button;
                    });
                $table->editColumn('status', function ($row) {
                    $checked = '';
                    if ($row['status'] == 1) {
                        $checked = 'checked';
                    }
                    return '<label class="switch">
                        <input type="checkbox"
                            ' . $checked . '
                            class="check_pendidikan"
                            data-id="' . $row['id'] . '">
                        <span class="slider round"></span>
                    </label>';
                });
                $table->editColumn('media', function ($row) {
                    if ($row['media'] == 'facebook') {
                        $icon = 'fab fa-facebook-f';
                        $color = 'facebook';
                    } elseif ($row['media'] == 'instagram') {
                        $icon = 'fab fa-instagram';
                        $color = 'dribbble';
                    } elseif ($row['media'] == 'linkedin') {
                        $icon = 'fab fa-linkedin-in';
                        $color = 'linkedin';
                    } elseif ($row['media'] == 'twitter') {
                        $icon = 'fab fa-twitter';
                        $color = 'twitter';
                    } elseif ($row['media'] == 'website') {
                        $icon = 'fas fa-wifi';
                        $color = 'info';
                    }
                    return '<a href="' . $row['nama'] . '" target="_blank"><div><i class="' . $icon . ' border-color-' . $color . ' text-' . $color . ' fa-2x"></i><br><small class="text-' . $color . '">' . $row['media'] . '</small></div></a>';
                });
                $table->editColumn('url', function ($row) {
                    return '<a href="' . $row['nama'] . '" target="_blank">' . $row['nama'] . '</a>';
                });
                $table->rawColumns(['action', 'status', 'media', 'url']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.alumni.detail.profile.v_sosial')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'galeri') {
            $kategori = $this->katGaleriApi->id_sekolah(session('id_sekolah'));
            $kategori = $kategori['body']['data'];
            return view('content.alumni.detail.profile.v_galeri')->with(['alumni' => $alumni, 'template' => session('template'), 'kategori' => $kategori]);
        }

        if ($_GET['method'] == 'blog') {
            $kategori = $this->kategoriApi->id_sekolah(session('id_sekolah'));
            // dd($kategori);
            $kategori = $kategori['body']['data'];
            if ($_GET['aksi'] == 'create') {
                return view('content.alumni.detail.profile.v_create_blog')->with([
                    'alumni' => $alumni, 'template' => session('template'),
                    'kategori' => $kategori
                ]);
            }
            if ($_GET['aksi'] == 'edit') {
                $blog = $this->blogApi->get_by_id(Help::decode($_GET['key']));
                // dd($blog);
                $blog = $blog['body']['data'];
                return view('content.alumni.detail.profile.v_create_blog')->with([
                    'alumni' => $alumni, 'template' => session('template'),
                    'kategori' => $kategori, 'blog' => $blog
                ]);
            }
            return view('content.alumni.detail.profile.v_blog')->with(['alumni' => $alumni, 'template' => session('template')]);
        }

        if ($_GET['method'] == 'myjob') {
            $pekerjaan = $this->pekerjaanApi->by_alumni(session('id'));
            // dd($pekerjaan);
            $pekerjaan = $pekerjaan['body']['data'];
            $jenis = $this->jenisPekerjaanApi->sekolah(session('id_sekolah'));
            // dd($jenis);
            $jenis = $jenis['body']['data'];
            return view('content.alumni.detail.profile.v_pekerjaan_alumni')->with([
                'alumni' => $alumni, 'template' => session('template'),
                'pekerjaan' => $pekerjaan, 'jenis' => $jenis
            ]);
        }
    }

    public function edit($id)
    {
        $alumni = $this->alumniApi->get_by_id($id);
        // dd($alumni);
        $alumni = $alumni['body']['data'];
        $jurusan = $this->alumniJurusanApi->id_sekolah(session('id_sekolah'));
        $jurusan = $jurusan['body']['data'];
        return view('content.alumni.detail.v_edit')->with(['alumni' => $alumni, 'jurusan' => $jurusan]);;
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'no_induk' => $request['no_induk'],
            'id_jurusan' => $request['id_jurusan'],
            'nama' => $request['nama'],
            'jenkel' => $request['jenkel'],
            'agama' => $request['agama'],
            'telepon' => $request['telepon'],
            'email' => $request['email'],
            'tempat_lahir' => $request['tempat_lahir'],
            'pendidikan_terakhir' => $request['pendidikan_terakhir'],
            'tgl_lahir' => $request['tgl_lahir'],
            'alamat' => $request['alamat'],
            'tahun_lulus' => $request['tahun_lulus'],
            'tahun_angkatan' => $request['tahun_angkatan'],
            'no_ijazah' => $request['no_ijazah'],
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            $result = $this->alumniApi->create_file(json_encode($data));
        } else {
            $result = $this->alumniApi->create(json_encode($data));
        }
        // dd($data);
        // dd($result);
        if ($result['code'] == 200) {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }
        return response()->json($message);
    }

    public function update(Request $request)
    {
        // dd(session()->all());
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'no_induk' => $request['no_induk'],
            'id_jurusan' => $request['id_jurusan'],
            'nama' => $request['nama'],
            'jenkel' => $request['jenkel'],
            'agama' => $request['agama'],
            'telepon' => $request['telepon'],
            'email' => $request['email'],
            'tempat_lahir' => $request['tempat_lahir'],
            'tahun_angkatan' => $request['tahun_angkatan'],
            'pendidikan_terakhir' => $request['pendidikan_terakhir'],
            'tgl_lahir' => $request['tgl_lahir'],
            'alamat' => $request['alamat'],
            'tahun_lulus' => $request['tahun_lulus'],
            'no_ijazah' => $request['no_ijazah'],
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            $result = $this->alumniApi->update_file(json_encode($data));
            // dd("tes");
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->alumniApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->alumniApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }
        if ($request['from']) {
            return response()->json($message);
        } else {
            return redirect()->back()->with(['message' => $message]);
        }
    }

    public function resetPassword(Request $request)
    {
        if ($request['params'] == 'manual') {
            $first_password = $request['password'];
        } else {
            $first_password = null;
        }

        $data = array(
            'first_password' => $first_password,
            'id' => $request['id']
        );
        $reset = $this->alumniApi->reset_password(json_encode($data));
        if ($reset['code'] == 200) {
            $message = array(
                'message' => $reset['body']['message'],
                'icon'  => 'success',
                'data'  => $reset['body']['data'],
                'status'  => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $reset['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }
        return response()->json($message);
        // dd($reset);
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update_status = $this->alumniApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function profile(Request $request)
    {
        // dd($request);
        if ($request->params == "auth") {
            $alumni = $this->alumniApi->get_by_id(session('id'));
        } else {
            $alumni = $this->alumniApi->get_by_id($request['id']);
        }
        $alumni = $alumni['body']['data'];
        $html = '
        <div class="row">
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">NAMA</h6>
                <p class="mr-t-0">' . ucfirst($alumni['nama']) . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">JURUSAN</h6>
                <p class="mr-t-0">' . $alumni['jurusan'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Nomor Induk</h6>
                <p class="mr-t-0">' . $alumni['no_induk'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Telepon</h6>
                <p class="mr-t-0">' . $alumni['telepon'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Jenis Kelamin</h6>
                <p class="mr-t-0">' . $alumni['jenkel'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Agama</h6>
                <p class="mr-t-0">' . $alumni['agama'] . '
                </p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Tempat, Tanggal Lahir</h6>
                <p class="mr-t-0">
                    ' . $alumni['tempat_lahir'] . ', ' . $alumni['tgl_lahir'] . '
                </p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Tahun Lulus</h6>
                <p class="mr-t-0">
                    ' . $alumni['tahun_lulus'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Nomor Ijazah</h6>
                <p class="mr-t-0">' . $alumni['no_ijazah'] . '</p>
            </div>
            <div class="col-md-6">
                <h6 class="text-muted text-uppercase">Alamat</h6>
                <p class="mr-t-0">' . $alumni['alamat'] . '</p>
            </div>
        </div>
        ';
        return response()->json($html);
    }

    public function detail_profile(Request $request)
    {
        $alumni = $this->alumniApi->get_by_id($request['id']);
        $alumni = $alumni['body']['data'];
        return response()->json($alumni);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->alumniApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
