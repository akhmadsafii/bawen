<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\PekerjaanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PekerjaanController extends Controller
{
    private $pekerjaanApi;

    public function __construct()
    {
        $this->pekerjaanApi = new PekerjaanApi();
    }

    public function store(Request $request)
    {
        $data = array(
            'posisi' => $request['posisi'],
            'industri' => $request['industri'],
            'keterangan' => $request['keterangan'],
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
            'id_alumni' => session('role') == 'alumni-alumni' ? session('id') : $request['id_alumni'],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status']
        );
        if ($request['aktif']) {
            $data['aktif'] = 1;
        } else {
            $data['aktif'] = 0;
        }
        $result = $this->pekerjaanApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function detail_alumni(Request $request)
    {
        if ($request->params == "auth") {
            $params = "auth";
            $id = null;
        } else {
            $params = "alumni";
            $id = $request['id'];
        }
        return response()->json($this->by_alumni($params, $id));
    }

    public function edit(Request $request)
    {
        $post  = $this->pekerjaanApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'posisi' => $request['posisi'],
            'industri' => $request['industri'],
            'keterangan' => $request['keterangan'],
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
            'id_alumni' => session('role') == 'alumni-alumni' ? session('id') : $request['id_alumni'],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status']
        );
        if ($request['aktif']) {
            $data['aktif'] = 1;
        } else {
            $data['aktif'] = 0;
        }
        $result = $this->pekerjaanApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->pekerjaanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->pekerjaanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->pekerjaanApi->all_trash();
        // dd($jurusan);
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->pekerjaanApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function by_alumni($params, $id)
    {
        // dd($params);
        if ($params == 'auth') {
            $pekerjaan = $this->pekerjaanApi->by_auth_alumni();
            $alumni = "auth";
        } else {
            $pekerjaan = $this->pekerjaanApi->by_id_alumni($id);
            $alumni = "alumni";
        }
        $pekerjaan = $pekerjaan['body']['data'];
        $html = '';
        if (!empty($pekerjaan)) {
            $html .= '
            <table style="width: 100%">';
            if (session('role') == 'admin-alumni' || $alumni == 'auth') {
                $html .= '<tr>
                    <td style="width: 20%">
                        <a href="javascript:void(0)" onclick="btnAddPekerjaan()" class="btn btn-outline-info mb-3">
                            <i class="fa fa-plus-circle"></i> Tambah Pekerjaan
                        </a>
                    </td>
                    <td></td>
                    <td></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>';
            }

            $html .= '<tbody>
            ';
            foreach ($pekerjaan as $pk) {
                $html .= '
                <tr class="mt-2"
                    style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                    <td style="vertical-align: middle" class="pr-2 pl-2">
                        ' . Help::getMonthYear($pk['tgl_mulai']) . '
                        -
                        ' . Help::getMonthYear($pk['tgl_selesai']) . '

                    </td>
                    <td style="vertical-align: middle">
                        <h5 class="mt-0">
                            <b>' . $pk['posisi'] . '</b>
                        </h5>
                        <h5 class="mt-0">' . $pk['industri'] . '
                        </h5>
                        <table style="width: 100%">
                            <tr>
                                <td>' . $pk['keterangan'] . '</td>
                            </tr>
                        </table>
                    <td>
                    <td style="text-align: right; vertical-align: middle"
                        class="pr-2 pl-2">
                        <ul class="social-links list-inline mb-0 mt-2">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                    data-placement="top"
                                    class="btn btn-info btn-sm editPekerjaan"
                                    data-original-title="Edit Pekerjaan"
                                    data-id="' . $pk['id'] . '">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                    data-placement="top"
                                    class="btn btn-danger btn-sm deletePekerjaan"
                                    data-original-title="Hapus Pendidikan"
                                    data-id="' . $pk['id'] . '">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            ';
            }
            $html .= '</tbody>
            </table>';
        } else {
            if ($alumni == 'auth' || session('role') == 'admin-alumni') {
                $html .= '
                <table style="width: 100%">
                    <tr>
                        <td colspan="3">
                            <form id="formPekerjaan" action="javascript:void(0)"
                                onsubmit="addPekerjaan(this)" name="formPekerjaan"
                                class="form-horizontal">
                                <div class="form-group row">
                                    <input type="hidden" name="id_alumni" value="' . session('id') . '">
                                    <input type="hidden" name="byDetail" value="true">
                                    <label class="col-md-3 col-form-label" for="l0">Posisi</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="posisi" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Industi</label>
                                    <div class="col-md-9">
                                        <input type="text" name="industri" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                                    <div class="col-md-9">
                                        <textarea name="keterangan" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Masa Kerja</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="checkbox" name="checkOrg" id="checkOrg"
                                                    onclick="displayOrg()">
                                                <label for="" style="font-weight: normal">Masih
                                                    Aktif</label>
                                            </div>
                                            <div class="col-md-12" id="aktifOrg"
                                                style="display: none">
                                                <small>*tanggal mulai</small>
                                                <input type="date" name="tanggal_mulai_on"
                                                    id="tanggal_mulai_on" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row" id="notOrg">
                                            <div class="col-md-6">
                                                <small>*tanggal mulai</small>
                                                <input type="date" name="tanggal_mulai"
                                                    id="tanggal_mulai" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <small>*tanggal selesai</small>
                                                <input type="date" name="tanggal_selesai"
                                                    id="tanggal_selesai" class="form-control"
                                                    value="tanggal selesai">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15"></label>
                                    <div class="col-md-9">
                                        <div class="button pull-right">
                                            <input type="hidden" name="action" id="action"
                                                value="Add" />
                                            <button type="submit" class="btn btn-outline-info"
                                                id="saveBtn" value="create">Simpan</button>
                                            <button class="btn btn-outline-danger"
                                                id="btnCancel">Batalkan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                </table>
            ';
            } else {
                $html .= '<p style="text-align:center">Untuk saat ini data pekerjaan dari alumni sedang kosong</p>';
            }
        }
        return $html;
    }
}
