<?php

namespace App\Http\Controllers\Sekolah;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Sekolah\FakultasApi;

class FakultasController extends Controller
{

    public $fakultasApi;

    public function __construct()
    {
        $this->fakultasApi = new FakultasApi("api/data/mutu/fakultas");
    }

    public function index(Request $request){
        $dataFakultas = $this->fakultasApi->get_by_sekolah();
        $dataFakultas = $dataFakultas['body']['data'];
        
        if ($request->ajax()) {
            $table = datatables()->of($dataFakultas);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="ShowModal('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Delete('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            
            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.penjamin_mutu.master.fakultas')->with([]);
    }

    public function create(Request $request){
        $kode = $request->kode;
        $id_sekolah = $request->id_sekolah;
        $nama = $request->nama;

        $data = array(
            "id_sekolah" => session('id_sekolah'),
            "kode" => $kode,
            "nama" => $nama,
        );

        $fakultas = $this->fakultasApi->create(json_encode($data));
        
        if ($fakultas['code'] == 200) {
            return response()->json([
                'message' => $fakultas['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $fakultas['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request){
        
        $data = $this->fakultasApi->get_detail($request->id);
        $data = $data['body']['data'];
        return response()->json($data);
        
        
    }

    public function show($id)
    {
        //
    }


    public function edit(Request $request)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $fakultas = $this->fakultasApi->soft_delete($request->id);
        //dd($fakultas);
        
        if ($fakultas['code'] == 200) {
            return redirect()->back()->with('success', 'pesan terkirim');  
        } else {
            return redirect()->back()->with('failed', 'pesan terkirim');  
        }
    }
}
