<?php

namespace App\Http\Controllers\Sekolah;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sekolah\FakultasApi;
use App\ApiService\Sekolah\JenjangApi;
use App\ApiService\Sekolah\KejuruanApi;

class KejuruanController extends Controller
{
    public $kejuruanApi;
    public $fakultasApi;
    public $jenjangApi;

    public function __construct()
    {
        $this->fakultasApi = new FakultasApi("api/data/mutu/fakultas");
        $this->kejuruanApi = new KejuruanApi("api/data/mutu/kejuruan");
        $this->jenjangApi = new JenjangApi("api/data/mutu/jenjang");
    }

    public function index(Request $request)
    {
        $dataFakultas = $this->fakultasApi->get_by_sekolah();
        $dataJenjang = $this->jenjangApi->get_by_sekolah();
        $dataKejuruan = $this->kejuruanApi->get_by_sekolah();

        $dataFakultas = $dataFakultas['body']['data'];
        $dataKejuruan = $dataKejuruan['body']['data'];
        $dataJenjang = $dataJenjang['body']['data'];
        
        if ($request->ajax()) {
            $table = datatables()->of($dataKejuruan);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="ShowModal('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Delete('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            
            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.penjamin_mutu.master.kejuruan',compact('dataFakultas','dataJenjang'));
    }

   
    public function create(Request $request){
        
        $id_sekolah = 1;
        $nama = $request->nama;
        $id_fakultas = $request->id_fakultas;
        $id_jenjang = $request->id_jenjang;

        $data = array(
            "id_sekolah" => 1,
            "nama" => $nama,
            "id_fakultas" => $id_fakultas,
            "id_jenjang" => $id_jenjang,
        );

        $kejuruan = $this->kejuruanApi->create(json_encode($data));
        
        if ($kejuruan['code'] == 200) {
            return response()->json([
                'message' => $kejuruan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $kejuruan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request){
        $kejuruan = $this->kejuruanApi->soft_delete($request->id);
        //dd($kejuruan);
        
        if ($kejuruan['code'] == 200) {
            return redirect()->back()->with('success', 'pesan terkirim');  
        } else {
            return redirect()->back()->with('failed', 'pesan terkirim');  
        }
    }

    public function update(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $nama = $request->nama;
        $id_fakultas = $request->id_fakultas;
        $id_jenjang = $request->id_jenjang;
        $id = $request->id;


        $data = array(
            "id_sekolah" => 1,
            "nama" => $nama,
            "id_fakultas" => $id_fakultas,
            "id_jenjang" => $id_jenjang,
        );

        $kejuruan = $this->kejuruanApi->update($id,json_encode($data));
        
        if ($kejuruan['code'] == 200) {
            return response()->json([
                'message' => $kejuruan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $kejuruan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    
    public function destroy($id)
    {
        //
    }
}
