<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\AdminAbsensiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AdminAbsensiController extends Controller
{
    private $adminApi;

    public function __construct()
    {
        $this->adminApi = new AdminAbsensiApi();
    }

    public function index()
    {
        $admin = $this->adminApi->by_sekolah();
        // dd($admin);
        $admin = $admin['body']['data'];
        $template = session('template');
        if (session('role') == 'admin' || session('role') == 'admin-cbt') {
            $template = 'default';
        }
        session()->put('title', 'Data Admin');
        return view('content.absensi.v_admin')->with(['template' => $template, 'admin' => $admin]);
    }

    public function create()
    {
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $template = session('template');
        session()->put('title', 'Buat admin');
        return view('content.cbt.v_create_admin')->with(['template' => $template, 'mapel' => $mapel]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'email' => $request['email'],
            'first_password' => $request['password'],
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request['nama'],
            'email' => $request['email'],
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        // dd();
        $admin = $this->adminApi->get_by_id($request['id']);
        // dd($admin);
        $admin = $admin['body']['data'];
        return response()->json($admin);
    }

    public function delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'admin' => $admin,
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->adminApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function res_pass(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->adminApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    private function data_admin()
    {
        $admin = $this->adminApi->by_sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (!empty($admin)) {
            $no = 1;
            foreach ($admin as $adm) {
                $checked = '';
                if ($adm['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                    <td class="vertical-middle">' . $no++ . '</td>

                    <td class="vertical-middle"><b>' . ucwords($adm['nama']) . '
                            <br><small>email : ' . $adm['email'] . '</small></b>
                    </td>
                    <td class="vertical-middle">
                        <label class="switch">
                            <input type="checkbox" ' . $checked . '
                                class="admin_check" data-id="' . $adm['id'] . '">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td class="text-center vertical-middle">

                        <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                            data-id="' . $adm['id'] . '"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)" class="btn btn-purple btn-sm reset_pass"
                            data-id="' . $adm['id'] . '"><i
                                class="fas fa-key"></i></a>
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm delete"
                        data-id="' . $adm['id'] . '"><i
                            class="fas fa-trash"></i></a>
                    </td>
            </tr>';
            }
        } else {
            $html .= ' <tr>
            <td colspan="8" class="text-center">Data saat ini kosong</td>
        </tr>';
        }
        return $html;
    }
}
