<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\User\GuruApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    private $guruApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Daftar Guru');
        $guru = $this->guruApi->get_by_sekolah();
        $result = $guru['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    if ($data['akun_absen'] != null) {
                        return '<a href="javascript:void(0)" class="btn btn-success btn-sm"><i class="fas fa-check-circle"></i></a>';
                    } else {
                        return '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="btn btn-info btn-sm addGuru"><i class="fas fa-sync-alt"></i></a>';
                    }

                });
            $table->editColumn('status', function ($item) {
                $checked = '';
                if ($item['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                    <input type="checkbox" '.$checked.' class="guru_check" data-id="'.$item['id'].'">
                    <span class="slider round"></span>
                </label>';
            });
            $table->editColumn('nama', function ($row) {
                return '<b>'.ucwords($row['nama']).'</b><p class="m-0">NIP. '.$row['nip'].' / NIK. '.$row['nik'].'</p><small>Alamat '.$row['alamat'].'</small>';
            });
            $table->rawColumns(['action', 'nama', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_guru')->with(['template' => $template]);
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->guruApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
