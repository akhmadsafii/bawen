<?php

namespace App\Http\Controllers\Absensi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AbsenController extends Controller
{
    public function libur()
    {
        $template = session('template');
        if(session('role') == 'admin-absensi'){
            $template = 'default';
        }
        return view('content.absensi.v_libur')->with(['template' => $template]);
    }
}
