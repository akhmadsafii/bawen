<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\JadwalApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class jadwalController extends Controller
{
    private $jadwalApi;

    public function __construct()
    {
        $this->jadwalApi = new JadwalApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Settingan Absensi Sekolah');
        $jadwal = $this->jadwalApi->get_sekolah();
        $jadwal = $jadwal['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_jadwal')->with(['template' => $template, 'jadwal' => $jadwal]);
    }

    public function store(Request $request)
    {
        $data = array(
            'hari' => $request['hari'],
            'jam_masuk' => $request['jam_masuk'],
            'jam_pulang' => $request['jam_pulang'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jadwalApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $jadwal = $this->data_jadwal();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jadwal' => $jadwal
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'hari' => $request['hari'],
            'jam_masuk' => $request['jam_masuk'],
            'jam_pulang' => $request['jam_pulang'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jadwalApi->update(json_encode($data));
        if ($result['code'] == 200) {
            $jadwal = $this->data_jadwal();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jadwal' => $jadwal
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $jadwal  = $this->jadwalApi->get_by_id($request['id']);
        // dd($jadwal);
        $result = $jadwal['body']['data'];
        return response()->json($result);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->jadwalApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $jadwal = $this->data_jadwal();
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jadwal' => $jadwal
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
    
    private function data_jadwal()
    {
        $jadwal = $this->jadwalApi->get_sekolah();
        $jadwal = $jadwal['body']['data'];
        $html = '';
        if (!empty($jadwal)) {
            foreach($jadwal as $jd){
                $checked = '';
                if ($jd['status'] == 1) {
                    $checked = 'checked';
                }
                $html .='<tr>
                <td class="text-center">'.$jd['hari'].'</td>
                <td class="text-center">'.$jd['jam_masuk'].'</td>
                <td class="text-center">'.$jd['jam_pulang'].'</td>
                <td>'.$jd['keterangan'].'</td>
                <td class="text-center">
                    <label class="switch">
                        <input type="checkbox" '.$checked.' class="check_jadwal" data-id="'.$jd['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="btn btn-info btn-sm edit" data-id="'.$jd['id'].'"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="'.$jd['id'].'"><i class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="6">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
        
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->jadwalApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
