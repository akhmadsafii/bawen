<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\LiburApi;
use App\ApiService\Master\EnvironmentApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LiburController extends Controller
{
    private $liburApi;
    private $envApi;

    public function __construct()
    {
        $this->liburApi = new LiburApi();
        $this->envApi = new EnvironmentApi();
    }

    public function index(Request $request)
    {
        // dd(session('tahun'));
        session()->put('title', 'Libur Weekend');
        $libur = $this->liburApi->get_sekolah(date("Y"));
        // dd($libur);
        $result = $libur['body']['data'];
        $prei = $this->liburApi->getLiburYear(date("Y"));
        // dd($prei);
        $prei = $prei['body'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm mx-1"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm mx-1"><i class="fas fa-trash"></i></a>';
                    return $button;
                });
            $table->editColumn('status', function ($item) {
                $checked = '';
                if ($item['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                    <input type="checkbox" ' . $checked . ' class="libur_check" data-id="' . $item['id'] . '">
                    <span class="slider round"></span>
                </label>';
            });
            $table->editColumn('nama', function ($row) {
                return '<b>' . ucwords($row['nama']) . '</b><p class="m-0"><small>Keterangan. ' . $row['keterangan'] . '</small></p>';
            });
            $table->editColumn('tanggal', function ($rows) {
                return Help::getDay($rows['tgl_libur']);
            });
            $table->rawColumns(['action', 'nama', 'status', 'tanggal']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_libur')->with(['template' => $template, 'libur' => $prei]);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request['nama'],
            'tgl_libur' => $request['tgl_libur'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->liburApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'tgl_libur' => $request['tgl_libur'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->liburApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $libur  = $this->liburApi->get_by_id($request['id']);
        $result = $libur['body']['data'];
        return response()->json($result);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->liburApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function syncrone_data(Request $request)
    {
        // dd($request);
        $syncrone = $this->liburApi->syncrone_data($request['tahun']);
        // dd($syncrone);
        if ($syncrone['code'] == 200) {
            $data_hari = $this->liburApi->getLiburYear($request['tahun']);
            // dd($data_hari);
            $data_hari = $data_hari['body'];
            if (!empty($data_hari)) {
                foreach ($data_hari as $hr) {
                    // dd($hr);
                    $status = 0;
                    if ($hr['is_national_holiday'] == true) {
                        $status = 1;
                    }
                    $data = array(
                        'tgl_libur' => $hr['holiday_date'],
                        'nama' => $hr['holiday_name'],
                        'status' => $status,
                        'id_sekolah' => session('id_sekolah')
                    );
                    $result = $this->liburApi->create(json_encode($data));
                }
                if ($result['code'] == 200) {
                    return response()->json([
                        'message' => $result['body']['message'],
                        'icon'  => 'success',
                        'status'  => 'berhasil',
                    ]);
                } else {
                    return response()->json([
                        'message' => $result['body']['message'],
                        'icon'  => 'error',
                        'status'  => 'gagal'
                    ]);
                }
            } else {
                return response()->json([
                    'message' => "Proses Syncronisasi Gagal",
                    'icon'  => 'error',
                    'status'  => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'message' => "Proses Syncronisasi Gagal",
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function filterBulan(Request $request)
    {
        $libur = $this->liburApi->getLibur($request['bulan'], $request['tahun']);
        $libur = $libur['body'];
        $html = '';
        if (!empty($libur)) {
            $no = 1;
            foreach ($libur as $lb) {
                $checked = '';
                if ($lb['is_national_holiday'] == true) {
                    $checked = 'checked';
                }
                $html .= '
                <tr>
                <td>' . $no++ . '</td>
                <td>' . Help::getDay($lb['holiday_date']) . '</td>
                <td>' . $lb['holiday_name'] . '</td>
                <td>
                    <label class="switch">
                        <input type="checkbox" ' . $checked . ' disabled>
                            <span class="slider round"></span>
                    </label>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= ' <tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }

        return response()->json($html);
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->liburApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
