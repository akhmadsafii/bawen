<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\JenisApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JenisController extends Controller
{
    private $jenisApi;

    public function __construct()
    {
        $this->jenisApi = new JenisApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Jenis Ujian');
        $jenis = $this->jenisApi->by_sekolah();
        // dd($jenis);
        if ($jenis['code'] != 200) {
            $pesan = array(
                'message' => $jenis['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('pesan', $pesan);
        }
        $result = $jenis['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit m-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete m-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                    return $button;
                });
            $table->editColumn('status', function ($row) {
                // dd($row);
                $checked = '';
                if ($row['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch"><input type="checkbox" ' . $checked . ' class="jenis_check" data-id="' . $row['id'] . '"><span class="slider round"></span></label>';
            });
            $table->rawColumns(['action', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $template = 'default';
        return view('content.cbt.v_jenis')->with(['template' => $template]);
    }



    public function store(Request $request)
    {
        $data = array(
            'kode' => $request['kode'],
            'nama' => $request['nama'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jenisApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'kode' => $request['kode'],
            'nama' => $request['nama'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jenisApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $jenis = $this->jenisApi->get_by_id($request['id']);
        $jenis = $jenis['body']['data'];
        return response()->json($jenis);
    }

    public function delete(Request $request)
    {

        $delete = $this->jenisApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
