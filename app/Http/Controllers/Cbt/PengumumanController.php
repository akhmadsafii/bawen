<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\PengumumanApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class PengumumanController extends Controller
{
    private $pengumumanApi;
    private $tahunApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->pengumumanApi = new PengumumanApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index(Request $request)
    {
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        if ($_GET['rb'] == 'all') {
            $pengumuman = $this->pengumumanApi->by_sekolah(Help::decode($_GET['th']));
        } else {
            $pengumuman = $this->pengumumanApi->by_rombel(Help::decode($_GET['rb']), Help::decode($_GET['th']));
        }
        $pengumuman = $pengumuman['body']['data'];
        // dd($pengumuman);
        if ($request->ajax()) {
            $table = datatables()->of($pengumuman)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('rombel', function ($row) {
                $resultstr = [];
                foreach ($row['rombels'] as $rmb) {
                    $resultstr[] = $rmb['nama'];
                }
                return implode(', ', $resultstr);
            });
            $table->editColumn('status', function ($row) {
                $check = '';
                if ($row['status'] == 1) {
                    $check = 'checked';
                }
                return '<label class="switch">
                    <input type="checkbox" ' . $check . '
                        class="pengumuman_check" data-id="' . $row['id'] . '">
                    <span class="slider round"></span>
                </label>';
            });
            $table->rawColumns(['action', 'rombel', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        session()->put('title', 'Data Pengumuman');
        return view('content.cbt.v_pengumuman')->with(['jurusan' => $jurusan, 'tahun' => $tahun]);
    }

    public function detail(Request $request)
    {
        $pengumuman = $this->pengumumanApi->get_by_id($request['id']);
        $pengumuman = $pengumuman['body']['data'];
        // dd($pengumuman);
        return response()->json($pengumuman);
    }

    public function store(Request $request)
    {
        // dd($id_rombel);
        $data = array(
            'judul' => $request->judul,
            'isi' => $request->pengumuman,
            'rombel' => implode('###', $request['id_rombel']),
            'id_ta_sm' => Help::decode($request->th),
            'id_sekolah' => session('id_sekolah')
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pengumumanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pengumumanApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'judul' => $request->judul,
            'isi' => $request->pengumuman,
            'rombel' => implode('###', $request['id_rombel']),
            'id_ta_sm' => Help::decode($request->th),
            'id_sekolah' => session('id_sekolah')
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pengumumanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pengumumanApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->pengumumanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->mapelApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
