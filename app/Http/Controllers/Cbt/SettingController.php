<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\SettingApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Settings;

class SettingController extends Controller
{
    private $settingApi;

    public function __construct()
    {
        $this->settingApi = new SettingApi();
    }
    
    public function index()
    {
        $setting = $this->settingApi->by_sekolah();
        // dd($setting);
        $setting = $setting['body']['data'];
        if(!empty($setting)){
            $data = "on";
        }else{
            $data = "off";
        }
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        session()->put('title', 'Data Soal');
        return view('content.cbt.v_setting')->with(['template' => $template, 'setting' => $setting, 'data' => $data]);
    }

    public function store(Request $request)
    {
        $data = array(
            'judul_login' => $request['judul'],
            'nama_aplikasi' => $request['nama'],
            'jumlah_opsi' => $request['opsi_soal'],
            'id_sekolah' => session('id_sekolah'),
        );
        if (isset($request['token'])) {
            $data['token'] = 1;
        }
        if (isset($request['lihat_nilai'])) {
            $data['lihat_nilai'] = 1;
        }
        if (isset($request['tombol_selesai'])) {
            $data['tombol_selesai'] = 1;
        }
        $result = $this->settingApi->create(json_encode($data));
        dd($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'kode_mapel' => $request->kode_mapel[0],
            'nama' => $request->nama[0],
            'ruang' => $request->ruang,
            'kelompok' => $request->kelompok[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        $result = $this->settingApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $setting = $this->data_mapel();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'mapel' => $setting
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->settingApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $setting = $this->data_mapel();
            return response()->json([
                'mapel' => $setting,
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->settingApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
