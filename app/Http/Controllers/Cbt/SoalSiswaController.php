<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Cbt\DurasiSiswaApi;
use App\ApiService\Cbt\JawabanApi;
use App\ApiService\Cbt\NilaiApi;
use App\ApiService\Cbt\SoalApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SoalSiswaController extends Controller
{
    private $jawabanApi;
    private $soalApi;
    private $durasiApi;
    private $nilaiApi;
    private $bankSoalApi;

    public function __construct()
    {
        $this->jawabanApi = new JawabanApi();
        $this->soalApi = new SoalApi();
        $this->durasiApi = new DurasiSiswaApi();
        $this->nilaiApi = new NilaiApi();
        $this->bankSoalApi = new BankSoalApi();
    }

    public function store(Request $request)
    {
        // dd($request);
        $data_soal = json_decode($request['data']);
        $bank = $this->bankSoalApi->get_by_id($data_soal->id_bank);
        // dd($bank);
        $bank = $bank['body']['data'];
        // dd($data_soal);
        $jawaban =  $this->jawabanApi->get_by_id($data_soal->id_soal_siswa);
        // dd($jawaban);
        $jawaban = $jawaban['body']['data'];


        if ($data_soal->jenis == 1) {
            $js = strtolower($data_soal->jawaban_alias);
            $point_soal = $bank['bobot_pilgan'] / $bank['pilgan_aktif'];
        } elseif ($data_soal->jenis == 2) {
            $js = implode('###', $data_soal->jawaban_siswa);
            $point_soal = $bank['bobot_kompleks'] / $bank['kompleks_aktif'];
        } elseif ($data_soal->jenis == 4) {
            $js = $data_soal->jawaban_siswa;
            $point_soal = $bank['bobot_isian'] / $bank['isian_aktif'];
        } elseif ($data_soal->jenis == 5) {
            $js = $data_soal->jawaban_siswa;
            $point_soal = $bank['bobot_essay'] / $bank['essay_aktif'];
        }
        // dd('jawaban benar = ' . $jawaban['jawaban_benar'] . ', === jawban siswa = ' . $js);
        $data = array(
            'id' => $data_soal->id_soal_siswa,
            'id_bank' => $data_soal->id_bank,
            'id_jadwal' => $data_soal->id_jadwal,
            'id_soal' => $data_soal->id_soal,
            'id_kelas_siswa' => $data_soal->id_siswa,
            'tipe' => $data_soal->jenis,
            'no_soal_alias' => $data_soal->no_soal_alias,
            'jawaban_alias' => strtolower($data_soal->jawaban_alias),
            'jawaban_siswa' => strtolower($js),
            'opsi_alias_a' => $jawaban['opsi_alias_a'],
            'opsi_alias_b' => $jawaban['opsi_alias_b'],
            'opsi_alias_c' => $jawaban['opsi_alias_c'],
            'opsi_alias_d' => $jawaban['opsi_alias_d'],
            'opsi_alias_e' => $jawaban['opsi_alias_e'],
            'jawaban_benar' => $jawaban['jawaban_benar'],
            'soal_akhir' => $jawaban['soal_akhir'],
            'point_soal' => $point_soal,
            'nilai_koreksi' => strtolower($jawaban['jawaban_benar']) != strtolower($js) ? 0 : $point_soal,
            'nilai_otomatis' => strtolower($jawaban['jawaban_benar']) != strtolower($js) ? 0 : $point_soal,
            'id_sekolah' => session('id_sekolah'),

        );
        // dd($data);
        $result = $this->jawabanApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'soal_terjawab' => $result['body']['data']['terjawab'],
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'soal_terjawab' => null,
            ]);
        }
        // dd($result);
    }

    public function get_data(Request $r)
    {
        // dd($r);
        $jawaban = $this->jawabanApi->by_gabungan($r['jadwal'], $r['bank'], $r['nomor'], $r['siswa']);
        $jawaban = $jawaban['body']['data'];
        if ($r['elapsed'] != "0") {
            // dd($r['elapsed']);
            $jawaban['durasi']['lama_ujian'] = $r['elapsed'];
            // dd($durasi);
            $update = $this->durasiApi->update(json_encode($jawaban['durasi']));
        }


        if ($jawaban['detail_jawaban']['tipe'] == 1) {
            $alphabet = range('A', 'Z');
            $soal_pg = [];
            for ($i = 0; $i < $jawaban['detail_jawaban']['jumlah_opsi']; $i++) {
                // dd(strtolower($jawaban['jawaban_alias']));
                $checked = '';
                if (strtolower($alphabet[$i]) == strtolower($jawaban['detail_jawaban']['jawaban_alias'])) {
                    $checked = 'checked';
                }
                $soal_pg[] = array(
                    'value' => $jawaban['detail_jawaban']['opsi_alias_' . strtolower($alphabet[$i])],
                    'valAlias' => strtolower($alphabet[$i]),
                    'opsi' => $jawaban['detail_jawaban']['detail_soal']['opsi_' . strtolower($alphabet[$i])],
                    'checked' => $checked
                );
            }
            $jawaban['detail_jawaban']['soal_opsi'] = $soal_pg;
            // dd($jawaban);
        } elseif ($jawaban['detail_jawaban']['tipe'] == 2) {
            $soal_option = $jawaban['detail_jawaban']['detail_soal']['opsi_a'];
            // dd($soal_option);
            $soal_option = json_decode($soal_option, true);
            $soal = [];
            foreach ($soal_option as $s) {
                $check = '';
                foreach (explode('###', $jawaban['detail_jawaban']['jawaban_siswa']) as $jwbn) {
                    if (strtolower($jwbn) ==  strtolower($s['jawaban'])) {
                        $check = 'checked';
                    }
                }
                $soal[] = array(
                    'jawaban' => $s['jawaban'],
                    'checked' => $check,
                    'isian' => $s['isian'],
                );
            }
            $jawaban['detail_jawaban']['soal_opsi'] = $soal;
        }
        $html = '<div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">';
        $nomor = 1;
        foreach ($jawaban['by_bank_siswa'] as $ss) {
            // dd($ss);
            if ($ss['jawaban_siswa'] == '' || $ss['jawaban_siswa'] == null) {
                $kelas = 'btn-outline-secondary';
                $badge = '<div id="badge' . $ss['no_soal_alias'] . '" class="badge badge-pill badge-danger border border-light" style="font-size:12pt; width: 30px; height: 30px;  margin-left: 30px;">✘</div>';
            } else {
                $badge = '<div id="badge' . $ss['no_soal_alias'] . '" class="badge badge-pill badge-success border border-dark" style="font-size:12pt; width: 30px; height: 30px;  margin-left: 30px;">✓</div>';
                $kelas = 'btn-primary';
            }
            $html .= '<button id="btn' . $ss['no_soal_alias'] . '" type="button" class="btn ' . $kelas . ' m-1" data-nomorsoal="' . $ss['no_soal_alias'] . '" data-idsoal="' . $ss['id_soal'] . '" data-jenis="' . $ss['tipe'] . '" onclick="loadSoal(this)"><b>' . $nomor . '</b>
                <div id="box' . $ss['no_soal_alias'] . '" style="float: right">' . $badge . '</div>
            </button>
            ';
            $nomor++;
        }
        $html .= '</div></div>';
        $jawaban['detail_jawaban']['soal_modal'] = $html;
        // dd($durasi);
        $jawaban['detail_jawaban']['id_durasi'] = $jawaban['durasi']['id'];
        $waktu = $jawaban['durasi']['lama_ujian'] != null ? $jawaban['durasi']['lama_ujian'] : '00:00:00';
        $elaps =  $r['elapsed'] != "0" ? $r['elapsed'] : $waktu;
        $jawaban['detail_jawaban']['elapsed'] = $elaps;
        $a =  strtotime($elaps);
        $b =  strtotime($jawaban['durasi']['durasi_ujian']);
        $diff = $b - $a;
        $x = gmdate("H:i:s", $diff);
        $jawaban['detail_jawaban']['timer'] = empty($r['timer']) ? $x  : $r['timer'];
        $jawaban['detail_jawaban']['soal_terjawab'] = $jawaban['detail_jawaban']['jawaban_dijawab'];
        return response()->json($jawaban['detail_jawaban']);
    }

    public function selesai(Request $r)
    {
        $durasi = $this->durasiApi->jadwal_siswa($r['siswa'], $r['jadwal']);
        $durasi = $durasi['body']['data'];
        $durasi['status_ujian'] = 2;
        $durasi['selesai'] = date('Y-m-d H:i:s');
        $result = $this->durasiApi->update(json_encode($durasi));
        if ($result['code'] == 200) {

            $hasil = $this->jawabanApi->bank_siswa($r['bank'], $r['siswa']);
            // dd($hasil);
            $hasil = $hasil['body']['data'];
            $nilai_pg = 0;
            $nilai_kp = 0;
            $nilai_isian = 0;
            $nilai_essay = 0;
            $jumlah = 0;
            foreach ($hasil as $hs) {
                if ($hs['tipe'] == 1 && $hs['jawaban_benar'] == $hs['jawaban_alias']) {
                    $jumlah += 1;
                }
                if ($hs['tipe'] == 1) {
                    $nilai_pg += $hs['nilai_koreksi'];
                }
                if ($hs['tipe'] == 2) {
                    $nilai_kp += $hs['nilai_koreksi'];
                }
                if ($hs['tipe'] == 4) {
                    $nilai_isian += $hs['nilai_koreksi'];
                }
                if ($hs['tipe'] == 5) {
                    $nilai_essay += $hs['nilai_koreksi'];
                }
            }
            $data = array(
                'id_kelas_siswa' => $r['siswa'],
                'id_jadwal' => $r['jadwal'],
                'pilgan_benar' => $jumlah,
                'pilgan_nilai' => round($nilai_pg, 1),
                'kompleks_nilai' => round($nilai_kp, 1),
                'isian_nilai' => round($nilai_isian, 1),
                'essai_nilai' => round($nilai_essay, 1),
                'id_sekolah' => session('id_sekolah')
            );
            $res_nilai = $this->nilaiApi->create(json_encode($data));
            // dd($res_nilai);
            if ($res_nilai['code'] == 200) {
                return response()->json([
                    'message' => $res_nilai['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil',
                ]);
            } else {
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal',
                ]);
            }
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function save(Request $request)
    {
        // dd($request);
        $hasil_jawaban = json_decode($request['data'], true);
        $durasi = $this->durasiApi->jadwal_siswa($request['siswa'], $request['jadwal']);
        $durasi = $durasi['body']['data'];
        // dd($durasi);
        $durasi['selesai'] = date('Y-m-d H:i:s');
        $durasi['status_ujian'] = 2;
        $update = $this->durasiApi->update(json_encode($durasi));
        if (!empty($hasil_jawaban)) {
            $jawaban = $this->jawabanApi->by_detail_soal($request['jadwal'], $request['bank'], $hasil_jawaban['no_soal_alias'], $request['siswa']);
            $jawaban = $jawaban['body']['data'];
            // dd($jawaban);
            $data = $jawaban;
            $data['jawaban_siswa'] = $hasil_jawaban->jawaban_siswa;
            $result = $this->jawabanApi->update(json_encode($data));
            if ($result['code'] == 200) {
                $hasil = $this->jawabanApi->bank_siswa($request['bank'], $request['siswa']);
                // dd($hasil);
                $hasil = $hasil['body']['data'];
                $jumlah = 0;
                $nilai_pg = 0;
                $nilai_kp = 0;
                $nilai_isian = 0;
                $nilai_essay = 0;
                foreach ($hasil as $hs) {
                    if ($hs['tipe'] == 1 && $hs['jawaban_benar'] == $hs['jawaban_alias']) {
                        $jumlah += 1;
                    }
                    if ($hs['tipe'] == 1) {
                        $nilai_pg += $hs['nilai_koreksi'];
                    }
                    if ($hs['tipe'] == 2) {
                        $nilai_kp += $hs['nilai_koreksi'];
                    }
                    if ($hs['tipe'] == 4) {
                        $nilai_isian += $hs['nilai_koreksi'];
                    }
                    if ($hs['tipe'] == 4) {
                        $nilai_essay += $hs['nilai_koreksi'];
                    }
                }
                $data = array(
                    'id_kelas_siswa' => $request['siswa'],
                    'id_jadwal' => $request['jadwal'],
                    'pilgan_benar' => $jumlah,
                    'pilgan_nilai' => round($nilai_pg, 1),
                    'kompleks_nilai' => round($nilai_kp, 1),
                    'isian_nilai' => round($nilai_isian, 1),
                    'essai_nilai' => round($nilai_essay, 1),
                    'id_sekolah' => session('id_sekolah')
                );
                $res_nilai = $this->nilaiApi->create(json_encode($data));
                if ($res_nilai['code'] == 200) {
                    return response()->json([
                        'message' => $res_nilai['body']['message'],
                        'icon'  => 'success',
                        'status' => 'berhasil',
                        'value' => 'habis'
                    ]);
                } else {
                    return response()->json([
                        'message' => $result['body']['message'],
                        'icon'  => 'error',
                        'status' => 'gagal',
                        'value' => 'habis'
                    ]);
                }
            } else {
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal',
                    'value' => 'habis'
                ]);
            }
        } else {
            return response()->json([
                'icon'  => 'success',
                'status' => 'berhasil',
                'value' => 'habis'
            ]);
        }
    }
}
