<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\RuangApi;
use App\ApiService\Cbt\SesiSiswaApi;
use App\ApiService\Cbt\SessiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SesiSiswaController extends Controller
{
    private $sesiSiswaApi;
    private $ruangApi;
    private $sesiApi;

    public function __construct()
    {
        $this->sesiSiswaApi = new SesiSiswaApi();
        $this->ruangApi = new RuangApi();
        $this->sesiApi = new SessiApi();
    }

    public function store(Request $p)
    {
        // dd($p);
        for ($i = 1; $i <= $p['jumlah_siswa']; $i++) {
            $id_siswa = $p['id_siswa_' . $i];
            $id_ruang = $p['id_ruang_' . $i];
            $id_sesi = $p['id_sesi_' . $i];
            $data = array(
                'id_kelas_siswa' => $id_siswa,
                'id_ruang' => $id_ruang,
                'id_sesi' => $id_sesi,
                'id_ta_sm' => session('id_tahun_ajar'),
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->sesiSiswaApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function get_by_rombel(Request $request)
    {
        $s_siswa = $this->sesiSiswaApi->get_by_rombel($request['id_rombel'], session('id_tahun_ajar'));
        // dd($s_siswa);
        $s_siswa = $s_siswa['body']['data'];
        // dd($s_siswa);
        $html = '<form action="javascript:void(0)" id="formSesiSiswa">
        <div class="table-responsive">
        <table class="table table-bordered">
            <tr class="bg-info">
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Ruang</th>
                <th>Sesi</th>
            </tr>';
        if (!empty($s_siswa['sesi_siswa'])) {
            $no = 1;
            foreach ($s_siswa['sesi_siswa'] as $ss) {
                $html .= '<tr>
                <input type="hidden" name="jumlah_siswa" value="' . count($s_siswa['sesi_siswa']) . '">
                    <td>' . $no . '</td>
                    <td>' . $ss['nama'] . '</td>
                    <td>' . $ss['rombel'] . '</td>';
                if ($s_siswa['set_siswa'] != 1) {
                    $html .= '<td>
                    <input type="hidden" name="id_siswa_' . $no . '"
                                                    value="' . $ss['id'] . '">
                        <select class="form-control" name="id_ruang_' . $no . '">
                            <option disabled>Pilih Ruang..</option>';
                    $ruang = $this->ruangApi->by_sekolah();
                    $ruang = $ruang['body']['data'];
                    foreach ($ruang as $rg) {
                        $ruang_check = '';
                        if ($rg['id'] == $ss['id_ruang']) {
                            $ruang_check = 'selected';
                        }
                        $html .= '<option value="' . $rg['id'] . '" ' . $ruang_check . '>' . $rg['kode'] . '</option>';
                    }
                    $html .= '</select>
                    </td>
                    <td>
                    <select class="form-control" name="id_sesi_' . $no . '">
                            <option disabled>Pilih Sesi..</option>';
                    $sesi = $this->sesiApi->by_sekolah();
                    $sesi = $sesi['body']['data'];
                    foreach ($sesi as $ses) {
                        $sesi_check = '';
                        if ($ss['id_sesi'] == $ses['id']) {
                            $sesi_check = 'selected';
                        }
                        $html .= '<option value="' . $ses['id'] . '" ' . $sesi_check . '>' . $ses['nama'] . '</option>';
                    }
                    $html .= '</select>
                    </td>
                </tr>';
                } else {
                    $html .= ' <td>
                    <input type="hidden" name="id_siswa_' . $no . '"
                    value="' . $ss['id'] . '">
                        <select class="form-control" name="id_ruang_' . $no . '">
                            <option disabled>Pilih Ruang..</option>';
                    $ruang = $this->ruangApi->by_sekolah();
                    $ruang = $ruang['body']['data'];
                    foreach ($ruang as $rg) {
                        $html .= '<option value="' . $rg['id'] . '">' . $rg['kode'] . '</option>';
                    }
                    $html .= '</select>
                    </td>
                    <td>
                    <select class="form-control" name="id_sesi_' . $no . '">
                            <option disabled>Pilih Sesi..</option>';
                    $sesi = $this->sesiApi->by_sekolah();
                    $sesi = $sesi['body']['data'];
                    foreach ($sesi as $ss) {
                        $html .= '<option value="' . $ss['id'] . '">' . $ss['nama'] . '</option>';
                    }
                    $html .= '</select>
                    </td>
                </tr>';
                }
                $no++;
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        $html .= '</table></div><button class="btn btn-info pull-right" id="btnAddSesi"><i class="fas fa-save"> Simpan</i></button></form>';

        return response()->json($html);
    }
}
