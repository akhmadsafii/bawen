<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\JenisApi;
use App\ApiService\Cbt\SesiSiswaApi;
use App\ApiService\Cbt\SessiApi;
use App\ApiService\Cbt\SettingPesertaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class PesertaUjianController extends Controller
{
    private $jenisApi;
    private $sesiSiswaApi;
    private $tahunApi;
    private $sesiApi;
    private $pesertaApi;

    public function __construct()
    {
        $this->jenisApi = new JenisApi();
        $this->sesiSiswaApi = new SesiSiswaApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->sesiApi = new SessiApi();
        $this->pesertaApi = new SettingPesertaApi();
    }

    public function index()
    {
        $jenis = $this->jenisApi->by_sekolah();
        // dd($ruang);
        $jenis = $jenis['body']['data'];
        if (!empty($_GET)) {
            if ($_GET['m'] == 'ruang') {
                $sesi_siswa = $this->sesiSiswaApi->by_ruang(session('id_tahun_ajar'));
            } else {
                $sesi_siswa = $this->sesiSiswaApi->by_rombel(session('id_tahun_ajar'));
            }
        } else {
            $sesi_siswa = $this->sesiSiswaApi->by_ruang(session('id_tahun_ajar'));
        }

        $peserta = $this->pesertaApi->by_sekolah(session('id_sekolah'));
        // dd($peserta);
        $peserta = $peserta['body']['data'];
        $sesi_siswa = $sesi_siswa['body']['data'];
        $sesi = $this->sesiApi->by_sekolah(session('id_sekolah'));
        // dd($sesi);
        $sesi = $sesi['body']['data'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        $tahun = $tahun['body']['data'];
        // dd($tahun);

        session()->put('title', 'Data Mata Pelajaran');
        return view('content.cbt.cetak.v_peserta_ujian')->with([
            'template' => 'default', 'jenis' => $jenis, 'sesi' => $sesi,
            'sesi_siswa' => $sesi_siswa, 'tahun' => $tahun, 'peserta' => $peserta
        ]);
    }

    public function logo_kanan(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->pesertaApi->logo_kanan(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function logo_kiri(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->pesertaApi->logo_kiri(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
