<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\NomorPesertaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class NomorPesertaController extends Controller
{
    private $nomorPesertaApi;
    private $rombelApi;
    private $kelasSiswaApi;
    private $tahunApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->nomorPesertaApi = new NomorPesertaApi();
        $this->rombelApi = new RombelApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        $jurusan = $this->jurusanApi->menu();
        // dd($jurusan);
        $jurusan = $jurusan['body']['data'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        session()->put('title', 'Nomor Peserta');
        return view('content.cbt.v_nomor_peserta')->with([
            'template' => 'default', 'tahun' => $tahun,
            'jurusan' => $jurusan
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $siswa_decode = json_decode($request['siswa'], true);
        // dd($siswa_decode);
        $siswa = [];
        foreach ($siswa_decode as $sw) {
            $siswa[] = array(
                'id_kelas_siswa' => $sw['id'],
                'nomor_peserta' => $sw['nomor'],
                'id_ta_sm' => session('id_tahun_ajar'),
                'id_sekolah' => session('id_sekolah'),
            );
        }
        $data = array(
            'forms' => $siswa
        );
        $result = $this->nomorPesertaApi->create_many(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function show_data(Request $request)
    {
        $data = array(
            'rombel' => implode('###', $request['id_rombel']),
            'tahun' => session('tahun'),
        );
        // dd($data);
        $siswa = $this->nomorPesertaApi->get_data(json_encode($data));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        return response()->json($siswa);
    }

    public function download_template()
    {
        // dd($_GET);
        $download = $this->nomorPesertaApi->download_template($_GET['rombel'], session('tahun'));
        // dd($download);
        return redirect()->to($download);
    }

    public function reset_nomor(Request $request)
    {
        // dd($request);
        $data = array(
            'rombel' => implode('###', $request['rombel']),
            'tahun' => session('tahun'),
        );
        $delete = $this->nomorPesertaApi->remove_data(json_encode($data));
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        // dd(session()->all());
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->nomorPesertaApi->upload_excel(json_encode($data), session('id_tahun_ajar'));
        File::delete($path);
        // dd($result);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
