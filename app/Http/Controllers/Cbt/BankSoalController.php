<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BankSoalController extends Controller
{
    private $bankApi;
    private $mapelApi;
    private $jurusanApi;
    private $guruApi;
    private $kelasApi;
    private $rombelApi;
    private $grApi;

    public function __construct()
    {
        $this->bankApi = new BankSoalApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
        $this->guruApi = new GuruPelajaranApi();
        $this->grApi = new GuruApi();
        $this->rombelApi = new RombelApi();
    }

    public function index()
    {
        // dd(session('id_tahun_ajar'));
        if (session('role') == 'guru') {
            $soal = $this->bankApi->by_guru(session('id'), session('id_tahun_ajar'));
        } else {
            if (!empty($_GET)) {
                if ($_GET['type'] == 1) {
                    $soal = $this->bankApi->by_guru($_GET['id'], session('id_tahun_ajar'));
                } elseif ($_GET['type'] == 2) {
                    $soal = $this->bankApi->by_mapel($_GET['id']);
                } else {
                    $soal = $this->bankApi->by_rombel($_GET['id'], session('id_tahun_ajar'));
                }
            } else {
                $soal = $this->bankApi->by_sekolah(session('id_tahun_ajar'));
                // dd($soal);
            }
        }
        // dd($soal);
        $soal = $soal['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $guru = $this->grApi->get_by_sekolah(session('id_sekolah'));
        // dd($guru);
        $guru = $guru['body']['data'];
        session()->put('title', 'Bank Soal');
        $template = 'default';
        return view('content.cbt.bank.v_bank_soal')->with([
            'template' => $template, 'soal' => $soal,
            'rombel' => $rombel, 'mapel' => $mapel, 'guru' => $guru
        ]);
    }

    public function create()
    {
        session()->put('title', 'Buat Bank Soal');
        $template = 'default';
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $bank = [];
        $guru = [];
        $kelas = [];
        $rombel = [];
        if (isset($_GET["code"])) {
            $bank = $this->bankApi->get_by_id(Help::decode($_GET['code']));
            // dd($bank);
            $bank = $bank['body']['data'];
            $guru = $this->guruApi->get_by_mapel($bank['id_mapel']);
            $guru = $guru['body']['data'];
            $kelas = $this->kelasApi->get_by_jurusan($bank['id_jurusan']);
            $kelas = $kelas['body']['data'];
            $rombel = $this->rombelApi->get_by_kelas($bank['id_kelas']);
            // dd($rombel);
            $rombel = $rombel['body']['data'];
        }
        return view('content.cbt.bank.v_add_bank_soal')->with([
            'template' => $template, 'mapel' => $mapel, 'kelas' => $kelas,
            'jurusan' => $jurusan, 'bank' => $bank, 'guru' => $guru, 'rombel' => $rombel
        ]);
    }

    public function store(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'kode' => $request['kode'],
            'id_mapel' => $request['mapel'],
            'id_kelas' => $request['id_kelas'],
            'id_guru' => implode('###', $request['id_guru']),
            'id_rombel' => implode('###', $request['id_rombel']),
            'id_ta_sm' => session('id_tahun_ajar'),
            'jml_pilgan' => $request['tampil_pg'],
            'bobot_pilgan' => $request['bobot_pg'],
            'opsi' => $request['opsi'],
            'jml_kompleks' => $request['tampil_komplek'],
            'bobot_kompleks' => $request['bobot_komplek'],
            'jml_jodohkan' => $request['tampil_jodoh'],
            'bobot_jodohkan' => $request['bobot_jodoh'],
            'jml_isian' => $request['tampil_es'],
            'bobot_isian' => $request['bobot_es'],
            'jml_essay' => $request['tampil_uraian'],
            'bobot_essay' => $request['bobot_uraian'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        $result = $this->bankApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail()
    {
        $soal = $this->bankApi->get_by_id(Help::decode($_GET['code']));
        $soal = $soal['body']['data'];
        // dd($soal);
        $template = 'default';
        session()->put('title', 'Detail Soal');
        return view('content.cbt.bank.v_detail_bank_soal')->with(['template' => $template, 'soal' => $soal]);
    }

    public function by_guru(Request $request)
    {
        $bank = $this->bankApi->by_guru($request['id'], session('id_tahun_ajar'));
        $bank = $bank['body']['data'];
        return response()->json($bank);
    }

    public function load_by_guru(Request $request)
    {
        $id_bank = $request['id_bank'];
        $bank  = $this->bankApi->by_guru($request['id_guru'], session('id_tahun_ajar'));
        $bank = $bank['body']['data'];
        $output = '';
        foreach ($bank as $bn) {
            $output .= '<option value="' . $bn['id'] . '" ' . (($bn['id'] == $id_bank) ? 'selected="selected"' : "") . '>' . $bn['kode'] . '</option>';
        }
        return $output;
    }

    public function import()
    {
        $bank = $this->bankApi->get_by_id(Help::decode($_GET['code']));
        $bank = $bank['body']['data'];
        // dd($bank);
        $template = 'default';
        return view('content.cbt.bank.v_import_soal')->with(['template' => $template, 'bank' => $bank]);
    }

    public function delete_many(Request $request)
    {
        foreach (json_decode($request['ids']) as $id) {
            $delete = $this->bankApi->delete($id);
        }
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function copy_soal(Request $request)
    {
        // dd($request);
        $copy = $this->bankApi->copy(json_encode(["id" => $request['id']]));
        if ($copy['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $copy['body']['message'],
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $copy['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
