<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\DurasiSiswaApi;
use App\ApiService\Cbt\HasilApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\SesiSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;

class UjianController extends Controller
{
    private $sesiSiswaApi;
    private $jadwalApi;
    private $tahunApi;
    private $rombelApi;
    private $durasiApi;
    private $hasilApi;

    public function __construct()
    {
        $this->sesiSiswaApi = new SesiSiswaApi();
        $this->jadwalApi = new JadwalApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->rombelApi = new RombelApi();
        $this->durasiApi = new DurasiSiswaApi();
        $this->hasilApi = new HasilApi();
    }

    public function ujian()
    {
        $sesiSiswa = $this->sesiSiswaApi->get_by_siswa(session('id_kelas_siswa'), session('id_tahun_ajar'));
        // dd($sesiSiswa);
        $sesiSiswa = $sesiSiswa['body']['data'];
        $jadwal = $this->jadwalApi->jadwal_by_kelas_siswa(session('id_kelas_siswa'), session('id_tahun_ajar'));
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        $jdwl = [];
        foreach ($jadwal as $jd) {
            $durasi = $this->durasiApi->jadwal_siswa(session('id_kelas_siswa'), $jd['id']);
            // dd($durasi);
            $durasi = $durasi['body']['data'];
            if (empty($durasi)) {
                // dd(date('H:i'));
                if (date('H:i') <= date('H:i', strtotime($jd['sesi_dari'])) && date('H:i') >= date('H:i', strtotime($jd['sesi_selesai']))) {
                    $status = 'MENUNGGU';
                } else {
                    $status = 'KERJAKAN';
                }
            } else {
                if ($durasi['status_ujian'] == 0) {
                    if (date('H:i') <= date('H:i', strtotime($jd['sesi_dari'])) && date('H:i') >= date('H:i', strtotime($jd['sesi_selesai']))) {
                        $status = 'MENUNGGU';
                    } else {
                        $status = 'KERJAKAN';
                    }
                } elseif ($durasi['status_ujian'] == 1) {
                    $status = 'LANJUTKAN';
                } elseif ($durasi['status_ujian'] == 2) {
                    $status = 'SUDAH DIKERJAKAN';
                }
            }

            $jdwl[] = array(
                'id' => $jd['id'],
                'jenis' => $jd['jenis'],
                'status_kerjakan' => $status,
                'kode_bank' => $jd['kode_bank'],
                'sesi_selesai' => $jd['sesi_selesai'],
                'sesi_dari' => $jd['sesi_dari'],
                'durasi_ujian' => $jd['durasi_ujian'],
                'tgl_mulai' => $jd['tgl_mulai'],
                'tgl_selesai' => $jd['tgl_selesai'],
            );
        }
        // dd($jdwl);
        session()->put('title', 'Data Token');
        return view('content.cbt.siswa.v_ujian')->with(['sesi_siswa' => $sesiSiswa, 'jadwal' => $jdwl]);
    }

    public function konfirmasi()
    {
        $jadwal = $this->jadwalApi->get_by_id(Help::decode($_GET['key']));
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        $tahun = $tahun['body']['data'];
        $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        return view('content.cbt.siswa.v_konfirmasi')->with(['jadwal' => $jadwal, 'tahun' => $tahun]);
    }

    public function soal()
    {
        // dd(session()->all());
        $jadwal = $this->jadwalApi->get_by_id(Help::decode($_GET['kode']));
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        return view('content.cbt.siswa.soal.v_soal')->with(['jadwal' => $jadwal]);
    }

    public function hasil()
    {
        // dd("tes");
        $hasil = $this->hasilApi->hasil_siswa(session('id_kelas_siswa'), session('id_tahun_ajar'));
        // dd($hasil);
        $hasil = $hasil['body']['data'];
        return view('content.cbt.siswa.v_hasil')->with(['hasil' => $hasil]);
    }
}
