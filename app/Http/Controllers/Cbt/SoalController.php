<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\SoalApi;
use App\ApiService\Master\MapelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SoalController extends Controller
{
    private $soalApi;
    private $bankApi;
    private $jadwalApi;

    public function __construct()
    {
        $this->soalApi = new SoalApi();
        $this->bankApi = new BankSoalApi();
        $this->jadwalApi = new JadwalApi();
    }

    public function index()
    {

        $template = session('template');
        if (session('role') == 'admin-cbt') {
            $template = 'default';
        }
        session()->put('title', 'Data Soal');
        return view('content.cbt.v_soal')->with(['template' => $template]);
    }

    public function add()
    {
        $bank = $this->bankApi->get_by_id(Help::decode($_GET['bank']));
        // dd($bank);
        $bank = $bank['body']['data'];
        $soal = $this->soalApi->get_by_jenis_bank(Help::decode($_GET['bank']), $_GET['tab']);
        // dd($soal);
        $soal = $soal['body']['data'];
        session()->put('title', 'Buat Soal');
        return view('content.cbt.soal.v_add_soal')->with(['template' => 'default', 'bank' => $bank, 'soal' => $soal]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $alphabet = range('A', 'Z');
        $opt_jwb = [];
        for ($i = 0; $i < $request['jumlah_pilgan']; $i++) {
            // $jawaban1 = $request['jawaban2_' . $alphabet[$i]];
            $opt_jwb[] = array(
                'jawaban' => $alphabet[$i],
                'isian' => $request['jawaban2_' . strtolower($alphabet[$i])]
            );
        }
        $jawaban_a = $request['jawaban_a'];
        $id_bank = Help::decode($request['bank_id']);
        if ($request['jenis'] == 1) {
            $jawaban = $request['jawaban_pg'];
        } else if ($request['jenis'] == 2) {
            $arrNewSku = array();
            $incI = 0;
            foreach ($opt_jwb as $arrData) {
                $arrNewSku[$incI]['jawaban'] = $arrData['jawaban'];
                $arrNewSku[$incI]['isian'] = $arrData['isian'];
                $incI++;
            }
            $jawaban_a = json_encode($arrNewSku);
            $jawaban = null;
            if ($request['jawaban_benar_pg2']) {
                $jawaban = implode('###', $request['jawaban_benar_pg2']);
            }
        } else if ($request['jenis'] == 3) {
            $jawaban_a = json_encode($opt_jwb);
            $jawaban = null;
            if ($request['jawaban_benar_pg2']) {
                $jawaban = implode('###', $request['jawaban_benar_pg2']);
            }
        } else if ($request['jenis'] == 4) {
            $jawaban = null;
            if ($request['jawaban_isian']) {
                $jawaban = $request['jawaban_isian'];
            }
        } else {
            $jawaban = null;
            if ($request['jawaban_essai']) {
                $jawaban = $request['jawaban_essai'];
            }
        }
        $data = array(
            'id' => $request['id_soal'],
            'id_bank' => $id_bank,
            'nomor_soal' => $request['nomor_soal'],
            'tipe' => $request['jenis'],
            'soal' => $request['soal'],
            'jawaban' => $jawaban,
            'opsi_a' => $jawaban_a,
            'opsi_b' => $request['jawaban_b'],
            'opsi_c' => $request['jawaban_c'],
            'opsi_d' => $request['jawaban_d'],
            'opsi_e' => $request['jawaban_e'],
            'id_sekolah' => session('id_sekolah'),
            'id_ta_sm' => session('id_tahun_ajar')
        );
        // dd($data);
        $result = $this->soalApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function tampil_soal(Request $request)
    {
        // dd($request);
        $nomor_soal = $request['nomor_soal'];
        $tipe = $request['tipe'];
        $soal = $this->soalApi->get_by_id($request['id']);
        // dd($soal);
        $soal = $soal['body']['data'];
        // dd($soal);
        $bank = $this->bankApi->get_by_id($soal['id_bank']);
        $bank = $bank['body']['data'];
        // dd(json_decode($soal['opsi_a']));
        $html = '';
        if ($tipe == 1) {
            $html = '<div class="card my-shadow my-3">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h5 class="box-title mr-b-0">Soal PG Nomor: ' . $nomor_soal . '</h5>
                                </div>
                                <div class="col-md-4">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-info simpanSoal">
                                            <i class="fa fa-plus mr-1"></i>Simpan
                                        </button>
                                        <button type="button" class="btn btn-danger" id="hapus-soal" data-id="' . $soal['id'] . '">
                                            <i class="fa fa-trash mr-1"></i>Hapus
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="soal-area mt-4">
                                <div class="row">
                                    <div class="col-md-6" id="soal-area">
                                        <div class="form-group">
                                            <label>Soal</label>
                                            <textarea name="soal" id="" cols="30" rows="10"
                                                class="editor">' . $soal['soal'] . '</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="jawaban-area">
                                        <div id="root-opsi-pg" class="">';
            for ($i = 1; $i <= $bank['opsi']; $i++) {
                $html .= '<div class="mb-3 ml-3">
                                                <label>Jawaban
                                                   ' . Help::alphabet($i) . '</label>
                                                <textarea name="jawaban_' . strtolower(Help::alphabet($i)) . '" cols="30" rows="10"
                                                    class="editor">' . $soal['opsi_' . strtolower(Help::alphabet($i))] . '</textarea>
                                            </div>';
            }
            $html .= '</div>

                                            <div class="mt-1 ml-3" id="jawaban-benar">
                                                <label>Jawaban Benar</label>
                                                <div id="root-jawaban-pg" class="">
                                                    <select name="jawaban_pg" id="jawaban" required class="form-control">
                                                        <option value="" selected="selected">Pilih Jawaban Benar :</option>';


            for ($b = 1; $b <= $bank['opsi']; $b++) {
                $select = '';
                if ($soal['jawaban'] == strtolower(Help::alphabet($b))) {
                    $select = 'selected';
                }
                $html .= '<option value="' . strtolower(Help::alphabet($b)) . '" ' . $select . '>
                                                    ' . Help::alphabet($b) . '</option>';
            }
            $html .= '</select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        } elseif ($tipe == 2) {
            $jumlah_pilgand =  1;
            // dd($soal['opsi_a']);
            // dd(json_decode($soal['opsi_a'], true));
            if (count(json_decode($soal['opsi_a'])) != 0) {
                $jumlah_pilgand =  count(json_decode($soal['opsi_a']));
            }
            $html .= '
            <div class="card my-shadow my-3">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="box-title mr-b-0">Soal Kompleks Nomor: ' . $soal['nomor_soal'] . '</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-info simpanSoal">
                                                <i class="fa fa-plus mr-1"></i>Simpan
                                            </button>
                                            <button type="button" class="btn btn-danger" id="hapus-soal" data-id="' . $soal['id'] . '">
                                                <i class="fa fa-trash mr-1"></i>Hapus
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="soal-area mt-4">
                                    <div class="row">
                                        <div class="col-md-6" id="soal-area">
                                            <div class="form-group">
                                                <label>Soal</label>
                                                <textarea name="soal" id="" cols="30" rows="10"
                                                    class="editor">' . $soal['soal'] . '</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6" id="jawaban-area">
                                        <input type="hidden" id="jumlah_pilgan" name="jumlah_pilgan" value="' . $jumlah_pilgand . '"';
            if (empty(json_decode($soal['opsi_a']))) {
                $html .= '<div class="pg-kompleks">
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <label>Jawaban A</label>
                                </div>
                                <div class="col-6 text-right d-flex justify-content-end"> <b>Jawaban
                                        benar</b> <input class="check-pg2" type="checkbox"
                                        style="width: 24px; height: 24px; margin-left: 8px;"
                                        name="jawaban_benar_pg2[]"  value="">
                                </div>
                                <div class="col-md-12">
                                    <textarea name="jawaban2_a" id="" cols="30" rows="10"
                                        class="editor"></textarea>
                                </div>
                            </div>';
            } else {
                foreach (json_decode($soal['opsi_a']) as $jwb) {
                    $check = '';
                    foreach (explode('###', $soal['jawaban']) as $jwbn) {
                        if ($jwbn ==  strtolower($jwb->jawaban)) {
                            $check = 'checked';
                        }
                    }
                    $html .= '<div class="pg-kompleks">
                                    <div class="row mb-3">
                                        <div class="col-md-6">
                                            <label>Jawaban ' . $jwb->jawaban . '</label>
                                        </div>
                                        <div class="col-6 text-right d-flex justify-content-end"> <b>Jawaban
                                                benar</b> <input class="check-pg2" type="checkbox"
                                                style="width: 24px; height: 24px; margin-left: 8px;"
                                                name="jawaban_benar_pg2[]" ' . $check . '  value="' . strtolower($jwb->jawaban) . '">
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="jawaban2_' . strtolower($jwb->jawaban) . '" id="" cols="30" rows="10"
                                                class="editor">' . $jwb->isian . '</textarea>
                                        </div>
                                    </div>';
                }
            }

            $html .= '<div class="addKompleks"></div>
                                            <div class="row my-2">
                                                <div class="col-md-12">
                                                    <button id="tambah-jawaban-pg2" onclick="tambahData()" type="button"
                                                        class="btn btn-success">Tambah
                                                        Jawaban</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            ';
        } elseif ($tipe == 3) {
            # code...
        } elseif ($tipe == 4) {
            $html .= '
            <div class="card my-shadow my-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="box-title mr-b-0">Soal Isian Nomor: ' . $soal['nomor_soal'] . '</h5>
                            </div>
                            <div class="col-md-4">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-info simpanSoal">
                                        <i class="fa fa-plus mr-1"></i>Simpan
                                    </button>
                                    <button type="button" class="btn btn-danger" id="hapus-soal" data-id="' . $soal['id'] . '">
                                        <i class="fa fa-trash mr-1"></i>Hapus
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="soal-area mt-4">
                            <div class="row">
                                <div class="col-md-6" id="soal-area">
                                    <div class="form-group">
                                        <label>Soal</label>
                                        <textarea name="soal" id="" cols="30" rows="10"
                                            class="editor">' . $soal['soal'] . '</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="pg-kompleks">
                                        <div class="row mb-3">
                                            <label>Jawaban Benar</label>
                                            <textarea name="jawaban_isian" id="" rows="3" class="form-control">' . $soal['jawaban'] . '</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        } else {
            $html .= '<div class="card my-shadow my-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="box-title mr-b-0">Soal Essai Nomor: ' . $soal['nomor_soal'] . '</h5>
                    </div>
                    <div class="col-md-4">
                        <div class="float-right">
                            <button type="submit" class="btn btn-info simpanSoal">
                                <i class="fa fa-plus mr-1"></i>Simpan
                            </button>
                            <button type="button" class="btn btn-danger" id="hapus-soal" data-id="' . $soal['id'] . '">
                                <i class="fa fa-trash mr-1"></i>Hapus
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="soal-area mt-4">
                    <div class="row">
                        <div class="col-md-6" id="soal-area">
                            <div class="form-group">
                                <label>Soal</label>
                                <textarea name="soal" id="" cols="30" rows="10"
                                    class="editor">' . $soal['soal'] . '</textarea>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="pg-kompleks">
                                <div class="row mb-3">
                                    <label>Jawaban Benar</label>
                                    <textarea name="jawaban_essai" id="" rows="3" class="form-control editor">' . $soal['jawaban'] . '</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        }

        return $html;
    }

    public function update_tampil(Request $request)
    {
        for ($i = 1; $i <= $request['total_soal']; $i++) {
            $id_soal = $request['id_soal_' . $i];
            $status_soal = !$request['status_soal_' . $i] ? 0 : 1;

            $data = array(
                'id' => $id_soal,
                'status_tampil' => $status_soal,
            );
            $update = $this->soalApi->update_tampil(json_encode($data));
        }
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soal_tampil(Request $r)
    {
        // dd($r);
        $jadwal = $this->jadwalApi->get_by_id($r['jadwal']);
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        $soal = $this->soalApi->by_bank($r['bank']);
        // dd($show_soal);
        $soal = $soal['body']['data'];
        $list = [];
        $no = 1;
        foreach ($soal as $so) {
            $so['nomor'] = $no++;
            $list[] = $so;
        }
        return response()->json($output);
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->soalApi->upload_excel(json_encode($data), $request['id_bank']);
        // dd($result);
        File::delete($path);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $r)
    {
        $delete = $this->soalApi->delete($r['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
