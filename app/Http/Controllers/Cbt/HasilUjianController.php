<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\HasilApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\JawabanApi;
use App\ApiService\Cbt\NilaiApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HasilUjianController extends Controller
{
    private $rombelApi;
    private $hasilApi;
    private $jadwalApi;
    private $jawabanApi;
    private $tahunApi;
    private $nilaiApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->hasilApi = new HasilApi();
        $this->jadwalApi = new JadwalApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->jawabanApi = new JawabanApi();
        $this->nilaiApi = new NilaiApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        session()->put('title', 'Status Siswa');
        // dd(request()->getRequestUri());
        session()->put('url_back', request()->getRequestUri());
        // dd(session()->all());
        $kelas = (isset($_GET["kelas"])) ? $_GET["kelas"] : null;
        $jadwal = (isset($_GET["jadwal"])) ? $_GET["jadwal"] : null;
        $hasil = [];
        $data_jadwal = [];
        $nilai_jadwal = 0;
        if ($kelas != null && $jadwal != null) {
            $nilai_jadwal = 1;
            $hasil = $this->hasilApi->filter_hasil($kelas, $jadwal, session('id_tahun_ajar'));
            // dd($hasil);
            $hasil = $hasil['body']['data'];
            $data_jadwal = $this->jadwalApi->get_by_id($jadwal);
            $data_jadwal = $data_jadwal['body']['data'];
            $dr = $this->rombelApi->get_by_id($kelas);
            $dr = $dr['body']['data'];
            $data_jadwal['rombel'] = $dr['nama'];
        }
        return view('content.cbt.v_hasil_ujian')->with([
            'template' => 'default', 'jurusan' => $jurusan, 'hasil' => $hasil,
            'jadwal' => $data_jadwal, 'nilai_jadwal' => $nilai_jadwal
        ]);
    }

    public function detail()
    {
        // dd("ping");
        $detail = $this->hasilApi->detail_hasil($_GET['siswa'], $_GET['jadwal'], session('id_tahun_ajar'));
        $detail = $detail['body']['data'];
        // dd($detail);
        $nilai = $this->nilaiApi->nilai_by_siswa($_GET['siswa'], $_GET['jadwal']);
        // dd($nilai);
        $nilai = $nilai['body']['data'];
        $hasil = $this->hasilApi->detail_jawaban($_GET['siswa'], $_GET['jadwal'], session('id_tahun_ajar'));
        // dd($hasil);
        $hasil = $hasil['body']['data'];
        // dd($hasil);
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        return view('content.cbt.v_detail_hasil')->with(['template' => 'default', 'detail' => $detail, 'hasil' => $hasil, 'tahun' => $tahun, 'nilai' => $nilai]);
    }

    public function simpan(Request $request)
    {
        // dd($request);
        $nilai = json_decode($request['nilai']);
        // dd($nilai);
        $total = 0;
        foreach ($nilai as $nl) {
            $in = array(
                'id_soal' => $nl->id_soal,
                'id_kelas_siswa' => $request['siswa'],
                'id_jadwal' => $request['jadwal'],
                'nilai_koreksi' => $nl->koreksi
            );
            $total += $nl->koreksi;
            $result = $this->jawabanApi->update_koreksi(json_encode($in));
        }
        if ($result['code'] == 200) {
            $data_nilai = array(
                'id' => $request['id_nilai'],
                'jenis' => $request['jenis'],
                'nilai' => $total,
            );
            $result_nilai = $this->nilaiApi->update_nilai(json_encode($data_nilai));
            if ($result_nilai['code'] == 200) {
                return response()->json([
                    'message' => $result_nilai['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil',
                ]);
            } else {
                return response()->json([
                    'message' => $result_nilai['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal',
                ]);
            }
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update_koreksi(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'koreksi' => 1,
        );
        $result = $this->nilaiApi->update_koreksi(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }
}
