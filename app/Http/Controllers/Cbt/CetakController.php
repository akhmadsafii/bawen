<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\SettingKartuApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class CetakController extends Controller
{
    private $rombelApi;
    private $kartuApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->kartuApi = new SettingKartuApi();
    }

    public function index()
    {
        session()->put('title', 'Data Mata Pelajaran');
        return view('content.cbt.cetak.v_cetak')->with(['template' => 'default']);
    }

    public function kartu_peserta()
    {
        $kartu = $this->kartuApi->by_sekolah(session('id_sekolah'));
        $kartu = $kartu['body']['data'];
        // dd($kartu);
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Setting Kartu');
        return view('content.cbt.cetak.v_kartu_peserta')->with(['template' => 'default', 'rombel' => $rombel, 'kartu' => $kartu]);
    }

    public function save_kartu(Request $request)
    {
        // dd($request);
        $data = array(
            'kepsek' => $request['kepsek'],
            'kabupaten' => $request['kabupaten'],
            'header1' => $request['header_1'],
            'header2' => $request['header_2'],
            'header3' => $request['header_3'],
            'header4' => $request['header_4'],
            'tanggal' => $request['tanggal'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->kartuApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_logo_kiri(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->kartuApi->logo_kiri(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_logo_kanan(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->kartuApi->logo_kanan(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_ttd(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->kartuApi->logo_ttd(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_stempel(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->kartuApi->stempel(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function cetak()
    {
        $cetak = $this->kartuApi->cetak_rombel($_GET['rombel'], session('tahun'), session('id_tahun_ajar'));
        // dd($cetak);
        $cetak = $cetak['body']['data'];
        return response()->json($cetak);
    }
}
