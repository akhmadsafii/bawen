<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnalisaController extends Controller
{
    private $tahunApi;
    private $jadwalApi;

    public function __construct()
    {
        $this->tahunApi = new TahunAjaranApi();
        $this->jadwalApi = new JadwalApi();
    }

    public function index()
    {
        $tahun = $this->tahunApi->get_by_sekolah();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        $jadwal = $this->jadwalApi->by_sekolah();
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        session()->put('title', 'Status Siswa');
        return view('content.cbt.v_analisa_soal')->with(['template' => 'default', 'tahun' => $tahun, 'jadwal' => $jadwal]);
    }

    public  function get_rombel(Request $request)
    {
        $rombel = $this->jadwalApi->get_rombel($request['id_jadwal']);
        $rombel = $rombel['body']['data'];
        return response()->json($rombel);
    }
}
