<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\DaftarHadirApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\RuangApi;
use App\ApiService\Cbt\SessiApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class DaftarHadirController extends Controller
{
    private $hadirApi;
    private $ruangApi;
    private $sesiApi;
    private $jadwalApi;
    private $rombelApi;

    public function __construct()
    {
        $this->hadirApi = new DaftarHadirApi();
        $this->ruangApi = new RuangApi();
        $this->sesiApi = new SessiApi();
        $this->rombelApi = new RombelApi();
        $this->jadwalApi = new JadwalApi();
    }

    public function index()
    {
        $ruang = $this->ruangApi->by_sekolah();
        // dd($ruang);
        $ruang = $ruang['body']['data'];
        $sesi = $this->sesiApi->by_sekolah();
        $sesi = $sesi['body']['data'];
        $jadwal = $this->jadwalApi->by_sekolah();
        $jadwal = $jadwal['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $hadir = $this->hadirApi->by_sekolah(session('id_sekolah'));
        // dd($hadir);
        $hadir = $hadir['body']['data'];
        // dd($hadir);
        session()->put('title', 'Data Mata Pelajaran');
        return view('content.cbt.cetak.v_kehadiran')->with(['template' => 'default', 'ruang' => $ruang, 'sesi' => $sesi, 'jadwal' => $jadwal, 'rombel' => $rombel, 'hadir' => $hadir]);
    }

    public function save_hadir(Request $request)
    {
        // dd($request);
        $data = array(
            'header1' => $request['header_1'],
            'header2' => $request['header_2'],
            'header3' => $request['header_3'],
            'header4' => $request['header_4'],
            'proktor' => $request['proktor'],
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->hadirApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function print_ruang(Request $request)
    {
        // dd($request);
        $data = array(
            'id_jadwal' => $request['jadwal'],
            'id_sesi' => $request['sesi'],
            'id_ruang' => $request['ruang'],
            'id_ta_sm' => session('id_tahun_ajar'),
        );
        $hadir = $this->hadirApi->print_ruang(json_encode($data));
        if ($hadir['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $hadir['body']['message'],
                'status' => 'berhasil',
                'hadir' => $hadir['body']['data']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $hadir['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function print_kelas(Request $request)
    {
        // dd($request);
        $data = array(
            'id_jadwal' => $request['jadwal'],
            'id_sesi' => $request['sesi'],
            'id_rombel' => $request['kelas'],
            'id_ta_sm' => session('id_tahun_ajar'),
        );
        $hadir = $this->hadirApi->print_rombel(json_encode($data));
        // dd($hadir);
        if ($hadir['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $hadir['body']['message'],
                'status' => 'berhasil',
                'hadir' => $hadir['body']['data']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $hadir['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_loki(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->hadirApi->logo_kiri(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_loka(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->hadirApi->logo_kanan(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_proktor(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->hadirApi->ttd_proktor(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_pengawas1(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->hadirApi->ttd_pengawas1(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function upload_pengawas2(Request $request)
    {
        // dd($request);
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->hadirApi->ttd_pengawas2(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
