<?php

namespace App\Http\Controllers\Cbt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RekapNilaiController extends Controller
{
    // private $ruangApi;
    // private $jadwalApi;

    // public function __construct()
    // {
    //     $this->jadwalApi = new JadwalApi();
    //     $this->rombelApi = new RombelApi();
    // }

    public function index()
    {
        // $rombel = $this->rombelApi->get_by_sekolah();
        // $rombel = $rombel['body']['data'];
        session()->put('title', 'Status Siswa');
        return view('content.cbt.v_rekap_nilai')->with(['template' => 'default']);
    }

    public  function get_rombel(Request $request)
    {
        // dd($request);
        $rombel = $this->jadwalApi->get_rombel($request['id_jadwal']);
        $rombel = $rombel['body']['data'];
        return response()->json($rombel);
    }
}
