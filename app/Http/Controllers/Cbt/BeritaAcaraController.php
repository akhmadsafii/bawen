<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BeritaAcaraApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\RuangApi;
use App\ApiService\Cbt\SessiApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class BeritaAcaraController extends Controller
{
    private $ruangApi;
    private $sesiApi;
    private $jadwalApi;
    private $rombelApi;
    private $acaraApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->ruangApi = new RuangApi();
        $this->sesiApi = new SessiApi();
        $this->rombelApi = new RombelApi();
        $this->jadwalApi = new JadwalApi();
        $this->acaraApi = new BeritaAcaraApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        $ruang = $this->ruangApi->by_sekolah();
        // dd($ruang);
        $ruang = $ruang['body']['data'];
        $sesi = $this->sesiApi->by_sekolah();
        $sesi = $sesi['body']['data'];
        $jadwal = $this->jadwalApi->by_sekolah();
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        $berita = $this->acaraApi->by_sekolah(session('id_sekolah'));
        $berita = $berita['body']['data'];
        // dd($berita);
        session()->put('title', 'Data Mata Pelajaran');
        return view('content.cbt.cetak.v_berita_acara')->with([
            'template' => 'default', 'ruang' => $ruang, 'sesi' => $sesi,
            'jadwal' => $jadwal,  'berita' => $berita,
            'jurusan' => $jurusan
        ]);
    }

    public function save_berita(Request $request)
    {
        // dd($request);
        $data = array(
            'header1' => $request['header_1'],
            'header2' => $request['header_2'],
            'header3' => $request['header_3'],
            'header4' => $request['header_4'],
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->acaraApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function print(Request $request)
    {
        dd($request);
    }

    public function logo_kanan(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->acaraApi->upload_logo_kanan(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function logo_kiri(Request $request)
    {
        $data = array(
            'id_sekolah' => session('id_sekolah'),
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->acaraApi->upload_logo_kiri(json_encode($data));
        // dd($result);
        File::delete($path);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
