<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\KelasRuangApi;
use App\ApiService\Cbt\RuangApi;
use App\ApiService\Cbt\SessiApi;
use App\ApiService\Master\RombelApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KelasRuangController extends Controller
{
    private $ruangApi;
    private $sesiApi;
    private $rombelApi;
    private $kelasRuangApi;

    public function __construct()
    {
        $this->ruangApi = new RuangApi();
        $this->sesiApi = new SessiApi();
        $this->rombelApi = new RombelApi();
        $this->kelasRuangApi = new KelasRuangApi();
    }

    public function index()
    {
        $ruang = $this->ruangApi->by_sekolah();
        $ruang = $ruang['body']['data'];
        $kelas_ruang = $this->kelasRuangApi->by_sekolah();
        $kelas_ruang = $kelas_ruang['body']['data'];
        $form_ruang = empty($kelas_ruang) ? 'add' : 'edit';
        $sesi = $this->sesiApi->by_sekolah();
        $sesi = $sesi['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Ruang Kelas Ujian');
        $template = 'default';
        return view('content.cbt.v_ruang_kelas')->with(['template' => $template, 'ruang' => $ruang, 'sesi' => $sesi, 'rombel' => $rombel, 'form_ruang' => $form_ruang, 'kelas_ruang' => $kelas_ruang]);
    }

    public function update(Request $request)
    {
        $data = [];
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            $id_rombel = $request['id_rombel_' . $i];
            $ruang = $request['ruang_' . $i];
            $sesi = $request['sesi_' . $i];
            $set_siswa = !$request['status_ruang_' . $i] ? 0 : 1;

            $data[] = array(
                'id_rombel' => $id_rombel,
                'id_ruang' => $ruang,
                'id_sesi' => $sesi,
                'set_siswa' => $set_siswa,
                'id_sekolah' => session('id_sekolah'),
                'id_ta_sm' => session('id_tahun_ajar'),
            );
        }
        $data_send = array(
            'kelas_ruangs' => $data
        );
        // dd($data_send);
        $result = $this->kelasRuangApi->create_many(json_encode($data_send), session('id_tahun_ajar'));
        // dd($data);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
