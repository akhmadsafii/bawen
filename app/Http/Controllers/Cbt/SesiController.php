<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\SessiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SesiController extends Controller
{
    private $sesiApi;

    public function __construct()
    {
        $this->sesiApi = new SessiApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Sesi Ujian');
        $sesi = $this->sesiApi->by_sekolah();
        // dd($sesi);
        if ($sesi['code'] != 200) {
            $pesan = array(
                'message' => $sesi['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('pesan', $pesan);
        }
        $result = $sesi['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit m-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<a href="javascript:void(0)" class="btn btn-danger btn-sm delete m-1" data-id="' . $data['id'] . '"><i class="fas fa-trash"></i></a>';
                    return $button;
                });
            $table->editColumn('status', function ($row) {
                // dd($row);
                $checked = '';
                if ($row['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch"><input type="checkbox" ' . $checked . ' class="sesi_check" data-id="' . $row['id'] . '"><span class="slider round"></span></label>';
            });

            $table->editColumn('waktu', function ($wkt) {
                $mulai = '-';
                $selesai = '-';
                if ($wkt['jam_mulai'] != null) {
                    $mulai = $wkt['jam_mulai'];
                }
                if ($wkt['jam_selesai'] != null) {
                    $selesai = $wkt['jam_selesai'];
                }
                return $mulai . ' s/d ' . $selesai;
            });
            $table->rawColumns(['action', 'status', 'waktu']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $template = 'default';
        return view('content.cbt.v_sesi')->with(['template' => $template]);
    }



    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'kode' => $request['kode'],
            'nama' => $request['nama'],
            'status' => $request['status'],
            'jam_mulai' => $request['jam_mulai'],
            'jam_selesai' => $request['jam_selesai'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sesiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'kode' => $request['kode'],
            'nama' => $request['nama'],
            'jam_mulai' => $request['jam_mulai'],
            'jam_selesai' => $request['jam_selesai'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sesiApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $sesi = $this->sesiApi->get_by_id($request['id']);
        $sesi = $sesi['body']['data'];
        return response()->json($sesi);
    }

    public function delete(Request $request)
    {

        $delete = $this->sesiApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
