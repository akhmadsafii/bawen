<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Cbt\DurasiSiswaApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\JawabanApi;
use App\ApiService\Cbt\SoalApi;
use App\ApiService\Cbt\TokenApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    private $tokenApi;
    private $soalApi;
    private $jawabanApi;
    private $bankApi;
    private $jadwalApi;
    private $durasiApi;

    public function __construct()
    {
        $this->tokenApi = new TokenApi();
        $this->soalApi = new SoalApi();
        $this->jawabanApi = new JawabanApi();
        $this->bankApi = new BankSoalApi();
        $this->jadwalApi = new JadwalApi();
        $this->durasiApi = new DurasiSiswaApi();
    }

    public function index()
    {
        $token = $this->tokenApi->by_sekolah(session('id_sekolah'));
        // dd($token);
        $token = $token['body']['data'];
        session()->put('title', 'Data Token');
        return view('content.cbt.v_token')->with(['template' => 'default', 'token' => $token]);
    }


    public function generate()
    {
        $token = strtoupper(Str::random(5));
        $data = array(
            'token' => $token,
            'id_sekolah' => session('id_sekolah')
        );
        $update = $this->tokenApi->create(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'token' => $token
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function konfirmasi(Request $r)
    {
        // dd($r);
        $jadwal = $this->jadwalApi->get_by_id($r['id_jadwal']);
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        // dd($jadwal);
        $data = array(
            'token' => $r['value']
        );
        $token = $this->tokenApi->check(json_encode($data));
        $token = $token['body']['data'];
        if ($token != null || $r['token'] == 0) {
            if ($r['status'] == 'lanjutkan') {
                return response()->json([
                    'message' => 'Token berhasil diterima',
                    'icon'  => 'success',
                    'status'  => 'berhasil',
                    'url_red' => route('cbt_siswa-soal_ujian', ['kode' => Help::encode($r['id_jadwal'])])
                ]);
            } else {
                $soal = $this->soalApi->by_bank($r['id_bank']);
                $soal = $soal['body']['data'];
                if (empty($soal)) {
                    return response()->json([
                        'message' => 'Pastikan soal di bank sudah tersedia dan status soal di jadwal ditampilkan',
                        'icon'  => 'error',
                        'status'  => 'gagal'
                    ]);
                } else {
                    $numbers = range(count($soal), 1);
                    if ($jadwal['acak_soal'] == 1) {
                        shuffle($numbers);
                    }
                    foreach ($soal as $so) {
                        // dd($so);
                        $nomer = array_pop($numbers);
                        $data_soal = array(
                            'id_bank' => $so['id_bank'],
                            'id_jadwal' => $r['id_jadwal'],
                            'id_soal' => $so['id'],
                            'id_kelas_siswa' => session('id_kelas_siswa'),
                            'tipe' => $so['tipe'],
                            'no_soal_alias' => $nomer,
                            'jawaban_benar' => $so['jawaban'],
                            'soal_akhir' => $nomer == count($soal) ? 1 : 0,
                            'id_sekolah' => session('id_sekolah'),
                        );
                        if ($so['tipe'] == 1) {
                            $alphabet = range('A', 'Z');
                            $numb = range(0, $jadwal['bank_soal']['opsi'] - 1);
                            shuffle($numb);
                            for ($i = 0; $i < $jadwal['bank_soal']['opsi']; $i++) {
                                $data_soal['opsi_alias_' . strtolower($alphabet[$i])] =  strtolower($alphabet[array_pop($numb)]);
                            }
                        }
                        $result = $this->jawabanApi->create(json_encode($data_soal));
                    }

                    if ($result['code'] == 200) {
                        $data_durasi = array(
                            'id_kelas_siswa' => session('id_kelas_siswa'),
                            'id_jadwal' => $r['id_jadwal'],
                            'mulai' => date('Y-m-d H:i:s'),
                            'reset' => 0,
                            'status_ujian' => 1,
                            'id_sekolah' => session('id_sekolah'),
                        );
                        $result_durasi = $this->durasiApi->create(json_encode($data_durasi));
                        if ($result_durasi['code'] == 200) {
                            return response()->json([
                                'message' => 'Token berhasil diterima',
                                'icon'  => 'success',
                                'status'  => 'berhasil',
                                'url_red' => route('cbt_siswa-soal_ujian', ['kode' => Help::encode($r['id_jadwal'])])
                            ]);
                        } else {
                            return response()->json([
                                'message' => $result_durasi['body']['message'],
                                'icon'  => 'error',
                                'status'  => 'gagal'
                            ]);
                        }

                        return response()->json([
                            'message' => 'Token berhasil diterima',
                            'icon'  => 'success',
                            'status'  => 'berhasil',
                            'url_red' => route('cbt_siswa-soal_ujian', ['kode' => Help::encode($r['id_jadwal'])])
                        ]);
                    } else {
                        return response()->json([
                            'message' => $result['body']['message'],
                            'icon'  => 'error',
                            'status'  => 'gagal'
                        ]);
                    }
                }
            }
        } else {
            return response()->json([
                'message' => "Token bermasalah",
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
