<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Cbt\DurasiSiswaApi;
use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Master\RombelApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatusUjianController extends Controller
{
    private $ruangApi;
    private $jadwalApi;
    private $bankApi;
    private $durasiApi;

    public function __construct()
    {
        $this->jadwalApi = new JadwalApi();
        $this->rombelApi = new RombelApi();
        $this->durasiApi = new DurasiSiswaApi();
        $this->bankApi = new BankSoalApi();
    }

    public function index()
    {
        if (session('role') == 'guru') {
            $jadwal = $this->jadwalApi->by_guru(session('id'), session('id_tahun_ajar'));
        } else {
            $jadwal = $this->jadwalApi->by_sekolah();
        }
        $jadwal = $jadwal['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Status Siswa');
        return view('content.cbt.v_status_siswa')->with(['template' => 'default', 'jadwal' => $jadwal, 'rombel' => $rombel]);
    }

    public  function get_rombel(Request $request)
    {
        // dd($request);
        $rombel = $this->jadwalApi->get_rombel($request['id_jadwal']);
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        return response()->json($rombel);
    }

    public function filter_rombel()
    {
        // dd($_GET);
        $durasi = $this->durasiApi->filter($_GET['kelas'], $_GET['jadwal'], session('id_tahun_ajar'));
        // dd($durasi);
        $durasi = $durasi['body']['data'];
        $detail = $this->bankApi->by_jadwal($_GET['jadwal']);
        $detail = $detail['body']['data'];
        $data = array(
            'siswa' => $durasi,
            'detail' => $detail
        );
        return response()->json($data);
    }

    public function store_siswa(Request $r)
    {
        // dd($r);
        $req =  json_decode($r->aksi, true);
        // dd($req);
        $reset = [];
        $selesaikan = [];
        $ulang = [];
        foreach ($req['reset'] as $rst) {
            $clear_rst = explode('###', $rst);
            $reset[] = array(
                'id_kelas_siswa' => $clear_rst[0],
                'id_jadwal' => $clear_rst[1],
                'reset' => 1
            );
        }
        foreach ($req['force'] as $frc) {
            $clear_frc = explode('###', $frc);
            $selesaikan[] = array(
                'id_kelas_siswa' => $clear_frc[0],
                'id_jadwal' => $clear_frc[1],
            );
        }
        foreach ($req['ulang'] as $ulg) {
            $clear_ulg = explode('###', $ulg);
            $ulang[] = array(
                'id_kelas_siswa' => $clear_ulg[0],
                'id_jadwal' => $clear_ulg[1],
            );
        }

        $data = array(
            'reset' => $reset,
            'selesai' => $selesaikan,
            'ulang' => $ulang,
        );
        $forms = array(
            'forms' => $data
        );
        $result = $this->durasiApi->change_status(json_encode($forms));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
