<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Master\PrestasiGuruApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class PrestasiGuruController extends Controller
{
    private $prestasiApi;
    private $guruApi;

    public function __construct()
    {
        $this->prestasiApi = new PrestasiGuruApi();
        $this->guruApi = new GuruApi();
    }

    public function index()
    {
        $prestasi = $this->prestasiApi->get_by_sekolah();
        $prestasi = $prestasi['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        // dd($prestasi);
        // $template = 'default';
        // if (session('role') != 'induk-admin') {
        //     $template = session('template');
        // }
        $template = session('template');
        if (session('role') == 'induk-admin') {
            $template = 'default';
        }
        return view('content.kesiswaan.prestasi_guru')->with(['prestasi' => $prestasi, 'template' => $template, 'guru' => $guru, 'url' => $this->prestasiApi->import()]);
    }



    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_guru' => Help::decode($request['id_guru']),
            'prestasi' => $request['nama'],
            'keterangan' => $request['keterangan'],
            'tahun' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->prestasiApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $prestasi = $this->data_prestasi();
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'prestasi' => $prestasi,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $prestasi = $this->prestasiApi->get_by_id($request['id']);
        $prestasi = $prestasi['body']['data'];
        $prestasi['id_guru'] = Help::encode($prestasi['id_guru']);
        return response()->json($prestasi);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_guru' => Help::decode($request['id_guru']),
            'prestasi' => $request['nama'],
            'keterangan' => $request['keterangan'],
            'tahun' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->prestasiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $prestasi = $this->data_prestasi();
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'prestasi' => $prestasi,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->prestasiApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            $prestasi = $this->data_prestasi();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'prestasi' => $prestasi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_prestasi()
    {
        $prestasi = $this->prestasiApi->get_by_sekolah();
        $prestasi = $prestasi['body']['data'];
        $html = '';
        if (!empty($prestasi)) {
            $no = 1;
            foreach ($prestasi as $pt) {
                $nip = '-';
                $keterangan = '-';
                if ($pt['nip'] != null) {
                    $nip = $pt['nip'];
                }
                if ($pt['keterangan'] != null) {
                    $keterangan = $pt['keterangan'];
                }
                $html .= '<tr>
                <td class="vertical-middle">' . $no++ . '</td>
                <td>
                    <b>' . $pt['nama'] . '</b>
                    <br><small>NIP. ' . $nip . '</small>
                </td>
                <td><b>' . $pt['prestasi'] . '</b><br><small>Keterangan : ' . $keterangan . '</small>
                </td>
                <td class="vertical-middle">' . $pt['tahun'] . '</td>';
                if (session('role') != 'supervisor') {
                    $html .= '<td class="text-center vcertical-middle">
                    <a href="javascript:void(0)" data-id="' . $pt['id'] . '"
                        class="edit btn btn-info btn-sm"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="' . $pt['id'] . '"
                        class="delete btn btn-sm btn-danger"><i
                            class="fas fa-trash"></i></a>
                </td>';
                }
                $html .= '</tr>';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini belum tersedia</td></tr>';
        }
        return $html;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->prestasiApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            $prestasi = $this->data_prestasi();
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'prestasi' => $prestasi
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
