<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\KategoriPelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PelanggaranController extends Controller
{
    private $pelanggaranApi;
    private $kategoriApi;

    public function __construct()
    {
        $this->pelanggaranApi = new PelanggaranApi();
        $this->kategoriApi = new KategoriPelanggaranApi();
    }

    public function index(Request $request)
    {
        $kategori = $this->kategoriApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        // dd($kategori);
        $pelanggaran = $this->pelanggaranApi->get_by_sekolah();
        // dd($pelanggaran);
        $result = $pelanggaran['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Hapus</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Pelanggaran');
        return view('content.point.admin.v_pelanggaran')->with(['kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kategori_pelanggaran' => $request['id_kategori'],
            'nama' => $request['nama'],
            'point' => $request['bobot'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->pelanggaranApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $pelanggaran = $this->data_pelanggaran($request['id_kategori']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'pelanggaran' => $pelanggaran
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $pelanggaran = $this->pelanggaranApi->get_by_id($request['id']);
        $pelanggaran = $pelanggaran['body']['data'];
        return response()->json($pelanggaran);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_kategori_pelanggaran' => $request['id_kategori'],
            'nama' => $request['nama'],
            'point' => $request['bobot'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->pelanggaranApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $pelanggaran = $this->data_pelanggaran($request['id_kategori']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'pelanggaran' => $pelanggaran
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->pelanggaranApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $pelanggaran = $this->data_pelanggaran($request['id_kategori']);
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'pelanggaran' => $pelanggaran
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    private function data_pelanggaran($id)
    {
        $pelanggaran = $this->pelanggaranApi->by_kategori($id);
        $pelanggaran = $pelanggaran['body']['data'];
        $html = '';
        if (!empty($pelanggaran)) {
            $nomer = 1;
            foreach ($pelanggaran as $plg) {
                $checked = '';
                if ($plg['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td>' . $nomer++ . '</td>
                <td>' . $plg['nama'] . '</td>
                <td>' . $plg['point'] . '</td>
                <td>
                    <label class="switch">
                        <input type="checkbox" ' . $checked . ' class="pelanggaran_check"
                            data-id="' . $plg['id'] . '">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)"
                        class="btn btn-info btn-sm ePlg"
                        data-id="' . $plg['id'] . '"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)"
                        class="btn btn-danger btn-sm dPlg"
                        data-id="' . $plg['id'] . '" data-kategori="'.$plg['id_kategori_pelanggaran'].'"><i
                            class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }
}
