<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Master\AnggotaEkskulApi;
use App\ApiService\Master\EkstrakurikulerApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnggotaEkskulController extends Controller
{
    private $anggotaApi;
    private $ekskulApi;
    private $tahunApi;

    public function __construct()
    {
        $this->anggotaApi = new AnggotaEkskulApi();
        $this->ekskulApi = new EkstrakurikulerApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        $anggota = $this->anggotaApi->get_by_sekolah($_GET['tahun']);
        $anggota = $anggota['body']['data'];
        // dd()
        $tahun = $this->tahunApi->get_tahun_ajaran();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        session()->put('title', "Ekstrakurikuler");
        $template = session('template');
        if (session('role') == 'admin-kesiswaan') {
            $template = 'default';
        }
        if ($request->ajax()) {
            $table = datatables()->of($anggota)
                ->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="edit"
                        data-id="' . $data['id'] . '"> <i class="material-icons list-icon md-18 text-info">edit</i>
                    </a>';
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.point.bimbingan_konseling.v_ekskul')->with([
            'template' => $template, 'tahun' => $tahun
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_ekstra' => $request['ekskul'],
            'l' => $request['l'],
            'p' => $request['p'],
            'tahun' => $request['tahun_ajaran'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->anggotaApi->create(json_encode($data));
        // dd($result);
        $html = $this->get_data();
        if ($result['code'] == 200) {
            $message = array(
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'html' => $html
            );
        } else {
            $message = array(
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal',
                'html' => $html
            );
        }
        return response()->json($message);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'id_ekstra' => $request['id_ekstra'],
            'l' => $request['l'],
            'p' => $request['p'],
            'tahun' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->anggotaApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        // dd($request);
        $data = $this->anggotaApi->get_by_ekskul($request['id'], $request['tahun']);
        // dd($data);
        $data = $data['body']['data'];
        $result = array(
            'id' => $data != null ? $data['id'] : null,
            'id_ekstra' => $request['id'],
            'l' => $data != null ? $data['l'] : 0,
            'p' => $data != null ? $data['p'] : 0,
            'tahun' => $request['tahun']
        );
        // dd($result);
        return response()->json($result);
    }

    public function delete(Request $request)
    {
        $delete = $this->anggotaApi->soft_delete($request['id']);
        $html = $this->get_data();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $html
            ]);
        }
    }

    private function get_data()
    {
        $anggota = $this->anggotaApi->get_by_sekolah(session('tahun'));
        // dd($ekskul);
        $anggota = $anggota['body']['data'];
        $html = '';
        $no = 1;
        foreach ($anggota as $ag) {
            $l = $ag['l'] != null ? $ag['l'] : '0';
            $p = $ag['p'] != null ? $ag['p'] : '0';
            $html .= '<tr>
            <td class="text-center">' . $no++ . '</td>
            <td class="text-center">' . $ag['nama'] . '</td>
            <td class="text-center">' . $l . '</td>
            <td class="text-center">' . $p . '</td>
            <td class="text-center">
                <a href="javascript:void(0)" class="edit' . $ag['id'] . '"
                    onclick="editEkskul(' . $ag['id'] . ')">
                    <i class="material-icons list-icon md-18 text-info">edit</i>
                </a>
            </td>
        </tr>';
        }
        return $html;
    }
}
