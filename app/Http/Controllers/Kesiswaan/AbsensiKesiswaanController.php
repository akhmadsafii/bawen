<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\RombelApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Foreach_;
use App\Helpers\Help;

class AbsensiKesiswaanController extends Controller
{
    private $dataSiswaApi;
    private $rombelApi;
    private $guruPelajaranApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->guruPelajaranApi = new GuruPelajaranApi();
        $this->dataSiswaApi = new DataSiswaApi();
    }

    public function index()
    {
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Absensi Siswa');
        return view('content.point.bimbingan_konseling.v_absensi')->with(['rombel' => $rombel, 'template' => session('template')]);
    }

    public function get_mapel(Request $request)
    {
        // dd($request);
        $pelajaran = $this->guruPelajaranApi->get_rombel($request['id']);
        $pelajaran = $pelajaran['body']['data'];
        return response()->json($pelajaran);
    }

    public function get_siswa(Request $request)
    {
        // dd($request);
        $siswa = $this->dataSiswaApi->get_absensi($request['id_mapel'], $request['id_rombel'], session('id_tahun_ajar'));
        if ($siswa['code'] != 200) {
            return response()->json([
                'icon' => "error",
                'message' => "Maaf proses Gagal, karena ada yg belum sesuai",
                'status' => 'gagal'
            ]);
        }
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            foreach ($siswa as $sw) {
                $html .= '<tr data-toggle="collapse"
                    data-target="#demo' . $sw['id'] . '"
                    class="accordion-toggle">
                    <td>
                        <button class="btn btn-default btn-xs">
                            <span class="fa fa-info-circle text-info"></span>
                        </button>
                    </td>
                    <td>' . ucfirst($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['nisn'] . '</td>
                </tr>
                <tr>
                    <td colspan="4" class="hiddenRow">
                        <div class="accordian-body collapse"
                            id="demo' . $sw['id'] . '">
                            <table class="table table-striped widget-status-table">
                                <thead>
                                    <tr>
                                        <th class="text-center">Pertemuan
                                        </th>
                                        <th class="text-center">Kehadiran
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>';
                foreach ($sw['pertemuan'] as $pt) {
                    if ($pt['id_data_siswa'] != null) {
                        $html .= '<tr>
                                        <td class="text-center">
                                            ' . $pt['pertemuan'] . '
                                        </td>
                                        <td class="text-center">
                                            <a href="javasript:void(0)" class="btn edit btn-sm"><span class="badge badge-info text-inverse">' . $pt['kehadiran'] . '</span></a>
                                        </td>
                                    </tr>';
                    } else {
                        $html .= '<tr><td colspan="2" class="text-center">Siswa belum pernah mengikuti pelajaran</td></tr>';
                    }
                }
                $html .= '</tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data Siswa dengan filter tersebut kosong</td></tr>';
        }



        return response()->json($html);
    }

    public function rekapan()
    {
        $mapel = $this->guruPelajaranApi->get_by_rombel(session('id_rombel'));
        $mapel = $mapel['body']['data'];
        return view('content.point.bimbingan_konseling.v_rekap_absensi')->with(['mapel' => $mapel, 'template' => 'default']);
    }
}
