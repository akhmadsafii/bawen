<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\KategoriPelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranSiswaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class PelanggaranSiswaController extends Controller
{
    private $rombelApi;
    private $kelassiswaApi;
    private $pelanggaranApi;
    private $kategoriApi;
    private $pelApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->pelanggaranApi = new PelanggaranSiswaApi();
        $this->pelApi = new PelanggaranApi();
        $this->kategoriApi = new KategoriPelanggaranApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        session()->put('title', 'Input Pelanggaran');
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        $kategori = $this->kategoriApi->get_by_sekolah();
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        if (session("role") == "ortu") {
            $pelanggarans = $this->pelanggaranApi->get_by_kelas_siswa(session("id_kelas_siswa"));
            $pelanggaran = $pelanggarans['body']['data'];
            $siswa = $this->kelassiswaApi->get_by_id(session('id_kelas_siswa'));
            $siswa = $siswa['body']['data'];
            $siswa['jml_pelanggaran'] = count($pelanggaran);
            $siswa['total_point'] = $pelanggarans['body']['total'];
            return view('content.point.siswa.v_pelanggaran')->with(['template' => session('template'), 'pelanggaran' => $pelanggaran, 'siswa' => $siswa]);
        } elseif (session("role") == "admin") {
            Session::put('title', 'Sanksi Pelanggaran');
            $pelanggaran = $this->pelanggaranApi->get_sekolah();
            // dd($pelanggaran);
            $pelanggaran = $pelanggaran['body']['data'];
            return view('content.point.admin.v_pelanggaran_siswa')->with(['jurusan' => $jurusan]);
        }
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin' || session('role') == 'bk') {
            $template = 'default';
        }


        $routes = "point_pelanggaran_siswa-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $rombel = (isset($_GET["rombel"])) ? $_GET["rombel"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->kelassiswaApi->rombel_siswa($page, session('tahun'), $rombel, $search);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        // dd($result);
        $siswa = $result['body']['data'];
        // dd($alumni);
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSiswaRombel($meta, $routes, session('tahun'), $rombel, $search);
        // dd($pagination);
        return view('content.point.bimbingan_konseling.pelanggaran_siswa.v_pelanggaran_siswa', compact('jurusan', 'kategori', 'rombel', 'pagination', 'search', 'page', 'siswa', 'routes'));
    }

    public function next_step()
    {
        // dd("tes");
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        if ($_GET['siswa'] == '') {
            // dd("tes");
            $pesan = array(
                'message' => 'Harap menginput siswa terlebih dulu',
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->route('point_pelanggaran_siswa-beranda')->with('message', $pesan);
        }
        session()->put('title', 'Siswa yang melanggar');
        $kategori = $this->kategoriApi->get_by_sekolah();
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        $siswa = [];
        $kelas_siswa = explode('-', $_GET['siswa']);
        // dd($kelas_siswa);
        foreach ($kelas_siswa as $ks) {
            $k_siswa = $this->kelassiswaApi->get_by_id(Help::decode($ks));
            $k_siswa = $k_siswa['body']['data'];
            $siswa[] = $k_siswa;
        }
        return view('content.point.bimbingan_konseling.pelanggaran_siswa.v_next_step', compact('siswa', 'kategori', 'jurusan'));
        // dd($siswa);
    }

    public function get_siswa(Request $request)
    {
        // dd($request);
        $id_rombel = $request['id_rombel'];
        $nisn = $request['nisn'];
        if ($nisn == null && $id_rombel != null) {
            $post  = $this->kelassiswaApi->get_by_rombel($id_rombel, session('tahun'));
            // dd($post);
        } elseif ($id_rombel == null && $nisn != null) {
            $post  = $this->kelassiswaApi->get_by_nisn($nisn, session('tahun'));
        } else {
            $post  = $this->kelassiswaApi->get_by_rombel_nisn($id_rombel, $nisn, session('tahun'));
        }
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function get_pelanggaran(Request $request)
    {
        // dd($request);
        $id_kategori = $request['id_kategori'];
        $kategori = $this->kategoriApi->get_by_id($id_kategori);
        $result = $kategori['body']['data'];
        return response()->json($result);
    }

    public function next_input(Request $request)
    {
        // dd($request);
        if ($request['id_kelas_siswa']) {
            $siswa = $request['id_kelas_siswa'];
            $data = [];
            foreach ($siswa as $s) {
                $siswa = $this->kelassiswaApi->get_by_id($s);
                $siswa = $siswa['body']['data'];
                $data[] = $siswa;
            }
            $respon = array(
                'data' => $data,
                'status' => "berhasil",
            );
        } else {
            $respon = array(
                'data' => "silahkan pilih siswa terlebih dahulu",
                'status' => 'gagal'
            );
        }
        return response()->json($respon);
    }

    public function store_siswa(Request $request)
    {
        // dd($request);
        $siswa = explode('-', $request['siswa']);
        foreach ($request['id_pelanggaran'] as $p) {
            foreach ($siswa as $s) {
                $point = $this->pelApi->get_by_id($p);
                $point = $point['body']['data'];
                $data = array(
                    'id_kelas_siswa' => Help::decode($s),
                    'id_pelanggaran' => (int)$p,
                    'tgl_pelanggaran' => date("Y-m-d"),
                    'point' => $point['point'],
                    'id_guru' => session('role') == 'guru' ? session('id') : null,
                    'id_sekolah' => session('id_sekolah'),
                );
                $result = $this->pelanggaranApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        // dd($data_mentah);
    }

    public function finish_step(Request $request)
    {
        $siswa = $request['id_kelas_siswa'];
        $pelanggaran = $request['id_pelanggaran'];
        $data_mentah = [];
        foreach ($pelanggaran as $p) {
            foreach ($siswa as $s) {
                $data_mentah[] = array(
                    'id_kelas_siswa' => (int)$s,
                    'id_pelanggaran' => (int)$p,
                    'tgl_pelanggaran' => date("Y-m-d")
                );
            }
        }
        foreach ($data_mentah as $dm) {
            $point = $this->pelApi->get_by_id($dm['id_pelanggaran']);
            $point = $point['body']['data']['point'];
            // dd($point);
            $data_insert = array(
                'id_kelas_siswa' => $dm['id_kelas_siswa'],
                'id_pelanggaran' => $dm['id_pelanggaran'],
                'tgl_pelanggaran' => $dm['tgl_pelanggaran'],
                'point' => $point,
                'id_guru' => session('role') == 'guru' ? session('id') : null,
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->pelanggaranApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $pelanggaran = $this->pelanggaranApi->get_by_id($request['id']);
        // dd($pelanggaran);
        $pelanggaran = $pelanggaran['body']['data'];
        return response()->json($pelanggaran);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->pelanggaranApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function getCustomFilterData(Request $request)
    {
        // dd($request);
        if (request()->ajax()) {
            if ($request['id_kelas_siswa'] != null) {
                if ($request['id_kelas_siswa'] == 'all') {
                    $pelanggaran = $this->pelanggaranApi->by_rombel($request['id_rombel']);
                } else {
                    $pelanggaran = $this->pelanggaranApi->get_by_kelas_siswa($request['id_kelas_siswa']);
                }
            } elseif ($request['id_rombel'] && $request['id_kelas_siswa'] == null) {
                if ($request['id_rombel'] == 'all') {
                    $pelanggaran = $this->pelanggaranApi->get_by_data();
                } else {
                    $pelanggaran = $this->pelanggaranApi->by_rombel($request['id_rombel']);
                }
            } else {
                $pelanggaran = $this->pelanggaranApi->get_by_data();
            }
            $result = $pelanggaran['body']['data'];
            // dd($result);
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="btn btn-info btn-sm detail" data-id="' . $data['id_kelas_siswa'] . '" data-nama="' . $data['nama'] . '"><i class="fas fa-info-circle"></i></a>';
                });
            $table->editColumn('whatsapp', function ($row) {
                if ($row['telp_ortu'] != null) {
                    return '<a href="' . $row['wa_ortu'] . '" target="_blank" class="text-success"><i class="fab fa-whatsapp fa-3x"></i> <br> <small class="text-success">' . $row['telp_ortu'] . '</small></a>';
                } else {
                    return '-';
                }
            });
            $table->editColumn('siswa', function ($row) {
                return '<b>' . $row['nama'] . '</b><p>NIS/NISN. ' . $row['nis'] . ' / ' . $row['nisn'] . '</p>';
            });
            $table->editColumn('rombel', function ($row) {
                return '<b>' . $row['rombel'] . '</b><p>Jurusan. ' . $row['jurusan'] . '</p>';
            });
            $table->rawColumns(['action', 'whatsapp', 'siswa', 'rombel']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function getDetail(Request $request)
    {
        // dd($request);
        if (request()->ajax()) {
            $pelanggaran = $this->pelanggaranApi->list_by_kelas_siswa($request['kelas_siswa']);
            $result = $pelanggaran['body']['data'];
            $table = datatables()->of($result)
                ->addColumn('tanggal', function ($data) {
                    return Help::getTanggal($data['tgl_pelanggaran']);
                });
            $table->editColumn('penanganan', function ($row) {
                $kelas = 'danger';
                $status = 'Belum';
                if ($row['penanganan'] == 1) {
                    $kelas = 'success';
                    $status = 'Sudah';
                }
                return '<span class="badge badge-' . $kelas . ' text-inverse">' . $status . '</span>';
            });
            $table->rawColumns(['tanggal', 'penanganan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function get_filter_siswa(Request $request)
    {
        if (request()->ajax()) {
            if ($request->filter_siswa == "terbanyak") {
                $pelanggaran = $this->pelanggaranApi->get_by_pelanggaran_terbanyak();
                $result = $pelanggaran['body']['data'];
            } elseif ($request->filter_siswa == "berat") {
                $pelanggaran = $this->pelanggaranApi->get_by_pelanggaran_terbanyak();
                $result = $pelanggaran['body']['data'];
            } else {
                $pelanggaran = $this->pelanggaranApi->pelanggaran_warning();
                $result = $pelanggaran['body']['data'];
            }
            // dd($result);

            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="delete" data-id="' . $data['id_kelas_siswa'] . '" class="delete-' . $data['id_kelas_siswa'] . ' btn btn-info btn-sm" onclick="detail(' . $data['id_kelas_siswa'] . ')"><i class="fas fa-info-circle"></i></button>';
                    if ($data['wa_ortu'] != null) {
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<a href="' . $data['wa_ortu'] . '" data-toggle="tooltip" class="btn btn-success btn-sm" target="_blank"><i class="fab fa-whatsapp"></i></a>';
                    }
                    return $button;
                });
            $table->editColumn('profile', function ($row) {
                return '<b>' . $row['nama'] . '</b><p class="m-0">NIS/NISN. ' . $row['nis'] . '/' . $row['nisn'] . '</p>';
            });
            $table->editColumn('kelas', function ($row) {
                return '<p class="m-0"><b>' . $row['rombel'] . '</b></p><p class="m-0">Jurusan. ' . $row['jurusan'] . '</p>';
            });
            $table->editColumn('statistik', function ($row) {
                return '<p class="m-0">' . $row['jumlah_pelanggaran'] . ' Pelanggaran</p><p class="m-0">' . $row['total_point'] . ' Point</p>';
            });
            $table->rawColumns(['action', 'profile', 'kelas', 'statistik']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function get_pelanggaran_terbanyak()
    {
        $peringkat_pelanggaran = $this->pelanggaranApi->peringkat_pelanggaran_terbanyak();
        $peringkat_pelanggaran = $peringkat_pelanggaran['body']['data'];
        $pelanggaran_kelas = $this->pelanggaranApi->peringkat_pelanggaran_kelas();
        $pelanggaran_kelas = $pelanggaran_kelas['body']['data'];
        $bar = [];
        if (!empty($pelanggaran_kelas)) {
            foreach ($pelanggaran_kelas as $pk) {
                $bar[] = array(
                    'rombel' => $pk['nama'],
                    'jumlah' => $pk['total_pelanggaran'],
                );
            }
        }

        $donut = [];
        if (!empty($peringkat_pelanggaran)) {
            foreach ($peringkat_pelanggaran as $pe) {
                $donut[] = array(
                    'label' => $pe['nama'],
                    'value' => $pe['jumlah'],
                );
            }
        }
        $output = array(
            'donut' => $donut,
            'bar' => $bar,
        );
        return response()->json($output);
    }

    public function get_pelanggaran_dashboard_siswa()
    {
        $all_pelanggaran = $this->pelanggaranApi->get_by_kelas_siswa(session('id_kelas_siswa'));
        $all_pelanggaran = $all_pelanggaran['body']['data'];
        $sering = $this->pelanggaranApi->get_by_sering_dilakukan(session('id_kelas_siswa'));
        $sering = $sering['body']['data'];
        $bar = [];
        if (!empty($sering)) {
            foreach ($sering as $sr) {
                $bar[] = array(
                    'pelanggaran' => $sr['pelanggaran'],
                    'jumlah' => $sr['jumlah'],
                );
            }
        }

        $donut = [];
        if (!empty($all_pelanggaran)) {
            foreach ($all_pelanggaran as $ap) {
                $donut[] = array(
                    'label' => $ap['pelanggaran'],
                    'value' => $ap['point'],
                );
            }
        }
        $output = array(
            'donut' => $donut,
            'bar' => $bar,
        );
        return response()->json($output);
    }

    public function update_sanksi(Request $request)
    {
        // dd($request);
        $reset = 0;
        $penanganan = 0;
        if ($request['reset_point']) {
            $reset = 1;
        }

        if ($request['ditangani']) {
            $penanganan = 1;
        }

        $data = array(
            'id' => $request['id_pelanggaran'],
            'id_sanksi_pelanggaran' => $request['sanksi'],
            'reset_point' => $reset,
            'penanganan' => $penanganan,
        );

        $result = $this->pelanggaranApi->update_sanksi(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function rekapan()
    {
        $routes = "pelanggaran_siswa-rekapan";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $pelanggarans = $this->pelanggaranApi->get_by_id_rombel(session('id_rombel'), $search);
        $pelanggaran = $pelanggarans['body']['data'];
        $meta = $pelanggarans['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        return view('content.point.bimbingan_konseling.pelanggaran_siswa.v_rekap_pelanggaran')->with(['template' => 'default', 'pelanggaran' => $pelanggaran, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search]);;
    }
}
