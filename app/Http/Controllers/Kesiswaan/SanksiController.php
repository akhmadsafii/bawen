<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\SanksiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SanksiController extends Controller
{
    private $sanksiApi;

    public function __construct()
    {
        $this->sanksiApi = new SanksiApi();
    }

    public function index()
    {
        $sanksi = $this->sanksiApi->get_by_sekolah();
        // dd($sanksi);
        $sanksi = $sanksi['body']['data'];
        Session::put('title', 'Sanksi Pelanggaran');
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.point.admin.v_sanksi')->with(['template' => $template, 'sanksi' => $sanksi]);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->create(json_encode($data));
        $sanksi = $this->data_sanksi();
        if ($result['code'] == 200) {
            $icon = "success";
            $message = $result['body']['message'];
            $status = "berhasil";
        } else {
            $icon = "error";
            $message = $result['body']['message'];
            $status = "gagal";
        }
        return response()->json([
            'icon' => $icon,
            'message' => $message,
            'status' => $status,
            'sanksi' => $sanksi
        ]);
    }

    public function edit(Request $request)
    {
        $id = $request['id'];
        $sanksi = $this->sanksiApi->get_by_id($id);
        $sanksi = $sanksi['body']['data'];
        return response()->json($sanksi);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->update_info(json_encode($data));
        $sanksi = $this->data_sanksi();
        if ($result['code'] == 200) {
            $icon = "success";
            $message = $result['body']['message'];
            $status = "berhasil";
        } else {
            $icon = "error";
            $message = $result['body']['message'];
            $status = "gagal";
        }
        return response()->json([
            'icon' => $icon,
            'message' => $message,
            'status' => $status,
            'sanksi' => $sanksi
        ]);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->sanksiApi->soft_delete($request['id']);
        $sanksi = $this->data_sanksi();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'sanksi' => $sanksi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'sanksi' => $sanksi
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->sanksiApi->delete($id);

        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',

            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',

            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->sanksiApi->all_trash();
        $result = $jurusan['body']['data'];
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->editColumn('bobot', function ($row) {
            return $row['bobot_dari'] . ' - ' . $row['bobot_sampai'];
        });
        $table->rawColumns(['action', 'bobot']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->sanksiApi->restore($id);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_sanksi()
    {
        $sanksi = $this->sanksiApi->get_by_sekolah();
        $sanksi = $sanksi['body']['data'];
        $html = '';
        if (!empty($sanksi)) {
            foreach ($sanksi as $ss) {
                $no = 1;
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$ss['nama'].'</td>
                    <td>'.$ss['bobot_dari'].'</td>
                    <td>'.$ss['bobot_sampai'].'</td>
                    <td>'.$ss['sanksi'].'</td>';
                if (session('role') != 'supervisor') {
                    $html .= '<td class="text-center">
                        <a href="javascript:void(0)" data-id="'.$ss['id'].'"
                            class="edit"><i
                                class="material-icons list-icon md-18 text-info">edit</i></a>
                        <a href="javascript:void(0)" data-id="'.$ss['id'].'"
                            class="delete"><i
                                class="material-icons list-icon md-18 text-danger">delete</i></a>
                    </td>';
                }
                $html .= '</tr>';
            }
        } else {
            $html .= ' <tr><td colspan="6" class="text-center">Data saat ini belum tersedia</td></tr>';
        }
        return $html;
    }
}
