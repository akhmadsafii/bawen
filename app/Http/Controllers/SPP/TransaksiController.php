<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ApiService;
use App\ApiService\Master\KelasSiswaApi;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $listTransaksi = $this->getTransaksi();
            return datatables()
                ->of($listTransaksi)
                ->make(true);
        }
        $responSiswa = (new KelasSiswaApi)->get_by_sekolah(session('tahun'));
        return view('content.spp.transaksi')->with([
            'dataTahunAjaran' => $this->getTahunAjaran(),
            'dataSiswa' => $responSiswa['body']['data'],
        ]);
    }

    public function create(Request $request)
    {
        $payload = [
            'id_sekolah' => $request->session()->get('id_sekolah'),
            'id_kelas_siswa' => $request->id_kelas_siswa,
            'nis' => $request->nis,
            'tahun_ajaran' => $request->tahun_ajaran,
            'tgl_bayar' => $request->tgl_bayar,
            'details' => $request->details,
        ];
        $responPost = $this->postTransaksi($payload);
        return response()->json($responPost)
            ->setStatusCode($responPost['code']);
    }

    public function searchSiswa()
    {
        return datatables()->of([])->make(true);
    }

    public function detail(Request $request, $kode_transaksi)
    {
        if ($request->ajax()) {
            $responDetail = ApiService::request(
                env("API_URL") . 'api/data/spp/detail_transaksi/transaksi/' . urlencode($kode_transaksi),
                "GET",
                null
            );

            // Prepare format data respon
            if (!empty($responDetail['body']['data'])) {
                $nama_bulan = [
                    1 => 'Juli',
                    2 => 'Agustus',
                    3 => 'September',
                    4 => 'Oktober',
                    5 => 'November',
                    6 => 'Desember',
                    7 => 'Januari',
                    8 => 'Februari',
                    9 => 'Maret',
                    10 => 'April',
                    11 => 'Mei',
                    12 => 'Juni',
                ];
                foreach ($responDetail['body']['data'] as $key => $value) {
                    $responTagihan = ApiService::request(
                        env("API_URL") . 'api/data/spp/tagihan/' . $value['id_tagihan'],
                        "GET",
                        null
                    );
                    $responDetail['body']['data'][$key]['nama_tagihan'] = $responTagihan['body']['data']['nama'];
                    $responDetail['body']['data'][$key]['nama_bulan'] = $nama_bulan[$value['bulan']];
                }
            }

            return response()->json($responDetail);
        }
        return response()->json('hai');
    }

    private function getTransaksi()
    {
        $endpoint = env("API_URL") . 'api/data/spp/transaksi/sekolah/profile';
        $respon = ApiService::request($endpoint, "GET", null);
        return $respon['body']['data'];
    }

    private function postTransaksi($payload)
    {
        $responTransaksi = ApiService::request(
            env("API_URL") . 'api/data/spp/transaksi',
            "POST",
            json_encode($payload)
        );
        return $responTransaksi;
    }

    private function getTahunAjaran()
    {
        $responTahunAjaran = ApiService::request(
            env("API_URL") . 'api/data/master/tahun_ajaran/sekolah/tahun_ajaran',
            "GET",
            null
        );
        $tahunDesc = collect($responTahunAjaran['body']['data'])
            ->sortDesc()
            ->map(function ($item) {
                list($taAwal, $taAkhir) = explode('/', $item['tahun_ajaran']);
                return array(
                    'ta_mulai' => $taAwal,
                    'ta_akhir' => $taAkhir,
                    'tahun_ajaran' => $item['tahun_ajaran'],
                );
            })
            ->values()->all();
        return $tahunDesc;
    }
}
