<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use App\ApiService\Master\JurusanApi;
use Illuminate\Http\Request;

use App\Helpers\MockData;

class SPPController extends Controller
{

    public $jurusanApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        //$dataJurusan = $this->get_jurusan();
        //dd(Session());
        //return view('content.spp.dashboard',['jurusan' => 'dataJurusan']);
        return redirect()->route('sumbangan-spp');
    }

    public function spp(Request $request)
    {
        $dataSPP = (MockData::getMockData())->getOneDataSPP($request->route('id'));

        return view('content.spp.spp-detail')->with([
            "data" => $dataSPP,
        ]);
    }

    public function get_jurusan(){
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $result = $jurusan['body']['data'];
        return ($result);
    }
}
