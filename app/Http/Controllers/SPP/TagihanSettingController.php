<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ApiService;

class TagihanSettingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $listPengaturanTagihan = $this->getSettingTagihan();
            return datatables()
                ->of($listPengaturanTagihan)
                ->make(true);
        }

        $listTahunAjaran = $this->getTahunAjaran();
        $listPosPemasukan = $this->getPosPemasukan();
        $listkelas = $this->getKelas();

        return view('content.spp.setting_tagihan')->with([
            'dataTahunAjaran' => $listTahunAjaran,
            'dataPosPemasukan' => $listPosPemasukan,
            'dataKelas' => $listkelas,
        ]);
    }

    public function create(Request $request)
    {
        $payload = [
            'id_sekolah' => $request->session()->get('id_sekolah'),
            // input
            'kode' => $request->kode,
            'tahun_ajaran' => $request->tahun_ajaran,
            'id_pos_pemasukan' => $request->id_pos_pemasukan,
            'id_kelas' => $request->id_kelas,
            'nama' => $request->nama,
            'nominal' => $request->nominal,
            'periode' => $request->periode,
            // list bulan disimpan dalam bentuk string bukan array
            'bulan' => !empty($request->bulan) ? implode(',', $request->bulan) : $request->bulan,
        ];
        $responPost = $this->postSettingTagihan($payload);
        return response()->json($responPost)
            ->setStatusCode($responPost['code']);
    }

    public function edit(Request $request, $id)
    {
        $payload = [
            'id' => $id,
            'id_sekolah' => $request->session()->get('id_sekolah'),
            // input
            'kode' => $request->kode,
            'tahun_ajaran' => $request->tahun_ajaran,
            'id_pos_pemasukan' => $request->id_pos_pemasukan,
            'id_kelas' => $request->id_kelas,
            'nama' => $request->nama,
            'nominal' => $request->nominal,
            'periode' => $request->periode,
            // list bulan disimpan dalam bentuk string bukan array
            'bulan' => !empty($request->bulan) ? implode(',', $request->bulan) : $request->bulan,
        ];
        $responPut = $this->putSettingTagihan($payload, $id);
        return response()
            ->json($responPut)
            ->setStatusCode($responPut['code']);
    }

    private function getSettingTagihan()
    {
        $endpoint = env("API_URL") . 'api/data/spp/tagihan/sekolah/profile';
        $respon = ApiService::request($endpoint, "GET", null);
        return $respon['body']['data'];
    }

    private function getTahunAjaran()
    {
        $responTahunAjaran = ApiService::request(
            env("API_URL") . 'api/data/master/tahun_ajaran/sekolah/tahun_ajaran',
            "GET",
            null
        );
        $tahunDesc = collect($responTahunAjaran['body']['data'])
            ->sortDesc()
            ->map(function ($item) {
                list($taAwal, $taAkhir) = explode('/', $item['tahun_ajaran']);
                return array(
                    'ta_mulai' => $taAwal,
                    'ta_akhir' => $taAkhir,
                    'tahun_ajaran' => $item['tahun_ajaran'],
                );
            })
            ->values()->all();
        return $tahunDesc;
    }

    private function getPosPemasukan()
    {
        $responApi = ApiService::request(
            env("API_URL") . 'api/data/spp/pos_pemasukan/sekolah/profile',
            "GET",
            null
        );
        return $responApi['body']['data'];
    }

    private function getKelas()
    {
        $responProfile = ApiService::request(
            env('API_URL') . 'api/data/master/kelas/profile',
            "GET",
            null
        );
        return $responProfile['body']['data'];
    }

    private function postSettingTagihan($payload)
    {
        $responApi = ApiService::request(
            env("API_URL") . 'api/data/spp/tagihan',
            "POST",
            json_encode($payload)
        );
        return $responApi;
    }

    private function putSettingTagihan($payload, $id)
    {
        $responApi = ApiService::request(
            env("API_URL") . 'api/data/spp/tagihan/' . $id,
            "PUT",
            json_encode($payload)
        );
        return $responApi;
    }
}
