<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\Master\AplikasiMobileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AplikasiMobileController extends Controller
{
    private $mobileApi;

    public function __construct()
    {
        $this->mobileApi = new AplikasiMobileApi();
    }

    public function index()
    {
        session()->put('title', 'Program Mobile');
        $mobile = $this->mobileApi->get_by_sekolah();
        // dd($mobile);
        $mobile = $mobile['body']['data'];
        return view('content.superadmin.master.v_mobile_program')->with(['mobile' => $mobile]);
    }

    public function detail(Request $request)
    {
        $mobile = $this->mobileApi->get_by_id($request['id']);
        $mobile = $mobile['body']['data'];
        // dd($mobile);
        $html = '<input type="hidden" name="id" id="id_role" value="' . $mobile['id'] . '">
        <div class="form-group">
            <label for="name" class="col-sm-12 control-label">Keterangan</label>
            <div class="col-sm-12">
                <textarea name="keterangan" id="keterangan" class="form-control" rows="3">' . $mobile['keterangan'] . '</textarea>
            </div>
        </div>
        <table class="table" >
            <tr>
                <th>No</th>
                <th>Program</th>
                <th class="text-center">Status</th>
            </tr>
            <input type="hidden" name="jumlah" value="' . count($mobile['program']) . '"';
        $no = 1;
        foreach ($mobile['program'] as $program) {
            $checked = '';
            if ($program['status'] == true) {
                $checked = 'checked';
            }
            $html .= '
                <tr>
                <input type="hidden" name="id_program_' . $no . '" value="' . $program['id'] . '">
                    <td class="vertical-middle">' . $no . '.</td>
                    <td class="vertical-middle">' . $program['nama'] . '</td>
                    <td class="text-center">
                            <label class="switch mb-0">
                            <input type="checkbox" name="program_check_' . $no . '" ' . $checked . '>
                            <span class="slider round"></span>
                        </label>
                    </td>
                </tr>';
            $no++;
        }
        $html .= '</table>';

        $data = array(
            'role' => $mobile['role'],
            'keterangan' => $mobile['keterangan'],
            'isi' => $html
        );
        return response()->json($data);
    }

    public function update(Request $request)
    {
        // dd($request);
        $id_program = [];
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            if($request['program_check_'.$i]){
                $id_program[] = $request['id_program_'.$i];
            }
        }
        $dt_program = implode(',', $id_program);
        $data = array(
            'id' => $request['id'],
            'keterangan' => $request['keterangan'],
            'aplikasi' => $dt_program
        );
        $result = $this->mobileApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
