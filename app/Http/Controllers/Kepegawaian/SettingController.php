<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\SettingApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SettingController extends Controller
{
    private $settingApi;

    public function __construct()
    {
        $this->settingApi = new SettingApi();
    }

    public function create()
    {
        session()->put('title', 'Data Setting');
        $setting = $this->settingApi->sekolah(session('id_sekolah'));
        // dd($setting);
        $setting = $setting['body']['data'];
        return view('content.kepegawaian.v_pengaturan')->with(['template' => 'default', 'setting' => $setting]);
    }

    public function save(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'jenjang' => $request->jenjang,
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'provinsi' => $request->provinsi,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->settingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->settingApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
