<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\PegawaiApi;
use App\ApiService\Master\EnvironmentApi;
use App\ApiService\Master\WhatsappApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WhatsappController extends Controller
{
    private $pegawaiApi;
    private $watsapApi;
    private $envApi;

    public function __construct()
    {
        $this->pegawaiApi = new PegawaiApi();
        $this->watsapApi = new WhatsappApi();
        $this->envApi = new EnvironmentApi();
    }

    public function pesan()
    {
        session()->put('title', 'Kirim Pesan WhatsApp');
        $pegawai = $this->pegawaiApi->sekolah();
        $pegawai = $pegawai['body']['data'];
        // dd($pegawai);
        return view('content.kepegawaian.whatsapp.v_create_wa')->with(['template' => 'default', 'pegawai' => $pegawai]);
    }

    public function send(Request $request)
    {
        // dd($request);
        foreach ($request['kontak'] as $kn) {
            $data = array(
                'number' => $kn,
                'message' => $request['pesan']
            );
            $result = $this->watsapApi->send_notif(session('id_sekolah'), json_encode($data));
        }

        if ($result['code'] == 200) {
            return response()->json([
                'message' => 'Pesan berhasil terkirim',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function setting()
    {
        session()->put('title', 'Data Setting');
        $setting = $this->envApi->by_jenis_sekolah("whatsapp_blast", session('id_sekolah'));
        $setting = $setting['body']['data'];
        return view('content.kepegawaian.whatsapp.v_setting_wa')->with(['template' => 'default', 'setting' => $setting]);
    }
}
