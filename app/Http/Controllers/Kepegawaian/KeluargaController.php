<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\KeluargaApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KeluargaController extends Controller
{
    private $keluargaApi;

    public function __construct()
    {
        $this->keluargaApi = new KeluargaApi();
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'id_pegawai' => $request->id_pegawai,
            'pekerjaan' => $request->pekerjaan,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
            'tgl_nikah' => date('Y-m-d', strtotime($request->tgl_nikah)),
            'ke' => $request->ke,
            'nik' => $request->nik,
            'penghasilan' => str_replace('.', '', $request->penghasilan),
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->keluargaApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'id_pegawai' => $request->id_pegawai,
            'pekerjaan' => $request->pekerjaan,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
            'tgl_nikah' => date('Y-m-d', strtotime($request->tgl_nikah)),
            'ke' => $request->ke,
            'nik' => $request->nik,
            'penghasilan' => str_replace('.', '', $request->penghasilan),
            'id_sekolah' => session('id_sekolah')
        );
        $keluarga = $this->keluargaApi->update_info(json_encode($data));
        if ($keluarga['code'] == 200) {
            return response()->json([
                'message' => $keluarga['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $keluarga['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $keluarga = $this->keluargaApi->get_by_id($request['id']);
        $keluarga = $keluarga['body']['data'];
        $keluarga['penghasilan'] = str_replace(',', '.', number_format($keluarga['penghasilan']));
        return response()->json($keluarga);
    }

    public function delete(Request $request)
    {
        $delete = $this->keluargaApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
