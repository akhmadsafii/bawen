<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\GajiApi;
use App\ApiService\Kepegawaian\PegawaiApi;
use App\ApiService\Kepegawaian\TunjanganPotonganPegApi;
use App\ApiService\Kepegawaian\TunjanganPotonganTahunApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenggajianController extends Controller
{
    private $pegawaiApi;
    private $tahunTunjanganApi;
    private $gajiApi;
    private $tunjanganPegawaiApi;

    public function __construct()
    {
        $this->pegawaiApi = new PegawaiApi();
        $this->tahunTunjanganApi = new TunjanganPotonganTahunApi();
        $this->gajiApi = new GajiApi();
        $this->tunjanganPegawaiApi = new TunjanganPotonganPegApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Pegawai');
        // dd(Help::getMonthToIndo($_GET['bulan']));
        $pegawai = $this->gajiApi->by_profile(ucwords($_GET['bulan']), $_GET['tahun']);
        $result = $pegawai['body']['data'];
        // dd($result);
        if ($request->ajax()) {
            $table =  datatables()->of($result)
                ->addColumn('action', function ($data) {
                    if ($data['gaji_kotor'] != null) {
                        $button = '<a href="' . route('kepegawaian-tunjangan_penggajian_detail', ['method' => 'detail',  'nip' => $data['nip'], 'k' => Help::encode($data['id']), 'bulan' => $_GET['bulan'], 'tahun' => $_GET['tahun']]) . '" class="btn btn-success btn-sm text-white"><i class="fas fa-info-circle"></i> detail</a>';
                    } else {
                        $button = '<a href="' . route('kepegawaian-tunjangan_penggajian_detail', ['method' => 'insert',  'nip' => $data['nip'], 'k' => Help::encode($data['id']), 'bulan' => $_GET['bulan'], 'tahun' => $_GET['tahun']]) . '" class="btn btn-info btn-sm text-white"><i class="fas fa-plus"></i> insert</a>';
                    }
                    return $button;
                });
            $table->editColumn('gaji', function ($row) {
                if ($row['gaji_bersih'] != null) {
                    return 'Rp. ' . str_replace(',', '.', number_format($row['gaji_bersih']));
                } else {
                    return '<span class="badge badge-danger text-inverse">Tidak ada slip gaji</span>';
                }
            })
                ->rawColumns(['action', 'gaji'])
                ->addIndexColumn();
            return $table->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-kepegawaian' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.kepegawaian.penggajian.v_list_pegawai')->with(['template' => $template]);
    }

    public function detail()
    {
        $detail = $this->gajiApi->detail_pegawai(Help::decode($_GET['k']), ucwords($_GET['bulan']), $_GET['tahun']);
        $detail = $detail['body']['data'];
        // dd($detail);
        $pegawai = $this->pegawaiApi->get_by_id(Help::decode($_GET['k']));
        // dd($pegawai);
        $pegawai = $pegawai['body']['data'];
        if ($_GET['method'] == 'insert') {
            session()->put('bulan_sesi', $_GET['bulan']);
            session()->put('redirect_prev', url()->previous());
            session()->put('tahun_sesi', $_GET['tahun']);
            $tahun_tunjangan = $this->tahunTunjanganApi->by_tahun($_GET['tahun']);
            $tahun_tunjangan = $tahun_tunjangan['body']['data'];
            return view('content.kepegawaian.penggajian.v_insert_gaji')->with(['template' => 'default', 'pegawai' => $pegawai, 'tunjangan' => $tahun_tunjangan]);
        } elseif ($_GET['method'] == 'edit') {
            session()->put('redirect_prev', url()->previous());
            // dd($detail);
            return view('content.kepegawaian.penggajian.v_edit_gaji')->with(['template' => 'default', 'detail' => $detail]);
        } else {
            return view('content.kepegawaian.penggajian.v_detail_gaji')->with(['template' => 'default', 'detail' => $detail]);
        }
    }

    public function storeMany(Request $request)
    {
        // dd($request);
        $array_tunjangan = [];
        $array_potongan = [];
        for ($i = 1; $i <= $request['jumlah_tunjangan']; $i++) {
            $nominal = str_replace('.', '', $request['nominal_tunjangan_' . $i]);
            $array_tunjangan[] = array(
                "id" => $request['id_tunjangan_' . $i],
                "nominal" => $nominal
            );
        }
        for ($p = 1; $p <= $request['jumlah_potongan']; $p++) {
            $nominal = str_replace('.', '', $request['nominal_potongan_' . $p]);
            $array_potongan[] = array(
                "id" => $request['id_potongan_' . $p],
                "nominal" => $nominal
            );
        }
        // dd($result);
        $data = array(
            'id_pegawai' => Help::decode($request['id_pegawai']),
            'bulan' => $request['bulan'],
            'tahun_ajaran' => $request['tahun_ajaran'],
            'gaji_pokok' => str_replace('.', '', $request['gaji_pokok']),
            'gaji_bersih' => str_replace('.', '', $request['gaji_bersih']),
            'gaji_kotor' => str_replace('.', '', $request['gaji_kotor']),
            'tgl_bayar' => date('Y-m-d', strtotime($request->tanggal)),
            'tunjangan_potongans' => array_merge($array_tunjangan, $array_potongan),
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        // dd($_GET);
        // if($request['method'] == 'insert'){
        //     $data_peg_th = array(
        //         'id_pegawai' => Help::decode($request['id_pegawai']),
        //         'tahun_ajaran' => $request['tahun_ajaran'],
        //         'tunjangan_potongans' => array_merge($array_tunjangan, $array_potongan),
        //         'id_sekolah' => session('id_sekolah')
        //     );
        //     $result_tunjangan = $this->tunjanganPegawaiApi->store_many(json_encode($data_peg_th));
        // }
        $result = $this->gajiApi->store_detail(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                // 'previous_back' => route('kepegawaian-tunjangan_penggajian', ['bulan' => session('bulan_sesi'), 'tahun' => session('tahun_sesi')])
                'previous_back' => session('redirect_prev')
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
