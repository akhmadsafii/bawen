<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\TunjanganPotonganTahunApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TunjanganPotonganTahunController extends Controller
{
    private $tunjanganApi;
    private $tahunApi;

    public function __construct()
    {
        $this->tunjanganApi = new TunjanganPotonganTahunApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Setting Tunjangan & Potongan');
        $tahun = $this->tahunApi->get_tahun_ajaran();
        $tahun = $tahun['body']['data'];
        $tunjangan = $this->tunjanganApi->by_tahun($_GET['tahun']);
        // dd($tunjangan);
        $tunjangan = $tunjangan['body']['data'];
        return view('content.kepegawaian.v_tunjangan_potongan_tahun')->with([
            'template' => 'default', 'tahun' => $tahun, 'tunjangan' => $tunjangan
        ]);
    }

    public function store_many(Request $request)
    {
        // dd($request);
        $tunjangan_potongans = [];
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            $tunjangan_potongans[] = array(
                "id" => $request['id_' . $i],
                "nominal" => str_replace('.', '', $request['nominal_' . $i]),
                "jenis" => $request['jenis']
            );
        }
        $next_tahun = $request['tahun'] + 1;
        $data = array(
            'tahun_ajaran' => $request['tahun'] . "/" . $next_tahun,
            'id_sekolah' => session('id_sekolah'),
            'tunjangan_potongans' => $tunjangan_potongans
        );
        $result = $this->tunjanganApi->setting_many(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
