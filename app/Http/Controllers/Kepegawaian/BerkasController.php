<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\FileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class BerkasController extends Controller
{
    private $berkasApi;

    public function __construct()
    {
        $this->berkasApi = new FileApi();
    }

    public function store(Request $request)
    {
        $data = array(
            'id_jenis_file' => $request->id_jenis,
            'keterangan' => $request->keterangan,
            'id_pegawai' => $request->id_pegawai,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->berkasApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->berkasApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'id_jenis_file' => $request->id_jenis,
            'keterangan' => $request->keterangan,
            'id_pegawai' => $request->id_pegawai,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->berkasApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->berkasApi->update_info(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $berkas = $this->berkasApi->get_by_id($request['id']);
        $berkas = $berkas['body']['data'];
        return response()->json($berkas);
    }

    public function delete(Request $request)
    {
        $delete = $this->berkasApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function download()
    {
        $post  = $this->berkasApi->get_by_id(Help::decode($_GET['k']));
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        $fileSource = $result['file'];
        $fileName   = end($nama_file);
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
