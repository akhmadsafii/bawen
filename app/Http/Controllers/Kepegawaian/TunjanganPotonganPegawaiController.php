<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\TunjanganPotonganPegApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TunjanganPotonganPegawaiController extends Controller
{
    private $tunpotApi;
    private $tunjanganPegawaiApi;

    public function __construct()
    {
        $this->tunjanganPegawaiApi = new TunjanganPotonganPegApi();
    }

    public function store(Request $request)
    {
        $array_tunjangan = [];
        $array_potongan = [];
        for ($i = 1; $i <= $request['jumlah_tunjangan']; $i++) {
            $array_tunjangan[] = array(
                "id" => $request['id_tunjangan_' . $i],
                "status_terima" => empty($request['tunjangan_check_' . $i]) ? 0 : 1,
            );
        }
        for ($p = 1; $p <= $request['jumlah_potongan']; $p++) {
            $array_potongan[] = array(
                "id" => $request['id_potongan_' . $p],
                "status_terima" => empty($request['potongan_check_' . $p]) ? 0 : 1,
            );
        }
        $next_tahun = $request['tahun'] + 1;
        $data = array(
            'id_pegawai' => Help::decode($request['pegawai']),
            'tahun_ajaran' => $request['tahun'] . "/" . $next_tahun,
            'tunjangan_potongans' => array_merge($array_tunjangan, $array_potongan),
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->tunjanganPegawaiApi->store_many(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }
}
