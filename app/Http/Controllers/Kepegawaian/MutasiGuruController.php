<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Cbt\JenisApi;
use App\ApiService\Kepegawaian\GolonganApi;
use App\ApiService\Kepegawaian\JabatanApi;
use App\ApiService\Kepegawaian\MutasiGuruApi;
use App\ApiService\Kepegawaian\PangkatApi;
use App\ApiService\Kepegawaian\PegawaiApi;
use App\ApiService\Kepegawaian\SKApi;
use App\ApiService\Kepegawaian\StatusApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MutasiGuruController extends Controller
{
    private $mutasiApi;
    private $tahunApi;
    private $skApi;
    private $pangkatApi;
    private $jenisApi;
    private $statusApi;
    private $jabatanApi;
    private $pegawaiApi;
    private $golonganApi;

    public function __construct()
    {
        $this->tahunApi = new TahunAjaranApi();
        $this->mutasiApi = new MutasiGuruApi();
        $this->skApi = new SKApi();
        $this->pangkatApi = new PangkatApi();
        $this->jenisApi = new JenisApi();
        $this->statusApi = new StatusApi();
        $this->jabatanApi = new JabatanApi();
        $this->pegawaiApi = new PegawaiApi();
        $this->golonganApi = new GolonganApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Mutasi');
        $tahun = $this->tahunApi->get_tahun_ajaran();
        $tahun = $tahun['body']['data'];
        $jabatan = $this->jabatanApi->sekolah();
        $jabatan = $jabatan['body']['data'];
        $golongan = $this->golonganApi->sekolah();
        $golongan = $golongan['body']['data'];
        $pangkat = $this->pangkatApi->sekolah();
        $pangkat = $pangkat['body']['data'];
        if ($_GET['tahun'] == 'all' && $_GET['jenis'] == 'semua') {
            $mutasi = $this->mutasiApi->by_sekolah();
        } elseif ($_GET['tahun'] != 'all' && $_GET['jenis'] == 'semua') {
            $mutasi = $this->mutasiApi->by_tahun($_GET['tahun']);
        } elseif ($_GET['tahun'] == 'all' && $_GET['jenis'] != 'semua') {
            $mutasi = $this->mutasiApi->by_jenis($_GET['jenis']);
        } else {
            $mutasi = $this->mutasiApi->by_tahun_jenis($_GET['tahun'], $_GET['jenis']);
        }
        // dd($mutasi);
        $mutasi = $mutasi['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($mutasi)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash" style="letter-spacing: 0;"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    if ($data['jenis'] == 'keluar') {
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-purple btn-sm kembali"><i class="fas fa-undo-alt" style="letter-spacing: 0;"></i></button>&nbsp;&nbsp;';
                    }
                    return $button;
                });
            $table->editColumn('detail', function ($row) {
                return '<a href="' . route('kepegawaian-mutasi_riwayat_pegawai', ['nip' => $row['nip'], 'k' => Help::encode($row['id'])]) . '" class="text-info"><b>' . $row['nama'] . '</b><p class="m-0">NIP. ' . $row['nip'] . '</p></a>';
            });
            $table->editColumn('jenis', function ($row) {
                if ($row['jenis'] == 'masuk') {
                    return '<i class="fas fa-user-plus fa-2x text-success"></i><br><small class="text-success">Mutasi Masuk</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                } elseif ($row['jenis'] == 'pangkat') {
                    return '<i class="fas fa-user-tie fa-2x text-purple"></i><br><small class="text-purple">Mutasi Pangkat</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                } else {
                    return '<i class="fas fa-user-minus fa-2x text-danger"></i><br><small class="text-danger">Mutasi Keluar</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                }
            });
            $table->editColumn('jabatan', function ($row) {
                if ($row['jabatan_pegawai'] != null) {
                    return '<div class="text-center"><b>' . $row['jabatan_pegawai'] . '</b><p>' . Help::getTanggal($row['tmt_jabatan_pegawai']) . '</p></div>';
                } else {
                    return '<span class="badge badge-info text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('pangkat', function ($row) {
                if ($row['pangkat_pegawai'] != null) {
                    return '<div class="text-center"><b>' . $row['pangkat_pegawai'] . '</b><p>' . Help::getTanggal($row['tmt_pangkat_pegawai']) . '</p></div>';
                } else {
                    return '<span class="badge badge-info text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('golongan', function ($row) {
                if ($row['golongan_pegawai'] != null) {
                    return '<div class="text-center"><b>' . $row['golongan_pegawai'] . '</b><p>' . Help::getTanggal($row['tmt_golongan']) . '</p></div>';
                } else {
                    return '<span class="badge badge-info text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('gaji', function ($row) {
                if ($row['gaji_pokok'] != null) {
                    return '<div class="text-center"><b>' . str_replace(',', '.', number_format($row['gaji_pokok'])) . '</b><p>' . Help::getTanggal($row['tmt_gaji']) . '</p></div>';
                } else {
                    return '<span class="badge badge-info text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('pensiun', function ($row) {
                if ($row['pensiun'] != null) {
                    return '<h3 class="m-0">' . $row['pensiun'] . '<sup style="font-size: x-small;">THN</sup></h3><p class="m-0">' . Help::getTanggal($row['tmt_pensiun']) . '</p>';
                } else {
                    return '<span class="badge badge-info text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('tahun', function ($row) {
                return '<b>' . $row['tahun_ajaran'] . '</b><p class="m-0">Keterangan : ' . $row['keterangan'] . '</p>';
            })
                ->rawColumns(['action', 'detail', 'jenis', 'tahun', 'jabatan', 'pangkat', 'golongan', 'gaji', 'pensiun'])
                ->addIndexColumn();
            return $table->make(true);
        }
        $sk = $this->skApi->sekolah();
        $sk = $sk['body']['data'];

        $jenis = $this->jenisApi->by_sekolah();
        $jenis = $jenis['body']['data'];
        $status = $this->statusApi->sekolah();
        $status = $status['body']['data'];

        return view('content.kepegawaian.mutasi.mutasi_masuk')->with([
            'template' => 'default', 'tahun' => $tahun, 'sk' => $sk, 'golongan' => $golongan,
            'jenis' => $jenis, 'status' => $status, 'jabatan' => $jabatan
        ]);
    }

    public function store_in(Request $request)
    {
        foreach ($request['id_pegawai'] as $id_pegawai) {
            $data = array(
                'id_pegawai' => $id_pegawai,
                'tgl_mutasi' => date('Y-m-d', strtotime($request->tanggal_mutasi)),
                'keterangan' => $request->keterangan,
                'tahun_ajaran' => $request->tahun_ajaran,
                'id_sekolah' => session('id_sekolah')
            );
            $result = $this->mutasiApi->create_v2(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function store_out(Request $request)
    {
        foreach ($request['id_pegawai'] as $id_pegawai) {
            $data = array(
                'id_pegawai' => $id_pegawai,
                'tgl_mutasi' => date('Y-m-d', strtotime($request->tanggal_mutasi)),
                'keterangan' => $request->keterangan,
                'tahun_ajaran' => $request->tahun_ajaran,
                'id_sekolah' => session('id_sekolah')
            );
            $result = $this->mutasiApi->create_out(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function detail(Request $request)
    {
        // dd($request);
        $mutasi = $this->mutasiApi->get_by_id($request['id']);
        // dd($mutasi);
        $mutasi = $mutasi['body']['data'];
        return response()->json($mutasi);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'id_pegawai' => $request['id_pegawai'],
            'keterangan' => $request->keterangan,
            'tgl_mutasi' => date('Y-m-d', strtotime($request->tanggal_mutasi)),
            'tahun_ajaran' => $request->tahun_ajaran,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->mutasiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->mutasiApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function kembalikan(Request $request)
    {
        $kembali = $this->mutasiApi->kembalikan($request['id']);
        if ($kembali['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $kembali['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $kembali['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function create(Request $request)
    {
        $pegawai = $this->pegawaiApi->sekolah();
        $pegawai = $pegawai['body']['data'];
        // dd($pegawai);
        $tahun = $this->tahunApi->get_tahun_ajaran();
        $tahun = $tahun['body']['data'];
        $golongan = $this->golonganApi->sekolah();
        $golongan = $golongan['body']['data'];
        $pangkat = $this->pangkatApi->sekolah();
        $pangkat = $pangkat['body']['data'];
        $jabatan = $this->jabatanApi->sekolah();
        $jabatan = $jabatan['body']['data'];
        // dd($jabatan);
        if ($request->ajax()) {
            $table = datatables()->of($pegawai)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-danger btn-sm m-1 mutasi" data-id="' . $data['id'] . '"><i class="fas fa-user-alt-slash"></i> Keluar</a>';
                    $button .= '<a href="javascript:void(0)" class="btn btn-info btn-sm m-1 jabatan" data-id="' . $data['id'] . '"><i class="fas fa-id-card-alt"></i> Jabatan</a>';
                    return $button;
                });
            $table->editColumn('checkbox', function ($row) {
                return '<input type="checkbox" class="check_pegawai" value="' . $row['id'] . '">';
            });
            $table->editColumn('pegawai', function ($row) {
                return '<a href="' . route('kepegawaian-mutasi_riwayat_pegawai', ['nip' => $row['nip'], 'k' => Help::encode($row['id'])]) . '" class="text-info"><b>' . $row['nama'] . '</b><p class="my-0">NIP. ' . $row['nip'] . '</p></a>';
            });
            $table->editColumn('jabatan', function ($row) {
                if ($row['jabatan_pegawai'] != null) {
                    return '<b>' . $row['jabatan_pegawai'] . '</b><p class="my-0">Pangkat. ' . $row['pangkat_pegawai'] . '</p><small>' . Help::getTanggal($row['tmt_capeg']) . '</small>';
                } else {
                    return '-';
                }
            });
            $table->editColumn('jenis', function ($row) {
                if ($row['mutasi_last']['id'] != null) {
                    $jenis_mutasi = $row['mutasi_last']['jenis'];
                    if ($jenis_mutasi == 'masuk') {
                        return '<div class="text-center"><i class="fas fa-sign-in-alt fa-2x text-success text-center"></i><p class="m-0 text-success">Masuk</p><small>' . Help::getTanggal($row['mutasi_last']['tgl_mutasi']) . '</small></div>';
                    } elseif ($jenis_mutasi == 'keluar') {
                        return '<div class="text-center"><i class="fas fa-sign-out-alt fa-2x text-danger text-center"></i><p class="m-0 text-danger">Keluar</p><small>' . Help::getTanggal($row['mutasi_last']['tgl_mutasi']) . '</small></div>';
                    } else {
                        return '<div class="text-center"><i class="fas fa-user-tie fa-2x text-purple text-center"></i><p class="m-0 text-purple">Pangkat</p><small>' . Help::getTanggal($row['mutasi_last']['tgl_mutasi']) . '</small></div>';
                    }
                } else {
                    return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('pangkat', function ($row) {
                if ($row['mutasi_last']['id'] != null) {
                    $jenis = $row['mutasi_last']['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<b>' . $row['mutasi_last']['pangkat_pegawai'] . '</b><p class="m-0">' . Help::getTanggal($row['mutasi_last']['tmt_pangkat_pegawai']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                } else {
                    return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('golongan', function ($row) {
                if ($row['mutasi_last']['id'] != null) {
                    $jenis = $row['mutasi_last']['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<b>' . $row['mutasi_last']['golongan_pegawai'] . '</b><p class="m-0">' . Help::getTanggal($row['mutasi_last']['tmt_golongan_pegawai']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                } else {
                    return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                }
            });
            $table->editColumn('pensiun', function ($row) {
                if ($row['mutasi_last']['id'] != null) {
                    $jenis = $row['mutasi_last']['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<h3 class="m-0">' . $row['mutasi_last']['pensiun'] . '<sup style="font-size: x-small;">THN</sup></h3><p class="m-0">' . Help::getTanggal($row['mutasi_last']['tmt_pensiun']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                } else {
                    return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                }
            });
            $table->rawColumns(['action', 'checkbox', 'pegawai', 'jabatan', 'jenis', 'pangkat', 'pensiun', 'golongan']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.kepegawaian.mutasi.v_create_mutasi')->with([
            'template' => 'default', 'tahun' => $tahun, 'golongan' => $golongan, 'pangkat' => $pangkat,
            'jabatan' => $jabatan
        ]);
    }

    public function get_pegawai(Request $request)
    {
        $html = '<table class="table table-bordered">
        <tr>
            <th>Nama</th>
            <th>Jabatan</th>
            <th></th>
        </tr>';
        $no = 1;
        foreach ($request['pegawai'] as $pg) {
            $dt_pegawai = $this->pegawaiApi->get_by_id($pg);
            $dt_pegawai = $dt_pegawai['body']['data'];
            $html .= ' <tr id="row' . $no . '">
                    <input type="hidden" name="id_pegawai[]" value="' . $dt_pegawai['id'] . '">
                    <td class="vertical-middle"> <b>' . $dt_pegawai['nama'] . '</b><p class="m-0">NIP. ' . $dt_pegawai['nip'] . '</p></td>
                    <td class="vertical-middle">' . $dt_pegawai['jabatan_pegawai'] . '</td>
                    <td class="text-center"><button class="btn btn-danger btn-sm btn-remove" id="' . $no . '"><i class="fas fa-trash-alt"></i></button></td>
                </tr>';
            $no++;
        }

        return response()->json($html);
    }

    public function store_jabatan(Request $request)
    {
        // dd($request);
        foreach ($request['id_pegawai'] as $id_pegawai) {
            $data = array(
                'id_pegawai' => $id_pegawai,
                'gaji_pokok' => str_replace('.', '', $request->gaji),
                'id_jabatan_pegawai' => $request->id_jabatan,
                'id_pangkat_pegawai' => $request->id_pangkat,
                'jenis' => "pangkat",
                'tgl_mutasi' => date('Y-m-d', strtotime($request->tgl_mutasi)),
                'id_pangkat_pegawai' => $request->id_pangkat,
                'tmt_gaji' => date('Y-m-d', strtotime($request->tmt_gaji)),
                'tmt_jabatan_pegawai' => date('Y-m-d', strtotime($request->tmt_jabatan)),
                'tmt_pangkat_pegawai' => $request->tahun . "-" . $request->bulan . "-01",
                'id_golongan' => $request->id_golongan,
                'tmt_golongan' => date('Y-m-d', strtotime($request->tmt_golongan)),
                'pensiun' => $request->pensiun,
                'tmt_pensiun' => date('Y-m-d', strtotime($request->tmt_pensiun)),
                'keterangan' => $request->keterangan,
                'tahun_ajaran' => $request->tahun_ajaran,
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->mutasiApi->create_pangkat(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function riwayat_pegawai(Request $request)
    {
        $mutasi = $this->mutasiApi->by_pegawai(Help::decode($_GET['k']));
        // dd($mutasi);
        $mutasi = $mutasi['body']['data'];
        $pegawai = $this->pegawaiApi->get_by_id(Help::decode($_GET['k']));
        // dd($pegawai);
        $pegawai = $pegawai['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($mutasi)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt" style="letter-spacing: 0;"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    if ($data['jenis'] == 'keluar') {
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-purple btn-sm kembali"><i class="fas fa-undo-alt" style="letter-spacing: 0;"></i></button>&nbsp;&nbsp;';
                    }
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash" style="letter-spacing: 0;"></i></button>';
                    return $button;
                });
            $table->editColumn('jenis', function ($row) {
                if ($row['jenis'] == 'masuk') {
                    return '<i class="fas fa-user-plus fa-2x text-success"></i><br><small class="text-success">Mutasi Masuk</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                } elseif ($row['jenis'] == 'pangkat') {
                    return '<i class="fas fa-user-tie fa-2x text-purple"></i><br><small class="text-purple">Mutasi Pangkat</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p> <a href="javascript:void(0)" class="detail" data-id="' . $row['id'] . '"><small class="text-info"><i class="fas fa-info-circle"></i> Lihat detail </small></a>';
                } else {
                    return '<i class="fas fa-user-minus fa-2x text-danger"></i><br><small class="text-danger">Mutasi Keluar</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                }
            })
                ->rawColumns(['action', 'jenis'])
                ->addIndexColumn();
            return $table->make(true);
        }
        return view('content.kepegawaian.mutasi.v_riwayat_mutasi')->with([
            'template' => 'default', 'mutasi' => $mutasi, 'pegawai' => $pegawai
        ]);
    }
}
