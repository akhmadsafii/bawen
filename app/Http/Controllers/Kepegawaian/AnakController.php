<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\AnakApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnakController extends Controller
{
    private $anakApi;

    public function __construct()
    {
        $this->anakApi = new AnakApi();
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'id_pegawai' => $request->id_pegawai,
            'nik' => $request->nik,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tahun . "-" . $request->bulan . "-" . $request->tanggal,
            'status_anak' => $request->status_anak,
            'jenkel' => $request->jenkel,
            'ke' => $request->ke,
            'status_nikah' => $request->status_nikah,
            'status_tunjangan' => $request->status_tunjangan,
            'status_kerja' => $request->status_kerja,
            'sekolah' => $request->sekolah,
            'putusan' => $request->putusan,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->anakApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'id_pegawai' => $request->id_pegawai,
            'nik' => $request->nik,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tahun . "-" . $request->bulan . "-" . $request->tanggal,
            'status_anak' => $request->status_anak,
            'jenkel' => $request->jenkel,
            'ke' => $request->ke,
            'status_nikah' => $request->status_nikah,
            'status_tunjangan' => $request->status_tunjangan,
            'status_kerja' => $request->status_kerja,
            'sekolah' => $request->sekolah,
            'putusan' => $request->putusan,
            'id_sekolah' => session('id_sekolah')
        );
        $anak = $this->anakApi->update_info(json_encode($data));
        if ($anak['code'] == 200) {
            return response()->json([
                'message' => $anak['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $anak['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $anak = $this->anakApi->get_by_id($request['id']);
        $anak = $anak['body']['data'];
        $anak['tanggal'] = date('d', strtotime($anak['tgl_lahir']));
        $anak['bulan'] = date('m', strtotime($anak['tgl_lahir']));
        $anak['tahun'] = date('Y', strtotime($anak['tgl_lahir']));
        return response()->json($anak);
    }

    public function delete(Request $request)
    {
        $delete = $this->anakApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
