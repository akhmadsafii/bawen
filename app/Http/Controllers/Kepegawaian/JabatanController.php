<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\JabatanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    private $jabatanApi;

    public function __construct()
    {
        $this->jabatanApi = new JabatanApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Jabatan');
        $jabatan = $this->jabatanApi->sekolah();
        // dd($jabatan);
        $result = $jabatan['body']['data'];
        // dd($result);
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-kepegawaian' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.kepegawaian.v_jabatan')->with(['template' => $template]);
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah')
            );
            $result = $this->jabatanApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'id_sekolah' => session('id_sekolah'),
            ];

        $jabatan = $this->jabatanApi->update_info(json_encode($data));
        // dd($jurusan);
        if ($jabatan['code'] == 200) {
            return response()->json([
                'message' => $jabatan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $jabatan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $jabatan = $this->jabatanApi->get_by_id($request['id']);
        $jabatan = $jabatan['body']['data'];
        return response()->json($jabatan);
    }

    public function delete(Request $request)
    {
        $delete = $this->jabatanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
