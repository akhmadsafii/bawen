<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\AnakApi;
use App\ApiService\Kepegawaian\FileApi;
use App\ApiService\Kepegawaian\GajiApi;
use App\ApiService\Kepegawaian\JabatanApi;
use App\ApiService\Kepegawaian\JenisApi;
use App\ApiService\Kepegawaian\JenisFileApi;
use App\ApiService\Kepegawaian\KeluargaApi;
use App\ApiService\Kepegawaian\MutasiGuruApi;
use App\ApiService\Kepegawaian\PangkatApi;
use App\ApiService\Kepegawaian\PegawaiApi;
use App\ApiService\Kepegawaian\SKApi;
use App\ApiService\Kepegawaian\StatusApi;
use App\ApiService\Kepegawaian\TunjanganPotonganPegApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    private $pegawaiApi;
    private $pangkatApi;
    private $jenisApi;
    private $statusApi;
    private $jabatanApi;
    private $skApi;
    private $keluargaApi;
    private $anakApi;
    private $fileApi;
    private $JenisFileApi;
    private $gajiApi;
    private $tahunApi;
    private $tunjanganPegawaiApi;
    private $mutasiApi;

    public function __construct()
    {
        $this->pegawaiApi = new PegawaiApi();
        $this->pangkatApi = new PangkatApi();
        $this->jenisApi = new JenisApi();
        $this->statusApi = new StatusApi();
        $this->jabatanApi = new JabatanApi();
        $this->skApi = new SKApi();
        $this->keluargaApi = new KeluargaApi();
        $this->anakApi = new AnakApi();
        $this->fileApi = new FileApi();
        $this->JenisFileApi = new JenisFileApi();
        $this->gajiApi = new GajiApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->tunjanganPegawaiApi = new TunjanganPotonganPegApi();
        $this->mutasiApi = new MutasiGuruApi();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        session()->put('title', 'Data Pegawai');
        $pangkat = $this->pegawaiApi->sekolah();
        $result = $pangkat['body']['data'];
        // dd($result);
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    if (session('role') == 'admin-kepegawaian') {
                        $button = '<a href="' . route('kepegawaian-pegawai_edit', ['method' => 'biodata',  'nip' => $data['nip'], 'k' => Help::encode($data['id'])]) . '" class="btn btn-info btn-sm m-1"><i class="fas fa-info-circle"></i></a>';
                        $button .= '<a href="' . route('kepegawaian-pegawai_edit', ['method' => 'setting',  'nip' => $data['nip'], 'k' => Help::encode($data['id']), 'tahun' => session('tahun')]) . '" class="btn btn-warning btn-sm m-1"><i class="fas fa-cog"></i></i></a>';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-purple btn-sm reset_pass m-1"><i class="fas fa-key"></i></button>';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete m-1"><i class="fa fa-trash"></i></button>';
                        return $button;
                    } else {
                        return '<a href="' . route('kepegawaian-pegawai_edit', ['method' => 'biodata',  'nip' => $data['nip'], 'k' => Help::encode($data['id'])]) . '" class="btn btn-info btn-sm m-1"><i class="fas fa-info-circle"></i></a>';
                    }
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        // $template = session('template');
        // if (session('role') == 'admin-kepegawaian' || session('role') == 'admin') {
        //     $template = 'default';
        // }
        return view('content.kepegawaian.pegawai.v_pegawai')->with(['template' => 'default']);
    }

    public function add()
    {
        // dd("tes");
        $golongan = $this->pangkatApi->sekolah();
        // dd($golongan);
        $golongan = $golongan['body']['data'];
        $jenis = $this->jenisApi->sekolah();
        $jenis = $jenis['body']['data'];
        $status = $this->statusApi->sekolah();
        $status = $status['body']['data'];
        $jabatan = $this->jabatanApi->sekolah();
        $jabatan = $jabatan['body']['data'];
        $sk = $this->skApi->sekolah();
        $sk = $sk['body']['data'];
        return view('content.kepegawaian.pegawai.v_add_pegawai')->with([
            'template' => 'default', 'golongan' => $golongan, 'sk' => $sk,
            'jenis' => $jenis, 'status' => $status, 'jabatan' => $jabatan
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nip' => $request->nip,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'first_password' => $request->password,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tahun . "-" . $request->bulan . "-" . $request->tanggal,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->desa,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'id_sk' => $request->sk,
            'id_pangkat_pegawai' => $request->golongan,
            'id_jenis_pegawai' => $request->id_jenis_golongan,
            'tmt_capeg' => date('Y-m-d', strtotime($request->tmt_capeg)),
            'tmt_golongan' => date('Y-m-d', strtotime($request->tmt_golongan)),
            'id_status_pegawai' => $request->id_status_pegawai,
            'id_jabatan_pegawai' => $request->id_jabatan_pegawai,
            'digaji_menurut' => $request->digaji_menurut,
            'gaji_pokok' => str_replace('.', '', $request->gaji_pokok),
            'masa_kerja_golongan' => $request->masa_kerja_golongan,
            'masa_kerja_keseluruhan' => $request->masa_kerja_keseluruhan,
            'npwp' => $request->npwp,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->pegawaiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd(session()->all());
        $pegawai = $this->pegawaiApi->get_by_id(Help::decode($_GET['k']));
        // dd($pegawai);
        $pegawai = $pegawai['body']['data'];
        if ($_GET['method'] == 'biodata') {
            $golongan = $this->pangkatApi->sekolah();
            // dd($golongan);
            $golongan = $golongan['body']['data'];
            $jenis = $this->jenisApi->sekolah();
            $jenis = $jenis['body']['data'];
            $status = $this->statusApi->sekolah();
            $status = $status['body']['data'];
            $jabatan = $this->jabatanApi->sekolah();
            $jabatan = $jabatan['body']['data'];
            $sk = $this->skApi->sekolah();
            $sk = $sk['body']['data'];
            return view('content.kepegawaian.pegawai.v_detail_pegawai')->with([
                'template' => 'default', 'pegawai' => $pegawai, 'golongan' => $golongan,
                'sk' => $sk, 'jenis' => $jenis, 'status' => $status, 'jabatan' => $jabatan
            ]);
        }

        if ($_GET['method'] == 'keluarga') {
            $keluarga = $this->keluargaApi->by_pegawai(Help::decode($_GET['k']));
            // dd($keluarga);
            $result = $keluarga['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($result)
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm m-1"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '&nbsp;';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete m-1"><i class="fa fa-trash"></i></button>';
                        return $button;
                    });
                $table->editColumn('ttl', function ($row) {
                    return $row['tempat_lahir'] . ", " . Help::getTanggal($row['tgl_lahir']);
                });
                $table->editColumn('tgl_nikah', function ($row) {
                    return Help::getTanggal($row['tgl_nikah']);
                });
                $table->editColumn('penghasilan', function ($row) {
                    return str_replace(',', '.', number_format($row['penghasilan']));
                })
                    ->rawColumns(['action', 'ttl', 'tgl_nikah', 'penghasilan'])
                    ->addIndexColumn();
                return $table->make(true);
            }
            return view('content.kepegawaian.pegawai.v_keluarga')->with([
                'template' => 'default', 'pegawai' => $pegawai
            ]);
        }

        if ($_GET['method'] == 'anak') {
            $anak = $this->anakApi->by_pegawai(Help::decode($_GET['k']));
            // dd($anak);
            $result = $anak['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($result)
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '&nbsp;';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
                        return $button;
                    });
                $table->editColumn('ttl', function ($row) {
                    return $row['tempat_lahir'] . ", " . Help::getTanggal($row['tgl_lahir']);
                })
                    ->rawColumns(['action', 'ttl'])
                    ->addIndexColumn();
                return $table->make(true);
            }
            return view('content.kepegawaian.pegawai.v_anak')->with([
                'template' => 'default', 'pegawai' => $pegawai
            ]);
        }

        if ($_GET['method'] == 'setting') {
            // dd($pegawai);
            $tahun = $this->tahunApi->get_tahun_ajaran();
            $tahun = $tahun['body']['data'];
            $tunjangan = $this->tunjanganPegawaiApi->by_pegawai_tahun($pegawai['id'], $_GET['tahun']);
            // dd($tunjangan);
            $tunjangan = $tunjangan['body']['data'];
            return view('content.kepegawaian.v_tunjangan_potongan_pegawai')->with([
                'template' => 'default', 'tahun' => $tahun, 'tunjangan' => $tunjangan, 'pegawai' => $pegawai
            ]);
        }

        if ($_GET['method'] == 'berkas') {
            $jenis = $this->JenisFileApi->sekolah();
            $jenis = $jenis['body']['data'];
            $file = $this->fileApi->by_pegawai(Help::decode($_GET['k']));
            // dd($file);
            $result = $file['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($result)
                    ->addColumn('action', function ($data) {
                        if (session('role') == 'admin-kepegawaian' || session('id') == (new \App\Helpers\Help())->decode($_GET['k'])) {
                            $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                            $button .= '&nbsp;';
                            $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
                        } else {
                            $button = '-';
                        }
                        return $button;
                    });
                $table->editColumn('tanggal', function ($row) {
                    return Help::getTanggal($row['tanggal']);
                });
                $table->editColumn('lampiran', function ($row) {
                    $lm = explode('.', $row['file']);
                    if (end($lm) == 'jpg' || end($lm) == 'png' || end($lm) == 'jpeg') {
                        return '<a href="' . $row['file'] . '"><img src="' . $row['file'] . '" border="0" width="60" align="center" /><a>';
                    } else {
                        return '<a href="' . route('kepegawaian-download_berkas', ['k' => Help::encode($row['id'])]) . '" target="_blank"><i class="fas fa-file-alt fa-3x"></i> <br> <small>Berkas File</small></a>';
                    }
                    return Help::getTanggal($row['tanggal']);
                })
                    ->rawColumns(['action', 'tanggal', 'lampiran'])
                    ->addIndexColumn();
                return $table->make(true);
            }
            return view('content.kepegawaian.pegawai.v_berkas_file')->with([
                'template' => 'default', 'pegawai' => $pegawai, 'jenis' => $jenis
            ]);
        }

        if ($_GET['method'] == 'gaji') {
            $detail = $this->gajiApi->detail_pegawai(Help::decode($_GET['k']), ucwords($_GET['bulan']), $_GET['tahun']);
            // dd($detail);
            $detail = $detail['body']['data'];
            // dd($detail);
            if (empty($detail)) {
                $pesan = array(
                    'message' => "Penggajian belum terdaftar untuk saat ini. silahkan tambah slip gaji terlebih dahulu",
                    'icon' => 'error',
                    'status' => 'gagal'
                );
                return redirect()->back()->with('message', $pesan);
            }
            return view('content.kepegawaian.pegawai.v_penghasilan')->with([
                'template' => 'default', 'pegawai' => $pegawai, 'detail' => $detail
            ]);
        }

        if ($_GET['method'] == 'mutasi') {
            $mutasi = $this->mutasiApi->by_pegawai(Help::decode($_GET['k']));
            // dd($mutasi);
            $mutasi = $mutasi['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($mutasi)
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '&nbsp;';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
                        return $button;
                    });
                $table->editColumn('detail', function ($row) {
                    return '<b>' . $row['nama'] . '</b><p class="m-0">NIP. ' . $row['nip'] . '</p>';
                });
                $table->editColumn('jenis', function ($row) {
                    if ($row['jenis'] == 'masuk') {
                        return '<i class="fas fa-user-plus fa-2x text-success"></i><br><small class="text-success">Mutasi Masuk</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                    } elseif ($row['jenis'] == 'pangkat') {
                        return '<i class="fas fa-user-tie fa-2x text-purple"></i><br><small class="text-purple">Mutasi Pangkat</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                    } else {
                        return '<i class="fas fa-user-minus fa-2x text-danger"></i><br><small class="text-danger">Mutasi Keluar</small><p class="m-0">' . Help::getTanggal($row['tgl_mutasi']) . '</p>';
                    }
                });
                $table->editColumn('jabatan', function ($row) {
                    if ($row['jabatan_pegawai'] != null) {
                        return '<b>' . $row['jabatan_pegawai'] . '</b><p class="my-0">Pangkat. ' . $row['pangkat_pegawai'] . '</p>';
                    } else {
                        return '-';
                    }
                });
                $table->editColumn('pangkat', function ($row) {
                    $jenis = $row['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<b>' . $row['pangkat_pegawai'] . '</b><p class="m-0">' . Help::getTanggal($row['tmt_pangkat_pegawai']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                });
                $table->editColumn('golongan', function ($row) {
                    $jenis = $row['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<b>' . $row['golongan_pegawai'] . '</b><p class="m-0">' . Help::getTanggal($row['tmt_golongan']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                });
                $table->editColumn('pensiun', function ($row) {
                    $jenis = $row['jenis'];
                    if ($jenis == 'pangkat') {
                        return '<h3 class="m-0">' . $row['pensiun'] . '<sup style="font-size: x-small;">THN</sup></h3><p class="m-0">' . Help::getTanggal($row['tmt_pensiun']) . '</p>';
                    } else {
                        return '<span class="badge badge-danger text-inverse">Tidak ada</span>';
                    }
                });
                $table->rawColumns(['detail', 'jenis', 'jabatan', 'pangkat', 'golongan', 'pensiun'])
                    ->addIndexColumn();
                return $table->make(true);
            }
            return view('content.kepegawaian.pegawai.v_mutasi')->with([
                'template' => 'default', 'pegawai' => $pegawai
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nip' => $request->nip,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tahun . "-" . $request->bulan . "-" . $request->tanggal,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->desa,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'id_sk' => $request->sk,
            'id_pangkat_pegawai' => $request->golongan,
            'id_jenis_pegawai' => $request->id_jenis_golongan,
            'tmt_capeg' => date('Y-m-d', strtotime($request->tmt_capeg)),
            'tmt_golongan' => date('Y-m-d', strtotime($request->tmt_golongan)),
            'id_status_pegawai' => $request->id_status_pegawai,
            'id_jabatan_pegawai' => $request->id_jabatan_pegawai,
            'digaji_menurut' => $request->digaji_menurut,
            'gaji_pokok' => str_replace('.', '', $request->gaji_pokok),
            'masa_kerja_golongan' => $request->masa_kerja_golongan,
            'masa_kerja_keseluruhan' => $request->masa_kerja_keseluruhan,
            'npwp' => $request->npwp,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->pegawaiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
        $result = $this->pegawaiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $pegawai = $this->pegawaiApi->get_by_id($request['id']);
        $pegawai = $pegawai['body']['data'];
        return response()->json($pegawai);
    }

    public function delete(Request $request)
    {
        $delete = $this->pegawaiApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function res_pass(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->pegawaiApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }
}
