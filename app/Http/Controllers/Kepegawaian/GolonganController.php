<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\GolonganApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GolonganController extends Controller
{
    private $golonganApi;

    public function __construct()
    {
        $this->golonganApi = new GolonganApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Golongan Ijazah');
        $golongan = $this->golonganApi->sekolah();
        // dd($golongan);
        $result = $golongan['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-kepegawaian' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.kepegawaian.v_golongan')->with(['template' => $template, 'golongan' => $golongan]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->golonganApi->create(json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
                'id_sekolah' => session('id_sekolah'),
            ];

        $pangkat = $this->golonganApi->update_info(json_encode($data));
        if ($pangkat['code'] == 200) {
            return response()->json([
                'message' => $pangkat['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $pangkat['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $pangkat = $this->golonganApi->get_by_id($request['id']);
        $pangkat = $pangkat['body']['data'];
        return response()->json($pangkat);
    }

    public function delete(Request $request)
    {
        $delete = $this->golonganApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
