<?php

namespace App\Http\Controllers\Kepegawaian;

use App\ApiService\Kepegawaian\TunjanganPotonganApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TunjanganPotonganController extends Controller
{
    private $tunpotApi;

    public function __construct()
    {
        $this->tunpotApi = new TunjanganPotonganApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Tunjangan & Potongan');
        $tunjangan = $this->tunpotApi->by_jenis('tunjangan');
        // dd($tunjangan);
        $tunjangan = $tunjangan['body']['data'];
        $potongan = $this->tunpotApi->by_jenis('potongan');
        $potongan = $potongan['body']['data'];
        $lembur = $this->tunpotApi->by_jenis('lembur');
        $lembur = $lembur['body']['data'];
        return view('content.kepegawaian.v_tunjangan_potongan')->with([
            'template' => 'default', 'tunjangan' => $tunjangan,
            'potongan' => $potongan, 'lembur' => $lembur
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->tunpotApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $tunjangan = $this->load_data($request->jenis);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'tunjangan' => $tunjangan
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            $data = array(
                "id" => $request['id_tunjangan_' . $i],
                "nama" => $request['nama_' . $i],
                "jenis" => $request['jenis'],
                "id_sekolah" => session('id_sekolah'),
            );
            $update = $this->tunpotApi->update_info(json_encode($data));
        }
        // dd($update);
        if ($update['code'] == 200) {
            $tunjangan = $this->load_data($request->jenis);
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'tunjangan' => $tunjangan
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $jabatan = $this->tunpotApi->get_by_id($request['id']);
        $jabatan = $jabatan['body']['data'];
        return response()->json($jabatan);
    }

    public function delete(Request $request)
    {
        $delete = $this->tunpotApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $tunjangan = $this->load_data($request->tunjangan);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'tunjangan' => $tunjangan
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function load_data($jenis)
    {
        $data = $this->tunpotApi->by_jenis($jenis);
        $data = $data['body']['data'];
        $html = '';
        if (!empty($data)) {
            $no = 1;
            foreach ($data as $dt) {
                $html .= '<tr>
                        <input type="hidden" name="id_tunjangan_' . $no . '"
                            value="' . $dt['id'] . '">
                        <td class="vertical-middle">' . $no . '</td>
                        <td>
                            <div class="input-group">
                                <input class="form-control" name="nama_' . $no . '"
                                    value="' . $dt['nama'] . '" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-danger delete"
                                        data-id="' . $dt['id'] . '" data-tunjangan="' . $dt['jenis'] . '" type="button"><i
                                            class="fas fa-trash"></i></button>
                                </span>
                            </div>
                        </td>
                    </tr>';
                $no++;
            }
            $html .= '<tr>
                        <input type="hidden" name="jumlah" value="' . count($data) . '">
                        <input type="hidden" name="jenis" value="' . $jenis . '">
                        <td colspan="2">
                            <button type="submit" class="btn btn-info pull-right" id="btnUpdate_' . $jenis . '"><i
                                    class="fas fa-sync-alt"></i> Update</button>
                        </td>
                    </tr>';
        } else {
            $html .= '<tr>
                    <td colspan="2" class="text-center">' . ucwords($jenis) . ' saat ini tidak
                        tersedia
                    </td>
                </tr>';
        }

        return $html;
    }
}
