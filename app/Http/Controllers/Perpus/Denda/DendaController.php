<?php

namespace App\Http\Controllers\Perpus\Denda;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Denda\DendaApi;

class DendaController extends Controller
{
    private $dendaApi;

    function __construct(){
    	$this->dendaApi = new DendaApi();
    }

    function get_all(Request $request){
    	$denda = $this->dendaApi->get_all();
    	$denda = $denda['body']['data'];
        //dd($denda);

        if($request->ajax()){
            $table = datatables()->of($denda);
            $table->addIndexColumn();
            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="modalDetailDenda('.$row['id'].')" id="btnInfo-'.$row['id'].'" class="btn btn-light btn-sm mt-0 mr-1">
                         <i class="fas fa-info-circle"></i>
                    </a>
                </div>';
            });
            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }

        return view('content.perpus.admin.denda.denda');
    }

    function get_one(Request $request){
    	$id = $request->id;
    	$denda = $this->dendaApi->get_detail($id);

    	if ($denda['code'] == 200) {
            return response()->json([
                'data' => $denda["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $denda["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function get_denda_user(Request $request){

        $datas = $this->dendaApi->get_by_user( session('id_peminjam') );
        $datas = $datas['body']['data'];

         if($request->ajax()){
            $table = datatables()->of($datas);
            $table->addIndexColumn();

            return $table->make(true);
        }
    }
}
