<?php

namespace App\Http\Controllers\Perpus\Pengumuman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Perpus\Pengumuman\PengumumanApi;

class PengumumanController extends Controller
{
    private $pengumuman;

    function __construct(){
        $this->pengumuman = new PengumumanApi();
    }

    public function index(Request $request)
    {
        $data = $this->pengumuman->getAll();
        $data = $data['body']['data'];
        
        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('nama',function ($row) {
                return '
                   <div>'.$row['nama'].'</div>
                ';
            });

            $table->editColumn('aksi',function ($row) {
                return '
                   <div class="btn-group btn-group-sm" style="float: none;">
                        <button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
                        </button>
                        <button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
                        </button>
                    </div>
                ';
            });
            $table->rawColumns(['aksi','nama']);
            return $table->make(true);
        }


        return view('content.perpus.admin.pengumuman.pengumuman');
    }

   
    public function create(Request $request)
    {
        $data = [
            'nama' => $request->nama,
            'tgl_akhir' => $request->tgl_akhir,
            'id_sekolah' => session('id_sekolah')
        ];

        if(!empty($request->id)){
            $data['id'] = $request->id;
        }

        $data = $this->pengumuman->store(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

   
    public function show(Request $request)
    {
        $id = $request->id;
        $data = $this->pengumuman->getOne($id);

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $data = $this->pengumuman->delete($id);

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  

    }
}
