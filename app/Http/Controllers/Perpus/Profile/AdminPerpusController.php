<?php

namespace App\Http\Controllers\Perpus\Profile;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Perpus\User\AdminPerpusApi;
use App\ApiService\User\ProfileApi;
use Image;

class AdminPerpusController extends Controller
{
    private $adminApi,$profileApi;

    function __construct(){
    	$this->adminApi = new AdminPerpusApi();
    	$this->profileApi = new ProfileApi();
    }

     function index(){
    	$admin = $this->adminApi->get_by_id(session('id'));
    	$admin = $admin['body']['data'];
    	return view('content.perpus.profile.profile_admin',compact('admin'));
    }

     function update_profile(Request $request){
    	$data = array(
    		'id' => session('id'),
    		'nama' => $request->nama,
            'username' => $request->username,
            'nip' => $request->nip,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'tempat_lahir' =>$request->tempat_lahir,
            'tgl_lahir' =>$request->tgl_lahir,
            'jenkel' => $request->jenkel,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah')
    	);

    	if ($files = $request->file('foto')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $basePath = "file/perpus/profile/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;

            $result = $this->profileApi->update_profile_image(json_encode($data));
            File::delete($path);
            //dd($result);
            $code = $result['code'];

        }else{
            $result = $this->profileApi->update_profile(json_encode($data));
        }

        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();
    }

    function update_password(Request $request){

    	$data = array(
    		'current_password' => $request->current_password,
    		'new_password' => $request->new_password,
            'confirm_password' => $request->confirm_password
    	);

    	$result = $this->profileApi->update_profile(json_encode($data));

    	$message = $result['body']['message'];

    	Session::flash('message', $message);

        return redirect()->back();
    }

    function getAll(Request $request){
        $admin = $this->adminApi->get_all();
        $admin = $admin['body']['data'];

        //dd($admin);

        if($request->ajax()){
            $table = datatables()->of($admin);
            $table->addIndexColumn();

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                           <div class="btn-group btn-group-sm" style="float: none;">
                                <button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
                                </button>
                                <button type="button" onclick="detail_modal('.$row['id'].')" class="tabledit-detail-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-info-circle"></i>
                                </button>
                                <button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                       </div>
                   </td>
                ';
            });

            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.perpus.admin.user.admin');

    }

    public function store(Request $request){

        if( $request->hasFile( 'image' ) ) {
            $image = $request->file( 'image' );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }

        $data = [
            'nama' => $request->nama,
            'nip' => $request->nip ,
            'username' => $request->username ,
            'email' => $request->email ,
            'jenkel' => $request->jenkel ,
            'telepon' => $request->telepon ,
            'tempat_lahir' => $request->tempat_lahir ,
            'tgl_lahir' => $request->tgl_lahir ,
            'alamat' => $request->alamat ,
            'foto' => $fullImage,
            'id_sekolah' => session('id_sekolah')
        ];

        if($request->id != null){
            $data['id'] = $request->id;
            $result = $this->adminApi->update(json_encode($data));
        }else{
            $data['first_password'] = $request->password ;
            $result = $this->adminApi->create(json_encode($data));
        }



        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();

    }

    public function getOne(Request $request){
        $id = $request->id;

        $data = $this->adminApi->get_by_id($id);

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

    public function delete(Request $request){
        $id = $request->id;

        $data = $this->adminApi->soft_delete($id);

        if ($data['code'] == 200) {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

}
