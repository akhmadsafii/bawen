<?php

namespace App\Http\Controllers\Perpus\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Perpus\Laporan\LaporanApi;
use App\ApiService\Perpus\Kartu\KartuApi;
use App\Helpers\Help;

class LaporanController extends Controller
{
    private $laporanApi,$kartuApi;
    private $kartu;

    function __construct(){
    	$this->laporanApi = new LaporanApi();
        $this->kartuApi = new KartuApi();
        $this->kartu = $this->kartuApi->get_detail(1);
        $this->kartu = $this->kartu['body']['data'];
    }

    public function index(Request $request){

        if(empty($request->bulan)){
            $id = date('n');
        }else{
            $id = $request->bulan;
        }

        $bulan = Help::getNumberMonthIndo($id);


    	$kartuApi = new KartuApi();
    	$kartu = $kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];

    	$user = $this->laporanApi->get_user($id);
        //dd($user);
    	//$userList = $user['body']['data'];
    	$userRole = $user['body']['role'];

    	$gmd = $this->laporanApi->gmd($id);
    	$gmd = $gmd['body']['data'];

    	$koleksi = $this->laporanApi->koleksi($id);
    	$koleksi = $koleksi['body']['data'];

    	$jenis = $this->laporanApi->jenis($id);
    	$jenis = $jenis['body']['data'];

    	$pengadaan = $this->laporanApi->pengadaan_by_mount($id);
    	$pengadaan = $pengadaan['body'];

    	$peminjaman = $this->laporanApi->peminjaman_by_mount($id);
    	$peminjaman = $peminjaman['body'];

    	$denda = $this->laporanApi->denda_by_mount($id);
    	$denda = $denda['body'];

    	return view('content.perpus.admin.laporan.laporan_bulanan',
    	[ 'userRole' => $userRole , 'gmds' => $gmd,
    	  'koleksis' => $koleksi , 'jeniss' => $jenis,
    	  'pengadaans' => $pengadaan, 'peminjamans' => $peminjaman,
    	  'dendas' => $denda, 'kartu' => $kartu , 'bulan' => $bulan



    	]);

    }

    public function peminjaman(Request $request){
        if(empty($request->bulan)){
            $id = date('n');
        }else{
            $id = $request->bulan;
        }

        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $bulan = Help::getNumberMonthIndo($id);

        $peminjaman = $this->laporanApi->peminjaman_by_mount($id);
        $peminjaman = $peminjaman['body']['data'];
        //dd($peminjaman);
        return view('content.perpus.admin.laporan.laporan_peminjaman',compact('bulan','kartu','peminjaman'));
    }

    public function peminjamanTerlambat(Request $request){
        $kartu = $this->kartu;

        $peminjaman = $this->laporanApi->peminjaman_terlambat();
        $peminjaman = $peminjaman['body']['data'];
        //dd($peminjaman);

        return view('content.perpus.admin.laporan.laporan_peminjaman_terlambat',compact('kartu','peminjaman'));
    }

    public function pengadaan(Request $request){
        if(empty($request->bulan)){
            $id = date('n');
        }else{
            $id = $request->bulan;
        }

        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $bulan = Help::getNumberMonthIndo($id);

        $pengadaan = $this->laporanApi->pengadaan_by_mount($id);
        $pengadaan = $pengadaan['body'];
        //dd($pengadaan);
        return view('content.perpus.admin.laporan.laporan_pengadaan',compact('bulan','kartu','pengadaan'));
    }

     public function denda(Request $request){
        if(empty($request->bulan)){
            $id = date('n');
        }else{
            $id = $request->bulan;
        }

        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $bulan = Help::getNumberMonthIndo($id);

        $denda = $this->laporanApi->denda_by_mount($id);
        $denda = $denda['body'];
        //dd($denda);
        return view('content.perpus.admin.laporan.laporan_denda',compact('bulan','kartu','denda'));
    }

    public function pustaka(){
    	$kartuApi = new KartuApi();
    	$kartu = $kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];

    	$bukus = $this->laporanApi->get_pustaka();
    	$bukus = $bukus['body']['data'];

    	return view('content.perpus.admin.laporan.laporan_pustaka',compact('bukus','kartu'));
    }

    public function pustakaByRak(){
    	$kartuApi = new KartuApi();
    	$kartu = $kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];

    	$bukus = $this->laporanApi->get_pustaka_by_rak();
    	$bukus = $bukus['body']['data'];

    	return view('content.perpus.admin.laporan.laporan_pustaka_by_rak',compact('bukus','kartu'));
    }

    public function pustakaByJenis(){
    	$kartuApi = new KartuApi();
    	$kartu = $kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];

    	$bukus = $this->laporanApi->get_pustaka_by_jenis();
    	$bukus = $bukus['body']['data'];

    	return view('content.perpus.admin.laporan.laporan_pustaka_by_jenis',compact('bukus','kartu'));
    }

    public function pusdig(){
        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $bukus = $this->laporanApi->get_pustaka_digital();
        $bukus = $bukus['body']['data'];

        return view('content.perpus.admin.laporan.laporan_pusdig',compact('bukus','kartu'));

    }

    public function pusdigByJenis(){
        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $bukus = $this->laporanApi->get_pustaka_digital_by_jenis();
        $bukus = $bukus['body']['data'];

        return view('content.perpus.admin.laporan.laporan_pusdig_by_jenis',compact('bukus','kartu'));

    }

    public function user(Request $request){

        if(empty($request->bulan)){
            $id = date('n');
        }else{
            $id = $request->bulan;
        }

        $bulan = Help::getNumberMonthIndo($id);


    	$kartuApi = new KartuApi();
    	$kartu = $kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];

    	$users = $this->laporanApi->get_user($id);
    	$users = $users['body'];

        $admins = $this->laporanApi->get_admin();
        $admins = $admins['body']['data'];
        //dd($users);

    	return view('content.perpus.admin.laporan.laporan_user',compact('users','admins','kartu','bulan'));

    }
}
