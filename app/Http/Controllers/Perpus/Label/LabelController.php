<?php

namespace App\Http\Controllers\Perpus\Label;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Perpus\Label\LabelApi;
use Image;


class LabelController extends Controller
{
    private $labelApi;

    function __construct(){
    	$this->labelApi = new labelApi();
    }

    function index(){
    	$label = $this->labelApi->get_detail(1);
    	$label = $label['body']['data'];
    	//dd($label);

    	return view('content.perpus.admin.label.label',compact('label'));
    }

    function store(Request $request){

    	if( $request->hasFile( 'file' ) ) {
            $image = $request->file( 'file' );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }



    	$data = [
    		'id' => $request->id,
    		'nama_label' => $request->nama_label,
    		'alamat' => $request->alamat,
    		'nama_instansi' => $request->nama_instansi,
    		'logo' => $fullImage,
    		'id_sekolah' => session('id_sekolah'),
    	];

    	//dd($data);

    	$result = $this->labelApi->update(json_encode($data));
    	//dd($result);

    	$message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();
    }

    function get_one(){
        $label = $this->labelApi->get_detail(1);
        //$label = $label['body']['data'];

        if ($label['code'] == 200) {
            return response()->json([
                'data' => $label["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $label["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }


}
