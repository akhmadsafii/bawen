<?php

namespace App\Http\Controllers\Perpus\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Perpus\Dashboard\DashboardApi;

class DashboardController extends Controller
{
    private $dashboardApi;

    function __construct(){
    	$this->dashboardApi = new DashboardApi();
    }

    function peminjaman(){
    	$data = $this->dashboardApi->get_peminjaman();
    	$data = $data['body']['data'];
    	dd($data);
    }
}
