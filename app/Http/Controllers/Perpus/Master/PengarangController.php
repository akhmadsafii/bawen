<?php

namespace App\Http\Controllers\Perpus\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Master\PengarangApi;


class PengarangController extends Controller
{
    private $pengarangApi;

    function __construct(){
        $this->middleware('check.api.token')->except('get_detail');
    	$this->pengarangApi = new PengarangApi();
    }

    //public
    function get_detail($id){
        
        $pengarang = $this->pengarangApi->get_detail($id);
        $pengarang = $pengarang["body"]["data"];
        
        $bukus = $pengarang['buku'];
        $pusdigs = $pengarang['pusdig'];

        return view('content.perpus.public.penulis',compact('pengarang','bukus','pusdigs'));
    }


    function get_all(Request $request){
    	$pengarang = $this->pengarangApi->get_all();
    	$pengarang = $pengarang['body']['data'];

    	//dd($pengarang);

    	if($request->ajax()){
    		$table = datatables()->of($pengarang);
            $table->addIndexColumn();

           
            $table->editColumn('nama',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-nama-'.$row['id'].'">'.$row['nama'].'</span>
                   	<input class="tabledit-input form-control input-sm input-nama-'.$row['id'].'" type="text" name="nama" value="'.$row['nama'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('biodata',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                    <span class="tabledit-span show-biodata-'.$row['id'].'">'.$row['biodata'].'</span>
                    
                    <textarea class="tabledit-input form-control input-sm input-biodata-'.$row['id'].'" type="text" name="biodata" value="'.$row['biodata'].'" style="display: none;" rows="3" placeholder="'.$row['biodata'].'">'.$row['biodata'].'</textarea>
                   </td>
                ';
            });

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
	                       		</button>
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                       	</div>
	                       <button type="button" onclick="submit_edit('.$row['id'].')" class="tabledit-save-button-'.$row['id'].' btn btn-sm btn-success" style="float: none; display: none;">Save</button>
	                       <button type="button" class="tabledit-confirm-button btn btn-sm btn-danger" style="display: none; float: none;">Confirm</button>
	                       <button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi','nama','biodata']);
            return $table->make(true);
    	}

    	return view('content.perpus.admin.pengarang.pengarang');
    }

    function get_one(Request $request){
    	$id = $request->id;
    	$pengarang = $this->pengarangApi->get_detail($id);

    	if ($pengarang['code'] == 200) {
            return response()->json([
                'data' => $pengarang["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $pengarang["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function store(Request $request){
        $data = [
            'biodata' => $request->biodata,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->pengarangApi->create(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function update(Request $request){
        $data = [
        	'id' => $request->id,
            'biodata' => $request->biodata,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->pengarangApi->update(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function delete(Request $request){
        $id = $request->id;

        $result = $this->pengarangApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


}
