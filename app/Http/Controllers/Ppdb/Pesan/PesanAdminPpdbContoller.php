<?php

namespace App\Http\Controllers\Ppdb\Pesan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use App\ApiService\Ppdb\Akun\PesertaPpdbApi;
use Pusher\Pusher;

class PesanAdminPpdbContoller extends Controller
{

    protected $AdminPpdbApi;
    protected $PesertaPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
        $this->PesertaPpdbApi = new PesertaPpdbApi();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Pesan');
        return view('ppdb.components.Pesan.v_index_admin');
    }

   /** setting pusher  */
   public function config_pusher()
   {

      $seetingApi = $this->AdminPpdbApi->intergration_api(session('id_sekolah'));
      $parseApi   = $seetingApi['body']['data'] ?? array();
      $PUSHER_APP_KEY      = '';
      $PUSHER_APP_CLUSTER  = '';
      $PUSHER_APP_ID       = '';
      $PUSHER_APP_SECRET   = '';

      if (!empty($parseApi)) {
          foreach ($parseApi as $key => $val) {
              if ($val['jenis'] == 'notification') {
                  if ($val['token'] != '-' && $val['status'] == '1') {
                      if ($val['kode'] == 'PUSHER_APP_KEY') {
                          $PUSHER_APP_KEY = $val['token'];
                      } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                          $PUSHER_APP_CLUSTER = $val['token'];
                      }
                      else if ($val['kode'] == 'PUSHER_APP_ID') {
                          $PUSHER_APP_ID = $val['token'];
                      }
                      else if ($val['kode'] == 'PUSHER_APP_SECRET') {
                          $PUSHER_APP_SECRET = $val['token'];
                      }
                  }
              }
          }
      }

       $app_id = $PUSHER_APP_ID ;
       $app_secret = $PUSHER_APP_SECRET;
       $app_key    = $PUSHER_APP_KEY;
       $app_cluster = $PUSHER_APP_CLUSTER;
       if(!empty($app_id) && !empty($app_secret) && !empty($app_key) && !empty( $app_cluster)){
        $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
        return $pusher;
     }else{
         return '';
     }
   }


    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request)
    {
        $detail_sample = $this->AdminPpdbApi->get_pesan_admin();
        //dd($detail_sample);
        $result = $detail_sample['body']['data'];
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show pesan admin" data-gambar="' . $data['file'] . '"   data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';

                    if($data['ditutup']=='Belum Ditutup'){
                        $button .= '<a  class="btn btn-warning btn-sm edit pesan admin" data-gambar="' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                        $button .= '<a  class="btn btn-success btn-sm balas pesan admin" data-gambar="' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-send"></i> </a>&nbsp;';
                        $button .= '<a  class="btn btn-primary btn-sm closed pesan admin" data-gambar="' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-close"></i> </a>&nbsp;';
                    }

                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove pesan admin" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('sesi', function ($row) {
                switch ($row['ditutup']) {
                    case 'Belum Ditutup':
                        return 'Belum Ditutup';
                        break;
                    case 'Ditutup':
                        return $row['ditutup'];
                        break;
                }
            });

            $table->rawColumns(['action','sesi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * ajax trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajaxtrash(Request $request)
    {
        $detail_pesan = $this->AdminPpdbApi->trash_pesan_sekolah();
        $result = $detail_pesan['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore pesan admin " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force pesan admin " data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::put('title', 'Buat Pesan');
        $detail_sample = $this->AdminPpdbApi->get_data_peserta();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Pesan.v_new_message_admin')->with(['peserta' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,png,jpg,svg|max:2048',
            'nama' => 'required',
            'id_peserta' => 'required',
            'pesan' => 'required',
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'pesan.required' => 'Form nama tidak boleh kosong!',
            'id_peserta.required' => 'Form Peserta tidak boleh kosong!',
            'image.mimes' => ' file hanya boleh diupload meliputi:jpeg,png,jpg,svg ',
        ]);

        $data = array();
        $data['nama'] = $request->nama;
        $data['isi']  = $request->pesan;
        $data['id_peserta'] = $request->id_peserta;
        $data['id_sekolah'] = session('id_sekolah');
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Pesan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_pesan_file(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->post_pesan(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return redirect()->route('pesan-admin')->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_pesan_admin_show($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function from_message_peserta($id)
    {
        $response_show_detail = $this->PesertaPpdbApi->get_pesan_user_peserta($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_pesan_admin_show($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /** get pesan from user  */
    public function pesanUser($id){
        $response_show_detail = $this->PesertaPpdbApi->get_pesan_user_peserta($id);
        $param = ['detail'=>$response_show_detail['body']['data'] ?? array(),'id'=>$id];
        return view('ppdb.components.Pesan.v_index_admin_user')->with($param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'mimes:jpeg,png,jpg,svg|max:2048',
            'nama' => 'required',
            'id_peserta' => 'required',
            'pesan' => 'required',
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'pesan.required' => 'Form nama tidak boleh kosong!',
            'id_peserta.required' => 'Form Peserta tidak boleh kosong!',
            'image.mimes' => ' file hanya boleh diupload meliputi:jpeg,png,jpg,svg ',
        ]);

        $data = array();
        $data['id'] = $id;
        $data['nama'] = $request->nama;
        $data['isi']  = $request->pesan;
        $data['id_peserta'] = $request->id_peserta;
        $data['id_sekolah'] = session('id_sekolah');
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Pesan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_pesan_file(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->update_pesan(json_encode($data));
        }
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function balas_pesan(Request $request, $id)
    {
        $request->validate([
            'image' => 'mimes:jpeg,png,jpg,svg|max:2048',
            'pesan' => 'required',
        ], [
            'pesan.required' => 'Form Pesan tidak boleh kosong!',
            'image.mimes' => ' file hanya boleh diupload meliputi:jpeg,png,jpg,svg ',
        ]);

        $data = array();
        $data['id_contact'] = $id;
        $data['isi']  = $request->pesan;
        $data['id_sekolah'] = session('id_sekolah');
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Pesan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_balas_pesan_file(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->post_balas_pesan(json_encode($data));
        }
        if ($response_update['code'] == '200') {

            $notif = $this->config_pusher();
            if(!empty($notif)){
                $message = [
                    'id_contact'=>$id,
                    'id_user'  => $request->id_user,
                    'pesan'=>$request->pesan
                ];
                $notif->trigger('pesan_user', 'pesan_user', $message);
            }

            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $response_show_detail = $this->AdminPpdbApi->delete_pesan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        //
        $response_show_detail = $this->AdminPpdbApi->restore_pesan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Delete permanent the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $response_show_detail = $this->AdminPpdbApi->delete_pesan_permanet($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tutup_pesan(Request $request, $id)
    {
        $data = array();
        $data['id']   = $id;
        $data['ditutup'] = '1';

        //dd($data);

        $response_update = $this->AdminPpdbApi->post_pesan_tutup(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data'=>$response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data'=>$response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }
}
