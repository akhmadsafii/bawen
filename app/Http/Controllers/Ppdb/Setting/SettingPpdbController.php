<?php

namespace App\Http\Controllers\Ppdb\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;


class SettingPpdbController extends Controller
{
    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Pengaturan PPDB');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'];
        //dd($setting_data);
        return view('ppdb.components.Setting.v_setting_ppdb')->with(['rows' => $setting_data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pembayaran()
    {
        Session::put('title', 'Pengaturan Pembayaran ');
        $detail_sample = $this->AdminPpdbApi->get_data_setting_pembayaran(session('id_sekolah'));
        $result = $detail_sample['body']['data'] ?? '';
        return view('ppdb.components.Setting.v_setting_pembayaran')->with(['rows' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        Session::put('title', 'Pengaturan Form ');
        $detail_sample = $this->AdminPpdbApi->get_data_setting_form();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_setting_form')->with(['rows' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customform()
    {
        Session::put('title', 'Pengaturan Form Custom');
        $detail_sample = $this->AdminPpdbApi->get_data_custom_type_form();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_setting_form_custom')->with(['typeform' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Create_customform()
    {
        Session::put('title', 'Buat Form Baru');
        $detail_sample = $this->AdminPpdbApi->get_data_custom_type_form();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_setting_form_new')->with(['typeform' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Create_customformtype()
    {
        Session::put('title', 'Buat tipe Form Baru');
        return view('ppdb.components.Setting.v_setting_form_new_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jenis_form()
    {
        Session::put('title', 'Pengaturan Form Custom');
        return view('ppdb.components.Setting.v_setting_form_custom_type');
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_custom_form(Request $request)
    {
        $detail_sample = $this->AdminPpdbApi->get_data_customform();

        $result = $detail_sample['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show form_custom" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit form_custom"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('status_form', function ($row) {
                switch ($row['status_form']) {
                    case 'Aktif':
                        return '<i class="fa fa-eye"></i>';
                        break;
                    case 'Tidak Aktif':
                        return '<i class="fa fa-eye-slash"></i>';
                        break;
                }
            });
            $table->rawColumns(['action', 'status_form']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_type_form(Request $request)
    {
        $detail_sample = $this->AdminPpdbApi->get_data_custom_type_form();

        $result = $detail_sample['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show form_custom type" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit form_custom type"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('status', function ($row) {
                if ($row['status'] == 'Aktif') {
                    return '<i class="fa fa-eye"></i>';
                } else if ($row['status_form'] == 'Tidak Aktif') {
                    return '<i class="fa fa-eye-slash"></i>';
                }
            });

            $table->rawColumns(['action', 'status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show_form_setting()
    {
        $detail_sample = $this->AdminPpdbApi->get_data_setting_form();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_setting_form_preview')->with(['kategori_form' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function form_card_setting()
    {
        $detail_sample = $this->AdminPpdbApi->get_data_setting_formcard();
        $result = $detail_sample['body']['data'] ?? '';
        return view('ppdb.components.Setting.v_setting_form_preview')->with(['kategori_form' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sambutan()
    {
        Session::put('title', 'Pengaturan Sambutan ');
        $detail_sample = $this->AdminPpdbApi->get_data_setting_sambutan(session('id_sekolah'));
        $result = $detail_sample['body']['data'] ?? '';
        return view('ppdb.components.Setting.v_setting_sambutan')->with(['rows' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_create_form(Request $request)
    {
        $request->validate([
            'jenis_form' => 'required',
            'nama' => 'required',
            'tipe' => 'required'
        ], [
            'jenis_form.required' => 'Form  jenis Form tidak boleh kosong!',
            'nama.required' => 'Form nama tidak boleh kosong!',
            'tipe.required' => 'Form nama tidak boleh kosong!'
        ]);
        $data = array();
        $data['id_jenis']          = $request->jenis_form;
        $data['nama']              = $request->nama;
        $data['tipe']              = $request->tipe;
        $data['id_sekolah']        = session('id_sekolah');
        $response_update = $this->AdminPpdbApi->post_createform_new(json_encode($data));
        if ($response_update['code'] == '200') {
            return redirect()->route('custom-form')->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_create_formtype(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
        ]);
        $data = array();
        $data['nama']              = $request->nama;
        $data['id_sekolah']        = session('id_sekolah');
        $response_update = $this->AdminPpdbApi->post_createform_newtype(json_encode($data));
        if ($response_update['code'] == '200') {
            return redirect()->route('type-form')->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showform($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_formid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showformtype($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_formtypeid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showformtypeEdit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_formtypeid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showformEdit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_formid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Store update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update_customform(Request $request, $id)
    {
        $request->validate([
            'jenis_form' => 'required',
            'nama' => 'required',
            'tipe' => 'required'
        ], [
            'jenis_form.required' => 'Form  jenis Form tidak boleh kosong!',
            'nama.required' => 'Form nama tidak boleh kosong!',
            'tipe.required' => 'Form nama tidak boleh kosong!'
        ]);
        $data = array();
        $data['id']                = $id;
        $data['id_jenis']          = $request->jenis_form;
        $data['nama']              = $request->nama;
        $data['tipe']              = $request->tipe;
        $data['id_sekolah']        = session('id_sekolah');
        $response_update = $this->AdminPpdbApi->update_createform_new(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update_create_formtype(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
        ]);
        $data = array();
        $data['id']                = $id;
        $data['nama']              = $request->nama;
        $data['id_sekolah']        = session('id_sekolah');
        $response_update = $this->AdminPpdbApi->update_createform_newtype(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_sambutan(Request $request)
    {
        //
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi' => 'required',
            'judul' => 'required'
        ], [
            'isi.required' => 'Form isi tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
            'judul.required' => 'Form judul tidak boleh kosong!'
        ]);

        $data = array();
        $data['judul']             = $request->judul;
        $data['isi']               = $request->isi;
        $data['id_sekolah']        = session('id_sekolah');

        //dd($data);

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_setting_sambutanfile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->post_data_setting_sambutan(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

     /** hapus foto  */
     public function remove_photo(Request $request)
     {
         $data = array();
         $data['judul']             = $request->judul;
         $data['isi']               = $request->isi;
         $data['id_sekolah']        = session('id_sekolah');
         $data['hapus']             = 'hapus';
         $response_update = $this->AdminPpdbApi->post_data_setting_sambutan(json_encode($data));
         if ($response_update['code'] == '200') {
             return response()->json(
                 [
                     'message' => $response_update['body']['message'],
                     'data' => $response_update['body']['data'],
                     'info' => 'success'
                 ]
             );
         } else {
             return response()->json(
                 [
                     'message' => $response_update['body']['message'],
                     'data' => $response_update['body']['data'],
                     'info' => 'error'
                 ]
             );
         }
     }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_form(Request $request)
    {
        //
        //validate form save or update

        $data = array();
        foreach ((array) $request->form as $r => $v) {
            $form = array();
            $form['id'] = $r;
            $form['status_form'] = $v;
            $data[] = $form;
        }

        $post_form = array('forms' => $data);

        $response_update = $this->AdminPpdbApi->post_data_setting_form_update(json_encode($post_form));

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store pembayaran a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_pembayaran(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'keterangan' => 'required',
            'biaya_pendaftaran' => 'required',
            'pembayaran' => 'required',
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'keterangan.required' => 'Form keterangan tidak boleh kosong!',
            'biaya_pendaftaran.required' => 'Form biaya pendaftaran tidak boleh kosong!',
            'pembayaran.required' => 'Form verifikasi pembayaran harus pilih salah satu Ya / Tidak!',
        ]);

        $nominal    = str_replace(",", "", $request->biaya_pendaftaran);

        $data = [
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'biaya_pendaftaran' => $nominal,
            'pembayaran' => $request->pembayaran,
            'id_sekolah' => session('id_sekolah')
        ];
        $response_update = $this->AdminPpdbApi->post_data_setting_pembayaran(json_encode($data));
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        $data['head1']         = $request->head1;
        $data['head2']         = $request->head2;
        $data['head3']         = $request->head3;
        $data['alamat']        = $request->alamat;
        $data['prolog']        = $request->prolog;
        $data['penutup']       = $request->penutup;
        $data['nama_kepsek']   = $request->nama_kepsek;
        $data['gelar']         = $request->gelar;
        $data['nip_kepsek']    = $request->nip_kepsek;
        $data['tempat_keputusan'] = $request->tempat_keputusan;
        $data['tgl_keputusan']    = $request->tanggal_keputusan;
        $data['tahun_ajaran']     = $request->tahun_ajaran;
        $data['tgl_tutup']        = $request->tanggal_tutup;
        $data['jam_tutup']        = $request->jam_tutup.':00';
        $data['kuota']            = $request->kuota;
        $data['jurusan']          = $request->jurusan;
        $data['jalur_ppdb']       = $request->jalur_ppdb;
        $data['copyright']        = $request->copyright;
        $data['penomoran_otomatis'] = $request->penomoran_otomatis;
        $data['id_sekolah']         = session('id_sekolah');
        $data['syarat_login']       = $request->syarat_login;
        $data['whatsapp']            = $request->whatsapp;
        $data['telepon']            = $request->telepon;
        $data['latitude']           = $request->lat;
        $data['longitude']          = $request->long;
        $data['jarak_maksimal']     = $request->jarak_maksimal;

        //dd($data);

        if ($files = $request->file('logo1')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['logo_pertama'] = $path;
            //$data['path'][0] = $path;
        }

        if ($files2 = $request->file('logo2')) {
            $namaFile = $files2->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files2->move($basePath, $imageName);
            $path2 = $basePath . $imageName;
            $data['logo_kedua'] = $path2;
            //$data['path'][1] = $path2;
        }

        if ($files3 = $request->file('stempel')) {
            $namaFile = $files3->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files3->move($basePath, $imageName);
            $path3 = $basePath . $imageName;
            $data['stempel'] = $path3;
            //$data['path'][2] = $path3;
        }
        if ($files4 = $request->file('ttd_kepsek')) {
            $namaFile = $files4->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files4->move($basePath, $imageName);
            $path4 = $basePath . $imageName;
            $data['ttd_kepsek'] = $path4;
            //$data['path'][3] = $path4;
        }

        if ($request->file('logo1') || $request->file('logo2') || $request->file('stempel') || $request->file('ttd_kepsek')) {
            $response_update = $this->AdminPpdbApi->update_data_setting_ppdbfile(json_encode($data));
        } else {
            $response_update = $this->AdminPpdbApi->update_data_setting_ppdb(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $responsed = $response_update['body']['data'];
            //create session
            Session::put('footer', $responsed['copyright']);
            Session::put('logo', $responsed['logo1']);

            Session::put('koordinat_latitude',$request->lat);
            Session::put('koordinat_longitude',$request->long);

            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }

            return redirect()->route('setting-ppdb')->with('success', $response_update['body']['message']);
        } else {
            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }
            return back()->with('error', $response_update['body']['message']);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function template_kartu()
    {
        Session::put('title', 'Pengaturan Template Kartu');
        $detail_sample = $this->AdminPpdbApi->get_data_template_kartu();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_setting_template_kartu')->with(['form_kartu' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function template_surat()
    {
        Session::put('title', 'Pengaturan Template Surat');
        $detail_sample = $this->AdminPpdbApi->get_data_template_surat();
        $result = $detail_sample['body']['data'] ?? array();
        // dd($result);
        return view('ppdb.components.Setting.v_setting_template_surat')->with(['form_surat' => $result]);
    }

    /**
     * Store  update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_template(Request $request)
    {
        //
        //validate form save or update

        $data = array();
        foreach ((array) $request->form as $r => $v) {
            if($r == '9' && $v == '0' || $r == '10' && $v == '0' || $r == '11' && $v == '0' || $r == '12' && $v == '0'){
                return back()->with('error', 'Header 1,2,3 Alamat Harus di checklist semua!');
            }
            $form = array();
            $form['id'] = $r;
            $form['aktif'] = $v;
            $data[] = $form;
        }

        //dd($data);

        $post_form = array('templates' => $data);

        $response_update = $this->AdminPpdbApi->update_template(json_encode($post_form));

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_aktif()
    {
        Session::put('title', 'Pengaturan Form Pendaftaran');
        $detail_sample = $this->AdminPpdbApi->get_form_aktif();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_urutan_form')->with(['rows' => $result]);
    }

    /**
     * Store  update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_urutanform(Request $request)
    {
        //
        //validate form save or update

        $data = array();
        foreach ((array) $request->form as $r => $v) {
            $form = array();
            $form['id'] = $r;
            $form['urutan_form'] = $v;
            $data[] = $form;
        }

        $post_form = array('forms' => $data);

        //dd($post_form);
        $response_update = $this->AdminPpdbApi->post_update_form_urutan(json_encode($post_form));

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview_cardx()
    {
        Session::put('title', 'Preview Kartu ');
        $detail_sample = $this->AdminPpdbApi->get_preview_form_template(session('id_sekolah'));
        $result = $detail_sample['body']['data'] ?? array();
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_kartu();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_preview_form_jadwal')->with(['form' => $result, 'jadwal' => $result_jadwal, 'form_setting' => $result_setting]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview_form()
    {
        Session::put('title', 'Preview Form');
        $detail_sample = $this->AdminPpdbApi->get_preview_print_form(session('id_sekolah'));
        $result = $detail_sample['body']['data'] ?? array();
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_surat();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_preview_form')->with(['form' => $result, 'jadwal' => $result_jadwal, 'form_setting' => $result_setting]);
    }

    /** preview form pengumuman */
    public function preview_pengumuman()
    {
        Session::put('title', 'Preview Template Pengumuman ');
        $detail_sample = $this->AdminPpdbApi->get_pengumuman_preview();
        $result = $detail_sample['body']['data'] ?? array();
        // dd($result);
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_surat();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_preview_form_surat')->with(['form' => $result, 'form_setting' => $result_setting]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_setting_kartu()
    {
        Session::put('title', 'Setting Kartu');
        $detail_sample = $this->AdminPpdbApi->get_setting_kartu();
        $result = $detail_sample['body']['data'] ?? array();
        return view('ppdb.components.Setting.v_card_active')->with(['rows' => $result]);
    }

    /**
     * Store  update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_statusform(Request $request)
    {
        //
        //validate form save or update

        $data = array();
        foreach ((array) $request->form as $r => $v) {
            $form = array();
            $form['id'] = $r;
            $form['status_kartu'] = $v;
            $data[] = $form;
        }

        $post_form = array('forms' => $data);

        //dd($post_form);
        $response_update = $this->AdminPpdbApi->post_update_form_status(json_encode($post_form));

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_setting_pusher()
    {
        Session::put('title', 'Setting Notifikasi by Pusher');
        $notifikasi_setting = $this->AdminPpdbApi->setting_pusher('notification',session('id_sekolah'));
        $parse_notifikasi   = $notifikasi_setting['body']['data'] ?? array();
        $param      = [
            'notifikasi'      => $parse_notifikasi
        ];
        return view('ppdb.components.Setting.v_pusher')->with($param);
    }

    /** strore environtment pusher
     * * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_pusher(Request $request)
    {
        $data4 = array();
        $data4['kode'] = 'PUSHER_APP_ID';
        $data4['jenis'] = 'notification';
        $data4['nama']  = 'App ID';
        $data4['token']  = $request->app_id_pusher;
        $data4['id_sekolah'] = session('id_sekolah');

        $response_show_detail4 = $this->AdminPpdbApi->setting_environment_post(json_encode($data4));

        $data5 = array();
        $data5['kode'] = 'PUSHER_APP_KEY';
        $data5['jenis'] = 'notification';
        $data5['nama']  = 'App Key';
        $data5['token']  = $request->app_key_pusher;
        $data5['id_sekolah'] = session('id_sekolah');

        $response_show_detail5 = $this->AdminPpdbApi->setting_environment_post(json_encode($data5));

        $data6 = array();
        $data6['kode'] = 'PUSHER_APP_SECRET';
        $data6['jenis'] = 'notification';
        $data6['nama']  = 'App Secret';
        $data6['token']  = $request->app_secret_pusher;
        $data6['id_sekolah'] = session('id_sekolah');

        $response_show_detail6 = $this->AdminPpdbApi->setting_environment_post(json_encode($data6));

        $data7 = array();
        $data7['kode'] = 'PUSHER_APP_CLUSTER';
        $data7['jenis'] = 'notification';
        $data7['nama']  = 'App Cluster';
        $data7['token']  = $request->app_cluster_pusher;
        $data7['id_sekolah'] = session('id_sekolah');

        $response_show_detail7 = $this->AdminPpdbApi->setting_environment_post(json_encode($data7));
        if (
             $response_show_detail4['code'] == '200'
             &&
             $response_show_detail5['code'] == '200'
             &&
             $response_show_detail6['code'] == '200'
             &&
             $response_show_detail7['code'] == '200'
           ) {
            return response()->json(
                [
                    'message' => 'Update Successfull',
                    'data' => array_merge($data4,$data5,$data6,$data7),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'Update Gagal',
                    'data' => '',
                    'info' => 'error',
                ]
            );
        }
    }


}
