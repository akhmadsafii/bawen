<?php

namespace App\Http\Controllers\Ppdb\Informasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;

class RulesController extends Controller
{
    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Informasi | Alur Pendaftaran ');
        $detail_alur = $this->AdminPpdbApi->get_detail_alur(session('id_sekolah'));
        $data = $detail_alur['body']['data'];
        //dd($detail_alur);
        return view('ppdb.components.Informasi.v_alur')->with(['rows'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi'=>'required',
            'judul' => 'required'
        ],[
            'isi.required'=>'Form isi tidak boleh kosong!',
            'judul.required'=>'Form Judul tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb'
        ]);

        $data = array();
        $data['judul'] = $request->judul;
        $data['isi'] = $request->isi;
        $data['id_sekolah'] = session('id_sekolah');

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/informasi/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->get_data_alurwithfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->get_data_alur(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return back()->with('success','Successfully Alur is saved!');
        }else{
            return back()->with('error',$response_update['body']['message']);
        }

    }

    /** hapus foto  */
    public function remove_photo(Request $request){
        $data = array();
        $data['judul']             = $request->judul;
        $data['isi']               = $request->isi;
        $data['id_sekolah']        = session('id_sekolah');
        $data['hapus']             = 'hapus';
        $response_update = $this->AdminPpdbApi->get_data_alur(json_encode($data));
        if($response_update['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
