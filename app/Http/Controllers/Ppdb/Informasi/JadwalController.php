<?php

namespace App\Http\Controllers\Ppdb\Informasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
class JadwalController extends Controller
{
    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Informasi | Jadwal ');
        return view('ppdb.components.Informasi.v_jadwal');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

     public function ajax_data(Request $request){
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result = $detail_jadwal['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm show jadwal " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button> &nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm edit jadwal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove jadwal" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_status(Request $request){
        $detail_jadwal = $this->AdminPpdbApi->get_data_profiljadwal_all_sekolah();
        $result = $detail_jadwal['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm show jadwal " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button> &nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm edit jadwal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove jadwal" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_status_hidden(Request $request){
        $detail_jadwal = $this->AdminPpdbApi->get_data_profiljadwal_all_sekolah();
        $result = $detail_jadwal['body']['data'];

        $filter_array = array();

        foreach($result as $value){
            if($value['status_kode'] == '0'){
                $filter_array[] = $value;
            }
        }

        if ($request->ajax()) {
            $table = datatables()->of($filter_array)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm show jadwal " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button> &nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm edit jadwal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove jadwal" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Session::put('title', 'Informasi | Tambah Jadwal ');
        return view('ppdb.components.Informasi.v_jadwal_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi'=>'required',
            'nama' => 'required',
            'tempat'=>'required',
            'tanggal_mulai'=>'required',
            'tanggal_akhir'=>'required',
            'urutan'=>'required'
        ],[
            'isi.required'=>'Form isi tidak boleh kosong!',
            'nama.required'=>'Form nama tidak boleh kosong!',
            'tempat.required'=>'Form tempat tidak boleh kosong!',
            'tanggal_mulai.required'=>'Form tanggal mulai tidak boleh kosong!',
            'tanggal_akhir.required'=>'Form tanggal akhir tidak boleh kosong!',
            'urutan.required'=>'Form urutan tidak boleh kosong!',
        ]);

        $data = array();
        $data['nama']        = $request->nama;
        $data['tempat']      = $request->tempat;
        $data['tgl_mulai']   = $request->tanggal_mulai;
        $data['tgl_akhir']   = $request->tanggal_akhir;
        $data['keterangan']  = $request->isi;
        $data['urutan']      = $request->urutan;
        $data['id_sekolah']  = session('id_sekolah');

        $response_update = $this->AdminPpdbApi->post_data_jadwal(json_encode($data));
        if($response_update['code'] == '200'){
            return redirect()->route('informasi-jadwal')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_jadwal = $this->AdminPpdbApi->get_data_jadwal($id);
        if($info_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$info_jadwal['body']['message'],
                    'data'=>$info_jadwal['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$info_jadwal['body']['message'],
                    'data'=>$info_jadwal['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_jadwal = $this->AdminPpdbApi->get_data_jadwal($id);
        if($info_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$info_jadwal['body']['message'],
                    'data'=>$info_jadwal['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$info_jadwal['body']['message'],
                    'data'=>$info_jadwal['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'isi'=>'required',
            'nama' => 'required',
            'tempat'=>'required',
            'tanggal_mulai'=>'required',
            'tanggal_akhir'=>'required',
            'urutan'=>'required'
        ],[
            'isi.required'=>'Form isi tidak boleh kosong!',
            'nama.required'=>'Form nama tidak boleh kosong!',
            'tempat.required'=>'Form tempat tidak boleh kosong!',
            'tanggal_mulai.required'=>'Form tanggal mulai tidak boleh kosong!',
            'tanggal_akhir.required'=>'Form tanggal akhir tidak boleh kosong!',
            'urutan.required'=>'Form urutan tidak boleh kosong!',
        ]);

        // request form
        $data = array();
        $data['nama']        = $request->nama;
        $data['tempat']      = $request->tempat;
        $data['tgl_mulai']   = $request->tanggal_mulai;
        $data['tgl_akhir']   = $request->tanggal_akhir;
        $data['keterangan']  = $request->isi;
        $data['urutan']      = $request->urutan;
        $data['status']      = $request->status;

        $update_jadwal = $this->AdminPpdbApi->update_data_jadwal_info($id,json_encode($data));

        if($update_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$update_jadwal['body']['message'],
                    'data'=>$update_jadwal['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$update_jadwal['body']['message'],
                    'data'=>$update_jadwal['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_jadwal = $this->AdminPpdbApi->delete_data_jadwal($id);
        if($delete_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$delete_jadwal['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$delete_jadwal['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * ajax trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

     public function ajaxtrash(Request $request){
        $detail_jadwal = $this->AdminPpdbApi->trash_data_jadwal();
        $result = $detail_jadwal['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore jadwal " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force jadwal" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }

      /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function restore($id)
    {
        $restore_jadwal = $this->AdminPpdbApi->restore_data_jadwal($id);
        if($restore_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$restore_jadwal['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$restore_jadwal['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Force Delete Permanent  the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteForce($id){
        $force_delete_jadwal = $this->AdminPpdbApi->delete_permanent_data_jadwal($id);
        if($force_delete_jadwal['code'] == '200'){
            return response()->json(
                [
                    'message'=>$force_delete_jadwal['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$force_delete_jadwal['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }
}
