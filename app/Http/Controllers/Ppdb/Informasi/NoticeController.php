<?php

namespace App\Http\Controllers\Ppdb\Informasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use Pusher\Pusher;

class NoticeController extends Controller
{
    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Informasi | Pengumuman');

        return view('ppdb.components.Informasi.v_pengumuman');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_data(Request $request){
        $detail_pengumuman = $this->AdminPpdbApi->get_data_pengumuman_all_sekolah();
        $result = $detail_pengumuman['body']['data'];
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm show feed " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button> &nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm edit feed"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove feed" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                    switch($row['file']){
                        case '':
                            return '-';
                        break;
                        default:
                        return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
                        break;
                    }
                });
            $table->editColumn('status', function ($row) {
             if($row['status'] == 'Aktif'){
                    return '<i class="fa fa-eye"></i>';
                }else if($row['status']=='Tidak Aktif'){
                    return '<i class="fa fa-eye-slash"></i>';
                }
            });
            $table->rawColumns(['action','gambar','status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        Session::put('title', 'Informasi | Tambah Pengumuman');
        return view('ppdb.components.Informasi.v_pengumuman_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi'=>'required',
            'judul' => 'required'
        ],[
            'isi.required'=>'Form isi tidak boleh kosong!',
            'judul.required'=>'Form Judul tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb'
        ]);

        $data = array();
        $data['judul'] = $request->judul;
        $data['isi'] = $request->isi;
        $data['id_sekolah'] = session('id_sekolah');

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/informasi/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_post = $this->AdminPpdbApi->post_data_feedfile(json_encode($data));
            file::delete($path);
        }else{
            $response_post = $this->AdminPpdbApi->post_data_feed(json_encode($data));
        }

        if($response_post['code'] == '200'){

            $notif = $this->config_pusher();
            if(!empty($notif)){
                $message = " Pemberitahuan / Informasi ".$request->judul;
                $notif->trigger('pengumuman', 'event_pengumuman', $message);
            }
            return redirect()->route('informasi-pengumuman')->with('success',$response_post['body']['message']);
        }else{
            return back()->with('error',$response_post['body']['message']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $response_feed_detail = $this->AdminPpdbApi->get_data_detail_feed($id);
        if($response_feed_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_feed_detail['body']['message'],
                    'data'=>$response_feed_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_feed_detail['body']['message'],
                    'data'=>$response_feed_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_feed_detail = $this->AdminPpdbApi->get_data_detail_feed($id);
        if($response_feed_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_feed_detail['body']['message'],
                    'data'=>$response_feed_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_feed_detail['body']['message'],
                    'data'=>$response_feed_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi'=>'required',
            'judul' => 'required'
        ],[
            'isi.required'=>'Form isi tidak boleh kosong!',
            'judul.required'=>'Form Judul tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb'
        ]);

        $data = array();
        $data['id']    = $request->id_pengumuman;
        $data['judul'] = $request->judul;
        $data['isi'] = $request->isi;
        $data['id_sekolah'] = session('id_sekolah');
        $data['status'] = $request->status;

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/informasi/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_post = $this->AdminPpdbApi->update_data_feedfile(json_encode($data));
            file::delete($path);
        }else{
            $response_post = $this->AdminPpdbApi->update_data_feed(json_encode($data));
        }

        if($response_post['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_post['body']['message'],
                    'data'=>$response_post['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_post['body']['message'],
                    'data'=>$response_post['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_feed = $this->AdminPpdbApi->delete_data_feed($id);
        if($delete_feed['code'] == '200'){
            return response()->json(
                [
                    'message'=>$delete_feed['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$delete_feed['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Ajax Trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajaxtrash(Request $request){
        $detail_feed = $this->AdminPpdbApi->trash_data_feed();
        $result = $detail_feed['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore feed " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force feed" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }

      /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function restore($id)
    {
        $restore_feed = $this->AdminPpdbApi->restore_data_feed($id);
        if($restore_feed['code'] == '200'){
            return response()->json(
                [
                    'message'=>$restore_feed['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$restore_feed['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Force Delete Permanent  the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteForce($id){
        $force_delete_feed = $this->AdminPpdbApi->delete_dataforce_feed($id);
        if($force_delete_feed['code'] == '200'){
            return response()->json(
                [
                    'message'=>$force_delete_feed['body']['message'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$force_delete_feed['body']['message'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

     /** setting pusher  */
     public function config_pusher()
     {

        $seetingApi = $this->AdminPpdbApi->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $PUSHER_APP_ID       = '';
        $PUSHER_APP_SECRET   = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                        else if ($val['kode'] == 'PUSHER_APP_ID') {
                            $PUSHER_APP_ID = $val['token'];
                        }
                        else if ($val['kode'] == 'PUSHER_APP_SECRET') {
                            $PUSHER_APP_SECRET = $val['token'];
                        }
                    }
                }
            }
        }

         $app_id = $PUSHER_APP_ID ;
         $app_secret = $PUSHER_APP_SECRET;
         $app_key    = $PUSHER_APP_KEY;
         $app_cluster = $PUSHER_APP_CLUSTER;
         if(!empty($app_id) && !empty($app_secret) && !empty($app_key) && !empty( $app_cluster)){
            $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
            return $pusher;
         }else{
             return '';
         }
     }
}
