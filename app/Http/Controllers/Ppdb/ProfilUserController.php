<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\Akun\PesertaPpdbApi;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class ProfilUserController extends Controller
{
    protected $PesertaPpdbApi;

     /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->PesertaPpdbApi = new PesertaPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Informasi | Ubah Profil User ');
        $profil_user = $this->PesertaPpdbApi->get_info_profil_user();
        $profil_data  = $profil_user['body']['data'];
        //dd($profil_data);
        session()->put('avatar',$profil_data['file']); //create session avatar
        return view('ppdb.components.Profil.v_profil_user')->with(['profil_data'=>$profil_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_password()
    {
        Session::put('title', 'Informasi | Ubah Password user ');
        return view('ppdb.components.Profil.v_password_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*$request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama'=>'required|max:255',
            'alamat' => 'required|max:255',
            'kota'=>'required',
            'telephone'=>'required|numeric',
            'email'=>'required|email',
            'nisn' =>'required'
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'alamat.required'=>'Form alamat tidak boleh kosong!',
            'kota.required'=>'Form kota tidak boleh kosong!',
            'telephone.required'=>'Form telephone tidak boleh kosong!',
            //'telephone.digits'=>'Nomor telephone maksimal 13 digit!',
            'telephone.numeric'=>'Nomor telephone hanya diperbolehkan berupa angka!',
            'email.required'=>'Form email tidak boleh kosong!',
            'email.email'=>'Format email tidak valid ! ',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
            'nisn.required'=>'Form nisn tidak boleh kosong!',
        ]);*/

        // from request input form
        $data_post = array();
        $data_post['username']     = session('username');
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;
        $data_post['nisn']         = $request->nisn;
        $data_post['longitude']    = $request->long;
        $data_post['latitude']     = $request->lat;

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->PesertaPpdbApi->update_profil_userwithfile($json_body);
            File::delete($path);
        }else{
            $json_body = json_encode($data_post);
              //update ke api backend
            $response_update = $this->PesertaPpdbApi->update_profil_user($json_body);
        }

        if($response_update['code'] == '200'){
            //create session update username than redirect back
            session()->put('username',$request->nama);
            return back()->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {

        $request->validate([
            //'username'=>'required|max:255',
            'password_lama' => 'required|max:255',
            'password_baru'=>'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru'=>'required',
        ],[
            //'username.required'=>'Form username tidak boleh kosong!',
            'password_lama.required'=>'Form password lama tidak boleh kosong!',
            'password_baru.required'=>'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required'=>'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' =>'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        //$data_post['username']         = $request->username;
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->PesertaPpdbApi->update_password_user($json_body);

        //dd($response_update);

        if($response_update['code'] == '200'){
            //create session update username than redirect back
            session()->put('username',$request->username);
            return back()->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
