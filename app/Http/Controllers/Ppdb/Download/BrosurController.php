<?php

namespace App\Http\Controllers\Ppdb\Download;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;

class BrosurController extends Controller
{

    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Brosur');
        return view('ppdb.components.Download.v_brosur');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::put('title', 'Download | Tambah Brosur');
        return view('ppdb.components.Download.v_add_brosur');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama'=>'required',
            'isi'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
            'isi.required'=>'Form isi tidak boleh kosong!',
        ]);

        $data = array();
        $data['nama']      = $request->nama;
        $data['keterangan'] = $request->isi;
        $data['id_sekolah'] = session('id_sekolah');

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/brosur/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_brosurfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->post_data_brosur(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return redirect()->route('brosur-ppdb')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request){
        $detail_brosur = $this->AdminPpdbApi->get_data_brosur();
        //dd($detail_brosur);
        $result = $detail_brosur['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('status', function ($row) {
               if($row['status_kode'] == '1'){
                   return '<i class="fa fa-eye"></i>';
               }else if($row['status_kode']=='0'){
                   return '<i class="fa fa-eye-slash"></i>';
               }
            });
            $table->rawColumns(['action', 'gambar','status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //detail brosur
        $response_brosur_detail = $this->AdminPpdbApi->get_detail_brosur($id);
        if($response_brosur_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $response_brosur_detail = $this->AdminPpdbApi->get_detail_brosur($id);
        if($response_brosur_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama'=>'required',
            'isi'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
            'isi.required'=>'Form isi tidak boleh kosong!',
        ]);

        $data = array();
        $data['id']        = $id;
        $data['nama']      = $request->nama;
        $data['keterangan'] = $request->isi;
        $data['status']     = $request->status;
        $data['id_sekolah'] = session('id_sekolah');

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/brosur/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_data_brosurfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->update_data_brosur(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response_brosur_detail = $this->AdminPpdbApi->delete_data_brosur($id);
        if($response_brosur_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    //'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    //'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }


        /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function restore($id)
    {
        $restore_brosur = $this->AdminPpdbApi->restore_data_brosur($id);
        if($restore_brosur['code'] == '200'){
            return response()->json(
                [
                    'message'=>$restore_brosur['body']['message'],
                    'info'=>'success',
                    'icon'=>'success',
                    'data'=>$restore_brosur['body']['data'],
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$restore_brosur['body']['message'],
                    'info'=>'error',
                    'icon'=>'error',
                    'data'=>$restore_brosur['body']['data'],
                ]);
        }
    }

    /**
     * Ajax Trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajaxtrash(Request $request){
        $detail_brosur = $this->AdminPpdbApi->trash_data_brosur();
        $result = $detail_brosur['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore download " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force download" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }


        /**
     * Remove force  the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteForce($id)
    {
        $response_brosur_detail = $this->AdminPpdbApi->deletepermanent_data_brosur($id);
        if($response_brosur_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                    //'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_brosur_detail['body']['message'],
                   // 'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

}
