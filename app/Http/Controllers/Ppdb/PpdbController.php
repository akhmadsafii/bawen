<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;


class PpdbController extends Controller
{

    private $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * auth form redirect route
     */

    public function access()
    {
        session()->put('config', 'ppdb');
        return redirect()->route('auth.login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Admin PPDB Online ');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_paid = $this->AdminPpdbApi->get_peserta_dashboard($pecah_data[0]);
            $peserta_diterima = $this->AdminPpdbApi->get_keputusan_peserta_diterima_dashboard($pecah_data[0]);
            $total_register          = $payment_paid['body']['data']['total'];
            $total_peserta_diterima  = $peserta_diterima['body']['data']['diterima'];
            $total_peserta_ditolak  = $peserta_diterima['body']['data']['tidak_diterima'];
            $total_belum_diputuskan = $peserta_diterima['body']['data']['belum_diputuskan'];
            $total_pembayaran_all    = $payment_paid['body']['data']['pembayaran_semua'];
            $total_pembayaran_konfirm = $payment_paid['body']['data']['pembayaran_konfirmasi'];
        } else {
            $total_register          = 0;
            $total_peserta_diterima  = 0;
            $total_peserta_ditolak   = 0;
            $total_belum_diputuskan  = 0;
            $total_pembayaran_all    = 0;
            $total_pembayaran_konfirm = 0;
        }

        $param = [
            'total_register' => $total_register,
            'total_peserta_diterima' => $total_peserta_diterima,
            'total_peserta_ditolak' => $total_peserta_ditolak,
            'total_belum_diputuskan' => $total_belum_diputuskan,
            'total_pembayaran_all'  => $total_pembayaran_all,
            'total_pembayaran_confirm' => $total_pembayaran_konfirm
        ];

        return view('ppdb.components.v_page_dasbor')->with($param);
    }

    /**
     * Ajax Data payment pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_payment_pending(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_pending = $this->AdminPpdbApi->get_payment_with_confirm($pecah_data[0]);
            $result          = $payment_pending['body']['data'] ?? array();
        } else {
            $result          = array();
        }
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks


        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <a href="' . route('detail_pembayaran', ['id' => $data['id']]) . '" class="btn btn-info btn-sm show payment" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail_pembayaran($id)
    {
        Session::put('title', 'Detail Pembayaran');
        $response_show_detail = $this->AdminPpdbApi->get_payment_by_id($id);
        $result = $response_show_detail['body']['data'] ?? array();
        return view('ppdb.components.Payment.v_payment_detail')->with(['id_pembayaran' => $id,'result'=>$result]);
    }



    /**
     * Display detail pembayaran the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_pembayaran($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     *
     * show the form for editing the specified resource
     *
     * @return \Illuminate\Http\Response
     */

    public function get_profil()
    {
        Session::put('title', 'My Profile');
        return view('ppdb.components.v_profils');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Ajax Data Pesan
     *
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_pesan()
    {
        $detail_sample = $this->AdminPpdbApi->get_pesan_admin();
        $result = $detail_sample['body']['data'] ?? array();
        $pesan = array();
        foreach ($result as $key => $val) {
            $list = array();
            $list['id']      = $val['id_peserta'];
            $list['peserta'] = $val['peserta'];
            $list['judul']   = $val['nama'];
            $list['pesan']   = $val['isi'];
            $list['file']    = $val['file'];
            $pesan[] = $list;
        }
        return response()->json(['data' => $pesan, 'info' => 'success']);
    }

    /**
     * Ajax Data analytic
     *
     * @return \Illuminate\Http\Response
     */

    public function ajax_analytic()
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_paid = $this->AdminPpdbApi->get_peserta_dashboard($pecah_data[0]);
            $peserta_diterima = $this->AdminPpdbApi->get_keputusan_peserta_diterima_dashboard($pecah_data[0]);
            $total_register          = $payment_paid['body']['data']['total'];
            $total_peserta_diterima  = $peserta_diterima['body']['data']['diterima'];
            $total_peserta_ditolak   = $peserta_diterima['body']['data']['tidak_diterima'];
            $total_belum_diputuskan  = $peserta_diterima['body']['data']['belum_diputuskan'];
            $total_pembayaran_all    = $payment_paid['body']['data']['pembayaran_semua'];
            $total_pembayaran_konfirm = $payment_paid['body']['data']['pembayaran_konfirmasi'];
        } else {
            $total_register          = 0;
            $total_peserta_diterima  = 0;
            $total_peserta_ditolak   = 0;
            $total_belum_diputuskan  = 0;
            $total_pembayaran_all    = 0;
            $total_pembayaran_konfirm = 0;
        }

        $param = [
            'total_register' => $total_register,
            'total_peserta_diterima' => $total_peserta_diterima,
            'total_peserta_ditolak' => $total_peserta_ditolak,
            'total_belum_diputuskan' => $total_belum_diputuskan,
            'total_pembayaran_all'  => number_format($total_pembayaran_all, 0),
            'total_pembayaran_confirm' => number_format($total_pembayaran_konfirm, 0)
        ];

        return response()->json(['data' => $param, 'info' => 'success']);
    }
}
