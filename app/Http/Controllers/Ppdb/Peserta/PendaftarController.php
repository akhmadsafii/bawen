<?php

namespace App\Http\Controllers\Ppdb\Peserta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use App\ApiService\Ppdb\Akun\PesertaPpdbApi;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Pusher\Pusher;

class PendaftarController extends Controller
{
    protected $AdminPpdbApi;
    protected $PesertaPpdbApi;
    public $urlApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
        $this->urlApi = env("API_URL");
        $this->PesertaPpdbApi = new PesertaPpdbApi();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Peserta | Pendaftar');
        return view('ppdb.components.Peserta.v_pendaftar');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register_ulang_peserta()
    {
        Session::put('title', 'Peserta | Register Ulang Pendaftar Peserta');
        return view('ppdb.components.Peserta.v_register_ulang');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function peserta_diterima()
    {
        Session::put('title', 'Peserta | Pendaftar Diterima');
        return view('ppdb.components.Peserta.v_pendaftar_diterima');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function peserta_ditolak()
    {
        Session::put('title', 'Peserta | Pendaftar Ditolak');
        return view('ppdb.components.Peserta.v_pendaftar_ditolak');
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request)
    {
        $detail_sample = $this->AdminPpdbApi->get_data_peserta();

        $result = $detail_sample['body']['data'];

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download pendaftar" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download pendaftar" data-gambar="' . $this->urlApi . 'public/' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download pendaftar" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file']  . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
            $table->editColumn('email', function ($row) {
                switch ($row['email']) {
                    case '':
                        return '-';
                    default:
                        return $row['email'];
                }
            });
            $table->editColumn('nama_peserta', function ($row) {
                return $row['nama'];
            });
            $table->editColumn('status', function ($row) {
                if ($row['status'] == '1') {
                    return '<i class="fa fa-eye"></i>';
                } else if ($row['status'] == '0') {
                    return '<i class="fa fa-eye-slash"></i>';
                }
            });
            $table->editColumn('telepon', function ($row) {
                switch ($row['telepon']) {
                    case '':
                        return '-';
                    default:
                        return $row['telepon'];
                }
            });
            $table->editColumn('jenkel', function ($row) {
                switch ($row['jenkel']) {
                    case 'l':
                        return 'Laki-laki';
                    case 'p':
                        return 'Perempuan';
                }
            });
            $table->rawColumns(['action', 'status', 'nama_peserta', 'photo', 'jenkel']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_register_ul(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $detail_sample = $this->AdminPpdbApi->get_peserta_registerul($pecah_data[0]);
        } else {
            $pecah_data   = array();
            $detail_sample = $this->AdminPpdbApi->get_peserta_registerul(session('tahun'));
        }

        $result = $detail_sample['body']['data'];

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-info btn-sm show download pendaftar"  href="' . route('detail-pendaftar-regiter', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-eye"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm show file download pendaftar"  href="' . route('getfile-peserta', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-file"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download pendaftar" data-gambar="' . $this->urlApi . "/public/" . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm print file download pendaftar"  href="' . route('print_register_peserta', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff" target="_blank"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('check', function ($row) {
                return '<input type="checkbox" name="listbox[]" class="checklist" value="' . $row['id'] . '">';
            });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $this->urlApi . "/public/" . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
            $table->editColumn('email', function ($row) {
                switch ($row['email']) {
                    case '':
                        return '-';
                    default:
                        return $row['email'];
                }
            });
            $table->editColumn('keputusan', function ($row) {
                switch ($row['keputusan']) {
                    case '1':
                        return 'diterima';
                    case '2':
                        return 'ditolak';
                }
            });
            $table->editColumn('jarak_zonasi', function ($row) {
                $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
                $setting_data = $setting['body']['data'] ?? array();
                if (!empty($setting_data)) {
                    $jarak = $this->getDistanceBetween($row['latitude'], $row['longitude'], $setting_data['latitude'], $setting_data['longitude']);
                } else {
                    $jarak = 0;
                }

                $let_jarak = '';

                if ($jarak >= $setting_data['jarak_maksimal']) {
                    $let_jarak = '<span class="text-red">' . $jarak . '</span>';
                } else {
                    $let_jarak = '<span class="text-success">' . $jarak . '</span>';
                }

                return $let_jarak;
            });
            $table->editColumn('nama_peserta', function ($row) {
                return $row['nama'];
            });
            $table->editColumn('status', function ($row) {
                if ($row['status'] == '1') {
                    return '<i class="fa fa-eye"></i>';
                } else if ($row['status'] == '0') {
                    return '<i class="fa fa-eye-slash"></i>';
                }
            });
            $table->editColumn('telepon', function ($row) {
                switch ($row['telepon']) {
                    case '':
                        return '-';
                    default:
                        return $row['telepon'];
                }
            });
            $table->editColumn('jenkel', function ($row) {
                switch ($row['jenkel']) {
                    case 'l':
                        return 'Laki-laki';
                    case 'p':
                        return 'Perempuan';
                }
            });
            $table->rawColumns(['action', 'status', 'nama_peserta', 'photo', 'jenkel', 'jarak_zonasi', 'check', 'keputusan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data peserta diterima
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_peserta_diterima(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $detail_sample = $this->AdminPpdbApi->get_data_peserta_diterima($pecah_data[0]);
        } else {
            $pecah_data   = array();
            $detail_sample = $this->AdminPpdbApi->get_data_peserta_diterima(session('tahun'));
        }

        $result = $detail_sample['body']['data'] ?? array();

        //dd($detail_sample);

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download pendaftar" title="Lihat data " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm print file download pendaftar"  href="' . route('print_register_peserta', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff" target="_blank"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download pendaftar" title="Edit data" data-gambar="' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm show file download pendaftar"  href="' . route('getfile-peserta', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-file"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-primary btn-sm cancel download pendaftar" title="Batal" data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-remove"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download pendaftar" title="Hapus data" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
            $table->editColumn('email', function ($row) {
                switch ($row['email']) {
                    case '':
                        return '-';
                    default:
                        return $row['email'];
                }
            });
            $table->editColumn('nama_peserta', function ($row) {
                return $row['nama'];
            });

            $table->editColumn('keputusan', function ($row) {
                return $row['keputusan'];
            });
            $table->editColumn('telepon', function ($row) {
                switch ($row['telepon']) {
                    case '':
                        return '-';
                    default:
                        return $row['telepon'];
                }
            });
            $table->editColumn('jenkel', function ($row) {
                switch ($row['jenkel']) {
                    case 'l':
                        return 'Laki-laki';
                    case 'p':
                        return 'Perempuan';
                }
            });
            $table->rawColumns(['action', 'nama_peserta', 'photo', 'keputusan', 'jenkel']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** get jarak zonasi */
    public function getDistanceBetween($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km')
    {
        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch ($unit) {
            case 'Mi':
                break;
            case 'Km':
                $distance = $distance * 1.609344;
        }
        return (round($distance, 2)) . ' ' . $unit;
    }

    /**
     * Ajax Data peserta ditolak
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_peserta_ditolak(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $detail_sample = $this->AdminPpdbApi->get_data_peserta_ditolak($pecah_data[0]);
        } else {
            $pecah_data   = array();
            $detail_sample = $this->AdminPpdbApi->get_data_peserta_ditolak(session('tahun'));
        }

        $result = $detail_sample['body']['data'] ?? array();
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download pendaftar" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download pendaftar" data-gambar="' . $data['file'] . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm show file download pendaftar"  href="' . route('getfile-peserta', ['id' => $data['id']]) . '"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-file"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download pendaftar" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
            $table->editColumn('email', function ($row) {
                switch ($row['email']) {
                    case '':
                        return '-';
                    default:
                        return $row['email'];
                }
            });
            $table->editColumn('nama_peserta', function ($row) {
                return $row['nama'];
            });

            $table->editColumn('keputusan', function ($row) {
                return $row['keputusan'];
            });
            $table->editColumn('telepon', function ($row) {
                switch ($row['telepon']) {
                    case '':
                        return '-';
                    default:
                        return $row['telepon'];
                }
            });
            $table->editColumn('jenkel', function ($row) {
                switch ($row['jenkel']) {
                    case 'l':
                        return 'Laki-laki';
                    case 'p':
                        return 'Perempuan';
                }
            });
            $table->rawColumns(['action', 'nama_peserta', 'photo', 'keputusan', 'jenkel']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Session::put('title', 'Peserta | Form Pendaftar');
        return view('ppdb.components.Peserta.v_pendaftar_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required',
            'nisn' => 'required',
            'email' => 'required|email',
            'jenkel' => 'required',
            'password' => 'required'
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'nisn.required' => 'Form nisn tidak boleh kosong!',
            'email.required' => 'Form email tidak boleh kosong!',
            'jenkel.required' => 'Form jenis kelamin harus pilih salah satu Laki-laki / Perempuan!',
            'password.required' => 'Form password tidak boleh kosong!',
            'image.required' => 'Form Gambar tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
        ]);

        $data = array();
        $data['nama']           = $request->nama;
        $data['jenkel']         = $request->jenkel;
        $data['nisn']           = $request->nisn;
        $data['email']          = $request->email;
        $data['first_password'] = $request->password;
        $data['longitude']         = $request->long;
        $data['latitude']          = $request->lat;
        $data['id_sekolah']     = session('id_sekolah');
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profile/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_pesertafile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->post_data_peserta(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return redirect()->route('akun-pendaftar')->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //detail brosur
        $response_show_detail = $this->AdminPpdbApi->get_detail_peserta_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showdetail($id)
    {
        //detail brosur
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }

        $response_show_detail = $this->AdminPpdbApi->get_data_peserta_detailbyid($id, $pecah_data[0]);

        $param = [
            'detail_info' => $response_show_detail['body']['data'],
            'tahun_ajaran' => $tahun_ajaran,
            'id_peserta' => $id
        ];

        return view('ppdb.components.Peserta.v_detail_peserta_form')->with($param);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print_register_peserta($id)
    {
        //detail brosur
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }

        $response_show_detail = $this->AdminPpdbApi->get_preview_print_formbyuser($id, $pecah_data[0]);
        $param = [
            'form' => $response_show_detail['body']['data'],
            'tahun_ajaran' => $tahun_ajaran,
        ];

        return view('ppdb.components.Peserta.v_detail_peserta_form_print')->with($param);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_edit_detail = $this->AdminPpdbApi->get_detail_peserta_byid($id);
        if ($response_edit_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_edit_detail['body']['message'],
                    'data' => $response_edit_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_edit_detail['body']['message'],
                    'data' => $response_edit_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate form save or update
        $request->validate([
            'nama' => 'required',
            'nisn' => 'required',
            'jenkel' => 'required',
            'password' => 'required'
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'nisn.required' => 'Form nisn tidak boleh kosong!',
            'jenkel.required' => 'Form jenis kelamin harus pilih salah satu Laki-laki / Perempuan!',
            'password.required' => 'Form password tidak boleh kosong!',
        ]);

        $data = array();
        $data['id']             = $id;
        $data['nama']           = $request->nama;
        $data['jenkel']         = $request->jenkel;
        $data['nisn']           = $request->nisn;
        $data['email']          = $request->email;
        $data['first_password'] = $request->password;
        $data['status']         = $request->status;
        $data['longitude']         = $request->long;
        $data['latitude']          = $request->lat;
        $data['alamat']            = $request->alamat;
        $data['keputusan']       = '0';
        $data['id_sekolah']     = session('id_sekolah');
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profile/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_data_pesertafile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->AdminPpdbApi->update_data_peserta(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                    'icon' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error',
                    'icon' => 'error'
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id)
    {

        $request->validate([
            'keputusan' => 'required',
        ], [
            'keputusan.required' => 'Pilih Salah Satu Keputusan Terima atau Tidak !',
        ]);

        $data = array();
        $data['id']                           = $id;
        $data['keputusan']                    = $request->keputusan;
        $data['keterangan_keputusan']         = $request->catatan;
        $data['id_sekolah']                   = session('id_sekolah');

        $status = 'Dipertimbangkan';
        switch ($data['keputusan']) {
            case '1':
                $status = 'Diterima';
                break;
            case '0':
                $status = 'Ditolak';
                break;
        }

        $response_update = $this->AdminPpdbApi->update_status_peserta_diterima(json_encode($data));

        if ($response_update['code'] == '200') {
            $notif = $this->config_pusher();
            if (!empty($notif)) {
                $userdata = $response_update['body']['data'];
                $message = "Keputusan Penerima PPDB " . $userdata['nama'] . " telah " . $status;
                $notif->trigger('keputusan', 'event_keputusan', $message);
            }

            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                    'keputusan' => $request->keputusan
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error',
                    'keputusan' => $request->keputusan
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status_tolak($id)
    {

        $data = array();
        $data['id']                   = $id;
        $data['keputusan']            = '0';
        $data['keterangan_keputusan'] = 'Dibatalkan';
        $data['id_sekolah']           = session('id_sekolah');

        $response_update = $this->AdminPpdbApi->update_status_peserta_diterima(json_encode($data));

        if ($response_update['code'] == '200') {

            $notif = $this->config_pusher();
            if (!empty($notif)) {
                $userdata = $response_update['body']['data'];
                $message = "Keputusan Penerima PPDB " . $userdata['nama'] . " telah " . $data['keterangan_keputusan'];
                $notif->trigger('keputusan', 'event_keputusan', $message);
            }

            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                    'icon' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error',
                    'icon' => 'error'
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $response_show_detail = $this->AdminPpdbApi->delete_data_peserta($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        //
        $response_show_detail = $this->AdminPpdbApi->restore_data_peserta($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteForce($id)
    {
        //
        $response_show_detail = $this->AdminPpdbApi->deleteforce_data_peserta($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * ajax trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajaxtrash(Request $request)
    {
        $detail_jadwal = $this->AdminPpdbApi->get_trash_peserta();
        //dd($detail_jadwal);
        $result = $detail_jadwal['body']['data'] ?? array();
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore pendaftar " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force pendaftar" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Get file peserta  the specified resource from storage.
     *
     * @param  int  $id
     * @param Request
     * @return \Illuminate\Http\Response
     */
    public function getfile_peserta($id, Request $request)
    {
        $detail_sample = $this->AdminPpdbApi->get_data_file_support_peserta($id);

        $result = $detail_sample['body']['data'];

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $namaFile = explode(".", basename($data['file']));
                    $button .= '<a class="btn btn-info" href="' . route('force_download_peserta', ['userid' => $data['id_peserta'], 'namafile' => $namaFile[0]]) . '"><i class="fa fa-download"></i></a>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                $infoPath  = pathinfo($row['file']);
                $extension = $infoPath['extension'];
                if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'svg') {
                    return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
                } else {
                    return '-';
                }
            });
            $table->editColumn('extension', function ($row) {
                $infoPath = pathinfo($row['file']);
                return $infoPath['extension'];
            });
            $table->rawColumns(['action', 'extension', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** force download file
     ** @param $id,$userid,$name
     */
    public function getdownload($id, $userid, $name)
    {
        $download = $this->force_download_curl($id, $userid, $name);
        if ($download['code'] == '200') {
            $file = public_path($download['filename']);
            if (!empty($file)) {
                $headers = array(
                    'Content-Type:' . mime_content_type($file),
                );
                return response()->download($file, $download['filename'], $headers)->deleteFileAfterSend(true);
            } else {
                return back()->with('error', 'download Gagal');
            }
        } else {
            return back()->with('error', 'download Gagal');
        }
    }


    /** force download file
     ** @param $userid
     */
    public function fgetdownload($userid, $namaFile)
    {
        $post = $this->AdminPpdbApi->get_data_file_support_peserta($userid);
        $result = $post['body']['data'] ?? array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $filename = explode(".", basename($val['file']));
                if ($filename[0] == $namaFile) {
                    $headers = ['Content-Type:' . $filename[1]];
                    $pathToFile = storage_path('app/' . basename($val['file']));
                    $getContent = $this->curl_get_file_contents($val['file']);
                    file_put_contents($pathToFile, $getContent);
                    return response()->download($pathToFile, basename($val['file']), $headers)->deleteFileAfterSend(true);
                }
            }
        } else {
            return back()->with('error', 'download Gagal');
        }
    }


    /** get curl force download */

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    /**
     * curl file download
     * @param $url
     */

    private function force_download_curl($url, $userid, $name)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        if (!empty($token)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token,
            ));
        }
        $saveTo = $userid . '-' . $name . '-' . basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_register()
    {
        Session::put('title', 'Formulir Pendaftaran');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $form_daftar = $this->PesertaPpdbApi->get_form_peserta_pendaftaran(session('id'), $pecah_data[0]);
            $result          = $form_daftar['body']['data'] ?? array();
        } else {
            $result          = array();
        }

        $param = [
            'form' => $result['form'] ?? '',
        ];

        return view('ppdb.components.Peserta.v_form_register_admin')->with($param);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_register(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        /*if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }*/
        $data = array();
        $form_ = array();
        $data['id_sekolah']    = session('id_sekolah');
        $data['tahun_ajaran']  = $tahun_ajaran;
        $data['id_peserta']    = $request->id_peserta;
        foreach ((array) $request->form as $r => $v) {
            $form = array();
            $form['pendaftaran_form'] = $r . "#" . $v;
            $form_[] = $form;
        }
        $data['forms'] = $form_;
        $response_update = $this->AdminPpdbApi->post_add_form_pendaftaran(json_encode($data));
        if ($response_update['code'] == '200') {
            return redirect()->route('register-ulang')->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display a listing of the resource.
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function filepeserta($id)
    {
        Session::put('title', 'Peserta | File Peserta');
        return view('ppdb.components.v_file_peserta')->with(['id_peserta' => $id]);
    }

    /** export file excel */
    public function export_file()
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_update = $this->AdminPpdbApi->export_data_peserta($pecah_data[0]);
        $data_rows = $response_update['body']['data'];

        $form_Column = array();
        $form_Cell   = array();
        foreach ($data_rows as $key => $val) {
            $form = array();
            $form2 = array();
            foreach ($val['form']  as $k => $v) {
                $form[] = $v['nama'];
                $form2[] = $v['value'];
            }
            $form_Column[] = $form;
            $form_Cell[]   = $form2;
        }


        $array_column = array();
        $spreadSheet = new Spreadsheet();

        foreach ($form_Column[0] as $rx => $vc) {
            $array_column[] = $vc;
        }

        $spreadSheet->getActiveSheet()->fromArray($array_column, null, 'A1');
        $spreadSheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
        $spreadSheet->getActiveSheet()->fromArray($form_Cell, null, 'A2');
        $style_col = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
                'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
                'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
                'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
                'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ],
            'fill' => array(
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => array('argb' => 'FF4F81BD')
            ),
            'font'  => array(
                'color' => array('rgb' => 'FFFFFF'),
            )
        ];
        $spreadSheet->getActiveSheet()->getStyle('A1:' . $spreadSheet->getActiveSheet()->getHighestColumn() . '1')->getFont()->setBold(true);
        $spreadSheet->getActiveSheet()->getStyle('A1:' . $spreadSheet->getActiveSheet()->getHighestColumn() . '1')->applyFromArray($style_col);

        $Excel_writer = new Xls($spreadSheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="data_siswa_ppdb_' . $pecah_data[0] . '.xls"');
        header('Cache-Control: max-age=0');
        //ob_end_clean();
        $Excel_writer->save('php://output');
        return back()->with('success', 'File Berhasil di Export');
    }

    /** setting pusher  */
    public function config_pusher()
    {

        $seetingApi = $this->AdminPpdbApi->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $PUSHER_APP_ID       = '';
        $PUSHER_APP_SECRET   = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_ID') {
                            $PUSHER_APP_ID = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_SECRET') {
                            $PUSHER_APP_SECRET = $val['token'];
                        }
                    }
                }
            }
        }

        $app_id = $PUSHER_APP_ID;
        $app_secret = $PUSHER_APP_SECRET;
        $app_key    = $PUSHER_APP_KEY;
        $app_cluster = $PUSHER_APP_CLUSTER;
        if (!empty($app_id) && !empty($app_secret) && !empty($app_key) && !empty($app_cluster)) {
            $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
            return $pusher;
        } else {
            return '';
        }
    }

    //update massal status penerimaaan
    public function update_status_massal(Request $request)
    {
        $data = array();
        $data['id_peserta'] = $request->id_peserta_siswa;
        $data['keputusan']  = $request->keputusan;
        $data['keterangan'] = $request->catatan;
        $response_show_detail = $this->AdminPpdbApi->update_massal_status(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }
}
