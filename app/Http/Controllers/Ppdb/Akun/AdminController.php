<?php

namespace App\Http\Controllers\Ppdb\Akun;

use App\ApiService\Ppdb\AdminApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AdminController extends Controller
{
    private $adminApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Admin Prakerin');
        $admin = $this->adminApi->get_by_sekolah();
        // dd($admin);
        $admin = $admin['body']['data'];
        return view('content.admin.ppdb.v_admin')->with(['admin' => $admin]);
    }

    public function detail(Request $request)
    {
        $post  = $this->adminApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function edit()
    {
        $admin = $this->adminApi->get_by_id(Help::decode($_GET['code']));
        // dd($admin);
        $admin = $admin['body']['data'];
        return view('content.admin.ppdb.v_edit_admin')->with(['admin' => $admin]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'username' => $request->username,
            'jenkel' => $request->jenkel,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'first_password' => $request->password,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->adminApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'username' => $request->username,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->adminApi->update_info(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        $admin = $this->data_admin();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'admin' => $admin
            ]);
        }
    }

    private function data_admin()
    {
        $admin = $this->adminApi->get_by_sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (!empty($admin)) {
            $no = 1;
            foreach ($admin as $adm) {
                $html .= '
                <tr>
                <td>' . $no++ . '</td>
                <td>' . $adm['nama'] . '</td>
                <td>' . $adm['username'] . '</td>
                <td>' . $adm['telepon'] . '</td>
                <td>' . $adm['email'] . '</td>
                <td>
                    <a href="' . route('admin_ppdb-edit', ['code' => Help::encode($adm['id']), 'name' => str_slug($adm['nama'])]) . '" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" data-id="' . $adm['id'] . '"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function res_pass(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->adminApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->adminApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->adminApi->update_foto(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
