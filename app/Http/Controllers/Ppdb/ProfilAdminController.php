<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class ProfilAdminController extends Controller
{

     protected $AdminPpdbApi;


     /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Informasi | Ubah Profil Admin ');
        $profil_admin = $this->AdminPpdbApi->get_info_profil_admin();
        $profil_data  = $profil_admin['body']['data'];
        session()->put('avatar',$profil_data['file']); //create session avatar
        return view('ppdb.components.Profil.v_profil_admin')->with(['profil_data'=>$profil_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_password()
    {
        Session::put('title', 'Informasi | Ubah Password Admin ');
        return view('ppdb.components.Profil.v_password_admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd("tes");

        // $request->validate([
        //     'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     'nama'=>'required|max:255',
        //     'alamat' => 'required|max:255',
        //     'kota'=>'required',
        //     'telephone'=>'required|numeric|digits:13',
        //     'email'=>'required|email|unique:users'
        // ],[
        //     'nama.required'=>'Form nama tidak boleh kosong!',
        //     'alamat.required'=>'Form alamat tidak boleh kosong!',
        //     'kota.required'=>'Form kota tidak boleh kosong!',
        //     'telephone.required'=>'Form telephone tidak boleh kosong!',
        //     'telephone.digits'=>'Nomor telephone maksimal 13 digit!',
        //     'telephone.numeric'=>'Nomor telephone hanya diperbolehkan berupa angka!',
        //     'email.required'=>'Form email tidak boleh kosong!',
        //     'email.email'=>'Format email tidak valid ! ',
        //     'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        // ]);

        // from request input form
        $data_post = array();
        $data_post['username']     = session('username');
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->AdminPpdbApi->update_profil_adminwithfile($json_body);
            File::delete($path);
        }else{
            // dd($data_post);
            $json_body = json_encode($data_post);
              //update ke api backend
            $response_update = $this->AdminPpdbApi->update_profil_admin($json_body);
        }
        // dd($response_update);

        if($response_update['code'] == '200'){
            //create session update username than redirect back
            session()->put('username',$request->nama);
            return back()->with('success','Successfully Profil Admin is saved!');
        }else{
            return back()->with('error',' Error Api Update Profil Admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {

        $request->validate([
            //'username'=>'required|max:255',
            'password_lama' => 'required|max:255',
            'password_baru'=>'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru'=>'required',
        ],[
            //'username.required'=>'Form username tidak boleh kosong!',
            'password_lama.required'=>'Form password lama tidak boleh kosong!',
            'password_baru.required'=>'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required'=>'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' =>'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        //$data_post['username']         = $request->username;
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->AdminPpdbApi->update_password_admin($json_body);

        //dd($response_update);

        if($response_update['code'] == '200'){
            //create session update username than redirect back
            session()->put('username',$request->username);
            return back()->with('success','Successfully Password Admin is saved!');
        }else{
            return back()->with('error',' Error Api Update Password Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
