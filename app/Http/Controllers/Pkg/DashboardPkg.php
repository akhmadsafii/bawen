<?php

namespace App\Http\Controllers\Pkg;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\PenilaianGuru\ApiPkgservice;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class DashboardPkg extends Controller
{
    protected $ApiPkgservice;

    public function __construct()
    {
        $this->ApiPkgservice = new ApiPkgservice();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_setting(session('id_sekolah'));
        if (!empty(session('tahun'))) {
            $tahun = session('tahun');
        } else {
            $tahun = \Carbon\Carbon::now()->format('Y');
        }
        $count_master = $this->ApiPkgservice->get_data_count_dashboard($tahun);
        $dashboard_max    = $this->ApiPkgservice->get_data_dashboard_max();
        $dashboard_min    = $this->ApiPkgservice->get_data_dashboard_min();
        $table_max_       = $this->ApiPkgservice->get_data_dashboard_max_periode($tahun);
        $table_min_       = $this->ApiPkgservice->get_data_dashboard_min_periode($tahun);
        $array_data_chart = array();
        $array_data_chart_min = array();
        $result_key = array();
        $result_keyx = array();
        if (!empty($dashboard_max['body']['data'])) {
            foreach ((array) $dashboard_max['body']['data'] as $key => $val) {
                $list_item = array();
                $list_item['periode'] = $val['tahun'];
                foreach ((array) $val['gurus'] as $tim) {
                    $list_item[$tim['nama']] = $tim['nilai_hasil'];
                }
                $array_data_chart[] = $list_item;
            }

            $current_key = array_keys(current($array_data_chart));
            $result_key = array_filter($current_key, function ($var) {
                return ($var != 'periode');
            });
        }

        if (!empty($dashboard_min['body']['data'])) {
            foreach ((array) $dashboard_min['body']['data'] as $key => $val) {
                $list_item = array();
                $list_item['periode'] = $val['tahun'];
                foreach ((array) $val['gurus'] as $tim) {
                    $list_item[$tim['nama']] = $tim['nilai_hasil'];
                }
                $array_data_chart_min[] = $list_item;
            }

            $current_key = array_keys(current($array_data_chart_min));
            $result_keyx = array_filter($current_key, function ($var) {
                return ($var != 'periode');
            });
        }

        $x =  "Sistem Informasi Management Penilaian Kinerja Guru ";
        Session::put('title', 'Sistem Informasi Management Penilaian Kinerja Guru');
        $rekap_guru = array();
        if (session('role') == 'guru-pkg') {
            $rekap_hasil_guru = $this->ApiPkgservice->get_data_rekap_guru(session('id'), $tahun);
            if (!empty($rekap_hasil_guru['body']['data'])) {
                $rekap_guru = $rekap_hasil_guru['body']['data'];
            }
        }
        $param = [
            'logx' => $x,
            'pengaturan' => $pengaturan['body']['data'] ?? array(),
            'count_pangkat' => $count_master['body']['data']['pangkat'] ?? '0',
            'count_jabatan' => $count_master['body']['data']['jabatan'] ?? '0',
            'count_pendidikan' => $count_master['body']['data']['pendidikan'] ?? '0',
            'count_guru' => $count_master['body']['data']['guru_penilai'] ?? '0',
            'count_guru_dinilai' => $count_master['body']['data']['guru_dinilai'] ?? '0',
            'count_guru_belum_dinilai' => $count_master['body']['data']['guru_belum_dinilai'] ?? '0',
            'array_chart' => $array_data_chart ?? array(),
            'array_key_chart' => array_values($result_key) ?? array(),
            'array_chart_min' => $array_data_chart_min ?? array(),
            'array_key_chart_min' => array_values($result_keyx) ?? array(),
            'array_table_chart' => $table_max_['body']['data'] ?? array(),
            'array_table_chart_min' => $table_min_['body']['data'] ?? array(),
            'rekap_guru' => $rekap_guru,
            'tahun'      => $tahun
        ];
        return view('penilaian_guru.components.dashboard')->with($param);
    }


    /** rekap rata rata survei guru dari tim penilaian  */
    public function rekap_rata_rata_survei()
    {

        if (!empty(session('tahun'))) {
            $tahun = session('tahun');
        } else {
            $tahun = \Carbon\Carbon::now()->format('Y');
        }

        $rekap_hasil_guru = $this->ApiPkgservice->get_data_rekap_guru(session('id'), $tahun);
        if (!empty($rekap_hasil_guru['body']['data'])) {
            $rekap_guru = $rekap_hasil_guru['body']['data'];
        }

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2020; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $param = [
            'rekap_guru' => $rekap_guru ?? array(),
            'tahun'      => $tahun,
            'tahunlist'  => $tahun_data
        ];

        return view('penilaian_guru.components.rekap_survei_guru')->with($param);
    }

    /** rekap rata rata survei guru dari tim penilaian  */
    public function rekap_rata_rata_surveifilter($tahun)
    {
        $rekap_hasil_guru = $this->ApiPkgservice->get_data_rekap_guru(session('id'), $tahun);
        if (!empty($rekap_hasil_guru['body']['data'])) {
            $rekap_guru = $rekap_hasil_guru['body']['data'];
        }

        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2020; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }

        $param = [
            'rekap_guru' => $rekap_guru ?? array(),
            'tahun'      => $tahun,
            'tahunlist'  => $tahun_data
        ];

        return view('penilaian_guru.components.rekap_survei_guru')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function profilUser()
    {
        Session::put('title', 'Profil');
        $profil_datax = $this->ApiPkgservice->get_info_profil();
        $param = [
            'profil_data' => $profil_datax['body']['data'] ?? array(),
        ];
        return view('penilaian_guru.components.profil')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_survei()
    { }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** update profil */
    public function update_profil(Request $request)
    {
        $data_post = array();
        $data_post['username']     = session('username');
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->ApiPkgservice->update_profil_file($json_body);
            File::delete($path);
        } else {
            $json_body = json_encode($data_post);
            //update ke api backend
            $response_update = $this->ApiPkgservice->update_profil($json_body);
        }

        if ($response_update['code'] == '200') {
            session()->put('username', $request->nama);
            $profil_datax = $this->ApiPkgservice->get_info_profil();
            if (!empty($profil_datax['body']['data']['file'])) {
                session()->put('avatar', $profil_datax['body']['data']['file']);
            }
            return back()->with('success', 'Successfully Profil  is saved!');
        } else {
            return back()->with('error', ' Error Api Update Profil ');
        }
    }

    /** update password  */
    public function store_update_password(Request $request)
    {
        $request->validate([
            'password_lama' => 'required|max:255',
            'password_baru' => 'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru' => 'required',
        ], [
            'password_lama.required' => 'Form password lama tidak boleh kosong!',
            'password_baru.required' => 'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required' => 'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' => 'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->ApiPkgservice->update_password($json_body);

        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->username);
            return back()->with('success', 'Successfully Password is saved!');
        } else {
            return back()->with('error', ' Error Api Update Password');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_admin()
    {
        Session::put('title', 'Halaman Master Data Administrator');
        return view('penilaian_guru.components.administrator.v_index');
    }

    /** ajax data master administrator  */
    public function ajax_master_data_admin(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_admin();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show admin  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit admin  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove admin  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('file', function ($row) {
                return $row['file'];
            });
            $table->rawColumns(['action', 'file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Saldo Akun  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_admin($id)
    {
        $akun = $this->ApiPkgservice->master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_admin(Request $request, $id)
    {
        $data = array();
        $data['username'] = $request->username;
        $data['id']       =  $id ?? $request->id_admin;
        $data['nama']     = $request->nama;
        $data['email']    = $request->email;
        $data['telepon']  = $request->telepon;
        $data['jenkel']   = $request->jenkel;
        $data['first_password'] = $request->password;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->ApiPkgservice->master_admin_post_file(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->ApiPkgservice->master_admin_post(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post_master_admin(Request $request)
    {
        $data = array();
        $data['username'] = $request->username_post;
        $data['nama']     = $request->nama_post;
        $data['email']    = $request->email_post;
        $data['telepon']  = $request->telepon_post;
        $data['jenkel']   = $request->jenkel_post;
        $data['first_password'] = $request->password_post;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image_post')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->ApiPkgservice->master_admin_post_filex(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->ApiPkgservice->master_admin_postx(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** ajax data  */
    public function ajax_trash_admin(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->trash_admin();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore admin  " data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent admin  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** restore admin  */
    public function restore_master_admin($id)
    {
        $akun = $this->ApiPkgservice->restore_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function remove_force_master_admin($id)
    {
        $akun = $this->ApiPkgservice->remove_permanent_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function destroy_master_admin($id)
    {
        $akun = $this->ApiPkgservice->remove_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data jabatan  */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_jabatan()
    {
        Session::put('title', 'Halaman Master Data Jabatan');
        return view('penilaian_guru.components.v_index_jabatan');
    }

    /** ajax data master jabatan  */
    public function ajax_master_data_jabatan(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_jabatan();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show jabatan  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit jabatan  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove jabatan  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Saldo Akun  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_jabatan($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_jabatanid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_jabatan(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_master_jabatanid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_jabatan(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_master_jabatan(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function destroy_master_jabatan($id)
    {
        $akun = $this->ApiPkgservice->remove_master_jabatan_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data Pendidikan  */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_pendidikan()
    {
        Session::put('title', 'Halaman Master Data Pendidikan');
        return view('penilaian_guru.components.v_index_pendidikan');
    }

    /** ajax data master jabatan  */
    public function ajax_master_data_pendidikan(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_pendidikan();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show pendidikan  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit pendidikan  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove pendidikan  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_pendidikan($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_pendidikanid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_pendidikan(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_master_pendidiakanid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_pendidikan(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_master_pendidikan(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function destroy_master_pendidikan($id)
    {
        $akun = $this->ApiPkgservice->remove_master_pendidikan_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data Pangkat  */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_pangkat()
    {
        Session::put('title', 'Halaman Master Data Pangkat');
        return view('penilaian_guru.components.v_index_pangkat');
    }

    /** ajax data master jabatan  */
    public function ajax_master_data_pangkat(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_pangkat();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show pangkat  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit pangkat  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove pangkat  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_pangkat($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_pangkatid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_pangkat(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['akk']       = $request->ak;
        $data['akpkb']     = $request->akpkb;
        $data['akp']       = $request->akp;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_master_pangkatid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_pangkat(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['akk']       = $request->ak_post;
        $data['akpkb']     = $request->akpkb_post;
        $data['akp']       = $request->akp_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_master_pangkat(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function destroy_master_pangkat($id)
    {
        $akun = $this->ApiPkgservice->remove_master_pangkat_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** display pengaturan setting */
    public function setting_pkg()
    {
        Session::put('title', 'Halaman Pengaturan');
        $pengaturan = $this->ApiPkgservice->get_data_master_setting(session('id_sekolah'));
        $param = [
            'pengaturan' => $pengaturan['body']['data'] ?? array()
        ];
        return view('penilaian_guru.components.v_index_pengaturan')->with($param);
    }

    /** post data pengaturan  */
    public function post_setting_pkg(Request $request)
    {
        $data = array();
        $data['nama_sekolah'] = $request->nama_sekolah_post;
        $data['kepala_sekolah'] = $request->kepala_sekolah_post;
        $data['nip_kepsek']     = $request->nip_kepala_sekolah_post;
        $data['kelurahan']    = $request->kelurahan_post;
        $data['kabupaten']    = $request->kabupaten_post;
        $data['kecamatan']    = $request->kecamatan_post;
        $data['provinsi']     = $request->provinsi_post;
        $data['alamat']       = $request->alamat_post;
        $data['mulai_penilaian'] = $request->tanggal_mulai;
        $data['akhir_penilaian'] = $request->tanggal_akhir;
        $data['tahun']           = $request->tahun_post;
        $data['id_sekolah']      = session('id_sekolah');

        $response_update = $this->ApiPkgservice->post_data_master_setting(json_encode($data));

        if ($response_update['code'] == '200') {
            session()->put('tahun', $request->tahun_post);
            return redirect()->back()->with(['success' => $response_update['body']['message']]);
        } else {
            return redirect()->back()->with(['error' => $response_update['body']['message']]);
        }
    }

    /** halaman master data Guru / Penilai  */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_guru_penilai()
    {
        Session::put('title', 'Halaman Master Data Guru Penilai');
        $pangkat = $this->ApiPkgservice->get_data_master_pangkat();
        $jabatan = $this->ApiPkgservice->get_data_master_jabatan();
        $pendidikan = $this->ApiPkgservice->get_data_master_pendidikan();
        $mapel      = $this->ApiPkgservice->master_mapel();
        $param = [
            'pangkat' => $pangkat['body']['data'] ?? array(),
            'jabatan' => $jabatan['body']['data'] ?? array(),
            'pendidikan' => $pendidikan['body']['data'] ?? array(),
            'mapel'      => $mapel['body']['data'] ?? array(),
            'pangkatx' => $pangkat['body']['data'] ?? array(),
            'jabatanx' => $jabatan['body']['data'] ?? array(),
            'pendidikanx' => $pendidikan['body']['data'] ?? array(),
            'mapelx'      => $mapel['body']['data'] ?? array()
        ];
        return view('penilaian_guru.components.v_index_guru')->with($param);
    }

    /** ajax data master guru penilai  */
    public function ajax_master_data_gurupenilai(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_guru_penilai();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show penilai  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit penilai  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove penilai  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->editColumn('telepon', function ($row) {
                return $row['telepon'] ?? '-';
            });

            $table->editColumn('alamat', function ($row) {
                return $row['alamat'] ?? '-';
            });

            $table->editColumn('tgl_lahir', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_lahir'])->isoFormat('dddd, D MMM Y');
            });
            $table->rawColumns(['action', 'photo', 'telepon', 'tgl_lahir', 'alamat']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_guru_penilai($id)
    {
        $akun = $this->ApiPkgservice->master_guru_penilaiid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_master_guru_penilai(Request $request)
    {
        $data = array();
        $data['id']       = $request->id_guru;
        $data['nama']     = $request->nama;
        $data['email']    = $request->email;
        $data['telepon']  = $request->telepon;
        $data['jenkel']   = $request->jenkel;
        $data['first_password'] = $request->password;
        $data['nip']            = $request->nip;
        $data['nik']            = $request->nik;
        $data['nuptk']          = $request->nuptk;
        $data['role']           = $request->role;
        $data['alamat']         = $request->alamat;
        $data['rt']             = $request->rt;
        $data['rw']             = $request->rw;
        $data['dusun']          = $request->dusun;
        $data['kecamatan']      = $request->kecamatan;
        $data['agama']          = $request->agama;
        $data['tempat_lahir']   = $request->tempat_lahir;
        $data['tgl_lahir']      = $request->tanggal_lahir;
        $data['kodepos']        = $request->kodepos;
        $data['id_jabatan']     = $request->jabatan;
        $data['id_pendidikan']  = $request->pendidikan;
        $data['id_pangkat']     = $request->pangkat;
        $data['id_mapel']       = $request->mapel;
        $data['informasi_lain'] = $request->informasi_lain;
        $data['terhitung_mulai_tanggal'] =  $request->terhitung_mulai_tanggal;
        $data['masa_kerja']             =  $request->masa_kerja;
        $data['lama_mengajar']            = $request->lama_mengajar;
        $data['jam_mengajar']            = $request->jam_mengajar;
        $data['jam_wajib_mengajar']      = $request->jam_wajib_mengajar;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->ApiPkgservice->update_master_guru_penilaifile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->ApiPkgservice->update_master_guru_penilai(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_guru_penilai(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['email']    = $request->email_post;
        $data['telepon']  = $request->telepon_post;
        $data['jenkel']   = $request->jenkel_post;
        $data['first_password'] = $request->password_post;
        $data['nip']            = $request->nip_post;
        $data['nik']            = $request->nik_post;
        $data['nuptk']          = $request->nuptk_post;
        $data['role']           = $request->role_post;
        $data['alamat']         = $request->alamat_post;
        $data['rt']             = $request->rt_post;
        $data['rw']             = $request->rw_post;
        $data['dusun']          = $request->dusun_post;
        $data['kecamatan']      = $request->kecamatan_post;
        $data['agama']          = $request->agama_post;
        $data['tempat_lahir']   = $request->tempat_lahir_post;
        $data['tgl_lahir']      = $request->tanggal_lahir_post;
        $data['kodepos']        = $request->kodepos_post;
        $data['id_jabatan']     = $request->jabatan_post;
        $data['id_pendidikan']  = $request->pendidikan_post;
        $data['id_pangkat']     = $request->pangkat_post;
        $data['id_mapel']       = $request->mapel_post;
        $data['informasi_lain'] = $request->informasi_lain_post;
        $data['terhitung_mulai_tanggal'] =  $request->terhitung_mulai_tanggal_post;
        $data['masa_kerja']              =  $request->masa_kerja_post;
        $data['lama_mengajar']            = $request->lama_mengajar_post;
        $data['jam_mengajar']             = $request->jam_mengajar_post;
        $data['jam_wajib_mengajar']       = $request->jam_wajib_mengajar_post;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->ApiPkgservice->post_master_guru_penilaifile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->ApiPkgservice->post_master_guru_penilai(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_guru_penilai($id)
    {
        $akun = $this->ApiPkgservice->remove_master_guruid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_force_master_guru_penilai($id)
    {
        $akun = $this->ApiPkgservice->remove_force_master_guruid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore_master_guru_penilai($id)
    {
        $akun = $this->ApiPkgservice->restore_master_guru_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** ajax trash master guru penilai  */
    public function ajax_trash_data_gurupenilai(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->trash_master_guru_penilai();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore penilai" data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent penilai  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->editColumn('telepon', function ($row) {
                return $row['telepon'] ?? '-';
            });

            $table->editColumn('alamat', function ($row) {
                return $row['alamat'] ?? '-';
            });

            $table->editColumn('tgl_lahir', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_lahir'])->isoFormat('dddd, D MMM Y');
            });
            $table->rawColumns(['action', 'photo', 'telepon', 'tgl_lahir', 'alamat']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** halaman master data Kompetensi*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_kompetensi()
    {
        Session::put('title', 'Halaman Master Data Kompetensi Penilaian');
        return view('penilaian_guru.components.v_index_kompetensi');
    }

    /** ajax data master kompetensi  */
    public function ajax_master_data_kompetensi(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_kompetensi();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show kompetensi  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit kompetensi  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    if ($data['id'] != 1) {
                        $button .= ' <button type="button" class="btn btn-danger btn-sm remove kompetensi  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    }
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_kompetensi($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_kompetensibyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_kompetensi(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['keterangan'] = $request->keterangan;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_master_kompetensibyid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_kompetensi($id)
    {
        $akun = $this->ApiPkgservice->remove_master_kompetensi($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_kompetensi(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['keterangan'] = $request->keterangan_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_master_kompetensi(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data indikator Kompetensi*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_indikator_kompetensi()
    {
        Session::put('title', 'Halaman Master Data Indikator Kompetensi Penilaian');
        $kompetensi = $this->ApiPkgservice->get_data_master_kompetensi();
        $param      = [
            'kompetensi_x' => $kompetensi['body']['data'] ?? array(),
            'kompetensix'   => $kompetensi['body']['data'] ?? array(),
        ];
        return view('penilaian_guru.components.v_index_kompetensi_indikator')->with($param);
    }

    /** ajax data indikator penilaian  */
    public function ajax_master_data_indikator(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_indikator();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show indikator  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit indikator  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove indikator  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action', 'id_kompetensi']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_indikator($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_indikatorid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_indikator_by_kompetensi($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_indikator_by_kompetensi($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** indikator per kompetensi */
    public function ajax_master_data_indikatorby_kompetensi(Request $request, $id)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_indikator_by_kompetensi($id);
        $result = $pengaturan['body']['data'] ?? array();
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_indikator(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['id_kompetensi'] = $request->id_kompetensi;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_master_indikatoribyid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_indikator($id)
    {
        $akun = $this->ApiPkgservice->remove_master_indikator($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_indikator(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['id_kompetensi'] = $request->id_kompetensi_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_master_indikatori(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data Kompetensi Sikap */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_kompetensi_sikap()
    {
        Session::put('title', 'Halaman Master Data  Kompetensi Sikap');
        return view('penilaian_guru.components.v_index_kompetensi_sikap');
    }

    /** ajax data master kompetensi sikap */
    public function ajax_master_data_kompetensiSikap(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_kompetensi_sikap();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show kompetensi sikap  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit kompetensi sikap " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    if ($data['id'] != 1) {
                        $button .= ' <button type="button" class="btn btn-danger btn-sm remove kompetensi sikap  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    }
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('keterangan', function ($row) {
                return $row['keterangan'] ?? '-';
            });
            $table->rawColumns(['action', 'keterangan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** indikator sikap detail per kompetensi */
    public function ajax_master_data_indikatorSikap(Request $request, $id)
    {
        $pengaturan = $this->ApiPkgservice->get_indikator_kompetensiSikap($id);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_kompetensi_sikap($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_komsikapbyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_kompetensi_sikap($id)
    {
        $akun = $this->ApiPkgservice->remove_master_kompetensisikap($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_kompetensisikap(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['keterangan'] = $request->keterangan;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_mastersikap_kompetensibyid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_kompetensisikap(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['keterangan'] = $request->keterangan_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_mastersikap_kompetensi(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data indikator Kompetensi Sikap */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_indikator_kompetensiSikap()
    {
        Session::put('title', 'Halaman Master Data Indikator Kompetensi Sikap');
        $kompetensi = $this->ApiPkgservice->get_data_master_kompetensi_sikap();
        $param      = [
            'sikap_x' => $kompetensi['body']['data'] ?? array(),
            'sikapx'   => $kompetensi['body']['data'] ?? array(),
        ];
        return view('penilaian_guru.components.v_index_kompetensi_indikatorsikap')->with($param);
    }

    /** ajax data indikator penilaian  */
    public function ajax_master_data_indikatorSikapCom(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_indikator_sikap();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show indikator sikap " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit indikator sikap " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove indikator sikap " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_indikator_sikap($id)
    {
        $akun = $this->ApiPkgservice->get_indikator_Sikap($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_indikator_sikap($id)
    {
        $akun = $this->ApiPkgservice->remove_master_indikatorsikap($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_indikator_sikap(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['id_kompetensi_sikap'] = $request->id_kompetensi_sikap;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_master_indikatorsikap_byid($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_indikator_sikap(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['id_kompetensi_sikap'] = $request->id_kompetensi_sikap_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_master_indikatorsikap(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data Kompetensi Lampiran */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_kompetensi_lampiranC()
    {
        Session::put('title', 'Halaman Master Data  Kompetensi LampiranC');
        return view('penilaian_guru.components.v_index_kompetensi_lampiran');
    }

    /** ajax data master kompetensi format C  */
    public function ajax_master_data_kompetensiFormatC(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_formatC();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show formatc  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit formatc  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    if ($data['id'] != '1') {
                        $button .= ' <button type="button" class="btn btn-danger btn-sm remove formatc  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    }
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_kompetensiFormatC($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_formatcbyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** ajax data master kompetensi  */
    public function ajax_master_data_formatCbyKompetensi(Request $request, $id)
    {
        $pengaturan = $this->ApiPkgservice->get_data_masterindikator_formatcbyid($id);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Remove   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_kompetensiFormatC($id)
    {
        $akun = $this->ApiPkgservice->remove_master_formatCKompetensi($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_kompetensiFormatC(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['kode']     = $request->kode;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_master_formatCKompetensi($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_kompetensiFormatC(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['kode']     = $request->kode_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_master_formatCKompetensi(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** halaman master data indikator Kompetensi Format C */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_indikator_kompetensiFormatC()
    {
        Session::put('title', 'Halaman Master Data Indikator Kompetensi Format C');
        $kompetensi = $this->ApiPkgservice->get_data_master_formatC();
        $param      = [
            'formatc_x' => $kompetensi['body']['data'] ?? array(),
            'formatcx'   => $kompetensi['body']['data'] ?? array(),
        ];
        return view('penilaian_guru.components.v_index_kompetensi_indikatorC')->with($param);
    }

    /** ajax data master kompetensi indikator format c  */
    public function ajax_master_data_indikatorFormatC(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_formatC_indikator();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show indikator formatc  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit indikator formatc  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove indikator formatc  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_formatc_kompetensiIndikator($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_formatC_indikatorid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_indikatorFormatC(Request $request, $id)
    {
        $data = array();
        $data['nama']     = $request->nama;
        $data['id_kompetensi_c'] = $request->id_kompetensi_c;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_master_formatCIndikator($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_master_indikatorFormatC($id)
    {
        $akun = $this->ApiPkgservice->remove_master_formatCKompetensiIndikator($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Post the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_master_indikatorFormatC(Request $request)
    {
        $data = array();
        $data['nama']     = $request->nama_post;
        $data['id_kompetensi_c'] = $request->id_kompetensi_c_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_master_formatCIndikator(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** halaman Pengaturan guru penilai periode  */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_settingGuruPenilai()
    {
        Session::put('title', 'Halaman Pengaturan Guru / Penilai berdasarkan Periode');
        $gurudata = $this->ApiPkgservice->get_data_master_guru_penilai();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2020; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        if (!empty($gurudata['body']['data'])) {
            $guru = array_filter($gurudata['body']['data'], function ($var) {
                return ($var['role'] == 'guru');
            });

            $penilai = array_filter($gurudata['body']['data'], function ($var) {
                return ($var['role'] == 'penilai');
            });
        }
        $param = [
            'guru' => $gurudata['body']['data'] ?? array(),
            'penilai' => $penilai ?? array(),
            'gurux' => $gurudata['body']['data'] ?? array(),
            'penilaix' => $penilai ?? array(),
            'tahunlist' => $tahun_data
        ];
        return view('penilaian_guru.components.v_index_settingGuruPenilai')->with($param);
    }

    /** ajax data master setting guru penilai   */
    public function ajax_master_settinggurupenilai(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_setting_guruPenilai();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit setting  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove setting  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->editColumn('photo_penilai', function ($row) {
                return '<img src="' . $row['file_penilai'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->rawColumns(['action', 'photo', 'photo_penilai']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax data master setting guru penilai   */
    public function ajax_periode_settinggurupenilai(Request $request, $tahun)
    {
        $pengaturan = $this->ApiPkgservice->get_data_periode_setting_guruPenilai($tahun);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit setting  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove setting  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->editColumn('photo_penilai', function ($row) {
                return '<img src="' . $row['file_penilai'] . '" width="100px" height="100px" class="rounded-circle">';
            });
            $table->rawColumns(['action', 'photo', 'photo_penilai']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_settinggurupenilai($id)
    {
        $akun = $this->ApiPkgservice->get_data_master_setting_guruPenilaiid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_setting_gurupenilai(Request $request, $id)
    {
        $data = array();
        $data['id_guru']     = $request->id_guru;
        $data['id_penilai']  = $request->id_penilai;
        $data['periode']     = $request->periode;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->update_data_settingguruPenilai($id, json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * POST the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post_setting_gurupenilai(Request $request)
    {
        $data = array();
        $data['id_guru']     = $request->id_guru_post;
        $data['id_penilai']  = $request->id_penilai_post;
        $data['periode']     = $request->periode_post;
        $data['id_sekolah']     = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_data_settingguruPenilai(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove_settingGuruPenilai($id)
    {
        $akun = $this->ApiPkgservice->remove_settingguruPenilai($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** ajax task penilaian    */
    public function ajax_task_penilaian_guru(Request $request, $id, $tahun)
    {
        $pengaturan = $this->ApiPkgservice->get_task_penilaian_guru($id, $tahun);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <a href="' . route('form_penilaianguru', ['id_guru' => $data['id_guru'], 'id_penilai' => $data['id_penilai'], 'periode' => $data['periode'], 'id_kompetensi' => '1']) . '" class="btn btn-info btn-sm show task-penilaian "><i class="material-icons">assignment</i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="80px" height="80px"  class="rounded-circle">';
            });
            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax task penilaian    */
    public function ajax_tim_penilaian_guru(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_setting_guruPenilai();

        if (session('role') == 'guru-pkg') {
            $result = array_filter($pengaturan['body']['data'], function ($var) {
                return ($var['id_guru'] == session('id'));
            });
        } else {
            $result = $pengaturan['body']['data'] ?? array();
        }

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <a href="' . route('form_penilaianguru', ['id_guru' => $data['id_guru'], 'id_penilai' => $data['id_penilai'], 'periode' => $data['periode'], 'id_kompetensi' => '1']) . '" class="btn btn-info btn-sm show task-penilaian "><i class="fa fa-eye"></i></a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="80px" height="80px"  class="rounded-circle">';
            });
            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** form penilaian guru  */
    public function form_penilaianguru($id_guru, $id_penilai, $periode, $kompetensi)
    {
        Session::put('title', 'Form Penilaian Kinerja Guru');
        $pengaturan = $this->ApiPkgservice->get_data_master_setting(session('id_sekolah'));
        if (empty($pengaturan['body']['data'])) {
            return redirect()->back()->with('error', 'Maaf Setting dulu Pengaturan Data Instansinya !');
        }
        if (!empty($pengaturan['body']['data']['akhir_penilaian'])) {
            $tanggal_akhir_kuisioner = $pengaturan['body']['data']['akhir_penilaian'];
            $tanggal_sekarang = \Carbon\Carbon::now()->format('Y-m-d');
            if ($tanggal_sekarang >= $tanggal_akhir_kuisioner) {
                return redirect()->back()->with('error', 'Maaf Pengisian Kuisioner Penilaian Guru telah Berakhir !');
            }
        }
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $detail_kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensibyid($kompetensi);
        $indikator_penilaian  = $this->ApiPkgservice->get_form_penilaian($id_guru, $id_penilai, $periode, $kompetensi);
        if (empty($indikator_penilaian['body']['data'])) {
            return redirect()->back()->with('error', 'Belum ada penilaian kinerja guru ' . session('username'));
        }
        $param = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ?? array(),
            'id_guru'             => $id_guru,
            'id_penilai'          => $id_penilai,
            'periode'             => $periode,
            'id_kompetensi'       => $kompetensi,
            'nama_guru'           => $indikator_penilaian['body']['data']['guru'] ?? '',
            'nip_guru'            => $indikator_penilaian['body']['data']['nip_guru'] ?? '',
            'detail_kompetensi'   => $detail_kompetensi_penilaian['body']['data'] ?? array(),
            'list_indikator'      => $indikator_penilaian['body']['data']['indikator_nilai'] ?? array()
        ];
        return view('penilaian_guru.components.v_form_penilaian')->with($param);
    }

    /** form penilaian sikap */
    public function form_penilaiangurusikap($id_guru, $id_penilai, $periode, $kompetensi)
    {
        Session::put('title', 'Form Penilaian Sikap Guru');
        $pengaturan = $this->ApiPkgservice->get_data_master_setting(session('id_sekolah'));
        if (empty($pengaturan['body']['data'])) {
            return redirect()->back()->with('error', 'Maaf Setting dulu Pengaturan Data Instansinya !');
        }
        if (!empty($pengaturan['body']['data']['akhir_penilaian'])) {
            $tanggal_akhir_kuisioner = $pengaturan['body']['data']['akhir_penilaian'];
            $tanggal_sekarang = \Carbon\Carbon::now()->format('Y-m-d');
            if ($tanggal_sekarang >= $tanggal_akhir_kuisioner) {
                return redirect()->back()->with('error', 'Maaf Pengisian Kuisioner Penilaian Guru telah Berakhir !');
            }
        }
        $kompetensi_sikap = $this->ApiPkgservice->get_data_master_kompetensi_sikap();
        $indikator_penilaian = $this->ApiPkgservice->get_form_penilaian_sikap($id_guru, $id_penilai, $periode, $kompetensi);
        if (empty($indikator_penilaian['body']['data'])) {
            return redirect()->back()->with('error', 'Belum ada penilaian kinerja guru ' . session('username'));
        }
        $param = [
            'kompetensi_penilaian' => $kompetensi_sikap['body']['data'] ?? array(),
            'id_guru'             => $id_guru,
            'id_penilai'          => $id_penilai,
            'periode'             => $periode,
            'id_kompetensi'       => $kompetensi,
            'nama_guru'           => $indikator_penilaian['body']['data']['guru'] ?? '',
            'nip_guru'            => $indikator_penilaian['body']['data']['nip_guru'] ?? '',
            'kompetensi_sikap'   => $indikator_penilaian['body']['data']['kompetensi_sikap'] ?? '',
            'kompetensi_sikap_keterangan' => $indikator_penilaian['body']['data']['keterangan_kompetensi_sikap'] ?? '',
            'list_indikator'      => $indikator_penilaian['body']['data']['indikator_nilai_sikap'] ?? array()
        ];

        return view('penilaian_guru.components.v_form_sikap')->with($param);
    }

    /** form lampiran format C */
    public function form_lampiran_formatC($id_guru, $id_penilai, $periode)
    {
        Session::put('title', 'Form Lampiran Format  C');
        $pengaturan = $this->ApiPkgservice->get_data_master_setting(session('id_sekolah'));
        if (empty($pengaturan['body']['data'])) {
            return redirect()->back()->with('error', 'Maaf Setting dulu Pengaturan Data Instansinya !');
        }
        if (!empty($pengaturan['body']['data']['akhir_penilaian'])) {
            $tanggal_akhir_kuisioner = $pengaturan['body']['data']['akhir_penilaian'];
            $tanggal_sekarang = \Carbon\Carbon::now()->format('Y-m-d');
            if ($tanggal_sekarang >= $tanggal_akhir_kuisioner) {
                return redirect()->back()->with('error', 'Maaf Pengisian Kuisioner Penilaian Guru telah Berakhir !');
            }
        }
        $indikator_penilaian = $this->ApiPkgservice->get_form_formatC($id_guru, $id_penilai, $periode);
        if (empty($indikator_penilaian['body']['data'])) {
            return redirect()->back()->with('error', 'Belum ada penilaian kinerja guru ' . session('username'));
        }
        $param = [
            'id_guru'             => $id_guru,
            'id_penilai'          => $id_penilai,
            'periode'             => $periode,
            'nama_guru'           => $indikator_penilaian['body']['data']['guru'] ?? '',
            'nip_guru'            => $indikator_penilaian['body']['data']['nip_guru'] ?? '',
            'list_indikator'      => $indikator_penilaian['body']['data']['komptensi_indikator'] ?? array()
        ];
        return view('penilaian_guru.components.v_form_formatc')->with($param);
    }

    /** store survei penilaian form */
    public function store_survei_penilaian_form(Request $request)
    {
        $data = array();
        $data['id_guru'] = $request->id_guru;
        $data['id_penilai'] = $request->id_penilai;
        $data['id_kompetensi'] = $request->id_kompetensi;
        $data['periode']       = $request->periode;
        foreach ((array) $request->option as $key => $val) {
            $data['id_indikator'] = $key;
            $data['nilai']        = $val;
        }
        $data['id_sekolah']       = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_survei_kompetensi_penilaian(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store survei penilaian sikap form */
    public function store_survei_sikap_form(Request $request)
    {
        $data = array();
        $data['id_guru'] = $request->id_guru;
        $data['id_penilai'] = $request->id_penilai;
        $data['id_kompetensi_sikap'] = $request->id_kompetensi;
        $data['periode']       = $request->periode;
        foreach ((array) $request->option as $key => $val) {
            $data['id_indikator_sikap'] = $key;
            $data['nilai']        = $val;
        }
        $data['id_sekolah']       = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_survei_kompetensi_sikapguru(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store survei penilaian sikap form */
    public function store_survei_formFormatC(Request $request)
    {
        $data = array();
        $data['id_guru'] = $request->id_guru;
        $data['id_penilai'] = $request->id_penilai;
        $data['id_kompetensi_c'] = $request->id_kompetensi_c;
        $data['periode']       = $request->periode;
        foreach ((array) $request->option as $key => $val) {
            $data['id_indikator_c'] = $key;
            $data['nilai']        = $val;
        }
        $data['id_sekolah']       = session('id_sekolah');
        $response_update = $this->ApiPkgservice->post_survei_kompetensi_FormatC(json_encode($data));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** form survei task */
    public function list_form_survei()
    {
        Session::put('title', 'List Form Survei Kinerja Guru');
        return view('penilaian_guru.components.list_task');
    }

    /** request penilaian guru  */
    public function request_form_survei()
    {
        Session::put('title', 'Pilih Tim Penilaian Kinerja Guru');
        $gurudata = $this->ApiPkgservice->get_data_master_guru_penilai();
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2020; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        if (!empty($gurudata['body']['data'])) {
            $guru = array_filter($gurudata['body']['data'], function ($var) {
                return ($var['role'] == 'guru');
            });

            $penilai = array_filter($gurudata['body']['data'], function ($var) {
                return ($var['role'] == 'penilai');
            });
        }
        $param = [
            'guru' => $guru ?? array(),
            'penilai' => $penilai ?? array()
        ];
        return view('penilaian_guru.components.request_penilaian')->with($param);
    }

    /** Display report  */
    public function report_pkg()
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2020; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        return view('penilaian_guru.components.v_report')->with(['tahunlist' => $tahun_data]);
    }

    /** ajax report pkg guru */
    public function ajax_report(Request $request)
    {
        $pengaturan = $this->ApiPkgservice->get_data_master_setting_guruPenilai();
        if (session('role') == 'guru-pkg') {
            $result = array_filter($pengaturan['body']['data'], function ($var) {
                return ($var['id_guru'] == session('id'));
            });
        } else {
            $result = $pengaturan['body']['data'] ?? array();
        }

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <a href="' . route('print_lampiran_pkg', ['id_guru' => $data['id_guru'], 'id_penilai' => $data['id_penilai'], 'periode' => $data['periode']]) . '" class="btn btn-info btn-sm show print " target="_blank"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax report  */
    public function ajax_periode_report(Request $request, $tahun)
    {
        $pengaturan = $this->ApiPkgservice->get_data_periode_setting_guruPenilai($tahun);

        if (session('role') == 'guru-pkg') {
            $result = array_filter($pengaturan['body']['data'], function ($var) {
                return ($var['id_guru'] == session('id'));
            });
        } else {
            $result = $pengaturan['body']['data'] ?? array();
        }

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <a href="' . route('print_lampiran_pkg', ['id_guru' => $data['id_guru'], 'id_penilai' => $data['id_penilai'], 'periode' => $data['periode']]) . '" class="btn btn-info btn-sm show print " target="_blank"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** print lampiran */
    public function print_lampiran($id_guru, $id_penilai, $periode, $kompetensi)
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $indikator_penilaian  = $this->ApiPkgservice->get_form_penilaian($id_guru, $id_penilai, $periode, $kompetensi);
        if (empty($indikator_penilaian['body']['data'])) {
            return redirect()->back()->with('error', 'Belum ada penilaian kinerja guru ' . session('username'));
        }
        $param  = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ??  array(),
            'id_guru'             => $id_guru,
            'id_penilai'          => $id_penilai,
            'periode'             => $periode,
            'id_kompetensi'       => $kompetensi,
            'nama_guru'           => $indikator_penilaian['body']['data']['guru'] ?? '',
            'nip_guru'            => $indikator_penilaian['body']['data']['nip_guru'] ?? '',
            'nama_penilai'        => $indikator_penilaian['body']['data']['penilai'] ?? '',
            'nip_penilai'        => $indikator_penilaian['body']['data']['nip_penilai'] ?? '',
            'nama_kompetensi'     => $indikator_penilaian['body']['data']['kompetensi'] ?? '',
            'keterangan_kompetensi' => $indikator_penilaian['body']['data']['keterangan_kompetensi'] ?? '',
            'list_indikator'      => $indikator_penilaian['body']['data']['indikator_nilai'] ?? array()
        ];
        return view('penilaian_guru.components.lampiranx')->with($param);
    }

    /** print lampiran 1 b */
    public function print_lampiran1b($id_guru, $id_penilai, $periode)
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $lampiran = $this->ApiPkgservice->get_data_lampiran($id_guru, $id_penilai, $periode);
        $param = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ??  array(),
            'id_guru' => $id_guru,
            'id_penilai' => $id_penilai,
            'periode'    => $periode,
            'lampiran' => $lampiran['body']['data'] ?? array(),
        ];
        return view('penilaian_guru.components.lampiranb')->with($param);
    }

    /** print lampiran 1 c */
    public function print_lampiran1c($id_guru, $id_penilai, $periode)
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $lampiran = $this->ApiPkgservice->get_data_lampiran($id_guru, $id_penilai, $periode);
        $indikator_penilaian = $this->ApiPkgservice->get_form_formatC($id_guru, $id_penilai, $periode);
        if (empty($indikator_penilaian['body']['data'])) {
            return redirect()->back()->with('error', 'Belum ada penilaian kinerja guru ' . session('username'));
        }
        $param = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ??  array(),
            'id_guru' => $id_guru,
            'id_penilai' => $id_penilai,
            'periode'    => $periode,
            'lampiran' => $lampiran['body']['data'] ?? array(),
            'list_indikator' => $indikator_penilaian['body']['data']['komptensi_indikator'] ?? array()
        ];
        return view('penilaian_guru.components.lampiranc')->with($param);
    }

    /** print lampiran 1 d */
    public function print_lampiran1d($id_guru, $id_penilai, $periode)
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $lampiran = $this->ApiPkgservice->get_data_lampiran($id_guru, $id_penilai, $periode);
        $lampiranx = $this->ApiPkgservice->get_data_lampiranD($id_guru, $id_penilai, $periode);
        $param = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ??  array(),
            'id_guru' => $id_guru,
            'id_penilai' => $id_penilai,
            'periode'    => $periode,
            'lampiran' => array_merge($lampiran['body']['data'], $lampiranx['body']['data']) ?? array(),
        ];
        return view('penilaian_guru.components.lampirand')->with($param);
    }


    /** print lampiran */
    public function print_lampiran_($id_guru, $id_penilai, $periode)
    {
        Session::put('title', 'Rekap Penilaian Kinerja guru');
        $kompetensi_penilaian = $this->ApiPkgservice->get_data_master_kompetensi();
        $param = [
            'kompetensi_penilaian' => $kompetensi_penilaian['body']['data'] ??  array(),
            'id_guru' => $id_guru,
            'id_penilai' => $id_penilai,
            'periode'    => $periode,
        ];
        return view('penilaian_guru.components.lampiran')->with($param);
    }
}
