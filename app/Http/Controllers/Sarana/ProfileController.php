<?php

namespace App\Http\Controllers\Sarana;

use App\ApiService\User\ProfileApi;

use App\Http\Controllers\Controller;

use App\ApiService\Sarana\User\AdminSaranaApi;
use App\ApiService\Sarana\User\UserSaranaApi;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

class ProfileController extends Controller
{
    private $profileApi;
    private $usersaranaApi;
    private $adminsaranaApi;
    private $id;

    public function __construct()
    {
        $this->usersaranaApi = new UserSaranaApi();
        $this->adminsaranaApi = new AdminSaranaApi();
    }

    public function index(){
        $id = session('id');
        if(session('role-sarana') == 'admin'){
                $data = $this->adminsaranaApi->get_by_id($id);
            }else{
                $data = $this->usersaranaApi->get_by_id($id);
            }
            $data = $data['body']['data'];
            //dd($data);
            return view('content.profile.profile_sarana.profile_sarana',compact('data'));
    }

    public function edit(Request $request){

        $data = array(
            'username' => $request->username,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
            'alamat' => $request->alamat
        );


        if ($request->file('image') != null) {
            $image = $request->file('image');
            $mimetype = $image->getMimeType();
            $image_base64 = base64_encode(file_get_contents($request->file('image')));
            //// Data yang dikirim ke API
            $data['file'] = "data:" . $mimetype . ";base64," . $image_base64;
        } else {
            //// Jika Gambar tidak di inputkan Data yang dikirim ke API menggunakan gambar No Image
            $data['file'] = Help::no_img_base64();
        }
        

        if(session('role-sarana') == 'admin'){
            $result = $this->adminsaranaApi->update_profile($data);
        }else{
            $result = $this->usersaranaApi->update_profile($data);
        }


        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }


    }

    public function change_password(Request $request){
        return view('content.profile.profile_sarana.password_sarana');
    }
}
