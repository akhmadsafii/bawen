<?php

namespace App\Http\Controllers\Sarana\Item;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\Item\ItemApi;
use App\ApiService\Sarana\Barang\BarangApi;
use App\ApiService\Sarana\Master\KategoriApi;
use App\ApiService\Sarana\Master\JenisApi;
use App\ApiService\Sarana\Master\LokasiApi;
use App\ApiService\Sarana\Master\SupllyerApi;
use App\ApiService\Sarana\Peminjaman\PeminjamanApi;

class ItemController extends Controller
{
	private $itemApi;
    private $barangApi;
	private $kategoriApi;
    private $jenisApi;
    private $lokasiApi;
    private $supllyerApi;
    private $pengadaanApi;
    private $peminjamanApi;

    function __construct(){
    	$this->itemApi = new ItemApi();
    	$this->barangApi = new BarangApi();
    	$this->kategoriApi = new KategoriApi();
        $this->jenisApi = new JenisApi();
        $this->lokasiApi = new LokasiApi();
        $this->supllyerApi = new SupllyerApi();
        $this->peminjamanApi = new PeminjamanApi();

    }

    function index(Request $request){
    	$lokasis = $this->lokasiApi->get_by_sekolah();
        $lokasis = $lokasis["body"]["data"];

        $supllyers = $this->supllyerApi->get_by_sekolah();
        $supllyers = $supllyers["body"]["data"];

    	$kategoris = $this->kategoriApi->get_by_sekolah();
        $kategoris = $kategoris["body"]["data"];

        $jeniss = $this->jenisApi->get_by_sekolah();
        $jeniss = $jeniss["body"]["data"];

        $barangs = $this->barangApi->get_by_sekolah();
        $barangs = $barangs["body"]["data"];

    	$data = $this->itemApi->get_by_sekolah();
        
    	$data = $data["body"]["data"];
        //dd($data);
        

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="modalEditItem('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deleteItem('.$row['id'].')" class="btn btn-danger btn-sm mx-1
                    ">
                        <i class="fa fa-trash ml-2" aria-hidden="true"></i>
                    </a>
                    <a onclick="riwayatItem('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.item.item',compact("jeniss","kategoris","lokasis","supllyers","barangs"));
    }

    function redirect(Request $request){
        $id = $request->id;
        return route('riwayat-item', $id);
    }

    function history($id,Request $request){
        $id = $id;

        $item = $this->itemApi->get_detail($id);
        $item = $item["body"]["data"];

        $data = $this->peminjamanApi->get_riwayat_item($id);
        $data = $data["body"]["data"];

        //dd($item);
        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.sarpras.item.riwayat_peminjaman',compact("data","item"));
    }

    function create(Request $request){
    	$data = array(
            'id_barang' => $request->id_barang,
            'id_lokasi' => $request->id_lokasi,
            'kode' => $request->kode,
            'tgl_diterima' => $request->tgl_diterima,
            'jumlah' => $request->jumlah,
            'kondisi' => $request->kondisi,
            'id_sekolah' => session('id_sekolah')
    	);
        
    	$result = $this->itemApi->create_many(json_encode($data));

    	if($result["code"] == 200){
    		return response()->json([
				'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
    		]);
    	}else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function detail(Request $request){
        $id = $request->id;

        $result = $this->itemApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function edit(Request $request){

        $id = $request->id;

        $data = array(
            'id_pengadaan' => $request->id_pengadaan,
            'id_barang' => $request->id_barang,
            'nama' => $request->nama,
            'kode' => $request->kode,
            'kondisi' => $request->kondisi,
            'id_jenis' => $request->id_jenis,
            'id_kategori' =>$request->id_kategori,
            'id_lokasi' => $request->id_lokasi,
            'id_sekolah' => session('id_sekolah'),
            'tgl_diperbarui' => now()->toDateString(),
        );
        //dd($data);

        $result = $this->itemApi->update($id,json_encode($data));

        //dd($result);

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function delete(Request $request){
        $id = $request->id;

        $result = $this->itemApi->soft_delete($id);

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    function riwayat($id,Request $request){

    }
}
