<?php

namespace App\Http\Controllers\Sarana\Master;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Sarana\Master\SupllyerApi;

class SupllyerBarangController extends Controller
{
    
    private $supllyerApi;

    public function __construct(){
        $this->supllyerApi = new SupllyerApi();
    }

    public function index(Request $request)
    {
        $data = $this->supllyerApi->get_by_sekolah();
        $data = $data["body"]["data"];
        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                <div class="input-group">
                    <a onclick="modalEditSupllyer('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deleteSupllyer('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="modalDetailSupllyer('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table->editColumn('files', function ($row) { 
                return '<a href="'.$row['file'].'" target="_blank">Lihat file</a>';
            });

            $table -> rawColumns(['aksi','files']);
            return $table->make(true);
        }
        return view('content.sarpras.master.supllyer');
    }

   public function create(Request $request)
    {
        //dd($request);
        $data_supllyer = array(
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'telepon' => $request->telepon,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/sarana/supllyer/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data_supllyer['path'] = $path;
            //dd($data_supllyer);
            $result = $this->supllyerApi->create_file(json_encode($data_supllyer));
            File::delete($path);
            //$code = $result['kode'];
            //$message = $result['data']['message'];
        }else{
            $result = $this->supllyerApi->create(json_encode($data_supllyer));
        }

        $result = $result['body']['message'];

        Session::flash('message', $result);   
        return redirect()->back();

    }

    public function detail(Request $request)
    {
        $id = $request->id;

        $result = $this->supllyerApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    
    public function edit(Request $request)
    {
        $data_supllyer = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );


        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/sarana/supllyer/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data_supllyer['path'] = $path;
            //dd($data_supllyer);
            $result = $this->supllyerApi->update_file(json_encode($data_supllyer));
            File::delete($path);
            $code = $result['kode'];
            $message = $result['data']['message'];
        }else{
            //dd($data_supllyer);
            $result = $this->supllyerApi->update(json_encode($data_supllyer));
        }
        //dd($result);

        $result = $result['body']['message'];

        //dd($result);
        Session::flash('message', $result);
        
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->supllyerApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

}
