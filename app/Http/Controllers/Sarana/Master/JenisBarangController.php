<?php

namespace App\Http\Controllers\Sarana\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\Master\JenisApi;

class JenisBarangController extends Controller
{
    private $jenisApi;

    public function __construct(){
        $this->jenisApi = new JenisApi();
    }

    public function index(Request $request)
    {
        $data = $this->jenisApi->get_by_sekolah();
        $data = $data["body"]["data"];
        $jenis = "jenis";


        //dd($data);
        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                <div class="input-group">
                    <a onclick="modalEditJenis('.$row['id'].')" class="btn btn-success btn-sm mt-0 mr-1">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deleteJenis('.$row['id'].')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash-o ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.master.jenis');
    }


    public function create(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
        );

        $result = $this->jenisApi->create(json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

        
    }

    
    public function detail(Request $request)
    {
        $id = $request->id;
        $result = $this->jenisApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

    
    public function edit(Request $request)
    {
        $id = $request->id;
        $data = array(
            
            'nama'=> $request->nama,
        );

        $result = $this->jenisApi->update($id,json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->jenisApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

}
