<?php

namespace App\Http\Controllers\Sarana\Peminjaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\User\UserSaranaApi;
use App\ApiService\Sarana\Item\ItemApi;
use App\ApiService\Sarana\Peminjaman\PeminjamanApi;

class PeminjamanController extends Controller
{
    private $userApi;
    private $itemApi;
	private $peminjamanApi;

	function __construct(){
		$this->itemApi = new ItemApi();
    	$this->userApi = new UserSaranaApi();
    	$this->peminjamanApi = new PeminjamanApi();
    }

    //DAFTAR BARANG SEMUA (ACTION)
    public function index(Request $request)
    {
    	$items = $this->itemApi->get_by_sekolah();
    	$items = $items["body"]["data"];

    	$users = $this->userApi->get_by_sekolah();
    	$users = $users["body"]["data"];

    	$data = $this->peminjamanApi->get_by_sekolah();
    	$data = $data["body"]["data"];

        //dd($data);

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">

                    <a onclick="modalEditPeminjaman('.$row['id'].')" class="btn btn-success btn-sm mr-1 mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.peminjaman',compact("data","users","items"));
    }


    //DAFTAR BARANG SEDANG DIPINJAM (READ ONLY)
    public function user_page(Request $request)
    {
        $items = $this->itemApi->get_by_sekolah();
        $items = $items["body"]["data"];

        $data = $this->peminjamanApi->get_dipinjam();
        $data = $data["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.user_peminjaman',compact("data","items"));
    }

    //DAFTER BARANG SEDANG DIPINJAM (ACTION)
    public function admin_page(Request $request)
    {
        $items = $this->itemApi->get_available_item();
    	$items = $items["body"]["data"];

    	$data = $this->peminjamanApi->get_dipinjam();
    	$data = $data["body"]["data"];

    	$users = $this->userApi->get_by_sekolah();
    	$users = $users["body"]["data"];

        //dd($items);

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">

                    <a onclick="modalEditPeminjaman('.$row['id'].')" class="btn btn-success btn-sm mr-1 mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.admin_peminjaman',compact("data","items","users"));
    }
    
    //PEMINJAMAN USER AKTIF SEDANG DIPINJAM 
    public function my(Request $request)
    {
        $id = session('id');

        $items = $this->itemApi->get_by_sekolah();
        $items = $items["body"]["data"];

        $data = $this->peminjamanApi->get_status_user($id,1);
        $data = $data["body"]["data"];
        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">

                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.peminjaman_saya',compact("data","items"));
    }

    //HISTORY PEMINJAMAN USER AKTIF (READ ONLY)
    public function history_saya(Request $request){
        $id = session('id');

        $data = $this->peminjamanApi->get_riwayat_user($id);
        $data = $data["body"]["data"];
        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.user_riwayat_peminjaman',compact("data"));
    }

    public function create(Request $request)
    {
    	if(session('role-sarana') == 'admin'){
    		$id_user = $request->id_user;
    	}else{
    		$id_user = session('id');
    	}

        if (!empty($request['id_item'])) {
            foreach ($request['id_item'] as $item) {
		    	$data = [
		            'id_item' => $item,
		            'id_user' => $id_user,
		            'tgl_sewa' => $request->tgl_sewa,
		            'tgl_kembali' => $request->tgl_kembali,
		            'keterangan' => $request->keterangan,
		            'id_sekolah' => session('id_sekolah')
		        ];

		        $result = $this->peminjamanApi->create(json_encode($data));
			}
		}

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request){

        $id = $request->id;


    	if(session('role-sarana') == 'admin'){
    		$id_user = $request->id_user;
    	}else{
    		$id_user = session('id');
    	}

        $data = [
            'id_item' => $request->id_item,
            'id_user' => $id_user,
            'tgl_sewa' => $request->tgl_sewa,
            'tgl_kembali' => $request->tgl_kembali,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah')
        ];

        $result = $this->peminjamanApi->update($id,json_encode($data));

        //dd($result);


        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }


    public function detail(Request $request){
        $id = $request->id;

        $result = $this->peminjamanApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }


    //RIWAYAT SEMUA BARANG
    public function riwayat(Request $request){
        $data = $this->peminjamanApi->get_by_sekolah();
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.riwayat_peminjaman',compact("data"));
    }

    //EXPIRED
    public function expired(Request $request){
        $data = $this->peminjamanApi->get_expired();
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPeminjaman('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.peminjaman.peminjaman_expired',compact("data"));
    }


    public function get_per_item(Request $request){
        $id = $request->id;
        $data = $this->peminjamanApi->get_per_item($id);
        $data = $data["body"]["data"];
        dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            return $table->make(true);
        }

    }


    public function delete(Request $request){
        $id = $request->id;

        $result = $this->peminjamanApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function kembali(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'status_kembali' => 1
        );


        $result = $this->peminjamanApi->kembali(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function reject(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id' => $request->id,
            'diterima' => 2,
            'tgl_diterima' => now()->toDateString()    
        );

        $result = $this->peminjamanApi->action(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
