<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class WaliKelasController extends Controller
{
    private $jurusanApi;
    private $walikelasApi;
    private $profileApi;
    private $guruApi;
    private $tahunajaranApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->walikelasApi = new WaliKelasApi();
        $this->guruApi = new GuruApi();
        $this->profileApi = new ProfileApi();
        $this->tahunajaranApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        Session::put('title', 'Data Wali Kelas');
        $tahun = $this->tahunajaranApi->get_tahun_ajaran();
        $tahun = $tahun['body']['data'];
        $url = $this->walikelasApi->import(session('id_sekolah'));
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        $walikelas = $this->walikelasApi->get_by_sekolah($_GET['tahun']);
        // dd($walikelas);
        if ($walikelas['code'] != 200) {
            $pesan = array(
                'message' => $walikelas['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $walikelas['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="reset_pass btn btn-warning btn-sm"><i class="fas fa-key"></i></button>';
                    return $button;
                });
            $table->editColumn('guru', function ($row) {
                return '<b>' . $row['nama'] . '</b><p class="m-0">NIP. ' . $row['nip'] . '</p>';
            });
            $table->editColumn('username', function ($row) {
                return '<p class="m-0">Login NIP. <span class="text-info">' . $row['login_nip'] . '</span></p><p class="m-0">Login NIK. <span class="text-info">' . $row['login_nik'] . '</span></p><p class="m-0">Login Telepon. <span class="text-info">' . $row['login_telp'] . '</span></p>';
            });
            $table->rawColumns(['action', 'guru', 'username']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.user.v_wali_kelas')->with([
            'data' => $result, 'template' => session('template'), 'tahun' => $tahun,
            'jurusan' => $jurusan, 'guru' => $guru, 'url' => $url
        ]);
    }

    public function store(Request $request)
    {
        $data_insert = array(
            'id_guru' => $request['id_guru'],
            'id_rombel' => $request['id_rombel'],
            'tahun' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->walikelasApi->create(json_encode($data_insert));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->walikelasApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'id_guru' => $request['id_guru'],
            'id_rombel' => $request['id_rombel'],
            'tahun' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->walikelasApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->walikelasApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->walikelasApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function restore($id)
    {
        $restore = $this->walikelasApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
    public function data_trash(Request $request)
    {
        Session::put('title', 'Data Trash Wali Kelas');
        $wali_kelas = $this->walikelasApi->all_trash();
        // dd($wali_kelas);
        $result = $wali_kelas['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm " onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function res_pass(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->walikelasApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->walikelasApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
