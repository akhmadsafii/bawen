<?php

namespace App\Http\Controllers\User;

use App\ApiService\Alumni\JurusanApi;
use App\ApiService\BKK\UserApi;
use App\ApiService\Kepegawaian\JabatanApi;
use App\ApiService\Kepegawaian\JenisApi;
use App\ApiService\Kepegawaian\PangkatApi;
use App\ApiService\Kepegawaian\SKApi;
use App\ApiService\Kepegawaian\StatusApi;
use App\ApiService\Master\JabatanSupervisorApi;
use App\ApiService\User\AdminApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\SiswaApi;
use App\ApiService\User\SupervisorApi;
//user mutu
use App\ApiService\PenjaminMutu\ProfileUser\UserMutuApi;
//sarana
use App\ApiService\Sarana\User\AdminSaranaApi;
use App\ApiService\Sarana\User\UserSaranaApi;

use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class ProfileController extends Controller
{
    private $guruApi;
    private $waliApi;
    private $supervisorApi;
    private $bkkApi;
    private $usermutuApi;
    private $jurusanAlumniApi;
    private $profileApi;
    private $pelamarApi;
    private $jabatanApi;
    private $skApi;
    private $kepJabatanApi;
    private $pangkatApi;
    private $kepJenisApi;
    private $statusApi;


    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->guruApi = new GuruApi();
        $this->supervisorApi = new SupervisorApi();
        $this->waliApi = new WaliKelasApi();
        $this->siswaApi = new SiswaApi();
        $this->adminApi = new AdminApi();
        $this->jurusanAlumniApi = new JurusanApi();
        $this->pelamarApi = new UserApi();
        $this->bkkApi = new UserApi();
        $this->usermutuApi = new UserMutuApi();
        $this->jurusanAlumniApi = new JurusanApi();
        $this->usersaranaApi = new UserSaranaApi();
        $this->adminsaranaApi = new AdminSaranaApi();
        $this->jabatanApi = new JabatanSupervisorApi();
        $this->skApi = new SKApi();
        $this->kepJabatanApi = new JabatanApi();
        $this->pangkatApi = new PangkatApi();
        $this->kepJenisApi = new JenisApi();
        $this->statusApi = new StatusApi();
    }

    public function edit(Request $request)
    {
        // dd("ping");
        Session::put('title', 'Edit Profile');
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $result = $profile['body']['data'];
        if (session('role') == 'admin' || session('role') == 'induk-admin' || session('role') == 'admin-absensi' || session('role') == 'learning-admin' || session('role') == 'admin-kepegawaian') {
            $template = 'default';
        } elseif (session('role') == 'bkk-admin') {
            $template = 'default';
            $request->session()->put('config', 'bursa_kerja');
        } elseif (session('role') == 'bkk-pelamar') {
            // dd(session()->all());
            $pelamar = $this->pelamarApi->get_by_id(session('id'));
            // dd($pelamar);
            $result = $pelamar['body']['data'];
            return view('content.bkk.dashboard.bkk-pelamar.profile.v_preview')->with(['data' => $result]);
        } elseif (session('role') == 'ortu') {
            $template = session('template');
        } elseif (session('role') == 'user-mutu') {
            $data = $this->usermutuApi->get_by_id(session('id'));
            $data = $data['body']['data'];
            //dd($data);
            return view('content.profile.profile_penjamin-mutu.profile_penjamin-mutu')->with(['data' => $data]);
        } else if (session('role') == 'user-sarpras' || session('role') == 'admin-sarpras') {
            if (session('role-sarana') == 'admin') {
                $data = $this->profileApi->get_profile();
            } else {
                $data = $this->profileApi->get_profile();
            }
            $data = $data['body']['data'];
            //dd($data);
            return view('content.profile.profile_sarana.profile_sarana', compact('data'));
        } else {
            $template = session('template');
            $result['tanggal'] = date('d/m/Y', strtotime($result['tgl_lahir']));
        }
        if (session('role') == 'alumni-alumni') {
            $jurusan = $this->jurusanAlumniApi->id_sekolah(session('id_sekolah'));
            $jurusan = $jurusan['body']['data'];
        } else {
            $jurusan = [];
        }
        // dd()
        if (session('config') == 'point' && session('role') == 'supervisor') {
            $template = 'default';
        }
        if (session('role') == 'supervisor') {
            $jabatan = $this->jabatanApi->get_by_sekolah();
            $jabatan = $jabatan['body']['data'];
            return view('content.profile.profile_supervisor.profile_supervisor')->with(['template' => $template, 'data' => $result, 'jurusan' => $jurusan, 'jabatan' => $jabatan]);
        }
        if (session('role') == 'pegawai') {
            $sk = $this->skApi->sekolah();
            $sk = $sk['body']['data'];
            $jabatan = $this->kepJabatanApi->sekolah();
            // dd($jabatan);
            $jabatan = $jabatan['body']['data'];
            $pangkat = $this->pangkatApi->sekolah();
            $pangkat = $pangkat['body']['data'];
            $jenis = $this->kepJenisApi->sekolah();
            $jenis = $jenis['body']['data'];
            $status = $this->statusApi->sekolah();
            $status = $status['body']['data'];
            // dd($status);
            return view('content.profile.profile_pegawai.profile_pegawai')->with([
                'template' => $template, 'data' => $result, 'sk' => $sk,
                'jabatan' => $jabatan, 'pangkat' => $pangkat, 'jenis' => $jenis, 'status' => $status
            ]);
        }
        // dd($result);
        return view('content.profile.profile_' . session('role') . '.profile_' . session('role'))->with(['template' => $template, 'data' => $result, 'jurusan' => $jurusan]);
    }

    public function edit_pelamar()
    {
        session()->put('title', 'Edit Pelamar');
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $result = $profile['body']['data'];
        $template = session('template');
        return view('content.profile.profile_bkk-pelamar.profile_bkk-pelamar')->with(['template' => $template, 'data' => $result]);
    }

    public function update_pelamar(Request $request)
    {
        // dd($request);
        $asal_sekolah = null;
        if($request['alumni'] == 0){
            $asal_sekolah = $request['asal_sekolah'];
        }
        $data = array(
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'agama' => $request->agama,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
            'pendidikan_akhir' => $request->pendidikan_akhir,
            'alamat' => $request->alamat,
            'alumni' => $request->alumni,
            'asal_sekolah' => $asal_sekolah,
        );
        $update = $this->profileApi->update_profile(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function update(Request $request)
    {
        // dd($request);

        if (session('role') == 'bkk-pelamar') {
            // dd($request);
            $profile = $this->profileApi->get_profile();
            $data_profile = $profile['body']['data'];
            unset($data_profile['file']);
            unset($data_profile['password']);
            unset($data_profile['first_password']);
            $data_profile['nama'] = $request['nama'];
            $data_profile['email'] = $request['email'];
            $data_profile['telepon'] = $request['telepon'];
            $data_profile['tempat_lahir'] = $request['tempat_lahir'];
            if ($request['tgl_lahir']) {
                $data_profile['tgl_lahir'] = $request['tgl_lahir'];
            } else {
                $data_profile['tgl_lahir'] = $request['tahun'] . "-" . $request['bulan'] . "-" . $request['tanggal'];
            }
            $data_profile['alamat'] = $request['alamat'];
            $update = $this->profileApi->update_profile(json_encode($data_profile));
            if ($update['code'] == 200) {
                return response()->json([
                    'success' => $update['body']['message'],
                    'icon'  => 'success',
                    'status'  => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'success' => $update['body']['message'],
                    'icon'  => 'error',
                    'status'  => 'gagal'
                ]);
            }
        } elseif (session('role') == 'user-mutu') {
            $data = array(
                'nama' => $request->nama,
                'no_induk' => $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
            //dd($data);
        } elseif (session('role') == 'user-sarpras' || session('role') == 'admin-sarpras') {
            $data = array(
                'nama' => $request->nama,
                'username' => $request->username,
                'no_induk' => $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
        } elseif (session('role') == 'pegawai') {
            $data = array(
                'nip' => $request->nip,
                'nik' => $request->nik,
                'nuptk' => $request->nuptk,
                'nama' => $request->nama,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'dusun' => $request->dusun,
                'kelurahan' => $request->kelurahan,
                'kecamatan' => $request->kecamatan,
                'kode_pos' => $request->kode_pos,
                'id_sk' => $request->id_sk,
                'id_pangkat_pegawai' => $request->id_pangkat_pegawai,
                'tmt_golongan' => date('Y-m-d', strtotime($request->tmt_golongan)),
                'id_jenis_pegawai' => $request->id_jenis_pegawai,
                'tmt_capeg' => date('Y-m-d', strtotime($request->tmt_capeg)),
                'id_status_pegawai' => $request->id_status_pegawai,
                'id_jabatan_pegawai' => $request->id_jabatan_pegawai,
                'digaji_menurut' => $request->digaji_menurut,
                'gaji_pokok' => str_replace('.', '', $request->gaji_pokok),
                'masa_kerja_golongan' => $request->masa_kerja_golongan,
                'masa_kerja_keseluruhan' => $request->masa_kerja_keseluruhan,
                'npwp' => $request->npwp,
                'id_sekolah' => session('id_sekolah')
            );
        } else {
            $data = array(
                'id' => $request->id ?? session('id'),
                'nik' => $request->nik,
                'nis' => $request->nis,
                'nisn' => $request->nisn,
                'tahun_angkatan' => $request->tahun_angkatan,
                'nuptk' => $request->nuptk,
                'status_pegawai' => $request->status_pegawai,
                'jenis_ptk' => $request->jenis_ptk,
                'pendidikan_terakhir' => $request->pendidikan_terakhir,
                'no_induk' => $request->no_induk,
                'id_jurusan' => $request->id_jurusan,
                'tahun_masuk' => $request->tahun_masuk,
                'tahun_lulus' => $request->tahun_lulus,
                'no_ijazah' => $request->no_ijazah,
                'id_kota' => $request->id_kota,
                'informasi_lain' => $request->informasi_lain,
                'id_jabatan' => $request->id_jabatan,
                'nama' => $request->nama,
                'pekerjaan' => $request->pekerjaan,
                'nip' => $request->nip,
                'jenkel' => $request->jenkel,
                'dusun' => $request->dusun,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kelurahan' => $request->kelurahan,
                'kecamatan' => $request->kecamatan,
                'kode_pos' => $request->kode_pos,
                'hp' => $request->hp,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'bio' => $request->bio,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'alamat' => $request->alamat,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            if (session('role') == 'alumni-alumni' || session('role') == 'admin-alumni') {
                $data['id_fakultas'] = Help::decode($request->id_fakultas);
            }
        }
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;

            if (session('role') == 'bkk-perusahaan') {
                $result = $this->bkkApi->update_info(json_encode($data));
            } elseif (session('role') == 'user-mutu') {
                //dd("ada foto");
                $result = $this->usermutuApi->update_profile_image(json_encode($data));
                return redirect()->back();
            } elseif (session('role') == 'user-sarpras' || session('role') == 'admin-sarpras') {
                //dd("ada foto");
                $result = $this->profileApi->update_profile_image(json_encode($data));
                return redirect()->back();
            } else {
                $result = $this->profileApi->update_profile_image(json_encode($data));
            }
            // dd($result);
            File::delete($path);
            if ($result['code'] == 200) {
                session()->put('usernmae', $request['nama']);
                $pesan = array(
                    'status' => 'berhasil',
                    'message' => $result['body']['message'],
                    'icon' => 'success'
                );
            } else {
                $pesan = array(
                    'status' => 'gagal',
                    'message' => $result['body']['message'],
                    'icon' => 'error'
                );
            }
            return response()->json($pesan);
        } else {
            if (session("role") == "bkk-perusahaan") {
                $result = $this->bkkApi->update(json_encode($data));
            } elseif (session('role') == 'user-mutu') {

                $result = $this->usermutuApi->update_profile(json_encode($data));
                //dd($result);
                return redirect()->back();
            } else if (session('role') == 'user-sarpras' || session('role') == 'admin-sarpras') {
                $result = $this->profileApi->update_profile(json_encode($data));
                return redirect()->back();
            } else {
                $result = $this->profileApi->update_profile(json_encode($data));
            }
            // dd($result);
            if ($result['code'] == 200) {
                session()->put('usernmae', $request['nama']);
                $pesan = array(
                    'status' => 'berhasil',
                    'message' => $result['body']['message'],
                    'icon' => 'success'
                );
            } else {
                $pesan = array(
                    'status' => 'gagal',
                    'message' => $result['body']['message'],
                    'icon' => 'error'
                );
            }
            return response()->json($pesan);
        }
    }

    public function change_password(Request $request)
    {
        $data_update = array(
            'current_password' => $request['current_password'],
            'new_password' => $request['new_password'],
            'confirm_password' => $request['confirm_password'],
        );
        $update = $this->profileApi->change_password(json_encode($data_update));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function v_change_password()
    {
        // dd(session('role'));
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        if (session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar') {
            return view('content.profile.profile_bkk-perusahaan.v_change_password')->with(['data' => $profile]);
        } else {
            return view('content.profile.v_change_password')->with(['data' => $profile]);
        }
    }

    public function reset_password()
    {
        if (session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar') {
            return view('content.profile.profile_bkk-perusahaan.v_reset_password')->with([]);
        } else {
            return view('content.profile.v_reset_password')->with([]);
        }
    }

    public function upload_paraf()
    {
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $profile = $profile['body']['data'];
        return view('content.profile.v_upload_paraf')->with(['profile' => $profile]);
    }

    public function update_paraf(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id' => session('id'),
        );

        $files = $request->file('image');
        if ($files) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            if (session('role') == 'walikelas') {
                $result = $this->waliApi->update_paraf(json_encode($data));
            } else {
                $result = $this->supervisorApi->update_paraf(json_encode($data));
            }
            File::delete($path);
        } else {
            $result = $this->guruApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }

        return redirect()->back()->with(['message' => $message]);
    }

    public function reset_passwords()
    {
        // dd(session('role'));
        if (session('role') == "admin") {
            $reset = $this->profileApi->reset_password_admin(session('id'));
        } elseif (session('role') == 'guru') {
            $reset = $this->profileApi->reset_password_guru(session('id'));
        } elseif (session('role') == 'siswa') {
            $reset = $this->profileApi->reset_password_siswa(session('id'));
        } elseif (session('role') == 'walikelas') {
            $reset = $this->profileApi->reset_password_walikelas(session('id'));
        } elseif (session('role') == 'superadmin') {
            $reset = $this->profileApi->reset_password_superadmin(session('id'));
        } elseif (session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar') {
            $reset = $this->profileApi->reset_password_user_bkk(session('id'));
        } elseif (session('role') == 'supervisor') {
            $reset = $this->profileApi->reset_password_supervisor(session('id'));
        } elseif (session('role') == 'admin-kepegawaian') {
            $reset = $this->profileApi->reset_password_admin_kepegawaian(session('id'));
        } elseif (session('role') == 'pegawai') {
            $reset = $this->profileApi->reset_password_pegawai(session('id'));
        }

        // dd($reset);
        if ($reset['code'] == 200) {
            return response()->json([
                'success' => $reset['body']['message'],
                'data' => $reset['body']['data'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $reset['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function preview()
    {
        if ((session('role') == 'bkk-pelamar')) {
            $profile = $this->bkkApi->get_by_id(session('id'));
            // dd($profile);
            $result = $profile['body']['data'];
            return view('content.bkk.dashboard.' . session('role') . '.profile.v_preview')->with(['data' => $result]);
        }
    }

    public function update_image(Request $request)
    {
        // dd("tes");
        $data = array(
            'id' => $request['id']
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bkkApi->update_image(json_encode($data));
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->bkkApi->update_info(json_encode($data));
            } else {
                return response()->json([
                    'success' => "Anda belum memilih gambar",
                    'icon'  => 'error',
                    'status'  => 'belum'
                ]);
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
