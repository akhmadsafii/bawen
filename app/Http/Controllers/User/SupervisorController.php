<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JabatanSupervisorApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\SupervisorApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class SupervisorController extends Controller
{
    private $profileApi;
    private $supervisorApi;
    private $jabatanApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->supervisorApi = new SupervisorApi();
        $this->jabatanApi = new JabatanSupervisorApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Supervisor');
        $url = $this->supervisorApi->import(session('id_sekolah'));
        // dd($url);
        $jabatan = $this->jabatanApi->get_by_sekolah();
        // dd($jabatan);
        $jabatan = $jabatan['body']['data'];
        $supervisor = $this->supervisorApi->get_by_sekolah();
        // dd($supervisor);
        if ($supervisor['code'] != 200) {
            $pesan = array(
                'message' => $supervisor['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $supervisor['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" class="key-' . $data['id'] . ' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'supervisor\')"><i class="fas fa-power-off"></i></button>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('ttl', function ($item) {
                return $item['tempat_lahir'] . ", " . Help::getTanggal($item['tgl_lahir']);
            });
            $table->rawColumns(['action', 'gambar', 'ttl']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.user.v_supervisor')->with(['jabatan' => $jabatan, 'url' => $url]);
    }

    public function store(Request $request)
    {
        $data = array(
            'id_jabatan' => $request->jabatan,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->supervisorApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->supervisorApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->supervisorApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        $result['jenkel'] = $result['jenkel'] == "Perempuan" ? "p" : "l";
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        // dd($result);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Supervisor berhasil diupdate',
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        }
        $data = array(
            'id' => $request->id,
            'nip' => $request->nip,
            'id_jabatan' => $request->jabatan,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->supervisorApi->update_info(json_encode($data));
            File::delete($path);
            $kode = $result['kode'];
            $sukses = $result['data']['message'];
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->supervisorApi->update(json_encode($data));
                $kode = $result['code'];
                $sukses = $result['body']['message'];
            } else {
                $data['hapus'] = "tidak";
                $result = $this->supervisorApi->update(json_encode($data));
                $kode = $result['code'];
                $sukses = $result['body']['message'];
            }
        }
        if ($kode == 200) {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => "Supervisor berhasil di hapus",
                'status' => 'berhasil'
            ]);
        }
        $delete = $this->supervisorApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->supervisorApi->delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $supervisor = $this->supervisorApi->all_trash();
        // dd($supervisor);
        $result = $supervisor['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->supervisorApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }


    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->supervisorApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'icon' => "success",
                'message' => "Password berhasil direset",
                'status' => 'berhasil'
            ]);
        }

        $reset = $this->profileApi->reset_password_supervisor($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
