<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\AlumniApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class AlumniController extends Controller
{
    private $tahunApi;
    private $alumniApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->tahunApi = new TahunAjaranApi();
        $this->alumniApi = new AlumniApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Alumni');
        $url = $this->alumniApi->import();
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $routes = "user-alumni";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->alumniApi->get_sekolah($page, $search);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('message', $pesan);
        }
        $alumni = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $template = session('template');
        if (session('role') == 'admin' || session('role') == 'bkk-admin') {
            $template = 'default';
        }
        return view('content.admin.user.v_alumni')->with(['template' => $template, 'url' => $url, 'jurusan' => $jurusan, 'tahun' => $tahun, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'alumni' => $alumni]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'id_jurusan' => $request->id_jurusan,
            'jenkel' => $request->jenkel,
            'email' => $request->email,
            'angkatan' => $request->tahun_angkatan1 . "/" . $request->tahun_ajaran2,
            'ponsel' => $request->telepon,
            'first_password' => $request->password,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->alumniApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->alumniApi->get_by_id($request['id']);
        $data = $post['body']['data'];
        return response()->json($data);
    }

    public function detail()
    {
        // dd("tes");
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $alumni = $this->alumniApi->get_by_id(Help::decode($_GET['code']));
        // dd($alumni);
        $alumni = $alumni['body']['data'];
        $template = session('template');
        return view('content.admin.user.v_edit_alumni', compact('template', 'alumni', 'jurusan'));
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'id_jurusan' => $request->id_jurusan,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'ponsel' => $request->ponsel,
            'pekerjaan' => $request->pekerjaan,
            'sesuai_jurusan' => $request->sesuai_jurusan,
            'bidang_usaha' => $request->bidang_usaha,
            'universitas' => $request->universitas,
            'program_studi' => $request->program_studi,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'angkatan' => $request->angkatan,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        $result = $this->alumniApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'warning',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->alumniApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->alumniApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $alumni = $this->alumniApi->all_trash();
        // dd($alumni);
        $result = $alumni['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->alumniApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->alumniApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function res_pass(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->alumniApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->alumniApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->alumniApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function delete_multiple(Request $request)
    {
        // dd($request);
        foreach ($request['id_alumni'] as $siswa) {
            $delete = $this->alumniApi->soft_delete($siswa);
        }
        // dd($delete);

        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
