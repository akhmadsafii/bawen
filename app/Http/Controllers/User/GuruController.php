<?php

namespace App\Http\Controllers\User;

use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Symfony\Component\Console\Question\Question;
use XcS\XcTools;

class GuruController extends Controller
{
    private $guruApi;
    private $profileApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        $url = $this->guruApi->import();
        Session::put('title', 'Data Guru');
        $guru = $this->guruApi->get_by_sekolah();
        if ($guru['code'] != 200) {
            $pesan = array(
                'message' => $guru['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $guru['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route('edit-detail_guru', ['k' => Help::encode($data['id']), 'name' => str_slug($data['nama'])]) . '" class="btn btn-info btn-sm m-1"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm m-1"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('jenis', function ($item) {
                // dd($item);
                if ($item['jenis_ptk'] == 'mapel') {
                    return '<i class="fas fa-chalkboard-teacher fa-2x text-info"></i> <br> <small class="text-info">Guru Mapel</small>';
                } else if ($item['jenis_ptk'] == 'bk') {
                    return '<i class="fas fa-user-cog fa-2x text-purple"></i><br> <small class="text-purple">Guru BK</small>';
                } else {
                    return '<i class="fas fa-user-tie fa-2x text-danger"></i> <br> <small class="text-danger">Belum di Set</small>';
                }
            });
            $table->editColumn('status', function ($st) {
                $checked = '';
                if ($st['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                    <input type="checkbox" ' . $checked . '
                            class="guru_check" data-id="' . $st['id'] . '">
                        <span class="slider round"></span>
                    </label>';
            });
            $table->rawColumns(['action', 'jenis', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        if (session('role') == 'supervisor') {
            return view('content.admin.user.v_s_guru')->with(['url' => $url, 'template' => session('template')]);
        }
        return view('content.admin.user.v_guru')->with(['url' => $url, 'template' => session('template')]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nip' => $request->nip,
            'nama' => $request->nama,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'status_pegawai' => $request->status_pegawai,
            'jenis_ptk' => $request->jenis_ptk,
            'id_sekolah' => session('id_sekolah'),
            'password' => $request['password'],
        );
        $result = $this->guruApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_guru'];
        $post  = $this->guruApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        $result['jenkel'] = $result['jenkel'] == "Perempuan" ? "p" : "l";
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'status_pegawai' => $request->status_pegawai,
            'jenis_ptk' => $request->jenis_ptk,
            'informasi_lain' => $request->informasi_lain,
            'nik' => $request->nik,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'hp' => $request->hp,
            'telepon' => $request->telepon,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        $result = $this->guruApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash(Request $request)
    {

        $delete = $this->guruApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->guruApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $guru = $this->guruApi->all_trash();
        // dd($guru);
        $result = $guru['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->guruApi->restore($request['id']);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->guruApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'icon' => "success",
                'message' => "Password berhasil direset",
                'status' => 'berhasil'
            ]);
        }
        $reset = $this->profileApi->reset_password_guru($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function detail()
    {
        // dd("detail guru");
        $guru = $this->guruApi->get_by_id(Help::decode($_GET['k']));
        // dd($guru);
        $guru = $guru['body']['data'];
        $template = session('template');
        return view('content.admin.user.v_edit_guru', compact('template', 'guru'));
    }

    public function res_pass(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->guruApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->guruApi->update_foto(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->guruApi->delete_profile($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->guruApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
