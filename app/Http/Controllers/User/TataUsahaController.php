<?php

namespace App\Http\Controllers\User;

use App\ApiService\User\ProfileApi;
use App\ApiService\User\TataUsahaApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\Helpers\Help;

class TataUsahaController extends Controller
{
    private $tatausahaApi;
    private $profileApi;

    public function __construct()
    {
        $this->tatausahaApi = new TataUsahaApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Tata Usaha');
        $url = $this->tatausahaApi->import();
        $tatausaha = $this->tatausahaApi->get_by_sekolah();
        // dd($tatausaha);
        if ($tatausaha['code'] != 200) {
            $pesan = array(
                'message' => $tatausaha['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $tatausaha['body']['data'];
        // dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route('detail_edit-tata_usaha', ['k' => Help::encode($data['id']), 'name' => str_slug($data['nama'])]) . '" class="btn btn-info btn-sm m-1"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm m-1"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('jenis', function ($item) {
                // dd($item);
                if ($item['jenis_ptk'] == 'laboran') {
                    return '<i class="fas fa-flask fa-2x text-info"></i> <br> <small class="text-info">Laboran</small>';
                } else if ($item['jenis_ptk'] == 'perpus') {
                    return '<i class="fas fa-book-reader fa-2x text-purple"></i><br> <small class="text-purple">Tenaga Perpustakaan</small>';
                } else if ($item['jenis_ptk'] == 'admin') {
                    return '<i class="fas fa-user-tag fa-2x text-facebook"></i><br> <small class="text-facebook">Tenaga Administrasi Sekolah</small>';
                } else if ($item['jenis_ptk'] == 'security') {
                    return '<i class="fas fa-shield-alt fa-2x text-success"></i><br> <small class="text-success">Petugas Keamanan</small>';
                } else if ($item['jenis_ptk'] == 'ob') {
                    return '<i class="fas fa-coffee fa-2x text-dark"></i><br> <small class="text-dark">Office Boy / Pesuruh</small>';
                } else {
                    return '<i class="fas fa-user-tie fa-2x text-danger"></i> <br> <small class="text-danger">Belum di Set</small>';
                }
            });
            $table->editColumn('status_pegawai', function ($sp) {
                // dd($item);
                if ($sp['status_pegawai'] == 'pns') {
                    return '<i class="fas fa-check-circle fa-2x text-success"></i> <br> <small class="text-success">PNS</small>';
                } elseif ($sp['status_pegawai'] == 'honorer') {
                    return '<i class="fas fa-user-circle fa-2x text-purple"></i><br> <small class="text-purple">Tenaga Kerja Honorer</small>';
                } else {
                    return '<i class="fas fa-times-circle fa-2x text-danger"></i><br> <small class="text-danger">Belum di input</small>';
                }
            });
            $table->editColumn('status', function ($st) {
                $checked = '';
                if ($st['status'] == 1) {
                    $checked = 'checked';
                }
                return '<label class="switch">
                        <input type="checkbox" ' . $checked . '
                                class="tu_check" data-id="' . $st['id'] . '">
                            <span class="slider round"></span>
                        </label>';
            });
            $table->rawColumns(['action', 'jenis', 'status', 'status_pegawai']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        if (session('role') == 'supervisor') {
            return view('content.admin.user.v_s_tata_usaha')->with(['url' => $url]);
        }
        return view('content.admin.user.v_tata_usaha')->with(['url' => $url]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'nuptk' => $request->nuptk,
            'status_pegawai' => $request->status_pegawai,
            'jenis_ptk' => $request->jenis_ptk,
            'id_sekolah' => session('id_sekolah'),
            'password' => $request->password,
        );
        $result = $this->tatausahaApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_tata_usaha'];
        $post  = $this->tatausahaApi->get_by_id($id);
        $result = $post['body']['data'];
        $result['jenkel'] = $result['jenkel'] == "Perempuan" ? "p" : "l";
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'nuptk' => $request->nuptk,
            'status_pegawai' => $request->status_pegawai,
            'jenis_ptk' => $request->jenis_ptk,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->tatausahaApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $id = $request['id_tata_usaha'];
        $delete = $this->tatausahaApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->tatausahaApi->delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $supervisor = $this->tatausahaApi->all_trash();
        // dd($supervisor);
        $result = $supervisor['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->tatausahaApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->tatausahaApi->upload_excel(json_encode($data));
        // dd($result);
        File::delete($path);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'icon' => "success",
                'message' => 'Password berhasil direset',
                'status' => 'berhasil'
            ]);
        }

        $reset = $this->profileApi->reset_password_tu($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function detail()
    {
        $tata_usaha = $this->tatausahaApi->get_by_id(Help::decode($_GET['k']));
        // dd($tata_usaha);
        $tata_usaha = $tata_usaha['body']['data'];
        $template = session('template');
        return view('content.admin.user.v_edit_tata_usaha', compact('template', 'tata_usaha'));
    }

    public function res_pass(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->tatausahaApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->tatausahaApi->update_foto(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->tatausahaApi->delete_profile($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->tatausahaApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
