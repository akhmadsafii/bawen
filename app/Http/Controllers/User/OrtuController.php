<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\OrangTuaApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class OrtuController extends Controller
{
    private $profileApi;
    private $ortuApi;
    private $rombelApi;
    private $kelasSiswaApi;
    private $jurusanApi;
    private $tahunApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->ortuApi = new OrangTuaApi();
        $this->rombelApi = new RombelApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        $routes = "user-orang_tua";
        $url = $this->ortuApi->import();
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        if (isset($_GET["jurusan"]) && $_GET["jurusan"] != '') {
            $jurusans = Help::decode($_GET["jurusan"]);
            $data_jr = $_GET['jurusan'];
        } else {
            $jurusans = "";
            $data_jr = "";
        }
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $asal = (isset($_GET["based"])) ? $_GET["based"] : " ";
        $tahuns = (isset($_GET["tahun"])) ? $_GET["tahun"] : "";
        $result = $this->ortuApi->get_by_sekolah($jurusans, $tahuns, $page, $search);
        //dd($result);
        $ortu = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::dataSiswa($data_jr, $tahuns, $meta, $routes, $search);
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        Session::put('title', 'Daftar Wali Murid');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];

        return view('content.point.bimbingan_konseling.v_wali_murid')->with([
            'template' => 'default', 'jurusan' => $jurusan, 'search' => $search, 'select_jurusan' => $data_jr,
            'routes' => $routes, 'tahun' => $tahun, 'tahuns' => $tahuns, 'pagination' => $pagination, 'asal' => $asal, 'ortu' => $ortu, 'rombel' => $rombel, 'url' => $url
        ]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json(
                [
                    'success' => 'Orang Tua berhasil ditambahkan',
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]
            );
        }
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'nama' => $request->nama,
            'nik' => $request->nik,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pekerjaan' => $request->pekerjaan,
            'id_siswa' => $request->siswa,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->ortuApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->ortuApi->create(json_encode($data));
        }

        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->ortuApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'nik' => $request->nik,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pekerjaan' => $request->pekerjaan,
            'id_siswa' => $request->siswa,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->ortuApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->ortuApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->ortuApi->update(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'warning',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->ortuApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->ortuApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $guru = $this->ortuApi->all_trash();
        // dd($guru);
        $result = $guru['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->ortuApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function ortu_edit(Request $request)
    {
        // dd($request);
        $id_rombel = $request['id_rombel'];
        $id = $request['id_siswa'];
        $post = $this->kelasSiswaApi->get_by_rombel($id_rombel, session('tahun'));
        $result = $post['body']['data'];
        $output = '';
        foreach ($result as $brand) {
            // dd($brand);
            $brand_id = $brand['id'];
            $brand_name = $brand['nama'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->ortuApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        $reset = $this->profileApi->reset_password_ortu($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
