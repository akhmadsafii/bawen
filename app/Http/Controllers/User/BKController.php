<?php

namespace App\Http\Controllers\User;

use App\ApiService\User\BKApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class BKController extends Controller
{
    private $profileApi;
    private $bkApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->bkApi = new BKApi();
    }

    public function index(Request $request)
    {
        $url = $this->bkApi->import();
        Session::put('title', 'Data User BK');
        $bk = $this->bkApi->get_by_sekolah();
        // dd($bk);
        if ($bk['code'] != 200) {
            $pesan = array(
                'message' => $bk['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $bk['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button"data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" class="key-'.$data['id'].' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'bk\')"><i class="fas fa-power-off"></i></button>';
                return $button;
            });
            $table->editColumn('ttl', function ($item) {
                return ucfirst($item['tempat_lahir']).", ".Help::getTanggal($item['tgl_lahir']);
            });
            $table->editColumn('gambar', function ($row) {
                return '<img src="'.$row['file'].'" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action','gambar','ttl']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.user.v_bk')->with(['url' => $url, 'template' => session('template')]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json(
                [
                    'success' => 'User BK berhasil ditambahkan',
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]
            );
        }

        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'informasi_lain' => $request->informasi_lain,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' =>date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bkApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->bkApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $bk  = $this->bkApi->get_by_id($request['id']);
        $result = $bk['body']['data'];
        $result['jenkel'] = $result['jenkel'] == "Perempuan" ? "p" : "l";
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if(session('demo')){
            return response()->json(
                [
                    'success' => 'User BK berhasil diupdate',
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]
            );
        }

        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'informasi_lain' => $request->informasi_lain,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' =>date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bkApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->bkApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->bkApi->update(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'error', 'status' => 'gagal']);
        }

    }

    public function trash(Request $request)
    {
        if(session('demo')){
            return response()->json([
                'success' => "success",
                'message' => 'User BK berhasil',
                'status' => 'berhasil'
            ]);
        }
        $delete = $this->bkApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }

    public function delete(Request $request)
    {
        $delete = $this->bkApi->delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $bk = $this->bkApi->all_trash();
        $result = $bk['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->bkApi->restore($request['id']);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->bkApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        if(session('demo')){
            return response()->json([
                'icon' => "success",
                'message' => 'Password berhasil direset',
                'status' => 'berhasil'
            ]);
        }

        $reset = $this->profileApi->reset_password_bk($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }
}
