<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;
use PDF;

class SiswaController extends Controller
{
    private $profileApi;
    private $siswaApi;
    private $jurusanApi;
    private $kelasSiswaApi;
    private $rombelApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->siswaApi = new SiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->rombelApi = new RombelApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Siswa');
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        $url = $this->kelasSiswaApi->import(session('id_sekolah'));
        $routes = "user-siswa";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->siswaApi->get_sekolah($page, $search);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $data = $result['body']['data'];
        //dd($data);
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $template = session('template');
        return view('content.admin.user.v_siswa', compact('url', 'template', 'jurusan', 'data', 'pagination', 'routes', 'search'));
    }

    public function store(Request $request)
    {
        $data = array(
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'kls_diterima' => $request->kls_diterima,
            'tgl_diterima' => date('Y-m-d', strtotime($request->tgl_diterima)),
            'id_sekolah' => session('id_sekolah'),
            'first_password' => $request['password'],
        );
        $result = $this->siswaApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit_detail()
    {
        $siswa = $this->siswaApi->get_by_id(Help::decode($_GET['k']));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        // $rombel = $this->rombelApi->get_by_sekolah();
        // $rombel = $rombel['body']['data'];
        $template = session('template');
        return view('content.admin.user.v_edit_siswa', compact('template', 'jurusan', 'siswa'));
    }

    public function edit(Request $request)
    {
        // dd($request);
        $id = $request['id_siswa'];
        $post  = $this->siswaApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'tahun_angkatan' => $request->tahun_angkatan,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'status_ortu' => $request->status_ortu,
            'jenis_tinggal' => $request->jenis_tinggal,
            'alat_transportasi' => $request->alat_transportasi,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'dusun' => $request->dusun,
            'lintang' => $request->lintang,
            'bujur' => $request->bujur,
            'anak_ke' => $request->anak_ke,
            'status_keluarga' => $request->status_keluarga,
            'kls_diterima' => $request->kls_diterima,
            'tgl_diterima' => $request->tgl_diterima,
            'no_ijazah' => $request->no_ijazah,
            'th_ijazah' => $request->th_ijazah,
            'no_skhun' => $request->no_skhun,
            'th_skhun' => $request->th_skhun,
            'nama_ayah' => $request->nama_ayah,
            'tahun_lahir_ayah' => $request->tahun_lahir_ayah,
            'pendidikan_ayah' => $request->pendidikan_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'penghasilan_ayah' => $request->penghasilan_dari . '###' . $request->penghasilan_sampai != null ? $request->penghasilan_sampai : 0,
            'telepon_ayah' => $request->telepon_ayah,
            'alamat_ayah' => $request->alamat_ayah,
            'nama_ibu' => $request->nama_ibu,
            'tahun_lahir_ibu' => $request->tahun_lahir_ibu,
            'pendidikan_ibu' => $request->pendidikan_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'telepon_ibu' => $request->telepon_ibu,
            'alamat_ibu' => $request->alamat_ibu,
            'keterangan_ortu' => $request->keterangan_ortu,
            'nama_wali' => $request->nama_wali,
            'pendidikan_wali' => $request->pendidikan_wali,
            'alamat_wali' => $request->alamat_wali,
            'telp_wali' => $request->telp_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'id_sekolah' => session('id_sekolah'),
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'kode_pos' => $request->kode_pos,
            // 'status' => 1
        );
        $result = $this->siswaApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash($id)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Siswa berhasil di hapus',
                'status' => 'berhasil'
            ]);
        }
        $delete = $this->siswaApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->siswaApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $guru = $this->siswaApi->all_trash();
        // dd($guru);
        $result = $guru['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->siswaApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->kelasSiswaApi->upload_excel(json_encode($data), session('tahun'));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        $reset = $this->profileApi->reset_password_siswa($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function res_pass(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->siswaApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->siswaApi->delete_profile($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        // dd($data);
        $result = $this->siswaApi->update_foto(json_encode($data));
        File::delete($path);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function delete_multiple(Request $request)
    {
        foreach ($request['id_siswa'] as $siswa) {
            $delete = $this->siswaApi->soft_delete($siswa);
        }
        // dd($delete);

        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function cetak_siswa()
    {
        // dd($key);
        $siswa = $this->siswaApi->get_by_id(Help::decode($_GET['k']));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        session()->put('title', $_GET['key']);
        // return view('content.admin.user.v_print_siswa', ['siswa' => $siswa]);
        $pdf = PDF::loadview('content.admin.user.v_print_siswa', ['siswa' => $siswa]);
        return $pdf->stream($_GET['key'] . '.pdf');
    }
}
