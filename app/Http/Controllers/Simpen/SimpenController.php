<?php

namespace App\Http\Controllers\Simpen;

use App\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Simpen\SimpenApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use App\Util\Utils;
use Illuminate\Support\Facades\File;
use XcS\XcTools;
use PDF;

class SimpenController extends Controller
{

    /**
     * __construct function new class Api
     */

    protected $api;
    protected $simpenApi;
    protected $sekolahApi;
    protected $tahunApi;

    public function __construct()
    {
        $this->simpenApi = new SimpenApi();
        $this->api = new Api();
        $this->sekolahApi = new SekolahApi();
        $this->tahunApi = new TahunAjaranApi();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Sistem Pengumuman Kelulusan ');
        if (!empty(session('role') == 'admin-simpen')) {

            $master_admin = $this->simpenApi->master_admin();
            //$master_siswa = $this->simpenApi->get_data_siswa();
            $setting = $this->simpenApi->get_data_setting(session('id_sekolah'));
            $dashboard = $this->simpenApi->get_data_dashboard(session('tahun'));
            $param = [
                'count_admin' => count($master_admin['body']['data']),
                'count_siswa' => $dashboard['body']['data']['total'] ?? '0',
                'jadwal_pengumuman' => $setting['body']['data']['tgl_buka'] ?? '',
                'count_lulus' => $dashboard['body']['data']['lulus'] ?? '0',
                'count_tidak_lulus' => $dashboard['body']['data']['tidak_lulus'] ?? '0',
                'surat_pendukung' => ''
            ];
            return view('simpen.components.simpen_index')->with($param);
        } else if (!empty(session('role') == 'siswa-simpen')) {
            if (!empty(session('id_sekolah'))) {
                $id_sekolah = session('id_sekolah');
            }
            $setting = $this->simpenApi->get_data_setting($id_sekolah);
            $sambutan = $this->simpenApi->get_data_sambutan($id_sekolah);
            $panduan  = $this->simpenApi->get_data_panduan($id_sekolah);
            $informasi = $this->simpenApi->get_data_informasi($id_sekolah);
            $pengumuman = $this->simpenApi->get_data_pengumuman(session('id'), $id_sekolah);
            $setting_pendukung = $this->simpenApi->get_data_setting_surat_pendukung($id_sekolah);

            $param = [
                'rows' => $setting['body']['data'] ?? array(),
                'sambutan' => $sambutan['body']['data'] ?? array(),
                'panduan'  => $panduan['body']['data'] ?? array(),
                'informasi' => $informasi['body']['data'] ?? array(),
                'pengumuman' => $pengumuman['body']['data'] ?? array(),
                'surat_pendukung' => $setting_pendukung['body']['data'] ?? array()
            ];
            return view('simpen.components.simpen_index_siswa')->with($param);
        } else {
            $this->api->logout();
            Session::flush();
            session()->put('login', false);
            session()->put('config', 'kelulusan');
            if (session('demo')) {
                $url = request()->url();
                $ex = explode('/', $url);
                $domain = $ex[2];
                $domain = preg_replace("/www./", "", $domain);
                $data = array(
                    'domain' => $domain
                );
                $sekolah = $this->sekolahApi->search_by_domain(json_encode($data));
                $sekolah = $sekolah['body']['data'];
                Session::put('id_sekolah', $sekolah['id']);
            }
            $pesan = array(
                'message' => 'Session user tidak ditemukan !',
                'icon' => 'error',
                'status' => 'error'
            );
            return redirect()->route('public-simpen')->with(['message' => $pesan]);
        }
    }

    public function pengumuman()
    {
        Session::put('title', 'Sistem Pengumuman Kelulusan ');
        if (!empty(session('id_sekolah'))) {
            $id_sekolah = session('id_sekolah');
        }
        $pengumuman = $this->simpenApi->get_data_pengumuman(session('id'), $id_sekolah);
        $setting_pendukung = $this->simpenApi->get_data_setting_surat_pendukung($id_sekolah);

        $param = [
            'pengumuman' => $pengumuman['body']['data'] ?? array(),
            'surat_pendukung' => $setting_pendukung['body']['data'] ?? array()
        ];
        return view('simpen.components.simpen_pengumuman_siswa')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_public()
    {
        //
        Session::put('title', 'Sistem Pengumuman Kelulusan ');
        if (!empty(session('id_sekolah'))) {
            $id_sekolah = session('id_sekolah');
            $setting = $this->simpenApi->get_data_setting($id_sekolah);
            $sambutan = $this->simpenApi->get_data_sambutan($id_sekolah);
            $panduan  = $this->simpenApi->get_data_panduan($id_sekolah);
            $informasi = $this->simpenApi->get_data_informasi($id_sekolah);
            $param = [
                'rows' => $setting['body']['data'] ?? array(),
                'sambutan' => $sambutan['body']['data'] ?? array(),
                'panduan'  => $panduan['body']['data'] ?? array(),
                'informasi' => $informasi['body']['data'] ?? array()
            ];
            return view('simpen.components.simpen_public')->with($param);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        Session::put('title', 'Pengaturan Template');
        $setting = $this->simpenApi->get_data_setting(session('id_sekolah'));
        $param = [
            'rows' => $setting['body']['data'] ?? array()
        ];
        return view('simpen.components.simpen_setting')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting_nilai_mapel()
    {
        Session::put('title', 'Pengaturan Nilai Mata Pelajaran');
        $kategori_nilai_mapel = $this->simpenApi->get_data_kategori_nilai(session('id_sekolah'));
        $parse_kategori       = $kategori_nilai_mapel['body']['data'] ?? array();
        $param = [
            'kategori_edit' => $parse_kategori,
            'kategori_post' => $parse_kategori
        ];
        return view('simpen.components.simpen_setting_mapel')->with($param);
    }

    /** ajax data master admin  */
    public function ajax_data_master_mapel(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $pengaturan = $this->simpenApi->get_data_nilaimapel($id_sekolah);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show mapel  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit mapel  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove mapel  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting_kategori_nilai()
    {
        Session::put('title', 'Master Data Kategori Nilai Mata Pelajaran');
        $param = [];
        return view('simpen.components.simpen_setting_mapel_kategori')->with($param);
    }

    /** ajax data master admin  */
    public function ajax_data_master_kategori(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $pengaturan = $this->simpenApi->get_data_kategori_nilai($id_sekolah);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show mapel  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit mapel  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display Saldo Akun  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_kategorin($id)
    {
        $akun = $this->simpenApi->get_data_kategori_byid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** update setting kategori mapel */
    public function store_update_nilaikategori(Request $request, $id)
    {
        $datapost = array();
        $datapost['nama']  = $request->nama_edit;
        $datapost['id_sekolah']    = session('id_sekolah');

        $response_update = $this->simpenApi->update_data_kategori_byid($id, json_encode($datapost));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** post setting kategori mapel */
    public function store_post_nilaikategori(Request $request)
    {
        $datapost = array();
        $datapost['nama']  = $request->nama_post;
        $datapost['kode']  = $request->kode_post;
        $datapost['id_sekolah']    = session('id_sekolah');

        $response_update = $this->simpenApi->post_data_kategori_mapel(json_encode($datapost));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting_sambutan()
    {
        Session::put('title', 'Halaman Sambutan');
        $setting = $this->simpenApi->get_data_sambutan(session('id_sekolah'));
        $param = [
            'rows' => $setting['body']['data'] ?? array()
        ];
        return view('simpen.components.simpen_master_sambutan')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting_panduan()
    {
        Session::put('title', 'Halaman Panduan');
        $setting = $this->simpenApi->get_data_panduan(session('id_sekolah'));
        $param = [
            'rows' => $setting['body']['data'] ?? array()
        ];
        return view('simpen.components.simpen_master_panduan')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting_informasi()
    {
        Session::put('title', 'Halaman Informasi');
        $setting = $this->simpenApi->get_data_informasi(session('id_sekolah'));
        $param = [
            'rows' => $setting['body']['data'] ?? array()
        ];
        return view('simpen.components.simpen_master_informasi')->with($param);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function master_admin()
    {
        Session::put('title', 'Master Administrator');
        $param = [];
        return view('simpen.components.master_admin')->with($param);
    }

    /** ajax data master admin  */
    public function ajax_data_master_admin(Request $request)
    {
        $pengaturan = $this->simpenApi->master_admin();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show admin  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit admin  " data-id="' . $data['id'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove admin  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('file', function ($row) {
                return $row['file'];
            });
            $table->rawColumns(['action', 'file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** store adjust  */
    public function store_setting_sambutan(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi' => 'required',
            'judul' => 'required'
        ], [
            'isi.required' => 'Form isi tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
            'judul.required' => 'Form judul tidak boleh kosong!'
        ]);

        $data = array();
        $data['judul']             = $request->judul;
        $data['isi']               = $request->isi;
        $data['id_sekolah']        = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->simpenApi->post_data_setting_sambutanfile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->post_data_setting_sambutan(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** store adjust  */
    public function store_setting_panduan(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi' => 'required',
            'judul' => 'required'
        ], [
            'isi.required' => 'Form isi tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
            'judul.required' => 'Form judul tidak boleh kosong!'
        ]);

        $data = array();
        $data['judul']             = $request->judul;
        $data['isi']               = $request->isi;
        $data['id_sekolah']        = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->simpenApi->post_data_setting_panduanfile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->post_data_setting_panduan(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** store adjust  */
    public function store_setting_informasi(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'isi' => 'required',
            'judul' => 'required'
        ], [
            'isi.required' => 'Form isi tidak boleh kosong!',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
            'judul.required' => 'Form judul tidak boleh kosong!'
        ]);

        $data = array();
        $data['judul']             = $request->judul;
        $data['isi']               = $request->isi;
        $data['id_sekolah']        = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->simpenApi->post_data_setting_informasifile(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->post_data_setting_informasi(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = array();
        $data['header1']         = $request->head1;
        $data['header2']         = $request->head2;
        $data['header3']         = $request->head3;
        $data['header4']         = $request->head4;
        $data['tingkat']       = $request->tingkat;
        $data['judul']         = $request->judul;
        $data['isi']           = $request->isi;
        $data['alamat']        = $request->alamat;
        $data['prolog']        = $request->prolog;
        $data['penutup']       = $request->penutup;
        $data['nama_kepsek']   = $request->nama_kepsek;
        $data['nip_kepsek']    = $request->nip_kepsek;
        $data['tempat_keputusan'] = $request->tempat_keputusan;
        $data['tgl_keputusan']    = $request->tanggal_keputusan;
        $data['tahun_ajaran']     = $request->tahun_ajaran;
        $data['tgl_buka']        = $request->tanggal_buka;
        $data['nomor_surat']     = $request->nomor_surat;
        $data['id_sekolah']         = session('id_sekolah');
        if ($files = $request->file('logo1')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['logo_pertama'] = $path;
            //$data['path'][0] = $path;
        }

        if ($files2 = $request->file('logo2')) {
            $namaFile = $files2->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files2->move($basePath, $imageName);
            $path2 = $basePath . $imageName;
            $data['logo_kedua'] = $path2;
            //$data['path'][1] = $path2;
        }

        if ($files3 = $request->file('stempel')) {
            $namaFile = $files3->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files3->move($basePath, $imageName);
            $path3 = $basePath . $imageName;
            $data['stempel'] = $path3;
            //$data['path'][2] = $path3;
        }
        if ($files4 = $request->file('ttd_kepsek')) {
            $namaFile = $files4->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files4->move($basePath, $imageName);
            $path4 = $basePath . $imageName;
            $data['ttd_kepsek'] = $path4;
            //$data['path'][3] = $path4;
        }

        if ($request->file('logo1') || $request->file('logo2') || $request->file('stempel') || $request->file('ttd_kepsek')) {
            $response_update = $this->simpenApi->update_data_setting_simpenfile(json_encode($data));
        } else {
            $response_update = $this->simpenApi->update_data_setting_simpen(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $responsed = $response_update['body']['data'];
            $tahunAjaran = explode("/", $request->tahun_ajaran);
            if (!empty($tahunAjaran)) {
                session()->put('tahun', $tahunAjaran[0]);
                session()->put('tahun_ajaran', $request->tahun_ajaran);
            }
            //create session
            Session::put('logo', $responsed['logo1']);

            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }

            return redirect()->route('setting-simpen')->with('success', $response_update['body']['message']);
        } else {
            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display Saldo Akun  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_admin($id)
    {
        $akun = $this->simpenApi->master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display   the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_master_mapel($id)
    {
        $akun = $this->simpenApi->get_data_nilaimapelbyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Display  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_master_mapel($id)
    {
        $akun = $this->simpenApi->remove_data_nilaimapelbyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }



    /**
     * Display  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore_master_mapel($id)
    {
        $akun = $this->simpenApi->restore_nilaiMapel($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Display  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function force_permanent_master_mapel($id)
    {
        $akun = $this->simpenApi->remove_permanent_data_nilaimapelbyid($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** ajax data  */
    public function ajax_trash_mapel(Request $request)
    {
        $pengaturan = $this->simpenApi->trash_nilaiMapel();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore mapel  " data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent mapel  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('status_tampil', function ($row) {
                if ($row['status_tampil'] == 1) {
                    return "<i class='fa fa-eye'></i>";
                } else if ($row['status_tampil'] == 0) {
                    return "<i class='fa fa-eye-slash'></i>";
                }
            });

            $table->rawColumns(['action', 'status_tampil']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }



    /** update setting mapel */
    public function store_update_nilaimpel(Request $request, $id)
    {
        $datapost = array();
        $datapost['kode'] = $request->kode_edit;
        $datapost['nama']  = $request->nama_edit;
        $datapost['jenis'] = $request->jenis_edit;
        $datapost['urutan'] = $request->urutan_edit;
        $datapost['status_tampil'] = $request->tampil_edit;
        $datapost['id_sekolah']    = session('id_sekolah');

        $response_update = $this->simpenApi->update_nilaimapel_setting(json_encode($datapost), $id);

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** update setting mapel */
    public function store_post_nilaimpel(Request $request)
    {
        $datapost = array();
        $datapost['kode'] = $request->kode_post;
        $datapost['nama']  = $request->nama_post;
        $datapost['jenis'] = $request->jenis_post;
        $datapost['urutan'] = $request->urutan_post;
        $datapost['status_tampil'] = $request->tampil_post;
        $datapost['id_sekolah']    = session('id_sekolah');

        $response_update = $this->simpenApi->post_nilaimapel_setting(json_encode($datapost));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function destroy_master_admin($id)
    {
        $akun = $this->simpenApi->remove_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** destroy admin  */
    public function remove_force_master_admin($id)
    {
        $akun = $this->simpenApi->remove_permanent_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    //'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /** restore admin  */
    public function restore_master_admin($id)
    {
        $akun = $this->simpenApi->restore_master_admin_id($id);
        if ($akun['code'] == '200') {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $akun['body']['message'],
                    'data' => $akun['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** ajax data  */
    public function ajax_trash_admin(Request $request)
    {
        $pengaturan = $this->simpenApi->trash_admin();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore admin  " data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent admin  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_master_admin(Request $request, $id)
    {
        $data = array();
        $data['username'] = $request->username;
        $data['id']       =  $id ?? $request->id_admin;
        $data['nama']     = $request->nama;
        $data['email']    = $request->email;
        $data['telepon']  = $request->telepon;
        $data['first_password'] = $request->password;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->simpenApi->master_admin_post_file(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->master_admin_post(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post_master_admin(Request $request)
    {
        $data = array();
        $data['username'] = $request->username_post;
        $data['nama']     = $request->nama_post;
        $data['email']    = $request->email_post;
        $data['telepon']  = $request->telepon_post;
        $data['first_password'] = $request->password_post;
        $data['id_sekolah']     = session('id_sekolah');

        if ($files = $request->file('image_post')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->simpenApi->master_admin_post_filex(json_encode($data));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->master_admin_postx(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** public function login verifiy */
    public function verify_login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $role = $request->role;
        $result = $this->api->login($username, $password, $role);
        if (isset($result['data']['access_token'])) {
            session()->put('token', $result['data']['access_token']);
            session()->put('role', $role);

            if (!empty($result['data']['id_sekolah'])) {
                session()->put('id_sekolah', $result['data']['id_sekolah']);
                $sekolah = $this->sekolahApi->get_by_id($result['data']['id_sekolah']);
                $sekolah = $sekolah['body']['data'];
                session()->put('sekolah', $sekolah['nama']);
            }

            if ((session('config') == 'kelulusan' && $role == 'admin-simpen')
                || (session('config') == 'kelulusan' && $role == 'siswa-simpen')
            ) {
                $message = array(
                    'message' => $result['message'],
                    'icon' => 'success',
                    'status' => 'berhasil'
                );
                $profil_admin = $this->simpenApi->get_info_profil();
                $profil_data  = $profil_admin['body']['data'] ?? array();
                session()->put('id',  $profil_data['id']);
                session()->put('avatar',  $profil_data['file']);
                session()->put('username', $profil_data['username']);
                session()->put('login', true);
                if (!empty($result['data']['tahun']) &&  !empty($result['data']['tahun_ajaran'])) {
                    session()->put('tahun', $result['data']['tahun']);
                    session()->put('tahun_ajaran', $result['data']['tahun_ajaran']);
                }
                return redirect()->route('dashboard-simpen-siswa')->with(['message' => $message]);
            }
        } else {
            $pesan = array(
                'message' => $result['message'],
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with(['message' => $pesan]);
        }
    }

    /** auth logout system  */
    public function logout(Request $request)
    {
        $this->api->logout();
        Session::flush();
        if (session('demo')) {
            $url = request()->url();
            $ex = explode('/', $url);
            $domain = $ex[2];
            $domain = preg_replace("/www./", "", $domain);
            $data = array(
                'domain' => $domain
            );
            $sekolah = $this->sekolahApi->search_by_domain(json_encode($data));
            $sekolah = $sekolah['body']['data'];
            Session::put('id_sekolah', $sekolah['id']);
        } else {
            $domain = request()->getHost();
            $domain = str_replace('www.', '', $domain);
            $sekolah = $this->sekolahApi->search_by_domain($domain);
            $sekolah = $sekolah['body']['data'];
            Session::put('id_sekolah', $sekolah['id']);
        }
        session()->put('login', false);
        session()->put('config', 'kelulusan');
        $pesan = array(
            'message' => 'Anda telah keluar dari sistem aplikasi ini !',
            'icon' => 'success',
            'status' => 'success'
        );
        return redirect()->route('public-simpen')->with(['message' => $pesan]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profil()
    {
        Session::put('title', 'Update Profil');
        $profil = $this->simpenApi->get_info_profil();
        $param = [
            'profil_data' => $profil['body']['data'] ?? array()
        ];
        return view('simpen.components.profil')->with($param);
    }

    /** update profil */
    public function updateProfil(Request $request)
    {
        $data_post = array();
        $data_post['username']     = session('username');
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->simpenApi->update_profil_adminwithfile($json_body);
            File::delete($path);
        } else {
            $json_body = json_encode($data_post);
            //update ke api backend
            $response_update = $this->simpenApi->update_profil_admin($json_body);
        }

        if ($response_update['code'] == '200') {
            session()->put('username', $request->nama);
            if (!empty($response_update['body']['data']['file'])) {
                $avatar = env('API_URL') . '/public/' . $response_update['body']['data']['file'];
                session()->put('avatar', $avatar);
            }
            return back()->with('success', 'Successfully Profil Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Profil Admin');
        }
    }

    /** update password  */
    public function update_password()
    {
        Session::put('title', 'Change Password');
        $param = [];
        return view('simpen.components.changePassword')->with($param);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_update_password(Request $request)
    {
        $request->validate([
            'password_lama' => 'required|max:255',
            'password_baru' => 'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru' => 'required',
        ], [
            'password_lama.required' => 'Form password lama tidak boleh kosong!',
            'password_baru.required' => 'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required' => 'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' => 'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->simpenApi->update_password_admin($json_body);

        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->username);
            return back()->with('success', 'Successfully Password Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Password Admin');
        }
    }


    /** ajax data  */
    public function ajax_trash_siswa(Request $request)
    {
        $pengaturan = $this->simpenApi->trash_siswa();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore siswa  " data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent siswa  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax data siswa */
    public function ajax_data_siswa(Request $request)
    {
        $pengaturan = $this->simpenApi->get_data_siswa();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    //$button .= ' <button type="button" class="btn btn-info btn-sm show siswa  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-info btn-sm edit siswa " data-id="' . $data['id'] . '"><i class="fa fa-pencil"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit-keputusan siswa  " data-id="' . $data['id'] . '"><i class="fa fa-exchange"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove siswa  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= ' <a href="' . route('print_skl_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i> SKL</a>&nbsp;';
                    $button .= ' <a href="' . route('print_ska_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i> SKA</a>&nbsp;';
                    $button .= ' <a href="' . route('print_nilai_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i>SKHU</a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });

            $table->editColumn('nis', function ($row) {
                return $row['nis'] ?? '-';
            });

            $table->editColumn('nisn', function ($row) {
                return $row['nisn'] ?? '-';
            });

            $table->editColumn('telepon', function ($row) {
                return $row['telepon'] ?? '-';
            });

            $table->editColumn('tgl_lahir', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_lahir'])->isoFormat('dddd, D MMM Y');
            });

            $table->editColumn('keputusan', function ($row) {
                if ($row['keputusan'] == 'lulus') {
                    return 'Lulus';
                } else if ($row['keputusan'] == 'tidak_lulus') {
                    return 'Tidak Lulus';
                }
            });

            $table->rawColumns(['action', 'photo', 'tgl_lahir', 'nisn', 'nis', 'keputusan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax data siswa */
    public function ajax_data_siswa_filter($tahun, Request $request)
    {
        $pengaturan = $this->simpenApi->filter_siswa($tahun);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    //$button .= ' <button type="button" class="btn btn-info btn-sm show siswa  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit siswa  " data-id="' . $data['id'] . '"><i class="fa fa-pencil"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-info btn-sm edit-keputusan siswa  " data-id="' . $data['id'] . '"><i class="fa fa-exchange"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove siswa  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= ' <a href="' . route('print_skl_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i> SKL</a>&nbsp;';
                    $button .= ' <a href="' . route('print_ska_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i> SKA</a>&nbsp;';
                    $button .= ' <a href="' . route('print_nilai_siswa', ['id_siswa' => $data['id']]) . '" class="btn btn-info btn-sm print siswa  " data-id="' . $data['id'] . '" target="_blank"><i class="fa fa-print"></i>SKHU</a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="100px" height="100px" class="rounded-circle">';
            });

            $table->editColumn('nis', function ($row) {
                return $row['nis'] ?? '-';
            });

            $table->editColumn('nisn', function ($row) {
                return $row['nisn'] ?? '-';
            });

            $table->editColumn('keputusan', function ($row) {
                if ($row['keputusan'] == 'lulus') {
                    return 'Lulus';
                } else if ($row['keputusan'] == 'tidak_lulus') {
                    return 'Tidak Lulus';
                }
            });

            $table->editColumn('tgl_lahir', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_lahir'])->isoFormat('dddd, D MMM Y');
            });

            $table->rawColumns(['action', 'photo', 'tgl_lahir', 'nis', 'nisn', 'keputusan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** halaman master data siswa  */
    public function master_siswa_index()
    {
        Session::put('title', 'Master Data Siswa');
        $id_sekolah = session('id_sekolah');
        $mapel = $this->simpenApi->get_data_nilaimapel($id_sekolah);
        $tahun_data = array();
        $year_now   = \Carbon\Carbon::now()->format('Y');
        for ($i = 2010; $i <= $year_now; $i++) {
            $tahun_data[] = $i;
        }
        $param = [
            'tahunlist' => $tahun_data,
            'request_tahun' => session('tahun'),
            'mapel' => $mapel['body']['data'] ?? array()
        ];
        return view('simpen.components.master_siswa')->with($param);
    }

    /** store siswa  */
    public function store_master_siswa(Request $request)
    {
        $datapost = array();
        $datapost['nisn'] = $request->nisn_post;
        $datapost['nis']  = $request->nis_post;
        $datapost['nama'] = $request->nama_post;
        $datapost['tempat_lahir'] = $request->tempat_lahir_post;
        $datapost['tgl_lahir']    = $request->tgl_lahir_post;
        $datapost['kelas']        = $request->kelas_post;
        $datapost['jurusan']      = $request->jurusan_post;
        $datapost['keputusan']    = $request->keputusan_post;
        $datapost['nomor_ujian']  = $request->nomor_ujian_post;
        $datapost['nomor_peserta'] = $request->nomor_peserta_post;
        $datapost['tahun_ajaran']  = $request->tahun_ajaran_post;
        $datapost['telepon']       = $request->telepon_post;
        $datapost['nik']           = $request->nik_post;
        $datapost['nama_ortu']     = $request->nama_orang_tua_post;
        $datapost['kurikulum']     = $request->kurikulum_post;
        $datapost['npsn']          = $request->npsn_post;
        $datapost['id_sekolah']    = session('id_sekolah');

        if ($files = $request->file('image_post')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $datapost['path'] = $path;
            $response_update = $this->simpenApi->post_data_siswafile(json_encode($datapost));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->post_data_siswa(json_encode($datapost));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** store siswa  */
    public function update_master_siswa(Request $request)
    {
        $datapost = array();
        $datapost['id']   = $request->id_siswa;
        $datapost['nisn'] = $request->nisn_edit;
        $datapost['nis']  = $request->nis_edit;
        $datapost['nama'] = $request->nama_edit;
        $datapost['tempat_lahir'] = $request->tempat_lahir_edit;
        $datapost['tgl_lahir']    = $request->tgl_lahir_edit;
        $datapost['kelas']        = $request->kelas_edit;
        $datapost['jurusan']      = $request->jurusan_edit;
        $datapost['keputusan']    = $request->keputusan_edit;
        $datapost['nomor_ujian']  = $request->nomor_ujian_edit;
        $datapost['nomor_peserta'] = $request->nomor_peserta_edit;
        $datapost['tahun_ajaran']  = $request->tahun_ajaran_edit;
        $datapost['nik']           = $request->nik_edit;
        $datapost['nama_ortu']     = $request->nama_orang_tua_edit;
        $datapost['kurikulum']     = $request->kurikulum_edit;
        $datapost['npsn']          = $request->npsn_edit;
        $datapost['id_sekolah']    = session('id_sekolah');

        if ($files = $request->file('image_edit')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/banner/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $datapost['path'] = $path;
            $response_update = $this->simpenApi->update_data_siswafile(json_encode($datapost));
            File::delete($path);
        } else {
            $response_update = $this->simpenApi->update_data_siswa(json_encode($datapost));
        }

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** store siswa  */
    public function update_keputusan_kelulusan(Request $request)
    {
        $datapost = array();
        $datapost['id']   = $request->id_siswax;
        $datapost['keputusan']    = $request->keputusan_editx;
        $datapost['id_sekolah']    = session('id_sekolah');

        $response_update = $this->simpenApi->update_data_kelulusan(json_encode($datapost));

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** import file excel data siswa  */
    public function import_file_datasiswa(Request $request)
    {
        if ($files = $request->file('file')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/import/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $datapost['path'] = $path;
            $response_update = $this->simpenApi->import_data_siswa(json_encode($datapost));
            File::delete($path);

            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /** get nilai siswa by id */
    public function getnilai_siswa($id)
    {
        $response_update = $this->simpenApi->get_nilai_siswabyid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** get nilai siswa by id */
    public function show_master_siswa($id)
    {
        $response_update = $this->simpenApi->get_data_siswa_byid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** remove master siswa */
    public function destroy_siswa($id)
    {
        $response_update = $this->simpenApi->remove_data_siswa_byid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** setting surat */
    public function setting_surat()
    {
        Session::put('title', 'Pengaturan Surat');
        $setting_surat = $this->simpenApi->get_data_settingsurat();
        // dd($setting_surat);
        $param = [
            'rows' => $setting_surat['body']['data'] ?? array()
        ];
        return view('simpen.components.setting_surat')->with($param);
    }

    /** ajax data setting surat */
    public function ajax_data_setting_surat(Request $request)
    {
        $pengaturan = $this->simpenApi->get_data_settingsurat();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show surat  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit surat  " data-id="' . $data['id'] . '"><i class="fa fa-pencil"></i></button>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove surat  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('status_tampil', function ($row) {
                if ($row['status_tampil'] == 1) {
                    return "<i class='fa fa-eye'></i>";
                } else if ($row['status_tampil'] == 0) {
                    return "<i class='fa fa-eye-slash'></i>";
                }
            });

            $table->rawColumns(['action', 'status_tampil']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** store update form setting surat */
    public function store_update_form_settingsurat(Request $request)
    {
        $data = [];
        for ($i = 1; $i <= $request['jumlah']; $i++) {
            $data[] = array(
                'id' => $request['id_surat_' . $i],
                'status_tampil' => $request['status_tampil_' . $i]
            );
        }
        $post_form = array('templates' => $data);
        // dd($post_form);
        $response_update  = $this->simpenApi->update_formsetting_surat(json_encode($post_form));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** get curl force download */

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    /** download file excel  */
    public function download_excel_mastersiswa()
    {
        $url = $this->simpenApi->downloadimport_data_siswa(session('id_sekolah'));
        $fileName   = \Carbon\Carbon::now()->timestamp . '_template_upload_siswa.xls';
        $PecahStr = explode(".", $fileName);
        $headers = ['Content-Type:' . $PecahStr[1]];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($url);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers)->deleteFileAfterSend(true);
    }

    /** nilai siswa master */
    public function nilai_siswa_index()
    {
        // dd(session()->all());
        $mapel = $this->simpenApi->get_data_nilaimapel(session('id_sekolah'));
        // dd($mapel);
        $mapel = $mapel['body']['data'];
        $routes = "nilai_siswa_index_simpen";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->simpenApi->siswa_by_sekolah($_GET['tahun'], $search, $page);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        // dd($result);
        $siswa = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $tahun = $this->tahunApi->get_tahun_ajaran();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        return view('simpen.components.nilai_siswa_new')->with([
            'mapel' => $mapel, 'siswa' => $siswa, 'pagination' => $pagination,
            'tahun' => $tahun, 'routes' => $routes, 'search' => $search, 'page' => $page
        ]);
    }

    // public function nilai_siswa_index()
    // {
    //     Session::put('title', 'Master Data Nilai Siswa ');
    //     $id_sekolah = session('id_sekolah');
    //     $mapel = $this->simpenApi->get_data_nilaimapel($id_sekolah);
    //     $param = [
    //         'mapel' => $mapel['body']['data'] ?? array()
    //     ];
    //     return view('simpen.components.nilai_siswa_index')->with($param);
    // }


    /** ajax data nilai siswa */
    public function ajax_data_nilai_siswa(Request $request)
    {
        $pengaturan = $this->simpenApi->get_data_nilai_siswa();
        // dd($pengaturan);
        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    //$button .= ' <button type="button" class="btn btn-info btn-sm show nilai  " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm edit nilai  " data-id="' . $data['id'] . '"><i class="fa fa-pencil"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove nilai  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /** get nilai siswa by id */
    public function show_master_nilai($id)
    {
        $response_update = $this->simpenApi->get_nilai_byid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /** remove nilai siswa by id */
    public function destroy_master_nilai(Request $request)
    {
        $response_update = $this->simpenApi->remove_nilai_byid($request['id']);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'status' => 'berhasil',
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'status' => 'gagal',
                    'info' => 'error',
                ]
            );
        }
    }

    /** remove nilai siswa by id */
    public function remove_permanent_master_nilai($id)
    {
        $response_update = $this->simpenApi->remove_permanent_nilai_byid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** remove nilai siswa by id */
    public function restore_master_nilai($id)
    {
        $response_update = $this->simpenApi->restore_nilai_byid($id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** update store nilai siswa  */
    public function update_nilai_siswa(Request $request)
    {
        // dd($request);
        $mapel = $this->simpenApi->get_data_nilaimapelbyid($request['mapel'][0]);
        $mapel = $mapel['body']['data'];
        $data = array(
            'id_siswa' => $request['id_siswa'],
            'id_nilai_mapel' => $request['mapel'][0],
            'initial' => $mapel['initial'],
            'nilai' => $request['nilai'][0],
            'id_sekolah' => session('id_sekolah'),
        );
        $update = $this->simpenApi->update_nilai_siswa(json_encode($data), $request['id']);
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    /** ajax data  */
    public function ajax_trash_nilai(Request $request)
    {
        $pengaturan = $this->simpenApi->get_trash_nilai_siswa();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-warning btn-sm restore nilai  " data-id="' . $data['id'] . '"><i class="fa fa-refresh"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove-permanent nilai  " data-id="' . $data['id'] . '"><i class="fa fa-remove"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** ajax data siswa */
    public function ajax_data_siswa_pilih(Request $request)
    {
        $pengaturan = $this->simpenApi->get_data_siswa();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm  pilih siswa  " data-id="' . $data['id'] . '" data-nama="' . $data['nama'] . '"><i class="fa fa-plus"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" width="50px" height="50px" class="rounded-circle">';
            });

            $table->editColumn('nis', function ($row) {
                return $row['nis'] ?? '-';
            });

            $table->editColumn('nisn', function ($row) {
                return $row['nisn'] ?? '-';
            });


            $table->rawColumns(['action', 'photo', 'tgl_lahir', 'nisn', 'nis']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** store input nilai manual */
    public function store_nilai_siswa(Request $request)
    {
        // $data = [];
        $nilai = $request['nilai'];
        $n = 0;
        foreach ($request['mapel'] as $mp) {
            $mapel = $this->simpenApi->get_data_nilaimapelbyid($mp);
            // dd($mapel);
            $mapel = $mapel['body']['data'];
            $data = array(
                "id_siswa" => $request['id_siswa'],
                "id_nilai_mapel" => $mp,
                "initial" => $mapel['initial'],
                "id_sekolah" => session('id_sekolah'),
                "nilai" => $nilai[$n++],
            );
            $result = $this->simpenApi->post_nilai_siswa(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        // dd($result);

        // if (!empty($request->akun_mapel_post)) {
        //     if (empty($request->siswa_id)) {
        //         return response()->json(
        //             [
        //                 'message' => 'Silahkan Pilih Siswa Terlebih Dahulu  ',
        //                 'data' => array(),
        //                 'info' => 'error',
        //             ]
        //         );
        //     }
        //     $checkdata = array();
        //     foreach ((array) $request->akun_mapel_post as $key => $val) {
        //         $data = array();
        //         $data['id_siswa'] = $request->siswa_id;
        //         $data['id_nilai_mapel'] = $val;
        //         $data['initial']        = $request->initial_post[$key];
        //         $data['nilai']          = $request->nilai_post[$key];
        //         $data['id_sekolah']     = session('id_sekolah');
        //         $checkdata[]            = $data;
        //         $response_update = $this->simpenApi->post_nilai_siswa(json_encode($data));
        //     }

        //     if ($response_update['code'] == '200') {
        //         return response()->json(
        //             [
        //                 'message' => $response_update['body']['message'],
        //                 'data' => $response_update['body']['data'] ?? array(),
        //                 'info' => 'success',
        //             ]
        //         );
        //     } else {
        //         return response()->json(
        //             [
        //                 'message' => $response_update['body']['message'],
        //                 'data' => $response_update['body']['data'] ?? array(),
        //                 'info' => 'error',
        //             ]
        //         );
        //     }
        // } else {
        //     return response()->json(
        //         [
        //             'message' => 'check Form ',
        //             'data' => array(),
        //             'info' => 'error',
        //         ]
        //     );
        // }
    }

    /** print pengumuman */
    public function print_pengumuman($id)
    {
        Session::put('title', 'Print Pengumuman Kelulusan');
        if (!empty(session('id_sekolah'))) {
            $id_sekolah = session('id_sekolah');
        }
        $setting = $this->simpenApi->get_data_setting($id_sekolah);
        $pengumuman = $this->simpenApi->get_data_pengumuman($id, $id_sekolah);
        $param = [
            'pengumuman' => $pengumuman['body']['data'] ?? array(),
            'rows' => $setting['body']['data'] ?? array(),
        ];
        return view('simpen.components.simpen_pengumuman_print')->with($param);
    }

    /** print pdf */
    public function print_pdf($id)
    {
        Session::put('title', 'Print Pengumuman Kelulusan');
        if (!empty(session('id_sekolah'))) {
            $id_sekolah = session('id_sekolah');
        }
        $setting = $this->simpenApi->get_data_setting($id_sekolah);
        $pengumuman = $this->simpenApi->get_data_pengumuman($id, $id_sekolah);
        $param = [
            'pengumuman' => $pengumuman['body']['data'] ?? array(),
            'rows' => $setting['body']['data'] ?? array(),
        ];

        $pdf = PDF::loadview('simpen.components.simpen_pengumuman_pdf', $param);
        $pdf->setPaper('legal', 'potrait');
        return $pdf->stream();
    }

    /** setting form letter other */
    public function setting_other()
    {
        Session::put('title', 'Setting Form Letter Other');
        $template_surat = $this->simpenApi->get_template_surat('skl', session('id_sekolah'));
        $parse_data     = $template_surat['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        return view('simpen.components.setting_surat_lainnya')->with($param);
    }

    /** setting form letter other */
    public function setting_otheralumni()
    {
        Session::put('title', 'Setting Form Letter Alumni');
        $template_surat = $this->simpenApi->get_template_surat('ska', session('id_sekolah'));
        $parse_data     = $template_surat['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        return view('simpen.components.setting_surat_alumni')->with($param);
    }

    /** setting form letter other */
    public function setting_otherskhun()
    {
        Session::put('title', 'Setting Form Letter Lain');
        $template_surat = $this->simpenApi->get_template_surat('lain', session('id_sekolah'));
        $parse_data     = $template_surat['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        return view('simpen.components.setting_surat_skhun')->with($param);
    }

    /** post setting surat kelulusan */
    public function post_setting_surat_kelulusan(Request $request)
    {
        $kategori = '';
        switch ($request->kategori_surat) {
            case 'surat_keterangan_lulus':
                $kategori = 'skl';
                break;
            case 'surat_keterangan_alumni':
                $kategori = 'ska';
                break;
            case 'surat_keterangan_lainnya':
                $kategori = 'lain';
                break;
            default:
                '';
        }

        $data = array();
        $data['kategori_surat']  = $kategori;
        $data['header1']         = $request->head1;
        $data['header2']         = $request->head2;
        $data['header3']         = $request->head3;
        $data['header4']         = $request->head4;
        $data['tingkat']       = $request->tingkat;
        $data['judul']         = $request->judul;
        $data['isi']           = $request->isi;
        $data['alamat']        = $request->alamat;
        $data['prolog']        = $request->prolog;
        $data['penutup']       = $request->penutup;
        $data['nama_kepsek']   = $request->nama_kepsek;
        $data['nip_kepsek']    = $request->nip_kepsek;
        $data['tempat_keputusan'] = $request->tempat_pembuatan;
        $data['tgl_keputusan']    = $request->tgl_surat;
        $data['nomor_surat']     = $request->nomor_surat;
        $data['id_sekolah']         = session('id_sekolah');
        if ($files = $request->file('logo1')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['logo_pertama'] = $path;
            //$data['path'][0] = $path;
        }

        if ($files2 = $request->file('logo2')) {
            $namaFile = $files2->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files2->move($basePath, $imageName);
            $path2 = $basePath . $imageName;
            $data['logo_kedua'] = $path2;
            //$data['path'][1] = $path2;
        }

        if ($files3 = $request->file('stempel')) {
            $namaFile = $files3->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files3->move($basePath, $imageName);
            $path3 = $basePath . $imageName;
            $data['stempel'] = $path3;
            //$data['path'][2] = $path3;
        }

        if ($files4 = $request->file('ttd_kepsek')) {
            $namaFile = $files4->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files4->move($basePath, $imageName);
            $path4 = $basePath . $imageName;
            $data['ttd_kepsek'] = $path4;
            //$data['path'][3] = $path4;
        }

        if ($request->file('logo1') || $request->file('logo2') || $request->file('stempel') || $request->file('ttd_kepsek')) {
            $response_update = $this->simpenApi->post_template_suratwithfile(json_encode($data));
        } else {
            $response_update = $this->simpenApi->post_template_surat(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $responsed = $response_update['body']['data'];
            //create session
            Session::put('logo', $responsed['logo1']);

            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }

            return redirect()->route('setting_template_other_simpen')->with('success', $response_update['body']['message']);
        } else {
            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** post setting surat alumni */
    public function post_setting_surat_alumni(Request $request)
    {
        $kategori = '';
        switch ($request->kategori_surat) {
            case 'surat_keterangan_lulus':
                $kategori = 'skl';
                break;
            case 'surat_keterangan_alumni':
                $kategori = 'ska';
                break;
            case 'surat_keterangan_lainnya':
                $kategori = 'lain';
                break;
            default:
                '';
        }

        $data = array();
        $data['kategori_surat']  = $kategori;
        $data['header1']         = $request->head1;
        $data['header2']         = $request->head2;
        $data['header3']         = $request->head3;
        $data['header4']         = $request->head4;
        $data['tingkat']       = $request->tingkat;
        $data['judul']         = $request->judul;
        $data['isi']           = $request->isi;
        $data['alamat']        = $request->alamat;
        $data['prolog']        = $request->prolog;
        $data['penutup']       = $request->penutup;
        $data['nama_kepsek']   = $request->nama_kepsek;
        $data['nip_kepsek']    = $request->nip_kepsek;
        $data['tempat_keputusan'] = $request->tempat_pembuatan;
        $data['tgl_keputusan']    = $request->tgl_surat;
        $data['nomor_surat']     = $request->nomor_surat;
        $data['id_sekolah']         = session('id_sekolah');
        if ($files = $request->file('logo1')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['logo_pertama'] = $path;
            //$data['path'][0] = $path;
        }

        if ($files2 = $request->file('logo2')) {
            $namaFile = $files2->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files2->move($basePath, $imageName);
            $path2 = $basePath . $imageName;
            $data['logo_kedua'] = $path2;
            //$data['path'][1] = $path2;
        }

        if ($files3 = $request->file('stempel')) {
            $namaFile = $files3->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files3->move($basePath, $imageName);
            $path3 = $basePath . $imageName;
            $data['stempel'] = $path3;
            //$data['path'][2] = $path3;
        }
        if ($files4 = $request->file('ttd_kepsek')) {
            $namaFile = $files4->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files4->move($basePath, $imageName);
            $path4 = $basePath . $imageName;
            $data['ttd_kepsek'] = $path4;
            //$data['path'][3] = $path4;
        }

        if ($request->file('logo1') || $request->file('logo2') || $request->file('stempel') || $request->file('ttd_kepsek')) {
            $response_update = $this->simpenApi->post_template_suratwithfile(json_encode($data));
        } else {
            $response_update = $this->simpenApi->post_template_surat(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $responsed = $response_update['body']['data'];
            //create session
            Session::put('logo', $responsed['logo1']);

            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }

            return redirect()->route('setting_otheralumni_simpen')->with('success', $response_update['body']['message']);
        } else {
            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** post setting surat other */
    public function post_setting_surat_other(Request $request)
    {
        $kategori = '';
        switch ($request->kategori_surat) {
            case 'surat_keterangan_lulus':
                $kategori = 'skl';
                break;
            case 'surat_keterangan_alumni':
                $kategori = 'ska';
                break;
            case 'surat_keterangan_lainnya':
                $kategori = 'lain';
                break;
            default:
                '';
        }

        $data = array();
        $data['kategori_surat']  = $kategori;
        $data['header1']         = $request->head1;
        $data['header2']         = $request->head2;
        $data['header3']         = $request->head3;
        $data['header4']         = $request->head4;
        $data['tingkat']       = $request->tingkat;
        $data['judul']         = $request->judul;
        $data['isi']           = $request->isi;
        $data['alamat']        = $request->alamat;
        $data['prolog']        = $request->prolog;
        $data['penutup']       = $request->penutup;
        $data['nama_kepsek']   = $request->nama_kepsek;
        $data['nip_kepsek']    = $request->nip_kepsek;
        $data['tempat_keputusan'] = $request->tempat_pembuatan;
        $data['tgl_keputusan']    = $request->tgl_surat;
        $data['nomor_surat']     = $request->nomor_surat;
        $data['id_sekolah']         = session('id_sekolah');
        if ($files = $request->file('logo1')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['logo_pertama'] = $path;
            //$data['path'][0] = $path;
        }

        if ($files2 = $request->file('logo2')) {
            $namaFile = $files2->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files2->move($basePath, $imageName);
            $path2 = $basePath . $imageName;
            $data['logo_kedua'] = $path2;
            //$data['path'][1] = $path2;
        }

        if ($files3 = $request->file('stempel')) {
            $namaFile = $files3->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files3->move($basePath, $imageName);
            $path3 = $basePath . $imageName;
            $data['stempel'] = $path3;
            //$data['path'][2] = $path3;
        }
        if ($files4 = $request->file('ttd_kepsek')) {
            $namaFile = $files4->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files4->move($basePath, $imageName);
            $path4 = $basePath . $imageName;
            $data['ttd_kepsek'] = $path4;
            //$data['path'][3] = $path4;
        }

        if ($request->file('logo1') || $request->file('logo2') || $request->file('stempel') || $request->file('ttd_kepsek')) {
            $response_update = $this->simpenApi->post_template_suratwithfile(json_encode($data));
        } else {
            $response_update = $this->simpenApi->post_template_surat(json_encode($data));
        }

        if ($response_update['code'] == '200') {
            $responsed = $response_update['body']['data'];
            //create session
            Session::put('logo', $responsed['logo1']);

            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }

            return redirect()->route('setting_otherskhun_simpen')->with('success', $response_update['body']['message']);
        } else {
            if (isset($path)) {
                if (file_exists($path)) {
                    file::delete($path);
                }
            }

            if (isset($path2)) {
                if (file_exists($path2)) {
                    file::delete($path2);
                }
            }

            if (isset($path3)) {
                if (file_exists($path3)) {
                    file::delete($path3);
                }
            }

            if (isset($path4)) {
                if (file_exists($path4)) {
                    file::delete($path4);
                }
            }
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** setting surat pendukung lainnya  */
    public function setting_pendukung_surat()
    {
        Session::put('title', 'Setting Surat Pendukung ');
        $setting_pendukung = $this->simpenApi->get_data_setting_surat_pendukung(session('id_sekolah'));
        $parse_data        = $setting_pendukung['body']['data'] ?? array();
        // dd($parse_data);
        $param             = ['rows' => $parse_data];
        return view('simpen.components.setting_surat_pendukung')->with($param);
    }

    /** post setting pendukung lainnya */
    public function store_setting_suratp(Request $request)
    {
        // dd($request);
        $data = array(
            'surat_nilai' => $request['surat_nilai'] ? 1 : 0,
            'surat_keterangan_lulus' => $request['surat_lulus'] ? 1 : 0,
            'surat_keterangan_alumni' => $request['surat_alumni'] ? 1 : 0,
            'surat_keterangan_lain' => $request['surat_lain'] ? 1 : 0,
            'id_sekolah' => session('id_sekolah')
        );
        $response_update  = $this->simpenApi->post_setting_surat_pendukung(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** print cetak nilai  */
    public function print_nilai_siswa($id_siswa)
    {
        Session::put('title', 'SURAT KETERANGAN HASIL UJIAN SEKOLAH');
        $data_nilai = $this->simpenApi->print_nilai_ujian(session('id_sekolah'), $id_siswa);
        $parse_data = $data_nilai['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        $pdf = PDF::loadview('simpen.components.print_nilai_siswa', $param);
        $pdf->setPaper('legal', 'potrait');
        return $pdf->stream();
    }

    /** print skl */
    public function print_skl_siswa($id_siswa)
    {
        Session::put('title', 'Cetak Surat Keterangan Lulus');
        $data_nilai = $this->simpenApi->print_skl(session('id_sekolah'), $id_siswa);
        $parse_data = $data_nilai['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        $pdf = PDF::loadview('simpen.components.print_skl', $param);
        $pdf->setPaper('legal', 'potrait');
        return $pdf->stream();
    }

    /** print ska */
    public function print_ska_siswa($id_siswa)
    {
        Session::put('title', 'Cetak Surat Keterangan Alumni');
        $data_nilai = $this->simpenApi->print_ska(session('id_sekolah'), $id_siswa);
        $parse_data = $data_nilai['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        $pdf = PDF::loadview('simpen.components.print_ska', $param);
        $pdf->setPaper('legal', 'potrait');
        return $pdf->stream();
    }

    /** print surat lainnya */
    public function print_surat_lainnya($id_siswa)
    {
        Session::put('title', 'Cetak Surat Keterangan Lainnya');
        $data_nilai = $this->simpenApi->print_surat_lain(session('id_sekolah'), $id_siswa);
        $parse_data = $data_nilai['body']['data'] ?? array();
        $param = ['rows' => $parse_data];
        $pdf = PDF::loadview('simpen.components.print_surat_lain', $param);
        $pdf->setPaper('legal', 'potrait');
        return $pdf->stream();
    }

    /** print all  */
    public function print_all_document($id_siswa)
    {
        Session::put('title', 'Cetak Surat Keterangan Lainnya');
        $data_nilai = $this->simpenApi->print_all(session('id_sekolah'), $id_siswa);
        $parse_data = $data_nilai['body']['data'] ?? array();
        dd($parse_data);
    }
}
