<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\EnvironmentApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EnvironmentController extends Controller
{
    private $envApi;

    public function __construct()
    {
        $this->envApi = new EnvironmentApi();
    }

    public function index()
    {
        session()->put('title', 'Data Aktivasi');
        $envi = $this->envApi->group_jenis(session('id_sekolah'));
        // dd($envi);
        $envi = $envi['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.admin.master.v_environment')->with(['template' => $template, 'envi' => $envi]);
    }

    public function update(Request $request)
    {
        $kode = $request->kode;
        $nama = $request->nama;
        $token = $request->token;
        $instruksi = $request->instruksi;
        // $data = [];
        foreach ($request['id'] as $i => $id) {
            $data = array(
                'id' => $id,
                'kode' => $kode[$i],
                'nama' => $nama[$i],
                'token' => $token[$i],
                'instruksi' => $instruksi[$i],
                'jenis' => $request['jenis'],
                'id_sekolah' => session('id_sekolah')
            );
            $update = $this->envApi->create(json_encode($data));
        }
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update_token(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'token' => $request['token'],
        );
        $update = $this->envApi->update_token(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }
}
