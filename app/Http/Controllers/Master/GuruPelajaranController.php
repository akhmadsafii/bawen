<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class GuruPelajaranController extends Controller
{
    private $gurupelajaranaApi;
    private $guruApi;
    private $mapelApi;
    private $jurusanApi;
    private $tahunApi;
    private $rombelApi;

    public function __construct()
    {
        $this->gurupelajaranaApi = new GuruPelajaranApi();
        $this->guruApi = new GuruApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->roomApi = new RoomApi();
        $this->rombelApi = new RombelApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];
        Session::put('title', 'Data Guru Pelajaran');
        $url = $this->gurupelajaranaApi->import(session('id_sekolah'));
        $guru = $this->guruApi->get_by_sekolah();  //gak kepake
        $guru = $guru['body']['data'];  //gak kepake
        $mapel = $this->mapelApi->get_by_sekolah(); //gak kepake
        $mapel = $mapel['body']['data'];  //gak kepake
        if ($_GET['rb'] != 'all') {
            $guru_pelajaran = $this->gurupelajaranaApi->by_rombel(Help::decode($_GET['rb']), Help::decode($_GET['th']));
        } else {
            $guru_pelajaran = $this->gurupelajaranaApi->get_by_sekolah(Help::decode($_GET['th']));
        }
        $result = $guru_pelajaran['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('guru', function ($row) {
                return '<b>' . $row['guru'] . '</b><p class="m-0">NIP. ' . $row['nip'] . '</p>';
            });
            $table->editColumn('tahun', function ($row) {
                return '<p class="m-0"> Tahun ajaran ' . $row['tahun_ajaran'] . '</p><p class="m-0">Semester. ' . $row['semester'] . '</p>';
            });
            $table->rawColumns(['action', 'guru', 'tahun']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.master.guru_pelajaran.v_data_pelajaran')->with([
            'url' => $url, 'kelas' => $guru_pelajaran, 'tahun' => $tahun, 'guru' => $guru,
            'mapel' => $mapel, 'jurusan' => $jurusan
        ]);
    }

    public function store(Request $request)
    {
        $data = array(
            "id_guru" => $request['id_guru'],
            "id_mapel" => $request['mapel'],
            'id_rombel' => Help::decode($request['rombel']),
            'id_ta_sm' =>  Help::decode($request['tahun_ajaran']),
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->gurupelajaranaApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $post  = $this->gurupelajaranaApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            "id" => $request['id'],
            "id_guru" => $request['id_guru'],
            "id_mapel" => $request['mapel'],
            'id_rombel' => Help::decode($request['rombel']),
            'id_ta_sm' =>  Help::decode($request['tahun_ajaran']),
            'id_sekolah' => session('id_sekolah'),
        );
        $result  = $this->gurupelajaranaApi->update_info(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->gurupelajaranaApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            // $guru_pelajaran = $this->data_guru_pelajaran($request['id_rombel']);
            return response()->json([
                // 'guru' => $guru_pelajaran,
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_mapel(Request $request)
    {
        $id_mapel = $request['id_mapel'];
        $data = $this->gurupelajaranaApi->get_by_mapel($id_mapel);
        $result = $data['body']['data'];
        return response()->json($result);
    }

    public function load_mapel_select(Request $request)
    {
        $id_guru = $request['id_guru'];
        $id_mapel = $request['id_mapel'];
        $post  = $this->gurupelajaranaApi->get_by_mapel($id_mapel);
        $result = $post['body']['data'];
        // dd($result);
        $output = '';
        foreach ($result as $brand) {
            $brand_id = $brand['id_guru'];
            $brand_name = $brand['guru'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id_guru) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->gurupelajaranaApi->upload_excel(json_encode($data), Help::decode($request['th']));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function getRombel(Request $request)
    {
        // dd($request);
        $rombel = $this->rombelApi->get_by_kelas($request['id']);
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        $html = '<div class="table-responsive">
        <table class="table table-hover"><tr><th>No</th>
                <th>Rombel</th>
                <th>Detail</th>
            </tr>';
        if (!empty($rombel)) {
            $no = 1;
            foreach ($rombel as $rmb) {
                $html .= ' <tr>
                <td>' . $no++ . '</td>
                <td>' . $rmb['nama'] . '</td>
                <td>
                    <a href="#">Detail</a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="3" class="text-center">Data belum tersedia</td></tr>';
        }
        $html .= '</table></div>';

        return $html;
    }
}
