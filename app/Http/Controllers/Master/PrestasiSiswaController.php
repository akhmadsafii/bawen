<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\PrestasiSiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use XcS\XcTools;

class PrestasiSiswaController extends Controller
{
    private $prestasiApi;

    public function __construct()
    {
        $this->prestasiApi = new PrestasiSiswaApi();
    }

    // public function index()
    // {
    //     $kategori = $this->katLombaApi->get_by_sekolah();
    //     // dd($kategori);
    //     $kategori = $kategori['body']['data'];
    //     return view('content.admin.master.v_lomba')->with(['kategori' => $kategori]);
    // }


    // public function data_siswa(Request $request)
    // {
    //     // dd($request);
    //     $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), $request['id_jurusan'], $request['id_kelas'], $request['id_rombel']);
    //     // dd($siswa);
    //     $siswa = $siswa['body']['data'];
    //     session()->put('request_temporary', [
    //         'id_jurusan' => $request['id_jurusan'],
    //         'id_kelas' => $request['id_kelas'],
    //         'id_rombel' => $request['id_rombel'],
    //     ]);
    //    $prestasi = $this->search_mutasi();

    //     return response()->json($prestasi);
    // }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kategori_lomba' => $request['id_kategori'],
            'nama_lomba' => $request['nama'],
            'tahun' => $request['tahun'],
            'nama_peserta' => $request['nama_peserta'],
            'kelas' => $request['kelas'],
            'peringkat_kab' => $request['peringkat_kab'],
            'peringkat_prov' => $request['peringkat_prov'],
            'peringkat_nas' => $request['peringkat_nas'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->prestasiApi->create(json_encode($data));
        $prestasi = $this->data_prestasi($request['id_kategori']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $prestasi
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $prestasi
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
                'id' => $request['id'],
                'id_kategori_lomba' => $request['id_kategori'],
                'nama_lomba' => $request['nama'],
                'tahun' => $request['tahun'],
                'nama_peserta' => $request['nama_peserta'],
                'kelas' => $request['kelas'],
                'peringkat_kab' => $request['peringkat_kab'],
                'peringkat_prov' => $request['peringkat_prov'],
                'peringkat_nas' => $request['peringkat_nas'],
                'keterangan' => $request['keterangan'],
                'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->prestasiApi->update_info(json_encode($data));
        $prestasi = $this->data_prestasi($request['id_kategori']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $prestasi
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $prestasi
            ]);
        }
    }

    public function detail(Request $request)
    {
        $detail = $this->prestasiApi->get_by_id($request['id']);
        $detail = $detail['body']['data'];
        // dd($detail);
        return response()->json($detail);
    }

    public function trash(Request $request)
    {
        $detail = $this->prestasiApi->get_by_id($request['id']);
        $id_kategori = $detail['body']['data']['id_kategori_lomba'];
        $delete = $this->prestasiApi->soft_delete($request['id']);
        $prestasi = $this->data_prestasi($id_kategori);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $prestasi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $prestasi
            ]);
        }
    }

    // public function import(Request $request)
    // {
    //     $files = $request->file('image');
    //     $namaFile = $files->getClientOriginalName();
    //     $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
    //     $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
    //     $basePath = "file/import/";
    //     Help::check_and_make_dir($basePath);
    //     $files->move($basePath, $imageName);
    //     $path = $basePath . $imageName;
    //     $data['path'] = $path;
    //     $result = $this->prestasiApi->upload_excel(json_encode($data));
    //     dd($result);
    //     $message = $result['body']['message'];
    //     if ($result['code'] == 200) {
    //         return response()->json([
    //             'success' => $message,
    //             'icon'  => 'success',
    //             'status' => 'berhasil'
    //         ]);
    //         File::delete($path);
    //     } else {
    //         return response()->json([
    //             'success' => $message[0]['error'],
    //             'icon'  => 'error',
    //             'status' => 'gagal'
    //         ]);
    //     }
    // }

    public function sample()
    {
        $download = $this->prestasiApi->import();
        return redirect($download);
    }

    private function data_prestasi($id)
    {
        $prestasi = $this->prestasiApi->by_kategori($id);
        $prestasi =$prestasi['body']['data'];
        $html = '';
        if (!empty($prestasi)) {
            $nomer = 1;
            foreach ($prestasi as $pt) {
                $html .= '
                <tr>
                    <td class="text-center">'.$nomer++.'</td>
                    <td class="text-center">'.$pt['nama_lomba'].'</td>
                    <td class="text-center">'.$pt['nama_peserta'].'</td>
                    <td class="text-center">'.$pt['kelas'].'</td>
                    <td class="text-center">
                        <a href="javascript:void(0)" data-id="'.$pt['id'].'"
                            class="btn btn-info btn-sm editPrestasi"><i
                                class="fas fa-pencil-alt"></i></a>
                        <button class="btn btn-danger btn-sm"
                            onclick="deletePrestasi('.$pt['id'].','.$id.')"><i
                                class="fas fa-trash"></i></button>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '
            <tr>
            <td colspan="5" class="text-center">Untuk saat ini Data Prestasi Siswa kosong</td>
            </tr>
            ';
        }
        return $html;
    }
}
