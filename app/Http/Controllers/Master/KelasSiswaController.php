<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class KelasSiswaController extends Controller
{
    private $rombelApi;
    private $kelassiswaApi;
    private $jurusanApi;
    private $kelasApi;
    private $tahunajaranApi;
    private $siswaApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
        $this->tahunajaranApi = new TahunAjaranApi();
        $this->siswaApi = new SiswaApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Siswa');
        $url = $this->kelassiswaApi->import(session('id_sekolah'));
        $tahun = $this->tahunajaranApi->get_tahun_ajaran();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        $jurusan = $this->jurusanApi->menu();
        $jurusan = $jurusan['body']['data'];

        if ($_GET['based'] == 'siswa') {
            $siswa = $this->siswaApi->not_kelas_siswa();
            // dd($siswa);
        } else {
            $siswa = $this->kelassiswaApi->get_by_rombel(Help::decode($_GET['rombel']), $_GET['tahun']);
        }
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($siswa)
                ->addColumn('action', function ($data) {
                    if ($_GET['based'] == 'siswa') {
                        $id = $data['id'];
                    } else {
                        $id = $data['id_siswa'];
                    }
                    return ' <a href="'.route('cetak_pdf-siswa', ['k' => (new \App\Helpers\Help())->encode($id),'key' => str_slug($data['nama'])]).'"
                    target="_blank" class="btn btn-purple btn-sm"><i class="fas fa-info-circle"></i></a>';
                });
            $table->editColumn('check', function ($row) {
                return '<input type="checkbox" id="manual_entry_' . $row['id'] . '" name="siswa[]"
                class="manual_entry_cb" value="' . $row['id'] . '" />';
            });
            $table->editColumn('siswa', function ($row) {
                return '<b>Nama : ' . $row['nama'] . '</b><br><small><b>NIS/NISN. ' . $row['nis'] . '/' . $row['nisn'] . '</b></small>';
            });
            $table->editColumn('ttl', function ($row) {
                return $row['tempat_lahir'] . ", " . Help::getTanggal($row['tgl_lahir']);
            });
            $table->rawColumns(['action', 'check', 'siswa', 'ttl']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.master.kelas_siswa.v_data_kelas_siswa_new')->with(['jurusan' => $jurusan, "tahun" => $tahun, 'url' => $url]);
    }



    public function store(Request $request)
    {
        // dd($request);
        if ($request->file('logo') != null) {
            $image = $request->file('logo');
            $mimetype = $image->getMimeType();
            $image_base64 = base64_encode(file_get_contents($request->file('logo')));
            $data['logo'] = "data:" . $mimetype . ";base64," . $image_base64;
        }
        $data =
            [
                'nama' => $request->nama,
                'npsn' => $request->npsn,
                'jenjang' => $request->jenjang,
                'status_sekolah' => $request->status_sekolah,
                'sk_pendirian' => $request->sk_pendirian,
                'tgl_sk_pendirian' => $request->tgl_sk_pendirian,
                'email' => $request->email,
                'fax' => $request->fax,
                'website' => $request->website,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kode_pos' => $request->kode_pos,
                'status' => 1
            ];

        $result = $this->sekolahApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit($id)
    {
        // dd('ping');
        $post  = $this->sekolahApi->get_by_id($id);
        $result = $post['body']['data'];
        // dd($post);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        foreach (json_decode($request['id_siswa']) as $key) {
            $rombel = $this->rombelApi->get_by_id(Help::decode($request['rombel']));
            $rombel = $rombel['body']['data'];
            if ($request['based'] == 'kelas') {
                $k_siswa = $this->kelassiswaApi->get_by_id($key);
                $kelas_siswa = $k_siswa['body']['data'];
                $data_update = array(
                    "id_siswa" =>  $kelas_siswa['id_siswa'],
                    "id_rombel" => $rombel['id'],
                    "id_kelas" => $rombel['id_kelas'],
                    "tahun" => $request['tahun'],
                    "id_jurusan" => $rombel['id_jurusan'],
                    "id_sekolah" => $kelas_siswa['id_sekolah'],
                );
                $result = $this->kelassiswaApi->create(json_encode($data_update));
            } else {
                $data_add = array(
                    'id_siswa' => $key,
                    'id_rombel' => $rombel['id'],
                    'id_kelas' => $rombel['id_kelas'],
                    'id_jurusan' => $rombel['id_jurusan'],
                    'tahun' => $request['tahun'],
                    'id_sekolah' => session('id_sekolah'),
                );
                $result = $this->kelassiswaApi->create(json_encode($data_add));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        foreach (json_decode($request['id_siswa']) as $key) {
            // dd
            if ($request['based'] == 'kelas') {
                $delete = $this->kelassiswaApi->soft_delete($key);
            } else {
                $delete = $this->siswaApi->soft_delete($key);
            }
        }
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function get_by_rombel(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $post  = $this->kelassiswaApi->get_by_rombel($id_rombel, session('tahun'));
        $result = $post['body']['data'];
        // dd($result);
        return response()->json($result);
    }

    public function load_siswa(Request $request)
    {
        // dd($request);
        if ($request['asal_data'] == 'siswa') {
            $siswa = $this->siswaApi->not_kelas_siswa();
        } else {
            $id_jurusan = null;
            if ($request['id_jurusan'] != null) {
                $id_jurusan = Help::decode($request['id_jurusan']);
            }
            $data = array(
                'tahun' => $request['id_tahun'],
                'id_jurusan' => $id_jurusan,
                'id_kelas' => $request['id_kelas'],
                'id_rombel' => $request['id_rombel'],
            );
            $siswa = $this->kelassiswaApi->get_search_data(json_encode($data));
        }
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $html = '';
        if (!empty($siswa)) {
            $no = 1;
            foreach ($siswa as $sw) {
                $html .= '
                <tr>
                    <td><input type="checkbox" id="manual_entry_' . $sw['id'] . '" name="siswa[]"
                            class="manual_entry_cb" value="' . $sw['id'] . '" /></td>
                    <td>' . $no++ . '</td>
                    <td>' . strtoupper($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['nisn'] . '</td>
                    <td><button data-toggle="collapse" data-target="#demo' . $sw['id'] . '"
                            class="btn btn-sm btn-success accordion-toggle"><i
                                class="fas fa-info-circle"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse" id="demo' . $sw['id'] . '">
                            <table class="table table-striped">
                                <tr>

                                </tr>
                                <tr>
                                    <th colspan="3" class="text-center" rowspan="6"><img
                                            src="' . $sw['file'] . '"
                                            alt="" width="200px"></th>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Nama</th>
                                    <td class="vertical-middle">' . strtoupper($sw['nama']) . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">NIK</th>
                                    <td class="vertical-middle">' . $sw['nik'] . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">NISN</th>
                                    <td class="vertical-middle">' . $sw['nisn'] . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">NIS</th>
                                    <td class="vertical-middle">' . $sw['nis'] . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Jenkel</th>
                                    <td class="vertical-middle">' . $sw['jenkel'] . '</td>
                                </tr>

                                <tr>
                                    <th class="vertical-middle">Telepon</th>
                                    <td class="vertical-middle">' . $sw['telepon'] . '</td>
                                    <td class="vertical-middle"></td>
                                    <th class="vertical-middle">Email</th>
                                    <td class="vertical-middle">' . $sw['email'] . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Tempat Lahir</th>
                                    <td class="vertical-middle">' . $sw['tempat_lahir'] . '</td>
                                    <td class="vertical-middle"></td>
                                    <th class="vertical-middle">Tgl Lahir</th>
                                    <td class="vertical-middle">' . $sw['tgl_lahir'] . '</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Agama</th>
                                    <td class="vertical-middle">' . $sw['agama'] . '</td>
                                    <td class="vertical-middle"></td>
                                    <th class="vertical-middle">Dibuat</th>
                                    <td class="vertical-middle">42342</td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Alamat</th>
                                    <td class="vertical-middle" colspan="5">Agung</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                ';
            }
            $html .= '
            <tr><td colspan="5"></td><td><button class="btn btn-info" data-toggle="collapse" data-target="#dataMove"><i class="fas fa-compress-arrows-alt"></i> Pindahkan</button></td></tr>
            <tr>
                <td colspan="6">
                    <div class="accordian-body collapse" id="dataMove">
                        <table class="table table-striped">
                            <tr>
                                <th width="100" class="vertical-middle">Jurusan</th>
                                <td><select name="jurusan" id="jurusan" class="form-control">
                                <option>Pilih Jurusan...</option>';
            $jurusan = $this->jurusanApi->get_by_sekolah();
            $jurusan = $jurusan['body']['data'];
            foreach ($jurusan as $jr) {
                // dd($jr);
                $html .= '<option value="' . Help::encode($jr['id']) . '">' . $jr['nama'] . '</option>';
            }
            $html .= '</select></td>
                                <th width="100" class="vertical-middle">Kelas</th>
                                <td>
                                <select name="kelas" id="kelas" class="form-control"><option value="">--- Pilih Kelas ---</option>';

            $kelas = $this->kelasApi->get_jurusan_null();
            // dd($kelas);
            $kelas = $kelas['body']['data'];
            foreach ($kelas as $kls) {
                $html .= '<option value="' . $kls['id'] . '">' . $kls['nama'] . '</option>';
            }
            $html .= '</select>
                                </td>
                                <th width="100" class="vertical-middle">Rombel</th>
                                <td>
                                    <select name="rombel" id="rombel" class="form-control" disabled>
                                        <option value="">--- Pilih Rombel ---</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="8" class="text-center"><button type="submit" class="btn btn-info btn-block" id="btnStart">Mulai Pindahkan</button></th>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            ';
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }

        if ($request['asal_data'] == 'siswa') {
            $data = array(
                'aksi' => 'siswa',
                'html' => $html
            );
        } else {
            $data = array(
                'aksi' => 'kelas',
                'tahun' => $request['id_tahun'],
                'id_jurusan' => $id_jurusan,
                'id_kelas' => $request['id_kelas'],
                'id_rombel' => $request['id_rombel'],
                'html' => $html
            );
        }

        return response()->json($data);
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->kelassiswaApi->upload_excel(json_encode($data), session('tahun'));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }


    public function export()
    {
        return $this->sekolahApi->export();
    }

    public function download()
    {
        $sekolah = $this->sekolahApi->get_all();
        $result = $sekolah['body']['data'];
        $pdf = PDF::loadView('pdf.invoice', $result);
        return $pdf->download('invoice.pdf');
    }

    public function load_kelas_siswa(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $id_kelas_siswa = $request['id_kelas_siswa'];
        $kelas_siswa  = $this->kelassiswaApi->get_by_rombel($id_rombel, session('tahun'));
        // dd($kelas_siswa);
        $kelas_siswa = $kelas_siswa['body']['data'];
        $output = '';
        foreach ($kelas_siswa as $ks) {
            $output .= '<option value="' . $ks['id'] . '" ' . (($ks['id'] == $id_kelas_siswa) ? 'selected="selected"' : "") . '>' . $ks['nama'] . ' NISN ' . $ks['nisn'] . '</option>';
        }
        return $output;
    }
}
