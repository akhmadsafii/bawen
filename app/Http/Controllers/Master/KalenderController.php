<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\User\ProfileApi;
use App\ApiService\Master\KalenderApi;
use App\Helpers\Help;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class KalenderController extends Controller
{
    private $kalenderApi;
    private $profileApi;

    public function __construct()
    {
        $this->kalenderApi = new KalenderApi();
        $this->profileApi = new ProfileApi();
    }

    public function index()
    {
        // dd(session()->all());
        Session::put('title', 'Kalender Akademik');
        if (request()->ajax()) {
            $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
            $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
            $kalender = $this->kalenderApi->get_acara($start, $end, session('id_sekolah'));
            $data = $kalender['body']['data'];
            return Response::json($data);
        }
        if(session("role") == "admin"){
            return view('content.admin.master.v_kalender');
        }else{
            return view('content.master.kalender.v_kalendar')->with(['template' => session('template')]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        $insertArr = [
            'title' => $request->title,
            'start' => date('Y-m-d', strtotime($request->start)),
            'end' => date('Y-m-d', strtotime($request->end)),
            'id_admin' => $profile['id'],
            'id_sekolah' => $profile['id_sekolah'],
        ];
        // dd($insertArr);
        $result = $this->kalenderApi->create(json_encode($insertArr));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderApi->get_by_id($request->id);
        $kalender = $kalender['body']['data'];

        $updateArr = [
            'id' => $request->id,
            'title' => $request->title,
            'start' => $request->start,
            'end' => $request->end,
            'id_admin' => $kalender['id_admin'],
            'id_sekolah' => $kalender['id_sekolah'],
        ];
        $result  = $this->kalenderApi->update_info(json_encode($updateArr));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit_klik(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderApi->get_by_id($request->id);
        $kalender = $kalender['body']['data'];

        $updateArr = [
            'id' => $request->id,
            'title' => $request->title,
            'start' => $request->start,
            'end' => $request->end,
            'id_admin' => $kalender['id_admin'],
            'id_sekolah' => $kalender['id_sekolah'],
        ];
        $result  = $this->kalenderApi->update_info(json_encode($updateArr));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->kalenderApi->soft_delete($request->id);
        if ($delete['code'] == 200) {
            $status = "berhasil";
            $message = $delete['body']['message'];
            $icon = 'success';
        } else {
            $status = "gagal";
            $message =  $delete['body']['message'];
            $icon = "error";
        }
        // dd($delete);
        return Response::json([
            'status' => $status,
            'message' => $message,
            'icon' => $icon
        ]);
    }

    public function data_kalender()
    {
        $kalender = $this->kalenderApi->get_sekolah(session('id_sekolah'));
        // dd($kalender);
        $result = $kalender['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<button type="button" name="delete" class="btn btn-danger btn-sm del-' . $data['id'] . '" onclick="deletes(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
            $table->editColumn('tanggal', function ($row) {
                return Help::getTanggal($row['start'])." - ".Help::getTanggal($row['end']);
            });
        $table->rawColumns(['action','tanggal']);
        $table->addIndexColumn();
        return $table->make(true);
    }
}
