<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JadwalApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class JadwalPelajaranController extends Controller
{
    private $kelasApi;
    private $mapelApi;
    private $rombelApi;
    private $jadwalApi;
    private $guruApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->kelasApi = new KelasApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->rombelApi = new RombelApi();
        $this->jadwalApi = new JadwalApi();
        $this->guruApi = new GuruPelajaranApi();
    }
    public function index(Request $request)
    {
        Session::put('title', 'Data Jadwal Pelajaran');
        $url = $this->jadwalApi->import(session('id_sekolah'));
        $mapel = $this->mapelApi->get_by_sekolah();

        if ($mapel['code'] != 200) {
            $pesan = array(
                'message' => $mapel['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $mapel = $mapel['body']['data'];
        $routes = "master-jadwal";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $rombel = (isset($_GET["rombel"])) ? $_GET["rombel"] : "";
        $id_jurusan = (isset($_GET["jurusan"])) ? $_GET["jurusan"] : "";
        $id_kelas = (isset($_GET["kelas"])) ? $_GET["kelas"] : "";
        $sort = (isset($_GET["sort"])) ? $_GET["sort"] : "ASC";
        $perPage = (isset($_GET["per_page"])) ? $_GET["per_page"] : "10";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->jadwalApi->get_sekolah($search, $status, $sort, $perPage, $page, $id_jurusan, $id_kelas, $rombel);
        // dd($result);
        $data = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pages = Utils::getPerPage();
        $sorts = Utils::getSort();
        $statuses = Utils::getStatus();
        $pagination = Utils::createLinksMetronic($meta, $routes, $search, $status, $sort);
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $kelas = $this->kelasApi->get_jurusan_null();
        $kelas = $kelas['body']['data'];
        return view('content.admin.master.v_jadwal_custom', compact('mapel', 'jurusan', 'url', 'kelas', 'data', 'pagination', 'pages', 'sorts', 'statuses', 'routes', 'search', 'status', 'sort', 'perPage', 'page','rombel', 'id_jurusan'));
    }

    public function get_datatable(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $kelas = $this->jadwalApi->get_by_rombel($id_rombel, session('id_tahun_ajar'));
        // dd($kelas);

        if ($id_rombel == 0) {
            $result = [];
        }else{
            $result = $kelas['body']['data'];
        }
        if ($result == null) {
            $result = [];
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-xs edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-xs" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function store(Request $request)
    {
        $rombel = $this->rombelApi->get_by_id($request['id_rombel']);
        $rombel = $rombel['body']['data'];
        // dd($rombel);
        $data_insert = array(
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_jurusan' => $rombel['id_jurusan'],
            'id_kelas' => $rombel['id_kelas'],
            'id_rombel' => $request['id_rombel'],
            'id_mapel' => $request['id_mapel'],
            'id_guru' => $request['id_guru'],
            'jam' => $request['jam'],
            'ruang' => $request['ruang'],
            'hari' => $request['hari'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jadwalApi->full_create(json_encode($data_insert));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }

    }

    public function edit(Request $request)
    {
        // dd($request);
        $id = $request['id_jadwal'];
        $jadwal = $this->jadwalApi->get_by_id($id);
        $jadwal = $jadwal['body']['data'];
        return response()->json($jadwal);
    }

    public function update(Request $request)
    {
        // dd($request);
        $jadwal = $this->jadwalApi->get_by_id($request['id']);
        $jadwal = $jadwal['body']['data'];
        $update = array(
            'id' => $request['id'],
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_jurusan' => $jadwal['id_jurusan'],
            'id_kelas' => $jadwal['id_kelas'],
            'id_rombel' => $jadwal['id_rombel'],
            'id_mapel' => $request['id_mapel'],
            'id_guru' => $request['id_guru'],
            'jam' => $request['jam'],
            'ruang' => $request['ruang'],
            'hari' => $request['hari'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jadwalApi->update_all(json_encode($update));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }

    }

    public function trash(Request $request)
    {
        if(session('demo')){
            return response()->json([
                'success' => "success",
                'message' => 'Jadwl pelajaran berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }

        $delete = $this->jadwalApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }

    public function delete($id)
    {
        $delete = $this->jadwalApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $mapel = $this->jadwalApi->all_trash();
        $result = $mapel['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->jadwalApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jadwalApi->upload_excel(json_encode($data), session('id_tahun_ajar'));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
