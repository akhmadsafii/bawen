<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\KompetensiDasarApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiDasarApi;
use App\ApiService\Raport\NilaiKDKetrampilanApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class NilaiKetrampilanController extends Controller
{
    private $gurupelajaranApi;
    private $dasarApi;
    private $intiApi;
    private $kelasSiswaApi;
    private $nilaiDasarApi;
    private $kdKetrampilanApi;
    private $hashids;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->dasarApi = new KompetensiDasarApi();
        $this->intiApi = new KompetensiApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->nilaiDasarApi = new NilaiDasarApi();
        $this->kdKetrampilanApi = new NilaiKDKetrampilanApi();
        $this->hashids = new Hashids();
    }

    public function index($id_code)
    {
        $id = $this->hashids->decode($id_code)[0];
        $guru = $this->gurupelajaranApi->get_by_id($id);
        $guru = $guru['body']['data'];
        $dasar = $this->dasarApi->by_mapel_kelas_jenis($guru['id_mapel'], $guru['id_kelas'], "keterampilan");
        // dd($dasar);
        $dasar = $dasar['body']['data'];
        $inti = $this->intiApi->by_mapel_kelas_jenis($guru['id_mapel'], $guru['id_kelas'], "keterampilan");
        // dd($inti);
        $inti = $inti['body']['data'];

        return view('content.raport.guru.mapel.v_nilai_ketrampilan')->with(['template' => session('template'), 'guru' => $guru, "dasar" => $dasar, 'inti' => $inti]);
    }

    // public function index($id_code)
    // {
    //     $id = $this->hashids->decode($id_code)[0];
    //     $guru = $this->gurupelajaranApi->get_by_id($id);
    //     $guru = $guru['body']['data'];
    //     // dd($guru);
    //     // $id_rombel = $guru['id_rombel'];
    //     $siswa = $this->kdKetrampilanApi->get_by_rombel($guru['id_guru'], $guru['id_mapel'], $guru['id_kelas'], $guru['id_rombel'], session('id_tahun_ajar'));
    //     // dd($siswa);
    //     // dd($siswa);
    //     $siswa = $siswa['body']['data'];
    //     if (empty($siswa)) {
    //         $kelas_siswa = $this->kelasSiswaApi->get_by_rombel($id_rombel, session('tahun'));
    //         // dd($kelas_siswa);
    //         $siswa = $kelas_siswa['body']['data'];
    //         $form = "add";
    //     } else {
    //         $form = "edit";
    //     }
    //     // $siswa['id_guru_pelajaran'] = $guru['id'];
    //     return view('content.raport.guru.mapel.v_nilai_pengetahuan_revisi')->with(['template' => session('template'), 'guru' => $guru, 'siswa' => $siswa, 'form' => $form, 'config' => $config]);
    // }

    public function edit($id)
    {
        $q = $this->db->query("SELECT *, 'edit' AS mode FROM t_mapel_kd WHERE id = '$id'")->row_array();

        $d = array();
        $d['status'] = "ok";

        if (empty($q)) {
            $d['data']['id'] = "";
            $d['data']['mode'] = "add";
            $d['data']['id_guru'] = "";
            $d['data']['id_mapel'] = "";
            $d['data']['tingkat'] = "";
            $d['data']['no_kd'] = "";
            $d['data']['jenis'] = "";
            $d['data']['jenis'] = "";
            $d['data']['nama_kd'] = "";
        } else {
            $d['data'] = $q;
        }
        j($d);
    }

    public function store(Request $request)
    {
        $id_guru = $request['id_guru_mapel'];
        $id_kd = $request['id_mapel_kd'];
        $id_kelas = $request['id_kelas'];
        $id_rombel = $request['id_rombel'];
        $id_mapel = $request['id_mapel'];
        $id_tahun_ajar = session('id_tahun_ajar');
        $i = 0;
        $data = [];
        foreach ($request['nilai'] as $s) {
            $data[] = array(
                    'id' => $request['id_siswa'][$i],
                    'id_ta_sm' => $id_tahun_ajar,
                    'id_kelas_siswa' => $request['id_siswa'][$i],
                    'id_kd' => $id_kd,
                    'id_mapel' => $id_mapel,
                    'id_kelas' => $id_kelas,
                    'id_rombel' => $id_rombel,
                    'id_guru' => $id_guru,
                    'nilai' => $s,
                    'id_sekolah' => session('id_sekolah'),
                );
            $i++;
        }
        foreach ($data as $dt) {
            $data_proccess = array(
                'id_kelas_siswa' => $dt['id_kelas_siswa'],
                'id_ta_sm' => $dt['id_ta_sm'],
                'id_kd' => $dt['id_kd'],
                'id_mapel' => $dt['id_mapel'],
                'id_kelas' => $dt['id_kelas'],
                'id_rombel' => $dt['id_rombel'],
                'id_guru' => $dt['id_guru'],
                'nilai' => $dt['nilai'],
                'id_sekolah' => $dt['id_sekolah'],
            );
            if ($data_proccess['nilai'] != null) {
                $result = $this->kdKetrampilanApi->create(json_encode($data_proccess));
            }
        }
        if ($result['code'] == 200) {
            $d['status'] = "berhasil";
            $d['icon'] = "success";
            $d['message'] = $result['body']['message'];
        } else {
            $d['status'] = "gagal";
            $d['icon'] = "danger";
            $d['message'] = $result['body']['message'];
        }
        return response()->json($d);
    }
}
