<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\EkstrakurikulerApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigApi;
use App\ApiService\Raport\ConfigTemplateApi;
use App\ApiService\Raport\LihatPtsApi;
use App\ApiService\Raport\LihatRaportApi;
use App\ApiService\Raport\NilaiCatatanApi;
use App\ApiService\Raport\NilaiPrestasiApi;
use App\ApiService\Raport\PredikatApi;
use App\ApiService\Raport\TemplateSampulApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Http\Request;

class CetakRaportController extends Controller
{
    private $kelasSiswaApi;
    private $tahunAjarApi;
    private $prestasiApi;
    private $catatanApi;
    private $sampulApi;
    private $sekolahApi;
    private $configApi;
    private $lihatApi;
    private $lihatPtsApi;
    private $templateApi;
    private $predikatApi;
    private $ekstraApi;
    private $rombelApi;

    public function __construct()
    {
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->tahunAjarApi = new TahunAjaranApi();
        $this->prestasiApi = new NilaiPrestasiApi();
        $this->catatanApi = new NilaiCatatanApi();
        $this->sampulApi = new TemplateSampulApi();
        $this->sekolahApi = new SekolahApi();
        $this->configApi = new ConfigApi();
        $this->lihatApi = new LihatRaportApi();
        $this->lihatPtsApi = new LihatPtsApi();
        $this->templateApi = new ConfigTemplateApi();
        $this->predikatApi = new PredikatApi();
        $this->ekstraApi = new EkstrakurikulerApi();
        $this->rombelApi = new RombelApi();
    }

    public function index()
    {
        // dd(session()->all());
        session()->put('title', 'Cetak Raport');
        $year = $this->tahunAjarApi->get_tahun_ajaran();
        $year = $year['body']['data'];
        $tahun = $this->tahunAjarApi->get_by_semester($_GET['tahun']);
        $tahun = $tahun['body']['data'];
        if (session('role') == 'ortu') {
            $rombel = $this->kelasSiswaApi->get_by_id(session('id_kelas_siswa'));
            // dd($rombel);
            $rombel = $rombel['body']['data'];
            session()->put('id_jurusan', $rombel['id_jurusan']);
        } else {
            $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
            $rombel = $rombel['body']['data'];
            session()->put('id_jurusan', $rombel['id_jurusan']);
        }

        $template = $this->templateApi->by_tahun_jurusan(session('id_jurusan'), session('id_tahun_ajar'));
        $template = $template['body']['data'];
        if ($template == null) {
            $pesan = array(
                'message' => "Template Raport untuk saat ini belum tersedia",
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        if (session("role") == "siswa" || session('role') == 'ortu') {
            return view('content.raport.guru.cetak_raport.v_cetak_raport_siswa')->with(['template' => 'default', 'tahun' => $tahun, 'temp' => $template, 'year' => $year]);
        } else {
            $siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
            // dd($siswa);
            $siswa = $siswa['body']['data'];
            return view('content.raport.guru.cetak_raport.v_cetak_raport')->with(['template' => session('template'), 'siswa' => $siswa, 'temp' => $template, 'year' => $year, 'tahun' => $tahun]);
        }
    }

    public function cetak_sampul()
    {
        $ekstra = $this->ekstraApi->get_by_sekolah();
        // dd($ekstra);
        $ekstra = $ekstra['body']['data'];
        // dd(session()->all());
        $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
        $rombel = $rombel['body']['data'];
        session()->put('id_jurusan', $rombel['id_jurusan']);
        $template = $this->templateApi->by_tahun_jurusan(session('id_jurusan'), Help::decode($_GET['ta_sm']));
        // dd($template);
        $template = $template['body']['data'];
        if ($template == null) {
            $pesan = array(
                'message' => "Template Raport dengan tahun ajar ini belum tersedia",
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $predikat = $this->predikatApi->get_by_sekolah();
        $predikat = $predikat['body']['data'];
        // dd($template);
        if (session('role') == 'siswa') {
            $kop = $this->sampulApi->get_by_S4(session('id_kelas_siswa'));
            if ($template['jenis'] == 'pas') {
                if ($template['template'] == 'k16') {
                    $lihat = $this->lihatApi->k16_auth_siswa(Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                } elseif ($template['template'] == 'manual') {
                    $lihat = $this->lihatApi->manual_auth_siswa(Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                } else {
                    $lihat = $this->lihatApi->kd_auth_siswa(Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                    if ($lihat['code'] != 200) {
                        $pesan = array(
                            'message' => 'Pastikan guru telah mengisi nilai kd',
                            'icon' => 'error',
                            'status' => 'gagal'
                        );
                        return redirect()->back()->with('error_api', $pesan);
                    }
                }
                // dd($lihat);
            } else {
                if ($template['template'] == 'sd') {
                    $lihat = $this->lihatPtsApi->manual_auth_siswa(Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                } elseif ($template['template'] == 'smp' || $template['template'] == 'smk' || $template['template'] == 'k16') {
                    $lihat = $this->lihatPtsApi->kd_auth_siswa_v2(Help::decode($_GET['ta_sm']));
                } else {
                    $lihat = $this->lihatPtsApi->kd_auth_siswa_v1(Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                }
            }
            // dd($lihat);
        } else {
            if (session('role') == 'ortu') {
                $ks = session('id_kelas_siswa');
            } else {
                $ks = Help::decode($_GET['ks']);
            }

            $kop = $this->sampulApi->get_by_S4($ks);
            if ($template['jenis'] == 'pas') {
                if ($template['template'] == 'k16') {
                    $lihat = $this->lihatApi->k16_kelas_siswa($ks, Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                } elseif ($template['template'] == 'manual') {
                    $lihat = $this->lihatApi->manual_kelas_siswa($ks, Help::decode($_GET['ta_sm']));
                } else {
                    $lihat = $this->lihatApi->kd_kelas_siswa($ks, Help::decode($_GET['ta_sm']));
                    if ($lihat['code'] != 200) {
                        $pesan = array(
                            'message' => 'Pastikan guru telah mengisi nilai kd',
                            'icon' => 'error',
                            'status' => 'gagal'
                        );
                        return redirect()->back()->with('error_api', $pesan);
                    }
                }
            } else {
                if ($template['template'] == 'k16') {
                    $lihat = $this->lihatPtsApi->k16_kelas_siswa($ks, Help::decode($_GET['ta_sm']));
                    // dd($lihat);
                } elseif ($template['template'] == 'manual' || $template['template'] == 'sd') {
                    $lihat = $this->lihatPtsApi->manual_kelas_siswa($ks, Help::decode($_GET['ta_sm']));
                } elseif ($template['template'] == 'smp' || $template['template'] == 'smk') {
                    $lihat = $this->lihatPtsApi->kd_kelas_siswa_v2($ks, Help::decode($_GET['ta_sm']));
                } else {
                    $lihat = $this->lihatPtsApi->kd_kelas_siswa_v1($ks, Help::decode($_GET['ta_sm']));
                }
            }
        }
        // dd($lihat);
        $lihat = $lihat['body']['data'];
        if ($lihat['nilai_absensi'] == null) {
            $pesan = array(
                'message' => 'Harap mengisi absensi di siswa di wali kelas dengan tahun ajar ini',
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        // sd  pas
        $view = 'content.raport.guru.cetak_raport.' . $template['template'] . '.v_print_' . $template['jenis'];

        session()->put('title', str_replace(' ', '-', $lihat['nama']));
        $kop = $kop['body']['data'];
        // return view($view)->with([
        //     'lihat' => $lihat, 'kop' => $kop, 'predikat' => $predikat, 'ekstra' => $ekstra
        // ]);
        $pdf = PDF::loadview($view, [
            'lihat' => $lihat, 'kop' => $kop, 'predikat' => $predikat, 'ekstra' => $ekstra
        ]);
        return $pdf->stream();
    }

    public function cetak_sampul_awal()
    {
        // dd("ping");
        $sampul = $this->sampulApi->by_tahun_ajar(Help::decode($_GET['ta_sm']));
        // dd($sampul);
        $sampul = $sampul['body']['data'];
        $config = $this->configApi->by_tahun_aktif(Help::decode($_GET['ta_sm']));
        $config = $config['body']['data'];
        // dd($config);
        if (empty($sampul)) {
            $pesan = array(
                'message' => "Sampul Raport dengan tahun ajar ini belum tersedia",
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        // dd($sampul);
        if (session("role") == "siswa") {
            $siswa = $this->sampulApi->get_S2();
        } else {
            $siswa = $this->kelasSiswaApi->get_by_id(Help::decode($_GET['ks']));
        }
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        $pdf = PDF::loadview('content.raport.guru.cetak_raport.v_sampul_first', [
            'sampul' => $sampul, 'siswa' => $siswa, 'sekolah' => $sekolah, 'config' => $config
        ]);
        return $pdf->stream();
    }

    public function cetak_sampul3($id)
    {
        if (session('role') == "siswa") {
            $data = $this->sampulApi->get_S4();
        } else {
            $data = $this->sampulApi->get_by_S4(Help::decode(request()->segment(5)));
        }
        // dd($data);
        $data = $data['body']['data'];
        $sekolah = $this->sekolahApi->get_by_id(session('id'));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        // dd($sekolah);
        $kop = $data['kop'];
        $siswa = $data['siswa'];
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        $config = $config['body']['data'];
    }

    public function cetak_sampul_raport($id)
    {
        // dd(session()->all());
        $tahun = $this->tahunAjarApi->get_by_id(session('id_tahun_ajar'));
        $semester = $tahun['body']['data']['angka_semester'];
        if (session("role") == "siswa") {
            $siswa = $this->kelasSiswaApi->cetak_raport(session('id'), request()->segment(5));
        } else {
            $siswa = $this->kelasSiswaApi->cetak_raport(Help::decode(request()->segment(5)), session('id_tahun_ajar'));
        }
        // dd($siswa);
        if ($siswa['code'] != 200) {
            $pesan = array(
                'message' => $siswa['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }

        $siswa = $siswa['body']['data'];
        $spiritual = $siswa['nilai_spiritual'];
        if ($spiritual['selalu'] != null) {
            $nilai_sikap_spiritual = 'Selalu melakukan sikap : ' . $spiritual['selalu'] . '; Mulai meningkat pada sikap : ' . $spiritual['meningkat'];
        } else {
            $nilai_sikap_spiritual = 'Belum diinput';
        }

        $nilai_sosial = $siswa['nilai_sosial'];
        $selalu = $nilai_sosial['selalu'];
        // dd($selalu);
        if ($selalu == null) {
            $nilai_sikap_sosial = 'Belum diinput';
        } else {
            $ruwet = array();
            foreach ($selalu as $sl) {
                $ruwet[] = $sl['nama'];
            }
            $so_text_selalu = implode(", ", $ruwet);
        }
        $wali_kelas = $siswa['wali_kelas'];
        $nilai_mapel = $siswa['nilai_mapel'];
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        $config = $config['body']['data'];
        return view('content.raport.guru.cetak_raport.v_sampul_raport')->with([
            'siswa' => $siswa, 'sosial' => $nilai_sikap_sosial, 'spiritual' => $nilai_sikap_spiritual,
            'ekstra' => $nilai_ekstra, 'absensi' => $nilai_absensi, 'semester' => $semester, 'wali' => $wali_kelas, 'nilai_utama' => $nilai_mapel, 'config' => $config
        ]);
    }

    public function cetak_prestasi($id)
    {
        $siswa = $this->kelasSiswaApi->get_by_id($id);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $prestasi = $this->prestasiApi->get_by_siswa($id, session('id_tahun_ajar'));
        $prestasi = $prestasi['body']['data'];
        $catatan = $this->catatanApi->get_by_siswa($id, session('id_tahun_ajar'));
        // dd($catatan);
        $catatan = $catatan['body']['data'];
        // dd($prestasi);
        return view('content.raport.guru.cetak_raport.v_cetak_prestasi')->with(['siswa' => $siswa, 'prestasi' => $prestasi, 'catatan' => $catatan]);
    }
}
