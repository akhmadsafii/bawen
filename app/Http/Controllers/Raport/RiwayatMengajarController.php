<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class RiwayatMengajarController extends Controller
{
    private $gurupelajaranApi;
    private $tahunApi;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        $mapel = $this->gurupelajaranApi->get_by_guru(session('id_tahun_ajar'));
        // dd($mapel);
        $mapel = $mapel['body']['data'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        $tahun = $tahun['body']['data'];
        // dd($tahun);
       
        Session::put('title', 'Riwayat Mengajar');
        return view('content.raport.guru.riwayat_mengajar.v_riwayat_mengajar')->with(['template' => session('template'), 'mapel' => $mapel, 'tahun' => $tahun]);
    }
}
