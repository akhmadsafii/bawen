<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigApi;
use App\ApiService\Raport\ConfigTemplateApi;
use App\ApiService\Raport\LihatRaportApi;
use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CetakLegerController extends Controller
{
    private $lihatApi;
    private $configApi;
    private $tahunAjarApi;
    private $templateApi;
    private $rombelApi;
    private $waliKelasApi;

    public function __construct()
    {
        $this->lihatApi = new LihatRaportApi();
        $this->configApi = new ConfigApi();
        $this->tahunAjarApi = new TahunAjaranApi();
        $this->templateApi = new ConfigTemplateApi();
        $this->rombelApi = new RombelApi();
        $this->waliKelasApi = new WaliKelasApi();
    }

    public function index()
    {
        $tahun = $this->tahunAjarApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->by_tahun_aktif(Help::decode($_GET['th']));
        $config = $config['body']['data'];
        session()->put('title', 'Cetak Leger');
        return view('content.raport.guru.cetak_leger.v_cetak_leger')->with(['template' => session('template'), 'config' => $config, 'tahun' => $tahun]);
    }

    public function print_leger()
    {
        $rombel = $this->rombelApi->get_by_id(session('id_rombel'));
        $rombel = $rombel['body']['data'];
        $tahun = $this->tahunAjarApi->get_by_id(Help::decode($_GET['th']));
        $tahun = $tahun['body']['data'];
        $wali = $this->waliKelasApi->by_rombel_tahun(session('id_rombel'), substr($tahun['tahun_ajaran'], 0, 4));
        // dd($wali);
        $wali = $wali['body']['data'];
        session()->put('id_jurusan', $rombel['id_jurusan']);
        $template = $this->templateApi->by_tahun_jurusan(session('id_jurusan'), Help::decode($_GET['th']));
        // dd($template);
        $template = $template['body']['data'];
        if ($template == null) {
            $pesan = array(
                'message' => "Template Raport dengan tahun ajar ini belum tersedia",
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        if ($template['template'] == 'k16') {
            $cetak = $this->lihatApi->print_leger_k16(Help::decode($_GET['th']), session('id_rombel'));
            // dd($cetak);
            $view = 'content.raport.guru.cetak_leger.v_print_leger';
            // $view = 'content.raport.guru.cetak_leger.v_print_manual';
        } elseif ($template['template'] == 'manual') {
            $cetak = $this->lihatApi->print_leger_manual(Help::decode($_GET['th']), session('id_rombel'));
            $view = 'content.raport.guru.cetak_leger.v_print_manual';
        } else {
            $cetak = $this->lihatApi->print_leger_kd(Help::decode($_GET['th']), session('id_rombel'));
            $view = 'content.raport.guru.cetak_leger.v_print_kd';
        }
        $cetak = $cetak['body'];
        return view($view)->with(['cetak' => $cetak, 'wali' => $wali, 'tahun' => $tahun]);
    }

    public function print_leger_ekstra()
    {
        $tahun = $this->tahunAjarApi->get_by_id(Help::decode($_GET['th']));
        $tahun = $tahun['body']['data'];
        $wali = $this->waliKelasApi->by_rombel_tahun(session('id_rombel'), substr($tahun['tahun_ajaran'], 0, 4));
        // dd($wali);
        $wali = $wali['body']['data'];
        $cetak = $this->lihatApi->print_leger_ektra(Help::decode($_GET['th']), session('id_rombel'));
        // dd($cetak);
        $cetak = $cetak['body']['data'];
        return view('content.raport.guru.cetak_leger.v_print_manual')->with(['cetak' => $cetak, 'wali' => $wali, 'tahun' => $tahun]);
    }
}
