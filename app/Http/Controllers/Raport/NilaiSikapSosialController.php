<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiSosialApi;
use App\ApiService\Raport\SikapSosialApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NilaiSikapSosialController extends Controller
{
    private $profileApi;
    private $sikapSosialApi;
    private $nilaiSosialApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->sikapSosialApi = new SikapSosialApi();
        $this->nilaiSosialApi = new NilaiSosialApi();
    }

    public function index()
    {
        // dd(session()->all());
        $siswa = $this->nilaiSosialApi->get_by_gabungan(session('tahun'), session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $form = "add";
        $sikap = $this->sikapSosialApi->get_by_sekolah();
        $sikap = $sikap['body']['data'];
        return view('content.raport.guru.nilai_sikap_sosial.v_nilai_sikap_sosial')->with(['template' => session('template'), "sikap" => $sikap, "siswa" => $siswa, "form" => $form]);
    }

    public function store(Request $request)
    {
        $p = $request;
        $mode_form = $p['mode_form'];
        $error = array();
        for ($i = 1; $i < $p['jumlah']; $i++) {
            $selalu = empty($p['selalu_' . $i]) ? "" : $p['selalu_' . $i];
            $meningkat = empty($p['meningkat_' . $i]) ? "" : $p['meningkat_' . $i];
            if (!empty($selalu)) {
                if (in_array($meningkat, $selalu)) {
                    $error[] = "Error baris " . $i . " : Isian \"meningkat\" sudah dipakai di isian \"Selalu\"";
                }
            } else {
                $error[] = "Error baris " . $i . " : masih kosong";
            }
        }
        if (empty($error)) {
            $strsql = "";
            for ($i = 1; $i < $p['jumlah']; $i++) {
                $id_siswa = $p['id_siswa_' . $i];
                $selalu = implode("<br>", $p['selalu_' . $i]);
                $meningkat = $p['meningkat_' . $i];
                if ($mode_form == "add") {
                    $data_add[] = array(
                        'id_wali_kelas' => session('id'),
                        'id_kelas_siswa' => $id_siswa,
                        'id_ta_sm' => session('id_tahun_ajar'),
                        'selalu' => $selalu,
                        'meningkat' => $meningkat,
                        'id_sekolah' => session('id_sekolah'),
                    );
                } else {
                    $kelas_siswa = $this->nilaiSosialApi->get_by_id($id_siswa);
                    $kelas_siswa = $kelas_siswa['body']['data']['id_kelas_siswa'];

                    $data_update[] = array(
                        'id_wali_kelas' => session('id'),
                        'id' => $id_siswa,
                        'id_kelas_siswa' => $kelas_siswa,
                        'id_ta_sm' => session('id_tahun_ajar'),
                        'selalu' => $selalu,
                        'meningkat' => $meningkat,
                        'id_sekolah' => session('id_sekolah'),
                    );
                }
            }
            if ($mode_form == "add") {
                foreach ($data_add as $dt_add) {
                    $data = array(
                        'id_wali_kelas' => $dt_add['id_wali_kelas'],
                        'id_kelas_siswa' => $dt_add['id_kelas_siswa'],
                        'id_ta_sm' => $dt_add['id_ta_sm'],
                        'selalu' => $dt_add['selalu'],
                        'meningkat' => $dt_add['meningkat'],
                        'id_sekolah' => $dt_add['id_sekolah'],
                    );
                    // dd($data_add);
                    $result = $this->nilaiSosialApi->create(json_encode($data));
                }
            } else {
                foreach ($data_update as $dt_update) {
                    $data_upd = array(
                        'id' => $dt_update['id'],
                        'id_wali_kelas' => $dt_update['id_wali_kelas'],
                        'id_kelas_siswa' => $dt_update['id_kelas_siswa'],
                        'id_ta_sm' => $dt_update['id_ta_sm'],
                        'selalu' => $dt_update['selalu'],
                        'meningkat' => $dt_update['meningkat'],
                        'id_sekolah' => $dt_update['id_sekolah'],
                    );
                    $result_update = $this->nilaiSosialApi->update_info(json_encode($data_upd));
                    // dd($result_update);
                }
            };

            $d['status'] = "ok";
            $d['data'] = "Data berhasil disimpan..";
        } else {
            $d['status'] = "gagal";
            $d['data'] = implode("<br>", $error);
        }
        return response()->json($d);
        // return response($d);
    }

    public function store_revisi(Request $request)
    {
        $p = $request;
        $mode_form = $p['mode_form'];
        $error = array();
        for ($i = 1; $i < $p['jumlah']; $i++) {
            $selalu = empty($p['selalu_' . $i]) ? "" : $p['selalu_' . $i];
            // dd($selalu);
            $meningkat = empty($p['meningkat_' . $i]) ? "" : $p['meningkat_' . $i];
            // dd($meningkat);
            if (!empty($selalu)) {
                if (in_array($meningkat, $selalu)) {
                    $error[] = "Error baris " . $i . " : Isian \"meningkat\" sudah dipakai di isian \"Selalu\"";
                }
            }
        }
        if (empty($error)) {
            $strsql = "";
            $data_add = [];
            for ($i = 1; $i < $p['jumlah']; $i++) {
                $id_siswa = $p['id_siswa_' . $i];
                // dd($p['selalu_'.$i]);
                $selalu = implode("<br>", (array)$p['selalu_' . $i]);
                // dd($selalu);
                $meningkat = $p['meningkat_' . $i];
                $data_add[] = array(
                    'id_wali_kelas' => session('id'),
                    'id_kelas_siswa' => $id_siswa,
                    'id_ta_sm' => session('id_tahun_ajar'),
                    'selalu' => $selalu,
                    'meningkat' => $meningkat,
                    'id_sekolah' => session('id_sekolah'),
                );
            }
            // dd($data_add);
            foreach ($data_add as $dt_add) {
                $data = array(
                    'id_wali_kelas' => $dt_add['id_wali_kelas'],
                    'id_kelas_siswa' => $dt_add['id_kelas_siswa'],
                    'id_ta_sm' => $dt_add['id_ta_sm'],
                    'selalu' => $dt_add['selalu'],
                    'meningkat' => $dt_add['meningkat'],
                    'id_sekolah' => $dt_add['id_sekolah'],
                );
                // dd($data);
                if ($data['selalu'] != '') {
                    // dd($data);
                    $result = $this->nilaiSosialApi->create(json_encode($data));
                    // dd($result);
                }
            }
            // dd($result);

            $d['status'] = "ok";
            $d['data'] = "Data berhasil disimpan..";
        } else {
            $d['status'] = "gagal";
            $d['data'] = implode("<br>", $error);
        }
        return response()->json($d);
    }

    public function cetak()
    {
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        $siswas = $this->nilaiSosialApi->get_by_gabungan(session('tahun'), session('id_tahun_ajar'));
        // dd($siswas);
        $siswas = $siswas['body']['data'];
        $siswa = [];
        foreach ($siswas as $ssw) {
            if ($ssw['selalu'] != null) {
                $selalus = explode('<br>', $ssw['selalu']);
                $selalu = [];
                foreach ($selalus as $sll) {
                    $sosial = $this->sikapSosialApi->get_by_id($sll);
                    // dd($sosial);
                    $sosial = $sosial['body']['data']['nama'];
                    $selalu[] = $sosial;
                }
                // dd($selalu);
                $sos = implode(", ", $selalu);
            } else {
                $sos = null;
            }
            if ($ssw['meningkat'] != null) {
                $meningkat = $this->sikapSosialApi->get_by_id($ssw['meningkat']);
                // dd($sosial);
                $meningkat = $meningkat['body']['data']['nama'];
            } else {
                $meningkat = null;
            }
            $siswa[] = array(
                'id_kelas_siswa' => $ssw['id_kelas_siswa'],
                'data' => $ssw['data'],
                'nis' => $ssw['nis'],
                'nisn' => $ssw['nisn'],
                'nama' => $ssw['nama'],
                'selalu' => $sos,
                'meningkat' => $meningkat,
            );
        }
        // dd($siswa);
        return view('content.raport.guru.nilai_sikap_sosial.v_cetak')->with(['siswa' => $siswa, 'profile' => $profile]);
    }
}
