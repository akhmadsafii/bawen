<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\KkmApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KkmController extends Controller
{
    private $tahunApi;
    private $kkmApi;
    private $mapelApi;
    private $jurusanApi;
    private $kelasApi;

    public function __construct()
    {
        $this->kkmApi = new KkmApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Settingan Template');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $kelas = [];
        $klas = [];
        $jrsn = [];
        $ajar = [];
        if ($_GET['th'] != 'null') {
            $ajar = $this->tahunApi->get_by_id(Help::decode($_GET['th']));
            // dd($ajar);
            $ajar = $ajar['body']['data'];
        }
        if ($_GET['jr'] != 'null') {
            $jrsn = $this->jurusanApi->get_by_id(Help::decode($_GET['jr']));
            // dd($jrsn);
            $jrsn = $jrsn['body']['data'];
            $kelas = $this->kelasApi->get_by_jurusan(Help::decode($_GET['jr']));
            // dd($kelas);
            $kelas = $kelas['body']['data'];
        }
        $kkm = [];
        if ($_GET['kl'] != 'null') {
            $klas = $this->kelasApi->get_by_id(Help::decode($_GET['kl']));
            // dd($klas);
            $klas = $klas['body']['data'];
            $kkm = $this->kkmApi->by_kelas(Help::decode($_GET['th']), Help::decode($_GET['kl']));
            // dd($kkm);
            $result = $kkm['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($result)
                    ->addColumn('action', function ($data) {
                        $button = '<a href="javascript:void(0)" data-id="' . $data['id'] . '" class="edit btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return $button;
                    });
                $table->rawColumns(['action']);
                $table->addIndexColumn();

                return $table->make(true);
            }
        }
        return view('content.admin.raport.v_kkm')->with([
            'tahun' => $tahun, 'kkm' => $kkm,
            'jurusan' => $jurusan, 'mapel' => $mapel,
            'kelas' => $kelas, 'jrsn' => $jrsn, 'ajar' => $ajar, 'klas' => $klas
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data_insert = array(
            "id_kelas" => Help::decode($request['id_kelas']),
            "id_mapel" => $request['id_mapel'],
            "id_ta_sm" => Help::decode($request['id_ta_sm']),
            "nilai" => $request['nilai'],
            "id_sekolah" => session('id_sekolah'),
        );
        $result = $this->kkmApi->create(json_encode($data_insert));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $kkm = $this->kkmApi->get_by_id($request['id']);
        $kkm = $kkm['body']['data'];
        return response()->json($kkm);
    }

    public function update(Request $request)
    {
        $data = array(
            "id" => $request['id'],
            "id_kelas" => Help::decode($request['id_kelas']),
            "id_mapel" => $request['id_mapel'],
            "id_ta_sm" => Help::decode($request['id_ta_sm']),
            "nilai" => $request['nilai'],
            "id_sekolah" => session('id_sekolah'),
        );
        $result = $this->kkmApi->update_info(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        // dd($id);
        $delete = $this->kkmApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
