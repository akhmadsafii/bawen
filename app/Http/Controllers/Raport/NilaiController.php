<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\NilaiApi;
use App\ApiService\Raport\NilaiConfigApi;
use App\ApiService\Raport\NilaiConfigPtsApi;
use App\ApiService\Raport\NilaiPtsApi;
use App\ApiService\Raport\PredikatApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    private $gurupelajaranApi;
    private $nilaiApi;
    private $nilaiPtsApi;
    private $configApi;
    private $predikatApi;
    private $configPtsApi;
    private $tahunApi;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->configApi = new NilaiConfigApi();
        $this->nilaiApi = new NilaiApi();
        $this->nilaiPtsApi = new NilaiPtsApi();
        $this->predikatApi = new PredikatApi();
        $this->configPtsApi = new NilaiConfigPtsApi();
        $this->tahunApi = new TahunAjaranApi();
    }


    public function nilai_pas()
    {
        $config = $this->configApi->get_by_sekolah_tahun_ajar(session('id_tahun_ajar'));
        // dd($config);
        $config = $config['body']['data'];
        $guru = $this->gurupelajaranApi->get_by_id(Help::decode($_GET['gp']));
        $guru = $guru['body']['data'];
        // $id_rombel = $guru['id_rombel'];
        $siswa = $this->nilaiApi->get_gabungan(session('tahun'), Help::decode($_GET['gp']), session('id_tahun_ajar'));
        $siswa = $siswa['body']['data'];
        return view('content.raport.guru.mapel.v_nilai_pengetahuan_revisi')->with(['template' => session('template'), 'guru' => $guru, 'siswa' => $siswa, 'config' => $config]);
    }

    public function nilai_pts(Request $request)
    {
        $config = $this->configPtsApi->by_tahun_ajar(Help::decode($_GET['th']));
        // dd($config);
        $config = $config['body']['data'];
        $guru = $this->gurupelajaranApi->get_by_id(Help::decode($_GET['gp']));
        // dd($guru);
        $guru = $guru['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $siswa = $this->nilaiPtsApi->get_gabungan(session('tahun'), Help::decode($_GET['gp']), Help::decode($_GET['th']));
        // dd($siswa);
        $siswa = $siswa['body']['data'] ?? [] ;
        if ($request->ajax()) {
            $table = datatables()->of($siswa)
                ->addColumn('profile', function ($data) {
                    return '<b class="text-uppercase">' . $data['nama'] . '</b><p class="m-0">NISN. ' . $data['nisn'] . '</p>';
                });
            $table->editColumn('nilai', function ($row) {
                return '<a href="javascript:void(0)" data-id="' . $row['id_kelas_siswa'] . '" data-uh="' . $row['nilai_uh_rata'] . '" data-uts="' . $row['nilai_uts'] . '" data-akhir="' . $row['nilai_akhir'] . '" data-predikat="' . $row['predikat'] . '" data-keterangan="' . $row['deskripsi'] . '" class="pull-right edit"><i class="fas fa-pencil-alt"></i></a>
                <center>
                    <table class="table-borderless">
                        <thead>
                            <tr>
                                <td class="p-0">Ulangan Harian</td>
                                <td class="p-0">: ' . $row['nilai_uh_rata'] . '</td>
                            </tr>
                            <tr>
                                <td class="p-0">Ulangan UTS</td>
                                <td class="p-0">: ' . $row['nilai_uts'] . '</td>
                            </tr>
                            <tr>
                                <th style="border-top: 2px solid" class="p-0">
                                    Nilai Akhir
                                </th>
                                <th style="border-top: 2px solid" class="p-0">
                                    : ' . $row['nilai_akhir'] . '
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div>*) Predikat ' . $row['predikat'] ?? "-" . '</div>
                </center>';
            });
            $table->editColumn('deskripsi', function ($row) {
                return '<div><p class="m-0">' . $row['deskripsi'] . '</p></div>';
            });
            $table->rawColumns(['profile', 'nilai', 'deskripsi']);
            $table->addIndexColumn();

            return $table->make(true);
        }

        return view('content.raport.guru.mapel.v_nilai_pts_manual')->with(['template' => session('template'), 'guru' => $guru, 'config' => $config, 'tahun' => $tahun]);
    }

    public function simpan_manual(Request $request)
    {
        // dd($reque)
        $p = $request;
        for ($i = 1; $i <= $p['jumlah']; $i++) {
            $id_siswa = $p['id_siswa_' . $i];
            $harian = $p['harian_' . $i];
            $uts = $p['uts_' . $i];
            $uas = $p['uas_' . $i];
            $akhir = $p['akhir_' . $i];
            $deskripsi = $p['deskripsi_' . $i];
            $predikat = $p['predikat_' . $i];
            $data_mentah[] = array(
                "siswa" => $id_siswa,
                "nilai_uh_rata" => $harian,
                "nilai_uts" => $uts,
                "nilai_uas" => $uas,
                "nilai_akhir" => $akhir,
                "deskripsi" => $deskripsi,
                "predikat" => $predikat,
                "id_guru_pelajaran" => $p['id_guru_pelajaran'],
            );
        }
        foreach ($data_mentah as $mnth) {
            $data_insert = array(
                'id_ta_sm' => session('id_tahun_ajar'),
                'id_kelas_siswa' => $mnth['siswa'],
                'id_guru_pelajaran' => $mnth['id_guru_pelajaran'],
                'nilai_uh_rata' => $mnth['nilai_uh_rata'],
                'nilai_uts' => $mnth['nilai_uts'],
                'nilai_uas' => $mnth['nilai_uas'],
                'nilai_akhir' => $mnth['nilai_akhir'],
                'deskripsi' => $mnth['deskripsi'],
                'predikat' => $mnth['predikat'],
                'id_sekolah' => session('id_sekolah'),
            );
            if ($data_insert['nilai_akhir'] != null) {
                $result = $this->nilaiApi->create(json_encode($data_insert));
            }
        }
        if ($result['code'] == 200) {
            $d['status'] = "berhasil";
            $d['icon'] = "success";
            $d['message'] = $result['body']['message'];
        } else {
            $d['status'] = "gagal";
            $d['icon'] = "danger";
            $d['message'] = $result['body']['message'];
        }
        return response()->json($d);
        // dd($result_update);
    }

    public function get_range(Request $request)
    {
        // dd($request);
        $nilai = $request['hasil'];
        $predikat = $this->predikatApi->get_range($nilai);
        $predikat = $predikat['body']['data'];
        return response()->json($predikat);
    }

    public function simpan_pts(Request $request)
    {
        // dd($request);
        $data = array(
            'id_ta_sm' => Help::decode($request->id_ta_sm),
            'id_kelas_siswa' => $request->id_kelas_siswa,
            'id_sekolah' => session('id_sekolah'),
            'id_guru_pelajaran' => Help::decode($request->id_guru_pelajaran),
            'status' => 1,
            'nilai_uh_rata' => $request->nilai_uh_rata,
            'nilai_uts' => $request->nilai_uts,
            'nilai_akhir' => $request->nilai_akhir,
            'deskripsi' => $request->deskripsi,
            'predikat' => $request->predikat,
        );
        $result = $this->nilaiPtsApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
