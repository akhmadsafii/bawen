<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\IndonesiaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\KelompokApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class IndustriController extends Controller
{
    private $industriApi;
    private $kelompokApi;
    private $indonesiaApi;
    private $pembimbingApi;
    private $tahunApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->kelompokApi = new KelompokApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Industri');
        $url = $this->industriApi->download_excel();
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        if ($industri['code'] != 200) {
            $pesan = array(
                'message' => $industri['body']['message'],
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $industri['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = ' <a href="' . route('pkl_industri-edit', ['k' => Help::encode($data['id']), 'key' => str_slug($data['nama']), 'based' => 'detail']) . '" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $template = session('template');
        if (session('role') == 'admin-prakerin') {
            $template = 'default';
        }
        if (session('role') == 'admin-prakerin' || session('role') == 'supervisor') {
            return view('content.prakerin.admin-prakerin.v_industri')->with(['template' => $template, 'url' => $url]);
        } else {
            return view('content.prakerin.' . session('role') . '.v_industri')->with(['template' => $template, 'provinsi' => $provinsi, 'kabupaten' => $kabupaten]);
        }
    }

    public function edit(Request $request)
    {
        // dd(session()->all());
        $industri = $this->industriApi->get_by_id(Help::decode($_GET['k']));
        // dd($industri);
        $industri = $industri['body']['data'];
        $provinsi = $this->indonesiaApi->get_provinsi();
        $provinsi = $provinsi['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-prakerin') {
            $template = 'default';
        }
        if ($_GET['based'] == 'detail') {
            return view('content.prakerin.admin-prakerin.industri.v_detail')->with(['industri' => $industri, 'template' => $template, 'provinsi' => $provinsi]);
        } else if ($_GET['based'] == 'kelompok') {
            $kelompok = $this->kelompokApi->get_by_industri(Help::decode($_GET['k']));
            // dd($kelompok);
            $kelompok = $kelompok['body']['data'];
            $tahun = $this->tahunApi->get_by_sekolah();
            // dd($tahun);
            $tahun = $tahun['body']['data'];
            if ($request->ajax()) {
                $table = datatables()->of($kelompok)
                    ->addColumn('action', function ($data) {
                        $button = ' <a href="javascript:void(0)" class="edit btn btn-info btn-sm m-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="detail btn btn-purple btn-sm m-1"><i class="fas fa-info-circle"></i></button>';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete-pembimbing btn btn-danger btn-sm m-1"><i class="fas fa-trash"></i></button>';
                        return $button;
                    });
                $table->editColumn('tahun_ajar', function ($row) {
                    return $row['tahun_ajaran'] . ", " . $row['semester'];
                });
                $table->editColumn('anggota', function ($row) {
                    return count($row['anggota']) . ' anggota';
                });
                $table->rawColumns(['action', 'tahun_ajar', 'anggota']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.prakerin.admin-prakerin.industri.v_kelompok')->with(['industri' => $industri, 'template' => $template, 'provinsi' => $provinsi, 'tahun' => $tahun]);
        } else {
            if ($request->ajax()) {
                $table = datatables()->of($industri['pembimbing'])
                    ->addColumn('action', function ($data) {
                        $button = ' <a href="javascript:void(0)" class="edit btn btn-info btn-sm m-1" data-id="' . $data['id'] . '"><i class="fas fa-pencil-alt"></i></a>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<button type="button" data-id="' . $data['id'] . '" class="delete-pembimbing btn btn-danger btn-sm m-1"><i class="fas fa-trash"></i></button>';
                        return $button;
                    });
                $table->editColumn('ttl', function ($row) {
                    return $row['tempat_lahir'] . ", " . Help::getTanggal($row['tgl_lahir']);
                });
                $table->rawColumns(['action', 'ttl']);
                $table->addIndexColumn();

                return $table->make(true);
            }
            return view('content.prakerin.admin-prakerin.industri.v_pembimbing')->with(['industri' => $industri, 'template' => $template, 'provinsi' => $provinsi]);
        }
    }

    public function detail(Request $request)
    {
        $industri = $this->industriApi->get_by_id($request['id']);
        $industri = $industri['body']['data'];
        return response()->json($industri);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'pimpinan' => $request->pimpinan,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->industriApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pimpinan' => $request->pimpinan,
            'id_provinsi' => $request->id_provinsi,
            'id_kabupaten' => $request->id_kabupaten,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $result = $this->industriApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->industriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }



    public function load_pembimbing_kelompok(Request $request)
    {
        $id_kelompok = $request['id_kelompok'];
        $id_pembimbing = $request['id_pemb_industri'];
        $pembimbing = $this->pembimbingApi->by_industri($request['id_industri']);
        $pembimbing = $pembimbing['body']['data'];
        $kelompok = $this->kelompokApi->get_by_industri($request['id_industri']);
        $kelompok = $kelompok['body']['data'];
        $data_kelompok = '';
        $data_pembimbing = '';
        foreach ($kelompok as $kl) {
            $data_kelompok .= '<option value="' . $kl['id'] . '" ' . (($kl['id'] == $id_kelompok) ? 'selected="selected"' : "") . '>' . $kl['nama'] . '</option>';
        }
        foreach ($pembimbing as $pb) {
            $data_pembimbing .= '<option value="' . $pb['id'] . '" ' . (($pb['id'] == $id_pembimbing) ? 'selected="selected"' : "") . '>' . $pb['nama'] . '</option>';
        }

        $output = array(
            'kelompok' => $data_kelompok,
            'pembimbing' => $data_pembimbing
        );
        return $output;
    }

    public function load_kabupaten(Request $request)
    {
        $id_kabupaten = $request['id_kabupaten'];
        $data  = $this->indonesiaApi->get_kabupaten_by_provinsi($request['id_provinsi']);
        // dd($data);
        $result = $data['body']['data'];
        // dd($result);
        $output = '';
        foreach ($result['cities'] as $brand) {
            $output .= '<option value="' . $brand['id'] . '" ' . (($brand['id'] == $id_kabupaten) ? 'selected="selected"' : "") . '>' . $brand['name'] . '</option>';
        }
        return $output;
    }

    public function deletePembimbing(Request $request)
    {
        $delete = $this->pembimbingApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function simpanPembimbing(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id_industri' => $request->id_industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pembimbingApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function updatePembimbing(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'id_industri' => $request->id_industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->pembimbingApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->pembimbingApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_pembimbing($id_industri)
    {
        $pembimbing = $this->pembimbingApi->by_industri($id_industri);
        $pembimbing = $pembimbing['body']['data'];
        $html = '';
        if (empty($pembimbing)) {
            $html .= '<tr><td colspan="6" class="text-center">Data belum tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($pembimbing as $pb) {
                $html .= '
                <tr>
                    <td>' . $no++ . '</td>
                    <td>' . strtoupper($pb['nama']) . '</td>
                    <td>' . $pb['telepon'] . '</td>
                    <td>' . $pb['email'] . '</td>
                    <td>' . $pb['alamat'] . '</td>
                    <td>
                        <a href="javascript:void(0)"
                            class="btn btn-purple btn-sm editPembimbing"
                            data-id="' . $pb['id'] . '" data-industri="' . $pb['id_industri'] . '"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)"
                            data-id="' . $pb['id'] . '" data-industri="' . $pb['id_industri'] . '"
                            class="deletePembimbing btn btn-danger btn-sm"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                ';
            }
        }
        return $html;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->industriApi->upload_excel(json_encode($data), $request['role']);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            $industri = $this->data_industri_full();
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'industri' => $industri
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->industriApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->industriApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
