<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\Prakerin\TugasApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;

class TugasController extends Controller
{
    private $pembimbingIndustriApi;
    private $tugasApi;
    private $guruApi;
    private $siswaApi;
    private $hash;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
        $this->tugasApi = new TugasApi();
        $this->siswaApi = new SiswaPrakerinApi();
        $this->pembimbingIndustriApi = new PembimbingIndustriApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        if (request()->ajax()) {
            $tugas = $this->tugasApi->get_by_user_siswa(session('id_prakerin_siswa'));
            $tugas = $tugas['body']['data'];
            $data = [];
            foreach ($tugas as $tg) {
                $loop = explode(', ', $tg['nama']);
                // dd($loop);
                $data[] = array(
                    'id' => $tg['id'],
                    'waktu' => $tg['waktu'],
                    'nama' => $tg['nama'],
                    'loop' => $loop,
                );
            }
            return Response::json($data);
        }
        Session::put('title', 'Kegiatan Siswa');
        if (session('role') == 'siswa' || session('role') == 'pembimbing-industri') {
            if (session('role') == 'pembimbing-industri') {
                $siswas = $this->siswaApi->get_by_pembimbing_industri(session('id'));
                // dd($siswas);
                $siswas = $siswas['body']['data'];
                $profile = $this->pembimbingIndustriApi->get_by_id(session('id'));
                // dd($profile);
                $paraf = $profile['body']['data']['paraf'];
                $siswa = [];
                foreach ($siswas as $ssw) {
                    $siswa[] = array(
                        'file' => $ssw['file'],
                        'nama' => $ssw['nama'],
                        'nisn' => $ssw['nisn'],
                        'id' => $ssw['id'],
                        'id_code' => $this->hash->encode($ssw['id'])
                    );
                }
                return view('content.prakerin.' . session('role') . '.v_tugas')->with(['template' => session('template'), 'siswa' => $siswa, 'paraf' => $paraf]);
            }
            return view('content.prakerin.' . session('role') . '.v_tugas')->with(['template' => session('template'), 'id_code' => $this->hash->encode(session('id'))]);
        } elseif (session('role') == 'guru' || session('role') == 'supervisor') {
            // dd(session()->all());
            if(session('role') == 'guru'){
                $siswas = $this->siswaApi->get_by_pembimbing_sekolah(session('id'));
            }else{
                $siswas = $this->siswaApi->sekolah(session('id_tahun_ajar'));
            }
            // dd($siswas);
            $siswas = $siswas['body']['data'];
            $siswa = [];
            foreach ($siswas as $ssw) {
                $siswa[] = array(
                    'file' => $ssw['file'],
                    'industri' => $ssw['industri'],
                    'alamat_industri' => $ssw['alamat_industri'],
                    'id_siswa' => $ssw['id_siswa'],
                    'nama' => $ssw['nama'],
                    'nisn' => $ssw['nisn'],
                    'id' => $ssw['id'],
                    'id_code' => $this->hash->encode($ssw['id'])
                );
            }
            return view('content.prakerin.guru.v_list_siswa')->with(['template' => session('template'), 'siswa' => $siswa]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->kelompokApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $nama = join(", ", $request->title);
        $data = array(
            'id_prakerin_siswa' => session('id_prakerin_siswa'),
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_sekolah' => session('id_sekolah'),
            'nama' => $nama,
            'waktu' => $request['start'],
        );
        $insert = $this->tugasApi->create(json_encode($data));

        if ($insert['code'] == 200) {
            return response()->json([
                'success' => $insert['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $insert['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $tugas = $this->tugasApi->get_by_id($request['id']);
        $tugas = $tugas['body']['data'];
        if (session('role') == 'guru') {
            $data = array(
                'id' => $request->id,
                'id_prakerin_siswa' => $tugas['id_prakerin_siswa'],
                'waktu' => $request['start'],
                'catatan' => $request['catatan'],
                'id_sekolah' => session('id_sekolah'),
                'nama' => $tugas['nama_tugas'],
                'id_ta_sm' => $tugas['id_tahun_ajar'],
                'status' => 1
            );
        } else {
            $nama = join(", ", $request->title);
            $data = array(
                'id' => $request->id,
                'id_prakerin_siswa' => $tugas['id_prakerin_siswa'],
                'waktu' => $request['start'],
                'id_sekolah' => session('id_sekolah'),
                'nama' => $nama,
                'id_ta_sm' => $tugas['id_tahun_ajar'],
                'status' => 1
            );
        }

        $update = $this->tugasApi->create(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->tugasApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function tugas_siswa($id)
    {
        // dd($id);
        $tugas = $this->tugasApi->get_by_user_siswa($id);
        $tugas = $tugas['body']['data'];
        $data = [];
        foreach ($tugas as $tg) {
            $loop = explode(', ', $tg['nama']);
            $data[] = array(
                    'id' => $tg['id'],
                    'waktu' => $tg['waktu'],
                    'nama' => $tg['nama'],
                    'catatan' => $tg['catatan'],
                    'loop' => $loop,
                );
        }
        return Response::json($data);
    }

    public function tugas_detail_siswa(Request $request)
    {
        $tugas = $this->siswaApi->get_by_id($request['id']);
        $data = $tugas['body']['data'];
        // dd($data);
        $html = '<div class="row">
                    <div class="col-sm-12">
                        <span class="float-left mr-3">
                            <img src="'.$data['file'].'" alt=""
                                class="thumb-lg rounded-circle" style="height: 88px; width: 88px;">
                        </span>
                        <div class="media-body text-white">
                            <h4 class="mt-1 mb-1 font-18" style="color: #fff">'.ucwords($data['nama']).'</h4>
                            <p class="font-13 text-light mb-0">'.$data['nisn'].'</p>
                            <p class="text-light mb-0">'.$data['jurusan'].'</p>
                        </div>
                    </div>
                </div>';
        if ($request['params'] == "kegiatan") {
            $outputs = '';
        } elseif ($request['params'] == 'paraf') {
            $tugas = $this->tugasApi->cetak_kegiatan($request['id']);
            $tugas = $tugas['body']['data'];
            $siswa = $this->siswaApi->get_by_id($request['id']);
            $siswa = $siswa['body']['data'];
            $outputs = '<div class="row" style="margin-bottom: 23px; width: 100%">
                            <div class="col-md-12" style="float: none; margin: 0 auto;">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Hari, Tanggal</th>
                                                <th>Kegiatan</th>
                                                <th style="text-align: center">Paraf</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
            $no = 1;
            foreach ($tugas as $tg) {
                if (empty($tg['nama'])) {
                    $outputs .= '
                    <tr style="background: #d23030">
                        <td style="color: #fff">'.$no++.'</td>
                        <td style="color: #fff">'.Help::getDay($tg['waktu']).'</td>
                        <td style="color: #fff">Tidak ada kegiatan siswa yang diinput</td>
                        <td>
                        </td>
                    </tr>
                    ';
                } else {
                    $check = '';
                    if ($tg['status'] == 4) {
                        $check = 'checked';
                    }
                    $outputs .= '
                    <tr>
                        <td>'.$no++.'</td>
                        <td>'.Help::getDay($tg['waktu']).'</td>
                        <td>'.$tg['nama'].'</td>
                        <td style="text-align: center">
                            <input type="checkbox" name="checked['.$tg['id'].']" '.$check.' class="chc" data-toggle="toggle">
                        </td>
                    <tr>
                    ';
                }
            }
            $outputs .= '<tr>
                                                <td colspan="4" style="text-align: right">
                                                <a href="tugas/cetak/'. $this->hash->encode($siswa['id']).'" class="btn btn-success" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                                                    <button type="submit" class="btn btn-info" id="saveCetak"><i class="fa fa-floppy-o"></i>
                                                        Simpan</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>';
        } elseif ($request['params'] == 'siswa') {
            $outputs = '
            <div class="container rounded bg-white mt-5 mb-5">
            <style>
            .chat-message {
                padding: 0px 20px 115px;
            }
            </style>
                <div class="row">
                    <div class="col-md-3 border-right">
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img
                                class="rounded-circle mt-5" width="150px"
                                src="'.$data['file'].'"><span
                                class="font-weight-bold">'.ucwords($data['nama']).'</span><span
                                class="text-black-50">'.$data['email'].'</span><span> </span></div>
                    </div>
                    <div class="col-md-9 border-right">
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile Settings</h4>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-12"><label class="labels">Name</label><input type="text"
                                        class="form-control" value="'.ucwords($data['nama']).'" readonly></div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12"><label class="labels">NISN</label><input type="text"
                                        class="form-control" value="'.$data['nisn'].'" readonly></div>
                                <div class="col-md-12"><label class="labels">NIS</label><input type="text"
                                        class="form-control" readonly value="'.$data['nis'].'"></div>
                                <div class="col-md-12"><label class="labels">Alamat</label><input type="text"
                                        class="form-control" readonly value="'.$data['alamat'].'"></div>
                                <div class="col-md-12"><label class="labels">Telepon</label><input type="text"
                                        class="form-control" readonly value="'.$data['telepon'].'"></div>
                                <div class="col-md-12"><label class="labels">Kelas</label><input type="text"
                                        class="form-control" readonly value="'.$data['kelas'].'"></div>
                                <div class="col-md-12"><label class="labels">Jurusan</label><input type="text"
                                        class="form-control" readonly value="'.ucwords($data['jurusan']).'"></div>
                                <div class="col-md-12"><label class="labels">Sekolah</label><input type="text"
                                        class="form-control" readonly value="'.$data['sekolah'].'"></div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6"><label class="labels">Tanggal Mulai</label><input type="text"
                                        class="form-control" readonly value="'.Help::getTanggal($data['tgl_mulai']).'"></div>
                                <div class="col-md-6"><label class="labels">Tanggal Selesai</label><input
                                        type="text" class="form-control" value="'.Help::getTanggal($data['tgl_selesai']).'" readonly></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ';
        } else {
            $guru = $this->guruApi->get_by_id($data['id_pemb_sekolah']);
            $guru = $guru['body']['data'];
            // dd($guru);
            $outputs = '
            <style>
            .chat-message {
                padding: 0 20px 115px;
            }

            .profile-header {
                background: #EEE;
                color: #0099da;
                font-size: 18px;
                padding: 15px 30px;
                margin-bottom: 20px;
            }

            .profile-border {
                border: #EEE 1px solid;
                border-radius: 1px;
                margin: 50px auto;
                padding-bottom: 20px;
            }

            .form-control {
                border-top: none;
                border-left: none;
                border-right: none;
                border-radius: 0px;
                height: 28px;
                color: #000;
                padding-left: 0px;
                box-shadow: none;
                margin: 10px auto;
            }

            label {
                text-align: right !important;
                margin-top: 17px !important;
                font-size: 9pt;
                font-weight: 500 !important;
                color: #000;
            }

            .profile-img {
                width: 167px;
                height: 210px;
                margin: 5px;
                padding: 5px;
                background: #EEE;
                border: 1px solid #CCC;
            }

            .change-pic {
                background: #1194e0;
                color: #FFF;
                padding: 5px 20px !imprtant;
                text-align: center;
            }

            .change-pic a {
                color: #FFF;
                text-decoration: none;
            }

            .change-pic a:hover {
                color: #CCC;
            }

            .custom-btn {
                border-radius: 2px;
                padding: 5px 25px;
                margin: 5px 10px;
            }
            </style>
            <div class="container profile-border">
                <div class="row">
                    <div class="profile-header">Pembimbing Sekolah Profile</div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">
                                        Nama<b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="text" name="" id="" value="'.ucwords($guru['nama']).'" class="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">Tgl Lahir <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="text"  value="'.Help::getTanggal($guru['tgl_lahir']).'" class="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">Email
                                        <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="text" value="'.$guru['email'].'" id="" class="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">Telepon <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="email" value="'.$guru['telepon'].'" id="" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">NIP
                                        <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="text" value="'.$guru['nip'].'" id="" class="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">Kelamin <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7" style="margin-bottom:15px;">
                                    <input type="text" value="'.$guru['jenkel'].'" id="" class="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-lg-4 col-md-4 col-sm-4 col-xs-5">Agama <b>:</b></label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <input type="text" value="'.$guru['agama'].'" id="" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" align="center">
                        <div class="profile-img">
                            <img src="'.$guru['file'].'"
                                alt="Profile Pic">
                        </div>
                    </div>
                </div>
            </div>
            ';
        }
        $output = array(
            'html' => $html,
            'output' => $outputs,
            'nisn' => $data['nisn'],
            'id_code' => $this->hash->encode($data['id']),
        );
        return Response::json($output);
    }

    public function save(Request $request)
    {
        $id_siswa = array_keys($request['checked']);
        $siswa = join(",", $id_siswa);
        $data = array(
            'id_tugas' => $siswa
        );
        $update = $this->tugasApi->rubah_paraf(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function check(Request $request)
    {
        $tanggal = date('Y-m-d', strtotime($request->tanggal));
        // dd($tanggal);
        $tugas = $this->tugasApi->get_by_user_siswa(session('id_prakerin_siswa'));
        $tugas = $tugas['body']['data'];
        $data = true;
        foreach ($tugas as $tg) {
            $waktu =  date('Y-m-d', strtotime($tg['waktu']));
            if ($tanggal == $waktu) {
                $data = false;
            }
        }
        return Response::json($data);
    }

    public function cetak($id)
    {
        // dd($id);
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $pembimbing = $this->pembimbingIndustriApi->get_by_id($siswa['id_pemb_industri']);
        $pembimbing = $pembimbing['body']['data'];
        $paraf = $pembimbing['paraf'];
        $tugas = $this->tugasApi->cetak_kegiatan($this->hash->decode($id)[0]);
        $tugas = $tugas['body']['data'];
        $agenda = [];
        foreach ($tugas as $tg) {
            $agenda[] = array(
                'waktu' => $tg['waktu'],
                'id_prakerin_siswa' => !empty($tg['id_prakerin_siswa']) ? $tg['id_prakerin_siswa'] : null,
                'nama' => !empty($tg['nama']) ? $tg['nama'] : null,
                'status' => !empty($tg['status']) ? $tg['status'] : null,
                'id' => !empty($tg['id']) ? $tg['id'] : null,
                'catatan' => !empty($tg['catatan']) ? $tg['catatan'] : null,
                'id_sekolah' => !empty($tg['catatan']) ? $tg['id_sekolah'] : null,
                'id_ta_sm' => !empty($tg['catatan']) ? $tg['id_ta_sm'] : null,

            );
        }
        return view('content.prakerin.siswa.v_cetak_kegiatan')->with(['template' => session('template'), 'siswa' => $siswa, 'agenda' => $agenda, 'paraf' => $paraf]);
    }
}
