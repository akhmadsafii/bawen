<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Prakerin\JenisNilaiApi;
use App\ApiService\Prakerin\KelompokApi;
use App\ApiService\Prakerin\NilaiApi;
use App\ApiService\Prakerin\NilaiSiswaApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\PembimbingSertifikatApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\Prakerin\TemplateSertifikatApi;
use App\ApiService\Prakerin\TugasApi;
use App\ApiService\Raport\PredikatApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Hashids\Hashids;
use Illuminate\Http\Request;

class NilaiSiswaController extends Controller
{
    private $pembimbingSertifikatApi;
    private $pembimbingIndustriApi;
    private $tugasApi;
    private $predikatApi;
    private $kelompokApi;
    private $nilaiSiswaApi;
    private $jenisApi;
    private $siswaApi;
    private $hash;
    private $jurusanApi;
    private $nilaiApi;
    private $kelasSiswaApi;
    private $templateApi;

    public function __construct()
    {
        $this->tugasApi = new TugasApi();
        $this->predikatApi = new PredikatApi();
        $this->siswaApi = new SiswaPrakerinApi();
        $this->kelompokApi = new KelompokApi();
        $this->jenisApi = new JenisNilaiApi();
        $this->nilaiApi = new NilaiApi();
        $this->nilaiSiswaApi = new NilaiSiswaApi();
        $this->pembimbingIndustriApi = new PembimbingIndustriApi();
        $this->pembimbingSertifikatApi = new PembimbingSertifikatApi();
        $this->templateApi = new TemplateSertifikatApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        if (session('role') == 'pembimbing-industri') {
            $siswas = $this->siswaApi->get_by_pembimbing_industri(session('id'));
            $kelompok = $this->kelompokApi->get_by_pembimbing_industri();
            $kelompok = $kelompok['body']['data'];
        } else {
            $siswas = $this->siswaApi->get_by_pembimbing_sekolah(session('id'));
            $kelompok = [];
        }
        // dd($kelompok
        $siswas = $siswas['body']['data'];
        $siswa = [];
        foreach ($siswas as $sw) {
            $siswa[] = array(
                'file' => $sw['file'],
                'nama' => $sw['nama'],
                'industri' => $sw['industri'],
                'status' => $sw['status'],
                'id_code' => $this->hash->encode($sw['id']),
            );
        }
        return view('content.prakerin.' . session('role') . '.v_nilai_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'kelompok' => $kelompok]);
    }

    public function edit(Request $request)
    {
        $industri = $this->industriApi->get_by_id($request['id']);
        $industri = $industri['body']['data'];
        $ex_file = explode('/', $industri['file']);
        $industri['file_edit'] = end($ex_file);
        return response()->json($industri);
    }

    public function get_nilai(Request $request)
    {
        $siswa = $this->siswaApi->get_by_id($request['id']);
        $siswa = $siswa['body']['data'];
        $nilai = $this->nilaiApi->get_by_jurusan($siswa['id_jurusan']);
        $nilai = $nilai['body']['data'];
        $html = '<div class="col-md-12">
        <table class="table"><tr><th>Jenis</th><th>nama</th><th>Nilai</th></tr>';

        foreach ($nilai as $nl) {
            $html .= '<tr>
                            <td>' . $nl['jenis_nilai'] . '</td>
                            <td>' . $nl['nama'] . '</td>
                            <td><input type="text" name="" id="" class="form-control"></td>
                        </tr>';
        }
        $html .= '</table></div>';
        return response()->json($html);
    }

    public function store(Request $request)
    {
        // dd($request);
        $id_nilai = $request['id_nilai'];
        $nilai = $request['nilai'];
        $res = [];
        foreach ($id_nilai as $key => $value) {
            foreach ($nilai as $k => $v) {
                if ($key == $k) {
                    $res[$key]['id_prakerin_nilai'] = $value;
                    $res[$key]['nilai'] = $v;
                }
            }
        }

        foreach ($res as $r) {
            $data_insert = array(
                'id_sekolah' => session('id_sekolah'),
                'status' => 1,
                'id_prakerin_siswa' => $request['id_siswa'],
                'id_prakerin_nilai' => $r['id_prakerin_nilai'],
                'nilai' => $r['nilai'],
            );
            $result = $this->nilaiSiswaApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
        // dd($request);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pimpinan' => $request->pimpinan,
            'id_provinsi' => $request->provinsi,
            'id_kabupaten' => $request->kabupaten,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->industriApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->industriApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->industriApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }


    public function delete($id)
    {
        $delete = $this->industriApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function jenis($id)
    {
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $siswa['id_code'] = $id;
        $jenis = $this->jenisApi->get_by_jurusan($siswa['id_jurusan']);
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $siswas = $this->siswaApi->get_by_pembimbing_industri(session('id'));
        // dd($siswas);
        $siswas = $siswas['body']['data'];
        $siswa_industri = [];
        foreach ($siswas as $sw) {
            $siswa_industri[] = array(
                'file' => $sw['file'],
                'nama' => $sw['nama'],
                'status' => $sw['status'],
                // 'id' => $sw['id'],
                'id_code' => $this->hash->encode($sw['id']),
            );
        }

        $cetak = $this->nilaiSiswaApi->cetak_nilai($siswa['id'], $siswa['id_jurusan']);
        $cetak = $cetak['body']['data'];
        return view('content.prakerin.' . session('role') . '.v_jenis_nilai')->with(['template' => session('template'), 'jenis' => $jenis, 'siswa' => $siswa, 'siswas' => $siswa_industri, 'cetak' => $cetak]);
    }

    public function get_by_jenis(Request $request)
    {
        // dd($request);
        $nilai = $this->nilaiSiswaApi->by_jenis($request['id_jenis'], $request['id']);
        $nilai = $nilai['body']['data'];
        // dd($nilai);
        if (!empty($nilai)) {
            $jenis = end($nilai);
            $html = '<h5 class="box-title">' . ucwords($jenis['jenis_nilai']) . '</h5>
                    <hr>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Nilai</th>
                                <th style="width: 30%">Masukan Nilai</th>
                            </tr>
                        </thead>';
            $no = 1;
            foreach ($nilai as $nl) {
                if (session('role') == 'siswa') {
                    $html .= '<tr>
                                        <td>' . $no . '</td>
                                        <td>' . $nl['nama'] . '</td>
                                        <td>' . $nl['nilai'] . '</td>
                                    </tr>';
                } else {
                    $html .= '<tr>
                                <td>' . $no . '</td>
                                <td>' . $nl['nama'] . '</td>
                                <input type="hidden" name="id_nilai[]" value="' . $nl['id_nilai'] . '">
                                <td><input type="number" name="nilai[]" value="' . $nl['nilai'] . '" class="form-control"></td>
                            </tr>';
                }
                $no++;
            }
        } else {
            $html = '
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Nilai</th>
                                <th style="width: 30%">Masukan Nilai</th>
                            </tr>
                        </thead>
                        <tr><td colspan="4" class="text-center">Harap masukan jenis nilai dulu ya</td></tr>';
        }

        if (session('role') == 'siswa') {
            $html .= '</table>';
        } else {
            $html .= ' <tr>
                    <td colspan="3">
                        <button type="submit" class="btn btn-info" id="saveNilai">Simpan</button>
                        <a href="#" class="btn btn-danger">Batalkan</a>
                    </td>
                </tr>
            </table>';
        }
        return response()->json($html);
    }

    public function cetak($id)
    {
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $predikat = $this->predikatApi->get_by_sekolah();
        $predikat = $predikat['body']['data'];
        $cetak = $this->nilaiSiswaApi->cetak_nilai($siswa['id'], $siswa['id_jurusan']);
        $cetak = $cetak['body']['data'];
        // dd($cetak);
        return view('content.prakerin.siswa.v_cetak_nilai')->with(['predikat' => $predikat, 'siswa' => $siswa, 'cetak' => $cetak]);
    }

    public function sertifikat($id)
    {
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        $siswa = $siswa['body']['data'];
        $sertifikat = $this->pembimbingSertifikatApi->get_by_pemb_industri($siswa['id_pemb_industri']);
        $sertifikat = $sertifikat['body']['data'];
        if ($sertifikat != null) {
            $template = $this->templateApi->get_by_id($sertifikat['id_template']);
            $template = $template['body']['data'];
        }
        return view('content.prakerin.siswa.v_cetak_sertifikat')->with(['siswa' => $siswa, 'template' => $template]);
    }

    public function cetak_kegiatan($id)
    {
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        $siswa = $siswa['body']['data'];
        if (session('role') == "siswa") {
            $pembimbing = $this->pembimbingIndustriApi->get_by_id($siswa['id_pemb_industri']);
            $pembimbing = $pembimbing['body']['data'];
            $paraf = $pembimbing['paraf'];
            $tugas = $this->tugasApi->cetak_kegiatan(session('id_prakerin_siswa'));
            $tugas = $tugas['body']['data'];
            $agenda = [];
            foreach ($tugas as $tg) {
                $agenda[] = array(
                    'waktu' => $tg['waktu'],
                    'id_prakerin_siswa' => !empty($tg['id_prakerin_siswa']) ? $tg['id_prakerin_siswa'] : null,
                    'nama' => !empty($tg['nama']) ? $tg['nama'] : null,
                    'status' => !empty($tg['status']) ? $tg['status'] : null,
                    'id' => !empty($tg['id']) ? $tg['id'] : null,
                    'catatan' => !empty($tg['catatan']) ? $tg['catatan'] : null,
                    'id_sekolah' => !empty($tg['catatan']) ? $tg['id_sekolah'] : null,
                    'id_ta_sm' => !empty($tg['catatan']) ? $tg['id_ta_sm'] : null,

                );
            }
            return view('content.prakerin.siswa.v_cetak_kegiatan')->with(['template' => session('template'), 'siswa' => $siswa, 'agenda' => $agenda, 'paraf' => $paraf]);
        }

        $tugas = $this->tugasApi->cetak_kegiatan($this->hash->decode($id)[0]);
        $tugas = $tugas['body']['data'];
        $agenda = [];
        foreach ($tugas as $tg) {
            $agenda[] = array(
                'waktu' => $tg['waktu'],
                'id_prakerin_siswa' => !empty($tg['id_prakerin_siswa']) ? $tg['id_prakerin_siswa'] : null,
                'nama' => !empty($tg['nama']) ? $tg['nama'] : null,
                'status' => !empty($tg['status']) ? $tg['status'] : null,
                'id' => !empty($tg['id']) ? $tg['id'] : null,
                'catatan' => !empty($tg['catatan']) ? $tg['catatan'] : null,
                'id_sekolah' => !empty($tg['catatan']) ? $tg['id_sekolah'] : null,
                'id_ta_sm' => !empty($tg['catatan']) ? $tg['id_ta_sm'] : null,

            );
        }
        return view('content.prakerin.siswa.v_lihat_kegiatan')->with(['agenda' => $agenda, 'template' => session('template'), 'siswa' => $siswa]);
    }

    public function list_siswa()
    {
        // dd(session()->all());
        session()->put('title', 'Daftar Siswa');
        $result = $this->siswaApi->sekolah(session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $result['body']['data'];
        $routes = "user-siswa";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        return view('content.prakerin.guru.v_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'search' => $search, 'pagination' => $pagination, 'routes' => $routes]);
    }

    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => $request['id_jurusan'],
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                <td>' . $nomer++ . '</td>
                <td>' . ucfirst($sw['nama']) . '</td>
                <td>' . $sw['nis'] . '</td>
                <td>' . $sw['nisn'] . '</td>
                <td>' . $sw['rombel'] . '</td>
                <td>' . $sw['jurusan'] . '</td>
                <td>
                    <button data-toggle="collapse" data-target="#demo' . $sw['id'] . '"
                        class="btn btn-sm btn-success accordion-toggle"><i
                            class="fas fa-info-circle"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo' . $sw['id'] . '">
                        <button class="btn btn-info btn-sm my-3 pull-right"
                            onclick="tambahTranskip(' . $sw['id'] . ')"><i class="fas fa-plus-circle"></i>
                            Tambah Transkip</button>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Keterangan</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataTranskrip' . $sw['id'] . '">';
                if (empty($sw['transkrips'])) {
                    $html .= ' <tr>
                                <td colspan="5" class="text-center">Data saat ini kosong</td>
                            </tr>';
                } else {
                    $no = 1;
                    foreach ($sw['transkrips'] as $tr) {
                        if ($tr['file'] == null) {
                            $download = '-';
                        } else {
                            $download = '<a href="' . route('raport_transkip-download', $tr['id']) . '"  target="_blank" class="btn btn-warning btn-xs">download file <i class="fa fa-download"></i></a>';
                        }
                        $html .= '<tr>
                                    <td class="text-center">' . $no++ . '</td>
                                    <td class="text-center">' . $tr['nama'] . '</td>
                                    <td class="text-center">' . $download . '</td>
                                    <td class="text-center">' . $tr['keterangan'] . '</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" data-id="' . $tr['id'] . '"
                                            class="btn btn-info btn-sm editTranskrip"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <button class="btn btn-danger btn-sm"
                                            onclick="deleteTranskrip(' . $tr['id'] . ', ' . $sw['id'] . ')"><i
                                                class="fas fa-trash"></i></button>
                                    </td>
                                </tr>';
                    }
                }
                $html .= ' </tbody>
                            </table>
                        </div>
                    </td>
                </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function show_sertifikat($id)
    {
        // dd($id);
        $siswa = $this->siswaApi->get_by_id($this->hash->decode($id)[0]);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $sertifikat = $this->pembimbingSertifikatApi->get_by_pemb_industri($siswa['id_pemb_industri']);
        if ($sertifikat['code'] != 200) {
            $message = array(
                'status' => 'gagal',
                'message' => 'Guru Pembimbing belum mengset template sertifikat',
                'icon' => 'error'
            );
            return redirect()->back()->with(['message' => $message]);
        }
        $sertifikat = $sertifikat['body']['data'];

        return view('content.prakerin.template_sertifikat.v_'.$sertifikat['template'])->with(['siswa' => $siswa]);
        // return view('content.raport.guru.cetak_raport.v_cetak_raport_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'tahun' => $tahun]);
    }
}
