<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\KelompokApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\KaprodiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KelompokController extends Controller
{
    private $tahunApi;
    private $kelompokApi;
    private $industriApi;
    private $kelasSiswaApi;
    private $siswaApi;
    private $jurusanApi;
    private $pembimbingApi;
    private $guruApi;
    private $sekolahApi;
    private $kaprodiApi;
    private $kelasApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->kelompokApi = new KelompokApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->siswaApi = new SiswaPrakerinApi();
        $this->guruApi = new GuruApi();
        $this->sekolahApi = new SekolahApi();
        $this->kaprodiApi = new KaprodiApi();
        $this->kelasApi = new KelasApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Kelompok');
        $tahun = $this->tahunApi->get_by_sekolah();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        $industri = $industri['body']['data'];
        $kelompok = $this->kelompokApi->sekolah_tanpa_login(session('id_sekolah'));
        $result = $kelompok['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-info btn-sm edit"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.prakerin.' . session('role') . '.v_kelompok')->with(['industri' => $industri, 'tahun' => $tahun]);
    }

    public function list_data()
    {
        // dd(session('role'));
        // dd(session('id_jurusan'));
        session()->put('title', "Kelompok Prakerin");
        if (session('role') == 'user-kaprodi') {
            $kelompok = $this->kelompokApi->by_jurusan(session('id_jurusan'), session('id_tahun_ajar'));
        } else {
            $kelompok = $this->kelompokApi->by_all_jurusan(session('id_tahun_ajar'));
        }
        // dd($kelompok);
        $kelompok = $kelompok['body']['data'];
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        $industri = $industri['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $pembimbing = $this->pembimbingApi->sekolah();
        $pembimbing = $pembimbing['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];

        if (session('role') == 'user-kaprodi') {
            $kelas = $this->kelasApi->get_by_jurusan(session('id_jurusan'));
            $kelas = $kelas['body']['data'];
            return view('content.prakerin.kaprodi.v_kelompok')->with(['kelompok' => $kelompok, 'tahun' => $tahun, 'industri' => $industri, 'pembimbing' => $pembimbing, 'guru' => $guru, 'kelas' => $kelas]);
        } else {
            $jurusan = $this->jurusanApi->get_by_sekolah();
            $jurusan = $jurusan['body']['data'];
            return view('content.prakerin.admin-prakerin.v_kelompok')->with(['kelompoks' => $kelompok, 'tahun' => $tahun, 'industri' => $industri, 'pembimbing' => $pembimbing, 'guru' => $guru, 'jurusan' => $jurusan]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->kelompokApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'id_sekolah' => session('id_sekolah'),
            'id_industri' => Help::decode($request['id_industri']),
            'id_ta_sm' => $request['id_tahun_ajar'],
            'status' => 1
        );
        $result = $this->kelompokApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request['nama'],
            'id_sekolah' => session('id_sekolah'),
            'id_industri' => Help::decode($request['id_industri']),
            'id_ta_sm' => $request['id_tahun_ajar'],
            'status' => 1
        );

        $result = $this->kelompokApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->kelompokApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_siswa(Request $request)
    {
        $delete = $this->siswaApi->soft_delete($request['id']);
        $html = $this->data_kelompok();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $html
            ]);
        }
    }

    public function create_siswa(Request $request)
    {
        // dd($request);

        if (!$request['kelas_siswa']) {
            return response()->json([
                'message' => "Harap masukan siswa terlebih dahulu",
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $this->data_kelompok()
            ]);
        }
        foreach ($request['kelas_siswa'] as $siswa) {
            $data = array(
                'id_kelas_siswa' => $siswa,
                'id_sekolah' => session('id_sekolah'),
                'tgl_mulai' => date('Y-m-d', strtotime($request->tgl_mulai)),
                'tgl_selesai' => date('Y-m-d', strtotime($request->tgl_selesai)),
                'id_prakerin_kelompok' => $request['kelompok'],
                'id_pemb_sekolah' => $request['guru'],
                'id_pemb_industri' => $request['pembimbing'],
                'id_ta_sm' => $request['tahun_ajar'],
                'status' => 1
            );
            $siswa = $this->siswaApi->create(json_encode($data));
        }
        $html = $this->data_kelompok();
        if ($siswa['code'] == 200) {
            return response()->json([
                'message' => $siswa['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'message' => $siswa['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $html
            ]);
        }
    }

    public function edit_waktu_siswa(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
        );
        $update = $this->siswaApi->update_waktu(json_encode($data));
        // dd($update);
        $html = $this->data_kelompok();
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $html
            ]);
        }
    }

    public function by_industri(Request $request)
    {
        // dd($request);
        $kelompok = $this->kelompokApi->get_by_industri($request['id']);
        // dd($kelompok);
        $kelompok = $kelompok['body']['data'];

        return response()->json($kelompok);
    }

    public function cetak($id_kelompok)
    {
        // dd(session()->all());
        $kelompok = $this->kelompokApi->get_by_id(Help::decode(last(request()->segments())));
        // dd($kelompok);
        $kelompok = $kelompok['body']['data'];;
        $kaprodi = $this->kaprodiApi->by_jurusan(session('id_jurusan'));
        // dd($kaprodi);
        $kaprodi = $kaprodi['body']['data'];

        return view('content.prakerin.kaprodi.v_cetak')->with(['kelompok' => $kelompok, 'kaprodi' => $kaprodi]);
    }

    public function list_siswa(Request $request)
    {
        $kelas = $this->kelasApi->get_by_id($request['id_kelas']);
        $kelas = $kelas['body']['data'];
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), $kelas['id_jurusan'], $request['id_kelas'], $request['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                <td>' . $nomer++ . '</td>
                <td>' . ucwords($sw['nama']) . '</td>
                <td>' . $sw['nisn'] . '</td>
                <td>' . $sw['nis'] . '</td>
                <td>' . $sw['rombel'] . '</td>
                <td>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="kelas_siswa[]" value="' . $sw['id'] . '"
                            id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            Pilih
                        </label>
                    </div>
                </td>
            </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="6" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return response()->json($html);
    }



    private function data_kelompok()
    {
        $kelompok = $this->kelompokApi->sekolah_tanpa_login(session('id_sekolah'));
        $kelompok = $kelompok['body']['data'];
        // dd($kelompok);
        $html = '';
        if (empty($kelompok)) {
            $html .= '<tr><td colspan="6" class="text-center">Data Kelompoh, saat ini tidak tersedia</td></tr>';
        } else {
            $nomer = 1;
            foreach ($kelompok as $kl) {
                $html .= '
                <tr>
                    <td>' . $nomer++ . '</td>
                    <td>' . $kl['nama'] . '</td>
                    <td>' . $kl['industri'] . '</td>
                    <td>' . $kl['tahun_ajaran'] . ' ' . $kl['semester'] . '</td>
                    <td>' . $kl['tahun_ajaran'] . ' ' . $kl['semester'] . '</td>
                    <td>
                        <button class="btn btn-success btn-sm" data-toggle="collapse"
                            data-target="#demo' . $kl['id'] . '" class="accordion-toggle"><i
                                class="fas fa-caret-square-down"></i> Lihat anggota</button>
                        <a href="javascript:void(0)" class="btn btn-info btn-sm editKelompok"
                            data-id="' . $kl['id'] . '"><i class="fas fa-edit"></i> Edit</a>
                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i>
                            Hapus</button>
                        <a href="' . route('pkl_kelompok-cetak', Help::encode($kl['id'])) . '" target="_blank" class="btn btn-purple btn-sm"><i
                            class="fas fa-print"></i>
                        Cetak</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" class="hiddenRow">
                        <div class="accordian-body collapse" id="demo' . $kl['id'] . '">
                            <div class="tambah">
                            <div class="tambah">
                            <button class="btn btn-info btn-sm pull-right mb-2"
                                onclick="addAnggota(' . $kl['id'] . ')"><i
                                    class="fas fa-user-plus"></i> Tambah
                                Anggota</button>
                        </div>
                            </div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nomer</th>
                                        <th>Profil</th>
                                        <th class="text-center">Kelas</th>
                                        <th class="text-center">Tanggal Prakerin</th>
                                        <th>Pembimbing</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>';
                $no = 1;
                if (!empty($kl['anggota'])) {
                    foreach ($kl['anggota'] as $ag) {
                        $html .= '<tr>
                                    <td class="text-center">' . $no++ . '</td>
                                    <td class="text-center"><b>' . ucwords($ag['nama']) . '</b> <p class="m-0">NIS/NISN. ' . $ag['nis'] . " / " . $ag['nisn'] . '</p>
                                    <small>Telepon . ' . $ag['telepon'] . '</small></td>
                                    <td class="text-center">' . $ag['rombel'] . '</td>
                                    <td class="text-center">
                                        ' . $ag['tgl_mulai'] . ' sampai ' . $ag['tgl_selesai'] . '
                                        <a href="javascript:void(0)"
                                        class="sw' . $ag['id'] . '"
                                        onclick="editSiswa(' . $ag['id'] . ')">
                                        <i
                                            class="material-icons list-icon md-18 text-info">edit</i>
                                    </a>
                                    </td>
                                    <td class="text-center">' . ucwords($ag['pemb_sekolah']) . '</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)"
                                            onclick="deleteSiswa(' . $ag['id'] . ')">
                                            <i
                                                class="material-icons list-icon md-18 text-danger">delete</i>
                                        </a>
                                    </td>
                                </tr>';
                    }
                } else {
                    $html .= '<tr><td colspan="7" class="text-center">Data siswa untuk saat ini kosong</td></tr>';
                }


                $html .= '</tbody></table></div></td></tr>';
            }
        }
        return $html;
    }

    public function save_multiple(Request $request)
    {
        // dd($request);
        $data_kelompok = array(
            'id_industri' => $request['id_industri'],
            'nama' => $request['nama'],
            'id_ta_sm' => $request['tahun_ajaran'],
            'id_sekolah' => session('id_sekolah'),
        );
        $rk = $this->kelompokApi->create(json_encode($data_kelompok));
        // dd($rk);
        if ($rk['code'] == 200) {
            if (!$request['kelas_siswa']) {
                return response()->json([
                    'message' => "Harap masukan siswa terlebih dahulu",
                    'icon'  => 'error',
                    'status'  => 'gagal',
                    'html' => $this->data_kelompok()
                ]);
            }
            foreach ($request['kelas_siswa'] as $siswa) {
                $data = array(
                    'id_kelas_siswa' => $siswa,
                    'id_sekolah' => session('id_sekolah'),
                    'tgl_mulai' => date('Y-m-d', strtotime($request->tgl_mulai)),
                    'tgl_selesai' => date('Y-m-d', strtotime($request->tgl_selesai)),
                    'id_prakerin_kelompok' => $rk['body']['data']['id'],
                    'id_pemb_sekolah' => $request['guru'],
                    'id_pemb_industri' => $request['pembimbing'],
                    'id_ta_sm' => $request['tahun_ajaran'],
                );
                $result = $this->siswaApi->create(json_encode($data));
            }
            if ($result['code'] == 200) {
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'success',
                    'status'  => 'berhasil',
                ]);
            } else {
                return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status'  => 'gagal',
                ]);
            }
            // dd($result);
        } else {
            return response()->json([
                'success' => $rk['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function get_anggota(Request $request)
    {
        if (request()->ajax()) {
            $siswa = $this->siswaApi->get_by_kelompok($request['id_kelompok']);
            // dd($siswa);
            $result = $siswa['body']['data'];
            $table = datatables()->of($result)
                ->addColumn('tanggal', function ($data) {
                    return Help::getTanggal($data['tgl_mulai']) . " - " . Help::getTanggal($data['tgl_selesai']);
                });
            $table->rawColumns(['tanggal']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }
}
