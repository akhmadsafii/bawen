<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Prakerin\JenisNilaiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Foreach_;

class JenisNilaiController extends Controller
{
    private $jurusanApi;
    private $jenisNilaiApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->jenisNilaiApi = new JenisNilaiApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Jenis Nilai');
        $jenis = $this->jenisNilaiApi->get_by_group();
        // dd($jenis);
        if ($jenis['code'] != 200) {
            $pesan = array(
                'message' => $jenis['body']['message'],
                'icon' => 'error',
                'status' => 'gagal'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jenis['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-info btn-sm edit" data-nama="' . $data['nama_jenis'] . '"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-nama="' . $data['nama_jenis'] . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    return $button;
                });
            $table->editColumn('jumlah', function ($row) {
                $jumlah = '<p class="text-danger my-0">Tidak ada jurusan</p>';
                if ($row['jumlah'] != 0) {
                    $jumlah = '<p class="text-info my-0">' . $row['jumlah'] . ' Jurusan</p>';
                }
                return $jumlah;
            });
            $table->rawColumns(['action', 'jumlah']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        $jurusan = $this->jurusanApi->get_by_sekolah(session('id_sekolah'));
        $jurusan = $jurusan['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.jenis_nilai.v_jenis_nilai')->with(['template' => $template, 'jurusan' => $jurusan]);
    }

    public function store(Request $request)
    {
        // dd($request);
        if (empty($request['id_jurusan']) || empty($request['nama'])) {
            return response()->json([
                'message' => "Harap masukan jurusan dan nama jenis terlebih dulu",
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
        $id_jurusan = implode(',', $request['id_jurusan']);
        $nama = implode(',', $request['nama']);
        $data = array(
            'nama' => $nama,
            'id_sekolah' => session('id_sekolah'),
            'id_jurusan' => $id_jurusan,
            'status' => 1
        );
        $result =  $this->jenisNilaiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $jenis = $this->jenisNilaiApi->search_by_name(json_encode(['jenis' => $request['old_name']]));
        $jenis = $jenis['body']['data'];
        if (!empty($jenis)) {
            foreach ($jenis as $jn) {
                $delete = $this->jenisNilaiApi->delete($jn['id']);
                // dd($delete);
            }
        }
        $id_jurusan = implode(',', $request['id_jurusan']);
        $data = array(
            'nama' => $request['nama'][0],
            'id_sekolah' => session('id_sekolah'),
            'id_jurusan' => $id_jurusan,
            'status' => 1
        );
        $result =  $this->jenisNilaiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->jenisNilaiApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }


    public function delete(Request $request)
    {
        $jenis = $this->jenisNilaiApi->search_by_name(json_encode(['jenis' => $request['nama']]));
        $jenis = $jenis['body']['data'];
        if (!empty($jenis)) {
            foreach ($jenis as $jn) {
                $delete = $this->jenisNilaiApi->soft_delete($jn['id']);
                // dd($delete);
            }
        }
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function by_name(Request $request)
    {
        $jenis = $this->jenisNilaiApi->search_by_name(json_encode(['jenis' => $request['nama']]));
        $jenis = $jenis['body']['data'];
        return response()->json($jenis);
    }

    public function get_by_jurusan(Request $request)
    {
        // dd($request);
        $jenis = $this->jenisNilaiApi->get_by_jurusan($request['id']);
        $jenis = $jenis['body']['data'];
        return response()->json($jenis);
    }
}
