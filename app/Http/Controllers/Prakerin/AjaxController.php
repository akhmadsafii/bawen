<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\NilaiSiswaApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\PembimbingSertifikatApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\Prakerin\TemplateSertifikatApi;
use App\ApiService\Prakerin\TugasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    private $pembimbingIndustriApi;
    private $tugasApi;
    private $siswaApi;
    private $templateApi;
    private $nilaiSiswaApi;
    private $pembimbingSertifikatApi;
    private $hash;

    public function __construct()
    {
        $this->tugasApi = new TugasApi();
        $this->siswaApi = new SiswaPrakerinApi();
        $this->nilaiSiswaApi = new NilaiSiswaApi();
        $this->pembimbingIndustriApi = new PembimbingIndustriApi();
        $this->pembimbingSertifikatApi = new PembimbingSertifikatApi();
        $this->templateApi = new TemplateSertifikatApi;
        $this->hash = new Hashids();
    }

    public function siswa_lihat_nilai(Request $request)
    {
        // dd($request);
        $siswa = $this->siswaApi->get_by_id($request['id']);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        if ($request['params'] == 'nilai') {
            $cetak = $this->nilaiSiswaApi->cetak_nilai($siswa['id'], $siswa['id_jurusan']);
            $cetak = $cetak['body']['data'];
            $html = '
                <div class="">
                    <a href="/program/sistem_pkl/nilai_siswa/cetak/' . $this->hash->encode($siswa['id']) . '" target="_blank" class="pull-right"
                        style="position: absolute; right: 0; top: 0;"><i class="fa fa-print"
                            style="font-size: 33px;"></i></a>
                </div>
                <table style="margin-bottom: 12px">
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;&nbsp;: ' . ucwords($cetak['nama']) . '</td>
                    </tr>
                    <tr>
                        <td>NISN</td>
                        <td>&nbsp;&nbsp;&nbsp;: ' . $cetak['nisn'] . '</td>
                    </tr>
                    <tr>
                        <td>Tingkat/ Program Keahlian</td>
                        <td>&nbsp;&nbsp;&nbsp;:
                            ' . Help::getKelas($cetak['kelas']) . ' / ' . ucwords($cetak['jurusan']) . '
                        </td>
                    </tr>
                    <tr>
                        <td>Nama dunia usaha / Dunia Industri</td>
                        <td>&nbsp;&nbsp;&nbsp;: AA Komputer</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>&nbsp;&nbsp;&nbsp;: AA Komputer</td>
                    </tr>
                    <tr>
                        <td>Waktu Pelaksanaan</td>
                        <td>&nbsp;&nbsp;&nbsp;:
                            ' . Help::getDayMonth($cetak['tgl_mulai']) . ' - ' . (new \App\Helpers\Help())->getTanggal($cetak['tgl_selesai']) . '
                        </td>
                    </tr>
                </table>';
            $numb = 1;
            foreach ($cetak['nilai'] as $jenil) {
                $html .=
                    '
                    <table class="table table-bordered" style="margin-bottom: 12px">
                    <tr>
                        <th colspan="3"><b>' . Help::getKelas($numb++) . '. ' . $jenil['jenis_nilai'] . '</b></th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Aspek yang dinilai</th>
                        <th>Nilai</th>
                    </tr>
                    ';
                $no = 1;
                $total = 0;
                foreach ($jenil['nilai'] as $nl) {
                    $html .= '<tr>
                        <td>' . $no++ . '.</td>
                        <td>' . ucfirst($nl['nama']) . '</td>
                        <td>' . $nl['nilai_siswa'] . '</td>
                    </tr>';
                    $total += $nl['nilai_siswa'];
                }
                // dd($jenil['nilai']);
                if (empty($jenil['nilai'])) {
                    $rata = $total/1;
                } else {
                    $rata = $total / count($jenil['nilai']);
                }
                $html .= '
                    <tr>
                    <td colspan="2" style="text-align: center"><b>Nilai Rata-rata</b></td>
                                <td><b>' . number_format($rata, 0, '.', ',') . '</b></td>
                            </tr>
                        </table>
                    ';
            }
            $html .= '
                <br><br>
                    <div style=" display: inline; float: left;">
                        Keterangan Penilaian: <br>
                        Nilai yang diburuhkan adalah nilai angka, dengan rentang sebagai berikut <br>
                        <table class="table table-bordered" style="width: 50%">
                            <tr>
                                <td>A</td>
                                <td>90 - 100</td>
                            </tr>
                            <tr>
                                <td>B</td>
                                <td>75 - 89</td>
                            </tr>
                            <tr>
                                <td>C</td>
                                <td>60 - 74</td>
                            </tr>
                            <tr>
                                <td>D</td>
                                <td>0 - 59</td>
                            </tr>
                        </table>
                    </div>
                ';

            return response()->json($html);
        } else {
            $sertifikat = $this->pembimbingSertifikatApi->get_by_pemb_industri($siswa['id_pemb_industri']);
            // dd($sertifikat);
            $sertifikat = $sertifikat['body']['data'];
            if ($sertifikat != null) {
                $template = $this->templateApi->get_by_id($sertifikat['id_template']);
                // dd($template);
                $template = $template['body']['data'];
                $html = '
                <div class="">
                    <a href="/program/sistem_pkl/nilai_siswa/sertifikat/' . $this->hash->encode($siswa['id']) . '" target="_blank" class="pull-right"
                        style="position: absolute; right: 0; top: 0;"><i class="fa fa-print"
                            style="font-size: 33px;"></i></a>
                </div>
                <style>' . $template['sourcode'] . '</style>
                <div class="sertifikat">
                    <div class="wadah_sertifikat">
                        <span class="title_sertifikat">Sertifikat PRAKERIND</span>
                        <br><br>
                        <span class="bidang_sertifikat"><i>This is to certify that</i></span>
                        <br><br>
                        <span class="siswa_sertifikat"><b>' . ucwords($siswa['nama']) . '</b></span><br /><br />
                        <span class="sertifikat_selesai"><i>telah menyelesaikan prakerind di</i></span> <br /><br />
                        <span class="kursus_sertifikat">' . $siswa['industri'] . '</span> <br /><br />
                        <span class="skore_sertifikat">dengan rata-rata <b>90</b></span>
                        <br /><br /><br /><br />
                        <span class="tgl_ser"><i>tanggal</i></span><br>
                        <span class="tgl_sertifikat">' . Help::getTanggal(date('Y-m-d H:i:s')) . '</span>
                        <br /><br /><br /><br />
                    </div>
                </div>
                ';
            } else {
                $html  = '<h4>Maaf pembimbing belum mengeset template sertifikat</h4>';
            }
            return response()->json($html);
        }
    }

    public function siswa_lihat_tugas(Request $request)
    {
        $siswa = $this->siswaApi->get_by_id($request['id']);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $pembimbing = $this->pembimbingIndustriApi->get_by_id($siswa['id_pemb_industri']);
        $pembimbing = $pembimbing['body']['data'];
        $paraf = $pembimbing['paraf'];
        $agenda = $this->tugasApi->cetak_kegiatan($request['id']);
        $agenda = $agenda['body']['data'];
        $tugas = [];
        foreach ($agenda as $tg) {
            $tugas[] = array(
                'waktu' => $tg['waktu'],
                'id_prakerin_siswa' => !empty($tg['id_prakerin_siswa']) ? $tg['id_prakerin_siswa'] : null,
                'nama' => !empty($tg['nama']) ? $tg['nama'] : null,
                'status' => !empty($tg['status']) ? $tg['status'] : null,
                'id' => !empty($tg['id']) ? $tg['id'] : null,
                'catatan' => !empty($tg['catatan']) ? $tg['catatan'] : null,
                'id_sekolah' => !empty($tg['catatan']) ? $tg['id_sekolah'] : null,
                'id_ta_sm' => !empty($tg['catatan']) ? $tg['id_ta_sm'] : null,

            );
        }
        $html = '
        <div class="">
            <a href="/program/sistem_pkl/tugas/cetak/' . $this->hash->encode($siswa['id']) . '" target="_blank" class="pull-right"
                style="position: absolute; right: 0; top: 0;"><i class="fa fa-print"
                    style="font-size: 33px;"></i></a>
        </div>
        <table style="margin-bottom: 12px">
        <tr>
            <td>Nama</td>
            <td>&nbsp;&nbsp;&nbsp;: ' . ucwords($siswa['nama']) . '</td>
        </tr>
        <tr>
            <td>NISN</td>
            <td>&nbsp;&nbsp;&nbsp;: ' . $siswa['nisn'] . '</td>
        </tr>
        <tr>
            <td>Tingkat/ Program Keahlian</td>
            <td>&nbsp;&nbsp;&nbsp;:
                ' . Help::getKelas($siswa['kelas']) . '/' . ucwords($siswa['jurusan']) . '
            </td>
        </tr>
        <tr>
            <td>Nama dunia usaha / Dunia Industri</td>
            <td>&nbsp;&nbsp;&nbsp;: ' . $siswa['industri'] . '</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>&nbsp;&nbsp;&nbsp;: ' . $siswa['alamat_industri'] . '</td>
        </tr>
        <tr>
            <td>Waktu Pelaksanaan</td>
            <td>&nbsp;&nbsp;&nbsp;:
                ' . Help::getDayMonth($siswa['tgl_mulai']) . ' - ' . Help::getTanggal($siswa['tgl_selesai']) . '
            </td>
        </tr>
        </table>
        <table class="table" style="width: 96%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Hari, Tanggal</th>
                    <th>Kegiatan</th>
                    <th>Paraf</th>
                </tr>
            </thead>
            <tbody>
            ';
        $no = 1;
        foreach ($tugas as $tg) {
            if ($tg['nama'] == null) {
                $html .= '
                <tr style="background: #f8a6a6">
                    <td style="color: blue">' . $no++ . '</td>
                    <td style="color: blue">' . Help::getDay($tg['waktu']) . '</td>
                    <td style="color: blue">Tidak ada kegiatan siswa yang diinput</td>
                    <td>
                    </td>
                </tr>
                ';
            } else {
                $par = '';
                if ($tg['status'] == 4) {
                    $par = '<img src="' . $paraf . '" alt="" style="height: 29px;">';
                }
                $html .= '
                <tr>
                    <td style="vertical-align: middle">' . $no++ . '</td>
                    <td style="vertical-align: middle">' . Help::getDay($tg['waktu']) . '</td>
                    <td style="vertical-align: middle">' . $tg['nama'] . '</td>
                    <td style="text-align: center; vertical-align: middle">
                    ' . $par . '
                    </td>
                <tr>
                ';
            }
        }
        $html .= '
            </tbody></table>
            ';
        return response()->json($html);
    }
}
