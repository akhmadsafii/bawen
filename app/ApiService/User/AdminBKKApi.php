<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class AdminBKKApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function update($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin/info";
        return ApiService::request($url, "POST", $body);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/admin/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/admin/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }



    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/admin/profile";
        return ApiService::request($url, "GET", null);
    }

    public function get_id_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/admin/profile/get/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/siswas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin/update_siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/bkk/admin/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_ajar($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/bkk/admin/kelas/admin/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/bkk/admin/file/template/import-admin";
        return $url;
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/admin/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/admin/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function delete_profile($id_admin)
    {
        $url = $this->urlApi . "api/data/bkk/admin/delete/profile/" . $id_admin;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/bkk/admin/update/foto/profile";
        return ApiService::request_image($url, "POST", $body);
    }
}
