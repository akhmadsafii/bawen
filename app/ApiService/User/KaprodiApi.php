<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class KaprodiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/kaprodi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/kaprodi";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/kaprodi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function by_jurusan($id_jurusan)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/list/kelompok/jurusan/" . $id_jurusan;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        
        $url = $this->urlApi . "api/data/user/kaprodi/profile";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/info";
        return ApiService::request($url, "PUT", $body);
    }   

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/kaprodi/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_ajar($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/kelas/kaprodi/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }
    
    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function import()
    {
        $url = $this->urlApi . "api/data/user/kaprodi/file/template/import-kaprodi";
        return $url;
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/kaprodi/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/kaprodi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function import_kaprodi()
    {
        $url = $this->urlApi . "api/data/user/kaprodi/file/excel-import";
        return $url;
    }
    
}
