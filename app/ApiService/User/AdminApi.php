<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class AdminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/admin";
        // dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/admin/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/user/admin/profile/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/admins/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/admin/update_admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/user/admin/update_status";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/admin/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/admin/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
