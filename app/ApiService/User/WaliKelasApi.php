<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class WaliKelasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/wali_kelas";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($tahun)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/profile/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_paraf($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/update/paraf/only";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/wali_kelass/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/update_wali_kelas";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/data/by/rombel/" . $id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function by_rombel_tahun($id_rombel, $tahun)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/filter/rombel/".$id_rombel."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/file/excel-example/" . $id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/wali_kelas/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
