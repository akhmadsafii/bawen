<?php

namespace App\ApiService\SuperMaster;

use App\Helpers\ApiService;

class ProgramApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/program/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/program/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/program/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/programs/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/program/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/program/update_program";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/master/program/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/program/trash/admin";
        return ApiService::request($url, "GET", null);
    }
}
