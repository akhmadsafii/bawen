<?php

namespace App\ApiService\SuperMaster;

use App\Helpers\ApiService;

class DatabaseApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/backup";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create()
    {
        $url = $this->urlApi . "api/backup/create";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function delete($nama_file)
    {
        $url = $this->urlApi . "api/backup/delete/" . $nama_file;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}
