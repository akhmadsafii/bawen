<?php

namespace App\ApiService\Perpus\Master;

use App\Helpers\ApiService;

class JenisApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/jenis";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/jenis/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/jenis/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/jenis";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function search_judul($body){
        $url = $this->urlApi . "api/data/perpus/jenis/search_default";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/jenis/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/jenis/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/jenis/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>