<?php

namespace App\ApiService\Perpus\Pengumuman;

use App\Helpers\ApiService;

class pengumumanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function getAll(){
    	$url = $this->urlApi . "api/data/perpus/pengumuman";
        return ApiService::request($url, "GET", null);
    }

    public function getOne($id){
    	$url = $this->urlApi . "api/data/perpus/pengumuman/detail/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function store($body){
    	$url = $this->urlApi . "api/data/perpus/pengumuman";
        return ApiService::request($url, "POST", $body);
    }

     public function delete($id){
    	$url = $this->urlApi . "api/data/perpus/pengumuman/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

}