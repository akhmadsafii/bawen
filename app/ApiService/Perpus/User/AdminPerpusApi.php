<?php

namespace App\ApiService\Perpus\User;

use App\Helpers\ApiService;

class AdminPerpusApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/perpus/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/perpus/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/perpus/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/perpus/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/perpus/admin/update";
        return ApiService::request($url, "POST", $body);
    } 
 
    public function delete($id)
    {
        $url = $this->urlApi . "api/data/admin/perpus/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

     public function restore($id)
    {
        $url = $this->urlApi . "api/data/perpus/admin/restore/" . $id;
        return ApiService::request($url, "POST", $body);
    }
    
}
