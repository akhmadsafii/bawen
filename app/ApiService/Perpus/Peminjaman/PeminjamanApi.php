<?php

namespace App\ApiService\Perpus\Peminjaman;

use App\Helpers\ApiService;

class PeminjamanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/peminjaman";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_dipinjam(){
        $url = $this->urlApi . "api/data/perpus/peminjaman/get/active";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_kembali(){
        $url = $this->urlApi . "api/data/perpus/peminjaman/get/returned";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function pengembalian($id){
        $url = $this->urlApi . "api/data/perpus/peminjaman/return/" . $id;
        // dd($url);
        return ApiService::request($url, "POST", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/peminjaman/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_user($id){
        $url = $this->urlApi . "api/data/perpus/peminjaman/user/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_book($id){
        $url = $this->urlApi . "api/data/perpus/peminjaman/book/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/peminjaman/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function extend($id)
    {
        $url = $this->urlApi . "api/data/perpus/peminjaman/extend/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/peminjaman";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . "api/data/perpus/peminjaman/info/". $id;
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/peminjaman/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/peminjaman/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>
