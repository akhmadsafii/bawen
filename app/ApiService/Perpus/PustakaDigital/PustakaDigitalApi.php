<?php

namespace App\ApiService\Perpus\PustakaDigital;

use App\Helpers\ApiService;

class PustakaDigitalApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/pusdig";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_home(){
        $url = $this->urlApi . "api/data/perpus/pusdig/home/page";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_home_all(){
        $url = $this->urlApi . "api/data/perpus/pusdig/home/all";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/pusdig/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/pusdig/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/pusdig";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/perpus/pusdig";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/pusdig/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body){
        $url = $this->urlApi . "api/data/perpus/pusdig/update";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function search_judul($body){
        $url = $this->urlApi . "api/data/perpus/pusdig/search_default";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

     public function search_advance($body){
        $url = $this->urlApi . "api/data/perpus/pusdig/search_advance";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/pusdig/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/pusdig/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>
