<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class KalenderWaliKelasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_acara($start, $end, $id_rombel)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/" . $start . "/" . $end . "/" . $id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }


    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombels/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/kalender_rombel/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
