<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class JawabanpengetahuanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/profile";
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_tugas($id_tugas)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/tugas/file/". $id_tugas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/update_guru";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tugas_siswa($id_tugas)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/tugas/siswa/".$id_tugas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function save_nilai($body)
    {
        $url = $this->urlApi . "api/data/learning/jawaban_pengetahuan/save/nilai";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
}
