<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class JurnalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_room($id_mapel, $id_kelas, $id_rombel, $id_ta)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/mapel/" . $id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/tahun/".$id_ta;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajars/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/jurnal_mengajar/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
