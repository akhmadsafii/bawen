<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class TugasPengetahuanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_mapel_rombel($id_rombel, $id_mapel)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/wali-kelas/tugas/mapel/".$id_mapel."/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/info";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_room_by_siswa($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_pengetahuan/siswa/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
