<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class PertemuanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/pertemuan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function by_room($id_room)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/by_room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_nama_jenis($nama)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/by_nama_jenis/" . $nama;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/pertemuans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/pertemuans/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/pertemuan/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
