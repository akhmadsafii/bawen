<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class KompetensiDasarApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_inti($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/kompetensi_inti/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function by_room($id_room)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/by_room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_mapel_kelas($id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/mapel/".$id_mapel."/kelas/".$id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_mapel_kelas_jenis($id_mapel, $id_kelas, $jenis)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/mapel/".$id_mapel."/kelas/".$id_kelas."/jenis/".$jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_nama_jenis($nama)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/by_nama_jenis/" . $nama;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasars/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi_dasar/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
