<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class DataSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/data_siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/room/" . $id_room. "/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_pertemuan($body)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/get/by_pertemuan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_room_last_kelas($id_room, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/last/room/" . $id_room. "/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rooms($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/absen-siswa/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function rooms($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room_last($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/room_last/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_absensi($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/absen-siswa/room/" . $id_room;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_absensi_pertemuan($id_room, $pertemuan)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/absen-siswa/room/".$id_room."/pertemuan/".$pertemuan;
        return ApiService::request($url, "GET", null);
    }

    public function get_loop_pertemuan($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/list_pertemuan/" . $id_room;
        return ApiService::request($url, "GET", null);
    }

    public function get_last_pertemuan($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/last_pertemuan/" . $id_room;
        return ApiService::request($url, "GET", null);
    }

    public function update_status_absen($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/siswa/tergabung/room/" . $id_room;
        return ApiService::request($url, "PUT", null);
    }

    public function get_by_pertemuan($id, $id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/pertemuan/" . $id . "/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_absensi($id_mapel, $id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/absen/mapel/".$id_mapel."/rombel/".$id_rombel."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/data_siswas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/data_siswas/info";
        return ApiService::request($url, "PUT", $body);
    }
    
    public function update_absensi($body)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/update/status/absensi";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    public function filter_date($id_room, $tgl_mulai, $tgl_selesai, $pertemuan)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/filter_date/".$id_room."/".$tgl_mulai."/akhir/".$tgl_selesai."/pertemuan/".$pertemuan;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_mapel($id_mapel, $id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/mapel/".$id_mapel."/rombel/".$id_rombel."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail_absensi($id_kelas_siswa, $id_mapel, $id_rombel, $id_ta)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/siswa/".$id_kelas_siswa."/mapel/".$id_mapel."/rombel/".$id_rombel."/tahun/".$id_ta;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
