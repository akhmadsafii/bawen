<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class SettingApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/config";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/config";
        return ApiService::request($url, "POST", $body);
    }
   
    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/config";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create_file1($body)
    {
        $url = $this->urlApi . "api/data/learning/config";
        return ApiService::request_image1($url, "POST", $body);
    }
   
    public function create_files($body)
    {
        $url = $this->urlApi . "api/data/learning/config";
        return ApiService::request_images($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/config/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/config/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/config/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($id)
    {
        $url = $this->urlApi . "api/data/learning/config/sekolah/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/config/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/config/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/config/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/config/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_loker_pelamar($id_loker, $id_pelamar)
    {
        $url = $this->urlApi . "api/data/learning/config/loker/".$id_loker."/pelamar/".$id_pelamar;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_loker($id_loker)
    {
        $url = $this->urlApi . "api/data/learning/config/loker/".$id_loker;
        return ApiService::request($url, "GET", null);
    }
}
