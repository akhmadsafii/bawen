<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class RoomApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/room";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_siswa($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/learning/room/siswa/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_delete_guru_mapel($id_guru, $id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/room/guru/".$id_guru."/mapel/".$id_mapel."/kelas/".$id_kelas;
        return ApiService::request($url, "GET", null);
    }

    public function get_guru($id_tahun)
    {
        $url = $this->urlApi . "api/data/learning/room/guru/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_last_pertemuan($id_room)
    {
        $url = $this->urlApi . "api/data/learning/data_siswa/pertemuan/room/". $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/room";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/room";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/learning/room/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function room_lengkap($id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/room/get/jurusan/and/kelas/" . $id_kelas;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/room/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/room/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/room/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/rooms/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_aktif()
    {
        $url = $this->urlApi . "api/data/learning/room/show/room/sekolah/aktif";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/room/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/room/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function aktifasi($body)
    {
        $url = $this->urlApi . "api/data/learning/room/proses/aktifasi/room";
        return ApiService::request($url, "POST", $body);
    }


    public function update_virtual($id_room, $body)
    {
        $url = $this->urlApi . "api/data/learning/room/virtual/room/".$id_room;
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/room/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/room/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_status_nonAktif($id)
    {
        $url = $this->urlApi . "api/data/learning/room/update/".$id;
        // dd($url);
        return ApiService::request($url, "PUT", null);
    }

    public function update_status_custom($body)
    {
        $url = $this->urlApi . "api/data/learning/room/update/status/on/off";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_rombel_by_kelas($id_kelas)
    {
        $url = $this->urlApi . "api/data/master/rombel/get/jurusan/and/kelas/".$id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/learning/room/detail/room/by_rombel/get_by_id/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_rombel_ta_sm($id_rombel, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/learning/room/rombel/".$id_rombel."/tahun/id_ta_sm/".$id_ta_sm;
        return ApiService::request($url, "GET", null);
    }
}
