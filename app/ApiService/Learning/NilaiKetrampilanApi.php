<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class NilaiKetrampilanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_room($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan";
        return ApiService::request($url, "POST", $body);
    }
    
    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/info";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/nilai_ketrampilan/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
