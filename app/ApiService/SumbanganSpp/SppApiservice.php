<?php

namespace App\ApiService\SumbanganSpp;

use App\Helpers\ApiService;

class SppApiservice
{
    /**
     * variabel global
     */
    public $urlApi;

    /**
     * __construct function with variable global for url domain
     */

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /** get profil  */
    public function get_info_profil()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /** profil setting sekolah template spp */
    public function setting_template()
    {
        $url = $this->urlApi . "api/data/spp/tagihan/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get tahun ajaran  */
    public function get_tahun_ajaran()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/sekolah/tahun_ajaran";
        return ApiService::request($url, "GET", null);
    }

    /** get data kelas  */
    public function get_data_kelas()
    {
        $url = $this->urlApi . "api/data/master/kelas";
        return ApiService::request($url, "GET", null);
    }

    /**akun pos */

    public function get_data_posx()
    {
        $url = $this->urlApi . "api/data/spp/pos";
        return ApiService::request($url, "GET", null);
    }

    /** get data pos pemasukan  */
    public function get_data_pos_pemasukan($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos/sekolah/tahun_ajaran/" . $tahun . "";
        return ApiService::request($url, "GET", null);
    }
    //pos by id
    public function get_data_pos_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos/" . $id;
        return ApiService::request($url, "GET", null);
    }

    // simpan pos
    public function post_data_pos($body)
    {
        $url = $this->urlApi . "api/data/spp/pos";
        return ApiService::request($url, "POST", $body);
    }

    // update pos
    public function update_data_pos($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos/" . $id;
        return ApiService::request($url, "PUT", $body);
    }



    //remove pos by id
    public function remove_pos_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    //remove pos by id
    public function remove_pos_pengeluaran_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get data detail pos by id  */

    public function get_data_pos_pemasukan_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/pos/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get pos pemasukan */
    public function get_data_pos_pemasukanx_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/pos/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_data_pos_pemasukanx($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/sekolah/tahun_ajaran/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    // update pos
    public function update_data_pos_pemasukan($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    // update pos
    public function update_data_pos_pengeluaran($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    // update pos target
    public function update_data_pos_pengeluaran_target($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** edit pos target  */
    public function get_data_pos_pemasukan_byidx($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/" . $id;
        return ApiService::request($url, "GET", null);
    }


    /** edit pos target  */
    public function get_data_pos_pengeluaran_byidx($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran/" . $id;
        return ApiService::request($url, "GET", null);
    }


    // update pos real
    public function update_data_pos_pemasukanreal($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/pos/realisasi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    // update pos real
    public function update_data_pos_pengeluaranreal($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran/pos/realisasi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    // post pos
    public function post_data_pos_pemasukan($body)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan";
        return ApiService::request($url, "POST", $body);
    }

    // post pos
    public function post_data_pos_pengeluaran($body)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran";
        return ApiService::request($url, "POST", $body);
    }

    // post pos realisasi pengeluaran
    public function post_data_pos_pengeluaran_realisasi($body)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran";
        return ApiService::request($url, "POST", $body);
    }

    //remove pos pemasukan

    public function remove_pos_pemasukan($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    //remove pos pengeluaran

    public function remove_pos_pengeluaran($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** get data pos pemasukan  */
    public function post_tagihan($body)
    {
        $url = $this->urlApi . "api/data/spp/tagihan";
        return ApiService::request($url, "POST", $body);
    }

    /** get data show tagihan by id   */
    public function show_tagihan($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data show tagihan by id   */
    public function show_tagihan_tahun($tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/sekolah/tahun_ajaran/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data show tagihan by id   */
    public function show_tagihan_siswa_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data show tagihan by id   */
    public function update_tagihan($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** remove tagihan setting */
    public function remove_tagihan($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** setting spp */
    public function setting_spp($id)
    {
        $url = $this->urlApi . "api/data/spp/setting/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** setting spp */
    public function rekeningspp($id)
    {
        $url = $this->urlApi . "api/data/spp/setting/sekolah/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** update rekening spp */
    public function update_spp($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/setting/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** get template setting pesan  */
    public function setting_pesan()
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** detail template setting pesan by id */
    public function setting_pesan_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** remove template setting pesan  */
    public function setting_pesan_byremoveid($id)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** post template setting pesan  */
    public function setting_pesan_post($body)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** get data siswa */
    public function siswaApi($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/profile/paginate/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get siswa by  id  */

    public function siswaApibyid($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa/get_by_siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/nisn/" . $id . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa_bybukuinduk($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/nisn/" . $id . "/tahun/" . $tahun . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa_by_kelas($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun . "/kelas/siswa/" . $id . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa_ortu($id)
    {
        $url = $this->urlApi . "api/data/user/ortu/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan years */
    public function search_tagihan($tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/sekolah/tahun_ajaran/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan bulan ini */
    public function search_tagihan_by_bulan($id, $bulan)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id . "/bulan/" . $bulan;
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan years and nisn */
    public function search_tagihan_by_nisn($nis, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/siswa/nis/" . $nis . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data siswa */
    public function kelasApi()
    {
        $url = $this->urlApi . "api/data/master/kelas/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** get data siswa */
    public function jurusanApi()
    {
        $url = $this->urlApi . "api/data/master/jurusan/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** get tagihan id siswa  */
    public function tagihan_siswa_by_kelas_id($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** tagihan bulan  */
    public function tagihan_siswa_bulan($id, $nama_bulan)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id . "/bulan/" . $nama_bulan . "";
        return ApiService::request($url, "GET", NULL);
    }


    /** TRANSAKSI    */
    public function transaksi_tagihan($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi";
        return ApiService::request($url, "POST", $body);
    }

    /** TRANSAKSI  detail   */
    public function transaksi_tagihan_detail($body)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi";
        return ApiService::request($url, "POST", $body);
    }

    /** get data history transaksi pembayaran spp  */
    public function get_transaksi()
    {
        $url = $this->urlApi . "api/data/spp/transaksi/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** get data history transaksi jurnal  */
    public function get_transaksi_jurnal()
    {
        $url = $this->urlApi . "api/data/spp/transaksi/sekolah/profile/jurnal";
        return ApiService::request($url, "GET", NULL);
    }

    /** get data history transaksi all  */
    public function get_transaksi_all()
    {
        $url = $this->urlApi . "api/data/spp/transaksi/sekolah/profile/all";
        return ApiService::request($url, "GET", NULL);
    }

    /** update data TRANSAKSI siswa  */
    public function update_transaksi($id, $body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** delete data TRANSAKSI siswa  */
    public function delete_transaksi($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** transaksi by id */
    public function get_transaksi_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** transaksi by id */
    public function get_transaksi_siswa_id($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** detail transaksi by code */
    public function get_detail_transaksi_bycode($id)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi/transaksi/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** keringanan spp */
    public function get_keringan_spp()
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan";
        return ApiService::request($url, "GET", NULL);
    }

    /** show keringan spp */
    public function get_keringan_spp_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** konfirmasi pembayaran spp */
    public function get_konfirmasi_spp()
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** konfirmasi pembayaran spp lunas */
    public function get_konfirmasi_spp_paid()
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/sekolah/profile/paid";
        return ApiService::request($url, "GET", NULL);
    }

    /** post keringanan  */
    public function post_keringananspp($body)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan";
        return ApiService::request($url, "POST", $body);
    }

    /** update keringanan  */
    public function update_keringananspp($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    //pos by id
    public function get_data_pos()
    {
        $url = $this->urlApi . "api/data/spp/pos/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    //download export file
    public function get_data_filepos()
    {
        $url = $this->urlApi . "api/data/spp/pos/file/excel-example";
        return ApiService::request($url, "GET", null);
    }

    //get report
    public function report_tagihan($id, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/profile/piutang/rombel/" . $id . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** rombel get  */
    public function rombel_data()
    {
        $url = $this->urlApi . "api/data/master/rombel/profile";
        return ApiService::request($url, "GET", null);
    }

    /** transaksi konfirmasi post */
    public function post_upload_konfirm($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/bukti/konfirmasi";
        return ApiService::request($url, "POST", $body);
    }

    /** transaksi konfirmasi post */
    public function post_upload_konfirmfile($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/bukti/konfirmasi";
        return ApiService::request_image($url, "POST", $body);
    }

    /** transaksi konfirmasi post */
    public function post_store_siswa_konfirm($id_kelas_siswa, $bulan, $id_pesan)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/siswa/" . $id_kelas_siswa . "/bulan/" . $bulan . "/pesan/" . $id_pesan;
        return ApiService::request($url, "POST", null);
    }

    /** create tagihan online  */
    public function create_transconfirm($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi";
        return ApiService::request($url, "POST", $body);
    }

    /** get confirmasi by code  */
    public function get_transconfirm($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/kode/" . $id . "/tagihan";
        return ApiService::request($url, "GET", null);
    }

    /** change Metode bayar  */
    public function change_metode($kode, $metode)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/kode/" . $kode . "/metode/" . $metode;
        return ApiService::request($url, "PUT", NULL);
    }

    /** link url pembayaran  */
    public function paid_spp_konfirmasi($via,$kode){
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/paid/via/".$via."/kode_vendor/".$kode;
        return ApiService::request($url, "GET", NULL);
    }

    /** show detail konfirmasi */

    public function get_transconfirm_detail($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** show detail konfirmasi */

    public function get_transconfirm_detail_bycode($id)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi/transaksi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** delete konfirmasi pembayaran */
    public function delete_konfirmasi_pembayaran($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** approve konfirmasi pembayaran */
    public function approve_konfirmasi_pembayaran($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/delete";
        return ApiService::request($url, "POST", $body);
    }

    /** reject pembayaran */
    public function reject_konfirmasi_pembayaran($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }


    /** get statsus order pembayaran midtrans  */

    public function get_cek_statusOrder($id, $idx)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/order/" . $id . "/status/sekolah/" . $idx;
        return ApiService::request($url, "GET", null);
    }

    /** forget password  */
    public function forget_pass($body)
    {
        $url = $this->urlApi . "api/forgot-password/email";
        return ApiService::request($url, "POST", $body);
    }

    /** get Token password */
    public function get_token_pass($token)
    {
        $url = $this->urlApi . "api/forgot-password/" . $token . "/check";
        return ApiService::request($url, "GET", null);
    }

    /** reset password  */
    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/forgot-password/reset";
        return ApiService::request($url, "POST", $body);
    }


    /**
     * update profil admin
     * parameter json body
     */

    public function update_profil($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * update profil admin with image
     * parameter json body
     */

    public function update_profil_withfile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /** send notifkasi  */
    public function send_notif($channel, $event, $message)
    {
        return ApiService::push_notif($channel, $event, $message);
    }

    /** config environment api key */
    public function intergration_api($id)
    {
        $url = $this->urlApi . "api/data/master/environment/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data ortu siswa by id_siswa */
    public function get_data_ortu($id_siswa)
    {
        $url = $this->urlApi . "api/data/user/ortu/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** get realisasi  */
    public function report_realisasi($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/profile/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get realisasi  */
    public function report_realisasi_pemasukan_dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/spp/dashboard/pos/pemasukan/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get realisasi  */
    public function report_realisasi_pengeluaran_dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/spp/dashboard/pos/pengeluaran/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get realisasi  */
    public function report_realisasi_filter($tahun, $bulan)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/profile/tahun/" . $tahun . "/bulan/" . $bulan;
        return ApiService::request($url, "GET", NULL);
    }

    /** notifikasi  */
    public function notifikasi()
    {
        $url = $this->urlApi . "api/data/spp/notifikasi";
        return ApiService::request($url, "GET", NULL);
    }

    /** post notifikasi */
    public function send_notifikasi($body)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi";
        return ApiService::request($url, "POST", $body);
    }

    /** get notifikasi ortu atau siswa */
    public function get_notifikasi_ortus($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/ortu/kelas_siswa/" . $id_kelas_siswa;
        return ApiService::request($url, "GET", NULL);
    }

    /** update read post notifikasi */
    public function read_notifikasi($id, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/ortu/notif/" . $id . "/kelas_siswa/" . $id_kelas_siswa;
        return ApiService::request($url, "PUT", NULL);
    }

    /** get notifikasi admin */
    public function get_notifikasi_admin()
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/sekolah/notif";
        return ApiService::request($url, "GET", NULL);
    }

    /** update read post notifikasi */
    public function read_notifikasi_admin($id)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/sekolah/notif/" . $id;
        return ApiService::request($url, "PUT", NULL);
    }

    /** akun kategori */
    public function akuntansi_kategori()
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function show_akuntansi_kategori($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function post_akuntansi_kategori($body)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori";
        return ApiService::request($url, "POST", $body);
    }

    /** update akun kategori */
    public function update_akuntansi_kategori($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** akun sub kategori */
    public function akuntansi_subkategori()
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function show_akuntansi_subkategori($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function show_akuntansi_subkategori_byAk($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/kategori/" . $id;
        return ApiService::request($url, "GET", NULL);
    }


    /** show akun kategori */
    public function post_akuntansi_subkategori($body)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub";
        return ApiService::request($url, "POST", $body);
    }

    /** update akun kategori */
    public function update_akuntansi_subkategori($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** akun rekening akuntansi */
    public function akuntansi_rekening()
    {
        $url = $this->urlApi . "api/data/spp/akun/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** akun rekening akuntansi show */
    public function akuntansi_rekening_id($id)
    {
        $url = $this->urlApi . "api/data/spp/akun/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** akun rekening akuntansi UPDATE */
    public function update_akuntansi_rekening($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/akun/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** akun rekening akuntansi POST */
    public function post_akuntansi_rekening($body)
    {
        $url = $this->urlApi . "api/data/spp/akun";
        return ApiService::request($url, "POST", $body);
    }

    /** integrasi pembayaran setting  */
    public function setting_environment_pembayaran($jenis, $id)
    {
        $url = $this->urlApi . "api/data/master/environment/jenis/" . $jenis . "/sekolah/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** integrasi pembayaran setting  */
    public function setting_environment_pembayaranpost($body)
    {
        $url = $this->urlApi . "api/data/master/environment";
        return ApiService::request($url, "POST", $body);
    }

    /** show kode akun */
    public function show_akuntansi_kode_akun($id)
    {
        $url = $this->urlApi . "api/data/spp/akun/last_kode/sub_kategori/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show kode akun */
    public function show_akuntansi_kode_subkategori($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/last_kode/kategori/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show kode akun */
    public function report_transaksi_by_date($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/tanggal/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show kode akun */
    public function report_transaksijurnal_by_date($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal/tanggal/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** create transaksi jurnal */
    public function transaksi_jurnal($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal";
        return ApiService::request($url, "POST", $body);
    }

    /** create transaksi jurnal */
    public function transaksi_jurnal_with_file($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal";
        return ApiService::request_image_http_query($url, "POST", $body);
    }

    /** create transaksi saldo akun */
    public function transaksi_saldo_akun($body)
    {
        $url = $this->urlApi . "api/data/spp/saldo_akun/sinkron";
        return ApiService::request($url, "POST", $body);
    }

    /** create transaksi saldo akun */
    public function update_saldo_akun($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/saldo_akun/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** get transaksi saldo akun */
    public function get_saldo_akun()
    {
        $url = $this->urlApi . "api/data/spp/saldo_akun";
        return ApiService::request($url, "GET", NULL);
    }

    /** show transaksi saldo akun */
    public function show_saldo_akun($id)
    {
        $url = $this->urlApi . "api/data/spp/saldo_akun/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** get akun pemasukan */
    public function get_akun_pemasukan()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/pemasukan";
        return ApiService::request($url, "GET", NULL);
    }

    /** akun harta  */
    public function get_akun_harta()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/harta";
        return ApiService::request($url, "GET", NULL);
    }

    /** akun pengeluaran  */
    public function get_akun_pengeluaran()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/pengeluaran";
        return ApiService::request($url, "GET", NULL);
    }

    /** akun hutang  */
    public function get_akun_hutang()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/hutang";
        return ApiService::request($url, "GET", NULL);
    }

    /** akun hutang  */
    public function get_akun_modal()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/modal";
        return ApiService::request($url, "GET", NULL);
    }

    /** get transaksi jurnal by kode transaksi */
    public function get_detail_jurnalbykode($id)
    {
        $url = $this->urlApi . "api/data/spp/jurnal/transaksi/kode/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** get report jurnal  */
    public function report_jurnal_tahunAjaran($bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/jurnal/data/transaksi/bulan/" . $bulan . "/tahun_ajaran/" . $tahun . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** get report jurnal  */
    public function report_jurnal($bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/jurnal/data/transaksi/bulan/" . $bulan . "/tahun/" . $tahun . "/masehi";
        return ApiService::request($url, "GET", NULL);
    }

    /** get report laba rugi  */
    public function report_labarugi($bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/laporan/laba_rugi/bulan/" . $bulan . "/tahun/" . $tahun . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** get buku besar akuntansi */
    public function report_bukubesar($id_akun, $bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/laporan/buku_besar/akun/" . $id_akun . "/bulan/" . $bulan . "/tahun/" . $tahun . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** get neraca  */
    public function report_neraca($tahun)
    {
        $url = $this->urlApi . "api/data/spp/laporan/neraca/tahun/" . $tahun . "/masehi";
        return ApiService::request($url, "GET", NULL);
    }

    /** get dashboard  */
    public function dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/spp/dashboard/pos/pemasukan/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get dashboard  */
    public function laba_rugi_dashboard($bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/dashboard/laporan/laba-rugi/bulan/" . $bulan . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get laba rugi by tahun */
    public function report_labarugibytahun($tahun)
    {
        $url = $this->urlApi . "api/data/spp/laporan/laba_rugi/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get anggaran dashboard */
    public function dashboard_anggaran($tahun)
    {
        $url = $this->urlApi . "api/data/spp/dashboard/anggaran/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** saldo awal multi or single */
    public function saldo_awal_bysekolah($tahun)
    {
        $url = $this->urlApi . "api/data/spp/saldo_akun/profile/sekolah/tahun/" . $tahun . "/masehi";
        return ApiService::request($url, "GET", NULL);
    }

    /** get all data siswa  */
    public function index_data_siswa()
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa";
        return ApiService::request($url, "GET", NULL);
    }

    public function index_data_siswa_get_tahun($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/profile/tahun/".$tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** post jurnal pemasukan  */
    public function post_pemasukan_jurnal($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal/pemasukan";
        return ApiService::request($url, "POST", $body);
    }

    /** post jurnal pemasukan  */
    public function post_pemasukan_jurnal_file($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal/pemasukan";
        return ApiService::request($url, "POST", $body);
    }


    public function post_pengeluaran_jurnal($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal/pengeluaran";
        return ApiService::request($url, "POST", $body);
    }

    public function post_pengeluaran_anggaran($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/pos/pengeluaran";
        return ApiService::request($url, "POST", $body);
    }

    public function post_pengeluaran_anggaranfile($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/pos/pengeluaran";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post jurnal pemasukan  */
    public function post_pengeluaran_jurnal_file($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/jurnal/pengeluaran";
        return ApiService::request($url, "POST", $body);
    }

    /** data pos pengeluaran */
    public function get_data_pos_pengeluaran()
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran";
        return ApiService::request($url, "GET", null);
    }

    /** id show pos pengeluaran  */
    public function get_data_pos_pengeluaran_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pengeluaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** target pengeluaran  */
    public function get_data_pos_target_pengeluaran()
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran";
        return ApiService::request($url, "GET", null);
    }

    /** target pengeluaran  */
    public function get_data_pos_target_pengeluaranid($id)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data realisasi pos pengeluaran  */
    public function get_data_realisasi_pengeluaran($tahun)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran/profile/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data realisasi pos pengeluaran  */
    public function get_data_realisasi_pengeluaran_month($month, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/data_pos_pengeluaran/profile/tahun/" . $tahun . "/bulan/" . $month;
        return ApiService::request($url, "GET", null);
    }

    /** post hutang  */
    public function post_hutang($body)
    {
        $url = $this->urlApi . "api/data/spp/hutang";
        return ApiService::request($url, "POST", $body);
    }

    /** post hutang  */
    public function post_hutang_with_file($body)
    {
        $url = $this->urlApi . "api/data/spp/hutang";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post hutang  */
    public function post_piutang($body)
    {
        $url = $this->urlApi . "api/data/spp/piutang";
        return ApiService::request($url, "POST", $body);
    }

    /** post hutang  */
    public function post_piutang_with_file($body)
    {
        $url = $this->urlApi . "api/data/spp/piutang";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get data hutang  */
    public function list_hutang()
    {
        $url = $this->urlApi . "api/data/spp/hutang/profile/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** get data hutang  */
    public function list_piutang()
    {
        $url = $this->urlApi . "api/data/spp/piutang/profile/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** get data id hutang */
    public function detail_hutang_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/hutang/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data id hutang */
    public function detail_piutang_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/piutang/" . $id;
        return ApiService::request($url, "GET", null);
    }

     /** get data cicilan hutang */
     public function get_cicilan_piutang()
     {
         $url = $this->urlApi . "api/data/spp/cicilan_piutang";
         return ApiService::request($url, "GET", null);
     }

     /** get data cicilan hutang */
     public function get_cicilan_piutang_detailid($id)
     {
         $url = $this->urlApi . "api/data/spp/cicilan_piutang/".$id;
         return ApiService::request($url, "GET", null);
     }

    /** get data cicilan hutang */
    public function get_cicilan_hutang()
    {
        $url = $this->urlApi . "api/data/spp/cicilan_hutang";
        return ApiService::request($url, "GET", null);
    }

    /** get data cicilan hutang */
    public function get_cicilan_hutang_detailid($id)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_hutang/".$id;
        return ApiService::request($url, "GET", null);
    }

    /** get data cicilan hutang */
    public function get_cicilan_hutang_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_hutang/hutang/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** post hutang  */
    public function post_hutang_cicilan($body)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_hutang";
        return ApiService::request($url, "POST", $body);
    }

    /** post hutang  */
    public function post_hutang_cicilan_with_file($body)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_hutang";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get data cicilan hutang */
    public function get_cicilan_piutang_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_piutang/piutang/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** post hutang  */
    public function post_piutang_cicilan($body)
    {
        $url = $this->urlApi . "api/data/spp/cicilan_piutang";
        return ApiService::request($url, "POST", $body);
    }

     /** post hutang  */
     public function post_piutang_cicilan_with_file($body)
     {
         $url = $this->urlApi . "api/data/spp/cicilan_piutang";
         return ApiService::request_image($url, "POST", $body);
     }

    /** get data akun */
    public function get_akun_piutang()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/piutang";
        return ApiService::request($url, "GET", null);
    }

    /** get data akun */
    public function get_akun_aktiva()
    {
        $url = $this->urlApi . "api/data/spp/akun/akun/aktiva";
        return ApiService::request($url, "GET", null);
    }

    /** post transaksi pembelian aktiva */
    public function purchase_aktiva($body){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva";
        return ApiService::request($url, "POST", $body);
    }

    /** post transaksi pembelian aktiva */
    public function purchase_aktiva_with_file($body){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post  penambahan asset aktiva */
    public function asset_post_add($body){
        $url = $this->urlApi . "api/data/spp/asset_aktiva";
        return ApiService::request($url, "POST", $body);
    }

    /** get asset  */
    public function get_asset_tetap(){
        $url = $this->urlApi . "api/data/spp/asset_aktiva/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

     /** get asset  */
     public function get_asset_tetapid($id){
        $url = $this->urlApi . "api/data/spp/asset_aktiva/".$id;
        return ApiService::request($url, "GET", null);
    }

    /** update  penambahan asset aktiva */
    public function asset_post_update($body,$id){
        $url = $this->urlApi . "api/data/spp/asset_aktiva/".$id;
        return ApiService::request($url, "PUT", $body);
    }

    /** get asset  */
    public function gettransaksi_purchase_asset(){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get asset  */
    public function gettransaksi_sales_asset(){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/sekolah/profile/penjualan";
        return ApiService::request($url, "GET", null);
    }

     /** get asset  */
     public function gettransaksi_purchase_assetid($id){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/".$id;
        return ApiService::request($url, "GET", null);
    }

     /** get asset  */
     public function gettransaksi_sales_assetid($id){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/penjualan/".$id;
        return ApiService::request($url, "GET", null);
    }

    /** asset by akun  */
    public function get_asset_byakun($id){
        $url = $this->urlApi . "api/data/spp/asset_aktiva/akun/".$id;
        return ApiService::request($url, "GET", null);
    }

     /** post transaksi pembelian aktiva */
     public function sales_aktiva($body){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/penjualan";
        return ApiService::request($url, "POST", $body);
    }

    /** post transaksi pembelian aktiva */
    public function sales_aktiva_with_file($body){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/penjualan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update usia ekonomis*/
    public function update_asset_usiaekonomis($body){
        $url = $this->urlApi . "api/data/spp/transaksi_aktiva/usia_ekonomis";
        return ApiService::request($url, "PUT", $body);
    }

    /** check stok asset  */
    public function check_stock_asset(){
        $url = $this->urlApi . "api/data/spp/asset_aktiva/stok/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data orang tua  */
    public function get_data_ortu_all(){
        $url = $this->urlApi . "api/data/user/ortu/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** store Many transaksi konfirmasi */
    public function konfirmasi_tagihan_massal($body){
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/many";
        //$url = "http://backend.bumidev.com/api/data/spp/transaksi_konfirmasi/many";
        return ApiService::request($url, "POST", $body);
    }

    /** get data sekolah by id */
    public function sekolah_data_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

}
