<?php

namespace App\ApiService\PenjaminMutu;

use App\Helpers\ApiService;

class PesanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_inbox(){
        $url = $this->urlApi . "api/data/mutu/pesan/status/inbox";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_send(){
        $url = $this->urlApi . "api/data/mutu/pesan/status/outgoing-box";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function get_detail($id){
        $url = $this->urlApi . "api/data/mutu/pesan/". $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/mutu/pesan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/mutu/pesan";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }


    public function delete($id)
    {
        $url = $this->urlApi . "api/data/mutu/pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }



}

?>