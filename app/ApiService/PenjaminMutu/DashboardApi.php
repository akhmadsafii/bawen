<?php

namespace App\ApiService\PenjaminMutu;

use App\Helpers\ApiService;

class DashboardApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_dashboard(){
        $url = $this->urlApi . "api/data/mutu/dashboard/data";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_dashboard_kategori(){
        $url = $this->urlApi . "api/data/mutu/dashboard/kategori";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}

?>