<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class TugasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/tugas";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user_siswa($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/siswa/profile/tugas/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_tugas($start, $id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/agenda/" . $start . "/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function cetak_kegiatan($id)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/siswa/profile/kegiatan/cetak/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function rubah_paraf($body)
    {
        $url = $this->urlApi . "api/data/prakerin/tugas/paraf/prakerin/siswa";
        // dd($body);
        return ApiService::request($url, "POST", $body);
    }
}
