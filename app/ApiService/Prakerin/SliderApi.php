<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class SliderApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/slider";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/slider";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/slider";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/sekolah/".$id_sekolah;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/info";
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/slider/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/slider/trash/slider";
        return ApiService::request($url, "GET", null);
    }

}
