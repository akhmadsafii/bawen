<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class PembimbingIndustriApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function delete_profile($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/delete/profile/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", $id);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/update/foto/profile";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function update_paraf($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/update/paraf";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function by_industri($id_industri)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/industri/" . $id_industri;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pembimbing/profile/user/" . $id;
        return ApiService::request($url, "GET", null);
    }
}
