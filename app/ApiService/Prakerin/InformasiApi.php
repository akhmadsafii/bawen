<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class InformasiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/informasi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah_tanpa_login($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/trash/admin";
        return ApiService::request($url, "GET", null);
    }
    
    public function pengunjung_menu($id)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/sekolah/profile/tampil/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_title($body)
    {
        $url = $this->urlApi . "api/data/prakerin/informasi/get/title";
        return ApiService::request($url, "POST", $body);
    }

}
