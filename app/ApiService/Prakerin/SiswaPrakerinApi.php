<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class SiswaPrakerinApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function create_loop($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/save/loop/kelas_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/sekolah/profile/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function update_waktu($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/waktu/update";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_pembimbing_sekolah($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/pembimbing/sekolah/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_pembimbing_industri($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/pemb_industri/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function search_pemb_industri($body, $id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/pemb_industri/".$id;
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function search_pemb_sekolah($body, $id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/pembimbing/sekolah/".$id;
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_kelompok($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/anggota/kelompok/siswa/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah($id_ta_sm, $search)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa/sekolah/profile/tahun/".$id_ta_sm."?search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
