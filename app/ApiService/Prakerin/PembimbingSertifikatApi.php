<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class PembimbingSertifikatApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah_tanpa_login($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/sekolah/get/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jurusan($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/sekolah/jurusan/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_pemb_industri($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri_sertifikat/industri/profile/pembimbing/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


}
