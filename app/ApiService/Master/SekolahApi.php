<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class SekolahApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function export()
    {
        $url = $this->urlApi . "api/data/master/sekolah/export";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah";
        return ApiService::request($url, "POST", $body);
    }

    public function tambah_program($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/tambah/program";
        return ApiService::request($url, "POST", $body);
    }
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/update_status";
        return ApiService::request($url, "POST", $body);
    }

    public function tambah_program_file($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/tambah/program";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/update_sekolah";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/info";
        return ApiService::request($url, "POST", $body);
    }

    public function filter($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/filter/nama";
        return ApiService::request($url, "POST", $body);
    }

    public function search_by_sub_domain($body)
    {
        $url = $this->urlApi . "api/data/master/sekolah/search/by/subdomain";
        //dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function search_by_domain($domain)
    {
        $url = $this->urlApi . "api/data/master/sekolah/search/by/domain/sekolah/".$domain;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    public function program_sekolah($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/program/template/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
