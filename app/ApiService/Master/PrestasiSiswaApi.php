<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class PrestasiSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
   
    public function by_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/by-kategori/lomba/" . $id_kategori;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/prestasi-siswa/file/excel-example/".session('id_sekolah');
        return $url;
    }
}
