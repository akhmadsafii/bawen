<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class KategoriEkstrakurikulerApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/sekolah/ekstra-profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikulers/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/file/excel";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/kategori_ekstra/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

}
