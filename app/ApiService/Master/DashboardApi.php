<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class DashboardApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_statistic()
    {
        $url = $this->urlApi . "api/data/master/dashboard_user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function statistic_cbt()
    {
        $url = $this->urlApi . "api/data/master/dashboard/program/cbt";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
