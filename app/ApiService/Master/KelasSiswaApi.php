<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class KelasSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_active()
    {
        $url = $this->urlApi . "api/data/kelas_siswa/active";
        return ApiService::request($url, "GET", null);
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function cetak_raport($id_kelas_siswa, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/kelas_siswa/".$id_kelas_siswa."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function siswa_rombel($id_siswa, $id_rombel)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa_rombel/" . $id_siswa . "/" . $id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id_siswa($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa/get_by_siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_search_data($body)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/search/siswa/tahun/send/get_by_all";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_rombel($id_rombel, $id_tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa/rombel/".$id_rombel."/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_jurusan($tahun, $id_jurusan)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun."/jurusan/".$id_jurusan;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kelas($tahun, $id_kelas)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun."/kelas/siswa/".$id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_jurusan_kelas($tahun, $id_jurusan, $id_kelas)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun."/jurusan/".$id_jurusan."/kelas/".$id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_jurusan_kelas_rombel($tahun, $id_jurusan, $id_kelas, $id_rombel)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun."/jurusan/".$id_jurusan."/kelas/".$id_kelas."/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_nisn($nisn, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/nisn/".$nisn."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel_nisn($id_rombel, $nisn, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/rombel/".$id_rombel."/nisn/".$nisn."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function count_siswa()
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/jumlah/rombel";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/profile/tahun/".$tahun;
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/info";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_siswa()
    {
        $url = $this->urlApi . "api/data/user/siswa/siswa/tanpa/kelas/siswa";
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/file/excel-import/siswa/kelas/tahun/".$tahun;
        return ApiService::request_image($url, "POST", $body);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/file/excel-example/siswa/kelas/".$id_sekolah;
        return $url;
    }

    public function data_siswa($page, $tahun, $jurusan, $nis)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/data/siswa/tahun/jurusan/nis?page=$page&tahun=$tahun&jurusan=$jurusan&nis=$nis";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function multiple_rombel($body)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa/get_rombel/multiple";
        return ApiService::request($url, "POST", $body);
    }

    public function paginate_siswa($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/profile/paginate/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    public function rombel_siswa($page, $tahun, $rombel, $search)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/data/siswa/tahun/rombel/nis?page=$page&tahun=$tahun&rombel=$rombel&search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
