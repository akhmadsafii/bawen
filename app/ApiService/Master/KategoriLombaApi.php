<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class KategoriLombaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/kategori-lomba/file/excel-example";
        return $url;
    }
}
