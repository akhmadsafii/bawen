<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;
use App\ApiService\Master\EnvironmentApi;

class WhatsappApi
{
    public $urlApi;
    private $envApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
        $this->envApi = new EnvironmentApi;
    }


    public function send_notif($id_sekolah, $body)
    {
        $envi =  $this->envApi->by_jenis_sekolah("whatsapp_blast", $id_sekolah);
        $url = $envi['body']['data']['token'];
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah";
        return ApiService::request($url, "POST", $body);
    }
}
