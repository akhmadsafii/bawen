<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class TahunAjaranApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function set_aktif($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/set-aktif/" . $id;
        return ApiService::request($url, "PUT", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_semester($tahun)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/semester/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajarans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function get_aktif($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/aktif/sekolah/".$id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    public function get_tahun_ajaran()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/sekolah/tahun_ajaran";
        return ApiService::request($url, "GET", null);
    }
}
