<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class AplikasiMobileApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/program_mobile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/program_mobile/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/program_mobile/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/program_mobile/info";
        return ApiService::request($url, "PUT", $body);
    }
}
