<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class MutasiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_out($body)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/create/out";
        return ApiService::request($url, "POST", $body);
    }
    
    public function create_in($body)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/create/in";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/mutasi-siswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
}
