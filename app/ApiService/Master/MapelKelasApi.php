<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class MapelKelasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas";
        // dd(ApiService::request($url, "POST", $body));
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kelas($id_kelas)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/kelas/" . $id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_mapel_kelas($id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/mapel/kelas/" . $id_mapel. "/". $id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelass/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/mapel_kelass/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/master/mapel_kelas/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
