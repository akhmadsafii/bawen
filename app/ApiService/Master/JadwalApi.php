<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class JadwalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/jadwal";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/jadwal";
        return ApiService::request($url, "POST", $body);
    }

    public function full_create($body)
    {
        $url = $this->urlApi . "api/v2/data/master/jadwal";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/jadwal/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel_hari($id_rombel, $hari, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/master/jadwal/hari-ini/rombel/" . $id_rombel . "/tahun/" . $id_ta_sm . "/hari/" . $hari;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/jadwal/rombel/" . $id . "/tahun/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel_guru($id_guru, $id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/jadwal/guru/" . $id_guru . "/rombel/" . $id_rombel . "/tahun/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_guru($id, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/jadwal/guru/" . $id . "/tahun/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_guru_hari_ini($id_guru, $id_tahun_ajar, $hari)
    {
        $url = $this->urlApi . "api/data/master/jadwal/hari-ini/guru/" . $id_guru . "/tahun/" . $id_tahun_ajar . "/hari/" . $hari;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/jadwal/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/jadwal/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah($search, $status, $sort, $perPage, $page, $jurusan, $kelas, $rombel)
    {
        $url = $this->urlApi . "api/data/master/jadwal/profile/sekolah?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page&jurusan=$jurusan&kelas=$kelas&rombel=$rombel";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/jadwal/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/jadwal/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function update_all($body)
    {
        $url = $this->urlApi . "api/v2/data/master/jadwal/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/jadwal/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/jadwal/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/jadwal/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/jadwal/file/excel-example/" . $id_sekolah;
        return $url;
    }

    public function upload_excel($body, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/jadwal/file/excel-import/" . $id_tahun_ajar;
        return ApiService::request_image($url, "POST", $body);
    }
}
