<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class KalenderApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/kalender";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/kalender";
        // dd(ApiService::request($url, "POST", $body));
        return ApiService::request($url, "POST", $body);
    }

    public function get_acara($start, $end, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/kalender/" . $start . "/" . $end . "/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/kalender/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/kalender/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/kalender/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/kalender/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/kalenders/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/kalender/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/kalender/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/kalender/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/master/kalender/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/kalender/sekolah/data/kalender/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
