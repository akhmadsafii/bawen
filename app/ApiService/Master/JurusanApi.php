<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class JurusanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/jurusan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/master/jurusan";
        // dd(ApiService::request($url, "POST", $body));
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/jurusan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/jurusan/delete/" . $id;
        //dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/jurusan/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/jurusan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah_with_id($id)
    {
        $url = $this->urlApi . "api/data/master/jurusan/sekolah/withId/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/jurusans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/jurusan/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/jurusans/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/jurusan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function menu()
    {
        $url = $this->urlApi . "api/data/master/jurusan/menu/jurusan/data";
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/jurusan/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/jurusan/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
