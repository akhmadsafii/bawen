<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class EnvironmentApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/environment";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/environment";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/environment/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function by_jenis_sekolah($jenis, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/environment/jenis/" . $jenis . "/sekolah/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function group_jenis($id)
    {
        $url = $this->urlApi . "api/data/master/environment/groupby/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_token($body)
    {
        $url = $this->urlApi . "api/data/master/environment/update/token";
        return ApiService::request($url, "POST", $body);
    }


    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/environment/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
}
