<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class ProgramApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request($url, "GET", null);
    }
    
    public function get_aktif()
    {
        $url = $this->urlApi . "api/data/master/program/program/all/aktif";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/master/program";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/program/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function up($id)
    {
        $url = $this->urlApi . "api/data/master/program/sort/up/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    public function down($id)
    {
        $url = $this->urlApi . "api/data/master/program/sort/down/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/master/program/update_status";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_slug($slug)
    {
        $url = $this->urlApi . "api/data/master/program/slug/" . $slug;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/programs/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/program/info";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/master/program/info";
        return ApiService::request_image($url, "POST", $body);
    }

}
