<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class LokerPelamarApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/pelamar";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_loker($id_loker)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/".$id_loker;
        return ApiService::request($url, "GET", null);
    }

    public function check_loker_user($id_loker, $id_user)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/".$id_loker."/user/".$id_user;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_login_pelamar()
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/loker/didaftar";
        return ApiService::request($url, "GET", null);
    }

    public function semua($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/terkirim/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function aktif($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/aktif/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function terkirim($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/status_kirim/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function diproses($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/diproses/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function diterima($id_user)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/user/".$id_user."/diterima";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function ditolak($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/ditolak/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function lamaran_terkirim($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/pelamar/terkirim/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function lamaran_batalkan($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/pelamar/batalkan/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function lamaran_diterima($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/pelamar/terima/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function lamaran_proses_terpilih($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/terkirim/dipilih/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function lamaran_proses_wawancara($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/terkirim/dipilih/interview/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function perusahaan_ditolak($id)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/perloker/pelamar/ditolak/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function riwayat_pelamar($id_user)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/daftar/lamaran/user/".$id_user;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function pelamar_diterima_alumni($body)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/diterima/alumni";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/bkk/pelamar/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
}
