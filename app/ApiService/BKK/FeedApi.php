<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class FeedApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/feed";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_diskusi()
    {
        $url = $this->urlApi . "api/data/bkk/feed/diskusi/data/resource";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function admin()
    {
        $url = $this->urlApi . "api/data/bkk/feed/admin/get/data/resource";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function admin_all()
    {
        $url = $this->urlApi . "api/data/bkk/feed/admin/get/data/resource/all";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/feed";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/feed/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/feed/info";
        return ApiService::request($url, "PUT", $body);
    }
    
    public function update_status($body, $id)
    {
        $url = $this->urlApi . "api/data/bkk/feed/update/status/tampil/".$id;
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/feed/trash/admin";
        return ApiService::request($url, "GET", null);
    }

}
