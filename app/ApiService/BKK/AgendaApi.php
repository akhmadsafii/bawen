<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class AgendaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/agenda";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/agenda";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/acara/semua/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/agenda/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_loker_pelamar($id_loker, $id_pelamar)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/loker/".$id_loker."/pelamar/".$id_pelamar;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_loker($id_loker)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/loker/".$id_loker;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_industri()
    {
        $url = $this->urlApi . "api/data/bkk/agenda/data/profile/industri";
        return ApiService::request($url, "GET", null);
    }

    public function get_acara($tgl_mulai, $tgl_selesai, $id_loker, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/mulai/".$tgl_mulai."/sampai/".$tgl_selesai."/".$id_loker."/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_all_acara($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/agenda/acara/semua/sekolah/".$id_sekolah;
        // dd($body);
        return ApiService::request($url, "GET", null);
    }
}
