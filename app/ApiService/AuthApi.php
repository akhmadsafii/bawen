<?php

namespace App\ApiService;

use App\ApiService;

class AuthApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_auth()
    {
        $url = $this->urlApi . "/api/auth/login";
        return ApiService::request($url, "POST", null);
    }
}
