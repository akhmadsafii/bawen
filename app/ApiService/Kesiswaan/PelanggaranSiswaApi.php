<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class PelanggaranSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($search)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/sekolah/profile?search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/sekolah/all/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id_rombel($id_rombel, $search)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/sekolah/profile/wali_kelas/rombel/" . $id_rombel . "?search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function get_by_data()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/cari/tahun";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_rombel($id_rombel, $tahun)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/list/rombel/" . $id_rombel . "/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function get_by_kelas_siswa($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/daftar/siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function list_by_kelas_siswa($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/list/pelanggaran/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sering_dilakukan($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/sering-dilakukan/siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_pelanggaran_terbanyak()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/peringkat/pelanggaran/siswa/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function peringkat_pelanggaran_terbanyak()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/peringkat/pelanggaran/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function peringkat_pelanggaran_kelas()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/peringkat/pelanggaran/kelas/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function pelanggaran_warning()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/data/siswa/warning";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function update_sanksi($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran_siswa/update/sanksi/point";
        return ApiService::request($url, "POST", $body);
    }
}
