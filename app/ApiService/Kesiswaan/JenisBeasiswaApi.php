<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class JenisBeasiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/jenis_beasiswa/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
