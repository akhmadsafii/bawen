<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class GajiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/trash/gaji";
        return ApiService::request($url, "GET", null);
    }

    public function by_profile($bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/profile/bulan/".$bulan."/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function detail_pegawai($id_pegawai, $bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/kepegawaian/gaji/pegawai/".$id_pegawai."/bulan/".$bulan."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function store_detail($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/kepegawaian/gaji/store/detail";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
}
