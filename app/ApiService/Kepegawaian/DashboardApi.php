<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class DashboardApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_dashboard()
    {
        $url = $this->urlApi . "api/data/kepegawaian/dashboard";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
