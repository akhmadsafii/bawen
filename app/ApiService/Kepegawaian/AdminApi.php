<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class AdminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/update/profile/password";
        return ApiService::request($url, "POST", $body);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/update/status/aktif";
        return ApiService::request($url, "POST", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/update";
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/update";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }
}
