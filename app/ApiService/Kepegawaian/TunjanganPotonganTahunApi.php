<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class TunjanganPotonganTahunApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/trash";
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun($tahun) //2021
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/group/jenis/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function setting_many($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/tunjangan_potongan_tahun/setting/many";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
}
