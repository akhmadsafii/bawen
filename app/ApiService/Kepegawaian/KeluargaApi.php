<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class KeluargaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/trash/keluarga";
        return ApiService::request($url, "GET", null);
    }

    public function by_pegawai($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/keluarga/pegawai/".$id;
        return ApiService::request($url, "GET", null);
    }
}
