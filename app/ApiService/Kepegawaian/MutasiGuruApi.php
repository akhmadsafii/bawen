<?php

namespace App\ApiService\Kepegawaian;

use App\Helpers\ApiService;

class MutasiGuruApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create_in($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/create/in";
        return ApiService::request($url, "POST", $body);
    }

    public function create_v2($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/v2/create/in";
        return ApiService::request($url, "POST", $body);
    }

    public function create_out($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/create/out";
        return ApiService::request($url, "POST", $body);
    }

    public function create_pangkat($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/pangkat";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function kembalikan($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/pegawai-undo/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function by_pegawai($id_pegawai)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/pegawai/" . $id_pegawai;
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/trash/mutasi";
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun($tahun)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/sekolah/profile/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun_jenis($tahun, $jenis)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/sekolah/profile/tahun/".$tahun."/jenis/".$jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_jenis($jenis)
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/sekolah/profile/not_tahun/jenis/".$jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/kepegawaian/mutasi/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }
}
