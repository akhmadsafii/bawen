<?php

namespace App\ApiService\Simpen;

use App\Helpers\ApiService;

class SimpenApi
{
    /**
     * variabel global
     */
    public $urlApi;

    /**
     * __construct function with variable global for url domain
     */

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /** get profil  */
    public function get_info_profil()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /**
     * update profil admin
     * parameter json body
     */

    public function update_profil_admin($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    /**
     * update profil admin with image
     * parameter json body
     */

    public function update_profil_adminwithfile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password_admin($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /** get data admin  */
    public function get_data_admin()
    {
        $url = $this->urlApi . "api/data/simpen/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data sambutan  */
    public function get_data_sambutan($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/sambutan/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** post sambutan  */
    public function post_data_setting_sambutanfile($body)
    {
        $url = $this->urlApi . "api/data/simpen/sambutan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post sambutan  */
    public function post_data_setting_sambutan($body)
    {
        $url = $this->urlApi . "api/data/simpen/sambutan";
        return ApiService::request($url, "POST", $body);
    }

    /** post panduan  */
    public function post_data_setting_panduanfile($body)
    {
        $url = $this->urlApi . "api/data/simpen/panduan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post panduan  */
    public function post_data_setting_panduan($body)
    {
        $url = $this->urlApi . "api/data/simpen/panduan";
        return ApiService::request($url, "POST", $body);
    }

    /** get data panduan  */
    public function get_data_panduan($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/panduan/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** get data panduan  */
    public function get_data_informasi($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/informasi/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** post panduan  */
    public function post_data_setting_informasifile($body)
    {
        $url = $this->urlApi . "api/data/simpen/informasi";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post panduan  */
    public function post_data_setting_informasi($body)
    {
        $url = $this->urlApi . "api/data/simpen/informasi";
        return ApiService::request($url, "POST", $body);
    }

    /** get data setting  */
    public function get_data_setting($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/setting/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** update setting  */
    public function update_data_setting_simpenfile($body)
    {
        $url = $this->urlApi . "api/data/simpen/setting";
        return ApiService::request_images_setting($url, "POST", $body);
    }

    /** update setting  */
    public function update_data_setting_simpen($body)
    {
        $url = $this->urlApi . "api/data/simpen/setting";
        return ApiService::request($url, "POST", $body);
    }

    /** master admin */
    public function master_admin()
    {
        $url = $this->urlApi . "api/data/simpen/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master admin by id */
    public function master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/simpen/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** master admin */
    public function master_admin_tahunAjaran()
    {
        $url = $this->urlApi . "api/data/simpen/admin/tahun_ajaran";
        return ApiService::request($url, "GET", null);
    }

    /** update master admin */
    public function master_admin_post_file($body)
    {
        $url = $this->urlApi . "api/data/simpen/admin/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update master admin */
    public function master_admin_post($body)
    {
        $url = $this->urlApi . "api/data/simpen/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    /** post master admin */
    public function master_admin_post_filex($body)
    {
        $url = $this->urlApi . "api/data/simpen/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post master admin */
    public function master_admin_postx($body)
    {
        $url = $this->urlApi . "api/data/simpen/admin";
        return ApiService::request($url, "POST", $body);
    }

    /** remove master admin by id */
    public function remove_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/simpen/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove master admin by id */
    public function remove_permanent_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/simpen/admin/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore master admin by id */
    public function restore_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/simpen/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** sampah admin  */
    public function trash_admin()
    {
        $url = $this->urlApi . "api/data/simpen/admin/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** setting nilai mapel  */
    public function get_data_nilaimapel($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** setting nilai mapel  */
    public function get_data_nilaimapelbyid($id)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** setting nilai mapel  */
    public function remove_data_nilaimapelbyid($id)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** setting nilai mapel  */
    public function remove_permanent_data_nilaimapelbyid($id)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** update nilai mapel */
    public function update_nilaimapel_setting($body, $id)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post nilai mapel */
    public function post_nilaimapel_setting($body)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel";
        return ApiService::request($url, "POST", $body);
    }

    /** sampah nilai mata pelajaran */
    public function trash_nilaiMapel()
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** restore setting nilai mata pelajaran  */
    public function restore_nilaiMapel($id)
    {
        $url = $this->urlApi . "api/data/simpen/nilai_mapel/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** get data siswa  */
    public function get_data_siswa()
    {
        $url = $this->urlApi . "api/data/simpen/siswa/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    // Siswa by sekolah
    public function siswa_by_sekolah($tahun, $search, $page)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/sekolah/tahun/" . $tahun . "/paginate?search=$search&page=$page";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    /** master siswa  */
    public function get_data_siswa_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** post siswa  */
    public function post_data_siswa($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa";
        return ApiService::request($url, "POST", $body);
    }

    /** post siswa  */
    public function post_data_siswafile($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update siswa  */
    public function update_data_siswa($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/info";
        return ApiService::request($url, "POST", $body);
    }

    /** update siswa  */
    public function update_data_kelulusan($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/keputusan";
        return ApiService::request($url, "PUT", $body);
    }

    /** update siswa  */
    public function update_data_siswafile($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** remove by id  */
    public function remove_data_siswa_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove permanent by id  */
    public function remove_permanent_data_siswa_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore by id siswa */
    public function restore_data_siswa_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** data sampah siswa */
    public function trash_siswa()
    {
        $url = $this->urlApi . "api/data/simpen/siswa/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** filetr data siswa by tahun  */
    public function filter_siswa($tahun)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/sekolah/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** file import data siswa  */
    public function import_data_siswa($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    /** download file import data siswa  */
    public function downloadimport_data_siswa($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/file/excel-example/sekolah/" . $id_sekolah;
        //return ApiService::request($url, "GET", null);
        return $url;
    }

    /** update keputusan manual */
    public function update_keputusan_manual($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa/keputusan";
        return ApiService::request($url, "PUT", $body);
    }

    /** setting surat */
    public function get_data_settingsurat()
    {
        $url = $this->urlApi . "api/data/simpen/setting/sekolah/profile/template";
        return ApiService::request($url, "GET", null);
    }

    /** post setting template */
    public function update_formsetting_surat($body)
    {
        $url = $this->urlApi . "api/data/simpen/setting/template";
        return ApiService::request($url, "POST", $body);
    }

    /** get nilai siswa by id siswa  */
    public function get_nilai_siswabyid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data nilai siswa  */
    public function get_data_nilai_siswa()
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get nilai siswa by id   */
    public function get_nilai_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** post nilai siswa manual */
    public function post_nilai_siswa($body)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai";
        return ApiService::request($url, "POST", $body);
    }

    /** update nilai siswa */
    public function update_nilai_siswa($body, $id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** remove nilai siswa by id   */
    public function remove_nilai_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove nilai siswa by id   */
    public function remove_permanent_nilai_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore nilai siswa by id   */
    public function restore_nilai_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** get data sampah nilai siswa  */
    public function get_trash_nilai_siswa()
    {
        $url = $this->urlApi . "api/data/simpen/siswa_nilai/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** get data pengumuman */
    public function get_data_pengumuman($id, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/pengumuman/sekolah/" . $id_sekolah . "/siswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** dashboard  */
    public function get_data_dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/simpen/pengumuman/dashboard/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data kategori nilai */
    public function get_data_kategori_nilai($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/kategori_nilai/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** get data kategori nilai by id  */
    public function get_data_kategori_byid($id)
    {
        $url = $this->urlApi . "api/data/simpen/kategori_nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data kategori nilai by id  */
    public function update_data_kategori_byid($id, $body)
    {
        $url = $this->urlApi . "api/data/simpen/kategori_nilai/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post kategori mapel */
    public function post_data_kategori_mapel($body)
    {
        $url = $this->urlApi . "api/data/simpen/kategori_nilai";
        return ApiService::request($url, "POST", $body);
    }

    /** template surat kelulusan  */
    public function get_template_surat($kategori, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/template_surat/sekolah/" . $id_sekolah . "/kategori/" . $kategori;
        return ApiService::request($url, "GET", null);
    }

    /** post template surat kelulusan  */
    public function post_template_surat($body)
    {
        $url = $this->urlApi . "api/data/simpen/template_surat";
        return ApiService::request($url, "POST", $body);
    }

    /** post template surat kelulusan  */
    public function post_template_suratwithfile($body)
    {
        $url = $this->urlApi . "api/data/simpen/template_surat";
        return ApiService::request_images_setting($url, "POST", $body);
    }

    /** get data surat pendukung */
    public function get_data_setting_surat_pendukung($id_sekolah)
    {
        $url = $this->urlApi . "api/data/simpen/setting_pendukung/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }

    /** post data surat pendukung */
    public function post_setting_surat_pendukung($body)
    {
        $url = $this->urlApi . "api/data/simpen/setting_pendukung";
        return ApiService::request($url, "POST", $body);
    }

    /** get data nilai siswa cetak  */
    public function print_nilai_ujian($id_sekolah, $id_siswa)
    {
        $url = $this->urlApi . "api/data/simpen/cetak/nilai/sekolah/" . $id_sekolah . "/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** get data skl */
    public function print_skl($id_sekolah, $id_siswa)
    {
        $url = $this->urlApi . "api/data/simpen/cetak/skl/sekolah/" . $id_sekolah . "/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** get data ska */
    public function print_ska($id_sekolah, $id_siswa)
    {
        $url = $this->urlApi . "api/data/simpen/cetak/ska/sekolah/" . $id_sekolah . "/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** get data surat lainnya */
    public function print_surat_lain($id_sekolah, $id_siswa)
    {
        $url = $this->urlApi . "api/data/simpen/cetak/surat_lain/sekolah/" . $id_sekolah . "/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** print all document  */
    public function print_all($id_sekolah, $id_siswa)
    {
        $url = $this->urlApi . "api/data/simpen/cetak/all/sekolah/" . $id_sekolah . "/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }
}
