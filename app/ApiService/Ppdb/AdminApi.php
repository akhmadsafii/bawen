<?php

namespace App\ApiService\Ppdb;

use App\Helpers\ApiService;

class AdminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/ppdb/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/ppdb/admin/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function restore($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/ppdb/admin/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }


    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function delete_profile($id_admin)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/delete/profile/" . $id_admin;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/update/foto/profile";
        return ApiService::request_image($url, "POST", $body);
    }
}
