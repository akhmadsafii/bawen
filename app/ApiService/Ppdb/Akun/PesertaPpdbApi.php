<?php

namespace App\ApiService\Ppdb\Akun;

use App\Helpers\ApiService;

class PesertaPpdbApi
{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /**
     * get data Profil User
     */

    public function get_info_profil_user()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /** post data profil user  */

    public function update_profil_user($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request($url, "POST", $body);
    }

    public function update_profil_userwithfile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password_user($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function trash_pesan_peserta()
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/trash/peserta";
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function get_pesan_user_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/peserta/" . $id;
        return ApiService::request($url, "GET", null);
    }


    /** form pesan  */
    public function update_pesan_status($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/update/ditutup";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function update_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/info";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function update_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function post_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function post_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function post_balas_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function post_balas_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function delete_pesan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** form pesan  */
    public function delete_pesan_permanet($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** form pesan  */
    public function restore_pesan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** form pesan  */
    public function get_pesan_peserta_show($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function get_pesan_response_admin($id)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan/pesan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function post_pesan_tutup($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/update/ditutup";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * Check Tagihan
     */

    public function get_tagihan_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/tagihan-pembayaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** post konfirm pembayarab */
    public function post_konfirm_pembayaran($body)
    {
        $url = $this->urlApi . "api/data/ppdb/tagihan-pembayaran/pembayaran";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get access halaman  */
    public function get_accses_link($id)
    {
        $url = $this->urlApi . "api/data/ppdb/tagihan-pembayaran/link/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get form formulir pendaftaran */

    public function get_form_peserta_pendaftaran($id,$tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/form/peserta/".$id."/form/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    /** pendaftaran form register siswa  */
    public function registerForm($body){
        $url = $this->urlApi . "api/data/ppdb/peserta/pendaftaran/siswa";
        return ApiService::request($url, "POST", $body);
    }

    /** get detail peserta pendaftar from    */
    public function get_data_peserta_detailbyid($id,$tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/detail/peserta/".$id."/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    /**
     *  get data preview card
     */

    public function get_preview_print_card($id,$tahun){
        $url = $this->urlApi . "api/data/ppdb/preview/peserta/cetak/kartu/".$id."/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
     }

     /** get data preview form */
     public function get_preview_print_form($id,$tahun){
        $url = $this->urlApi . "api/data/ppdb/preview/peserta/cetak/form/".$id."/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
     }

      /** get data preview form */
      public function get_pengumuman($id){
        $url = $this->urlApi . "api/data/ppdb/preview/peserta/cetak/pengumuman/".$id;
        return ApiService::request($url, "GET", null);
     }

      /** get data preview form */
      public function get_filedoc($id){
        $url = $this->urlApi . "api/data/ppdb/dokumen/peserta/".$id;
        return ApiService::request($url, "GET", null);
     }

     /** invoice pembayaran */
     public function download_invoice($id,$tahun){
        $url = $this->urlApi . "api/data/ppdb/pembayaran/invoice/".$id."/tahun/".$tahun;
        return $url;
     }

     /** forget password  */
     public function forget_pass($body){
        $url = $this->urlApi . "api/forgot-password/email/peserta";
        return ApiService::request($url, "POST", $body);
     }

     /** get Token password */
     public function get_token_pass($token){
        $url = $this->urlApi . "api/forgot-password/".$token."/check";
        return ApiService::request($url, "GET", null);
     }

       /** reset password  */
       public function reset_pass($body){
        $url = $this->urlApi . "api/forgot-password/reset";
        return ApiService::request($url, "POST", $body);
     }
}
