<?php

namespace App\ApiService\Ppdb\Akun;

use App\Helpers\ApiService;

class AdminPpdbApi
{
    /**
     * variabel global
     */
    public $urlApi;

    /**
     * __construct function with variable global for url domain
     */

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /**
     * get data Profil Admin
     */

    public function get_info_profil_admin()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /**
     * update profil admin
     * parameter json body
     */

    public function update_profil_admin($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    /**
     * update profil admin with image
     * parameter json body
     */

    public function update_profil_adminwithfile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password_admin($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * post banner with file
     */

    public function post_banner_adminwithfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/banner";
        return ApiService::request_image($url, "POST", $body);
    }

    /**
     * post banner without file
     */

    public function post_banner_admin($body)
    {
        $url = $this->urlApi . "api/data/ppdb/banner";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * get detail banner
     */

    public function get_detail_banner($id)
    {
        $url = $this->urlApi . "api/data/ppdb/banner/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get detail alur by id_sekolah
     */

    public function get_detail_alur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/alur/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * post data alur without file
     */

    public function get_data_alur($body)
    {
        $url = $this->urlApi . "api/data/ppdb/alur";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * post data alur with file
     */

    public function get_data_alurwithfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/alur";
        return ApiService::request_image($url, "POST", $body);
    }

    /**
     * get data syarat pendaftaran
     */

    public function get_data_syarat($body)
    {
        $url = $this->urlApi . "api/data/ppdb/syarat";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * post data syarat with file
     */

    public function get_data_syaratwithfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/syarat";
        return ApiService::request_image($url, "POST", $body);
    }

    /**
     * get detail syarat by id_sekolah
     */

    public function get_detail_syarat($id)
    {
        $url = $this->urlApi . "api/data/ppdb/syarat/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get data panduan pendaftaran
     */

    public function get_data_panduan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/panduan";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * post data panduan with file
     */

    public function get_data_panduanwithfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/panduan";
        return ApiService::request_image($url, "POST", $body);
    }

    /**
     * get detail panduan by id_sekolah
     */

    public function get_detail_panduan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/panduan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * post data jadwal
     */

    public function post_data_jadwal($body)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * get data jadwal by id sekolah
     */

    public function get_data_jadwal($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get data jadwal by id sekolah
     */

    public function get_data_jadwal_all($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data info jadwal  */
    public function update_data_jadwal_info($id, $body)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/info/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /**
     * delete data info jadwal
     */

    public function delete_data_jadwal($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /**
     * permanent delete data info jadwal
     */

    public function delete_permanent_data_jadwal($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /**
     * Force permanent delete data info jadwal
     */

    public function forecdelete_permanent_data_jadwal($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/force/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /**
     * restore data info jadwal
     */

    public function restore_data_jadwal($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /**
     * all profile data jadwal kegiatan sekolah
     */

    public function get_data_profiljadwal_all_sekolah()
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/all/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /**
     * Tempat Sampah data jadwal Kegiatan sekolah
     */

    public function trash_data_jadwal()
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /**
     * Get Data Pengumuman
     */

    public function get_data_pengumuman_all_sekolah()
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/all/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /**
     * post data pengumuman
     */

    public function post_data_feed($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * post data pengumuman with file
     */

    public function post_data_feedfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get detail pengumuman */
    public function get_data_detail_feed($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * update data pengumuman with file
     */

    public function update_data_feedfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /**
     * update data pengumuman with file
     */

    public function update_data_feed($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/info";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * Delete data pengumuman
     */

    public function delete_data_feed($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /**
     * Delete Permanent data pengumuman
     */

    public function delete_dataforce_feed($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /**
     * restore  data pengumuman
     */

    public function restore_data_feed($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /**  Trash data pengumuman  */
    public function trash_data_feed()
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** Get Data FAQ */
    public function get_data_faq($id)
    {
        $url = $this->urlApi . "api/data/ppdb/faq/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**post create or update data FAQ */
    public function post_data_faq($body)
    {
        $url = $this->urlApi . "api/data/ppdb/faq";
        return ApiService::request($url, "POST", $body);
    }

    /**post create or update data FAQ */
    public function post_data_faqfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/faq";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get data Brosur  */
    public function get_data_brosur()
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/all/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /**post create or update data brosur */
    public function post_data_brosur($body)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur";
        return ApiService::request($url, "POST", $body);
    }

    /**post create or update data brosur */
    public function post_data_brosurfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get data detail brosur  */
    public function get_detail_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** trash data brosur  */

    public function trash_data_brosur()
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /**post create or update data brosur */
    public function update_data_brosur($body)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/info";
        return ApiService::request($url, "POST", $body);
    }

    /**post create or update data brosur */
    public function update_data_brosurfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** delete data detail brosur  */
    public function delete_data_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete force data detail brosur  */
    public function deleteforce_data_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/force/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete permanent data detail brosur  */
    public function deletepermanent_data_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore data detail brosur  */
    public function restore_data_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** sample document file  */
    public function get_sampledata_doc()
    {
        $url = $this->urlApi . "api/data/ppdb/file/all/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /**post create or update data sample doc */
    public function post_data_sampledoc($body)
    {
        $url = $this->urlApi . "api/data/ppdb/file";
        return ApiService::request($url, "POST", $body);
    }

    /**post create or update data sample doc */
    public function post_data_sampledocfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/file";
        return ApiService::request_image($url, "POST", $body);
    }

    /** sample document file  */
    public function get_sampleinfo_doc($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**post create or update data sample doc */
    public function update_data_sampledoc($body)
    {
        $url = $this->urlApi . "api/data/ppdb/file/info";
        return ApiService::request($url, "POST", $body);
    }

    /**post create or update data sample doc */
    public function update_data_sampledocfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/file/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** delete data detail sample  */
    public function delete_data_sample($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete force data detail sample  */
    public function deleteforce_data_sample($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/force/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete permanent data detail sample  */
    public function deletepermanent_data_sample($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore data detail brosur  */
    public function restore_data_sample($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    /** trash data sample  */
    public function trash_data_sample()
    {
        $url = $this->urlApi . "api/data/ppdb/file/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** file support perserta */
    public function get_data_file_support_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/peserta/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get all file support  */
    public function get_data_dokumen()
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen";
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta  */
    public function get_data_peserta()
    {
        $url = $this->urlApi . "api/data/ppdb/peserta";
        return ApiService::request($url, "GET", null);
    }

    /** delete peserta permanet  */
    public function delete_permanet_data_pesertabyid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/delete/permanent/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get all peserta by id  */
    public function get_detail_peserta_byid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta by id  */
    public function get_detail_dokumen_byid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/" . $id;
        return ApiService::request($url, "GET", null);
    }


    /** post dokumen file support  */
    public function post_data_dokumen($body)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen";
        return ApiService::request($url, "POST", $body);
    }

    /** post dokumen file support  */
    public function post_data_dokumenfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post dokumen file support  */
    public function update_data_dokumen($body)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/info";
        return ApiService::request($url, "POST", $body);
    }

    /** post dokumen file support  */
    public function update_data_dokumenfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** delete force data detail dokumen  */
    public function deleteforce_data_dokumen($id)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/force/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete permanent data detail dokumen  */
    public function delete_data_dokumen($id)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete permanent data detail dokumen  */
    public function deletepermanent_data_dokumen($id)
    {
        $url = $this->urlApi . "api/data/ppdb/dokumen/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** post dokumen file support  */
    public function post_data_peserta($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta";
        return ApiService::request($url, "POST", $body);
    }

    /** post dokumen file support  */
    public function post_data_pesertafile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update dokumen file support  */
    public function update_data_peserta($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/info";
        return ApiService::request($url, "POST", $body);
    }

    /** update dokumen file support  */
    public function update_data_pesertafile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** delete data peserta  */
    public function delete_data_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete data peserta force */
    public function deleteforce_data_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete data peserta force */
    public function restore_data_peserta($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** update status penerimaan peserta  */
    public function update_data_peserta_status($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/update/diterima";
        return ApiService::request_image($url, "PUT", $body);
    }

    /** get all peserta trash  */
    public function get_trash_peserta()
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta diterima   */
    public function get_data_peserta_diterima($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/pendaftaran/tahun/" . $tahun . "/diterima/1";
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta diterima   */
    public function get_data_peserta_diterimax($id)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/publik/pendaftaran/sekolah/".$id;
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta ditolak   */
    public function get_data_peserta_ditolak($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/pendaftaran/tahun/" . $tahun . "/diterima/2";
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta ditolak   */
    public function update_status_peserta_diterima($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/update/diterima";
        return ApiService::request($url, "PUT", $body);
    }

    /** get all peserta ditolak   */
    public function get_data_setting_ppdb()
    {
        $url = $this->urlApi . "api/data/ppdb/setting";
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta ditolak   */
    public function get_data_setting_ppdbbyid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/setting/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update setting   */
    public function update_data_setting_ppdb($body)
    {
        $url = $this->urlApi . "api/data/ppdb/setting";
        return ApiService::request($url, "POST", $body);
    }

     /** pendaftaran form register siswa  */
     public function registerForm($body){
        $url = $this->urlApi . "api/data/ppdb/peserta/pendaftaran/siswa";
        return ApiService::request($url, "POST", $body);
    }

    /** update setting   */
    public function update_data_setting_ppdbfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/setting";
        return ApiService::request_images_setting($url, "POST", $body);
    }

    /** get all setting pembayaran   */
    public function get_data_setting_pembayaran($id)
    {
        $url = $this->urlApi . "api/data/ppdb/setting-pembayaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get all setting pembayaran   */
    public function get_data_setting_sambutan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/sambutan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update setting   */
    public function post_data_setting_sambutan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/sambutan";
        return ApiService::request($url, "POST", $body);
    }

    /** update setting   */
    public function post_data_setting_sambutanfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/sambutan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update setting   */
    public function post_data_setting_pembayaran($body)
    {
        $url = $this->urlApi . "api/data/ppdb/setting-pembayaran";
        return ApiService::request($url, "POST", $body);
    }

    /** get all setting pembayaran   */
    public function get_data_setting_form()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/groupby/jenis-form";
        return ApiService::request($url, "GET", null);
    }

    /** update setting   */
    public function post_data_setting_form_update($body)
    {
        $url = $this->urlApi . "api/data/ppdb/form/update/status_form";
        return ApiService::request($url, "POST", $body);
    }

    /** get all setting form preview   */
    public function get_data_setting_showform()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/form";
        return ApiService::request($url, "GET", null);
    }

    /** get all setting form card template   */
    public function get_data_setting_formcard()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/form";
        return ApiService::request($url, "GET", null);
    }

    /** get all data admin user    */
    public function get_data_AdminUser()
    {
        $url = $this->urlApi . "api/data/ppdb/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** update admin   */
    public function post_data_admin($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin";
        return ApiService::request($url, "POST", $body);
    }

    /** update admin   */
    public function post_data_adminfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    /** get all data admin user    */
    public function get_data_AdminUserbyid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }


    /** update admin   */
    public function update_data_admin($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    /** update admin   */
    public function update_data_adminfile($body)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** delete data admin user */
    public function delete_data_AdminUserbyid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** custom form  */
    public function get_data_customform()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/form";
        return ApiService::request($url, "GET", null);
    }

    /** custom form  */
    public function get_data_custom_type_form()
    {
        $url = $this->urlApi . "api/data/ppdb/jenis-form/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** form post new  */
    public function post_createform_new($body)
    {
        $url = $this->urlApi . "api/data/ppdb/form";
        return ApiService::request($url, "POST", $body);
    }

    /** form update new  */
    public function update_createform_new($body, $id)
    {
        $url = $this->urlApi . "api/data/ppdb/form/info/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** form post new  type */
    public function post_createform_newtype($body)
    {
        $url = $this->urlApi . "api/data/ppdb/jenis-form";
        return ApiService::request($url, "POST", $body);
    }

    /** form update new  */
    public function update_createform_newtype($body, $id)
    {
        $url = $this->urlApi . "api/data/ppdb/jenis-form/info/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** get data form by id  */
    public function get_data_formid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/form/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data form type  by id  */
    public function get_data_formtypeid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jenis-form/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data template form card  */
    public function get_data_template_kartu()
    {
        $url = $this->urlApi . "api/data/ppdb/setting-template/kartu";
        return ApiService::request($url, "GET", null);
    }

    /** get data template surat */
    public function get_data_template_surat()
    {
        $url = $this->urlApi . "api/data/ppdb/setting-template/surat";
        return ApiService::request($url, "GET", null);
    }

    /** form post template */
    public function update_template($body)
    {
        $url = $this->urlApi . "api/data/ppdb/setting-template/update";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function get_pesan_admin()
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function get_pesan_admin_show($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function update_pesan_status($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/update/ditutup";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function update_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/info";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function update_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function post_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function post_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function post_balas_pesan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** form pesan  */
    public function post_balas_pesan_file($body)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan";
        return ApiService::request_image($url, "POST", $body);
    }

    /** form pesan  */
    public function delete_pesan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** form pesan  */
    public function delete_pesan_permanet($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** form pesan  */
    public function restore_pesan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }



    /** form pesan  */
    public function trash_pesan_sekolah()
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function get_pesan_response_user($id)
    {
        $url = $this->urlApi . "api/data/ppdb/balasan-pesan/pesan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function post_pesan_tutup($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pesan/update/ditutup";
        return ApiService::request($url, "POST", $body);
    }

    /** Pembayaran Pending   */
    public function get_payment_with_pending()
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/sekolah/unpaid";
        return ApiService::request($url, "GET", null);
    }

    /** Pembayaran confirm   */
    public function get_payment_with_confirm($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/peserta/tahun/" . $tahun . "/pembayaran/0";
        return ApiService::request($url, "GET", null);
    }

    public function get_payment_with_paid($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/peserta/tahun/" . $tahun . "/pembayaran/1";
        return ApiService::request($url, "GET", null);
    }

    /** Pembayaran Pending   */

    public function get_payment_with_reject($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/peserta/tahun/" . $tahun . "/pembayaran/2";
        return ApiService::request($url, "GET", null);
    }

    /** detail pembayaran  */
    public function get_payment_by_id($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function update_status_payment($body)
    {
        $url = $this->urlApi . "api/data/ppdb/pembayaran/update";
        return ApiService::request($url, "PUT", $body);
    }

    /** dashboard pembayaran */
    public function get_peserta_dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/dashboard/peserta/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }
    /** get data total peserta yang diterima  */
    public function get_keputusan_peserta_diterima_dashboard($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/dashboard/keputusan/peserta/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data peserta register ulang  */
    public function get_peserta_registerul($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/daftar/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get detail peserta pendaftar from    */
    public function get_data_peserta_detailbyid($id, $tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/detail/peserta/" . $id . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }



    /** get form aktif urutan  */
    public function get_form_aktif()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/form/aktif";
        return ApiService::request($url, "GET", null);
    }

    /** form pesan  */
    public function post_update_form_urutan($body)
    {
        $url = $this->urlApi . "api/data/ppdb/form/update/urutan_form";
        return ApiService::request($url, "POST", $body);
    }

    /** get preview form   */
    public function get_preview_form_template($id)
    {
        $url = $this->urlApi . "api/data/ppdb/preview/sekolah/cetak/kartu/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get preview form   */
    public function get_preview_print_form($id)
    {
        $url = $this->urlApi . "api/data/ppdb/preview/sekolah/cetak/form/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data preview form */
    public function get_preview_print_formbyuser($id,$tahun){
        $url = $this->urlApi . "api/data/ppdb/preview/peserta/cetak/form/".$id."/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
     }

    /** get preview form   */
    public function get_setting_kartu()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/kartu";
        return ApiService::request($url, "GET", null);
    }

    /** get preview form   */
    public function get_setting_kartu_aktif()
    {
        $url = $this->urlApi . "api/data/ppdb/form/sekolah/kartu/aktif";
        return ApiService::request($url, "GET", null);
    }

    /** form status kartu  */
    public function post_update_form_status($body)
    {
        $url = $this->urlApi . "api/data/ppdb/form/kartu/update/status_kartu";
        return ApiService::request($url, "POST", $body);
    }

    /** form register peserta ppdb via admin   */
    public function post_add_form_pendaftaran($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/admin/pendaftaran/siswa";
        return ApiService::request($url, "POST", $body);
    }

    /** export data peserta diterima */
    public function export_data_peserta($tahun)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/pendaftaran/diterima/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data preview form */
    public function get_pengumuman_preview()
    {
        $url = $this->urlApi . "api/data/ppdb/preview/peserta/cetak/pengumuman/1";
        return ApiService::request($url, "GET", null);
    }

    /** integrasi pembayaran setting  */
    public function setting_environment_post($body)
    {
        $url = $this->urlApi . "api/data/master/environment";
        return ApiService::request($url, "POST", $body);
    }

    /** integrasi pembayaran setting  */
    public function setting_pusher($jenis, $id)
    {
        $url = $this->urlApi . "api/data/master/environment/jenis/" . $jenis . "/sekolah/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

      /** config environment api key */
      public function intergration_api($id)
      {
          $url = $this->urlApi . "api/data/master/environment/sekolah/" . $id;
          return ApiService::request($url, "GET", null);
      }

      /** post update massal penerimaan ppdb*/
      public function update_massal_status($body){
        $url = $this->urlApi . "api/data/ppdb/peserta/many/update/diterima";
        return ApiService::request($url, "PUT", $body);
      }
}
