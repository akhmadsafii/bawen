<?php

namespace App\ApiService\Ppdb;

use App\Helpers\ApiService;

class PublicPpdbApi
{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /**
     * get detail alur by id_sekolah
     */

    public function get_detail_alur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/alur/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get detail syarat by id_sekolah
     */

    public function get_detail_syarat($id)
    {
        $url = $this->urlApi . "api/data/ppdb/syarat/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get detail panduan by id_sekolah
     */

    public function get_detail_panduan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/panduan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /**
     * get detail banner
     */

    public function get_detail_banner($id)
    {
        $url = $this->urlApi . "api/data/ppdb/banner/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** register peserta  */
    public function post_register_peserta($body)
    {
        $url = $this->urlApi . "api/data/ppdb/peserta/register";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * get data jadwal by id sekolah
     */

    public function get_data_jadwal_all($id)
    {
        $url = $this->urlApi . "api/data/ppdb/jadwal-kegiatan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get setting sekolah  */
    public function get_setting_sekolah($id)
    {
        $url = $this->urlApi . "api/data/master/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data pengumuman by sekolah   */
    public function feeds($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function feeds_byid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update view feeds */
    public function view_feeds($id)
    {
        $url = $this->urlApi . "api/data/ppdb/pengumuman/dilihat/" . $id;
        return ApiService::request($url, "PUT", null);
    }

    /** file download */
    public function file_download_byid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/file/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** brosur download */
    public function brosur_download_byid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** Get Data FAQ */
    public function get_data_faq($id)
    {
        $url = $this->urlApi . "api/data/ppdb/faq/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get all peserta ditolak   */
    public function get_data_setting_ppdbbyid($id)
    {
        $url = $this->urlApi . "api/data/ppdb/setting/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }
    /** get data Sambutan  */
    public function get_data_setting_sambutan($id)
    {
        $url = $this->urlApi . "api/data/ppdb/sambutan/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data detail brosur  */
    public function get_detail_brosur($id)
    {
        $url = $this->urlApi . "api/data/ppdb/brosur/" . $id;
        return ApiService::request($url, "GET", null);
    }

     /** sample document file  */
     public function get_sampleinfo_doc($id)
     {
         $url = $this->urlApi . "api/data/ppdb/file/" . $id;
         return ApiService::request($url, "GET", null);
     }
}
