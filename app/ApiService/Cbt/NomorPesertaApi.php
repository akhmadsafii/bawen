<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class NomorPesertaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/nomor_peserta";
        return ApiService::request($url, "POST", $body);
    }

    public function create_many($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/many";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_data($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/data/rombel/tahun";
        return ApiService::request($url, "POST", $body);
    }

    public function remove_data($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/kosong/rombel";
        return ApiService::request($url, "POST", $body);
    }

    public function upload_excel($body, $id_ta_sm)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/file/excel-import/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function download_template($id_rombel, $tahun)
    {
        $url = $this->urlApi . "api/data/cbt/nomor_peserta/file/excel/rombel/" . $id_rombel . "/tahun/" . $tahun;
        // dd($url);
        return $url;
    }
}
