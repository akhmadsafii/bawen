<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SesiSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_rombel($id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/rombel/".$id_rombel."/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_siswa($id_kelas_siswa, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/profile/siswa/".$id_kelas_siswa."/ta_sm/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function by_ruang($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/profile/ruang/ta_sm/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }
   
    public function by_rombel($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/sesi_siswa/profile/rombel/ta_sm/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

}
