<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class JawabanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/jawaban";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/jawaban/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function update_koreksi($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/jawaban/update/koreksi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soal_by_siswa($id_soal, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/soal/".$id_soal."/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_detail_soal($id_jadwal, $id_bank, $nomor, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/jadwal/".$id_jadwal."/bank/".$id_bank."/nomor/".$nomor."/kelas_siswa/".$id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function bank_siswa($id_bank, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/bank/".$id_bank."/kelas_siswa/".$id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_gabungan($id_jadwal, $id_bank, $nomor, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/cbt/jawaban/v2/durasi/jadwal/".$id_jadwal."/bank/".$id_bank."/nomor/".$nomor."/kelas_siswa/".$id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
