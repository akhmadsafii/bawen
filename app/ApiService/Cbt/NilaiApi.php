<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class NilaiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/nilai";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/nilai/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/nilai";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function update_koreksi($body)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/koreksi";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function nilai_by_kelas_siswa($id_kelas_siswa, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/kelas_siswa/".$id_kelas_siswa."/ta_sm/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function nilai_by_siswa($id_kelas_siswa, $id_jadwal)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/kelas_siswa/".$id_kelas_siswa."/jadwal/".$id_jadwal;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_nilai($body)
    {
        $url = $this->urlApi . "api/data/cbt/nilai/update/nilai";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }



}
