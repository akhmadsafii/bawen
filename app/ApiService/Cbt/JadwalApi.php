<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class JadwalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/jadwal";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function jadwal_by_kelas_siswa($id_kelas_siswa, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/kelas_siswa/" . $id_kelas_siswa . "/ta_sm/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_rombel($id_jadwal)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/show/bank/rombel/by_jadwal/" . $id_jadwal;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_guru($id_guru, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/guru/" . $id_guru . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_rombel($id_rombel, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/rombel/" . $id_rombel . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_guru_dashboard($id_guru, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/dashboard/guru/" . $id_guru . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function jadwal_today()
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/data/today";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function jadwal_guru_today($id_guru, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/jadwal/data/today/guru/" . $id_guru . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
