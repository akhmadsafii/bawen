<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class HasilApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/hasil";
        return ApiService::request($url, "POST", $body);
    }

    public function filter_hasil($id_rombel, $id_jadwal, $id_tahun_ajar)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/hasil/rombel/" . $id_rombel . "/jadwal/" . $id_jadwal . "/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function detail_hasil($id_kelas_siswa, $id_jadwal, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/hasil/detail/kelas_siswa/" . $id_kelas_siswa . "/jadwal/" . $id_jadwal . "/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function detail_jawaban($id_kelas_siswa, $id_jadwal, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/hasil/jawaban/soal/detail/kelas_siswa/" . $id_kelas_siswa . "/jadwal/" . $id_jadwal . "/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function hasil_siswa($id_kelas_siswa, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/hasil/nilai/siswa/" . $id_kelas_siswa . "/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }
}
