<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class PengumumanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman";
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman/sekolah/profile/ta_sm/" . $id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function by_rombel($id_rombel, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman/rombel/".$id_rombel."/ta_sm/".$id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman/update/status/aktif";
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/pengumuman/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }
}
