<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class DaftarHadirApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/sekolah/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function print_ruang($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/cetak/ruang_jadwal";
        return ApiService::request($url, "POST", $body);
    }

    public function print_rombel($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/cetak/data/rombel_jadwal";
        return ApiService::request($url, "POST", $body);
    }

    public function logo_kiri($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/file1";
        return ApiService::request_image($url, "POST", $body);
    }

    public function logo_kanan($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/post/file2";
        return ApiService::request_image($url, "POST", $body);
    }

    public function ttd_proktor($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/post/ttd/proktor";
        return ApiService::request_image($url, "POST", $body);
    }

    public function ttd_pengawas1($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/post/ttd/peng1";
        return ApiService::request_image($url, "POST", $body);
    }

    public function ttd_pengawas2($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kehadiran/post/ttd/peng2";
        return ApiService::request_image($url, "POST", $body);
    }
}
