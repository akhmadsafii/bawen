<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class BankSoalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/sekolah/profile/ta_sm/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_guru($id_guru, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/guru/" . $id_guru . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_mapel($id_mapel)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/filter/mapel/" . $id_mapel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_rombel($id_rombel, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/data/rombel/" . $id_rombel . "/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function copy($body)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/copy";
        return ApiService::request($url, "POST", $body);
    }

    public function by_jadwal($id_jadwal)
    {
        $url = $this->urlApi . "api/data/cbt/bank_soal/jadwal/" . $id_jadwal;
        return ApiService::request($url, "GET", null);
    }

    public function download_import($id)
    {
        $url = $this->urlApi . "api/data/cbt/soal/file/excel/bank/" . $id;
        return $url;
    }
}
