<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class DurasiSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function jadwal_siswa($id_kelas_siswa, $id_jadwal)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/kelas_siswa/" . $id_kelas_siswa . "/jadwal/" . $id_jadwal;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function filter($id_rombel, $id_jadwal, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/rombel/" . $id_rombel . "/jadwal/" . $id_jadwal . "/ta_sm/" . $id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function change_status($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/durasi_siswa/update/change_status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
}
