<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SettingKartuApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/sekolah/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/setting_kartu";
        return ApiService::request($url, "POST", $body);
    }

    public function logo_kiri($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/file1";
        return ApiService::request_image($url, "POST", $body);
    }

    public function logo_kanan($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/post/file2";
        return ApiService::request_image($url, "POST", $body);
    }

    public function logo_ttd($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/post/file/ttd";
        return ApiService::request_image($url, "POST", $body);
    }

    public function stempel($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/post/file/position/stempel";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function cetak_rombel($id_rombel, $tahun, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/setting_kartu/cetak/rombel/" . $id_rombel . "/tahun/" . $tahun . "/ta_sm/" . $id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }
}
