<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class BeritaAcaraApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara";
        return ApiService::request($url, "POST", $body);
    }

    public function upload_logo_kiri($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/file1";
        return ApiService::request_image($url, "POST", $body);
    }

    public function upload_logo_kanan($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/post/file2";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_data($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/cbt/setting_berita_acara/data/rombel/tahun";
        return ApiService::request($url, "POST", $body);
    }

}
