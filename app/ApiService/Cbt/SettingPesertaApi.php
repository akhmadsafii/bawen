<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SettingPesertaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/setting_peserta";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/cbt/setting_peserta/sekolah/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    public function logo_kiri($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_peserta/file1";
        return ApiService::request_image($url, "POST", $body);
    }

    public function logo_kanan($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting_peserta/file2";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting_peserta/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
