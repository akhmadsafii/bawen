<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SoalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/soal";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/soal/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/soal";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/soal/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis_bank($id_bank, $id_jenis)
    {
        $url = $this->urlApi . "api/data/cbt/soal/bank/" . $id_bank . "/jenis/" . $id_jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_tampil($body)
    {
        $url = $this->urlApi . "api/data/cbt/soal/tampil";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function by_bank($id_bank)
    {
        $url = $this->urlApi . "api/data/cbt/soal/bank/" . $id_bank;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body, $id_bank)
    {
        $url = $this->urlApi . "api/data/cbt/soal/file/excel-import/bank/" . $id_bank;
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/soal/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
}
