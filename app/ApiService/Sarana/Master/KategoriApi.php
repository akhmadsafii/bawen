<?php

namespace App\ApiService\Sarana\Master;

use App\Helpers\ApiService;

class KategoriApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/jenis/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/info/". $id;
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/kategori-barang/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>