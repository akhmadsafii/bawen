<?php

namespace App\ApiService\Sarana\Master;

use App\Helpers\ApiService;

class SupllyerApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/suplayer";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/suplayer/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/suplayer/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/suplayer";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/sarpras/suplayer";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/sarpras/suplayer/info";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body){
        $url = $this->urlApi . "api/data/sarpras/suplayer/info";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/suplayer/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/suplayer/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>