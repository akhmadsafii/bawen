<?php

namespace App\ApiService\Sarana\Item;

use App\Helpers\ApiService;

class ItemApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/item";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/item/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_barang($id){
        $url = $this->urlApi . "api/data/sarpras/item/barang/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_available_item(){
        $url = $this->urlApi . "api/data/sarpras/item/allow/pinjam";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id){
        $url = $this->urlApi . "api/data/sarpras/item/jenis/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/item/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/item";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_many($body){
        $url = $this->urlApi . "api/data/sarpras/item/multi";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . "api/data/sarpras/item/info/". $id;
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/item/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/item/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>