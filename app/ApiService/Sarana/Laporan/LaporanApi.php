<?php

namespace App\ApiService\Sarana\Laporan;

use App\Helpers\ApiService;

class laporanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_barang(){
        $url = $this->urlApi . "api/data/sarpras/laporan/barang";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_item(){
        $url = $this->urlApi . "api/data/sarpras/laporan/item";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_user(){
        $url = $this->urlApi . "api/data/sarpras/laporan/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_item_by_lokasi($id){
        $url = $this->urlApi . "api/data/sarpras/laporan/item/lokasi/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_item_by_kondisi($id){
        $url = $this->urlApi . "api/data/sarpras/laporan/item/kondisi/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pengadaan(){
        $url = $this->urlApi . "api/data/sarpras/laporan/pengadaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pengadaan_diterima(){
        $url = $this->urlApi . "api/data/sarpras/laporan/pengadaan-diterima";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pemusnahan(){
        $url = $this->urlApi . "api/data/sarpras/laporan/pemusnahan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pemusnahan_diterima(){
        $url = $this->urlApi . "api/data/sarpras/laporan/pemusnahan-diterima";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_all_penyewaan(){
        $url = $this->urlApi . "api/data/sarpras/laporan/penyewaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_penyewaan($start,$end){
        $url = $this->urlApi . "api/data/sarpras/laporan/penyewaan/dari/". $start . "/sampai/" . $end ;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_penyewaan_pinjam(){
        $url = $this->urlApi . "api/data/sarpras/laporan/penyewaan/sewa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    

    
}

?>