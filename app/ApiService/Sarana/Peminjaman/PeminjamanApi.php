<?php

namespace App\ApiService\Sarana\Peminjaman;

use App\Helpers\ApiService;

class PeminjamanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/penyewaan";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_dipinjam(){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/sekolah/profile/sewa";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_kembali(){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/sekolah/profile";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_expired(){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/data/expiried";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_per_item($id){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/item/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_riwayat_user($id){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/user/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_status_user($id,$status){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/user/".$id."/status/".$status;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_riwayat_item($id){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/item/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function kembali($body){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/kembali";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/penyewaan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . "api/data/sarpras/penyewaan/info/". $id;
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/penyewaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }


}

?>