<?php

namespace App\ApiService\Sarana\User;

use App\Helpers\ApiService;

class UserSaranaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/sarpras/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/sarpras/user/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/sarpras/user";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/sarpras/user";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/sarpras/user/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/user/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    
    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/user/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_profile_image($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/sarpras/user/info";
        return ApiService::request_image($url, "POST", $body);
    } 

    public function update_profile($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/sarpras/user/info";
        return ApiService::request($url, "POST", $body);
    } 

    
    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/siswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    
}
