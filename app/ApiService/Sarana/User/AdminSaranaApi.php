<?php

namespace App\ApiService\Sarana\User;

use App\Helpers\ApiService;

class AdminSaranaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/sarpras/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/sarpras/admin/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/sarpras/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/sarpras/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/sarpras/admin/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/admin/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    
    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/admin/siswas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_profile_image($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/sarpras/admin/info";
        return ApiService::request_image($url, "POST", $body);
    } 

    public function update_profile($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/sarpras/admin/info";
        return ApiService::request($url, "POST", $body);
    } 
    
    public function delete($id)
    {
        $url = $this->urlApi . "api/data/admin/siswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    
}
