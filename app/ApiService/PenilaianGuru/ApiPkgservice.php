<?php

namespace App\ApiService\PenilaianGuru;

use App\Helpers\ApiService;

class ApiPkgservice
{
    /**
     * variabel global
     */
    public $urlApi;

    /**
     * __construct function with variable global for url domain
     */

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /** get profil  */
    public function get_info_profil()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /**
     * update profil
     * parameter json body
     */

    public function update_profil($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    /**
     * update profil with image
     * parameter json body
     */

    public function update_profil_file($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /** master data admin */
    public function get_data_master_admin()
    {
        $url = $this->urlApi . "api/data/pkg/admin/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master admin by id */
    public function master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/admin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update master admin */
    public function master_admin_post_file($body)
    {
        $url = $this->urlApi . "api/data/pkg/admin/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update master admin */
    public function master_admin_post($body)
    {
        $url = $this->urlApi . "api/data/pkg/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    /** post master admin */
    public function master_admin_post_filex($body)
    {
        $url = $this->urlApi . "api/data/pkg/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post master admin */
    public function master_admin_postx($body)
    {
        $url = $this->urlApi . "api/data/pkg/admin";
        return ApiService::request($url, "POST", $body);
    }


    /** remove master admin by id */
    public function remove_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove master admin by id */
    public function remove_permanent_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/admin/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** restore master admin by id */
    public function restore_master_admin_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** sampah admin  */
    public function trash_admin()
    {
        $url = $this->urlApi . "api/data/pkg/admin/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** master data jabatan */
    public function get_data_master_jabatan()
    {
        $url = $this->urlApi . "api/data/pkg/jabatan/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master data jabatan */
    public function get_data_master_jabatanid($id)
    {
        $url = $this->urlApi . "api/data/pkg/jabatan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** master data jabatan */
    public function update_master_jabatanid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/jabatan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** master data jabatan */
    public function post_master_jabatan($body)
    {
        $url = $this->urlApi . "api/data/pkg/jabatan";
        return ApiService::request($url, "POST", $body);
    }

    /** remove permanent jabatan */
    public function remove_permanent_master_jabatan_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/jabatan/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove jabatan */
    public function remove_master_jabatan_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/jabatan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** master data pendidikan  */
    public function get_data_master_pendidikan()
    {
        $url = $this->urlApi . "api/data/pkg/pendidikan/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master data pendidikan */
    public function get_data_master_pendidikanid($id)
    {
        $url = $this->urlApi . "api/data/pkg/pendidikan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update master pendidikan */
    public function update_master_pendidiakanid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/pendidikan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** remove master pendidikan */
    public function remove_master_pendidikan_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/pendidikan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** post master data pendidikan */
    public function post_master_pendidikan($body)
    {
        $url = $this->urlApi . "api/data/pkg/pendidikan";
        return ApiService::request($url, "POST", $body);
    }

    /** master data pangkat  */
    public function get_data_master_pangkat()
    {
        $url = $this->urlApi . "api/data/pkg/pangkat/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master data pangkat */
    public function get_data_master_pangkatid($id)
    {
        $url = $this->urlApi . "api/data/pkg/pangkat/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update master pangkat */
    public function update_master_pangkatid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/pangkat/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post master data pangkat */
    public function post_master_pangkat($body)
    {
        $url = $this->urlApi . "api/data/pkg/pangkat";
        return ApiService::request($url, "POST", $body);
    }

    /** remove master pendidikan */
    public function remove_master_pangkat_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/pangkat/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** setting data pkg */
    public function get_data_master_setting($id)
    {
        $url = $this->urlApi . "api/data/pkg/setting/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** postsetting data pkg */
    public function post_data_master_setting($body)
    {
        $url = $this->urlApi . "api/data/pkg/setting";
        return ApiService::request($url, "POST", $body);
    }

    /** master data penilaian */
    public function get_data_master_guru_penilai()
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master data profil guru penilai */
    public function get_data_master_guru_penilai_profil()
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/sekolah/profile/penilai";
        return ApiService::request($url, "GET", null);
    }

    /** master data get penilaian guru  */
    public function master_guru_penilaiid($id)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data mapel  */
    public function master_mapel()
    {
        $url = $this->urlApi . "api/data/master/mapel/profile";
        return ApiService::request($url, "GET", null);
    }

    /** update master guru penilai */
    public function update_master_guru_penilai($body)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/info";
        return ApiService::request($url, "POST", $body);
    }

    /** update master guru penilai */
    public function update_master_guru_penilaifile($body)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/info";
        return ApiService::request_image($url, "POST", $body);
    }

    /** post master guru penilai */
    public function post_master_guru_penilai($body)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai";
        return ApiService::request($url, "POST", $body);
    }

    /** post master guru penilai */
    public function post_master_guru_penilaifile($body)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai";
        return ApiService::request_image($url, "POST", $body);
    }

    /** remove master guru penilai */
    public function remove_master_guruid($id)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** delete permanent master guru penilai */
    public function remove_force_master_guruid($id)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** remove master guru penilai */
    public function restore_master_guru_id($id)
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    /** get data guru  */
    public function trash_master_guru_penilai()
    {
        $url = $this->urlApi . "api/data/pkg/guru_penilai/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    /** master data kompetensi */
    public function get_data_master_kompetensi()
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** show data kompetensi by id */
    public function get_data_master_kompetensibyid($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data kompetensi by id */
    public function update_data_master_kompetensibyid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** remove data kompetensi by id */
    public function remove_master_kompetensi($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** post data kompetensi  */
    public function post_data_master_kompetensi($body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi";
        return ApiService::request($url, "POST", $body);
    }

    /** master data indikator  */
    public function get_data_master_indikator()
    {
        $url = $this->urlApi . "api/data/pkg/indikator/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** master data indikator by id  */
    public function get_data_master_indikatorid($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data indikator */
    public function update_data_master_indikatoribyid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** delete indikator kompetensi */
    public function remove_master_indikator($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** post data indikator */
    public function post_data_master_indikatori($body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator";
        return ApiService::request($url, "POST", $body);
    }

    /** get data indikator per kompetensi */
    public function get_data_master_indikator_by_kompetensi($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator/kompetensi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data kompetensi sikap */
    public function get_data_master_kompetensi_sikap()
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_sikap/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data kompetensi sikap by id  */
    public function get_data_master_komsikapbyid($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_sikap/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data indikator kompetensi sikap */
    public function get_indikator_kompetensiSikap($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap/kompetensi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data kompetensi sikap by id */
    public function update_data_mastersikap_kompetensibyid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_sikap/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post data kompetensi sikap */
    public function post_data_mastersikap_kompetensi($body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_sikap";
        return ApiService::request($url, "POST", $body);
    }

    /** delete  kompetensi sikapF */
    public function remove_master_kompetensisikap($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_sikap/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get data kompetensi sikap */
    public function get_data_master_indikator_sikap()
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data indikator kompetensi sikap */
    public function get_indikator_Sikap($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data kompetensi sikap by id */
    public function update_data_master_indikatorsikap_byid($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post data kompetensi sikap by id */
    public function post_data_master_indikatorsikap($body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap";
        return ApiService::request($url, "POST", $body);
    }

    /** delete  kompetensi sikapF */
    public function remove_master_indikatorsikap($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_sikap/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get data kompetensi format c */
    public function get_data_master_formatC()
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_c/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data kompetensi format C by id  */
    public function get_data_master_formatcbyid($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_c/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data kompetensi format C by id  */
    public function get_data_masterindikator_formatcbyid($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c/kompetensi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** delete  kompetensi Format C */
    public function remove_master_formatCKompetensi($id)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_c/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** update data kompetensi Format C by id */
    public function update_data_master_formatCKompetensi($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_c/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** Post data kompetensi Format C  */
    public function post_data_master_formatCKompetensi($body)
    {
        $url = $this->urlApi . "api/data/pkg/kompetensi_c";
        return ApiService::request($url, "POST", $body);
    }

    /** get data kompetensi format c */
    public function get_data_master_formatC_indikator()
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data kompetensi format c by id */
    public function get_data_master_formatC_indikatorid($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update data kompetensi Format C by id */
    public function update_data_master_formatCIndikator($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** delete  kompetensi  Indikator Format C */
    public function remove_master_formatCKompetensiIndikator($id)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** Post data kompetensi Format C */
    public function post_data_master_formatCIndikator($body)
    {
        $url = $this->urlApi . "api/data/pkg/indikator_c";
        return ApiService::request($url, "POST", $body);
    }

    /** get data setting guru penilai */
    public function get_data_master_setting_guruPenilai()
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data setting guru penilai */
    public function get_data_master_setting_guruPenilaiid($id)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** update  get data setting guru penilai */
    public function update_data_settingguruPenilai($id, $body)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** post  get data setting guru penilai */
    public function post_data_settingguruPenilai($body)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai";
        return ApiService::request($url, "POST", $body);
    }

    /** remove setting guru penilai */
    public function remove_settingguruPenilai($id)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get data setting guru penilai */
    public function get_data_periode_setting_guruPenilai($tahun)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/sekolah/profile/periode/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data task penilai  */
    public function get_task_penilaian_guru($id, $tahun)
    {
        $url = $this->urlApi . "api/data/pkg/setting_guru_penilai/penilai/" . $id . "/periode/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get data form penilai */
    public function get_form_penilaian($id_guru, $id_penilai, $periode, $id_kompetensi)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_kinerja/guru/" . $id_guru . "/penilai/" . $id_penilai . "/kompetensi/" . $id_kompetensi . "/periode/" . $periode;
        return ApiService::request($url, "GET", null);
    }

    /** post data form survei penilai kinerja guru  kompetensi penilaian */
    public function post_survei_kompetensi_penilaian($body)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_kinerja/create_update";
        return ApiService::request($url, "POST", $body);
    }

    /** get data form penilai sikap */
    public function get_form_penilaian_sikap($id_guru, $id_penilai, $periode, $id_kompetensi)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_sikap/guru/" . $id_guru . "/penilai/" . $id_penilai . "/kompetensi/" . $id_kompetensi . "/periode/" . $periode;
        return ApiService::request($url, "GET", null);
    }

    /** post data form survei penilai kinerja guru  kompetensi penilaian */
    public function post_survei_kompetensi_sikapguru($body)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_sikap/create_update";
        return ApiService::request($url, "POST", $body);
    }

    /** get data form format c */
    public function get_form_formatC($id_guru, $id_penilai, $periode)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_c/guru/" . $id_guru . "/penilai/" . $id_penilai . "/periode/" . $periode;
        return ApiService::request($url, "GET", null);
    }

    /** post data form survei format C */
    public function post_survei_kompetensi_FormatC($body)
    {
        $url = $this->urlApi . "api/data/pkg/penilaian_c/create_update";
        return ApiService::request($url, "POST", $body);
    }

    /** get data lampiran  */
    public function get_data_lampiran($id_guru, $id_penilai, $periode)
    {
        $url = $this->urlApi . "api/data/pkg/rekap/lamp/guru/" . $id_guru . "/penilai/" . $id_penilai . "/periode/" . $periode;
        return ApiService::request($url, "GET", null);
    }

    /** get data lampiran  */
    public function get_data_lampiranD($id_guru, $id_penilai, $periode)
    {
        $url = $this->urlApi . "api/data/pkg/rekap/lamp1d/guru/" . $id_guru . "/penilai/" . $id_penilai . "/periode/" . $periode;
        return ApiService::request($url, "GET", null);
    }

    /** get data dashboard */
    public function get_data_dashboard(){
        $url = $this->urlApi . "api/data/pkg/admin/data/dashboard";
        return ApiService::request($url, "GET", null);
    }

    /** get data count dashboard */
    public function get_data_count_dashboard($periode){
        $url = $this->urlApi . "api/data/pkg/admin/data/dashboard/periode/".$periode;
        return ApiService::request($url, "GET", null);
    }

    /** get dashboard chart hight max */
    public function get_data_dashboard_max(){
        $url = $this->urlApi . "api/data/pkg/admin/data/dashboard/max";
        return ApiService::request($url, "GET", null);
    }

     /** get dashboard chart hight max */
     public function get_data_dashboard_min(){
        $url = $this->urlApi . "api/data/pkg/admin/data/dashboard/min";
        return ApiService::request($url, "GET", null);
    }

    /** get dashboard chart hight max */
    public function get_data_dashboard_min_periode($tahun){
        $url = $this->urlApi . "api/data/pkg/admin/data/min/dashboard/periode/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get dashboard chart hight max */
    public function get_data_dashboard_max_periode($tahun){
        $url = $this->urlApi . "api/data/pkg/admin/data/max/dashboard/periode/".$tahun;
        return ApiService::request($url, "GET", null);
    }

    /** get rekap hasil akhir guru  */
    public function get_data_rekap_guru($id,$tahun){
        $url = $this->urlApi . "api/data/pkg/rekap/lamp1d/guru/".$id."/periode/".$tahun;
        return ApiService::request($url, "GET", null);
    }

}
