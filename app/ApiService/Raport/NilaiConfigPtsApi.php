<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiConfigPtsApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/profile/get/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun_ajar($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/profile/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_config_pts/trash/admin";
        return ApiService::request($url, "GET", null);
    }

}
