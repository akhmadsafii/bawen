<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class LihatPtsApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function manual_auth_siswa($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/siswa/manual/tahun/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function manual_kelas_siswa($id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/kelas_siswa/" . $id_kelas_siswa . "/manual/tahun/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function k16_auth_siswa($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/siswa/k16/tahun/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function k16_kelas_siswa($id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/kelas_siswa/" . $id_kelas_siswa . "/k16/tahun/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    // v1 kd raport
    public function kd_kelas_siswa_v1($id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/kelas_siswa/" . $id_kelas_siswa . "/tahun/" . $id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function kd_auth_siswa_v1($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/siswa/tahun/" . $id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    // v2 Kd Raport
    public function kd_kelas_siswa_v2($id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/v2/kelas_siswa/" . $id_kelas_siswa . "/tahun/" . $id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function kd_auth_siswa_v2($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/v2/siswa/tahun/" . $id_ta_sm;
        return ApiService::request($url, "GET", null);
    }


    public function print_leger_kd($id_tahun_ajar, $id_rombel)
    {
        $url = $this->urlApi . "api/data/raport/lihat_pts/leger/ta_sm/" . $id_tahun_ajar . "/rombel/" . $id_rombel;
        return ApiService::request($url, "GET", null);
    }
}
