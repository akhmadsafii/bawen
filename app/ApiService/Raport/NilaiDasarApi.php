<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiDasarApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_nilai_jenis($id_mapel, $id_kelas, $id_rombel, $jenis)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/nilai/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/jenis/".$jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tampil_data_guru($id_kd, $id_mapel, $id_kelas, $id_rombel, $jenis, $id_guru, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/kd/".$id_kd."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/jenis/".$jenis."/guru/".$id_guru."/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tampil_gabungan($id_kd, $id_mapel, $id_kelas, $id_rombel, $jenis, $id_guru, $tahun, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/kd/".$id_kd."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/jenis/".$jenis."/guru/".$id_guru."/tahun/".$tahun."/sm/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function numrows($id_kd, $id_mapel, $id_kelas, $id_rombel, $jenis, $id_guru)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/kd/".$id_kd."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/jenis/".$jenis."/guru/".$id_guru;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tampil_data_guru_by_siswa($id_kd, $id_mapel, $id_kelas, $id_rombel, $jenis, $id_guru, $id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/kd/".$id_kd."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/jenis/".$jenis."/guru/".$id_guru."/siswa/".$id_kelas_siswa."/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
