<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiAbsensiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensis/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/rombel/".$id_rombel;
        return ApiService::request($url, "GET", null);
    }

    public function get_gabungan($tahun, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/tahun/".$tahun."/sm/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function import($tahun, $id_tahun_ajar, $id_rombel)
    {
        $url = $this->urlApi . "api/data/raport/nilai_absensi/example-import/tahun/".$tahun."/sm/".$id_tahun_ajar."/".$id_rombel;
        return $url;
    }
}
