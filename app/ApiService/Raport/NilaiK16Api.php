<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiK16Api
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_k16";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_k16";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_k16/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function get_gabungan($tahun, $id_guru_pelajaran, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai_k16/siswa/tahun/".$tahun."/guru_pelajaran/".$id_guru_pelajaran."/sm/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
