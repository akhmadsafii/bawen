<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class ConfigTemplateApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/config-template";
        return ApiService::request($url, "POST", $body);
    }

    public function by_tahun_ajar($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/config-template/tahun-ajaran/sm/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun_jurusan($id_jurusan, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/config-template/jurusan/".$id_jurusan."/tahun-ajaran/sm/".$id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

}
