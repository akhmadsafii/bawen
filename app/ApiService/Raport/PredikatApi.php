<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class PredikatApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/predikat";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/predikat/room/" . $id_room. "/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/predikat/absen-siswa/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/predikat/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/predikat";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/predikat/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/predikat/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/predikat/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/predikat/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/predikat/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/raport/predikat/sekolah/profile/parameter/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/predikat/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/predikat/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/predikat/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/predikat/trash/admin";
        return ApiService::request($url, "GET", null);
    }


    public function import()
    {
        $url = $this->urlApi . "api/data/raport/predikat/file/example-import";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/raport/predikat/file/predikat-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_range($nilai)
    {
        $url = $this->urlApi . "api/data/raport/predikat/get_range/hasil_akhir/".$nilai;
        return ApiService::request($url, "GET", null);
    }
}
