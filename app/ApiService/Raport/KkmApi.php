<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class KkmApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/kkm";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/kkm";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/raport/kkm";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/kkm/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/kkm/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/kkm/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/kkm/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_tahun_ajar($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/kkm/profile/ta_sm/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_kelas($id_ta_sm, $id_kelas)
    {
        $url = $this->urlApi . "api/data/raport/kkm/profile/ta_sm/" . $id_ta_sm . "/show/kelas/" . $id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/kkm/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/kkm/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/kkm/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/kkm/trash/admin";
        return ApiService::request($url, "GET", null);
    }
}
