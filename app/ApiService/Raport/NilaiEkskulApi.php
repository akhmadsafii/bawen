<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiEkskulApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/details/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_ekskul($id_tahun_ajar, $id_ekskul)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/wali-kelas/tahun/".$id_tahun_ajar."/ekskul/".$id_ekskul;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_gabungan($tahun, $id_ta_sm, $id_ekskul)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/nilai/wali-kelas/tahun/".$tahun."/sm/".$id_ta_sm."/ekskul/".$id_ekskul;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskuls/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/raport/nilai_ekskul/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
