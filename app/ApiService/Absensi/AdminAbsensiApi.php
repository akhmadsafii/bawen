<?php

namespace App\ApiService\Absensi;

use App\Helpers\ApiService;

class AdminAbsensiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/absensi/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/absensi/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/absensi/admin/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kode($body)
    {
        $url = $this->urlApi . "api/data/absensi/admin/profile/siswa/kode";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/absensi/admin/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/absensi/admin/update/status/aktif";
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/absensi/admin/delete/".$id;
        return ApiService::request($url, "DELETE", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/absensi/admin/update/profile/password";
        return ApiService::request($url, "POST", $body);
    }
}
