<?php

namespace App\ApiService\Absensi;

use App\Helpers\ApiService;

class JadwalApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/absensi/jadwal";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/jadwal";
        return ApiService::request($url, "POST", $body);
    }    
   
    public function update($body)
    {
        $url = $this->urlApi . "api/data/absensi/jadwal/info";
        return ApiService::request($url, "PUT", $body);
    }    
    
    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/absensi/jadwal/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/absensi/jadwal/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function get_sekolah()
    {
        $url = $this->urlApi . "api/data/absensi/jadwal/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/absensi/jadwal/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
   

}
