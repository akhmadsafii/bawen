<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class AgendaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/agenda";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda";
        return ApiService::request_image($url, "POST", $body);
    }
    

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/info";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_sosial($sosial)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/data/my-agenda/".$sosial;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function id_sekolah($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/sekolah/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function by_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/get/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/agenda/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/agenda/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }
}
