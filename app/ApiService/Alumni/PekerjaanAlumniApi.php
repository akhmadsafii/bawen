<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class PekerjaanAlumniApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/sekolah/profile/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/sekolah/profile/filter/jenis/" . $id_jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function by_alumni($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan-alumni/alumni/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
