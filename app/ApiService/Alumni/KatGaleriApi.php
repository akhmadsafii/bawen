<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class KatGaleriApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function sekolah($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/sekolah/data/page/sekolah?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/kategori-galeri/update/status";
        return ApiService::request($url, "POST", $body);
    }
}
