<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class SurveyApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/survei";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/survei";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/survei/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/survei/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/survei/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/survei/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/survei/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function sekolah($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/alumni/survei/sekolah/profile?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        return ApiService::request($url, "GET", null);
    }


    public function get_id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/survei/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_id_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/alumni/survei/data/kategori/" . $id_kategori;
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/survei/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/survei/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/survei/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/survei/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }
}
