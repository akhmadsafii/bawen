<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class PekerjaanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function by_id_alumni($id_alumni)
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/alumni/get/user/".$id_alumni;
        return ApiService::request($url, "GET", null);
    }
    
    public function by_auth_alumni()
    {
        $url = $this->urlApi . "api/data/alumni/pekerjaan/alumni/profile";
        return ApiService::request($url, "GET", null);
    }
}
