importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
var firebaseConfig = {
    apiKey: "AIzaSyCj2LfwxSDToSdWmFOk-Kyyp6k52Gi11NI",
    authDomain: "smart-school-b1610.firebaseapp.com",
    projectId: "smart-school-b1610",
    storageBucket: "smart-school-b1610.appspot.com",
    messagingSenderId: "431265159972",
    appId: "1:431265159972:web:81acd8eb57be07b242b1b1"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const { title, body} = payload.notification;
    const notificationOptions = {
        body,
    };

    self.registration.showNotification(title,
        notificationOptions);
});
